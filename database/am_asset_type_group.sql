-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3306
-- Thời gian đã tạo: Th8 07, 2018 lúc 04:14 AM
-- Phiên bản máy phục vụ: 5.7.19
-- Phiên bản PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `wordpress`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `am_asset_type_group`
--

DROP TABLE IF EXISTS `am_asset_type_group`;
CREATE TABLE IF NOT EXISTS `am_asset_type_group` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `assettype` varchar(1000) DEFAULT NULL,
  `maxmonth` int(11) DEFAULT NULL,
  `groupname` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `am_asset_type_group`
--

INSERT INTO `am_asset_type_group` (`id`, `assettype`, `maxmonth`, `groupname`) VALUES
(1, 'default', 36, 'Thiết bị, linh kiện khác'),
(2, '[9,18,32,39,44,48,2,55,57,21,26,19,20,37,45,52,3,17,16,27,11,33,41,14,6,5,4,60,28,31,34,38,42,47,50,53,56,58,61,1,12,35,10,43,13,51,54,15,62]', 60, 'Thiết bị hạ tầng,Thiết bị di động, Laptop,Desktop,Camera,Monitor');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
