CREATE TABLE `am_user_role_mapping` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Domain` varchar(45) NOT NULL DEFAULT '',
  `Permission` varchar(100) NOT NULL DEFAULT '',
  `Department` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
