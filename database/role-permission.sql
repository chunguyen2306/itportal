-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: wordpress
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `am_user_role_mapping`
--

DROP TABLE IF EXISTS `am_user_role_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_user_role_mapping` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Domain` varchar(45) NOT NULL DEFAULT '',
  `Permission` varchar(100) NOT NULL DEFAULT '',
  `Department` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_user_role_mapping`
--

LOCK TABLES `am_user_role_mapping` WRITE;
/*!40000 ALTER TABLE `am_user_role_mapping` DISABLE KEYS */;
INSERT INTO `am_user_role_mapping` VALUES (8,'tiennv6','[\"1\",\"2\"]',''),(9,'binhnt','[\"7\"]','B2S Studio'),(10,'landtt','[\"7\"]','Administration Management'),(11,'annh','[\"7\"]','Payment Product'),(12,'vietth2','[\"7\"]','Bank Integration'),(13,'dzung','[\"7\"]','Business & Marketing'),(14,'nhambt','[\"7\"]','Business Analytics'),(15,'tranntb','[\"7\"]','Design & Development'),(16,'quanglq','[\"7\"]','Customer & Game Services'),(17,'tomherron','[\"7\"]','Corporate, Investors and Strategy'),(18,'cuongbh','[\"7\"]','Channel'),(19,'namm','[\"7\"]','Data & Analytics Lab'),(20,'danhnt','[\"7\"]','Data Center'),(21,'ngantl2','[\"7\"]','Facility Management'),(22,'thanhtc','[\"7\"]','Finance & Operations'),(23,'thupth','[\"7\"]','External Affairs'),(24,'minhlh','[\"7\"]','Senior Management Team'),(25,'thanhtc','[\"7\"]','F&O'),(26,'loanhm','[\"7\"]','International Financial Reporting Standards'),(27,'nhukm','[\"7\"]','Game Business Development'),(28,'tranglh','[\"7\"]','Fund Channel'),(29,'longnv','[\"7\"]','Financial Information System'),(30,'tuandm3','[\"7\"]','Game RnD'),(31,'vietph','[\"7\"]','Game Technical'),(32,'trungnk','[\"7\"]','New FireBat Studio'),(33,'thanhnl','[\"7\"]','Information Security Office'),(34,'thanhnl','[\"7\"]','Information Technology'),(35,'nhanta','[\"7\"]','Sales IP'),(36,'vunguyen','[\"7\"]','Human Resources'),(37,'longdh','[\"7\"]','Internet of Things'),(38,'loanhm','[\"7\"]','IFRS'),(39,'thuyntp','[\"7\"]','Legal'),(40,'ancm','[\"7\"]','MadPoly Studio'),(41,'minhlh','[\"7\"]','MT'),(42,'chrisliu','[\"7\"]','Mobile Business Development'),(43,'thanhcnn','[\"7\"]','Merchant Platform'),(44,'hungnp','[\"7\"]','Mobile Publishing 2'),(45,'trungnk','[\"7\"]','FBS'),(46,'ninhmv','[\"7\"]','PASS'),(47,'hainpt','[\"7\"]','Payment App'),(48,'minhnh','[\"7\"]','PG3'),(49,'annh','[\"7\"]','PPR'),(50,'phuongbm','[\"7\"]','Product Group 1'),(51,'soant','[\"7\"]','Project Team'),(52,'hungnt','[\"7\"]','Product Group 2'),(53,'thanhnv2','[\"7\"]','Purchasing'),(54,'nhanta','[\"7\"]','SIP'),(55,'kiettt','[\"7\"]','Server & Storage'),(56,'minhlh','[\"7\"]','SMT'),(57,'longnt4','[\"7\"]','TALK'),(58,'huypm','[\"7\"]','Service Desk'),(59,'trongnvd','[\"7\"]','Traffic Acquisition'),(60,'tiepvv','[\"7\"]','ZaloPay Integration'),(61,'chinhnc','[\"7\"]','Zalo Group. Ads'),(62,'khaivq','[\"7\"]','Zalo Group. Business Management'),(63,'tungnt8','[\"7\"]','Zalo Group. Epi'),(64,'longpk','[\"7\"]','Zalo Group. Laban'),(65,'santh','[\"7\"]','Zalo Group. Media'),(66,'anhnv','[\"7\"]','Zalo Group. News'),(67,'minhlb','[\"7\"]','Zalo Group. Product'),(68,'tuanna14','[\"7\"]','Zalo Group. Sales'),(69,'tunm','[\"7\"]','Zalo Group. Technical'),(70,'tiepvv','[\"7\"]','ZPI'),(71,'tult','[\"8\"]','IT'),(72,'vandt','[\"3\"]',NULL),(73,'anhdt','[\"3\"]',NULL),(74,'quihn','[\"3\"]',NULL),(75,'cucth','[\"4\"]',NULL),(76,'khoalt','[\"4\"]','IT');
/*!40000 ALTER TABLE `am_user_role_mapping` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-04 17:49:44
