-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 14, 2018 at 07:30 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wordpress`
--

-- --------------------------------------------------------

--
-- Table structure for table `am_asset_model_review`
--

DROP TABLE IF EXISTS `am_asset_model_review`;
CREATE TABLE IF NOT EXISTS `am_asset_model_review` (
  `ID` int(10) UNSIGNED NOT NULL,
  `UserName` varchar(45) NOT NULL DEFAULT '',
  `Comment` longtext,
  `Date` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `Rate` double NOT NULL DEFAULT '0',
  `ModelID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `AssetName` varchar(45) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `am_asset_model_review`
--

INSERT INTO `am_asset_model_review` (`ID`, `UserName`, `Comment`, `Date`, `Rate`, `ModelID`, `AssetName`) VALUES
(14, 'tientm2', '<p>asdfasdfad</p>\n<p>dfadsfasdf</p>\n<p>adsf</p>\n<p>adf</p>\n<p>asdf</p>', 1511422146, 5, 2749, '0'),
(15, 'tientm2', '<p>dfadsfadsf</p>', 1511422235, 3, 2749, '0'),
(16, 'tientm2', '<p>sadFADSFASDFASDF</p>', 1511423580, 3, 2749, '0'),
(17, 'tientm2', '<p>DSFEWRWERWER</p>', 1511423598, 1, 2749, '0'),
(18, 'tientm2', '<p>agdfasdfadsf</p>', 1511423609, 1, 2749, '0'),
(19, 'tientm2', '<p>gfsfdgdsfgsdfg</p>', 1511423624, 1, 2749, '0'),
(20, 'tientm2', '<p>adsfasdfadf</p>', 1511423664, 3, 2749, '0'),
(21, 'tientm2', '<p>dsfdfgsdfg</p>', 1511423673, 2, 2749, '0'),
(22, 'tientm2', '<p>adsfasdf</p>', 1511423739, 4, 2749, '0'),
(23, 'tientm2', '<p>adsfadsf</p>', 1511423872, 3.5, 2749, '0'),
(24, 'tientm2', '<p>dfadsfasdfasdf</p>', 1511423878, 0.5, 2749, '0'),
(25, 'tientm2', '<p>asdfasdfadsf</p>', 1511423889, 1.5, 2749, '0'),
(26, 'tientm2', '<p>dfsdfgsdf</p>', 1511423899, 0.5, 2749, '0'),
(27, 'tientm2', '<p>werewqewrqwerqwer</p>', 1511423919, 4.5, 2749, '0'),
(28, 'tientm2', '<p>okay</p>', 1511432572, 4.5, 2749, '0'),
(29, 'tientm2', '<p>asdasdasdads</p>', 1511432685, 3.5, 2749, '0'),
(30, 'tientm2', '<p>adadasd</p>', 1511435395, 3.5, 2749, '0'),
(31, 'tientm2', '<p>th&ocirc;ng tin</p>', 1511437643, 3.5, 2749, '0'),
(32, 'tientm2', '<p>Th&ocirc;ng tin đ&aacute;nh gi&aacute;</p>', 1511438245, 3.5, 2749, '0'),
(33, 'tientm2', '<p>đ&aacute;nh gi&aacute;</p>', 1511773751, 3, 2749, '0'),
(34, 'tientm2', '<p>Th&ocirc;ng tin đ&aacute;nh gi&aacute;</p>', 1511773787, 4, 2749, '0'),
(35, 'tientm2', '<p>test</p>', 1521104339, 5, 5141, '0'),
(36, 'tientm2', '<p>test3</p>', 1521104472, 3.5, 5141, '0');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
