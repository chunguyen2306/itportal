-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 25, 2018 at 10:10 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wordpress`
--

-- --------------------------------------------------------

--
-- Table structure for table `am_page_content`
--

DROP TABLE IF EXISTS `am_page_content`;
CREATE TABLE IF NOT EXISTS `am_page_content` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `value` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `page` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `am_page_content`
--

INSERT INTO `am_page_content` (`id`, `name`, `value`, `page`) VALUES
(1, 'Tieudecot1', 'Phòng công nghệ thông tin', 'master-page-home.php'),
(2, 'Vitri1cot1', 'helpdesk@vng.com.vn', 'master-page-home.php'),
(3, 'Vitri2cot1', '1111', 'master-page-home.php'),
(4, 'Vitri3cot1', '+84934.088.588', 'master-page-home.php'),
(5, 'Vitri4cot1', 'Kênh hỗ trợ Yammer', 'master-page-home.php'),
(6, 'Vitri4cot1Url', 'https://www.yammer.com/vng.com.vn/#/threads/inGroup?type=in_group&feedId=11177278', 'master-page-home.php'),
(7, 'Tieudecot3', 'Quy định', 'master-page-home.php'),
(8, 'Vitri1cot3Text', 'Quy định 1', 'master-page-home.php'),
(9, 'Vitri1cot3Url', 'https://vnexpress.net', 'master-page-home.php'),
(10, 'Vitri2cot3Text', 'Quy định 2', 'master-page-home.php'),
(11, 'Vitri2cot3Url', 'https://google.com', 'master-page-home.php'),
(12, 'Vitri3cot3Text', 'Quy định 3', 'master-page-home.php'),
(13, 'Vitri3cot3Url', 'https://google.com', 'master-page-home.php'),
(14, 'Vitri4cot3Text', 'Quy định 4', 'master-page-home.php'),
(15, 'Vitri4cot3Url', 'https://google.com', 'master-page-home.php'),
(16, 'Tieudecot2', 'Hướng dẫn', 'master-page-home.php'),
(17, 'Vitri1cot2Text', 'Hướng dẫn 1', 'master-page-home.php'),
(18, 'Vitri1cot2Url', 'https://google.com', 'master-page-home.php'),
(19, 'Vitri2cot2Text', 'Hướng dẫn 2', 'master-page-home.php'),
(20, 'Vitri2cot2Url', 'https://google.com', 'master-page-home.php'),
(21, 'Vitri3cot2Text', 'Hướng dẫn 3', 'master-page-home.php'),
(22, 'Vitri3cot2Url', 'https://google.com', 'master-page-home.php'),
(23, 'Vitri4cot2Text', 'Hướng dẫn 4', 'master-page-home.php'),
(24, 'Vitri4cot2Url', 'https://google.com', 'master-page-home.php'),
(25, 'Session2Text', 'THIẾT BỊ THÔNG DỤNG', 'home.php '),
(26, 'Session3Text', 'Thiết bị khác test', 'home.php'),
(27, '27', 'https://google.com', 'master-page-home.php'),
(28, '28', 'Huong dan 1 test', 'master-page-home.php'),
(29, '29', 'https://vnexpress.net', 'master-page-home.php'),
(30, '30', 'Chính sách', 'master-page-home.php'),
(44, 'Session3Text', 'Sản phẩm tương tự', 'detail.php');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
