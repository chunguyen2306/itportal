CREATE TABLE `am_options` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `OptionName` varchar(100) NOT NULL,
  `OptionValue` text,
  `OptionCode` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `OptionName_UNIQUE` (`OptionName`),
  UNIQUE KEY `OptionCode_UNIQUE` (`OptionCode`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
