-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 10, 2018 at 11:16 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wordpress`
--

-- --------------------------------------------------------

--
-- Table structure for table `am_discountperyear`
--

DROP TABLE IF EXISTS `am_discountperyear`;
CREATE TABLE IF NOT EXISTS `am_discountperyear` (
  `ID` int(11) NOT NULL,
  `AssetTypeGroupID` int(11) DEFAULT NULL,
  `Year` int(11) DEFAULT NULL,
  `Percent` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `am_discountperyear`
--

INSERT INTO `am_discountperyear` (`ID`, `AssetTypeGroupID`, `Year`, `Percent`) VALUES
(1, 2, 1, 30),
(2, 2, 2, 25),
(3, 2, 3, 20),
(4, 2, 4, 15),
(5, 2, 5, 10),
(6, 1, 1, 50),
(7, 1, 2, 30),
(8, 1, 3, 20);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
