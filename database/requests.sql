-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: wordpress
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `am_request_approval_code`
--

DROP TABLE IF EXISTS `am_request_approval_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_approval_code` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RequestID` int(11) DEFAULT NULL,
  `Code` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_approval_code`
--

LOCK TABLES `am_request_approval_code` WRITE;
/*!40000 ALTER TABLE `am_request_approval_code` DISABLE KEYS */;
INSERT INTO `am_request_approval_code` VALUES (21,45,'80J9N8HG2XJOOXGRNQ33DHBAZV5NT04J17K8F0JX'),(22,56,'20VLML63PQK9XJSDCL002PSED8PFV8UZQI2HAJF0');
/*!40000 ALTER TABLE `am_request_approval_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_categories`
--

DROP TABLE IF EXISTS `am_request_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_categories` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_categories`
--

LOCK TABLES `am_request_categories` WRITE;
/*!40000 ALTER TABLE `am_request_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_request_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_comment_role`
--

DROP TABLE IF EXISTS `am_request_comment_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_comment_role` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Domain` varchar(45) DEFAULT NULL,
  `StepID` int(11) DEFAULT NULL,
  `Done` tinyint(1) DEFAULT NULL,
  `CommentID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_comment_role`
--

LOCK TABLES `am_request_comment_role` WRITE;
/*!40000 ALTER TABLE `am_request_comment_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_request_comment_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_comments`
--

DROP TABLE IF EXISTS `am_request_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_comments` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Comment` varchar(1000) DEFAULT '',
  `Time` datetime NOT NULL,
  `StepIndex` int(10) unsigned NOT NULL DEFAULT '0',
  `RequestID` int(10) unsigned NOT NULL DEFAULT '0',
  `Owner` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_comments`
--

LOCK TABLES `am_request_comments` WRITE;
/*!40000 ALTER TABLE `am_request_comments` DISABLE KEYS */;
INSERT INTO `am_request_comments` VALUES (1,'sxcvbn','2018-06-04 15:07:23',2,61,'tult'),(2,'sd','2018-06-04 15:31:27',0,62,'tiennv6');
/*!40000 ALTER TABLE `am_request_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_data_types`
--

DROP TABLE IF EXISTS `am_request_data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_data_types` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `DisplayName` varchar(100) NOT NULL DEFAULT '',
  `Description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_data_types`
--

LOCK TABLES `am_request_data_types` WRITE;
/*!40000 ALTER TABLE `am_request_data_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_request_data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_definition`
--

DROP TABLE IF EXISTS `am_request_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_definition` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CreateDate` datetime NOT NULL,
  `Creator` varchar(50) NOT NULL DEFAULT '',
  `IsDelete` tinyint(1) NOT NULL DEFAULT '0',
  `RequestReasonID` int(10) unsigned NOT NULL DEFAULT '0',
  `Description` varchar(200) NOT NULL DEFAULT '',
  `Name` varchar(50) NOT NULL DEFAULT '',
  `DisplayName` varchar(50) NOT NULL DEFAULT '',
  `OwnerField` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_definition`
--

LOCK TABLES `am_request_definition` WRITE;
/*!40000 ALTER TABLE `am_request_definition` DISABLE KEYS */;
INSERT INTO `am_request_definition` VALUES (80,'2017-11-17 17:58:06','dev',0,1,'đá','cap_tai_san','Cấp tài sản','nguoi_yeu_cau'),(81,'2017-11-23 21:04:53','dev',0,1,'ád','cap_ban_quyen','Cấp bản quyền','nguoi_yeu_cau'),(82,'2017-11-23 21:08:07','dev',0,1,'ád','cap_ban_quyen','Cấp tài sản IT','nguoi_yeu_cau'),(83,'2017-12-31 23:11:04','dev',1,3,'Báo mất thiết bị','bao_mat_thiet_bi','Báo mất thiết bị','nguoi'),(86,'2018-01-31 01:57:16','dev',1,6,'Trả thiết bị','tra_thiet_bi','Trả thiết bị','nguoi_yeu_cau'),(87,'2018-01-31 09:11:36','dev',0,5,'Hư hỏng thiết bị','hu_hong_thiet_bi','Hư hỏng thiết bị','nguoi_yeu_cau'),(88,'2018-01-31 09:21:16','dev',0,6,'Thay thế thiết bị','thay_the_thiet_bi','Thay thế thiết bị','nguoi_yeu_cau'),(89,'2018-02-05 10:07:41','dev',0,1,'Yêu cầu tài sản','yeu_cau_tai_san','Yêu cầu tài sản','nguoi_yeu_cau'),(91,'2018-04-20 19:50:55','tiennv6',0,1,'ád','dat_hang','Đặt hàng','nguoi_yeu_cau');
/*!40000 ALTER TABLE `am_request_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_definition_model_group`
--

DROP TABLE IF EXISTS `am_request_definition_model_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_definition_model_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DisplayName` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_definition_model_group`
--

LOCK TABLES `am_request_definition_model_group` WRITE;
/*!40000 ALTER TABLE `am_request_definition_model_group` DISABLE KEYS */;
INSERT INTO `am_request_definition_model_group` VALUES (25,'Nhóm không cần duyệt'),(28,'sd');
/*!40000 ALTER TABLE `am_request_definition_model_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_definition_model_group_detail`
--

DROP TABLE IF EXISTS `am_request_definition_model_group_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_definition_model_group_detail` (
  `GroupID` int(11) NOT NULL,
  `ModelName` varchar(500) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_definition_model_group_detail`
--

LOCK TABLES `am_request_definition_model_group_detail` WRITE;
/*!40000 ALTER TABLE `am_request_definition_model_group_detail` DISABLE KEYS */;
INSERT INTO `am_request_definition_model_group_detail` VALUES (25,'536'),(25,'531'),(25,'537'),(25,'538'),(25,'539'),(25,'540'),(25,'541'),(25,'542'),(25,'543'),(25,'544'),(25,'545'),(25,'546'),(25,'547'),(25,'548'),(25,'2743'),(25,'3626'),(25,'4213');
/*!40000 ALTER TABLE `am_request_definition_model_group_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_feature_mapping`
--

DROP TABLE IF EXISTS `am_request_feature_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_feature_mapping` (
  `ID` int(10) unsigned NOT NULL,
  `Name` varchar(200) NOT NULL DEFAULT '',
  `Output` text,
  `Mapping` text,
  `RequestID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_feature_mapping`
--

LOCK TABLES `am_request_feature_mapping` WRITE;
/*!40000 ALTER TABLE `am_request_feature_mapping` DISABLE KEYS */;
INSERT INTO `am_request_feature_mapping` VALUES (1,'Báo Mất Tài Sản','{\"CurrentDate\":\"\",\"CurrentUser\":\"\",\"Note\":\"\",\"Assets\":{\"AssetName\":\"\",\"AssetModel\":\"\", \"AssetType\":\"\"}}','[{\"FieldName\":\"nguoi\",\"MappingName\":\"CurrentUser\"},{\"FieldName\":\"nyc\",\"MappingName\":\"CurrentUser\"},{\"FieldName\":\"ghi_chu\",\"MappingName\":\"Note\"},{\"FieldName\":\"danh_sach_tai_san\",\"MappingName\":\"AssetList\",\"Detail\":[{\"FieldName\":\"ma_tai_san\",\"MappingName\":\"AssetName\"}]}]',83),(2,'Trả thiết bị','{\"CurrentDate\":\"\",\"CurrentUser\":\"\",\"Note\":\"\",\"Assets\":{\"AssetName\":\"\",\"AssetModel\":\"\", \"AssetType\":\"\"}}','[{\"FieldName\":\"nguoi_yeu_cau\",\"MappingName\":\"CurrentUser\"},{\"FieldName\":\"nyc\",\"MappingName\":\"CurrentUser\"},{\"FieldName\":\"ghi_chu\",\"MappingName\":\"Note\"},{\"FieldName\":\"danh_sach_tai_san\",\"MappingName\":\"AssetList\",\"Detail\":[{\"FieldName\":\"ma_tai_san\",\"MappingName\":\"AssetName\"},{\"FieldName\":\"loai_tai_san\",\"MappingName\":\"AssetTypeID\"},{\"FieldName\":\"model_tai_san\",\"MappingName\":\"AssetModelID\"}]}]',86),(3,'Hư hỏng thiết bị','{\"CurrentDate\":\"\",\"CurrentUser\":\"\",\"Note\":\"\",\"Assets\":{\"AssetName\":\"\",\"AssetModel\":\"\", \"AssetType\":\"\"}}','[{\"FieldName\":\"nguoi_yeu_cau\",\"MappingName\":\"CurrentUser\"},{\"FieldName\":\"nyc\",\"MappingName\":\"CurrentUser\"},{\"FieldName\":\"ghi_chu\",\"MappingName\":\"Note\"},{\"FieldName\":\"danh_sach_tai_san\",\"MappingName\":\"AssetList\",\"Detail\":[{\"FieldName\":\"ma_tai_san\",\"MappingName\":\"AssetName\"},{\"FieldName\":\"loai_tai_san\",\"MappingName\":\"AssetType\"},{\"FieldName\":\"model_tai_san\",\"MappingName\":\"AssetModel\"}]}]',87),(4,'Thay thế thiết bị','{\"CurrentDate\":\"\",\"CurrentUser\":\"\",\"Note\":\"\",\"Assets\":{\"AssetName\":\"\",\"AssetModel\":\"\", \"AssetType\":\"\"}}','[{\"FieldName\":\"nguoi_yeu_cau\",\"MappingName\":\"CurrentUser\"},{\"FieldName\":\"nyc\",\"MappingName\":\"CurrentUser\"},{\"FieldName\":\"ghi_chu\",\"MappingName\":\"Note\"},{\"FieldName\":\"danh_sach_tai_san\",\"MappingName\":\"AssetList\",\"Detail\":[{\"FieldName\":\"ma_tai_san\",\"MappingName\":\"AssetName\"},{\"FieldName\":\"model_tai_san\",\"MappingName\":\"AssetModel\"}]}]',88),(5,'Yêu cầu tài sản','{\"CurrentDate\":\"\",\"CurrentUser\":\"\",\"Note\":\"\",\"Assets\":{\"AssetName\":\"\",\"AssetModel\":\"\", \"AssetType\":\"\"}}','[{\"FieldName\":\"nguoi_yeu_cau\",\"MappingName\":\"CurrentUser\"},{\"FieldName\":\"nyc\",\"MappingName\":\"CurrentUser\"},{\"FieldName\":\"ghi_chu\",\"MappingName\":\"Note\"},{\"FieldName\":\"danh_sach_yeu_cau\",\"MappingName\":\"AssetList\",\"Detail\":[{\"FieldName\":\"model_tai_san\",\"MappingName\":\"AssetModel\"},{\"FieldName\":\"loai_tai_san\",\"MappingName\":\"AssetType\"}]},{\"FieldName\":\"danh_sach_tra\",\"MappingName\":\"ReturnAssetList\",\"Detail\":[{\"FieldName\":\"ma_tai_san1\",\"MappingName\":\"AssetName\"},{\"FieldName\":\"model_tai_san1\",\"MappingName\":\"AssetModel\"},{\"FieldName\":\"loai_tai_san1\",\"MappingName\":\"AssetType\"}]}]',89);
/*!40000 ALTER TABLE `am_request_feature_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_field_definition`
--

DROP TABLE IF EXISTS `am_request_field_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_field_definition` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `DisplayName` varchar(50) NOT NULL DEFAULT '',
  `MappingName` varchar(50) NOT NULL DEFAULT '',
  `RequestDataTypeID` int(10) unsigned NOT NULL DEFAULT '1',
  `RequestDefinitionID` int(10) unsigned NOT NULL DEFAULT '0',
  `ParentID` int(10) unsigned DEFAULT '0',
  `Config` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=609 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_field_definition`
--

LOCK TABLES `am_request_field_definition` WRITE;
/*!40000 ALTER TABLE `am_request_field_definition` DISABLE KEYS */;
INSERT INTO `am_request_field_definition` VALUES (498,'nyc','Thông tin người yêu cầu','Field0',8,80,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(499,'du_kien','Thời gian hoàn thành dự kiến','Field1',10,80,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(500,'ghi_chu','Ghi chú của người yêu cầu','Field2',9,80,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\",\"height\":\"170px\"},\"submitedStyle\":{\"display\":null}}'),(501,'nguoi_yeu_cau','Người yêu cầu','Field3',6,80,NULL,'{\"style\":{\"width\":\"32\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"none\"}}'),(502,'chi_tiet_yeu_cau','Chi tiết yêu cầu','Field4',5,80,NULL,'{\"style\":{\"width\":\"100\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"},\"row\":4,\"height\":193}'),(503,'model','Model','Field0',4,80,502,'{\"style\":{\"width\":\"35\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(504,'so_luong','Số lượng','Field1',2,80,502,'{\"style\":{\"width\":\"100px\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(505,'ghi_chu','Ghi chú','Field2',1,80,502,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(506,'nyc','Thông tin người yêu cầu','Field0',8,81,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(507,'du_kien','Thời gian hoàn thành dự kiến','Field1',10,81,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(508,'ghi_chu','Ghi chú của người yêu cầu','Field2',9,81,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\",\"height\":\"170px\"},\"submitedStyle\":{\"display\":null}}'),(509,'nguoi_yeu_cau','Người yêu cầu','Field3',6,81,NULL,'{\"style\":{\"width\":\"32\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"none\"}}'),(510,'chi_tiet_yeu_cau','Chi tiết yêu cầu','Field4',5,81,NULL,'{\"style\":{\"width\":\"100\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"},\"row\":4,\"height\":1}'),(511,'model','Model','Field0',4,81,510,'{\"style\":{\"width\":\"40\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(512,'ma','Mã','Field1',3,81,510,'{\"style\":{\"width\":\"150px\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(513,'ghi_chu','Ghi chú','Field2',1,81,510,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(514,'nyc','Thông tin người yêu cầu','Field0',8,82,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(515,'du_kien','Thời gian hoàn thành dự kiến','Field1',10,82,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(516,'ghi_chu','Ghi chú của người yêu cầu','Field2',9,82,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\",\"height\":\"170px\"},\"submitedStyle\":{\"display\":null}}'),(517,'nguoi_yeu_cau','Người yêu cầu','Field3',6,82,NULL,'{\"style\":{\"width\":\"32\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"none\"}}'),(518,'chi_tiet_yeu_cau','Chi tiết yêu cầu','Field4',5,82,NULL,'{\"style\":{\"width\":\"100\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"},\"row\":4,\"height\":1}'),(519,'model','Model','Field0',4,82,518,'{\"style\":{\"width\":\"40\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(520,'ma','Mã','Field1',3,82,518,'{\"style\":{\"width\":\"150px\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(521,'ghi_chu','Ghi chú','Field2',1,82,518,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(522,'nyc','Thông tin người yêu cầu','Field0',8,83,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(523,'du_kien','Thời gian hoàn thành dự kiến','Field1',10,83,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(524,'ghi_chu','Ghi chú của người yêu cầu','Field2',9,83,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\",\"height\":\"170px\"},\"submitedStyle\":{\"display\":null}}'),(525,'danh_sach_tai_san','Danh sách tài sản','Field3',5,83,NULL,'{\"style\":{\"width\":\"100\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"},\"row\":4,\"height\":153}'),(526,'ma_tai_san','Mã tài sản','Field0',1,83,525,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(527,'ly_do','Lý do','Field1',1,83,525,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(528,'tien_den_bu','Tiền đền bù','Field2',1,83,525,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(530,'nguoi','Người yêu cầu','Field4',6,83,NULL,'{\"style\":{\"width\":\"32\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":[]}'),(531,'nyc','Thông tin người yêu cầu','Field0',8,84,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(532,'du_kien','Thời gian hoàn thành dự kiến','Field1',10,84,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(533,'ghi_chu','Ghi chú của người yêu cầu','Field2',9,84,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\",\"height\":\"170px\"},\"submitedStyle\":{\"display\":null}}'),(534,'nguoi_yeu_cau','Người yêu cầu','Field3',6,84,NULL,'{\"style\":{\"width\":\"32\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(535,'danh_sach_tai_san','Danh sách tài sản','Field4',5,84,NULL,'{\"style\":{\"width\":\"100\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"},\"row\":4,\"height\":1}'),(536,'ma_tai_san','Mã tài sản','Field0',1,84,535,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(537,'loai_tai_san','Loại tài sản','Field1',1,84,535,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(538,'model_tai_san','Model tài sản','Field2',1,84,535,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(539,'ly_do_tra','Lý do trả','Field3',1,84,535,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(540,'nyc','Thông tin người yêu cầu','Field0',8,85,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(541,'du_kien','Thời gian hoàn thành dự kiến','Field1',10,85,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(542,'ghi_chu','Ghi chú của người yêu cầu','Field2',9,85,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\",\"height\":\"170px\"},\"submitedStyle\":{\"display\":null}}'),(543,'nguoi_yeu_cau','Người yêu cầu','Field3',6,85,NULL,'{\"style\":{\"width\":\"32\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(544,'danh_sach_tai_san','Danh sách tài sản','Field4',5,85,NULL,'{\"style\":{\"width\":\"100\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"},\"row\":4,\"height\":160}'),(545,'ma_tai_san','Mã tài sản','Field0',1,85,544,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(546,'loai_tai_san','Loại tài sản','Field1',1,85,544,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(547,'model_tai_san','Model tài sản','Field2',1,85,544,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(548,'ly_do_tra','Lý do trả','Field3',1,85,544,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(549,'nyc','Thông tin người yêu cầu','Field0',8,86,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(550,'du_kien','Thời gian hoàn thành dự kiến','Field1',10,86,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(551,'ghi_chu','Ghi chú của người yêu cầu','Field2',9,86,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\",\"height\":\"170px\"},\"submitedStyle\":{\"display\":null}}'),(552,'nguoi_yeu_cau','Người yêu cầu','Field3',6,86,NULL,'{\"style\":{\"width\":\"32\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(553,'danh_sach_tai_san','Danh sách tài sản','Field4',5,86,NULL,'{\"style\":{\"width\":\"100\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"},\"row\":\"5\",\"height\":191}'),(554,'ma_tai_san','Mã tài sản','Field0',1,86,553,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(555,'loai_tai_san','Loại tài sản','Field1',7,86,553,'{\"style\":{\"width\":null,\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":[]}'),(556,'model_tai_san','Model tài sản','Field2',4,86,553,'{\"style\":{\"width\":null,\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":[]}'),(557,'ly_do_tra','Lý do trả','Field3',1,86,553,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(558,'nyc','Thông tin người yêu cầu','Field0',8,87,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(559,'du_kien','Thời gian hoàn thành dự kiến','Field1',10,87,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(560,'ghi_chu','Ghi chú của người yêu cầu','Field2',9,87,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\",\"height\":\"170px\"},\"submitedStyle\":{\"display\":null}}'),(561,'nguoi_yeu_cau','Người yêu cầu','Field3',6,87,NULL,'{\"style\":{\"width\":\"32\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(562,'danh_sach_tai_san','Danh sách tài sản','Field4',5,87,NULL,'{\"style\":{\"width\":\"100\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"},\"row\":\"5\",\"height\":1}'),(563,'ma_tai_san','Mã tài sản','Field0',1,87,562,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(564,'loai_tai_san','Loại tài sản','Field1',1,87,562,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(565,'model_tai_san','Model tài sản','Field2',1,87,562,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(566,'ly_do_hu_hong','Lý do hư hỏng','Field3',1,87,562,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(567,'bao_hanh','Bảo hành','Field4',1,87,562,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(568,'tien_den_bu','Tiền đền bù','Field5',1,87,562,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(569,'nyc','Thông tin người yêu cầu','Field0',8,88,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(570,'du_kien','Thời gian hoàn thành dự kiến','Field1',10,88,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(571,'ghi_chu','Ghi chú của người yêu cầu','Field2',9,88,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\",\"height\":\"170px\"},\"submitedStyle\":{\"display\":null}}'),(572,'nguoi_yeu_cau','Người yêu cầu','Field3',6,88,NULL,'{\"style\":{\"width\":\"32\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(573,'danh_sach_tai_san','Danh sách tài sản trả','Field4',5,88,NULL,'{\"style\":{\"width\":\"100\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"},\"row\":4,\"height\":1}'),(574,'ma_tai_san','Mã tài sản','Field0',1,88,573,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(575,'model_tai_san','Model tài sản','Field1',1,88,573,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(576,'model_yeu_cau','Model yêu cầu','Field2',4,88,573,'{\"style\":{\"width\":null,\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(577,'danh_sach_tai_san_thay_the','Danh sách tài sản thay thế','Field5',5,88,NULL,'{\"style\":{\"width\":\"100\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"},\"row\":4,\"height\":1}'),(578,'ma_tai_san1','Mã tài sản','Field0',1,88,577,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(579,'loai_tai_san','Loại tài sản','Field1',7,88,577,'{\"style\":{\"width\":null,\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(580,'model_tai_san1','Model tài sản','Field2',4,88,577,'{\"style\":{\"width\":null,\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(581,'nyc','Thông tin người yêu cầu','Field0',8,89,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(582,'du_kien','Thời gian hoàn thành dự kiến','Field1',10,89,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(583,'ghi_chu','Ghi chú của người yêu cầu','Field2',9,89,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\",\"height\":\"170px\"},\"submitedStyle\":{\"display\":null}}'),(584,'nguoi_yeu_cau','Người yêu cầu','Field3',6,89,NULL,'{\"style\":{\"width\":\"32\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(585,'danh_sach_yeu_cau','Danh sách yêu cầu','Field4',5,89,NULL,'{\"style\":{\"width\":\"100\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"},\"row\":4,\"height\":1}'),(586,'loai_tai_san','Loại tài sản','Field0',1,89,585,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(587,'model_tai_san','Model tài sản','Field1',1,89,585,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(588,'ma_tai_san','Mã tài sản','Field2',1,89,585,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(589,'danh_sach_tra','Danh sách trả','Field5',5,89,NULL,'{\"style\":{\"width\":\"100\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"},\"row\":4,\"height\":1}'),(590,'loai_tai_san1','Loại tài sản','Field0',1,89,589,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(591,'model_tai_san1','Model tài sản','Field1',1,89,589,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(592,'ma_tai_san1','Mã tài sản','Field2',1,89,589,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(593,'nyc','Thông tin người yêu cầu','Field0',8,90,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(594,'du_kien','Thời gian hoàn thành dự kiến','Field1',10,90,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(595,'ghi_chu','Ghi chú của người yêu cầu','Field2',9,90,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\",\"height\":\"170px\"},\"submitedStyle\":{\"display\":null}}'),(596,'nguoi_yeu_cau','Người yêu cầu','Field3',6,90,NULL,'{\"style\":{\"width\":\"32\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"none\"}}'),(597,'chi_tiet_yeu_cau','Chi tiết yêu cầu','Field4',5,90,NULL,'{\"style\":{\"width\":\"100\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"},\"row\":4,\"height\":1}'),(598,'model','Model','Field0',4,90,597,'{\"style\":{\"width\":\"35\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(599,'so_luong','Số lượng','Field1',2,90,597,'{\"style\":{\"width\":\"100px\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(600,'ghi_chu1','Ghi chú','Field2',1,90,597,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}'),(601,'nyc','Thông tin người yêu cầu','Field0',8,91,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(602,'du_kien','Thời gian hoàn thành dự kiến','Field1',10,91,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\"},\"submitedStyle\":{\"display\":null}}'),(603,'ghi_chu','Ghi chú của người yêu cầu','Field2',9,91,NULL,'{\"style\":{\"width\":\"32\",\"display\":\"none\",\"height\":\"170px\"},\"submitedStyle\":{\"display\":null}}'),(604,'nguoi_yeu_cau','Người yêu cầu','Field3',6,91,NULL,'{\"style\":{\"width\":\"32\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"none\"}}'),(605,'chi_tiet_yeu_cau','Chi tiết yêu cầu','Field4',5,91,NULL,'{\"style\":{\"width\":\"100\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"},\"row\":4,\"height\":193}'),(606,'model','Model','Field0',4,91,605,'{\"style\":{\"width\":\"35\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(607,'so_luong','Số lượng','Field1',2,91,605,'{\"style\":{\"width\":\"100px\",\"float\":\"\",\"display\":\"Hiển thị\"},\"submitedStyle\":{\"display\":\"Hiển thị\"}}'),(608,'ghi_chu1','Ghi chú','Field2',1,91,605,'{\"style\":{\"width\":null},\"submitedStyle\":[],\"isCol\":1}');
/*!40000 ALTER TABLE `am_request_field_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_fields`
--

DROP TABLE IF EXISTS `am_request_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_fields` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RequestID` int(10) NOT NULL,
  `Field0` varchar(1000) DEFAULT '',
  `Field1` varchar(1000) DEFAULT '',
  `Field2` varchar(1000) DEFAULT '',
  `Field3` varchar(1000) DEFAULT '',
  `Field4` varchar(1000) DEFAULT '',
  `Field5` varchar(1000) DEFAULT '',
  `Field6` varchar(1000) DEFAULT '',
  `Field7` varchar(1000) DEFAULT '',
  `Field8` varchar(1000) DEFAULT '',
  `Field9` varchar(1000) DEFAULT '',
  `Field10` varchar(1000) DEFAULT '',
  `Field11` varchar(1000) DEFAULT '',
  `Field12` varchar(1000) DEFAULT '',
  `Field13` varchar(1000) DEFAULT '',
  `Field14` varchar(1000) DEFAULT '',
  PRIMARY KEY (`ID`,`RequestID`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_fields`
--

LOCK TABLES `am_request_fields` WRITE;
/*!40000 ALTER TABLE `am_request_fields` DISABLE KEYS */;
INSERT INTO `am_request_fields` VALUES (45,45,'6','Invalid Date','','tiennv6','36','','','','','','','','','',''),(46,46,'6','Invalid Date',NULL,'tiennv6','37','','','','','','','','','',''),(47,47,'6','Invalid Date',NULL,'tiennv6','38','','','','','','','','','',''),(48,48,'6','Invalid Date',NULL,'tiennv6','39','','','','','','','','','',''),(49,49,'6','Invalid Date',NULL,'tiennv6','40','','','','','','','','','',''),(50,50,'6','Invalid Date',NULL,'tiennv6','41','','','','','','','','','',''),(51,51,'6','Invalid Date',NULL,'tiennv6','42','','','','','','','','','',''),(52,52,'6','Invalid Date',NULL,'tiennv6','43','','','','','','','','','',''),(53,53,'6','Invalid Date',NULL,'tiennv6','44','','','','','','','','','',''),(54,54,'6','Invalid Date',NULL,'tiennv6','45','','','','','','','','','',''),(55,55,'6','Invalid Date',NULL,'tiennv6','46','','','','','','','','','',''),(56,56,'7','Invalid Date','','doanhvtm','47','','','','','','','','','',''),(57,57,'7','Invalid Date',NULL,'doanhvtm','48','','','','','','','','','',''),(58,58,'6',NULL,NULL,'tiennv6','49',NULL,'','','','','','','','',''),(59,59,'6',NULL,NULL,'tiennv6','50',NULL,'','','','','','','','',''),(60,60,'6',NULL,NULL,'tiennv6','51',NULL,'','','','','','','','',''),(61,61,'6','Invalid Date','','tiennv6','52','','','','','','','','','',''),(62,62,'6','Invalid Date','sd','tiennv6','53','','','','','','','','','',''),(63,63,'6',NULL,NULL,'tiennv6','54',NULL,'','','','','','','','',''),(64,64,'6',NULL,NULL,'tiennv6','55',NULL,'','','','','','','','',''),(65,65,'6',NULL,NULL,'tiennv6','56',NULL,'','','','','','','','','');
/*!40000 ALTER TABLE `am_request_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_notification`
--

DROP TABLE IF EXISTS `am_request_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_notification` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Domain` varchar(45) NOT NULL,
  `RequestID` int(11) NOT NULL,
  `Title` varchar(200) NOT NULL,
  `Content` text NOT NULL,
  `IsSeen` tinyint(1) NOT NULL,
  `IsRead` tinyint(1) NOT NULL,
  `TypeID` int(11) NOT NULL,
  `ShortContent` varchar(200) DEFAULT NULL,
  `Time` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_notification`
--

LOCK TABLES `am_request_notification` WRITE;
/*!40000 ALTER TABLE `am_request_notification` DISABLE KEYS */;
INSERT INTO `am_request_notification` VALUES (53,'tiennv6',0,'[ITAM3] Đơn hàng đã được tạo','45',1,0,1,NULL,'2018-05-18 19:20:24'),(54,'thanhnl',0,'[ITAM3] Yêu cầu duyệt từTiến. Nguyễn Văn (6)(6) ','45',1,0,1,NULL,'2018-05-18 19:20:26'),(55,'tiennv6',0,'[ITAM3] Đơn hàng đã được phe duyệt bởi Nguyễn Lê Thành&nbsp;','45',1,0,1,NULL,'2018-05-18 19:20:34'),(56,'doanhvtm',0,'[ITAM3] Đơn hàng đã được tạo','56',0,0,1,NULL,'2018-05-21 09:40:20'),(57,'thanhnl',0,'[ITAM3] Yêu cầu duyệt từVõ Thị Mỹ Doanh ','56',1,0,1,NULL,'2018-05-21 09:40:20'),(58,'doanhvtm',0,'[ITAM3] Đơn hàng đã được phe duyệt bởi Nguyễn Lê Thành&nbsp;','56',0,0,1,NULL,'2018-05-21 09:41:52');
/*!40000 ALTER TABLE `am_request_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_owner`
--

DROP TABLE IF EXISTS `am_request_owner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_owner` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Domain` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `Name` varchar(110) NOT NULL,
  `JobTitle` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `DepartmentName` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `Seat` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `Location` varchar(45) DEFAULT NULL,
  `IsCurrent` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_owner`
--

LOCK TABLES `am_request_owner` WRITE;
/*!40000 ALTER TABLE `am_request_owner` DISABLE KEYS */;
INSERT INTO `am_request_owner` VALUES (6,'tiennv6','Tiến. Nguyễn Văn (6)','Associate Software Engineer','Information Technology','FLEMINGTON - F14-191','FLEMINGTON',1),(7,'doanhvtm','Doanh. Võ Thị Mỹ','Senior IT Support Engineer','Information Technology','','FLEMINGTON',1);
/*!40000 ALTER TABLE `am_request_owner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_reasons`
--

DROP TABLE IF EXISTS `am_request_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_reasons` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `RequestTypeID` int(10) unsigned NOT NULL DEFAULT '0',
  `Description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_reasons`
--

LOCK TABLES `am_request_reasons` WRITE;
/*!40000 ALTER TABLE `am_request_reasons` DISABLE KEYS */;
INSERT INTO `am_request_reasons` VALUES (1,'Cấp Nhân Viên Mới',1,'Cấp Nhân Viên Mới'),(2,'Thay Thế Tài Sản',1,'Thay Thế Tài Sản'),(3,'Báo mất thiết bị',2,''),(4,'Trả thiết bị',2,'Trả thiết bị'),(5,'Hư hỏng thiết bị',2,'Hư hỏng thiết bị'),(6,'Thay thế thiết bị',2,'Thay thế thiết bị');
/*!40000 ALTER TABLE `am_request_reasons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_search`
--

DROP TABLE IF EXISTS `am_request_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_search` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DisplayName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_search`
--

LOCK TABLES `am_request_search` WRITE;
/*!40000 ALTER TABLE `am_request_search` DISABLE KEYS */;
INSERT INTO `am_request_search` VALUES (1,'Tìm nâng cao'),(5,'Test'),(6,'Modul đang phát triển: Tìm biên bản chờ xuất kho');
/*!40000 ALTER TABLE `am_request_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_search_column`
--

DROP TABLE IF EXISTS `am_request_search_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_search_column` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `DisplayName` varchar(100) DEFAULT NULL,
  `Source` varchar(500) DEFAULT NULL,
  `SearchID` int(11) DEFAULT NULL,
  `TypeID` int(11) DEFAULT NULL,
  `Style` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_search_column`
--

LOCK TABLES `am_request_search_column` WRITE;
/*!40000 ALTER TABLE `am_request_search_column` DISABLE KEYS */;
INSERT INTO `am_request_search_column` VALUES (1,'ID','Mã yêu cầu','ID',1,1,'{}'),(2,'nguoi_yeu_cau','Người yêu cầu',NULL,1,5,'{}'),(3,'cac_tai_san','Các tài sản','{\"80\":\"model\",\"81\":\"model\",\"82\":\"model\"}',1,3,'{}'),(4,'state','Tình trạng',NULL,1,6,'{}'),(5,'last_update','Lần cập nhật cuối',NULL,1,7,'{}'),(6,'dead_line','Hạn chờ','',1,8,'{}'),(66,'ID','Mã yêu cầu','\"ID\"',5,1,'{\"width\":\"200px\"}'),(67,'model','Model','[{\"wfID\":\"80\",\"name\":\"model\",\"parent\":\"chi_tiet_yeu_cau\"}]',5,2,'{\"width\":\"\"}');
/*!40000 ALTER TABLE `am_request_search_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_search_condition`
--

DROP TABLE IF EXISTS `am_request_search_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_search_condition` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `DisplayName` varchar(100) DEFAULT NULL,
  `ColumnName` varchar(50) DEFAULT NULL,
  `SearchID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_search_condition`
--

LOCK TABLES `am_request_search_condition` WRITE;
/*!40000 ALTER TABLE `am_request_search_condition` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_request_search_condition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_search_filter`
--

DROP TABLE IF EXISTS `am_request_search_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_search_filter` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `DisplayName` varchar(100) DEFAULT NULL,
  `ColumnName` varchar(50) DEFAULT NULL,
  `SearchID` int(11) DEFAULT NULL,
  `Style` varchar(500) DEFAULT '{}',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_search_filter`
--

LOCK TABLES `am_request_search_filter` WRITE;
/*!40000 ALTER TABLE `am_request_search_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_request_search_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_status`
--

DROP TABLE IF EXISTS `am_request_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_status` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `DisplayName` varchar(50) NOT NULL DEFAULT '',
  `Description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_status`
--

LOCK TABLES `am_request_status` WRITE;
/*!40000 ALTER TABLE `am_request_status` DISABLE KEYS */;
INSERT INTO `am_request_status` VALUES (1,'open','Đang mở',''),(2,'done','Đã hoàn thành',''),(3,'cancel','Đã bị đóng','');
/*!40000 ALTER TABLE `am_request_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_step_actions`
--

DROP TABLE IF EXISTS `am_request_step_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_step_actions` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT '',
  `RequestStepDefinitionID` int(10) unsigned NOT NULL DEFAULT '0',
  `EventID` tinyint(1) NOT NULL DEFAULT '0',
  `Config` text NOT NULL,
  `IsRollback` tinyint(1) NOT NULL DEFAULT '0',
  `IsSync` tinyint(1) NOT NULL DEFAULT '0',
  `ActionTypeID` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_step_actions`
--

LOCK TABLES `am_request_step_actions` WRITE;
/*!40000 ALTER TABLE `am_request_step_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_request_step_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_step_definitions`
--

DROP TABLE IF EXISTS `am_request_step_definitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_step_definitions` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `StepIndex` smallint(5) unsigned NOT NULL DEFAULT '0',
  `Name` varchar(50) NOT NULL DEFAULT '',
  `DisplayName` varchar(50) NOT NULL DEFAULT '',
  `RequestDefinitionID` int(10) unsigned NOT NULL DEFAULT '0',
  `Role` varchar(50) NOT NULL,
  `MaxIntendDuration` int(11) NOT NULL,
  `MinIntendDuration` int(11) NOT NULL,
  `CanRequestInfo` tinyint(1) DEFAULT '0',
  `BackStep` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=416 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_step_definitions`
--

LOCK TABLES `am_request_step_definitions` WRITE;
/*!40000 ALTER TABLE `am_request_step_definitions` DISABLE KEYS */;
INSERT INTO `am_request_step_definitions` VALUES (369,0,'nhap_thong_tin','Nhập thông tin',80,'null',24,3,NULL,NULL),(370,1,'deapth_head_duyet','Deapth Head duyệt',80,'\"7:nguoi_yeu_cau\"',24,3,0,NULL),(371,2,'it_manager_duyet','IT Manager duyệt',80,'8',24,3,1,NULL),(372,0,'nhap_thong_tin','Nhập thông tin',81,'null',24,3,NULL,NULL),(373,1,'deapth_head_duyet','Deapth Head duyệt',81,'\"7:nguoi_yeu_cau\"',24,3,0,NULL),(374,2,'it_manager_duyet','IT Manager duyệt',81,'8',24,3,0,NULL),(375,3,'nhan_ban_quyen','Giao bản quyền',81,'3',24,3,0,NULL),(376,4,'nhan_ban_quyen1','Nhận bản quyền',81,'\"2:nguoi_yeu_cau\"',24,3,0,NULL),(377,0,'nhap_thong_tin','Nhập thông tin',82,'null',24,3,NULL,NULL),(378,1,'deapth_head_duyet','Deapth Head duyệt',82,'\"7:nguoi_yeu_cau\"',24,3,0,NULL),(379,2,'it_manager_duyet','IT Manager duyệt',82,'8',24,3,0,NULL),(380,3,'nhan_ban_quyen','Chờ xuất kho',82,'4',24,3,0,NULL),(381,4,'nhan_ban_quyen1','Cài đặt - chuyển giao',82,'3',24,3,0,'nhan_ban_quyen'),(382,5,'nhan_tai_san','Nhận tài sản',82,'\"2:nguoi_yeu_cau\"',24,3,0,NULL),(383,0,'nhap_thong_tin','Khởi tạo',83,'null',24,3,NULL,NULL),(384,1,'xac_nhan','Xác nhận',83,'4',24,3,0,NULL),(385,2,'duyet_chi_phi','Duyệt chi phí',83,'6',24,3,0,NULL),(386,3,'xac_nhan1','Xác nhận',83,'\"2:nguoi\"',24,3,0,NULL),(387,0,'nhap_thong_tin','Nhập thông tin',84,'null',24,3,NULL,NULL),(388,1,'kiem_tra_tinh_trang','Kiểm tra tình trạng',84,'3',24,3,0,NULL),(389,2,'nhap_kho','User xác nhận',84,'\"2:nguoi_yeu_cau\"',24,3,0,NULL),(390,3,'nhap_kho1','Nhập kho',84,'4',24,3,0,NULL),(391,0,'nhap_thong_tin','Nhập thông tin',85,'null',24,3,NULL,NULL),(392,1,'kiem_tra_tinh_trang','Kiểm tra tình trạng',85,'3',24,3,0,NULL),(393,2,'nhap_kho','User xác nhận',85,'\"2:nguoi_yeu_cau\"',24,3,0,NULL),(394,3,'nhap_kho1','Nhập kho',85,'4',24,3,0,NULL),(395,0,'nhap_thong_tin','Nhập thông tin',86,'null',24,3,NULL,NULL),(396,1,'kiem_tra_tinh_trang','Kiểm tra tình trạng',86,'3',24,3,0,NULL),(397,2,'nhap_kho','User xác nhận',86,'\"2:nguoi_yeu_cau\"',24,3,0,NULL),(398,3,'nhap_kho1','Nhập kho',86,'4',24,3,0,NULL),(399,0,'nhap_thong_tin','Nhập thông tin',87,'null',24,3,NULL,NULL),(400,1,'kiem_tra_tinh_trang','Kiểm tra tình trạng',87,'3',24,3,0,NULL),(401,2,'tinh_trang_hu_hong','Tình trạng hư hỏng',87,'4',24,3,0,NULL),(402,3,'xac_nhan_den_bu','Xác nhận đền bù',87,'6',24,3,0,NULL),(403,4,'xac_nhan','Xác nhận',87,'\"2:nguoi_yeu_cau\"',24,3,0,NULL),(404,0,'nhap_thong_tin','Nhập thông tin',88,'null',24,3,NULL,NULL),(405,1,'kiem_tra_tinh_trang','Phê duyệt',88,'3',24,3,0,NULL),(406,2,'cap_phat','Cấp phát',88,'4',24,3,0,NULL),(407,3,'ban_giao','Bàn giao',88,'3',24,3,0,NULL),(408,4,'xac_nhan','Xác nhận',88,'\"2:nguoi_yeu_cau\"',24,3,0,NULL),(409,0,'nhap_thong_tin','Nhập thông tin',89,'null',24,3,NULL,NULL),(410,1,'xac_nhan_don_hang','Xác nhận đơn hàng',89,'3',24,3,0,NULL),(411,2,'xuat_hang','Xuất hàng',89,'4',24,3,0,NULL),(412,3,'giao_hang','Giao hàng',89,'3',24,3,0,NULL),(413,4,'hoan_tat','Hoàn tất',89,'\"2:nguoi_yeu_cau\"',24,3,0,NULL),(414,0,'nhap_thong_tin','Nhập thông tin',90,'null',24,3,NULL,NULL),(415,0,'nhap_thong_tin','Nhập thông tin',91,'null',24,3,NULL,NULL);
/*!40000 ALTER TABLE `am_request_step_definitions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_step_permission`
--

DROP TABLE IF EXISTS `am_request_step_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_step_permission` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `StepDefinitionID` int(10) unsigned NOT NULL DEFAULT '0',
  `FieldDefinitionID` int(10) unsigned NOT NULL DEFAULT '0',
  `Permission` varchar(45) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_step_permission`
--

LOCK TABLES `am_request_step_permission` WRITE;
/*!40000 ALTER TABLE `am_request_step_permission` DISABLE KEYS */;
INSERT INTO `am_request_step_permission` VALUES (2,380,520,'');
/*!40000 ALTER TABLE `am_request_step_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_steps`
--

DROP TABLE IF EXISTS `am_request_steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_steps` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DefinitionID` int(10) unsigned NOT NULL DEFAULT '0',
  `ActionDate` datetime DEFAULT NULL,
  `StartDate` datetime NOT NULL,
  `RequestID` int(10) unsigned NOT NULL DEFAULT '0',
  `Owner` varchar(50) NOT NULL,
  `IsApproved` tinyint(1) DEFAULT NULL,
  `MaxIntendTime` datetime NOT NULL,
  `MinIntendTime` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=286 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_steps`
--

LOCK TABLES `am_request_steps` WRITE;
/*!40000 ALTER TABLE `am_request_steps` DISABLE KEYS */;
INSERT INTO `am_request_steps` VALUES (183,369,'2018-05-18 19:20:23','0000-00-00 00:00:00',45,'tiennv6',1,'2018-05-18 19:20:23','2018-05-18 19:20:23'),(184,370,'2018-05-18 19:20:33','2018-05-18 19:20:26',45,'thanhnl',1,'2018-05-18 19:20:23','2018-05-19 22:20:23'),(185,371,'2018-05-18 19:28:50','2018-05-18 19:20:34',45,'tult',1,'2018-05-18 19:20:23','2018-05-21 01:20:23'),(186,377,'2018-05-18 19:20:23','2018-05-18 19:20:42',46,'tiennv6',1,'2018-05-18 19:20:42','2018-05-18 19:20:42'),(187,378,'2018-05-18 19:20:33','2018-05-18 19:20:26',46,'thanhnl',1,'2018-05-18 19:20:42','2018-05-18 19:20:42'),(188,379,'2018-05-18 19:20:42','2018-05-18 19:20:34',46,'tult',1,'2018-05-18 19:20:42','2018-05-18 19:20:42'),(189,380,NULL,'0000-00-00 00:00:00',46,'khoalt',NULL,'2018-05-18 19:20:42','2018-05-18 19:20:42'),(190,381,NULL,'0000-00-00 00:00:00',46,'trungvd',NULL,'2018-05-18 19:20:42','2018-05-19 22:20:42'),(191,382,NULL,'0000-00-00 00:00:00',46,'tiennv6',NULL,'2018-05-18 19:20:42','2018-05-21 01:20:42'),(192,372,'2018-05-18 19:20:23','2018-05-18 19:20:42',47,'tiennv6',1,'2018-05-18 19:20:42','2018-05-18 19:20:42'),(193,373,'2018-05-18 19:20:33','2018-05-18 19:20:26',47,'thanhnl',1,'2018-05-18 19:20:42','2018-05-18 19:20:42'),(194,374,'2018-05-18 19:20:42','2018-05-18 19:20:34',47,'tult',1,'2018-05-18 19:20:42','2018-05-18 19:20:42'),(195,375,NULL,'0000-00-00 00:00:00',47,'trungvd',NULL,'2018-05-18 19:20:42','2018-05-18 19:20:42'),(196,376,NULL,'0000-00-00 00:00:00',47,'tiennv6',NULL,'2018-05-18 19:20:42','2018-05-19 22:20:42'),(197,377,'2018-05-18 19:20:23','2018-05-18 19:24:17',48,'tiennv6',1,'2018-05-18 19:24:17','2018-05-18 19:24:17'),(198,378,'2018-05-18 19:20:33','2018-05-18 19:20:26',48,'thanhnl',1,'2018-05-18 19:24:17','2018-05-18 19:24:17'),(199,379,'2018-05-18 19:24:17','2018-05-18 19:20:34',48,'tult',1,'2018-05-18 19:24:17','2018-05-18 19:24:17'),(200,380,NULL,'0000-00-00 00:00:00',48,'khoalt',NULL,'2018-05-18 19:24:17','2018-05-18 19:24:17'),(201,381,NULL,'0000-00-00 00:00:00',48,'trungvd',NULL,'2018-05-18 19:24:17','2018-05-19 22:24:17'),(202,382,NULL,'0000-00-00 00:00:00',48,'tiennv6',NULL,'2018-05-18 19:24:17','2018-05-21 01:24:17'),(203,372,'2018-05-18 19:20:23','2018-05-18 19:24:17',49,'tiennv6',1,'2018-05-18 19:24:17','2018-05-18 19:24:17'),(204,373,'2018-05-18 19:20:33','2018-05-18 19:20:26',49,'thanhnl',1,'2018-05-18 19:24:17','2018-05-18 19:24:17'),(205,374,'2018-05-18 19:24:17','2018-05-18 19:20:34',49,'tult',1,'2018-05-18 19:24:17','2018-05-18 19:24:17'),(206,375,NULL,'0000-00-00 00:00:00',49,'trungvd',NULL,'2018-05-18 19:24:17','2018-05-18 19:24:17'),(207,376,NULL,'0000-00-00 00:00:00',49,'tiennv6',NULL,'2018-05-18 19:24:17','2018-05-19 22:24:17'),(208,377,'2018-05-18 19:20:23','2018-05-18 19:26:23',50,'tiennv6',1,'2018-05-18 19:26:23','2018-05-18 19:26:23'),(209,378,'2018-05-18 19:20:33','2018-05-18 19:20:26',50,'thanhnl',1,'2018-05-18 19:26:23','2018-05-18 19:26:23'),(210,379,'2018-05-18 19:26:23','2018-05-18 19:20:34',50,'tult',1,'2018-05-18 19:26:23','2018-05-18 19:26:23'),(211,380,NULL,'0000-00-00 00:00:00',50,'khoalt',NULL,'2018-05-18 19:26:23','2018-05-18 19:26:23'),(212,381,NULL,'0000-00-00 00:00:00',50,'trungvd',NULL,'2018-05-18 19:26:23','2018-05-19 22:26:23'),(213,382,NULL,'0000-00-00 00:00:00',50,'tiennv6',NULL,'2018-05-18 19:26:23','2018-05-21 01:26:23'),(214,372,'2018-05-18 19:20:23','2018-05-18 19:26:23',51,'tiennv6',1,'2018-05-18 19:26:23','2018-05-18 19:26:23'),(215,373,'2018-05-18 19:20:33','2018-05-18 19:20:26',51,'thanhnl',1,'2018-05-18 19:26:23','2018-05-18 19:26:23'),(216,374,'2018-05-18 19:26:23','2018-05-18 19:20:34',51,'tult',1,'2018-05-18 19:26:23','2018-05-18 19:26:23'),(217,375,NULL,'0000-00-00 00:00:00',51,'trungvd',NULL,'2018-05-18 19:26:23','2018-05-18 19:26:23'),(218,376,NULL,'0000-00-00 00:00:00',51,'tiennv6',NULL,'2018-05-18 19:26:23','2018-05-19 22:26:23'),(219,377,'2018-05-18 19:20:23','2018-05-18 19:27:28',52,'tiennv6',1,'2018-05-18 19:27:28','2018-05-18 19:27:28'),(220,378,'2018-05-18 19:20:33','2018-05-18 19:20:26',52,'thanhnl',1,'2018-05-18 19:27:28','2018-05-18 19:27:28'),(221,379,'2018-05-18 19:27:28','2018-05-18 19:20:34',52,'tult',1,'2018-05-18 19:27:28','2018-05-18 19:27:28'),(222,380,NULL,'0000-00-00 00:00:00',52,'khoalt',NULL,'2018-05-18 19:27:28','2018-05-18 19:27:28'),(223,381,NULL,'0000-00-00 00:00:00',52,'trungvd',NULL,'2018-05-18 19:27:28','2018-05-19 22:27:28'),(224,382,NULL,'0000-00-00 00:00:00',52,'tiennv6',NULL,'2018-05-18 19:27:28','2018-05-21 01:27:28'),(225,372,'2018-05-18 19:20:23','2018-05-18 19:27:28',53,'tiennv6',1,'2018-05-18 19:27:28','2018-05-18 19:27:28'),(226,373,'2018-05-18 19:20:33','2018-05-18 19:20:26',53,'thanhnl',1,'2018-05-18 19:27:28','2018-05-18 19:27:28'),(227,374,'2018-05-18 19:27:28','2018-05-18 19:20:34',53,'tult',1,'2018-05-18 19:27:28','2018-05-18 19:27:28'),(228,375,NULL,'0000-00-00 00:00:00',53,'trungvd',NULL,'2018-05-18 19:27:28','2018-05-18 19:27:28'),(229,376,NULL,'0000-00-00 00:00:00',53,'tiennv6',NULL,'2018-05-18 19:27:28','2018-05-19 22:27:28'),(230,377,'2018-05-18 19:20:23','2018-05-18 19:28:50',54,'tiennv6',1,'2018-05-18 19:28:50','2018-05-18 19:28:50'),(231,378,'2018-05-18 19:20:33','2018-05-18 19:20:26',54,'thanhnl',1,'2018-05-18 19:28:50','2018-05-18 19:28:50'),(232,379,'2018-05-18 19:28:50','2018-05-18 19:20:34',54,'tult',1,'2018-05-18 19:28:50','2018-05-18 19:28:50'),(233,380,NULL,'0000-00-00 00:00:00',54,'khoalt',NULL,'2018-05-18 19:28:50','2018-05-18 19:28:50'),(234,381,NULL,'0000-00-00 00:00:00',54,'trungvd',NULL,'2018-05-18 19:28:50','2018-05-19 22:28:50'),(235,382,NULL,'0000-00-00 00:00:00',54,'tiennv6',NULL,'2018-05-18 19:28:50','2018-05-21 01:28:50'),(236,372,'2018-05-18 19:20:23','2018-05-18 19:28:50',55,'tiennv6',1,'2018-05-18 19:28:50','2018-05-18 19:28:50'),(237,373,'2018-05-18 19:20:33','2018-05-18 19:20:26',55,'thanhnl',1,'2018-05-18 19:28:50','2018-05-18 19:28:50'),(238,374,'2018-05-18 19:28:50','2018-05-18 19:20:34',55,'tult',1,'2018-05-18 19:28:50','2018-05-18 19:28:50'),(239,375,'2018-05-31 11:06:54','0000-00-00 00:00:00',55,'trungvd',1,'2018-05-18 19:28:50','2018-05-18 19:28:50'),(240,376,'2018-06-04 15:17:07','2018-05-31 11:06:54',55,'tiennv6',1,'2018-05-18 19:28:50','2018-05-19 22:28:50'),(241,369,'2018-05-21 09:40:18','0000-00-00 00:00:00',56,'tiennv6',1,'2018-05-21 09:40:18','2018-05-21 09:40:18'),(242,370,'2018-05-21 09:41:51','2018-05-21 09:40:20',56,'thanhnl',1,'2018-05-21 09:40:18','2018-05-22 12:40:18'),(243,371,'2018-05-21 13:24:21','2018-05-21 09:41:52',56,'tult',1,'2018-05-21 09:40:18','2018-05-23 15:40:18'),(244,377,'2018-05-21 09:40:18','2018-05-21 13:24:21',57,'tiennv6',1,'2018-05-21 13:24:21','2018-05-21 13:24:21'),(245,378,'2018-05-21 09:41:51','2018-05-21 09:40:20',57,'thanhnl',1,'2018-05-21 13:24:21','2018-05-21 13:24:21'),(246,379,'2018-05-21 13:24:21','2018-05-21 09:41:52',57,'tult',1,'2018-05-21 13:24:21','2018-05-21 13:24:21'),(247,380,NULL,'0000-00-00 00:00:00',57,'khoalt',NULL,'2018-05-21 13:24:21','2018-05-21 13:24:21'),(248,381,NULL,'0000-00-00 00:00:00',57,'trungvd',NULL,'2018-05-21 13:24:21','2018-05-22 16:24:21'),(249,382,NULL,'0000-00-00 00:00:00',57,'doanhvtm',NULL,'2018-05-21 13:24:21','2018-05-23 19:24:21'),(250,409,'2018-06-01 13:59:09','0000-00-00 00:00:00',58,'tiennv6',1,'2018-06-01 13:59:09','2018-06-01 13:59:09'),(251,410,NULL,'2018-06-01 13:59:09',58,'trungvd',NULL,'2018-06-01 13:59:09','2018-06-02 16:59:09'),(252,411,NULL,'0000-00-00 00:00:00',58,'khoalt',NULL,'2018-06-01 13:59:09','2018-06-03 19:59:09'),(253,412,NULL,'0000-00-00 00:00:00',58,'trungvd',NULL,'2018-06-01 13:59:09','2018-06-04 22:59:09'),(254,413,NULL,'0000-00-00 00:00:00',58,'tiennv6',NULL,'2018-06-01 13:59:09','2018-06-06 01:59:09'),(255,409,'2018-06-04 14:48:56','0000-00-00 00:00:00',59,'tiennv6',1,'2018-06-04 14:48:56','2018-06-04 14:48:56'),(256,410,NULL,'2018-06-04 14:48:56',59,'trungvd',NULL,'2018-06-04 14:48:56','2018-06-05 17:48:56'),(257,411,NULL,'0000-00-00 00:00:00',59,'khoalt',NULL,'2018-06-04 14:48:56','2018-06-06 20:48:56'),(258,412,NULL,'0000-00-00 00:00:00',59,'trungvd',NULL,'2018-06-04 14:48:56','2018-06-07 23:48:56'),(259,413,NULL,'0000-00-00 00:00:00',59,'tiennv6',NULL,'2018-06-04 14:48:56','2018-06-09 02:48:56'),(260,409,'2018-06-04 14:49:54','0000-00-00 00:00:00',60,'tiennv6',1,'2018-06-04 14:49:54','2018-06-04 14:49:54'),(261,410,NULL,'2018-06-04 14:49:54',60,'trungvd',NULL,'2018-06-04 14:49:54','2018-06-05 17:49:54'),(262,411,NULL,'0000-00-00 00:00:00',60,'khoalt',NULL,'2018-06-04 14:49:54','2018-06-06 20:49:54'),(263,412,NULL,'0000-00-00 00:00:00',60,'trungvd',NULL,'2018-06-04 14:49:54','2018-06-07 23:49:54'),(264,413,NULL,'0000-00-00 00:00:00',60,'tiennv6',NULL,'2018-06-04 14:49:54','2018-06-09 02:49:54'),(265,369,'2018-06-04 15:02:23','0000-00-00 00:00:00',61,'tiennv6',1,'2018-06-04 15:02:23','2018-06-04 15:02:23'),(266,370,'2018-06-04 15:06:55','2018-06-04 15:02:23',61,'thanhnl',1,'2018-06-04 15:02:23','2018-06-05 18:02:23'),(267,371,'2018-06-04 15:07:23','2018-06-04 15:06:55',61,'tult',1,'2018-06-04 15:02:23','2018-06-06 21:02:23'),(268,369,'2018-06-04 15:31:27','0000-00-00 00:00:00',62,'tiennv6',1,'2018-06-04 15:31:27','2018-06-04 15:31:27'),(269,370,'2018-06-04 15:31:40','2018-06-04 15:31:27',62,'thanhnl',1,'2018-06-04 15:31:27','2018-06-05 18:31:27'),(270,371,'2018-06-04 16:13:26','2018-06-04 15:31:40',62,'tult',1,'2018-06-04 15:31:27','2018-06-06 21:31:27'),(271,409,'2018-06-04 16:40:50','0000-00-00 00:00:00',63,'tiennv6',1,'2018-06-04 16:40:50','2018-06-04 16:40:50'),(272,410,NULL,'2018-06-04 16:40:50',63,'trungvd',NULL,'2018-06-04 16:40:50','2018-06-05 19:40:50'),(273,411,NULL,'0000-00-00 00:00:00',63,'khoalt',NULL,'2018-06-04 16:40:50','2018-06-06 22:40:50'),(274,412,NULL,'0000-00-00 00:00:00',63,'trungvd',NULL,'2018-06-04 16:40:50','2018-06-08 01:40:50'),(275,413,NULL,'0000-00-00 00:00:00',63,'tiennv6',NULL,'2018-06-04 16:40:50','2018-06-09 04:40:50'),(276,409,'2018-06-04 16:41:31','0000-00-00 00:00:00',64,'tiennv6',1,'2018-06-04 16:41:31','2018-06-04 16:41:31'),(277,410,NULL,'2018-06-04 16:41:31',64,'trungvd',NULL,'2018-06-04 16:41:31','2018-06-05 19:41:31'),(278,411,NULL,'0000-00-00 00:00:00',64,'khoalt',NULL,'2018-06-04 16:41:31','2018-06-06 22:41:31'),(279,412,NULL,'0000-00-00 00:00:00',64,'trungvd',NULL,'2018-06-04 16:41:31','2018-06-08 01:41:31'),(280,413,NULL,'0000-00-00 00:00:00',64,'tiennv6',NULL,'2018-06-04 16:41:31','2018-06-09 04:41:31'),(281,409,'2018-06-04 17:13:42','0000-00-00 00:00:00',65,'tiennv6',1,'2018-06-04 17:13:42','2018-06-04 17:13:42'),(282,410,NULL,'2018-06-04 17:13:42',65,'trungvd',NULL,'2018-06-04 17:13:42','2018-06-05 20:13:42'),(283,411,NULL,'0000-00-00 00:00:00',65,'khoalt',NULL,'2018-06-04 17:13:42','2018-06-06 23:13:42'),(284,412,NULL,'0000-00-00 00:00:00',65,'trungvd',NULL,'2018-06-04 17:13:42','2018-06-08 02:13:42'),(285,413,NULL,'0000-00-00 00:00:00',65,'tiennv6',NULL,'2018-06-04 17:13:42','2018-06-09 05:13:42');
/*!40000 ALTER TABLE `am_request_steps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_sub_definition`
--

DROP TABLE IF EXISTS `am_request_sub_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_sub_definition` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `StepDefinitionID` int(11) DEFAULT NULL,
  `RequestDefinitionID` int(11) DEFAULT NULL,
  `Mapping` text,
  `Condition` text,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_sub_definition`
--

LOCK TABLES `am_request_sub_definition` WRITE;
/*!40000 ALTER TABLE `am_request_sub_definition` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_request_sub_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_table_data`
--

DROP TABLE IF EXISTS `am_request_table_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_table_data` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_table_data`
--

LOCK TABLES `am_request_table_data` WRITE;
/*!40000 ALTER TABLE `am_request_table_data` DISABLE KEYS */;
INSERT INTO `am_request_table_data` VALUES (36),(37),(38),(39),(40),(41),(42),(43),(44),(45),(46),(47),(48),(49),(50),(51),(52),(53),(54),(55),(56);
/*!40000 ALTER TABLE `am_request_table_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_table_detail_data`
--

DROP TABLE IF EXISTS `am_request_table_detail_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_table_detail_data` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TableID` int(10) NOT NULL,
  `Field0` varchar(1000) DEFAULT '',
  `Field1` varchar(1000) DEFAULT '',
  `Field2` varchar(1000) DEFAULT '',
  `Field3` varchar(1000) DEFAULT '',
  `Field4` varchar(1000) DEFAULT '',
  `Field5` varchar(1000) DEFAULT '',
  `Field6` varchar(1000) DEFAULT '',
  `Field7` varchar(1000) DEFAULT '',
  `Field8` varchar(1000) DEFAULT '',
  `Field9` varchar(1000) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=262 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_table_detail_data`
--

LOCK TABLES `am_request_table_detail_data` WRITE;
/*!40000 ALTER TABLE `am_request_table_detail_data` DISABLE KEYS */;
INSERT INTO `am_request_table_detail_data` VALUES (106,36,'Adapter Apple Macbook MLHE2SA.A','3',NULL,'','','','','','',''),(107,36,'03. Non-standard desktop','5',NULL,'','','','','','',''),(108,36,'Adobe Design Std CS6 6.0 MLP AOO License IE','5',NULL,'','','','','','',''),(109,36,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE','3',NULL,'','','','','','',''),(110,37,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(111,37,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(112,37,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(113,37,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(114,37,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(115,37,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(116,37,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(117,37,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(118,37,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(119,37,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(120,37,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(121,37,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(122,37,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(123,37,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(124,37,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(125,37,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(126,38,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(127,38,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(128,38,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(129,38,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(130,38,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(131,38,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(132,38,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(133,38,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(134,39,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(135,39,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(136,39,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(137,39,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(138,39,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(139,39,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(140,39,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(141,39,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(142,39,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(143,39,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(144,39,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(145,39,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(146,39,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(147,39,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(148,39,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(149,39,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(150,40,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(151,40,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(152,40,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(153,40,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(154,40,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(155,40,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(156,40,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(157,40,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(158,41,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(159,41,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(160,41,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(161,41,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(162,41,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(163,41,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(164,41,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(165,41,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(166,41,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(167,41,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(168,41,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(169,41,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(170,41,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(171,41,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(172,41,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(173,41,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(174,42,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(175,42,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(176,42,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(177,42,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(178,42,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(179,42,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(180,42,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(181,42,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(182,43,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(183,43,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(184,43,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(185,43,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(186,43,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(187,43,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(188,43,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(189,43,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(190,43,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(191,43,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(192,43,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(193,43,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(194,43,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(195,43,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(196,43,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(197,43,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(198,44,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(199,44,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(200,44,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(201,44,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(202,44,'Adobe Design Std CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(203,44,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(204,44,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(205,44,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE',NULL,NULL,'','','','','','',''),(206,45,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(207,45,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(208,45,'Adapter Apple Macbook MLHE2SA.A',NULL,NULL,'','','','','','',''),(209,45,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(210,45,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(211,45,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(212,45,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(213,45,'03. Non-standard desktop',NULL,NULL,'','','','','','',''),(214,46,'Adobe Design Std CS6 6.0 MLP AOO License IE','ADA00017',NULL,'','','','','','',''),(215,46,'Adobe Design Std CS6 6.0 MLP AOO License IE','BAR00002',NULL,'','','','','','',''),(216,46,'Adobe Design Std CS6 6.0 MLP AOO License IE','CAB00003',NULL,'','','','','','',''),(217,46,'Adobe Design Std CS6 6.0 MLP AOO License IE','',NULL,'','','','','','',''),(218,46,'Adobe Design Std CS6 6.0 MLP AOO License IE','EHD00002',NULL,'','','','','','',''),(219,46,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE','',NULL,'','','','','','',''),(220,46,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE','',NULL,'','','','','','',''),(221,46,'Adobe Premiere Pro CS6 6.0 MLP AOO License IE','',NULL,'','','','','','',''),(222,47,'Apple Magic Mouse 2','3',NULL,'','','','','','',''),(223,47,'Sony Sound Monitoring MDR-ZX100',NULL,NULL,'','','','','','',''),(224,48,'Apple Magic Mouse 2',NULL,NULL,'','','','','','',''),(225,48,'Apple Magic Mouse 2',NULL,NULL,'','','','','','',''),(226,48,'Apple Magic Mouse 2',NULL,NULL,'','','','','','',''),(227,49,NULL,'Corsair 4GB/1333 (PC)',NULL,'','','','','','',''),(228,49,NULL,'Corsair 4GB/1333 (PC)',NULL,'','','','','','',''),(229,49,NULL,'Corsair 4GB/1333 (PC)',NULL,'','','','','','',''),(230,50,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(231,50,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(232,50,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(233,50,NULL,'Apple: Macbook Pro MJLT2ZP.A',NULL,'','','','','','',''),(234,50,NULL,'Apple: Macbook Pro MJLT2ZP.A',NULL,'','','','','','',''),(235,51,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(236,51,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(237,51,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(238,51,NULL,'Apple: Macbook Pro MJLT2ZP.A',NULL,'','','','','','',''),(239,51,NULL,'Apple: Macbook Pro MJLT2ZP.A',NULL,'','','','','','',''),(240,52,'Adapter Access Point AIR-LAP1262N-E-K9','5',NULL,'','','','','','',''),(241,52,'Mon LCD Dell 19\"; 1908WFP','5',NULL,'','','','','','',''),(242,52,'Apple USB Ethernet','3','http://itportal:8080/user-page/order/detail/?wfID=80','','','','','','',''),(243,53,'01. Standard laptop X series',NULL,NULL,'','','','','','',''),(244,54,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(245,54,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(246,54,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(247,54,NULL,'Apple: Macbook Pro MJLT2ZP.A',NULL,'','','','','','',''),(248,54,NULL,'Apple: Macbook Pro MJLT2ZP.A',NULL,'','','','','','',''),(249,55,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(250,55,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(251,55,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(252,55,NULL,'Apple: Macbook Pro MJLT2ZP.A',NULL,'','','','','','',''),(253,55,NULL,'Apple: Macbook Pro MJLT2ZP.A',NULL,'','','','','','',''),(254,56,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(255,56,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(256,56,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(257,56,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(258,56,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(259,56,NULL,'000025/2016/LL/HCM',NULL,'','','','','','',''),(260,56,NULL,'Apple: Macbook Pro MJLT2ZP.A',NULL,'','','','','','',''),(261,56,NULL,'Apple: Macbook Pro MJLT2ZP.A',NULL,'','','','','','','');
/*!40000 ALTER TABLE `am_request_table_detail_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_request_types`
--

DROP TABLE IF EXISTS `am_request_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_types` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `RequestCategoryID` int(10) unsigned NOT NULL DEFAULT '0',
  `Description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_request_types`
--

LOCK TABLES `am_request_types` WRITE;
/*!40000 ALTER TABLE `am_request_types` DISABLE KEYS */;
INSERT INTO `am_request_types` VALUES (1,'Cấp Phát',1,'Cấp Phát'),(2,'Thu Hồi',1,'Thu Hồi'),(3,'Bảo Hành',1,'Bảo Hành');
/*!40000 ALTER TABLE `am_request_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_requests`
--

DROP TABLE IF EXISTS `am_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_requests` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DefinitionID` int(10) unsigned NOT NULL DEFAULT '0',
  `Creator` int(10) unsigned NOT NULL DEFAULT '0',
  `CreateDate` datetime NOT NULL,
  `CurrentIndex` smallint(5) NOT NULL,
  `RequestStatusID` int(10) unsigned NOT NULL DEFAULT '0',
  `ParrentID` int(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_requests`
--

LOCK TABLES `am_requests` WRITE;
/*!40000 ALTER TABLE `am_requests` DISABLE KEYS */;
INSERT INTO `am_requests` VALUES (45,80,0,'2018-05-18 19:20:23',3,1,NULL),(54,82,0,'2018-05-18 19:28:50',3,1,45),(55,81,0,'2018-05-18 19:28:50',5,2,45),(56,80,0,'2018-05-21 09:40:18',3,1,NULL),(57,82,0,'2018-05-21 13:24:21',3,1,56),(58,89,0,'2018-06-01 13:59:09',1,1,NULL),(59,89,0,'2018-06-04 14:48:55',1,1,NULL),(60,89,0,'2018-06-04 14:49:54',1,1,NULL),(61,80,0,'2018-06-04 15:02:22',2,2,NULL),(62,80,0,'2018-06-04 15:31:27',2,2,NULL),(63,89,0,'2018-06-04 16:40:50',1,1,NULL),(64,89,0,'2018-06-04 16:41:31',1,1,NULL),(65,89,0,'2018-06-04 17:13:42',1,1,NULL);
/*!40000 ALTER TABLE `am_requests` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-06 17:47:25
