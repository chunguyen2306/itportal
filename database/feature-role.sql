-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: wordpress
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `am_features`
--

DROP TABLE IF EXISTS `am_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_features` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `FeatureName` varchar(50) NOT NULL,
  `DisplayName` varchar(50) NOT NULL,
  `Description` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_features`
--

LOCK TABLES `am_features` WRITE;
/*!40000 ALTER TABLE `am_features` DISABLE KEYS */;
INSERT INTO `am_features` VALUES (1,'workflow.request.view','Xem đơn hàng','Tìm kiếm, tra cứu, xem đơn hàng'),(16,'workflow.request.accept','Duyệt đơn hàng','Duyệt đơn hàng'),(17,'resource.asset.view','Xem tài sản','Xem tài sản'),(18,'admin.permissions.grant','Phân quyền cho user','Phân quyền cho user'),(19,'resource.type.view','Xem danh sách loại tài sản',''),(20,'resource.model.view','Xem danh sách tên sản phẩm',''),(21,'menu-type.view','Xem loại menu',''),(22,'main-menu.view','Xem menu chính',''),(23,'personal.menu.view','Xem mune trang cá nhân',''),(24,'admin.menu.view','Xem menu trang quản trị',''),(25,'admin.edit-page.view','Chỉnh sửa trang','');
/*!40000 ALTER TABLE `am_features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_role_feature_mapping`
--

DROP TABLE IF EXISTS `am_role_feature_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_role_feature_mapping` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `RoleID` int(10) NOT NULL,
  `FeatureID` varchar(8000) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_role_feature_mapping`
--

LOCK TABLES `am_role_feature_mapping` WRITE;
/*!40000 ALTER TABLE `am_role_feature_mapping` DISABLE KEYS */;
INSERT INTO `am_role_feature_mapping` VALUES (1,1,'[\"1\",\"16\",\"17\",\"18\",\"19\",\"20\",\"21\",\"22\",\"23\",\"24\",\"25\"]'),(3,4,'[\"1\",\"16\",\"17\",\"19\"]'),(6,8,'[\"1\",\"16\"]');
/*!40000 ALTER TABLE `am_role_feature_mapping` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-28 18:56:10
