 -- add column am_request_steps 
ALTER TABLE `am_request_steps` 
ADD COLUMN `AssignedOwner` VARCHAR(50) NULL AFTER `Owner`;
ALTER TABLE `am_request_steps` 
ADD COLUMN `DelegateMode` TINYINT(1) NULL COMMENT 'cho biết hình thức ủy quyền\n\nbin  dec  description\n00    0     delegate + no-noti\n01    1     delegate + noti  \n10    2     assign +  no-noti\n11    3     assign +  noti  ' AFTER `MinIntendTime`;


 -- update new collumn data
SET SQL_SAFE_UPDATES = 0;
update am_request_steps set AssignedOwner = Owner;

select * from am_request_steps where RequestID = 3660;

