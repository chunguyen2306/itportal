-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: wordpress
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `am_asset_model_detail`
--

DROP TABLE IF EXISTS `am_asset_model_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_asset_model_detail` (
  `ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `Description` longtext,
  `ShortDescription` longtext,
  `Gallery` varchar(5000) DEFAULT NULL,
  `Thubmails` varchar(100) DEFAULT NULL,
  `Cost` int(10) unsigned DEFAULT NULL,
  `ModelID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `IsDisplay` tinyint(1) DEFAULT NULL,
  `ManualGuide` longtext,
  `IsStandard` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `NeedApproval` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ReNewYear` int(10) unsigned DEFAULT NULL,
  `TypeID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `ProductPageDescription` longtext,
  `ManufacturerName` varchar(100) DEFAULT NULL,
  `AssetName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_asset_model_review`
--

DROP TABLE IF EXISTS `am_asset_model_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_asset_model_review` (
  `ID` int(10) unsigned NOT NULL,
  `UserName` varchar(45) NOT NULL DEFAULT '',
  `Comment` longtext,
  `Date` bigint(20) unsigned NOT NULL DEFAULT '0',
  `Rate` double NOT NULL DEFAULT '0',
  `ModelID` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_asset_type_group`
--

DROP TABLE IF EXISTS `am_asset_type_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_asset_type_group` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `assettype` text,
  `maxmonth` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_assetcostformular`
--

DROP TABLE IF EXISTS `am_assetcostformular`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_assetcostformular` (
  `ID` int(10) unsigned NOT NULL,
  `Name` varchar(100) NOT NULL DEFAULT '',
  `Formular` varchar(1000) NOT NULL DEFAULT '',
  `IsShowOnAsset` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_assetcostvariables`
--

DROP TABLE IF EXISTS `am_assetcostvariables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_assetcostvariables` (
  `ID` int(10) unsigned NOT NULL,
  `Name` varchar(100) NOT NULL DEFAULT '',
  `Value` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_default_asset`
--

DROP TABLE IF EXISTS `am_default_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_default_asset` (
  `ID` int(10) unsigned NOT NULL,
  `JobGroupID` int(10) unsigned NOT NULL DEFAULT '0',
  `AssetType` varchar(45) DEFAULT NULL,
  `AssetModel` varchar(45) DEFAULT NULL,
  `Quantity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_discountperyear`
--

DROP TABLE IF EXISTS `am_discountperyear`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_discountperyear` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `AssetTypeGroupID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `Year` int(10) unsigned NOT NULL DEFAULT '0',
  `Percent` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_helpdeskconsole_log`
--

DROP TABLE IF EXISTS `am_helpdeskconsole_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_helpdeskconsole_log` (
  `ID` int(10) unsigned NOT NULL,
  `Action` varchar(45) NOT NULL DEFAULT '',
  `ActionTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Result` text NOT NULL,
  `User` varchar(45) NOT NULL DEFAULT '',
  `ActionUser` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_job_branch`
--

DROP TABLE IF EXISTS `am_job_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_job_branch` (
  `ID` int(10) unsigned NOT NULL,
  `BranchName` varchar(45) DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_job_groups`
--

DROP TABLE IF EXISTS `am_job_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_job_groups` (
  `ID` int(10) unsigned NOT NULL,
  `GroupName` varchar(45) DEFAULT NULL,
  `BranchID` int(10) unsigned NOT NULL DEFAULT '0',
  `Description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_main_menu`
--

DROP TABLE IF EXISTS `am_main_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_main_menu` (
  `ID` bigint(20) unsigned NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Parent` bigint(20) DEFAULT NULL,
  `Mapping` text,
  `Icon` varchar(100) NOT NULL DEFAULT 'no-icon.png',
  `TypeID` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_menu_type`
--

DROP TABLE IF EXISTS `am_menu_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_menu_type` (
  `ID` bigint(20) unsigned NOT NULL,
  `Name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_page_content`
--

DROP TABLE IF EXISTS `am_page_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_page_content` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `value` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `page` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_permission`
--

DROP TABLE IF EXISTS `am_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_permission` (
  `ID` int(10) unsigned NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Descriptions` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_approval_code`
--

DROP TABLE IF EXISTS `am_request_approval_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_approval_code` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RequestID` int(11) DEFAULT NULL,
  `Code` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_categories`
--

DROP TABLE IF EXISTS `am_request_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_categories` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_comment_role`
--

DROP TABLE IF EXISTS `am_request_comment_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_comment_role` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Domain` varchar(45) DEFAULT NULL,
  `StepID` int(11) DEFAULT NULL,
  `Done` tinyint(1) DEFAULT NULL,
  `CommentID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_comments`
--

DROP TABLE IF EXISTS `am_request_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_comments` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Comment` varchar(1000) DEFAULT '',
  `Time` datetime NOT NULL,
  `StepIndex` int(10) unsigned NOT NULL DEFAULT '0',
  `RequestID` int(10) unsigned NOT NULL DEFAULT '0',
  `Owner` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_data_types`
--

DROP TABLE IF EXISTS `am_request_data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_data_types` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `DisplayName` varchar(100) NOT NULL DEFAULT '',
  `Description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_definition`
--

DROP TABLE IF EXISTS `am_request_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_definition` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CreateDate` datetime NOT NULL,
  `Creator` varchar(50) NOT NULL DEFAULT '',
  `IsDelete` tinyint(1) NOT NULL,
  `RequestReasonID` int(10) unsigned NOT NULL DEFAULT '0',
  `Description` varchar(200) NOT NULL DEFAULT '',
  `Name` varchar(50) NOT NULL DEFAULT '',
  `DisplayName` varchar(50) NOT NULL DEFAULT '',
  `OwnerField` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_definition_model_group`
--

DROP TABLE IF EXISTS `am_request_definition_model_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_definition_model_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DisplayName` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_definition_model_group_detail`
--

DROP TABLE IF EXISTS `am_request_definition_model_group_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_definition_model_group_detail` (
  `GroupID` int(11) NOT NULL,
  `ModelID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_feature_mapping`
--

DROP TABLE IF EXISTS `am_request_feature_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_feature_mapping` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) NOT NULL DEFAULT '',
  `Output` text,
  `Mapping` text,
  `RequestID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_field_definition`
--

DROP TABLE IF EXISTS `am_request_field_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_field_definition` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `DisplayName` varchar(50) NOT NULL DEFAULT '',
  `MappingName` varchar(50) NOT NULL DEFAULT '',
  `RequestDataTypeID` int(10) unsigned NOT NULL DEFAULT '1',
  `RequestDefinitionID` int(10) unsigned NOT NULL DEFAULT '0',
  `ParentID` int(10) unsigned DEFAULT '0',
  `Config` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=633 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_fields`
--

DROP TABLE IF EXISTS `am_request_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_fields` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RequestID` int(10) NOT NULL,
  `Field0` varchar(1000) DEFAULT '',
  `Field1` varchar(1000) DEFAULT '',
  `Field2` varchar(1000) DEFAULT '',
  `Field3` varchar(1000) DEFAULT '',
  `Field4` varchar(1000) DEFAULT '',
  `Field5` varchar(1000) DEFAULT '',
  `Field6` varchar(1000) DEFAULT '',
  `Field7` varchar(1000) DEFAULT '',
  `Field8` varchar(1000) DEFAULT '',
  `Field9` varchar(1000) DEFAULT '',
  `Field10` varchar(1000) DEFAULT '',
  `Field11` varchar(1000) DEFAULT '',
  `Field12` varchar(1000) DEFAULT '',
  `Field13` varchar(1000) DEFAULT '',
  `Field14` varchar(1000) DEFAULT '',
  PRIMARY KEY (`ID`,`RequestID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_notification`
--

DROP TABLE IF EXISTS `am_request_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_notification` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Domain` varchar(45) NOT NULL,
  `RequestID` int(11) NOT NULL,
  `Title` varchar(200) NOT NULL,
  `Content` text NOT NULL,
  `IsSeen` tinyint(1) NOT NULL,
  `IsRead` tinyint(1) NOT NULL,
  `TypeID` int(11) NOT NULL,
  `ShortContent` varchar(200) DEFAULT NULL,
  `Time` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_owner`
--

DROP TABLE IF EXISTS `am_request_owner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_owner` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Domain` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `Name` varchar(110) NOT NULL,
  `JobTitle` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `DepartmentName` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `Seat` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `Location` varchar(45) DEFAULT NULL,
  `IsCurrent` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_reasons`
--

DROP TABLE IF EXISTS `am_request_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_reasons` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `RequestTypeID` int(10) unsigned NOT NULL DEFAULT '0',
  `Description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_search`
--

DROP TABLE IF EXISTS `am_request_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_search` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DisplayName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_search_column`
--

DROP TABLE IF EXISTS `am_request_search_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_search_column` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `DisplayName` varchar(100) DEFAULT NULL,
  `Source` varchar(500) DEFAULT NULL,
  `SearchID` int(11) DEFAULT NULL,
  `TypeID` int(11) DEFAULT NULL,
  `Style` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_search_condition`
--

DROP TABLE IF EXISTS `am_request_search_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_search_condition` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `DisplayName` varchar(100) DEFAULT NULL,
  `ColumnName` varchar(50) DEFAULT NULL,
  `SearchID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_search_filter`
--

DROP TABLE IF EXISTS `am_request_search_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_search_filter` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `DisplayName` varchar(100) DEFAULT NULL,
  `ColumnName` varchar(50) DEFAULT NULL,
  `SearchID` int(11) DEFAULT NULL,
  `Style` varchar(500) DEFAULT '{}',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_status`
--

DROP TABLE IF EXISTS `am_request_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_status` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `DisplayName` varchar(50) NOT NULL DEFAULT '',
  `Description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_step_actions`
--

DROP TABLE IF EXISTS `am_request_step_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_step_actions` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT '',
  `RequestStepDefinitionID` int(10) unsigned NOT NULL DEFAULT '0',
  `EventID` tinyint(1) NOT NULL DEFAULT '0',
  `Config` text NOT NULL,
  `IsRollback` tinyint(1) NOT NULL DEFAULT '0',
  `IsSync` tinyint(1) NOT NULL DEFAULT '0',
  `ActionTypeID` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_step_definitions`
--

DROP TABLE IF EXISTS `am_request_step_definitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_step_definitions` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `StepIndex` smallint(5) unsigned NOT NULL DEFAULT '0',
  `Name` varchar(50) NOT NULL DEFAULT '',
  `DisplayName` varchar(50) NOT NULL DEFAULT '',
  `RequestDefinitionID` int(10) unsigned NOT NULL DEFAULT '0',
  `Role` varchar(50) NOT NULL,
  `MaxIntendDuration` int(11) NOT NULL,
  `MinIntendDuration` int(11) NOT NULL,
  `CanRequestInfo` tinyint(1) DEFAULT '0',
  `BackStep` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=427 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_step_permission`
--

DROP TABLE IF EXISTS `am_request_step_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_step_permission` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `StepDefinitionID` int(10) unsigned NOT NULL DEFAULT '0',
  `FieldDefinitionID` int(10) unsigned NOT NULL DEFAULT '0',
  `Permission` varchar(45) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_steps`
--

DROP TABLE IF EXISTS `am_request_steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_steps` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DefinitionID` int(10) unsigned NOT NULL DEFAULT '0',
  `ActionDate` datetime DEFAULT NULL,
  `StartDate` datetime NOT NULL,
  `RequestID` int(10) unsigned NOT NULL DEFAULT '0',
  `Owner` varchar(50) NOT NULL,
  `IsApproved` tinyint(1) DEFAULT NULL,
  `MaxIntendTime` datetime NOT NULL,
  `MinIntendTime` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_sub_definition`
--

DROP TABLE IF EXISTS `am_request_sub_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_sub_definition` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `StepDefinitionID` int(11) DEFAULT NULL,
  `RequestDefinitionID` int(11) DEFAULT NULL,
  `Mapping` text,
  `Condition` text,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_table_data`
--

DROP TABLE IF EXISTS `am_request_table_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_table_data` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_table_detail_data`
--

DROP TABLE IF EXISTS `am_request_table_detail_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_table_detail_data` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TableID` int(10) NOT NULL,
  `Field0` varchar(1000) DEFAULT '',
  `Field1` varchar(1000) DEFAULT '',
  `Field2` varchar(1000) DEFAULT '',
  `Field3` varchar(1000) DEFAULT '',
  `Field4` varchar(1000) DEFAULT '',
  `Field5` varchar(1000) DEFAULT '',
  `Field6` varchar(1000) DEFAULT '',
  `Field7` varchar(1000) DEFAULT '',
  `Field8` varchar(1000) DEFAULT '',
  `Field9` varchar(1000) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_request_types`
--

DROP TABLE IF EXISTS `am_request_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_request_types` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `RequestCategoryID` int(10) unsigned NOT NULL DEFAULT '0',
  `Description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_requests`
--

DROP TABLE IF EXISTS `am_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_requests` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DefinitionID` int(10) unsigned NOT NULL DEFAULT '0',
  `Creator` int(10) unsigned NOT NULL DEFAULT '0',
  `CreateDate` datetime NOT NULL,
  `CurrentIndex` smallint(5) NOT NULL,
  `RequestStatusID` int(10) unsigned NOT NULL DEFAULT '0',
  `ParrentID` int(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_role_permission`
--

DROP TABLE IF EXISTS `am_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_role_permission` (
  `ID` int(10) unsigned NOT NULL,
  `RoleID` int(10) unsigned NOT NULL DEFAULT '0',
  `PerID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_roles`
--

DROP TABLE IF EXISTS `am_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_roles` (
  `ID` int(10) unsigned NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_user_role`
--

DROP TABLE IF EXISTS `am_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_user_role` (
  `ID` int(10) unsigned NOT NULL,
  `Domain` varchar(50) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `RoleID` int(11) NOT NULL DEFAULT '0',
  `empCode` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_user_role_mapping`
--

DROP TABLE IF EXISTS `am_user_role_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_user_role_mapping` (
  `ID` int(10) unsigned NOT NULL,
  `Domain` varchar(45) NOT NULL DEFAULT '',
  `Permission` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_user_sync_setting`
--

DROP TABLE IF EXISTS `am_user_sync_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_user_sync_setting` (
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Type` varchar(45) NOT NULL DEFAULT '',
  `Status` tinyint(4) DEFAULT NULL,
  `ID` int(10) unsigned NOT NULL,
  `Class` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `am_users`
--

DROP TABLE IF EXISTS `am_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_users` (
  `ID` int(6) unsigned NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `departmentCode` varchar(50) DEFAULT NULL,
  `departmentLead` varchar(30) DEFAULT NULL,
  `departmentName` varchar(100) DEFAULT NULL,
  `divisionCode` varchar(50) DEFAULT NULL,
  `divisionLead` varchar(30) DEFAULT NULL,
  `divisionName` varchar(100) DEFAULT NULL,
  `domainAccount` varchar(30) DEFAULT NULL,
  `empCode` varchar(30) DEFAULT NULL,
  `ext` varchar(10) DEFAULT NULL,
  `firstName` varchar(30) DEFAULT NULL,
  `fullName` varchar(100) DEFAULT NULL,
  `fullyCode` varchar(100) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `jobTitle` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `reportingLine` varchar(30) DEFAULT NULL,
  `seat` varchar(30) DEFAULT NULL,
  `teamLead` varchar(30) DEFAULT NULL,
  `teamName` varchar(100) DEFAULT NULL,
  `workingEmail` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amassetdetailhistory`
--

DROP TABLE IF EXISTS `amassetdetailhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amassetdetailhistory` (
  `ID` bigint(20) NOT NULL,
  `NewDetail` varchar(3000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `PreviousDetail` varchar(3000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `ActionDate` datetime(6) DEFAULT NULL,
  `UserAction` varchar(100) DEFAULT NULL,
  `Comment` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `SysComment` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `AssetName` varchar(300) CHARACTER SET utf8mb4 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amassetstatehistory`
--

DROP TABLE IF EXISTS `amassetstatehistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amassetstatehistory` (
  `ID` bigint(20) NOT NULL,
  `StateId` bigint(20) NOT NULL,
  `AssetName` varchar(300) CHARACTER SET utf8mb4 DEFAULT NULL,
  `NewState` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `PreviousState` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `ActionDate` datetime(6) DEFAULT NULL,
  `UserAction` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `TargetUser` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `Comment` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `SysComment` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbill`
--

DROP TABLE IF EXISTS `tbill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbill` (
  `ID` int(11) NOT NULL,
  `Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tdetail`
--

DROP TABLE IF EXISTS `tdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tdetail` (
  `ID` int(11) NOT NULL,
  `BillID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-20 17:55:41
