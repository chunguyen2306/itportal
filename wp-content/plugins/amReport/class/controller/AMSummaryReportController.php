<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 3/24/2017
 * Time: 4:10 PM
 */
if(!class_exists('AMSummaryReportController')) {

    //import your package here
    import('theme.package.Abstracts.AbstractController');
    import('plugin.amReport.class.factory.AMReportManagementFactory');

    class AMSummaryReportController extends AbstractController
    {
        protected function init()
        {
            $this->db = new AMReportManagementFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'new' => 'Summary Report/addnew',
                'list' => 'Summary Report/list'
            );
        }

        private function get_all() {
        }

        public function list_action(){
        }

        public function new_action($req, $post){
            global $amReport;
        }
        public function update_action() {

        }
        public function del_action() {

        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}