<?php
if(!class_exists('AMReportManagementController')) {

    //import your package here
    import('theme.package.Abstracts.AbstractController');
    import('plugin.amReport.class.factory.AMReportManagementFactory');

    class AMReportManagementController extends AbstractController
    {
        protected function init()
        {
            $this->db = new AMReportManagementFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'new' => 'Report Management/addnew',
                'list' => 'Report Management/list',
                'update' => 'Report Management/addnew',
                'reportinfo' => 'Report Management/reportinfo'
            );
        }

        public function getDBTable() {
            global $wpdb;
            $mytables = $wpdb->get_results("SHOW TABLES");

            foreach ($mytables as $mytable)
            {
                if(strpos($mytable->Tables_in_wordpress, "am_") !== false) {
                    $tables[] = $mytable->Tables_in_wordpress;
                }
            }
            return $tables;
        }

        public function get_all() {
            $reportCustomList = $this->db->query(array(
                'filter' => array(
                    array('ReportType = "Custom"','')
                )
            ));
            $this->model['reportCustomList'] = $reportCustomList;
        }

        public function list_action(){
            $this->get_all();
        }

        public function reportinfo_action() {

        }

        //Return Query Result Data
        public function getResult($quertStr) {
            global $wpdb;
            $result = $wpdb->get_results($quertStr, ARRAY_A);
            return $result;
        }

        //Return Result Header:
        public function getResultHeader($quertStr, $result, $tmpHeader) {
            global $amRoleManagementFactory, $amRolePermissionFactory;
            $tmpHeader = array();
            if(strpos($quertStr, $amRoleManagementFactory->tableInfo()['table_name']) !== false) {
                $tmpHeader = $tmpHeader + $amRoleManagementFactory->tableInfo()['table_columns'];
            }
            if(strpos($quertStr, $amRolePermissionFactory->tableInfo()['table_name']) !== false) {
                $tmpHeader = $tmpHeader +  $amRolePermissionFactory->tableInfo()['table_columns'];
            }

            $Header = array();
            foreach ($tmpHeader as $key => $value) {
                if(isset($result[0][$key])) {
                    array_push($Header, $value);
                }
            }
            return $Header;
        }

        public function new_action($req, $post){
            global $amReport;
            $tableName = $this->getDBTable();
            //var_dump($tableName);

            $data = array(
                'tables' => array('Role', 'Permission', 'Role Permission', 'User Role'),
                'related' => array (
                    'Role' => array('Permission', 'Role Permission', 'User Role'),
                    'Permission' => array('Role', 'Role Permission', 'User Role'),
                    'Role Permission' => array('Role', 'Permission', 'User Role'),
                    'User Role' => array('Role', 'Permission', 'Role Permission')
                ),
                'column' => array(
                    'Role' => array('ID', 'Name', 'Description'),
                    'Permission' => array('ID', 'Name', 'Descriptions'),
                    'Role Permission' => array('ID', 'RoleID', 'PerID'),
                    'User Role' => array('UserID', 'Domain', 'Name', 'RoleID')
                )
            );

            $this->model['data'] = $data;
            var_dump(json_encode($data));
        }

        public function update_action($req, $post){

        }

        public function del_action($req, $post){
            $id = $req['reportID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}