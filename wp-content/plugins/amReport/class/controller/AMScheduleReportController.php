<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 3/24/2017
 * Time: 4:10 PM
 */
if(!class_exists('AMScheduleReportController')) {

    //import your package here
    import('theme.package.Abstracts.AbstractController');
    import('plugin.amReport.class.factory.AMScheduleReportFactory');
    import('plugin.amReport.class.factory.AMReportManagementFactory');

    class AMScheduleReportController extends AbstractController
    {
        protected $amReportSettingData = array();
        protected $amReportTable = array();
        protected function init()
        {
            $this->db = new AMScheduleReportFactory();
            using('plugin.amReport.class.business.AMReportSettingData');
            $this->amReportSettingData = new AMReportSettingData();
            $this->amReportTable = new AMReportManagementFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'new' => 'Schedule Report/addnew',
                'list' => 'Schedule Report/list',
                'update' => 'Schedule Report/addnew'
            );
        }

        private function get_all() {
            $result = $this->db->query(array());
            return $result;
        }

        public function list_action(){
            $scheduleReport = $this->get_all();
            $this->model['scheduleReportList'] = $scheduleReport;
        }

        public function new_action($req, $post){
            $info = $this->amReportSettingData->setInfoToSettingScheduleType();
            $allReport = $this->amReportTable->query(array());

            $this->model['allReport'] = $allReport;
            $this->model['reportFormat'] = $info['Format'];
            $this->model['Month'] = $info['Month'];
            $this->model['Week'] = $info['Week'];
            $this->model['Date'] = $info['Date'];
            $this->model['Time'] = $info['Time'];

            $action = $post['btnAction'];
            if($action == 'Add new'){
                $scheduleType = '';
                $scheduleDate = '';
                $scheduleTime = '';
                switch($post['scheduleType']) {
                    case 'Once':
                        $scheduleDate = $post['onceDate'];
                        $scheduleTime = $post['onceHours'].':'.$post['onceMinutes'];
                        $scheduleType = 'Once';
                        break;
                    case 'Daily':
                        $scheduleDate = $post['startDate'];
                        $scheduleTime = $post['dailyHours'].':'.$post['dailyMinutes'];
                        $scheduleType = 'Daily';
                        break;
                    case 'Weekly':
                        $day = '';
                        if(sizeof($post['day']) > 0) {
                            foreach ($post['day'] as $item) {
                                if($day != '') {
                                    $day .= ', '.$item;
                                } else {
                                    $day .= $item;
                                }
                            }
                        }
                        $scheduleDate = $day;
                        $scheduleTime = $post['weeklyHours'].':'.$post['weeklyMinutes'];
                        $scheduleType = 'Weekly';
                        break;
                    case 'Monthly':
                        $scheduleDate = $post['monthlyDate'];
                        $scheduleTime = $post['monthlyHours'].':'.$post['monthlyMinutes'];
                        $scheduleType = 'Monthly';
                        break;
                }
                $reportID = 0;
                foreach ($allReport as $item) {
                    if($item->reportTitle == $post['reportTitle']) {
                        $reportID = $item->ID;
                    }
                }
                $newData = array(
                    'reportTitle' => $post['reportTitle'],
                    'reportFormat' => $post['reportFormat'],
                    'emailID' => $post['emailID'],
                    'emailSubject' => $post['emailSubject'],
                    'emailMessage' => $post['emailMessage'],
                    'scheduleType' => $scheduleType,
                    'reportID' => $reportID,
                    'scheduleDate' => $scheduleDate,
                    'scheduleTime' => $scheduleTime
                );

                $this->db->insert($newData);
                wp_redirect( $this->_getPath() );
                exit();
            } if($action == 'Reset'){
                $post['reportTitle'] = '';
                $post['reportType'] = '';
                $post['reportGroup'] = '';
                $post['reportDescription'] = '';
            }
        }

        public function update_action($req, $post) {
            $id = $req['id'];
            $action = $post['btnAction'];
            $this->_model('isUpdate',true);

            if($action == 'Update'){
                $scheduleType = '';
                $scheduleDate = '';
                $scheduleTime = '';
                switch($post['scheduleType']) {
                    case 'Once':
                        $scheduleDate = $post['onceDate'];
                        $scheduleTime = $post['onceHours'].':'.$post['onceMinutes'];
                        $scheduleType = 'Once';
                        break;
                    case 'Daily':
                        $scheduleDate = $post['startDate'];
                        $scheduleTime = $post['dailyHours'].':'.$post['dailyMinutes'];
                        $scheduleType = 'Daily';
                        break;
                    case 'Weekly':
                        $day = '';
                        if(sizeof($post['day']) > 0) {
                            foreach ($post['day'] as $item) {
                                if($day != '') {
                                    $day .= ', '.$item;
                                } else {
                                    $day .= $item;
                                }
                            }
                        }
                        $scheduleDate = $day;
                        $scheduleTime = $post['weeklyHours'].':'.$post['weeklyMinutes'];
                        $scheduleType = 'Weekly';
                        break;
                    case 'Monthly':
                        $month = '';
                        if(sizeof($post['day']) > 0) {
                            foreach ($post['month'] as $item) {
                                if($month != '') {
                                    $month .= ', '.$item;
                                } else {
                                    $month .= $item;
                                }
                            }
                        }
                        $scheduleDate = $post['monthlyDate'];
                        $scheduleTime = $post['monthlyHours'].':'.$post['monthlyMinutes'];
                        $scheduleType = 'Monthly';
                        break;
                }
                $newData = array(
                    'reportTitle' => $post['reportTitle'],
                    'reportFormat' => $post['reportFormat'],
                    'emailID' => $post['emailID'],
                    'emailSubject' => $post['emailSubject'],
                    'emailMessage' => $post['emailMessage'],
                    'scheduleType' => $scheduleType,
                    'reportID' => $post['reportID'],
                    'scheduleDate' => $scheduleDate,
                    'scheduleTime' => $scheduleTime
                );

                $updateResult = $this->db->update($newData, array(
                    "ID" => $id
                ));
                wp_redirect( $this->_getPath() );
                exit();
            } else {
                $info = $this->amReportSettingData->setInfoToSettingScheduleType();
                $allReport = $this->amReportTable->query(array());

                $this->model['allReport'] = $allReport;
                $this->model['reportFormat'] = $info['Format'];
                $this->model['Month'] = $info['Month'];
                $this->model['Week'] = $info['Week'];
                $this->model['Date'] = $info['Date'];
                $this->model['Time'] = $info['Time'];
                if(isset($id)){
                    $scheduleItem = $this->db->query(array(
                        'filter' => array(
                            array( "ID = ".$id, '')
                        )
                    ))[0];
                    $this->model['scheduleItem'] = $scheduleItem;
                }
            }
        }

        public function del_action($req, $post) {
            $id = $req['scheduleID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }
    }
}