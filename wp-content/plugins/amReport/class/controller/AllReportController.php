<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 21/04/17
 * Time: 3:27 PM
 */
if(!class_exists('AllReportController')) {
    import('theme.package.Abstracts.AbstractController');
    import('plugin.amReport.class.factory.AMReportManagementFactory');
    class AllReportController extends AbstractController
    {
        protected $amReportSetting = array();
        protected function init()
        {
            $this->db = new AMReportManagementFactory();
            using('plugin.amReport.class.business.AMReportSettingData');
            $this->amReportSetting = new AMReportSettingData();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'All Report/list',
                'execute' => 'All Report/execute'
            );
        }

        private function get_all() {
            $result = $this->db->query(array(
                'order' => 'reportGroup ASC'
            ));
            return $result;
        }

        public function list_action(){
            $allReportList = $this->get_all();
            $this->model['allReportList'] = $allReportList;
        }

        public function execute_action() {

        }
    }
}