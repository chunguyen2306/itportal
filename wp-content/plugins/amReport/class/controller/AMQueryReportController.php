<?php
if(!class_exists('AMQueryReportController')) {

    //import your package here
    import('theme.package.Abstracts.AbstractController');
    import('plugin.amReport.class.factory.AMReportManagementFactory');

    class AMQueryReportController extends AbstractController
    {
        protected $amReportBusiness;
        protected function init()
        {
            $this->db = new AMReportManagementFactory();
            using('plugin.amReport.class.business.AMReportBusiness');
            $this->amReportBusiness = new AMReportBusiness();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'new' => 'Query Reports/addnew',
                'list' => 'Query Reports/list',
                'update' => 'Query Reports/addnew',
                'execute' => 'Query Reports/execute'
            );
        }

        //TODO: Return Query Result Data
        public function getResult($quertStr, $startIndex, $pageLength) {
            global $wpdb;
            $quertStr .= ' LIMIT '.$pageLength.' OFFSET '.$startIndex;
            $result = $wpdb->get_results($quertStr, ARRAY_A);
            return $result;
        }

        //TODO: Return Group of Report.
        public function getGroupOfReport() {
            $groupReport = $this->db->query(array(
                'select' => 'distinct reportGroup'
            ));
            return $groupReport;
        }

        public function get_all() {
            $reportQueryList = $this->db->query(array(
                'filter' => array(
                    array('reportType = "Query Report"','')
                )
            ));
            $this->model['reportQueryList'] = $reportQueryList;
        }

        public function list_action(){
            $this->get_all();
        }

        public function execute_action($req, $post) {
            global $wpdb;
            $action = $post['btnAction'];
            $id = $req['ID'];
            $queryItem = $this->db->getOne(array(
                'filter' => array(
                    array( 'ID = '.$id, '')
                )
            ));
            if($queryItem != null){
                $queryStr = $queryItem->reportData;
                if($req['p'] == null) {
                    $pageIndex = 1;
                } else {
                    $pageIndex = $req['p'];
                }
                $wpdb->get_results($queryStr);
                $total = $wpdb->num_rows;
                $pageLength = 20;
                $startIndex = ($pageIndex-1) * $pageLength;

                $pageCount = ceil($total/$pageLength);
                $this->model['pageCount'] = $pageCount;

                if($pageIndex <= 1) {
                    $previousIndex = 1;
                } else {
                    $previousIndex = $pageIndex - 1;
                }
                $this->model['previousIndex'] = $previousIndex;
                if($pageIndex >= $pageCount) {
                    $nextIndex = $pageCount;
                } else {
                    $nextIndex = $pageIndex + 1;
                }
                $this->model['nextIndex'] = $nextIndex;

                $result = $this->getResult($queryStr, $startIndex, $pageLength);
                $resultHeader = $this->amReportBusiness->getResultHeader($queryStr, $result);
                if(sizeof($result) > 0){
                    $this->model['colsHeader'] = $resultHeader;
                }
                $this->model['rows'] = $result;
                if($action == 'Excel' || $action == 'PDF') {
                    $this->amReportBusiness->exportReportToFormat($queryStr, $action);
                }
            }
        }

        public function new_action($req, $post){
            global $userDomain;
            $reportGroup = $this->getGroupOfReport();
            $this->model['reportGroup'] = $reportGroup;

            if($userDomain == null) $userDomain = 'Undefined';
            $title = $post['reportTitle'];
            if($title == null) $title = 'Undefined';
            $reportQuery = str_replace(array('\"', "\'"), '"', $post['reportData']);
            $time = current_time( 'D, F jS, Y \a\t g:i a');
            $newData = array(
                'reportTitle' => $title,
                'reportType' => $post['reportType'],
                'reportGroup' => $post['reportGroup'],
                'reportData' => $reportQuery,
                'Description' => $post['reportDescription'],
                'createTime' => $time,
                'createBy' => $userDomain,
            );

            $action = $post['btnAction'];
            if($action == 'Add new'){
                $this->db->insert($newData);
                wp_redirect( $this->_getPath() );
                exit();
            } else if($action == 'Reset'){
                $post['reportTitle'] = '';
                $post['reportGroup'] = '';
                $post['reportType'] = '';
                $post['reportDescription'] = '';
            }
        }

        public function update_action($req, $post){
            global $userDomain;
            $reportGroup = $this->getGroupOfReport();
            $this->model['reportGroup'] = $reportGroup;
            if($userDomain == null) $userDomain = 'Undefined';
            $title = $post['reportTitle'];
            if($title == null) $title = 'Undefined';
            $time = current_time( 'D, F jS, Y \a\t g:i a');
            $id = $req['id'];
            $action = $post['btnAction'];
            $this->_model('isUpdate',true);

            if($action == 'Update'){
                $reportQuery = str_replace(array('\"'), '"', $post['reportData']);
                $newData = array(
                    'ID' => $id,
                    'reportTitle' => $title,
                    'reportType' => $post['reportType'],
                    'reportGroup' => $post['reportGroup'],
                    'reportData' => $reportQuery,
                    'Description' => $post['reportDescription'],
                    'createTime' => $time,
                    'createBy' => $userDomain
                );
                $updateResult = $this->db->update($newData, array(
                    "ID" => $id
                ));
                wp_redirect( $this->_getPath() );
                exit();
            } else {
                if(isset($id)){
                    $reportItem = $this->db->query(array(
                        'filter' => array(
                            array( "ID = ".$id, '')
                        )
                    ))[0];
                    $this->model['reportItem'] = $reportItem;
                }
            }
        }

        public function del_action($req, $post){
            $id = $req['reportID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}