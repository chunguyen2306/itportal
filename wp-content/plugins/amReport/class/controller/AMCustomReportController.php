<?php
if(!class_exists('AMCustomReportController')) {

    //import your package here
    import('theme.package.Abstracts.AbstractController');
    import('plugin.amReport.class.factory.AMReportManagementFactory');

    class AMCustomReportController extends AbstractController
    {
        protected $amReportBusiness;
        protected $amReportSetting;

        protected function init()
        {
            $this->db = new AMReportManagementFactory();
            using('plugin.amReport.class.business.AMReportBusiness');
            using('plugin.amReport.class.business.AMReportSettingData');
            $this->amReportBusiness = new AMReportBusiness();
            $this->amReportSetting = new AMReportSettingData();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'new' => 'Custom Report/addnew',
                'list' => 'Custom Report/list',
                'update' => 'Custom Report/addnew',
                'execute' => 'Custom Report/execute',
                'test' => 'Custom Report/test'
            );
        }

        //TODO: Return Result Data of Query Statements
        public function getResult($quertStr, $startIndex, $pageLength) {
            global $wpdb;
            $quertStr .= ' LIMIT '.$pageLength.' OFFSET '.$startIndex;
            $result = $wpdb->get_results($quertStr, ARRAY_A);
            return $result;
        }

        //TODO: Return Group of Report.
        public function getGroupOfReport() {
            $groupReport = $this->db->query(array(
                'select' => 'distinct reportGroup'
            ));
            return $groupReport;
        }

        //TODO: Get Data for Update
        public function splitAndGetValueInString ($strInput, $option) {
            $result = array();
            if($option === 'filter') {
                $tempStr = str_replace(' AND ', ' AND, ', $strInput);
                $tempStr = str_replace(' OR ', ' OR, ', $tempStr);
                $listFilter = explode(', ', $tempStr);

                foreach ($listFilter as $item) {
                    //TODO: check '=', 'in', '!=',...
                    $postfix = '';
                    $dbColumnName = '';
                    $criteria = '';
                    $value = '';
                    if (strpos($item, ' = ') !== false) {
                        $itemSplit = explode(' = ', $item);
                        $criteria = 'is';
                        $splitValue = explode(' ', $itemSplit[1]);
                        $value = str_replace('"', '', $splitValue[0]);
                        $splitColumnname = explode('.', $itemSplit[0]);
                        $postfix = $splitColumnname[0];
                        $dbColumnName = $splitColumnname[1];
                    } else if (strpos($item, ' != ') !== false) {
                        $itemSplit = explode(' != ', $item);
                        $criteria = 'not is';
                        $splitValue = explode(' ', $itemSplit[1]);
                        $value = str_replace('"', '', $splitValue[0]);
                        $splitColumnname = explode('.', $itemSplit[0]);
                        $postfix = $splitColumnname[0];
                        $dbColumnName = $splitColumnname[1];
                    } else if (strpos($item, ' not in ') !== false) {           //TODO: Bugg =>Check ().
                        $itemSplit = explode(' not in ( ', $item);
                        $criteria = 'not contains';
                        $listValue = str_replace(' )', '', $itemSplit[1]);
                        $value = str_replace('"', '', $listValue);      //TODO: repair by preg_replace
                        $splitColumnname = explode('.', $itemSplit[0]);
                        $postfix = $splitColumnname[0];
                        $dbColumnName = $splitColumnname[1];
                    } else if (strpos($item, ' in ') !== false) {
                        error_log(print_r(strpos($item, ' not in '), true));
                        $itemSplit = explode(' in ( ', $item);
                        $criteria = "contains";
                        $listValue = str_replace(' )', '', $itemSplit[1]);
                        $value = str_replace('"', '', $listValue);
                        $splitColumnname = explode('.', $itemSplit[0]);
                        $postfix = $splitColumnname[0];
                        $dbColumnName = $splitColumnname[1];
                    }
                    if (strpos($item, 'AND') !== false) {
                        $match = 'AND';
                    } else if (strpos($item, 'OR') !== false) {
                        $match = 'OR';
                    } else {
                        $match = '';
                    }
                    $result[] = array(
                        'postfix' => $postfix,
                        'column' => $dbColumnName,
                        'criteria' => $criteria,
                        'value' => $value,
                        'match' => $match
                    );
                }
            } else if ($option === 'orderby') {
                $tempStr = explode(', ', $strInput);
                foreach ($tempStr as $item) {
                    $itemSplit = explode(' ', $item);
                    $value = $itemSplit[1];
                    $columnNameStr = explode('.', $itemSplit[0]);
                    $postfix = $columnNameStr[0];
                    $dbColumnName = $columnNameStr[1];
                    $result[] = array(
                        'postfix' => $postfix,
                        'column' => $dbColumnName,
                        'value' => $value
                    );
                }
            }
            return $result;
        }

        public function getRelatedTableName ($lstRelatedTable, $tableInfo) {
            $relatedTable = array();
            if(isset($lstRelatedTable) and sizeof($lstRelatedTable) > 0) {
                foreach ($lstRelatedTable as $key => $item) {
                    if($key != 0) {
                        $dbRelatedTableName = explode(' ', $item)[0];
                        $relatedTable[] = $tableInfo[$dbRelatedTableName];
                    } else {
                        continue;
                    }
                }
            }
            return $relatedTable;
        }

        public function getCheckedColumn ($lstCheckedColumn, $columnInfo) {
            $checkedColumn = array();
            if(isset($lstCheckedColumn) and sizeof($lstCheckedColumn) > 0) {
                foreach ($lstCheckedColumn as $key => $item) {
                    $dbCheckedColumnName = explode(' ', $item)[0];
                    if(array_key_exists($dbCheckedColumnName, $columnInfo)) {
                        $checkedColumn[] = $columnInfo[$dbCheckedColumnName];
                    }
                }
            }
            return $checkedColumn;
        }

        public function getFilterAndOrderByList ($listColumnData, $listTable, $columnInfo, $option) {
            $result = array();
            if(isset($listColumnData) and sizeof($listColumnData) > 0) {
                foreach ($listColumnData as $key => $item) {
                    foreach ($listTable as $displayTableName) {
                        if(array_key_exists($item['column'], $columnInfo[$displayTableName])) {
                            if($option === 'filter') {
                                if($item['postfix'] !== 'a') {
                                    $result[] = array(
                                        'columnSelected' => $columnInfo[$displayTableName][$item['column']],
                                        'criteriaSelected' => $item['criteria'],
                                        'columnValue' => $item['value'],
                                        'matchSelected' => $item['match']
                                    );
                                    break;
                                } else {
                                    $result[] = array(
                                        'columnSelected' => $columnInfo[$listTable[0]][$item['column']],
                                        'criteriaSelected' => $item['criteria'],
                                        'columnValue' => $item['value'],
                                        'matchSelected' => $item['match']
                                    );
                                    break;
                                }
                            } else if ($option === 'orderby') {
                                if($item['postfix'] !== 'a') {
                                    $result[] = array(
                                        'orderByColumn' => $columnInfo[$displayTableName][$item['column']],
                                        'orderByMethod' => $item['value']
                                    );
                                    break;
                                } else {
                                    $result[] = array(
                                        'orderByColumn' => $columnInfo[$listTable[0]][$item['column']],
                                        'orderByMethod' => $item['value']
                                    );
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return $result;
        }

        //TODO: Get all report and return to list action.
        public function get_all() {
            $reportCustomList = $this->db->query(array(
                'filter' => array(
                    array('reportType = "Custom Report"','')
                )
            ));
            $this->model['reportCustomList'] = $reportCustomList;
        }

        public function list_action(){
            $this->get_all();
        }

        public function new_action($req, $post){
            $data = $this->amReportBusiness->getReportDataUseToAddNew();
            $this->model['data'] = $data;
            $reportGroup = $this->getGroupOfReport();
            $this->model['reportGroup'] = $reportGroup;
            $action = $post['btnAction'];

            if($action == 'Add new'){
                $reportDataStr = str_replace('\"', '"', $post['reportData']);
                $customSetting = json_decode($reportDataStr, true);
                unset(
                    $customSetting['listTable'],
                    $customSetting['selectedTableColumn'],
                    $customSetting['listRelated'],
                    $customSetting['listColumnRelated'],
                    $customSetting['filterCriteria'],
                    $customSetting['filterMatch'],
                    $customSetting['orderByColumnName'],
                    $customSetting['orderMethod']
                );

                $reportData = $this->amReportBusiness->getReportDataByCustomSetting($customSetting, $data['column']);

                global $userDomain;
                if($userDomain == null) $userDomain = 'Undefined';
                $title = $post['reportTitle'];
                if($title == null) $title = 'Undefined';
                $time = current_time( 'D, F jS, Y \a\t g:i a');
                $newData = array(
                    'reportTitle' => $title,
                    'reportType' => $post['reportType'],
                    'reportGroup' => $post['reportGroup'],
                    'reportData' => $reportData,
                    'Description' => $post['reportDescription'],
                    'createTime' => $time,
                    'createBy' => $userDomain,
                );

                $this->db->insert($newData);
                wp_redirect( $this->_getPath() );
                exit();
            } else if($action == 'Reset'){
                $post['reportTitle'] = '';
                $post['reportType'] = '';
                $post['reportGroup'] = '';
                $post['reportDescription'] = '';
            }
        }

        public function update_action($req, $post){
            $data = $this->amReportBusiness->getReportDataUseToAddNew();
            $this->model['data'] = $data;
            $reportGroup = $this->getGroupOfReport();
            $this->model['reportGroup'] = $reportGroup;
            $tableInfo = $this->amReportBusiness->setTableNameInfo('dbTableNameKey');
            $columnInfo = $this->amReportSetting->setListColumn();
            $id = $req['id'];
            $action = $post['btnAction'];
            $this->_model('isUpdate',true);
            $updateData = array();

            if($action == 'Update'){
                $reportDataStr = str_replace('\"', '"', $post['reportData']);
                $customSetting = json_decode($reportDataStr, true);
                unset(
                    $customSetting['listTable'],
                    $customSetting['selectedTableColumn'],
                    $customSetting['listRelated'],
                    $customSetting['listColumnRelated'],
                    $customSetting['filterCriteria'],
                    $customSetting['filterMatch'],
                    $customSetting['orderByColumnName'],
                    $customSetting['orderMethod']
                );

                $reportData = $this->amReportBusiness->getReportDataByCustomSetting($customSetting, $data['column']);

                global $userDomain;
                if($userDomain == null) $userDomain = 'Undefined';
                $title = $post['reportTitle'];
                if($title == null) $title = 'Undefined';
                $time = current_time( 'D, F jS, Y \a\t g:i a');
                $newData = array(
                    'reportTitle' => $title,
                    'reportType' => $post['reportType'],
                    'reportGroup' => $post['reportGroup'],
                    'reportData' => $reportData,
                    'Description' => $post['reportDescription'],
                    'createTime' => $time,
                    'createBy' => $userDomain,
                );

                $updateResult = $this->db->update($newData, array(
                    "ID" => $id
                ));
                wp_redirect( $this->_getPath() );
                exit();
            } else {
                $reportData = '';
                if(isset($id)){
                    $reportItem = $this->db->query(array(
                        'filter' => array(
                            array( "ID = ".$id, '')
                        )
                    ))[0];
                    $this->model['reportItem'] = $reportItem;
                    $reportData = $reportItem->reportData;
                }

                $orderby = explode(' order by ', $reportData);
                $filter = explode(' where ', $orderby[0]);
                $join = explode(' join ', $filter[0]);
                $from = explode(' from ', $join[0]);
                $table = explode (' ', $from[1]);
                $select = explode('select ', $from[0]);

                $selectedTableDisplayName = $tableInfo[$table[0]];
                $updateData['selectTable'] = $selectedTableDisplayName;
                $checkedColumnStr = str_replace('a.', '', $select[1]);
                $lstCheckedColumn = explode(',', $checkedColumnStr);
                $updateData['checkedColumn'] = $this->getCheckedColumn($lstCheckedColumn, $columnInfo[$selectedTableDisplayName]);
                $updateData['selectRelate'] = $this->getRelatedTableName($join, $tableInfo);

                $selectRelatedColumn = array();
                foreach ($updateData['selectRelate'] as $relatedTableName) {
                    foreach ($columnInfo[$relatedTableName] as $dbColumnName => $displayColumnName) {
                        if(strpos($select[1], $dbColumnName) !== false) {
                            $selectRelatedColumn[] = $displayColumnName;
                        }
                    }
                }

                $updateData['selectRelatedColumn'] = $selectRelatedColumn;

                $filterListData = $this->splitAndGetValueInString($filter[1], 'filter');
                $orderbyListData = $this->splitAndGetValueInString($orderby[1], 'orderby');
                $listTableUpdate[] = $selectedTableDisplayName;
                $listTable = array_merge($listTableUpdate, $updateData['selectRelate']);

                $filterList = $this->getFilterAndOrderByList($filterListData, $listTable, $columnInfo, 'filter');
                $orderbyList = $this->getFilterAndOrderByList($orderbyListData, $listTable, $columnInfo, 'orderby');

                $updateData['filterList'] = $filterList;
                $filterColumnSelected = array();
                foreach ($filterList as $item) {
                    $filterColumnSelected[] = $item['columnSelected'];
                }

                $updateData['orderByList'] = $orderbyList;
                $orderByColumnSelected = array();
                foreach ($orderbyList as $item) {
                    $orderByColumnSelected[] = $item['orderByColumn'];
                }

                $listColumnSelected = array_merge($updateData['checkedColumn'], $selectRelatedColumn);
                $filterColumnName = array_diff($listColumnSelected, $filterColumnSelected);
                $orderByColumnName = array_diff($listColumnSelected, $orderByColumnSelected);

                $updateData['filterColumnNameUpdate'] = array_merge($filterColumnName, array());
                $updateData['orderByColumnNameUpdate'] = array_merge($orderByColumnName, array());

                $this->model['updateData'] = $updateData;
            }
        }

        public function execute_action($req, $post) {
            global $wpdb;
            $action = $post['btnAction'];
            $id = $req['id'];
            $queryItem = $this->db->getOne(array(
                'filter' => array(
                    array( 'ID = '.$id, '')
                )
            ));
            if($queryItem != null){
                $queryStr = $queryItem->reportData;
                if($req['p'] == null) {
                    $pageIndex = 1;
                } else {
                    $pageIndex = $req['p'];
                }
                $wpdb->get_results($queryStr);
                $total = $wpdb->num_rows;
                $pageLength = 20;
                $startIndex = ($pageIndex-1) * $pageLength;

                $pageCount = ceil($total/$pageLength);
                $this->model['pageCount'] = $pageCount;

                if($pageIndex <= 1) {
                    $previousIndex = 1;
                } else {
                    $previousIndex = $pageIndex - 1;
                }
                $this->model['previousIndex'] = $previousIndex;
                if($pageIndex >= $pageCount) {
                    $nextIndex = $pageCount;
                } else {
                    $nextIndex = $pageIndex + 1;
                }
                $this->model['nextIndex'] = $nextIndex;

                $result = $this->getResult($queryStr, $startIndex, $pageLength);
                $resultHeader = $this->amReportBusiness->getResultHeader($queryStr, $result);
                if(sizeof($result) > 0){
                    $this->model['colsHeader'] = $resultHeader;
                }
                $this->model['rows'] = $result;

                if($action == 'Excel' or $action == 'PDF') {
                    $this->amReportBusiness->exportReportToFormat($queryStr, $action);
                }
            }
        }

        //TODO: ALL TEST
        public function headerTest($result) {
            $header = array();
            if(isset($result) and sizeof($result) > 0) {
                foreach ($result[0] as $key => $value) {
                    $header[] = $key;
                }
            }
            return $header;
        }

        public function dataTest ($APIFuncName, $param) {
            $getReportDataUseAPI = using('plugin.amReport.class.business.AMReportBusiness');
            $result = $getReportDataUseAPI->getReportDataByHttpRequest($APIFuncName,$param);
            return $result;
        }

        public function test_action($req, $post) {
            $action = $post['btnAction'];
            $result = $this->dataTest('getColumnName','WebConfiguration');

            error_log(print_r($result, true));

//            $this->model['data'] = $result;
//            $resultHeader = $this->headerTest($result);

//            if($action == 'Excel') {
//                $export = using('plugin.amReport.class.includes.export.amExportReportManagement');
//                $export->setExportAndSendMailOption($resultHeader, $result, 'Export Excel File');
//                exit();
//            }
        }

        public function del_action($req, $post){
            $id = $req['reportID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }
    }
}