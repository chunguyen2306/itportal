<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 04/05/17
 * Time: 5:16 PM
 */
if(!class_exists('amReportScheduleManager')){
    //import your package here
    import('plugin.amReport.class.factory.AMScheduleReportFactory');
    import('plugin.amReport.class.factory.AMReportManagementFactory');

    class amReportScheduleManager {
        //Your code here
        public function getScheduleReport() {
            $db = new AMScheduleReportFactory();

            $result = $db->query(array(
                'join' => array(
                    array('LEFT JOIN', AMReportManagementFactory::$table_name.' b', 'a.reportID', 'b.ID')
                )
            ));
            return $result;
        }

        public function run_one($reportData, $emailID, $emailSubject, $emailMessage) {
            $objClass = using('plugin.amReport.class.includes.schedules.items.ScheduleSendMail');
            $objClass->run($reportData, $emailID, $emailSubject, $emailMessage);
        }

        public function run() {
            $scheduleItem = $this->getScheduleReport();

            $currentDay = date('d/m/Y');
            $currentTime = date('H:i');
            $currentDayOfWeek = date('l');
            $currentDayOfMonth = date('d');

            foreach ($scheduleItem as $item) {
                switch ($item->scheduleType) {
                    case 'Once':
                        $scheduleDate = $item->scheduleDate;
                        $scheduleTime = $item->scheduleTime;

                        if ($scheduleDate == $currentDay and $scheduleTime == $currentTime) {
                            $this->run_one($item->reportData, $item->emailID, $item->emailSubject, $item->emailMessage);
                        }
                        break;
                    case 'Daily':
                        $scheduleTime = $item->scheduleTime;
                        if ($scheduleTime == $currentTime) {
                            $this->run_one($item->reportData, $item->emailID, $item->emailSubject, $item->emailMessage);
                        }
                        break;
                    case 'Weekly':
                        $scheduleDate = explode(', ', $item->scheduleDate);
                        $scheduleTime = $item->scheduleTime;

                        if(in_array($currentDayOfWeek, $scheduleDate) and $scheduleTime == $currentTime) {
                            $this->run_one($item->reportData, $item->emailID, $item->emailSubject, $item->emailMessage);
                        }
                        break;
                    case 'Monthly':
                        $scheduleDate = $item->scheduleDate;
                        $scheduleTime = $item->scheduleTime;

                        if($scheduleDate == $currentDayOfMonth and $scheduleTime == $currentTime) {
                            $this->run_one($item->reportData, $item->emailID, $item->emailSubject, $item->emailMessage);
                        }
                        break;
                }
            }
        }
    }
}
return new amReportScheduleManager();