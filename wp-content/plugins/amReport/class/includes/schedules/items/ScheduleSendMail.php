<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 04/05/17
 * Time: 4:52 PM
 */
if(!class_exists('ScheduleSendMail')) {
    import('plugin.amReport.class.includes.export.Excel.PHPExcel');
    class ScheduleSendMail
    {
        public $amReportSettingData = array();
        public function __construct() {
            using('plugin.amReport.class.business.AMReportSettingData');
            $this->amReportSettingData = new AMReportSettingData();
            add_action( 'phpmailer_init', array($this, 'mailer_config'), 10, 1);
        }

        public function getResultHeader($result) {
            $Header = array();
            $columnNameInfo = $this->amReportSettingData->getColumnNameInfo();
            if(sizeof($result) > 0) {
                foreach ($result[0] as $key => $value) {
                    if (isset($result[0][$key])) {
                        if($columnNameInfo[$key] == null) {
                            array_push($Header, $key);
                        } else {
                            array_push($Header, $columnNameInfo[$key]);
                        }
                    }
                }
            }
            return $Header;
        }

        function mailer_config(PHPMailer $mailer){
            $mailer->IsSMTP();
            $mailer->Host = "smtp.gmail.com";
            $mailer->Port = 465;
            $mailer->SMTPDebug = 0;
            $mailer->CharSet  = "utf-8";
            $mailer->SMTPAuth = true;
            $mailer->SMTPSecure = 'ssl';
            $mailer->Port = 465; // or 587
            $mailer->IsHTML(true);
            $mailer->Username = "appdevmailtestsmtp@gmail.com";
            $mailer->Password = "x@X12345678";
            $mailer->SetFrom("appdevmailtestsmtp@gmail.com");
        }

        public function run($quertStr, $recipients, $subject, $message) {
            $multiple_recipients = explode(',', $recipients);
            $from = 'appdevmailtestsmtp@gmail.com';

            global $wpdb;
            $result = $wpdb->get_results($quertStr, ARRAY_A);

            $header = $this->getResultHeader($result);

            $attachmentFile = using('plugin.amReport.class.includes.export.amExportReportManagement');

            $filecontent = $attachmentFile->setExportAndSendMailOption ($header, $result, 'Send to eMail');

            $filename = 'Report.xls';

            $content = chunk_split(base64_encode($filecontent));
            
            $uid = md5(uniqid(time()));

            $header = "From: MailTest <".$from.">\r\n";
            $header .= "Reply-To: ".$from."\r\n";
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";

            $body = "--".$uid."\r\n";
            $body .= "Content-type:text/plain; charset=iso-8859-1\r\n";
            $body .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
            $body .= $message."\r\n\r\n";
            $body .= "--".$uid."\r\n";
            $body .= "Content-Type: application/csv;charset=utf-8; name=\"".$filename."\"\r\n";
            $body .= "Content-Transfer-Encoding: base64\r\n";
            $body .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
            $body .= $content."\r\n\r\n";
            $body .= "--".$uid."--";

            $result = wp_mail($multiple_recipients, $subject, $body, $header);
            if($result) {
                error_log("Gửi email thành công!");
            } else {
                error_log("Gửi email không thành công!");
            }
        }
    }
}
return new ScheduleSendMail();