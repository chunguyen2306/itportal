/**
 * Created by Duy Dao on 24/04/17.
 */

$(document).ready(function() {
    $("input[name='scheduleType']").click(function () {
        $('#once').css('display', ($(this).val() === 'Once') ? 'block':'none');
        $('#dayly').css('display', ($(this).val() === 'Daily') ? 'block':'none');
        $('#weekly').css('display', ($(this).val() === 'Weekly') ? 'block':'none');
        $('#monthly').css('display', ($(this).val() === 'Monthly') ? 'block':'none');
    });

    $('#onceDate').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-3d'
    });

    $('#startDate').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-3d'
    });

    $('#weekday').change(function () {
        if ($(this).prop('checked')) {
            $('.DayofWeek').prop('checked', true);
        } else {
            $('.DayofWeek').prop('checked', false);
        }
    });

    $('#everyMonth').change(function () {
        if ($(this).prop('checked')) {
            $('.MonthofYear').prop('checked', true);
        } else {
            $('.MonthofYear').prop('checked', false);
        }
    });
});