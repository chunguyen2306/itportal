<?php
if(!class_exists('amExportReportManagement')) {
    //TODO: Import file to use in class:
    import('plugin.amReport.class.includes.export.Excel.PHPExcel');
    import('plugin.amReport.class.includes.export.FPDF.fpdf');

    class amExportReportManagement
    {
        //TODO: Check Content with Cell Width to use export PDF.
//        private function checkContentWithCellWidth($rowData, $cellWidth) {
//            if(isset($rowData) and sizeof($rowData) > 0) {
//                foreach ($rowData as $item) {
//                    if(strlen($item) > $cellWidth) {
//                        return true;
//                    } else {
//                        return false;
//                    }
//                }
//            } else return false;
//        }

        public function exportToPDF($header, $result)
        {
            $d = date('d_m_Y');

            $pdf = new FPDF();
            $pdf->AddPage();
            $pageWidth = $pdf->GetPageWidth();
            $cellWidth = ($pageWidth-20)/sizeof($header);

            if (sizeof($header) > 0) {
                $pdf->SetFont('Times', 'B', 11);
                foreach ($header as $item) {
                    $pdf->Cell($cellWidth, 8, $item, 1);
                }
            }
            if (sizeof($result) > 0) {
                $pdf->SetFont('Times', '', 10);
                foreach ($result as $row) {
                    $pdf->Ln();
                    $i = 0;
                    foreach ($row as $cellContent) {
//                        if($pdf->GetStringWidth($cellContent) > $cellWidth) {
//                            $pdf->MultiCell($cellWidth, 8, $cellContent, 1, 'L', false);
//                        } else {
//                            $pdf->Cell($cellWidth, 8, $cellContent, 1);
//                        }
                        $pdf->Cell($cellWidth, 8, $cellContent, 1);
                    }
                }
            }
            $pdf->Output('D', $d . '.pdf');
        }

        public function createExcelData($header, $result)
        {
//            $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
//            $cacheSettings = array( 'memoryCacheSize' => '16MB');
//            PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

            //TODO: Bước 1 - Khởi tạo đối tượng mới và xử lý
            $PHPExcel = new PHPExcel();

            $style = array(
                'font' => array(
                    'size' => 12,
                    'name' => 'Times New Roman'
                )
            );

            $PHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($style);

            //TODO: Bước 2 - Chọn sheet bắt đầu từ 0
            $PHPExcel->setActiveSheetIndex(0);

            //TODO: Bước 3 - Tạo tiêu đề cho sheet hiện tại
            $PHPExcel->getActiveSheet()->setTitle('Report');

            //TODO: Bước 4 - Tạo tiêu đề cho từng cell excel
            $index = 65;
            foreach ($header as $item) {
                $PHPExcel->getActiveSheet()->setCellValue(chr($index) . '1', $item);
                $index++;
            }

            $PHPExcel->getActiveSheet()->fromArray($result, NULL, 'A2');

            $first_letter = PHPExcel_Cell::stringFromColumnIndex(0);
            $last_letter = PHPExcel_Cell::stringFromColumnIndex(count($header));
            $header_range = "{$first_letter}1:{$last_letter}1";
            $PHPExcel->getActiveSheet()->getStyle($header_range)->getFont()->setBold(true);

            foreach (range($first_letter, $last_letter) as $columnID) {
                $PHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
            }

            return $PHPExcel;
        }

        public function setExportAndSendMailOption($header, $result, $option) {
            $fileContent = '';
            if(isset($result) and sizeof($result)>0) {
                switch ($option) {
                    case 'Export Excel File':
                        $d = date('d_m_Y');

                        //TODO: Tạo tên filename của file Excel chuẩn bị export.
                        $filename = 'Report in ' . $d . '.xls';

                        $phpExcel = $this->createExcelData($header, $result);
                        //TODO: Khởi tạo đối tượng Writer
                        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');

                        error_log(print_r(memory_get_usage() . "\n" , true));
                        error_log(print_r(memory_get_peak_usage() . "\n" . "\n" , true));

                        //TODO: Trả file về cho client download
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment; filename="' . $filename . '"');
                        header('Cache-Control: max-age=0');

                        if (isset($objWriter)) {
                            $objWriter->save('php://output');
                        }

                        break;
                    case 'Send to eMail':
                        $phpExcel = $this->createExcelData($header, $result);
                        //TODO: Khởi tạo đối tượng Writer
                        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');

                        ob_start();
                        $objWriter->save('php://output');
                        $fileContent = ob_get_contents();
                        ob_end_clean();
                        break;
                }
            }
            return $fileContent;
        }
    }
}
return new amExportReportManagement();