(function () {
    var viewModel = {
        listTable: ko.observableArray(data.tables),
        selectedTable: ko.observable(),

        selectTableEvent: function (model, event) {
            viewModel.listRelated(data.related[viewModel.selectedTable()]);
            viewModel.selectedTableColumn(data.column[viewModel.selectedTable()]);
            viewModel.selectedRelate([]);
            viewModel.checkedColumnOfSelectedTable([]);
            viewModel.selectedColumnRelated([]);
            viewModel.filterColumnName([]);
            viewModel.orderByColumnName([]);
        },

        selectedTableColumn: ko.observableArray([]),
        checkedColumnOfSelectedTable: ko.observableArray([]),
        listRelated: ko.observableArray([]),
        selectedRelate: ko.observableArray([]),

        selectRelateEvent: function (model, event) {
            viewModel.listColumnRelated([]);
            if(viewModel.selectedRelate().length > 0) {
                (viewModel.selectedRelate()).forEach(function (item) {
                    if(data.related[item].length > 0) {
                        (data.related[item]).forEach(function (value) {
                            if(value !== viewModel.selectedTable()) {
                                if(jQuery.inArray(value, viewModel.listRelated()) === -1) {
                                    viewModel.listRelated.push(value);
                                }
                            }
                        });
                    }
                    viewModel.listColumnRelated.push({ table: item, column: data.column[item]} );
                });
            }
        },

        listColumnRelated : ko.observableArray([]),
        selectedColumnRelated: ko.observableArray([]),

        selectColumnEvent: function (model, event) {
            viewModel.filterColumnName([]);
            viewModel.orderByColumnName([]);
            if(viewModel.checkedColumnOfSelectedTable().length > 0) {
                (viewModel.checkedColumnOfSelectedTable()).forEach(function (item) {
                    viewModel.filterColumnName.push(item);
                    viewModel.orderByColumnName.push(item);
                });
            }
            if(viewModel.selectedColumnRelated().length > 0) {
                (viewModel.selectedColumnRelated()).forEach(function (item) {
                    viewModel.filterColumnName.push(item);
                    viewModel.orderByColumnName.push(item);
                });
            }
        },

        filterColumnName : ko.observableArray([]),
        filterColumnSelected : ko.observable(),
        filterCriteria : ko.observableArray(['is','not is','contains','not contains']),
        filterCriteriaSelected : ko.observable(),
        filterMatch : ko.observableArray(['AND', 'OR']),
        filterMatchSelected : ko.observable(),
        filterValue : ko.observable(),

        myFilterList : ko.observableArray([]),

        addFilterList: function (model, event) {
            viewModel.myFilterList.push({
                columnSelected: viewModel.filterColumnSelected(),
                criteriaSelected: viewModel.filterCriteriaSelected(),
                columnValue: viewModel.filterValue(),
                matchSelected: viewModel.filterMatchSelected()
            });
            var index = viewModel.filterColumnName.indexOf(viewModel.filterColumnSelected());
            viewModel.filterColumnName.splice(index,1);
            viewModel.filterCriteriaSelected('');
            viewModel.filterValue('');
            viewModel.filterMatchSelected('');
        },
        removeFilterList: function (model, event) {
            var index = viewModel.myFilterList.indexOf(model);
            viewModel.filterColumnName.push(viewModel.myFilterList()[index].columnSelected);
            viewModel.myFilterList.splice(index,1);
        },

        orderByColumnName : ko.observableArray([]),
        orderByColumnSelected : ko.observable(),
        orderMethod: ko.observableArray(["ASC", "DESC"]),
        orderMethodSelected: ko.observable(),

        orderByFieldList: ko.observableArray([]),
        addOrderField: function (model, event) {
            viewModel.orderByFieldList.push({
                orderByColumn: viewModel.orderByColumnSelected(),
                orderByMethod: viewModel.orderMethodSelected()
            });
            var index = viewModel.orderByColumnName.indexOf(viewModel.orderByColumnSelected());
            viewModel.orderByColumnName.splice(index,1);
            viewModel.orderMethodSelected('');
        },
        removeOrderField: function (model, event) {
            var index = viewModel.orderByFieldList.indexOf(model);
            viewModel.orderByColumnName.push(viewModel.orderByFieldList()[index].orderByColumn);
            viewModel.orderByFieldList.splice(index,1);
        },
    };

    if(isUpdate) {
        viewModel.selectedTable(updateData.selectTable);
        viewModel.selectTableEvent();
        viewModel.checkedColumnOfSelectedTable(updateData.checkedColumn);
        viewModel.selectedRelate(updateData.selectRelate);
        viewModel.selectRelateEvent();
        viewModel.selectedColumnRelated(updateData.selectRelatedColumn);
        viewModel.selectColumnEvent();
        viewModel.filterColumnName(updateData.filterColumnNameUpdate);
        viewModel.myFilterList(updateData.filterList);
        viewModel.orderByColumnName(updateData.orderByColumnNameUpdate);
        viewModel.orderByFieldList(updateData.orderByList);
    }
    ko.applyBindings(viewModel);
})();