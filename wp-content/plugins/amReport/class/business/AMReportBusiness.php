<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 11/04/17
 * Time: 2:08 PM
 */
if(!class_exists('AMReportBusiness')) {
    import('theme.package.HttpRequest');
    class AMReportBusiness
    {
    //TODO: GET DATA USING API
        public function getReportDataByHttpRequest($option, $param) {
            $requestSetting = using('plugin.amReport.class.includes.APIReportDataSetting');
            $data = array();
            switch ($option) {
                case 'getTransferAsset':
                    $request = new HttpRequest(array(
                        'url' => $requestSetting['dburl'].$option,
                        'data' => array(
                            'fromDate' => $requestSetting['method'][$option]['fromDate'],
                            'toDate' => $requestSetting['method'][$option]['toDate']
                        )
                    ));
                    $response = $request->send();
                    $data = json_decode($response->reponseText, true);
                    break;
                case 'getDBTableName':
                    $request = new HttpRequest(array(
                        'url' => $requestSetting['dburl'].$option,
                        'data' => array(
                            'dbName' => $requestSetting['method'][$option]['dbName']
                        )
                    ));
                    $response = $request->send();
                    $data = json_decode($response->reponseText, true);
                    break;
                case 'getColumnName':
                    $request = new HttpRequest(array(
                        'url' => $requestSetting['dburl'].$option,
                        'data' => array(
                            'tableName' => $param
                        )
                    ));
                    $response = $request->send();
                    $data = json_decode($response->reponseText, true);
                    break;
            }
            return $data;
        }

    //TODO: COMMON TO PROCESS

        //TODO: Run Export
        public function exportReportToFormat ($quertStr, $format) {
            global $wpdb;
            $result = $wpdb->get_results($quertStr, ARRAY_A);
            $header = $this->getResultHeader($quertStr, $result);
            $export = using('plugin.amReport.class.includes.export.amExportReportManagement');
            switch ($format) {
                case 'Excel':
                    $export->setExportAndSendMailOption ($header, $result, 'Export Excel File');
                    break;
                case 'PDF':
                    $export->exportToPDF ($header, $result);
                    break;
            }
            exit();
        }

        //TODO: Return Result Header:
        public function getResultHeader($queryStr, $result) {
            $amReportSetting = using('plugin.amReport.class.business.AMReportSettingData');
            $columnNameInfo = $amReportSetting->setListColumn();
            $listTable = $this->setTableNameInfo('listDBTable');
            $displayTableNameInfo = $this->setTableNameInfo('dbTableNameKey');
            $queryArray = explode(' ', $queryStr);
            $tempTable = array();
            $Header = array();

            foreach ($listTable as $item) {
                if(in_array($item, $queryArray)) {
                    $tempTable[] = $displayTableNameInfo[$item];
                }
            }

            if(sizeof($result) > 0) {
                foreach ($result[0] as $key => $value) {
                    $flag = false;
                    foreach ($tempTable as $tableDisplayName) {
                        if($columnNameInfo[$tableDisplayName] != null) {
                            if (array_key_exists($key, $columnNameInfo[$tableDisplayName])) {
                                if ($columnNameInfo[$tableDisplayName][$key] != null) {
                                    array_push($Header, $columnNameInfo[$tableDisplayName][$key]);
                                    $flag = true;
                                    break;
                                }
                            }
                        } else {
                            break;
                        }
                    }
                    if(!$flag) {
                        array_push($Header, $key);
                    }
                }
            }
            return $Header;
        }

    //TODO: PREPARE DATA TO CREATE REPORT.

        private function setDBTableName() {
            //array('Table Display Name' => 'DB Table Name')
            global $wpdb;
            $mytables = $wpdb->get_results("SHOW TABLES");
            $tableTemp = array();
            foreach ($mytables as $mytable)
            {
                if(strpos($mytable->Tables_in_wordpress, "am_") !== false) {
                    $tableTemp[] = $mytable->Tables_in_wordpress;
                }
            }

            $tables = array();
            foreach ($tableTemp as $key => $item) {
                $value = $item;
                $item = str_replace('am_', '', $item);
                $item = str_replace('_', ' ', $item);
                if($value !== 'am_report' and $value !== 'am_schedule_report') {
                    $tables[] = array('dbTableName' => $value, 'tableDisplayName' => ucwords($item));
                }
            }

            return $tables;
        }

        public function setTableNameInfo ($option) {
            $table = $this->setDBTableName();
            $tableInfo = array();
            switch ($option) {
                case 'dbTableNameKey':
                    if(isset($table) and sizeof($table) > 0) {
                        foreach ($table as $item) {
                            $tableInfo[$item['dbTableName']] = $item['tableDisplayName'];
                        }
                    }
                    break;
                case 'tableDisplayNameKey':
                    if(isset($table) and sizeof($table) > 0) {
                        foreach ($table as $item) {
                            $tableInfo[$item['tableDisplayName']] = $item['dbTableName'];
                        }
                    }
                    break;
                case 'listDBTable':
                    if(isset($table) and sizeof($table) > 0) {
                        foreach ($table as $item) {
                            $tableInfo[] = $item['dbTableName'];
                        }
                    }
                    break;
            }

            return $tableInfo;
        }

        private function getColumnAvailable($tableDisplayName) {
            global $wpdb;
            using('plugin.amReport.class.business.AMReportSettingData');
            $amReportSettingData = new AMReportSettingData();
            $columnInfo = $amReportSettingData->setListColumn()[$tableDisplayName];
            $tableNameInfo = $this->setTableNameInfo('tableDisplayNameKey');
            $tableName = $tableNameInfo[$tableDisplayName];
            $column = $wpdb->get_col("DESC {$tableName}", 0);

            $tmp = array();
            $columnAvailable = array();
            foreach($column as $key => $value) {
                if(strpos($value, 'ID') !== false) {
                    $tmp[] = $value;
                }
            }
            $columnAvailableTemp = array_diff($column, $tmp);
            foreach ($columnAvailableTemp as $item) {
                if($columnInfo[$item] == '' or $columnInfo[$item] == null) {
                    unset($item);
                }
                else {
                    $columnAvailable[] = $columnInfo[$item];
                }
            }
            return $columnAvailable;
        }

        //TODO: Setting Data need to use.
        public function getReportDataUseToAddNew() {
            using('plugin.amReport.class.business.AMReportSettingData');
            $amReportSettingData = new AMReportSettingData();
            $data = array();
            $tableName = $this->setDBTableName();
            $data['tables'] = $tableName;
            $related = $amReportSettingData->setRelatedTable();
            $data['related'] = $related;
            $columnAvailable = array();
            foreach ($tableName as $key => $value) {
                $columnAvailable[$value['tableDisplayName']] = $this->getColumnAvailable($value['tableDisplayName']);
            }
            $data['column'] = $columnAvailable;

            return $data;
        }

    //TODO: CREATE QUERY STRING.

        //TODO: Create Join Query String for Related Table
        private function getJoinRelatedTable($firstPostfix, $firstDisplayTableName, $secondPostfix, $secondDisplayTableName) {
            using('plugin.amReport.class.business.AMReportSettingData');
            $amReportSettingData = new AMReportSettingData();
            $tableNameInfo = $this->setTableNameInfo('tableDisplayNameKey');
            $on = $amReportSettingData->setJoinOnSetting($firstPostfix, $secondPostfix);

            if($on[$firstDisplayTableName][$secondDisplayTableName] != null) {
                $secondDBTableName = $tableNameInfo[$secondDisplayTableName];
                $joinOnQuery = $secondDBTableName.' '.$secondPostfix.' on '.$on[$firstDisplayTableName][$secondDisplayTableName];
                return $joinOnQuery;
            } else {
                return '';
            }
        }

        //TODO: Create Select Query String for checking of Related Column
        private function createSelectRelatedColumnQuery ($postfixArray, $columnRelated) {
            using('plugin.amReport.class.business.AMReportSettingData');
            $amReportSettingData = new AMReportSettingData();
            $select = '';
            if(sizeof($postfixArray) > 0 and sizeof($columnRelated) > 0) {
                foreach ($postfixArray as $displayTableName => $postfix) {
                    $columnInfo = $amReportSettingData->setListColumn()[$displayTableName];
                    if(sizeof($columnInfo) > 0) {
                        foreach ($columnInfo as $dbColumnName => $displayColumnName) {
                            if(in_array($displayColumnName, $columnRelated)) {
                                if ($select == '') {
                                    //TODO: For the same column name.
                                    if($dbColumnName == 'Name' or $dbColumnName == 'Description' or $dbColumnName == 'Descriptions') {
                                        $select = $postfix.'.'. $dbColumnName . ' as "' . $displayColumnName . '"';
                                    } else {
                                        $select = $postfix.'.'.$dbColumnName;
                                    }
                                } else {
                                    //TODO: For the same column name.
                                    if($dbColumnName == 'Name' or $dbColumnName == 'Description' or $dbColumnName == 'Descriptions') {
                                        $select = $select . ','.$postfix.'.'.$dbColumnName . ' as "' . $displayColumnName . '"';
                                    } else {
                                        $select = $select . ','.$postfix.'.'.$dbColumnName;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $select;
        }

        //TODO: Return queryStr for reportData
        public function getReportDataByCustomSetting($customSetting, $column) {
            using('plugin.amReport.class.business.AMReportSettingData');
            $amReportSettingData = new AMReportSettingData();
            $displayTableName = $customSetting['selectedTable'];
            $tableNameInfo = $this->setTableNameInfo('tableDisplayNameKey');
            $queryStr = '';
            $select = '';
            $join = '';
            $filter = '';
            $orderby = '';

            if(isset($customSetting) and sizeof($customSetting) > 0) {
                $postfix = 97;
                if($displayTableName != null) {
                    $firstPostfix = chr($postfix);
                    $dbTableName = $tableNameInfo[$displayTableName];
                    $from = $dbTableName.' '.$firstPostfix;
                    $postfixArray = array();

                    if(sizeof($customSetting['selectedRelate']) > 0) {
                        $tmpArray = array();
                        foreach ($customSetting['selectedRelate'] as $item) {
                            $postfix++;
                            $postfixArray[$item] = chr($postfix);

                            //TODO: Create Join-On Query.
                            $joinQuery = $this->getJoinRelatedTable('a', $displayTableName, chr($postfix), $item);

                            if ($joinQuery == '') {
                                if (isset($tmpArray) and sizeof($tmpArray) > 0) {
                                    //check if only one to success.
                                    foreach ($tmpArray as $key => $value) {
                                        $joinQuery = $this->getJoinRelatedTable(chr($key), $value, chr($postfix), $item);
                                        if ($joinQuery != '') {
                                            $join = $join . ' join ' . $joinQuery;
                                            break;
                                        }
                                    }
                                    //if all not to success.
                                    if ($joinQuery == '') {
                                        $tmpArray[$postfix] = $item;
                                    }
                                }
                                $tmpArray[$postfix] = $item;
                            } else {
                                $join = $join . ' join ' . $joinQuery;
                                $tmpArray[$postfix] = $item;
                            }
                        }
                    } else {
                        $join = '';
                    }

                    //TODO: Create Select Column Query.
                    $columnInfo = $amReportSettingData->setListColumn()[$displayTableName];

                    if($customSetting['checkedColumnOfSelectedTable'] == null) {
                        $select = 'select * ';
                    } else {
                        foreach ($columnInfo as $key => $value) {
                            if (in_array($value, $customSetting['checkedColumnOfSelectedTable'])) {
                                if ($select == '') {
                                    //TODO: For the same column name.
                                    if($key == 'Name' or $key == 'Description' or $key == 'Descriptions') {
                                        $select = 'select a.' . $key . ' as "' . $value . '"';
                                    } else {
                                        $select = 'select a.' . $key;
                                    }
                                } else {
                                    //TODO: For the same column name.
                                    if($key == 'Name' or $key == 'Description' or $key == 'Descriptions') {
                                        $select = $select . ',a.' . $key . ' as "' . $value . '"';
                                    } else {
                                        $select = $select . ',a.' . $key;
                                    }
                                }
                            }
                        }
                        if($customSetting['selectedColumnRelated'] != null) {
                            $selectRelate = $this->createSelectRelatedColumnQuery($postfixArray, $customSetting['selectedColumnRelated']);
                            $select = $select .','. $selectRelate;
                        }
                    }

                    //TODO: Create Order By Query
                    if($customSetting['orderByFieldList'] != null) {
                        foreach($customSetting['orderByFieldList'] as $item) {
                            $dbColumnName = $amReportSettingData->getDBColumnName($displayTableName, $item['orderByColumn']);
                            if($dbColumnName == null) {
                                foreach ($postfixArray as $key => $postfix) {
                                    $dbColumnName = $amReportSettingData->getDBColumnName($key, $item['orderByColumn']);
                                    if($dbColumnName != null){
                                        if($orderby == '') {
                                            $orderby = $postfix.'.'.$dbColumnName.' '.$item['orderByMethod'];
                                        } else {
                                            $orderby = $orderby.', '.$postfix.'.'.$dbColumnName.' '.$item['orderByMethod'];
                                        }
                                    }
                                }
                            } else {
                                if($orderby == '') {
                                    $orderby = 'a.'.$dbColumnName.' '.$item['orderByMethod'];
                                } else {
                                    $orderby = $orderby.', a.'.$dbColumnName.' '.$item['orderByMethod'];
                                }
                            }
                        }
                    }

                    //TODO: Create Filter Query
                    if($customSetting['myFilterList'] != null) {
                        $criteria = array(
                            'is' => ' = ',
                            'not is' => ' != ',
                            'contains' => ' in ',
                            'not contains' => ' not in '
                        );

                        foreach ($customSetting['myFilterList'] as $item) {
                            $listFilterValue = '';

                            if($criteria[$item['criteriaSelected']] == ' in ' or $criteria[$item['criteriaSelected']] == ' not in ') {
                                $columnValue = explode(',', $item['columnValue']);
                                foreach ($columnValue as $value) {
                                    if($listFilterValue != '') {
                                        $listFilterValue = $listFilterValue.','.'"'.$value.'"';
                                    } else {
                                        $listFilterValue = '"'.$value.'"';
                                    }
                                }
                            } else {
                                $listFilterValue = '"'.$item['columnValue'].'"';
                            }

                            if(in_array($item['columnSelected'], $column[$displayTableName])) {
                                $filterColumnName = $amReportSettingData->getDBColumnName($displayTableName, $item['columnSelected']);

                                if($criteria[$item['criteriaSelected']] == ' in ' or $criteria[$item['criteriaSelected']] == ' not in ') {
                                    if($filter == '') {
                                        $filter = 'a.'.$filterColumnName . $criteria[$item['criteriaSelected']] . '( ' . $listFilterValue . ' )' . ' ' . $item['matchSelected'];
                                    } else {
                                        $filter = $filter.' a.'.$filterColumnName . $criteria[$item['criteriaSelected']] . '( ' . $listFilterValue . ' )' . ' ' . $item['matchSelected'];
                                    }
                                } else {
                                    if($filter == '') {
                                        $filter = 'a.'.$filterColumnName . $criteria[$item['criteriaSelected']] . $listFilterValue . ' ' . $item['matchSelected'];
                                    } else {
                                        $filter = $filter.' a'.$filterColumnName . $criteria[$item['criteriaSelected']] . $listFilterValue . ' ' . $item['matchSelected'];
                                    }
                                }
                            } else {
                                foreach ($postfixArray as $diplayRelatedTableName => $posfixName) {
                                    if(in_array($item['columnSelected'], $column[$diplayRelatedTableName])) {
                                        $filterColumnName = $amReportSettingData->getDBColumnName($diplayRelatedTableName, $item['columnSelected']);
                                        if($criteria[$item['criteriaSelected']] == ' in ' or $criteria[$item['criteriaSelected']] == ' not in ') {
                                            if($filter == '') {
                                                $filter = $posfixName.'.'.$filterColumnName . $criteria[$item['criteriaSelected']] . '( ' . $listFilterValue . ' )' . ' ' . $item['matchSelected'];
                                            } else {
                                                $filter = $filter.' '.$posfixName.'.'.$filterColumnName . $criteria[$item['criteriaSelected']] . '( ' . $listFilterValue . ' )' . ' ' . $item['matchSelected'];
                                            }
                                        } else {
                                            if($filter == '') {
                                                $filter = $posfixName.'.'.$filterColumnName . $criteria[$item['criteriaSelected']] . $listFilterValue . ' ' . $item['matchSelected'];
                                            } else {
                                                $filter = $filter.' '.$posfixName.'.'.$filterColumnName . $criteria[$item['criteriaSelected']] . $listFilterValue . ' ' . $item['matchSelected'];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if($filter == '' and $orderby == '') {
                        $queryStr = $select.' from '.$from.$join;
                    } else if ($filter == '' and $orderby != '') {
                        $queryStr = $select.' from '.$from.$join.' order by '.$orderby;
                    } else if ($filter != '' and $orderby == '') {
                        $queryStr = $select.' from '.$from.$join.' where '.$filter;
                    } else {
                        $queryStr = $select.' from '.$from.$join.' where '.$filter.' order by '.$orderby;
                    }
                }
            }
            return $queryStr;
        }
    }
}
return new AMReportBusiness();
?>