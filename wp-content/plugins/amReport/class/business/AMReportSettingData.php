<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 21/04/17
 * Time: 8:55 AM
 */
if(!class_exists('AMReportSettingData')) {
    class AMReportSettingData
    {
    //TODO: CUSTOM REPORT SETTING

        //TODO: Return Related Table Data
        public function setRelatedTable()
        {
            $related = array(
                'Default Asset' => array('Job Groups'),
                'Job Branch' => array('Job Groups'),
                'Job Groups' => array('Default Asset', 'Job Branch'),
                'Permission' => array('Role Permission'),
                'Product Model' => array('Product Type'),
                'Product Type' => array('Product Model'),
                'Role Permission' => array('Roles', 'Permission', 'User Role'),
                'Roles' => array('Role Permission', 'User Role'),
                'User Role' => array('Roles', 'Role Permission', 'Users'),
                'Users' => array('User Role')
            );

            return $related;
        }

        //TODO: Return Column Data (dbColumnName => displayColumnName)
        public function setListColumn()
        {
            $column = array(
                'Roles' => array(
                    'ID' => 'Role Code',
                    'Name' => 'Role Name',
                    'Description' => 'Role Description'
                ),
                'Permission' => array(
                    'ID' => 'Permission Code',
                    'Name' => 'Permission Name',
                    'Descriptions' => 'Permission Descriptions'
                ),
                'Role Permisison' => array(
                    'ID' => 'Role Permission Code',
                    'RoleID' => 'Role Code',
                    'PerID' => 'Permission Code'
                ),
                'User Role' => array(
                    'ID' => 'User Role Code',
                    'Domain' => 'User Domain',
                    'Name' => 'User Name',
                    'RoleID' => 'Role Code',
                    'empCode' => 'User Employee Code'
                ),
                'Users' => array(
                    'ID' => 'User Code',
                    'departmentCode' => 'Department Code',
                    'departmentLead' => 'Department Lead',
                    'departmentName' => 'Department Name',
                    'divisionCode' => 'Division Code',
                    'divisionLead' => 'Division Lead',
                    'divisionName' => 'Division Name',
                    'domainAccount' => 'Domain Account',
                    'empCode' => 'Employee Code',
                    'firstName' => 'First Name',
                    'lastName' => 'Last Name',
                    'fullName' => 'Full Name',
                    'gender' => 'Gender',
                    'jobTitle' => 'Job Title',
                    'location' => 'Location',
                    'phone' => 'Phone',
                    'seat' => 'Seat',
                    'teamLead' => 'Team Lead',
                    'teamName' => 'Team Name',
                    'workingEmail' => 'Working Email'
                ),
                'Default Asset' => array(
                    'ID' => 'Default Asset Code',
                    'JobGroupID' => 'Job Group Code',
                    'AssetType' => 'Asset Type',
                    'AssetModel' => 'Asset Model',
                    'Quantity' => 'Quantity'
                ),
                'Job Branch' => array(
                    'ID' => 'Job Branch Code',
                    'BranchName' => 'Branch Name',
                    'Description' => 'Job Branch Description'
                ),
                'Job Groups' => array(
                    'ID' => 'Job Group Code',
                    'GroupName' => 'Job Group Name',
                    'BranchID' => 'Job Branch Code',
                    'Description' => 'Job Group Description'
                ),
                'Product Model' => array(
                    'ID' => 'Product Model Code',
                    'Name' => 'Product Model Name',
                    'Type' => 'Product Model Type',
                    'DisplayName' => 'Display Name',
                    'ImageURL' => 'Image URL'
                ),
                'Product Type' => array(
                    'ID' => 'Product Type Code',
                    'Name' => 'Product Type Name',
                    'Description' => 'Product Type Description',
                    'DisplayName' => 'Display Name',
                    'IconURL' => 'Icon URL'
                )
            );
            return $column;
        }

        //TODO: Return dbColumnName use displayColumnName
        public function getDBColumnName($displayTableName, $columnDisplayName)
        {
            $column = $this->setListColumn();
            $listColumnInfo = $column[$displayTableName];
            $dbColumnName = '';
            foreach ($listColumnInfo as $dbColumn => $displayName) {
                if ($displayName === $columnDisplayName) {
                    $dbColumnName = $dbColumn;
                    break;
                }
            }
            return $dbColumnName;
        }

        //TODO: Return 'On' Setting Data for Join Statement
        public function setJoinOnSetting($firstPostfix, $secondPostfix)
        {
            $on = array(
                'Default Asset' => array(
                    'Job Groups' => $firstPostfix . '.JobGroupID = ' . $secondPostfix . '.ID'
                ),
                'Job Branch' => array(
                    'Job Groups' => $firstPostfix . '.ID = ' . $secondPostfix . '.BranchID'
                ),
                'Job Groups' => array(
                    'Default Asset' => $firstPostfix . '.ID = ' . $secondPostfix . '.JobGroupID',
                    'Job Branch' => $firstPostfix . '.BranchID = ' . $secondPostfix . '.ID'
                ),
                'Permission' => array(
                    'Role Permission' => $firstPostfix . '.ID = ' . $secondPostfix . '.PerID'
                ),
                'Product Model' => array(
                    'Product Type' => $firstPostfix . '.Type = ' . $secondPostfix . '.Name'
                ),
                'Product Type' => array(
                    'Product Model' => $firstPostfix . '.Name = ' . $secondPostfix . '.Type'
                ),
                'Role Permission' => array(
                    'Roles' => $firstPostfix . '.RoleID = ' . $secondPostfix . '.ID',
                    'Permission' => $firstPostfix . '.PerID = ' . $secondPostfix . '.ID',
                    'User Role' => $firstPostfix . '.RoleID = ' . $secondPostfix . '.RoleID'
                ),
                'Roles' => array(
                    'Role Permission' => $firstPostfix . '.ID = ' . $secondPostfix . '.RoleID',
                    'User Role' => $firstPostfix . '.ID = ' . $secondPostfix . '.RoleID'
                ),
                'User Role' => array(
                    'Roles' => $firstPostfix . '.RoleID = ' . $secondPostfix . '.ID',
                    'Role Permission' => $firstPostfix . '.RoleID = ' . $secondPostfix . '.RoleID',
                    'Users' => $firstPostfix . '.Domain = ' . $secondPostfix . '.domainAccount'
                ),
                'Users' => array(
                    'User Role' => $firstPostfix . '.domainAccount = ' . $secondPostfix . '.Domain'
                )
            );
            return $on;
        }

    //TODO: SCHEDULE REPORT SETTING

        //TODO: Return Schedule Type Setting
        public function setInfoToSettingScheduleType()
        {
            $info = array(
                'Date' => array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10',
                    '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
                    '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'),
                'Time' => array(
                    'Hours' => array('00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10',
                        '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'),
                    'Minutes' => array('00', '05', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55')
                ),
                'Week' => array('Chủ nhật', 'Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bảy'),
                'Format' => array('PDF', 'XLS')
            );
            return $info;
        }
    }
}
return new AMReportSettingData();
?>