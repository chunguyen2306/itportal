<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 24/04/17
 * Time: 11:15 AM
 */
if(!class_exists('AMScheduleReportFactory')) {
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMScheduleReportFactory extends AbstractDatabase {

        public static $table_name = 'am_schedule_report';

        public function tableInfo(){
            return array(
                'table_name' => AMScheduleReportFactory::$table_name,
                'table_columns' => array(
                    'ID' => 'Report Code',
                    'scheduleType' => 'Schedule Report Type',
                    'reportTitle' => 'Report Title',
                    'reportFormat' => 'Report Format',
                    'emailID' => 'To Email',
                    'emailSubject' => 'Email Subject',
                    'emailMessage' => 'Email Message'
                ),
                'table_columns_type' => array(
                    'ID' => 'int',
                    'scheduleType' => 'string',
                    'reportTitle' => 'string',
                    'reportFormat' => 'string',
                    'emailID' => 'string',
                    'emailSubject' => 'string',
                    'emailMessage' => 'string'
                )
            );
        }
    }
}