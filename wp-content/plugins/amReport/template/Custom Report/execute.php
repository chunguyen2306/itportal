<?php
$typeName = '&type=execute&id='.$_REQUEST['id'];
?>
<div class="wrap">
    <h1>Result of Custom Report
        <a href="<?php echo esc_url('?page='.$model['slug']); ?>" class="page-title-action" title="Back" >Back</a>
    </h1>
    <form class="" method="POST" action="<?php echo esc_url('?page='.$model['slug'].$typeName.'&noheader=true'); ?>">
        <button type="submit" class="btn btn-default pull-right" name="btnAction" value="Excel">
            <i class="fa fa-file-excel-o" style="font-size:28px;color:green"></i>
        </button>
        <button type="submit" class="btn btn-default pull-right" name="btnAction" value="PDF">
            <i class="fa fa-file-pdf-o" style="font-size:28px;color:red"></i>
        </button>
    </form>
    <br/>
    <table class="wp-list-table widefat fixed striped pages">
        <thead>
            <tr>
                <?php if(isset($model['colsHeader']) and sizeof($model['colsHeader']>0)) { ?>
                    <?php foreach ($model['colsHeader'] as $colHeader){ ?>
                        <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                            <a href="">
                                <span><?php echo $colHeader; ?></span>
                            </a>
                        </th>
                    <?php }} ?>
            </tr>
        </thead>
        <tbody>
            <?php if(isset($model['rows']) and sizeof($model['rows']>0)) { ?>
                <?php foreach ($model['rows'] as $row){ ?>
                <tr>
                    <?php foreach ($row as $colName => $colVal) { ?>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <span>
                            <?php echo $colVal; ?>
                        </span>
                    </td>
                    <?php } ?>
                </tr>
                <?php }} ?>
        </tbody>
    </table>
    <div class="text-center">
        <ul class="pagination">
            <?php
            echo '<li><a href="'.esc_url('?page=' . $model['slug']). $typeName .'">First</a></li>';

            echo '<li><a href="'.esc_url('?page=' . $model['slug'] . $typeName . '&p='. $model['previousIndex']).'">&laquo;</a></li>';

            $start = ($_REQUEST['p'] > 0) ? $_REQUEST['p'] : 1 ;
            $end = (($_REQUEST['p'] + 3) < $model['pageCount']) ? ($_REQUEST['p'] + 3) : $model['pageCount'] ;

            if($start > 1) {
                echo '<li><a href="' . esc_url('?page=' . $model['slug'] . $typeName . '&p=1') . '">1</a></li>';
                if($start > 2) echo '<li class="disabled"><span>...</span></li>';
            }

            for ($i = $start; $i <= $end; $i++) {
                $class = ($_REQUEST['p'] == $i) ? "active" : "";
                echo '<li class="'.$class.'"><a href="' . esc_url('?page=' . $model['slug'] . $typeName . '&p=' . $i) . '">'.$i.'</a></li>';
            }

            if($end < $model['pageCount']) {
                if($end < ($model['pageCount'] - 1)) echo '<li class="disabled"><span>...</span></li>';
                echo '<li><a href="' . esc_url('?page=' . $model['slug'] . $typeName . '&p=') . $model['pageCount'] . '">'.$model['pageCount'].'</a></li>';
            }

            echo '<li><a href="'.esc_url('?page=' . $model['slug'] . $typeName . '&p='. $model['nextIndex']).'">&raquo;</a></li>';
            echo '<li><a href="'.esc_url('?page=' . $model['slug'] . $typeName . '&p='. $model['pageCount']).'">Last</a></li>';
            ?>
        </ul>
    </div>
</div>