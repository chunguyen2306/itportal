<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 14/04/17
 * Time: 3:24 PM
 */
$typeName = '';
$title = '';
if(isset($model['isUpdate'])){
    $typeName = '&type=update&id='.$_REQUEST['id'];
    $title = 'Cập nhật thông tin của báo cáo';
} else {
    $typeName = '&type=new';
    $title = 'Tạo mới báo cáo';
}
importCSS('amCustomReportCSS', 'plugin.amReport.class.includes.amCustomReportCSS');
?>
<div class="wrap">
    <h1 style="padding-left: 15px;"><?php echo $title; ?><a href="<?php echo esc_url('?page='.$model['slug']); ?>" class="page-title-action" title="Cancel" >Cancel</a></h1>
    <br/>
    <div class="col-sm-12">
        <form class="form-group" method="post" action="<?php echo esc_url('?page='.$model['slug'].$typeName); ?>">
            <?php if(isset($model['isUpdate'])) { ?>
                <input type="hidden" class="form-control" name="reportID" value="<?php echo $model['reportItem']->ID; ?>"/>
            <?php } ?>

            <div class="form-group row">
                <label class="col-sm-3" for="reportTitle">Tiêu đề báo cáo <span id="star">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="reportTitle" value="<?php if(isset($model['isUpdate'])) { echo $model['reportItem']->reportTitle; } else { echo ''; } ?>" required/>
                </div>
            </div>

            <input type="hidden" class="form-control" name="reportType" value="<?php echo 'Custom Report'; ?>"/>

            <div class="form-group row">
                <label class="col-sm-3 " for="reportTitle">Nhóm báo cáo </label>
                <div class="col-sm-9">
                    <select id = "reportGroup" name="reportGroup" class="form-control" style="width: 100%; height: 36px; float: left;" onchange="this.nextElementSibling.value=this.value">
                        <option value="<?php echo $model['reportItem']->reportGroup;?>" <?php if(isset($model['isUpdate'])) { echo 'selected'; } ?>><?php if(isset($model['isUpdate'])) {echo $model['reportItem']->reportGroup;} ?></option>
                        <?php foreach ($model['reportGroup'] as $item) {
                            if($item->reportGroup !== $model['reportItem']->reportGroup) {
                                ?>
                                <option value="<?php echo $item->reportGroup;?>"><?php echo $item->reportGroup;?></option>
                            <?php }} ?>
                    </select>
                    <input
                            id="reportGroup"
                            name="reportGroup"
                            style="width: 96%; height: 31px; margin-left:3px; padding-left: 6px; margin-top: -34px; border: none; float: left;"
                            value="<?php if(isset($model['isUpdate'])) {echo $model['reportItem']->reportGroup;} else { echo ''; }?>"
                    />
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-3 " for="reportTitle">Thông tin về báo cáo</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="reportDescription" value="<?php if(isset($model['isUpdate'])) { echo $model['reportItem']->Description; } else { echo ''; } ?>"/>
                </div>
            </div>

            <div class="form-group row">
                <label for="reportData" style="margin-left: 15px;">Dữ liệu sử dụng trong báo cáo <span id="star">*</span> :</label>
            </div>

            <div class="form-group row">
                <label class="col-sm-3">Tên bảng: </label>
                <div class="col-sm-9">
                    <select class="form-control" style="height: 34px;" data-bind="
                      options: listTable,
                      value: selectedTable,
                      optionsText: 'tableDisplayName',
                      optionsValue: 'tableDisplayName',
                      optionsCaption: '--- Chọn tên bảng ---',
                      event: {
                            change: selectTableEvent
                      }
                      " required></select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="col-sm-12" id="relatedTable">
                        <label for="relatedTable">
                            Các bảng liên quan :
                        </label>
                    </div>
                    <div class="col-sm-2" data-bind="foreach: listRelated" style="width: 0px; padding-left: 0px;">
                        <label class="checkbox-inline" style="margin-left: 0px;">
                            <input type="checkbox" value=""
                                data-bind="checkedValue: $data,
                                    checked: $root.selectedRelate,
                                    event: {
                                        change: $root.selectRelateEvent
                                    }">
                        </label>
                    </div>
                    <div class="col-sm-10" style="padding-left: 5px;">
                        <div class="listRelateTable" style="margin-top: 10px;">
                            <select multiple class="form-control" id="listRelated"name="listRelated" style="height: 126px;" data-bind="
                                options: listRelated,
                                selectedOptions: selectedRelate,
                                event: {
                                    change: selectRelateEvent
                                }">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="listColumn">
                        <label for="selectedColumn" style="margin-bottom: 10px;">
                            Chọn các cột hiển thị trong báo cáo:
                        </label>
                        <div data-bind="foreach: selectedTableColumn">
                            <label class="checkbox-inline" style="width: 213px; margin-left: 0px;">
                                <input type="checkbox"
                                    data-bind="checkedValue: $data,
                                    checked: $parent.checkedColumnOfSelectedTable,
                                    event: {change: $root.selectColumnEvent}"
                                    >
                                <span data-bind="text: $data"></span>
                            </label>
                        </div>

                        <!-- ko foreach: listColumnRelated -->
                        <div class="forn-group row">
                            <div class="col-sm-12" data-bind="foreach: $data['column']">
                                <label class="checkbox-inline" style="width: 213px; margin-left: 0px;">
                                    <input type="checkbox" value=""
                                        data-bind="checkedValue: $data,
                                            checked: $root.selectedColumnRelated,
                                            event: {change: $root.selectColumnEvent}"
                                    >
                                    <span data-bind="text: $data"></span>
                                </label>
                            </div>
                        </div>
                        <!-- /ko -->
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-3">Thiết lập chọn lọc: </label>
                <div class="col-sm-9">
                    <table class="table">
                        <thead>
                        <tr>
                            <th class="col-sm-3">Tên cột</th>
                            <th class="col-sm-3">Tiêu chí</th>
                            <th class="col-sm-3">Giá trị</th>
                            <th class="col-sm-2">Và/Hoặc</th>
                            <th class="col-sm-1"></th>
                        </tr>
                        </thead>
                        <tbody>
                            <!-- ko foreach: {data: myFilterList, as: 'Filter'} -->
                                <tr>
                                    <td data-bind="text: Filter.columnSelected"></td>
                                    <td data-bind="text: Filter.criteriaSelected"></td>
                                    <td data-bind="text: Filter.columnValue"></td>
                                    <td data-bind="text: Filter.matchSelected"></td>
                                    <td>
                                        <button class="btn btn-flat danger" data-bind="click: $root.removeFilterList"><span class="glyphicon glyphicon-remove-circle"></span></button>
                                    </td>
                                </tr>
                            <!-- /ko -->
                            <tr>
                                <td>
                                    <select class="form-control" style="height: 34px;" data-bind="
                                        options: filterColumnName,
                                        value: filterColumnSelected,
                                        optionsText: $data,
                                        optionsValue: $data,
                                        optionsCaption: '--- Chọn tên cột ---',
                                        ">
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" style="height: 34px;" data-bind="
                                        options: filterCriteria,
                                        value: filterCriteriaSelected,
                                        optionsText: $data,
                                        optionsValue: $data,
                                        optionsCaption: '--- Chọn tiêu chí ---',
                                        ">
                                    </select>
                                </td>
                                <td>
                                    <input data-bind="value: filterValue" style="width: 100%; height: 34px;"/>
                                </td>
                                <td>
                                    <select class="form-control" style="height: 34px;" data-bind="
                                        options: filterMatch,
                                        value: filterMatchSelected,
                                        optionsText: $data,
                                        optionsValue: $data,
                                        optionsCaption: '--- Chọn ---',
                                        ">
                                    </select>
                                </td>
                                <td>
                                    <button class="btn btn-primary" data-bind="click: addFilterList"><span>Thêm chọn lọc</span></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="form-group row">

                <label class="col-sm-3">Sắp xếp thứ tự: </label>
                <div class="col-sm-9">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="col-sm-8">Tên cột</th>
                                <th class="col-sm-3">Phương thức</th>
                                <th class="col-sm-1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ko foreach: {data: orderByFieldList, as: 'Order'} -->
                            <tr>
                                <td data-bind="text: Order.orderByColumn"></td>
                                <td data-bind="text: Order.orderByMethod"></td>
                                <td>
                                    <button class="btn btn-flat danger" data-bind="click: $root.removeOrderField"><span class="glyphicon glyphicon-remove-circle"></span></button>
                                </td>
                            </tr>
                            <!-- /ko -->
                            <tr>
                                <td>
                                    <select class="form-control" style="height: 34px;" data-bind="
                                    options: orderByColumnName,
                                    value: orderByColumnSelected,
                                    optionsText: $data,
                                    optionsValue: $data,
                                    optionsCaption: '--- Chọn tên cột ---',
                                    ">
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" style="height: 34px;" data-bind="
                                    options: orderMethod,
                                    value: orderMethodSelected,
                                    optionsText: $data,
                                    optionsValue: $data,
                                    optionsCaption: '--- Chọn phương thức ---',
                                    ">
                                    </select>
                                </td>
                                <td>
                                    <button class="btn btn-primary" data-bind="click: addOrderField" style="margin-right: 0px;"><span>Thêm sắp xếp</span></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <input type="hidden" class="form-control" name="reportData" data-bind="value: ko.toJSON($root)"/>
            <div class="form-group row">
                <?php if(isset($model['isUpdate'])) { ?>
                    <input type="submit" class="btn btn-primary" name="btnAction" value="Update" />
                <?php } else { ?>
                    <input type="submit" class="btn btn-primary" style="margin-left: 15px;" name="btnAction" value="Add new"/>
                    <input type="submit" class="btn btn-primary" name="btnAction" value="Reset"/>
                <?php } ?>
            </div>
        </form>
    </div>
</div>
<script>
    var data = <?php echo json_encode($model['data']); ?>;

    <?php if(isset($model['updateData']) and sizeof($model['updateData']) > 0) {
       echo "var updateData = ".json_encode($model['updateData']).";";
    } ?>

    var isUpdate = <?php if(isset($model['isUpdate'])) { echo 'true'; } else { echo 'false'; } ?>;
    <?php importScript('amCustomReportScript','plugin.amReport.class.includes.amCustomReportScript');?>
</script>