<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 3/24/2017
 * Time: 4:21 PM
 */
?>
<div class="wrap col-sm-12">
    <h1>Danh sách lập lịch báo cáo <a href="<?php echo esc_url('?page='.$model['slug'].'&type=new'); ?>" class="page-title-action" title="New Schedule Report" >Tạo lập lịch báo cáo</a></h1>
    <br/>
    <table class="wp-list-table widefat fixed striped pages">
        <thead>
        <tr>
            <td id="cb" class="manage-column column-cb check-column">
                <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                <input id="cb-select-all-1" type="checkbox">
            </td>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Tiêu đề báo cáo</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Loại lập lịch</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Định dạng báo cáo</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Gửi tới Email</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(is_array($model['scheduleReportList']) && sizeof($model['scheduleReportList']) > 0) {
            foreach ($model['scheduleReportList'] as $scheduleItem) { ?>
                <tr>
                    <th id="cb" class="manage-column column-cb check-column">
                        <label class="screen-reader-text"
                               for="cb-select-<?php echo $scheduleItem->ID; ?>"><?php echo $scheduleItem->ID; ?></label>
                        <input id="cb-select-<?php echo $scheduleItem->ID; ?>" type="checkbox">
                    </th>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <a href="<?php echo '/wp-admin/admin.php?page='.$model['slug'].'&type=update&id='.$scheduleItem->ID; ?>">
                            <span><?php echo $scheduleItem->reportTitle; ?></span>
                        </a>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <span><?php
                            if($scheduleItem->scheduleType == 'Weekly') {
                                $day = explode(', ', $scheduleItem->scheduleDate);
                                if(sizeof($day) == 7) {
                                    echo $scheduleItem->scheduleType.' in everyday at '.$scheduleItem->scheduleTime;
                                } else {
                                    echo $scheduleItem->scheduleType.' in '.$scheduleItem->scheduleDate.' at '.$scheduleItem->scheduleTime;
                                }
                            } else {
                                echo $scheduleItem->scheduleType.' in '.$scheduleItem->scheduleDate.' at '.$scheduleItem->scheduleTime;
                            }
                        ?></span>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <span><?php echo $scheduleItem->reportFormat; ?></span>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <span><?php echo $scheduleItem->emailID; ?></span>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <form method="POST" action="<?php echo esc_url('?page=' . $model['slug'] . '&type=del'); ?>">
                            <input type="hidden" name="scheduleID" value="<?php echo $scheduleItem->ID; ?>"/>
                            <button type="submit" class="btn btn-flat danger"><span class="glyphicon glyphicon-trash"></span></button>
                        </form>
                    </td>
                </tr>
            <?php }
        }
        ?>
        </tbody>
    </table>
</div>
