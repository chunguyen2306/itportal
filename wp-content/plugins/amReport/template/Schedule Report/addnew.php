<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 3/24/2017
 * Time: 4:21 PM
 */
$typeName = '';
$title = '';
if(isset($model['isUpdate'])){
    $typeName = '&type=update&id='.$_REQUEST['id'];
    $title = 'Cập nhật lập lịch báo cáo ';
} else {
    $typeName = '&type=new';
    $title = 'Tạo mới lập lịch báo cáo ';
}
?>
<div class="wrap">
    <h1 style="padding-left: 15px;"><?php echo $title; ?><a href="<?php echo esc_url('?page='.$model['slug']); ?>" class="page-title-action" title="Cancel" >Cancel</a></h1>
    <br/>
    <div class="col-sm-10">
        <form class="form-group" method="post" action="<?php echo esc_url('?page='.$model['slug'].$typeName); ?>">
            <?php if(isset($model['isUpdate'])) { ?>
                <input type="hidden" class="form-control" name="scheduleID" value="<?php echo $model['scheduleItem']->ID; ?>"/>
                <input type="hidden" class="form-control" name="reportID" value="<?php echo $model['scheduleItem']->reportID; ?>"/>
            <?php } ?>

            <div class="form-group row">
                <label class="col-sm-2" for="reportTitle">Tiêu đề báo cáo <span style="color:red">*</span></label>
                <div class="col-sm-10">
                    <select  name="reportTitle" id="reportTitle" class="form-control" style="width: 100%; height: 35px">
                        <option value="<?php echo $model['scheduleItem']->reportTitle; ?>" <?php if(isset($model['isUpdate'])) { echo 'selected'; } ?>><?php if(isset($model['isUpdate'])) { echo $model['scheduleItem']->reportTitle; } else { echo '--- Chọn báo cáo ---'; } ?></option>
                        <?php if(isset($model['allReport']) and sizeof($model['allReport']) > 0) {
                            foreach ($model['allReport'] as $item) {
                                if($item->reportTitle != $model['scheduleItem']->reportTitle) {?>
                                    <option value="<?php echo $item->reportTitle; ?>"><?php echo $item->reportTitle; ?></option>
                        <?php }}}?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2" for="reportFormat">Định dạng báo cáo</label>
                <div class="col-sm-10">
                    <select  name="reportFormat" id="reportFormat" class="form-control" style="width: 100%; height: 35px">
                        <option value="<?php echo $model['scheduleItem']->reportFormat; ?>" <?php if(isset($model['isUpdate'])) { echo 'selected'; } ?>><?php if(isset($model['isUpdate'])) { echo $model['scheduleItem']->reportFormat; } else { echo '--- Chọn định dạng ---'; } ?></option>
                        <?php if(isset($model['reportFormat']) and sizeof($model['reportFormat']) > 0) {
                            foreach ($model['reportFormat'] as $item) {
                                if($item != $model['scheduleItem']->reportFormat) {?>
                                ?>
                                    <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
                            <?php }}} ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-12">Gửi báo cáo qua email</label>
            </div>

            <div class="form-group row">
                <p class="col-sm-2" for="emailID" style="padding-left: 40px;">Email người nhận <span style="color:red">*</span></p>
                <div class="col-sm-10">
                    <input class="form-control" type="text" placeholder="example@gmail.com" name="emailID" value="<?php if(isset($model['isUpdate'])) { echo $model['scheduleItem']->emailID; } else { echo ''; } ?>" required />
                </div>
            </div>

            <div class="form-group row">
                <p class="col-sm-2" for="emailSubject" style="padding-left: 40px;">Tiêu đề</p>
                <div class="col-sm-10">
                    <input class="form-control" type="text" name="emailSubject" value="<?php if(isset($model['isUpdate'])) { echo $model['scheduleItem']->emailSubject; } else { echo ''; } ?>" />
                </div>
            </div>

            <div class="form-group row">
                <p for="emailMessage" class="col-sm-2" style="padding-left: 40px;">Tin nhắn</p>
                <div class="col-sm-10">
                    <textarea class="form-control" type="text" name="emailMessage" style="min-height: 136px;"><?php if(isset($model['isUpdate'])) { echo $model['scheduleItem']->emailMessage; } else { echo ''; } ?></textarea>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-12" for="scheduleType">Loại báo cáo </label>
            </div>

            <div class="form-group row">
                <div class="col-sm-2">
                    <input id="Once" name="scheduleType" type="radio" value="Once" <?php if(isset($model['isUpdate'])) {
                        if($model['scheduleItem']->scheduleType === 'Once') {echo 'checked';}
                    }?>/>
                        <label for="Once" style="font-weight: normal; padding-left: 3px; padding-top: 6px;">Một lần</label><br/>
                    <input id="Daily" name="scheduleType" type="radio" value="Daily" <?php if(isset($model['isUpdate'])) {
                        if($model['scheduleItem']->scheduleType === 'Daily') {echo 'checked';}
                    }?>/>
                        <label for="Daily" style="font-weight: normal; padding-left: 3px; padding-top: 6px;">Hàng ngày</label><br />
                    <input id="Weekly" name="scheduleType" type="radio" value="Weekly" <?php if(isset($model['isUpdate'])) {
                        if($model['scheduleItem']->scheduleType === 'Weekly') {echo 'checked';}
                    }?>/>
                        <label for="Weekly" style="font-weight: normal; padding-left: 3px; padding-top: 6px;">Hàng tuần</label><br />
                    <input id="Monthly" name="scheduleType" type="radio" value="Monthly" <?php if(isset($model['isUpdate'])) {
                        if($model['scheduleItem']->scheduleType === 'Monthly') {echo 'checked';}
                    }?>/>
                        <label for="Monthly" style="font-weight: normal; padding-left: 3px; padding-top: 6px;">Hàng tháng</label><br />
                </div>

                <?php
                    if(isset($model['isUpdate'])) {
                        $hour = '';
                        $minute = '';
                        $time = explode(':', $model['scheduleItem']->scheduleTime);
                        $hour = $time[0];
                        $minute = $time[1];
                    }
                ?>

                <div class="col-sm-10 col" id="once" name="once" style="<?php if(isset($model['isUpdate'])) {
                    if($model['scheduleItem']->scheduleType === 'Once') {echo 'display: initial';} else { echo 'display: none';}
                } else {echo 'display: none';}?>">
                    <p style="font-weight: bold; font-size: inherit;">Tạo báo cáo một lần</p>
                    <div class="form-group row">
                        <div class="col-sm-2">Ngày : </div>
                        <div class="col-sm-10">
                            <input class="form-control" id="onceDate" name="onceDate" value="<?php if(isset($model['isUpdate'])) {echo $model['scheduleItem']->scheduleDate;} else {echo '';}?>" data-date-format="dd/mm/yyyy" style="width: 100%; height: 35px;">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">Thời gian chạy lập lịch : </div>
                        <div class="col-sm-10 row" style="padding-right: 0px;">
                            <div class="col-sm-2">Giờ: </div>
                            <div class="col-sm-4">
                                <select class="form-control" id="onceHours" name="onceHours" style="width: 100%; min-height: 35px;"  onfocus='this.size=6;' onblur='this.size=1;' onchange='this.size=1; this.blur();'">
                                <option value="<?php echo $hour;?>" <?php if(isset($model['isUpdate'])) { echo 'selected'; } ?>><?php if(isset($model['isUpdate'])) {echo $hour;} else { echo '--- Chọn giờ ---';} ?></option>
                                <?php foreach ($model['Time']['Hours'] as $item) {
                                    if($item !== $hour) {
                                    ?>
                                        <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
                                <?php }} ?>
                                </select>
                            </div>
                            <div class="col-sm-2">Phút: </div>
                            <div class="col-sm-4" style="padding-right: 0px;">
                                <select class="form-control" id="onceMinutes" name="onceMinutes" style="width: 100%; min-height: 35px;"  onfocus='this.size=6;' onblur='this.size=1;' onchange='this.size=1; this.blur();'">
                                <option value="<?php echo $minute;?>" <?php if(isset($model['isUpdate'])) { echo 'selected'; } ?>><?php if(isset($model['isUpdate'])) {echo $minute;} else { echo '--- Chọn phút ---';} ?></option>
                                <?php foreach ($model['Time']['Minutes'] as $item) {
                                    if($item !== $minute) {
                                    ?>
                                    <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
                                <?php }} ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-10 col" id="dayly" name="dayly" style="<?php if(isset($model['isUpdate'])) {
                    if($model['scheduleItem']->scheduleType === 'Daily') {echo 'display: initial';} else { echo 'display: none';}
                } else {echo 'display: none';}?>">
                    <p style="font-weight: bold; font-size: inherit;">Tạo báo cáo hàng ngày</p>
                    <div class="form-group row">
                        <div class="col-sm-2">Ngày bắt đầu : </div>
                        <div class="col-sm-10">
                            <input class="form-control" id="startDate" name="startDate" value="<?php if(isset($model['isUpdate'])) {echo $model['scheduleItem']->scheduleDate;} else {echo '';}?>" data-date-format="dd/mm/yyyy" style="width: 100%; height: 35px;">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">Thời gian chạy lập lịch : </div>
                        <div class="col-sm-10 row" style="padding-right: 0px;">
                            <div class="col-sm-2">Giờ: </div>
                            <div class="col-sm-4">
                                <select class="form-control" id="dailyHours" name="dailyHours" style="width: 100%; min-height: 35px;"  onfocus='this.size=6;' onblur='this.size=1;' onchange='this.size=1; this.blur();'">
                                <option value="<?php echo $hour;?>" <?php if(isset($model['isUpdate'])) { echo 'selected'; } ?>><?php if(isset($model['isUpdate'])) {echo $hour;} else { echo '--- Chọn giờ ---';} ?></option>
                                <?php foreach ($model['Time']['Hours'] as $item) {
                                    if($item !== $hour) {
                                        ?>
                                        <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
                                    <?php }} ?>
                                </select>
                            </div>
                            <div class="col-sm-2">Phút: </div>
                            <div class="col-sm-4" style="padding-right: 0px;">
                                <select class="form-control" id="dailyMinutes" name="dailyMinutes" style="width: 100%; min-height: 35px;"  onfocus='this.size=6;' onblur='this.size=1;' onchange='this.size=1; this.blur();'">
                                <option value="<?php echo $minute;?>" <?php if(isset($model['isUpdate'])) { echo 'selected'; } ?>><?php if(isset($model['isUpdate'])) {echo $minute;} else { echo '--- Chọn phút ---';} ?></option>
                                <?php foreach ($model['Time']['Minutes'] as $item) {
                                    if($item !== $minute) {
                                        ?>
                                        <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
                                    <?php }} ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-10 col" id="weekly" name="weekly" style="<?php if(isset($model['isUpdate'])) {
                    if($model['scheduleItem']->scheduleType === 'Weekly') {echo 'display: initial';} else { echo 'display: none';}
                } else {echo 'display: none';}?>">
                    <p style="font-weight: bold; font-size: inherit;">Tạo báo cáo hàng tuần</p>
                    <div class="form-group row">
                        <div class="col-sm-12" style="margin-bottom: 10px;">
                            <input type="checkbox" id="weekday"/>
                            <label for="weekday" style="font-weight: normal; margin-top: 3px; margin-bottom: 0px;">Mỗi ngày trong tuần : </label>
                        </div>
                        <div class="col-sm-12">
                            <?php
                                if(isset($model['isUpdate'])) {
                                    $day = explode(', ', $model['scheduleItem']->scheduleDate);
                                }
                            ?>
                            <?php foreach ($model['Week'] as $key => $item) { ?>
                                <div class="col-sm-3" style="padding-left: 0px; padding-right: 0px;">
                                    <input type="checkbox" id="<?php echo 'week'.$key; ?>" class="DayofWeek" name="day[]" value="<?php echo $item; ?>"
                                        <?php if(isset($model['isUpdate'])) {if(in_array($item, $day)) echo 'checked';} ?>
                                    />
                                    <label for="<?php echo 'week'.$key; ?>" style="font-weight: normal; margin-top: 3px; margin-bottom: 0px;"><?php echo $item; ?></label>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">Thời gian chạy lập lịch : </div>
                        <div class="col-sm-10 row" style="padding-right: 0px;">
                            <div class="col-sm-2">Giờ: </div>
                            <div class="col-sm-4">
                                <select class="form-control" id="weeklyHours" name="weeklyHours" style="width: 100%; min-height: 35px;"  onfocus='this.size=6;' onblur='this.size=1;' onchange='this.size=1; this.blur();'">
                                <option value="<?php echo $hour;?>" <?php if(isset($model['isUpdate'])) { echo 'selected'; } ?>><?php if(isset($model['isUpdate'])) {echo $hour;} else { echo '--- Chọn giờ ---';} ?></option>
                                <?php foreach ($model['Time']['Hours'] as $item) {
                                    if($item !== $hour) {
                                        ?>
                                        <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
                                    <?php }} ?>
                                </select>
                            </div>
                            <div class="col-sm-2">Phút: </div>
                            <div class="col-sm-4" style="padding-right: 0px;">
                                <select class="form-control" id="weeklyMinutes" name="weeklyMinutes" style="width: 100%; min-height: 35px;"  onfocus='this.size=6;' onblur='this.size=1;' onchange='this.size=1; this.blur();'">
                                <option value="<?php echo $minute;?>" <?php if(isset($model['isUpdate'])) { echo 'selected'; } ?>><?php if(isset($model['isUpdate'])) {echo $minute;} else { echo '--- Chọn phút ---';} ?></option>
                                <?php foreach ($model['Time']['Minutes'] as $item) {
                                    if($item !== $minute) {
                                        ?>
                                        <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
                                    <?php }} ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-10 col" id="monthly" name="monthly" style="<?php if(isset($model['isUpdate'])) {
                    if($model['scheduleItem']->scheduleType === 'Monthly') {echo 'display: initial';} else { echo 'display: none';}
                } else {echo 'display: none';}?>">
                    <p style="font-weight: bold; font-size: inherit;">Báo cáo hàng tháng</p>
                    <div class="form-group row">
                        <div class="col-sm-2">Chọn ngày : </div>
                        <div class="col-sm-10 row" style="padding-right: 0px; padding-left: 23px;">
                            <select class="form-control" id="monthlyDate" name="monthlyDate" style="width: 100%; min-height: 35px;"  onfocus='this.size=6;' onblur='this.size=1;' onchange='this.size=1; this.blur();'">
                            <option value="<?php echo $model['scheduleItem']->scheduleDate;?>" <?php if(isset($model['isUpdate'])) {echo 'selected';} ?>>
                                <?php if(isset($model['isUpdate'])) {echo $model['scheduleItem']->scheduleDate;} else {echo '--- Chọn ngày ---';} ?>
                            </option>
                            <?php foreach ($model['Date'] as $item) { ?>
                                <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">Thời gian chạy lập lịch : </div>
                        <div class="col-sm-10 row" style="padding-right: 0px;">
                            <div class="col-sm-2">Giờ: </div>
                            <div class="col-sm-4">
                                <select class="form-control" id="monthlyHours" name="monthlyHours" style="width: 100%; min-height: 35px;"  onfocus='this.size=6;' onblur='this.size=1;' onchange='this.size=1; this.blur();'">
                                <option value="<?php echo $hour;?>" <?php if(isset($model['isUpdate'])) { echo 'selected'; } ?>><?php if(isset($model['isUpdate'])) {echo $hour;} else { echo '--- Chọn giờ ---';} ?></option>
                                <?php foreach ($model['Time']['Hours'] as $item) {
                                    if($item !== $hour) {
                                        ?>
                                        <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
                                    <?php }} ?>
                                </select>
                            </div>
                            <div class="col-sm-2">Phút: </div>
                            <div class="col-sm-4" style="padding-right: 0px;">
                                <select class="form-control" id="monthlyMinutes" name="monthlyMinutes" style="width: 100%; min-height: 35px;"  onfocus='this.size=6;' onblur='this.size=1;' onchange='this.size=1; this.blur();'">
                                <option value="<?php echo $minute;?>" <?php if(isset($model['isUpdate'])) { echo 'selected'; } ?>><?php if(isset($model['isUpdate'])) {echo $minute;} else { echo '--- Chọn phút ---';} ?></option>
                                <?php foreach ($model['Time']['Minutes'] as $item) {
                                    if($item !== $minute) {
                                        ?>
                                        <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
                                    <?php }} ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="form-group row">
                <?php if(isset($model['isUpdate'])) { ?>
                    <input type="submit" class="button button-primary" name="btnAction" value="Update" />
                <?php } else { ?>
                    <input type="submit" class="button button-primary" name="btnAction" value="Add new"/>
                    <input type="submit" class="button button-primary" name="btnAction" value="Reset"/>
                <?php } ?>
            </div>

        </form>
    </div>
</div>
<script>
    <?php importScript('amScheduleScript', 'plugin.amReport.class.includes.amScheduleReport')?>;
</script>