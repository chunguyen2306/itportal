<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 3/24/2017
 * Time: 4:21 PM
 */

$typeName = '';
$title = '';
if(isset($model['isUpdate'])){
    $typeName = '&type=update&id='.$_REQUEST['id'];
    $title = 'Update Query Report';
} else {
    $typeName = '&type=new';
    $title = 'New Query Report ';
}
?>
<div class="wrap">
    <h1 style="padding-left: 15px;"><?php echo $title; ?><a href="<?php echo esc_url('?page='.$model['slug']); ?>" class="page-title-action" title="Cancel" >Cancel</a></h1>
    <br/>
    <div class="col-sm-8">
        <form class="form-group" method="post" action="<?php echo esc_url('?page='.$model['slug'].$typeName); ?>">
            <?php if(isset($model['isUpdate'])) { ?>
                <input type="hidden" class="form-control" name="reportID" value="<?php echo $model['reportItem']->ID; ?>"/>
            <?php } ?>

            <input type="hidden" class="form-control" name="reportType" value="<?php echo 'Query Report'; ?>"/>

            <div class="form-group row">
                <label class="col-sm-2" for="reportTitle">Report Title <span style="color:red">*</span></label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" placeholder="Untitled" name="reportTitle" value="<?php if(isset($model['isUpdate'])) { echo $model['reportItem']->reportTitle; } else { echo ''; } ?>" required />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2" for="reportGroup">Report Group <span style="color:red">*</span></label>
                <div class="col-sm-10">
                    <select id = "reportGroup" name="reportGroup" class="form-control" style="width: 100%; height: 36px; float: left;" onchange="this.nextElementSibling.value=this.value">
                        <option value="<?php echo $model['reportItem']->reportGroup;?>" <?php if(isset($model['isUpdate'])) { echo 'selected'; } ?>><?php if(isset($model['isUpdate'])) {echo $model['reportItem']->reportGroup;} ?></option>
                        <?php foreach ($model['reportGroup'] as $item) {
                            if($item->reportGroup !== $model['reportItem']->reportGroup) {
                            ?>
                            <option value="<?php echo $item->reportGroup;?>"><?php echo $item->reportGroup;?></option>
                        <?php }} ?>
                    </select>
                    <input
                        id="reportGroup"
                        name="reportGroup"
                        style="width: 96%; height: 31px; margin-left:3px; padding-left: 6px; margin-top: -34px; border: none; float: left;"
                        value="<?php if(isset($model['isUpdate'])) {echo $model['reportItem']->reportGroup;} else { echo ''; }?>"
                    />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2" for="reportDescription">Description</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" name="reportDescription" value="<?php if(isset($model['isUpdate'])) { echo $model['reportItem']->Description; } else { echo ''; } ?>"/>
                </div>
            </div>
            <div class="form-group row">
                <label for="reportData" class="col-sm-2">Query Statement <span style="color:red">*</span></label>
                <div class="col-sm-10">
                    <textarea class="form-control" type="text" name="reportData" style="min-height: 136px;" required><?php if(isset($model['isUpdate'])) { echo $model['reportItem']->reportData; } else { echo ''; } ?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <?php if(isset($model['isUpdate'])) { ?>
                    <input type="submit" class="button button-primary" name="btnAction" value="Update" />
                <?php } else { ?>
                    <input type="submit" class="button button-primary" name="btnAction" value="Add new"/>
                    <input type="submit" class="button button-primary" name="btnAction" value="Reset"/>
                <?php } ?>
            </div>
        </form>
    </div>
</div>