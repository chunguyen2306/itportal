<div class="wrap col-sm-12">
    <h1>Danh sách các báo cáo <a href="<?php echo esc_url('?page='.$model['slug'].'&type=new'); ?>" class="page-title-action" title="New Query Report" >Tạo báo cáo mới</a></h1>
    <br/>

    <table class="wp-list-table widefat fixed striped pages">
        <thead>
        <tr>
            <td id="cb" class="manage-column column-cb check-column">
                <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                <input id="cb-select-all-1" type="checkbox">
            </td>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Tiêu đề báo cáo</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Thông tin báo cáo</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Người tạo báo cáo</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Thời gian tạo báo cáo</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(is_array($model['reportQueryList']) && sizeof($model['reportQueryList']) > 0) {
            foreach ($model['reportQueryList'] as $reportItem) { ?>
                <tr>
                    <th id="cb" class="manage-column column-cb check-column">
                        <label class="screen-reader-text"
                               for="cb-select-<?php echo $reportItem->ID; ?>"><?php echo $reportItem->ID; ?></label>
                        <input id="cb-select-<?php echo $reportItem->ID; ?>" type="checkbox">
                    </th>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <a href="<?php echo '/wp-admin/admin.php?page='.$model['slug'].'&type=execute&ID='.$reportItem->ID; ?>">
                        <span><?php echo $reportItem->reportTitle; ?></span>
                        </a>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <span><?php echo $reportItem->Description; ?></span>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <span><?php echo $reportItem->createBy; ?></span>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <span><?php echo $reportItem->createTime; ?></span>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <a href="<?php echo '?page=' . $model['slug'] . '&type=update&id=' . $reportItem->ID; ?>" type="button" class="btn btn-flat success"><span class="glyphicon glyphicon-edit"></span></a>
                        <form method="POST" action="<?php echo esc_url('?page=' . $model['slug'] . '&type=del'); ?>">
                            <input type="hidden" name="reportID" value="<?php echo $reportItem->ID; ?>"/>
                            <button type="submit" class="btn btn-flat danger"><span class="glyphicon glyphicon-trash"></span></button>
                        </form>
                    </td>
                </tr>
            <?php }
        }
        ?>
        </tbody>
    </table>
</div>
