<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 21/04/17
 * Time: 3:37 PM
 */
?>

<div class="wrap col-sm-12">
    <h1>All of Reports </h1>
    <br/>

    <table class="wp-list-table widefat fixed striped pages">
        <thead>
        <tr>
            <td id="cb" class="manage-column column-cb check-column">
                <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                <input id="cb-select-all-1" type="checkbox">
            </td>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Report Title</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Report Group</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Descriptions</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Create By Users</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Time</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(is_array($model['allReportList']) && sizeof($model['allReportList']) > 0) {
            foreach ($model['allReportList'] as $allReportItem) { ?>
                <tr>
                    <th id="cb" class="manage-column column-cb check-column">
                        <label class="screen-reader-text"
                               for="cb-select-<?php echo $allReportItem->ID; ?>"><?php echo $allReportItem->ID; ?></label>
                        <input id="cb-select-<?php echo $allReportItem->ID; ?>" type="checkbox">
                    </th>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <a href="<?php echo '/wp-admin/admin.php?page='.$model['slug'].'&type=execute&id='.$allReportItem->ID; ?>">
                            <span><?php echo $allReportItem->reportTitle; ?></span>
                        </a>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <span><?php echo $allReportItem->reportGroup; ?></span>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <span><?php echo $allReportItem->Description; ?></span>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <span><?php echo $allReportItem->createBy; ?></span>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <span><?php echo $allReportItem->createTime; ?></span>
                    </td>
                </tr>
            <?php }
        }
        ?>
        </tbody>
    </table>
</div>