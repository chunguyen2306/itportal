<?php
/*
  Plugin Name: AM Report
  Plugin URI:
  Description: Chứa tất cả các chức năng liên quan đến Report.
  Version: 1.0
  Author: daopd
  Author URI:
  License:
  Text Domain: am-report
*/

if(!class_exists('AMReport')) {
    if ( ! function_exists( 'import' ) ) {
        require_once get_template_directory() . '/package/import.php';
    }
    import ('theme.package.Abstracts.AbstractPlugin');
    //Report Class Controller
    import('plugin.amReport.class.controller.AllReportController');
    import ('plugin.amReport.class.controller.AMCustomReportController');
    import ('plugin.amReport.class.controller.AMQueryReportController');
    import ('plugin.amReport.class.controller.AMScheduleReportController');

    class AMReport extends AbstractPlugin
    {
        public $db = array();
        public $model = array();

        public function __construct()
        {
            $this->ajaxName = 'amDefaultAsset';
            $this->pluginName = 'amDefaultAsset';
            $this->adminMenu = array(
                array(
                    'pageTitle' => 'AM Report',
                    'menuTitle' => 'AM Report',
                    'capability'=> 'manage_options',
                    'menuSlug' => 'am_default_asset',
                    'handler' => array(new AllReportController(array(
                        'slug' => 'am_all_report'
                    )), 'index'),
                    'icon' => 'dashicons-id-alt',
                    'child' => array(
                        array(
                            'pageTitle' => 'All Report',
                            'menuTitle' => 'All Report',
                            'capability'=> 'manage_options',
                            'menuSlug' => 'am_all_report',
                            'default' => true
                        ),
                        array(
                            'pageTitle' => 'Custom Report',
                            'menuTitle' => 'Custom Report',
                            'capability'=> 'manage_options',
                            'menuSlug' => 'am_custom_report',
                            'handler' => array(new AMCustomReportController(array(
                                'slug' => 'am_custom_report'
                            )), 'index')
                        ),
                        array(
                            'pageTitle' => 'Query Report',
                            'menuTitle' => 'Query Report',
                            'capability'=> 'manage_options',
                            'menuSlug' => 'am_query_report',
                            'handler' => array(new AMQueryReportController(array(
                                'slug' => 'am_query_report'
                            )), 'index')
                        ),
                        array(
                            'pageTitle' => 'Schedule Report',
                            'menuTitle' => 'Schedule Report',
                            'capability'=> 'manage_options',
                            'menuSlug' => 'am_schedule_report',
                            'handler' => array(new AMScheduleReportController(array(
                                'slug' => 'am_schedule_report'
                            )), 'index')
                        )
                    )
                )
            );
            parent::__construct();
        }

        protected function init_custom_hook()
        {
            // TODO: Implement init_custom_hook() method.
        }

        protected function custom_init()
        {

        }

        function add_admin_menu()
        {
            add_submenu_page(
                'am_all_report',
                'Custom Report',
                'Custom Report',
                'manage_options',
                'am_custom_report',
                array(new AMCustomReportController(array(
                    'slug' => 'am_custom_report'
                )), 'index')
            );

            add_submenu_page(
                'am_all_report',
                'Query Report',
                'Query Report',
                'manage_options',
                'am_query_report',
                array(new AMQueryReportController(array(
                    'slug' => 'am_query_report'
                )), 'index')
            );

            add_submenu_page(
                'am_all_report',
                'Schedule Report',
                'Schedule Report',
                'manage_options',
                'am_schedule_report',
                array(new AMScheduleReportController(array(
                    'slug' => 'am_schedule_report'
                )), 'index')
            );
        }
    }
    $amReport = new AMReport();
    $GLOBALS['amReport'] = $amReport;
    $amReportScheduleManager = using('plugin.amReport.class.includes.schedules.amReportScheduleManager');
    $GLOBALS['amReportScheduleManager'] = $amReportScheduleManager;

    add_action('schedule_send_mail_event', 'do_schedule');

    function do_schedule(){
        global $amReportScheduleManager;
        $amReportScheduleManager->run();
    }

    function add_cron_schedules( $schedules ) {
        // add a 'weekly' schedule to the existing set
        $schedules['minute'] = array(
            'interval' => 60,
            'display' => __('Once Minute')
        );
        return $schedules;
    }
    add_filter( 'cron_schedules',  'add_cron_schedules');

    wp_schedule_event(strtotime('2017-01-01 10:00:00'), 'minute', 'schedule_send_mail_event');

//    register_activation_hook(__FILE__, 'plugin_amreport_activation');
//
//    function plugin_amreport_activation() {
//        if (! wp_next_scheduled ( 'schedule_send_mail_event' )) {
//            wp_schedule_event(strtotime('2017-01-01 10:00:00'), 'minute', 'schedule_send_mail_event');
//        }
//    }
//
    register_deactivation_hook(__FILE__, 'plugin_amreport_deactivation');

    function plugin_amreport_deactivation() {
        wp_clear_scheduled_hook('schedule_send_mail_event');
    }
}