<?php
/*
  Plugin Name: AM Rewrite Manager
  Plugin URI:
  Description:
  Version: 1.0
  Author: tientm2
  Author URI:
  License:
  Text Domain: am-rewrite-manager
*/

if ( ! function_exists( 'import' ) ) {
    require_once get_template_directory() . '/package/import.php';
}

add_action('init', 'amRewriteApply');

function amRewriteApply(){
    $amRoute = using('theme.package.route');

    foreach ($amRoute as $routeItem){
        add_rewrite_rule(
            $routeItem['regex'],
            $routeItem['query'],
            'top' );
    }

    flush_rewrite_rules();
}