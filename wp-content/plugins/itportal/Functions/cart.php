<?php
function generateAssetPopUp() {
  echo '' ?>
<div component="popup" name="popReturnAsset" width="70%" height='70%'>
  <h2>
    <span data-bind="text: i18n('cart:popup-return-asset:left-side:title:lbl:text', [$root.full_name()])"></span>
    <button class="red close" data-bind="click: do_AddRetriveAsset">
      <i class="fa fa-close"></i>
    </button>
  </h2>
  <div class="left-side-popup left-step2">
    <div class="popup-content list-item-manage">
      <div>
        <button class="gray" data-bind='click: returnAllItemSelected,
                  text: i18n("cart:popup-return-asset:left-side:return-all-item:btn:text")'></button>
        <button class="gray" data-bind='click: fixAllItemSelected,
                  text: i18n("cart:popup-return-asset:left-side:fix-all-item:btn:text")'></button>
        <button class="gray" data-bind='click: loseAllItemSelected,
                  text: i18n("cart:popup-return-asset:left-side:lose-all-item:btn:text")'></button>
        <div class="search_control">
          <input type="search" id="txb_SearchAsset" data-bind="attr: {placeholder: i18n('cart:popup-return-asset:left-side:search:input:placeholder')}" />
          <button id="search_asset_bt">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
      <tbody>
        <table class="datatable no-control table-step1">
          <thead>
            <tr>
              <th>
                <input id="product_checked_all_id" name="product_checkbox" component="checkbox" class="nolabel onlycheck"
                  type="checkbox" data-bind="checked: isCheckAllPage, event: {change: $root.check_SelectAllInPage}">
              </th>
              <th>Loại sản phẩm</th>
              <th>Hình ảnh sản phẩm</th>
              <th>Thông tin sản phẩm</th>
              <th>Trạng thái sản phẩm</th>
              <th></th>
            </tr>
          </thead>

          <tbody data-bind="foreach: myAsset">
            <tr data-bind='attr:{ class: $root.revertItemRetrieList($index())?"active-background":""}'>
              <td>
                <input id="product_checkbox_id" name="product_checkbox" component="checkbox" class="nolabel onlycheck"
                  type="checkbox" data-bind="text: RESOURCENAME, checked: $root.revertItemRetrieList($index()), attr: {value: $index}, event: {change: $root.selectItem}">
              </td>
              <td>
                <strong data-bind="text: COMPONENTTYPENAME"></strong>
              </td>
              <td data-bind="text: COMPONENTTYPENAME"></td>
              <td data-bind="text: COMPONENTNAME"></td>
              <td>
                <span class='icon-action return' title="Trả sản phẩm" data-bind='click: $root.selectReturnItem'><i
                    data-bind='attr:{ class: $root.revertTypeRetrieList($index(),"return")?"fa fa-undo active-tr-retrieve btn-orange":"fa fa-undo"}'></i></span>
                <span class='icon-action fix' title="Báo hư hỏng" data-bind='click: $root.selectFixItem'><i data-bind='attr:{ class: $root.revertTypeRetrieList($index(),"fix")?"fa fa-wrench active-tr-retrieve btn-blue":"fa fa-wrench"}'></i></span>
                <span class='icon-action miss' title="Báo mất" data-bind='click: $root.selectMissItem'><i data-bind='attr:{ class: $root.revertTypeRetrieList($index(),"lose")?"fa fa-ban active-tr-retrieve btn-red":"fa fa-ban"}'></i></span>
              </td>
            </tr>
          </tbody>
        </table>
        <div class="popup-bottom">
          <button id="ok_bt" data-bind="click: AddItemActionList">Xác nhận</button>
        </div>
      </tbody>
      <!-- <div section="no-item">
          <p id="search_no_item" data-bind="text: i18n('cart:popup-return-asset:left-side:no-asset:lbl:text')"></p>
        </div> -->
      <div class="asset-popup-bottom-bar">
        <div>
          <button id="select_all_bt" class="gray" data-bind="click: $root.do_SelectAll">
            Chọn tất cả
          </button>
          <button id="remove_all_bt" class="gray" data-bind="click: $root.do_RemoveAll">
            Bỏ chọn tất cả
          </button>
        </div>
        <div component="paging" pageItemLimit="5" name="assetPaging"></div>
      </div>
      <!-- ko if: myAsset().length === 0 -->
      <div section="no-item">
        <p id="search_no_item" data-bind="text: i18n('cart:popup-return-asset:left-side:no-asset:lbl:text')"></p>
      </div>
      <!-- /ko -->
    </div>
  </div>
</div>
<?php
} 

function generateProductStep() {
  echo '' ?>
  <div data-bind='if: step() == 0' style='padding: 0 20px;'>
    <!-- top bar -->
    <div class="information-step-top-bar">
      <button class="btn-add-my-item blue" data-bind="click: do_ReturnAsset"><i class="fa fa-plus"></i> Thêm
        sản phẩm</button>
      <div class="search-bar">
        <div class="search_control">
          <input type="search" id="txbSearchItems" placeholder="Từ khoá tìm kiếm">
          <button id="searchBtn">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </div>
    <!-- data table -->
    <!-- ko if: ProductCart().length + OtherProductCart().length + listFixItem().length + listReturnItem().length + listLoseItem().length == 0 -->
    <div section="no-item">
      <img id="empty-cart" src="<?php echo get_template_directory_uri(); ?>/img/empty-cart/empty_cart_1.png">
      <p data-bind="text: i18n('cart:left-side:table:empty-cart:lbl:text')"></p>
    </div>
    <!-- /ko -->
    <!-- ko ifnot: ProductCart().length + OtherProductCart().length + listFixItem().length + listReturnItem().length + listLoseItem().length == 0 -->
    <table class="table-step1">
      <thead>
        <tr>
          <th>Loại sản phẩm</th>
          <th>Hình ảnh</th>
          <th>Thông tin</th>
          <th>Số lượng</th>
          <th>Trạng thái</th>
          <th></th>
        </tr>
      </thead>
      <tbody data-bind="foreach: { data: ProductCart, afterRender: do_InitComponent}">
        <tr id="information-row">
          <td>
            <strong data-bind="text: $data.COMPONENTTYPENAME">
            </strong>
          </td>
          <td>
            <div id="box_thumbnail">
              <img id="thumbnail" draggable="false" data-bind="click: $root.view_detail_product, attr: { src: $root.getProductThumbnail($data) }" />
            </div>
          </td>
          <td>
            <strong data-bind="text: $data.COMPONENTNAME"></strong>
          </td>
          <td>
            <input component="number" type="number" min="1" max="10" data-bind="value: QUANTITY,
                    event: {change: $root.changeQuantity }">
          </td>
          <td>
            <span class='label new-item'>Đặt mới</span>
          </td>
          <td width="40px">
            <a data-bind="click: $root.do_RemoveFormCart">
              <i class="fa fa-trash"></i>
            </a>
          </td>
        </tr>
        <tr id="notification-row">
          <td id="notification-text" colspan="4">
            <!-- ko if: $data.STATUS == "NotEnough" -->
            <span data-bind="text: i18n('cart:left-side:table:product-notification:not-enough:lbl:text',
                                [$data.STOCK, $data.DAYSOFPURCHASE])"></span>
            <!-- /ko -->
            <!-- ko if: $data.STATUS == "OutOfStock" -->
            <span data-bind="text: i18n('cart:left-side:table:product-notification:out-of-stock:lbl:text',
                                [$data.DAYSOFPURCHASE])"></span>
            <!-- /ko -->
          </td>
        </tr>
      </tbody>
      <tbody data-bind="foreach: { data: OtherProductCart, afterRender: do_InitComponent}">
        <tr id="information-row">
          <td>
            <div id="box_thumbnail">
              <img id="thumbnail" draggable="false" data-bind="click: $root.view_detail_order_product, attr: { src: $root.getOtherProductThumbnail() }" />
            </div>
          </td>
          <td data-bind="text: ModelType"></td>
          <td data-bind="text: ModelName"></td>
          <td>
            <input component="number" type="number" min="1" max="10" data-bind="value: Quantity,
                    event: {change: $root.changeQuantityCartOrder }">
          </td>
          <td width="40px">
            <a data-bind="click: $root.do_RemoveFormCartOrder">
              <i class="fa fa-trash"></i>
            </a>
          </td>
        </tr>
        <tr>
          <td id="notification-text" colspan="4">
          </td>
        </tr>
      </tbody>
      <tbody data-bind='foreach: { data: listFixItem}'>
        <tr id="information-row">
          <td>
            <strong data-bind="text: $data.COMPONENTTYPENAME"></strong>
          </td>
          <td>
            <div id="box_thumbnail">
              <img id="thumbnail" draggable="false" data-bind="click: $root.view_detail_product, attr: { src: $root.getProductThumbnail($data) }" />
            </div>
          </td>
          <td>
            <strong data-bind="text: $data.COMPONENTNAME"></strong>
          </td>
          <td>
            <input component="number" type="number" min="1" max="1" disabled data-bind="value: 1">
          </td>
          <td>
            <span class='label fix-item'>Báo hư hỏng</span>
          </td>
          <td>
            <a data-bind="click: $root.do_RemoveFromlistFixItem">
              <i class="fa fa-trash"></i>
            </a>
          </td>
        </tr>
        <td id="notification-row" colspan="4">
        </td>
      </tbody>
      <tbody data-bind='foreach: { data: listLoseItem}'>
        <tr id="information-row">
          <td>
            <strong data-bind="text: $data.COMPONENTTYPENAME"></strong>
          </td>
          <td>
            <div id="box_thumbnail">
              <img id="thumbnail" draggable="false" data-bind="click: $root.view_detail_product, attr: { src: $root.getProductThumbnail($data) }" />
            </div>
          </td>
          <td>
            <strong data-bind="text: $data.COMPONENTNAME"></strong>
          </td>
          <td>
            <input component="number" type="number" min="1" max="1" disabled data-bind="value: 1">
          </td>
          <td>
            <span class='label miss-item'>Báo mất</span>
          </td>
          <td>
            <a data-bind="click: $root.do_RemoveFromlistLoseItem">
              <i class="fa fa-trash"></i>
            </a>
          </td>
        </tr>
        <td id="notification-row" colspan="4">
        </td>
      </tbody>
      <tbody data-bind='foreach: { data: listReturnItem}'>
        <tr id="information-row">
          <td>
            <strong data-bind="text: $data.COMPONENTTYPENAME"></strong>
          </td>
          <td>
            <div id="box_thumbnail">
              <img id="thumbnail" draggable="false" data-bind="click: $root.view_detail_product, attr: { src: $root.getProductThumbnail($data) }" />
            </div>
          </td>
          <td>
            <strong data-bind="text: $data.COMPONENTNAME"></strong>
          </td>
          <td>
            <input component="number" type="number" min="1" max="1" disabled data-bind="value: 1">
          </td>
          <td>
            <span class='label return-item'>Báo trả</span>
          </td>
          <td>
            <a data-bind="click: $root.do_RemoveFromlistReturnItem">
              <i class="fa fa-trash"></i>
            </a>
          </td>
        </tr>
        <td id="notification-row" colspan="4">
        </td>
      </tbody>
    </table>
    <!-- /ko -->
    <div component="local-paging" pageItemLimit="5" name="localPaging" data-bind="template: { afterRender: localPagingRendered, beforeRemove: localPagingBeforeRemove }"></div>
  </div>
<?php
}

function generateInformationStep() {
  echo '' ?>
<div data-bind="if: step() == 1">
  <!-- ko ifnot: ProductCart().length + OtherProductCart().length == 0 -->
  <div class="left-step2">
    <div class='title'>Sản phẩm đặt mới</div>
    <div class="list-item-manage">

      <table class="table-step1">
        <tbody data-bind='foreach: { data: ProductCart}'>
          <tr>
            <td data-bind="text: $data.COMPONENTTYPENAME"></td>
            <td data-bind="text: $data.COMPONENTNAME"></td>
            <td><span class='label new-item' data-bind='text: "Số lượng:" + $data.QUANTITY'></span></td>
          </tr>
        </tbody>
        <tbody data-bind='foreach: { data: OtherProductCart}'>
          <tr>
            <td data-bind="text: $data.COMPONENTTYPENAME"></td>
            <td data-bind="text: $data.COMPONENTNAME"></td>
            <td><span class='label new-item' data-bind='text: "Số lượng:" + $data.QUANTITY'></span></td>
          </tr>
        </tbody>
      </table>

    </div>
    <!-- ko if: getLengthListEditItem() > 3 -->
    <div class='collapse-list-item' data-bind='click: showMoreItem'>Cùng với <span data-bind='text: remainEditItem'></span>
      sản phẩm khác</div>
    <!-- /ko -->
    <div class='note-inblock'>
      <div>Ghi chú:</div>
      <div> <input id='note_new_text_area' type="text" data-bind="value: noteNewItem" placeholder="Để lại ghi chú cho người phê duyệt"></div>
    </div>
  </div>
  <!-- /ko -->
  <!-- ko ifnot: listFixItem().length + listLoseItem().length + listReturnItem().length  == 0 -->
  <div class="block-center-step2"></div>
  <div class="left-step2">
    <div class='title'>Sản phẩm khác</div>
    <div class="list-item-manage">

      <table class="table-step1">
        <!-- ko if: listFixItem().length !== 0 -->
        <tbody data-bind='foreach: { data: listFixItem}'>
          <tr>
            <td data-bind="text: $data.COMPONENTTYPENAME"></td>
            <td data-bind="text: $data.COMPONENTNAME"></td>
            <td data-bind="text: $data.RESOURCENAME"></td>
            <td>
              <span class='label fix-item'>Báo hư hỏng</span>
            </td>
          </tr>
        </tbody>
        <!-- /ko -->
        <!-- ko if: listLoseItem().length !== 0 -->
        <tbody data-bind='foreach: { data: listLoseItem}'>
          <tr>
            <td data-bind="text: $data.COMPONENTTYPENAME"></td>
            <td data-bind="text: $data.COMPONENTNAME"></td>
            <td data-bind="text: $data.RESOURCENAME"></td>
            <td>
              <span class='label miss-item'>Báo mất</span>
            </td>
          </tr>
        </tbody>
        <!-- /ko -->
        <!-- ko if: listReturnItem().length !== 0 -->
        <tbody data-bind='foreach: { data: listReturnItem}'>
          <tr>
            <td data-bind="text: $data.COMPONENTTYPENAME"></td>
            <td data-bind="text: $data.COMPONENTNAME"></td>
            <td data-bind="text: $data.RESOURCENAME"></td>
            <td>
              <span class='label return-item'>Trả sản phẩm</span>
            </td>
          </tr>
        </tbody>
        <!-- /ko -->
      </table>

    </div>
    <!-- ko if: getLengthListEditItem() > 3 -->
    <div class='collapse-list-item' data-bind='click: showMoreItem'>Cùng với <span data-bind='text: remainEditItem'></span>
      sản phẩm khác</div>
    <!-- /ko -->
    <div class='note-inblock'>
      <div>Ghi chú:</div>
      <div> <input id='note_edit_text_area' name="Note" type="text" data-bind="value: noteEditItem" placeholder="Để lại ghi chú cho người phê duyệt"></div>
    </div>
  </div>
  <!-- /ko -->
</div>

<?php
}
function generateLeftStep3() {
  echo '' ?>
<div class='left-step3' data-bind="if: step() == 2">
  <div class='block-table'>
    <!-- ko if: ProductCart().length + OtherProductCart().length !== 0 || checkEditItem-->
    <table class="table-step1">
      <thead>
        <tr>
          <th>Loại</th>
          <th>Hình ảnh</th>
          <th>Thông tin</th>
          <th>Số lượng</th>
          <th>Trạng thái</th>
        </tr>
      </thead>
      <tbody data-bind="foreach: { data: ProductCart, afterRender: do_InitComponent}">
        <tr id="information-row">
          <td>
            <strong data-bind="text: $data.COMPONENTTYPENAME">
            </strong>
          </td>
          <td rowspan="1">
            <div id="box_thumbnail">
              <img id="thumbnail" draggable="false" data-bind="click: $root.view_detail_product, attr: { src: $root.getProductThumbnail($data) }" />
            </div>
          </td>
          <td>
            <strong data-bind="text: $data.COMPONENTNAME">
            </strong>
          </td>
          <td>
            <span data-bind="text: $data.QUANTITY"></span> sản phẩm

          </td>
          <td>
            <span class='label new-item'>Đặt mới</span>
          </td>

        </tr>

      </tbody>
      <tbody data-bind="foreach: { data: OtherProductCart, afterRender: do_InitComponent}">
        <tr id="information-row">
          <td>
            <strong data-bind="text: $data.ModelType">
            </strong>
          </td>
          <td>
            <div id="box_thumbnail">
              <img id="thumbnail" draggable="false" data-bind="click: $root.view_detail_order_product, attr: { src: $root.getOtherProductThumbnail() }" />
            </div>
          </td>
          <td data-bind="text: ModelName"></td>
          <td>
            <span data-bind="text: $data.Quantity"></span> sản phẩm
          </td>
          <td>
            <span class='label new-item'>Đặt mới</span>
          </td>
        </tr>
      </tbody>
      <tbody data-bind='foreach: { data: listFixItem}'>
        <tr>
          <td>
            <div id="box_thumbnail">
              <img id="thumbnail" draggable="false" data-bind="click: $root.view_detail_product, attr: { src: $root.getProductThumbnail($data) }" />
            </div>
          </td>
          <td>
            <strong data-bind="text: $data.RESOURCENAME"></strong>
            <br>
            <strong data-bind="text: $data.COMPONENTNAME"></strong>
          </td>
          <td>
            1 sản phẩm
          </td>
          <td>
            <span class='label fix-item'>Báo hư hỏng</span>
          </td>

        </tr>
      </tbody>
      <tbody data-bind='foreach: { data: listLoseItem}'>
        <tr>
          <td>
            <div id="box_thumbnail">
              <img id="thumbnail" draggable="false" data-bind="click: $root.view_detail_product, attr: { src: $root.getProductThumbnail($data) }" />
            </div>
          </td>
          <td>
            <strong data-bind="text: $data.RESOURCENAME"></strong>
            <br>
            <strong data-bind="text: $data.COMPONENTNAME"></strong>
          </td>
          <td>
            1 sản phẩm
          </td>
          <td>
            <span class='label miss-item'>Báo mất</span>
          </td>
        </tr>
      </tbody>
      <tbody data-bind='foreach: { data: listReturnItem}'>
        <tr>
          <td>
            <div id="box_thumbnail">
              <img id="thumbnail" draggable="false" data-bind="click: $root.view_detail_product, attr: { src: $root.getProductThumbnail($data) }" />
            </div>
          </td>
          <td>
            <strong data-bind="text: $data.RESOURCENAME"></strong>
            <br>
            <strong data-bind="text: $data.COMPONENTNAME"></strong>
          </td>
          <td>
            1 sản phẩm
          </td>
          <td>
            <span class='label return-item'>Báo trả</span>
          </td>
        </tr>
      </tbody>
    </table>
    <!-- /ko -->
    <!-- ko if: ProductCart().length + OtherProductCart().length == 0 && !checkEditItem -->
    <div class='table-no-item'> Không có sản phẩm</div>
    <!-- /ko -->
    <div class='back-step1' data-bind='click: ChangeToFirstPage'> Chỉnh sửa</div>
  </div>
</div>
<?php
}