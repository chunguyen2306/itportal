<?php

function generate_product_menu($resourceTypeName)
{

    $resourceTypeAPI = using('plugin.itportal.API.Options.MainMenu');
    $subMenu = $resourceTypeAPI->GetAsSubMenu(array(
        'parentName' => $resourceTypeName
    ));
    global $_AppVersion;

    ?>

    <div class="select-product">
        <h2><?php echo $resourceTypeName ?></h2>
        <ul class="list-product">
            <?php foreach ($subMenu as $subKey => $subItem) {
//                $fullPath = getFullPath('upload.main-menu', '', 'url') . '/' . $subItem->Icon . '?v=' . date_timestamp_get(date_create());
                $fullPath = getFullPath('upload.main-menu', '', 'url') . '/' . $subItem->Icon . '?v=' . $_AppVersion;

                $isActive = url_param(2);
                if ($isActive == $subItem->Name) {
                    $isActive = 'active';
                } else {
                    $isActive = '';
                }
                echo "<script>console.log( 'Debug Logger: " . $fullPath . "' );</script>"
                ?>
                <li class="<?php echo $isActive; ?>">
                    <a href="/product/<?php echo $resourceTypeName; ?>/<?php echo $subItem->Name; ?>"
                       title="<?php echo $subItem->Name; ?>">
                        <span class="icon"><img width="16px" height="16px" src="<?php echo $fullPath; ?>"/>
                        </span><?php echo $subItem->Name; ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php }

function generate_home_menu()
{
    $mainMenuAPI = using('plugin.itportal.API.Options.MainMenu');
    $menuTypeAPI = using('plugin.itportal.API.Options.MenuType');
    $mainMenu = $mainMenuAPI->GetAsMenu(array());

    global $_AppVersion;

    ?>

    <ul id="main-nav__list">
        <?php foreach ($mainMenu as $key => $item) {
            $isActive = url_param(1);
            $page = url_param(0);
            $pageTag = 'product';

            $typeName = $menuTypeAPI->GetById(['ID' => $item->TypeID]);
            if ($typeName == null) {
                $typeName = 'Asset';
            } else {
                $typeName = $typeName->Name;
            }
            if ($typeName == 'Post') {
                $pageTag = 'document';
            }

            if ($page == '' || $page == 'product') {
                if ($isActive == $item->Name) {
                    $isActive = 'activeMenu';
                } else if ($isActive == '' && $key == 0) {
                    $isActive = 'active';
                } else {
                    $isActive = '';
                }
            }

            ?>
            <li class="<?php echo $isActive; ?>">

                <?php

                $itemLink = '';

                if ($pageTag === 'document') {
                    $mapping = json_decode($item->Mapping);
                    if (sizeof($mapping) > 0) {
                        $cate = get_categories([
                            'term_taxonomy_id' => $mapping[0]
                        ])[0];

                        //$itemLink = $item->Name.'/'.$cate->slug;
                        $itemLink = $cate->slug;
                    }
                } else {
                    $itemLink = $item->Name;
                }
                ?>

                <a href="/<?php echo $pageTag; ?>/<?php echo $itemLink; ?>" title="<?php echo $item->Name; ?>">
                        <?php if ($key == 0) { ?>
                            <i class="fa fa-home"></i>
                        <?php } ?>
                        <i class="fa fa-chevron-down"></i>
                        <span><?php echo $item->Name; ?></span>
                    </a>
                    <div class="select-product">
                        <h2><?php echo $item->Name; ?></h2>
                        <ul class="list-product">
                            <?php foreach ($item->Childs as $subKey => $subItem) {
//                                $fullPath = getFullPath('upload.main-menu', '', 'url') . '/' . $subItem->Icon . '?v=' . date_timestamp_get(date_create());
                                $fullPath = getFullPath('upload.main-menu', '', 'url') . '/' . $subItem->Icon . '?v=' . $_AppVersion;
                                $itemLink = '';
                                if ($pageTag === 'document') {
                                    $mapping = json_decode($subItem->Mapping);
                                    if (sizeof($mapping) > 0) {
                                        $cate = get_categories([
                                            'term_taxonomy_id' => $mapping[0]
                                        ])[0];

                                        $itemLink = $cate->slug;
                                    }
                                } else {
                                    $itemLink = $subItem->Name;
                                } ?>

                                <li>
                                    <a href="/<?php echo $pageTag; ?>/<?php echo $item->Name; ?>/<?php echo $itemLink; ?>"
                                       title="<?php echo $subItem->Name; ?>">
                                        <span class="icon"><img width="16px" height="16px"
                                                                src="<?php echo $fullPath; ?>"/></span>
                                        <?php echo $subItem->Name; ?>
                                    </a>
                                </li>

                            <?php } ?>
                        </ul>
                    </div>
            </li>
        <?php } ?>
    </ul>

    <?php
}

function generate_slide_product($products)
{
    $tabsEml = '';
    $contentEml = '';
    if (!is_array($products)) {
        $products = array();
    }
    foreach ($products as $product) {

        $tabItem = '<li><a target="content-' . $product->COMPONENTTYPENAME . '" title="' . $product->COMPONENTTYPENAME . '">' .
            $product->COMPONENTTYPENAME .
            '</a></li>';
        $tabsEml = $tabsEml . $tabItem;
        $productItems = '';
        foreach ($product->Components as $component) {
            if ($component->Detail == null) {
                $component->Detail = (object)array(
                    'ProductPageDescription' => '',
                    'Thubmails' => '',
                    'Price' => 0,
                    'BuyDays' => 7,
                    'TransferDays' =>2,
                    'NeedApproval' => false,
                    'IsDisplay' => 1,
                );
            }
            if($component->Detail->IsDisplay == 0){
                continue;
            }
            if($component->Stock == 0){
                $component->Detail->TransferDays = $component->Detail->BuyDays + $component->Detail->TransferDays;
            }
            $imgSrc = '';
            if ($component->Detail->Thubmails != '') {
                $imgSrc = getFullPath('upload.component.' . $component->COMPONENTID, '', 'uri') . '/' . $component->Detail->Thubmails;
            } else {
                $str = $component->DESCRIPTION;
                $re = '/Icon: (((?!Notes|Icon|Code).)*)/';
                preg_match($re, $str, $matches);
                if(sizeof($matches) > 1){
                    $imgSrc = getFullPath('upload.component_type', '', 'uri') . "/$matches[1]";
                } else {
                    $imgSrc = getFullPath('upload.component', '', 'uri') . '/default.png';
                }

            }
            //$valueOfModel =  getMinValueOfModel($component->COMPONENTID);
            $productItem = '<li COMPONENTID="' . $component->COMPONENTID . '" class="swiper-slide">' .
                '<span class="transferDays" title="Giao hàng trong '.$component->Detail->TransferDays.' ngày"><span>'.$component->Detail->TransferDays.'D</span></span>'.
                '<span class="needApproval" title="Cần được phê duyệt" needapproval="'.$component->Detail->NeedApproval.'">Cần <b>phê duyệt</b></span>'.
                '<a href="/detail/' . $component->COMPONENTID . '" class="fancybox">' .
                ($component->Stock == 0?'<span class="outStock">Không có sẵn</span>':'').
                '<span style="background-image: url(\'' . $imgSrc . '\')" class="imgstyle">' .
                '</span>' .
                '<span class="info-text">' .
                '<span name="COMPONENTNAME">' . $component->COMPONENTNAME . '</span>' .
                '<span class="short-description">'.
                ($component->Detail->ProductPageDescription != null? strip_tags($component->Detail->ProductPageDescription) : strip_tags($component->Detail->ShortDescription)).
                '</span>'.
                '</span>' .
                //'<span class="price">'. 100 .'₫</span>'.
                '</a>' .
                '<input type="hidden" name="COMPONENTTYPEID" value="' . $component->COMPONENTTYPEID . '" />' .
                '<input type="hidden" name="COMPONENTTYPENAME" value="' . $component->COMPONENTTYPENAME . '" />' .
                '<button name="btnAddToCart" class="full">Đặt ngay</button>' .
                '</li>';
            $productItems = $productItems . $productItem;
        }
        $contentItem = '<div class="posts__contain tab-details" id="content-' . $product->COMPONENTTYPENAME . '">' .
            '<div class="swiper-container primary-banner">' .
            '<ul class="swiper-wrapper">' .
                $productItems .
            '</ul>' .
            '<div class="swiper-button-next"></div>' .
            '<div class="swiper-button-prev"></div>' .
            '<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>' .
            '</div>' .
            '</div>';
        $contentEml = $contentEml . $contentItem;
    }

    echo '<ul class="posts__tabs">';
    echo $tabsEml;
    echo '</ul>';
    echo $contentEml;
}

function get_user_info($userAccount)
{
    $userAPI = using('plugin.itportal.API.User.AMUserAjax');

    return $userAPI->GetUserByDomain(['domainAccount' => $userAccount]);
}

function get_user_default_icon($user)
{
    $displayName = '';
    $displayName = $displayName . mb_substr($user->lastName, 0, 1, "utf-8");
    $displayName = $displayName . mb_substr($user->firstName, 0, 1, "utf-8");

    $color = '#' . substr(strval(floor(ord($displayName[0]) / 3 / 100 * 16777215)), 0, 6);

    return [
        'name' => $displayName,
        'color' => $color
    ];
}

function sort_panel_filter($a, $b) {
    return strnatcmp($a,$b);
}

function generate_filter_panel_list($arr, $name, $prefix, $nameStr, $idStr, $filStr){
    if(is_array($arr) && sizeof($arr) > 0) {

        uasort($arr, function ($a, $b) use ($nameStr) {
            if(isset($nameStr) && $nameStr !== '') {
                return strcmp($a[$nameStr], $b[$nameStr]);
            } else {
                return strcmp($a, $b);
            }
        });
        ?>
    <div class="filter-panel">
        <h2><?php echo $name ?></h2>
        <ul class="list-filter">
            <?php foreach ($arr as $subKey => $subItem) {

                $filVal = $prefix.'-'.($idStr == ''?$subItem:$subItem[$idStr]);
                $isActive = false;
                $isActive = strpos($filStr, $filVal) !== false ?1:0;
                ?>
                <li>
                    <label>
                        <input component="checkbox" class="nolabel onlychecked" data-bind="checked: Filter, event: { change: chkFil_OnClick }" type="checkbox" value="<?php echo $filVal; ?>" >
                        <span><?php echo $nameStr == ''?$subItem:$subItem[$nameStr]; ?></span>
                    </label>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php }
}

function generate_filter_panel(){
    $apiCMDBComponentType = using('plugin.itportal.API.CMDB.ComponentType');
    $apiCMDBComponent = using('plugin.itportal.API.CMDB.Component');
    $apiMainMenu = using('plugin.itportal.API.Options.MainMenu');
    $filStr = '';
    //$filArr = [];
    if(isset($_GET['fil'])) {
        $filStr = $_GET['fil'];
        //$filArr = json_decode($filStr);
    }
    if(url_param(2) !== ''){
        $menuName = url_param(2);
    } else {
        $menuName = url_param(1);
    }

    $menuItem = $apiMainMenu->GetByName(['menuName'=>$menuName]);
    $typeList = json_decode($apiCMDBComponentType->GetByIDs([
        'listID' => $menuItem->Mapping
    ]), true);
    generate_filter_panel_list($typeList['Result'], 'Loại sản phẩm', 'p', 'COMPONENTTYPENAME', 'COMPONENTTYPEID', $filStr);

    $seletedType = [];
    if(isset($_GET['fil']) && sizeof(am_json_decode($filStr)) > 0) {
        $seletedType = preg_replace('/p-/', '',$filStr);
        $seletedType = am_json_decode($seletedType);
        for($i = sizeof($seletedType); $i >= 0; $i--){
            if( strpos($seletedType[$i],'m-') === 0 ||
                strpos($seletedType[$i],'l-') === 0 ||
                strpos($seletedType[$i],'a-') === 0 ||
                strpos($seletedType[$i],'y-') === 0){
                unset($seletedType[$i]);
            }
        }
        $seletedType = array_values($seletedType);
    }

    if(sizeof($seletedType) === 0){
        $seletedType = json_decode($menuItem->Mapping);
    }

    $mafList = json_decode($apiCMDBComponent->GetManufactureByComponentType([
        'listID' => json_encode($seletedType)
    ]), true);
    generate_filter_panel_list($mafList['Result'], 'Nhà sản xuất', 'm', '', '', $filStr);

}
