<?php
/**
 * Created by PhpStorm.
 * User: tientm2
 * Date: 9/11/2017
 * Time: 7:09 PM
 */
function am_add_menu_admin($menu, $className = ''){

    $current_user = wp_get_current_user();
    $userRole = get_user_permission($current_user->user_login);
    echo '<ul class="menu '.$className.'">';

    foreach ($menu as $item){
        if(check_permisson($item['Role']) === false){
            continue;
        }

        $emlItem = sprintf('<li '.
            'data-bind="attr: { class: Menu.AdminMenu.isActive(\'%s\')+\' {isParent}\' }">'.
            '<a href="%s" data-bind="text: i18n(\'%s\')"></a></li>', $item['Link'], $item['Link'], $item['Name']);

        if(isset($item['Sub'])){
            $emlItem = str_replace('{isParent}', 'parent', $emlItem);
            echo $emlItem;
            foreach ($item['Sub'] as $subItem){

                if(check_permisson($subItem['Role']) === false){
                    continue;
                }

                echo sprintf('<li '.
                    'data-bind="attr: { class: Menu.AdminMenu.isActive(\'%s\')+\' sub\' }">'.
                    '<a href="%s" data-bind="text: i18n(\'%s\')"></a></li>', $subItem['Link'], $subItem['Link'], $subItem['Name']);
            }
        } else {
            $emlItem = str_replace('{isParent}', '', $emlItem);
            echo $emlItem;
        }
    }
    echo '</ul>';
}