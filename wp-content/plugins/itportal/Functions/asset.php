<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 23/7/2018
 * Time: 4:24 PM
 */

function getInFoAssetResource($resourceName){
    import('plugin.itportal.API.CMDB.CMDB');
    $cmdbAPI = new CMDB();
    $resource = am_json_decode($cmdbAPI->get("CMDB2", ['m', 'resourcename'], ['GetAllAssetDetail', $resourceName]));
    //lấy tạm ngày mua hàng để tính thời gian bắt đầu sử dụng sẽ tính thời gian sử theo cách khác

    $response = am_json_decode($cmdbAPI->get("CMDB2", ["m", "assetname"], ["GetAssetCost", $resourceName]));
    $resource->Cost = $response * 23000;
    $timenow = new stdClass();
    $timenow->Year = date('Y');
    $timenow->Month = date('m');
    $resource->assetUsedDate = ($timenow->Year - date('Y', $resource->GeneralAssetInfo->AcquitionDate/1000)) * 12
        + ($timenow->Month - date('m', $resource->GeneralAssetInfo->AcquitionDate/1000));

    //lấy thông tin chi phí
    $resource = getUsingCost($resource);
    //lấy danh sách ghi chú
    $assetHistoryAPI = using('plugin.itportal.API.Asset.APIAssetStateHistory');
    $resource->assetHistoryComment = $assetHistoryAPI->GetAssetCommentHistory(['assetName' => $resourceName]);
    //Lich sư thay doi trang thái
    $assetHistory=[];
    $assetHistory = am_json_decode($cmdbAPI->get("CMDB2",
        ["m", "resourcename"],
        ["GetAllResourceStateHistoryByResourceName", $resourceName]));
    $requests=[];
    foreach ($assetHistory as $item) {
        $itamHistory = $assetHistoryAPI->GetAssetStateHistoryByCMDBHistoryID(array(
            'CMDBHistoryID' => $item->ID
        ));
        foreach ($itamHistory as $historyItem) {
            $request = new stdClass();
            if ($historyItem->AssetName == $item->AssetName) {
                $item->ITAMComment = $historyItem->Comment;
                $item->SysComment = $historyItem->SysComment;
                $item->ITAMActionUser = $historyItem->UserAction;
                if($item->NewState =='In Use' && $item->PreviousState == 'Delivered User'){
                    $request->idInfo =$item->SysComment;
                    $request->orderdate = $item->StartDate;
                    array_push($requests, $request );
                }
            }
        }
    }
    $resource->HistoryChangeState = $assetHistory;
    $resource->HistoryInUse = $requests;
    //thong tin chung tu ban giao
    $resource = setNewestOrderIDs($resource);
    //lấy thông tin trạng thái
    $result = am_json_decode($cmdbAPI->get("CMDB2", ['m', 'resourcename'], ['GetResourceStateByAssetName', $resourceName]));
    $resource->Status = $result->Status;
    $resource = getOwnerAsset($resource,$result);
    if($resource->IsComponent){
        $res = am_json_decode($cmdbAPI->get("CMDB2", ['m', 'resourcename'], ['GetResourceStateByAssetName', $resource->Parent]));
        $resource->ParentOwnerAccount = $res->OwnerAccount;
        $resource->ParentOwner = $res->Owner;
        $owner = am_json_decode($cmdbAPI->get("CMDB2", ['m', 'username', 'startindex', 'limit'], ['GetUserInformation', $resource->ParentOwnerAccount, 0, 10]));
    }else{
        $resource->ParentOwner='';
        $resource->ParentOwnerAccount='';
        $owner = am_json_decode($cmdbAPI->get("CMDB2", ['m', 'username', 'startindex', 'limit'], ['GetUserInformation', $resource->OwnerAccount, 0, 10]));
    }
    $resource->OwnerJobTitle = $owner->Title;
    $resource->Deparment = $owner->Deparment;


return $resource;
}

function getUsingCost($resource){


    $assetTypeGAPI = using('plugin.itportal.API.Asset.APIAssetTypeGroup');
    $assetDiscountAPI = using('plugin.itportal.API.Asset.APIAssetDiscountPercentYear');
    $dataAssetgroup = $assetTypeGAPI->getByAssetType(['assettype' => $resource->ResourceTypeID]);


    if ($dataAssetgroup[0] == null) {
        $dataAssetgroup = $assetTypeGAPI->getByDefault();
    }

    $groupID = $dataAssetgroup[0]->id;
    $maxMonth = $dataAssetgroup[0]->maxmonth;
    $maxYear = $maxMonth / 12;


    $realYear = $resource->assetUsedDate / 12;

    $roundYear = ceil($realYear);
    if($roundYear==0){
        $percentYear=0;
    }else{$percentYear = (float)$realYear / $roundYear;}


    if ($realYear >= $maxYear) {
        $usingpercent = 5;
        $usingcost = 0;
    } else {
        $sumpercent = $assetDiscountAPI->getTotalPercentByAssetGroupID(['roundyear' => $roundYear, 'groupid' => $groupID])[0]->TotalPer;

        $discountyearpercent = $assetDiscountAPI->getYearPercentByAssetGroupID(['year' => $roundYear, 'groupid' => $groupID])[0]->Percent;

        $usingpercent = $percentYear * $discountyearpercent + $sumpercent;

        $resource->usecost = $resource->Cost - ($resource->Cost * $usingpercent) / 100;
    }
    $resource->usingCost = $resource->usecost + ($resource->Cost * 0.05);
    return($resource);
}

function setNewestOrderIDs($resource){

    // Lấy thông tin chứng từ bàn giao
    $re = '/\[Request: (\d+)\]\[Workflow: ([^\]]+)\]\[Step: ([^\]]+)\]/m';
    preg_match_all($re,$resource->HistoryInUse[0]->idInfo, $matches, PREG_SET_ORDER, 0);

    if($matches[0]){
        $resource->order= $matches[0][1];
        $resource->orderdate = $resource->HistoryInUse[0]->orderdate;
    }else{
        $resource->order= null;
        $resource->orderdate = null;
    }
    return $resource;
}

function getOwnerAsset($resource,$result){

    if ($resource->Status == 'In Use') {
        $resource->Owner = $result->Owner;
        $resource->OwnerAccount = $result->OwnerAccount;

    }else if($resource->Status == 'In Store'){
        $resource->Owner = null;
        $resource->OwnerAccount = null;
    }else{
        if(is_array(HistoryChangeState)){
            $resource->OwnerAccount = $resource->HistoryChangeState[0]->ActionUserAccount;
            $resource->Owner = $resource->HistoryChangeState[0]->Owner;
        }else{
            $resource->Owner = null;
            $resource->OwnerAccount = null;
        }
    }
    return $resource;
}
//cpnguyen 26/11
function generateCategorySlide($category) {
    $categoryContent = '';
    $allCategory = '';
    // echo "<script>console.log( 'Debug Objects: " .  . "' );</script>";
    $categoryType = $_GET['categoryType'];
    
    foreach($category as $categoryName) {
        if($categoryName == 'All')  {
            //highlight chip if is selected
            if($categoryType == '' || $categoryType == 'All') {
                $allCategory = '<div class="selected-chip" data-bind="event : { click: chip_select }">' . $categoryName . '</div>';
            } 
            else {
                $allCategory = '<div class="chip" data-bind="event : { click: chip_select }">' . $categoryName . '</div>';
            }
        } 
        else {
            //highlight chip if is selected
            if ($categoryType == $categoryName ) {
                $categoryContent = '<div class="selected-chip" data-bind="event : { click: chip_select }">' . $categoryName . '</div>' . $categoryContent;
            }
            else {
                $categoryContent = $categoryContent . '<div class="chip" data-bind="event : { click: chip_select }">' . $categoryName . '</div>';
            }
        }
    }

    //append 'All' content to first position
    $categoryContent = '<div class="scrollable-chip-row">' . $categoryContent . '</div>';

    echo '<div class="chip-row-container">' . $allCategory .
        $categoryContent .
        '</div>';
}
?>