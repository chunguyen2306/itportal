<?php
/**
 * Created by PhpStorm.
 * User: tientm2
 * Date: 3/29/2018
 * Time: 6:07 PM
 */

import('theme.package.AMSecurity');
global $security;
$security = new AMSecurity();

function get_user_permission($domain){
    $roleAndPermissionAPI = using('plugin.itportal.API.RoleAndPermission.APIRoleAndPermission');

    return $roleAndPermissionAPI->GetUserRoleByDomain(['domain' => $domain]);
}

function check_permisson($requieRole){

    if(!isset($requieRole) || $requieRole === '' || $requieRole === null ) {
        return true;
    }

    global $security;

    if($security->checkBySecurityDoc($requieRole, [])) {
        return true;
    } else {
        return false;
    }
}