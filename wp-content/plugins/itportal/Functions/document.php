<?php

function generate_document_tree($menuname, $cate, $isGetChild)
{
    $mainMenuAPI = using('plugin.itportal.API.Options.MainMenu');
    $subCate = get_categories([
        'hide_empty' => 0,
        'parent' => $cate->cat_ID
    ]);
    $catePost = get_posts([
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'id',
                'terms' => $cate->cat_ID, // Where term_id of Term 1 is "1".
                'include_children' => false
            )
        )
    ]);

    echo '<ul class="document-tree">';
    if ($subCate != null) {
        foreach ($subCate as $cateItem) {
            $menuItem = $mainMenuAPI->GetByName([
                'menuName'=>$cateItem->name
            ]);
            echo '<li>';
            //echo '<i class="fa fa-folder"></i> ';
            echo sprintf('<a href="/document/%s" class="folder" title="%s">' .
                '<span class="icon"><img width="16px" height="16px" src="/wp-content/uploads/main-menu/%s"></span>' .
                '%s</a>', $menuname . '/' . $cateItem->slug, $cateItem->name, $menuItem->Icon, $cateItem->name);

            if ($isGetChild) {
                generate_document_tree($menuname, $cateItem, $isGetChild);
            }
            echo '</li>';
        }
    }
    foreach ($catePost as $post) {
        echo '<li>';
        echo '<i class="fa fa-file-o"></i> ';
        echo sprintf('<a href="/document/%s" title="%s">%s</a>', $menuname . '/post/' . $post->post_name, $post->post_title, $post->post_title);
        echo '</li>';
    }
    echo '</ul>';
    if ($subCate == null) {
        return;
    }
}

function find_parent_tree($cate)
{
    if ($cate->parent == 0) {
        return $cate;
    } else {
        $nextCate = get_categories([
            'hide_empty' => 0,
            'term_taxonomy_id' => $cate->parent
        ])[0];
        if ($nextCate->parent == 0) {
            return $cate;
        } else {
            return find_parent_tree($nextCate);
        }
    }
}

function generate_document_tree_menu($menuname, $catename, $postname, $isGetRoot)
{
    $mainMenuAPI = using('plugin.itportal.API.Options.MainMenu');
    $mainMenu = $mainMenuAPI->GetAsMenu(array());

    if ($catename === '') {
        $catename = $menuname;
    }
    if ($catename == 'post') {
        $post = query_posts([
            'name' => $postname
        ])[0];
        $cate = get_the_category($post->ID)[0];
    } else {
        $cate = get_categories([
            'hide_empty' => 0,
            'slug' => $catename
        ])[0];
    }

    if ($cate != null) {
        if ($isGetRoot) {
            $cate = find_parent_tree($cate);
        }

        echo '<h2 class="document-title">' . $cate->name . '</h2>';
        generate_document_tree($menuname, $cate, $isGetRoot);
    } else {
        echo '<h2 class="document-title">' . $catename . '</h2>';
    }
}

function generate_document_list($menuname, $catename, $postname)
{
    if ($catename == '') {
        generate_document_tree_menu($menuname, $catename, $postname, false);
    } else {
        generate_document_tree_menu($menuname, $catename, $postname, true);
    }

}

function get_post_content($menuName, $cateName, $postname) {

    if ($cateName == 'post') {
        $post = query_posts([
            'name' => $postname
        ])[0];
        echo '<h1>' . $post->post_title . '</h1>';
        echo '<div>' . $post->post_content . '</div>';
    } else {

        if($cateName == ''){
            $cateName = $menuName;
        }

        $cate = get_categories([
            'hide_empty' => 0,
            'slug' => $cateName
        ])[0];

        $posts = query_posts([
            'post_type' => 'post',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field' => 'id',
                    'terms' => $cate->cat_ID, // Where term_id of Term 1 is "1".
                    'include_children' => false
                )
            ),
            'meta_query' => array(
                array(
                    'key' => 'default_post',
                    'value' => true,
                    'compare' => '=',
                )
            )
        ]);

        if (sizeof($posts) == 0) {
            echo '<h3 class="sys-selectpost">Vui lòng chọn bài viết bạn muốn đọc</h3>';
        } else {
            foreach ($posts as $post) {
                echo '<h1>' . $post->post_title . '</h1>';
                echo '<div>' . $post->post_content . '</div>';
            }
        }
    }
}