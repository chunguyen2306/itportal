<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if (!class_exists('APIRoleAndFeature')) {

    import('plugin.itportal.Factory.AMRoleFeatureFactory');
    import('plugin.itportal.Factory.AMFeatureRoleMappingFactory');
    import('plugin.itportal.Factory.AMRoleManagementFactory');
    import('plugin.amUser.class.includes.RoleID');

    class APIRoleAndFeature
    {
        private $roleFac = null;
        private $roleFeatureFac = null;
        private $featureRoleMappingFac = null;

        function __construct()
        {
            $this->roleFac = new AMRoleManagementFactory();
            $this->roleFeatureFac = new AMRoleFeatureFactory();
            $this->featureRoleMappingFac = new AMFeatureRoleMappingFactory();
        }

        public function IsExistFeature($PARAM)
        {
            $data = isset($PARAM['data']) ? $PARAM['data'] : null;

            $result = $this->roleFeatureFac->insert($data);

            return array(
                "State" => $result == 1 ? true : false,
                "Result" => $data
            );
        }

        public function GetAllFeatures($PARAM)
        {
            $pageIndex = isset($PARAM['pageindex']) ? $PARAM['pageindex'] : -1;
            $limit = isset($PARAM['limit']) ? $PARAM['limit'] : 0;

            $agrs = [];

            if ($limit !== 0) {
                $agrs['limit'] = array(
                    "at" => $pageIndex * $limit,
                    "length" => $limit,

                );
            }

            $agrs["order"] = "a.FeatureName";

            return $this->roleFeatureFac->query($agrs);
        }

        public function CreateFeature($PARAM)
        {
            $data = isset($PARAM['data']) ? $PARAM['data'] : null;

            $result = $this->roleFeatureFac->insert($data);

            return array(
                "State" => $result == 1 ? true : false,
                "Result" => $data
            );
        }

        public function DeleteFeature($PARAM)
        {
            $id = isset($PARAM['id']) ? $PARAM['id'] : null;

            $result = $this->roleFeatureFac->delete(array(
                'ID' => $id
            ));

            return array(
                "State" => $result == 1 ? true : false
            );
        }

        public function UpdateFeature($PARAM)
        {
            $data = isset($PARAM['data']) ? $PARAM['data'] : null;

            $result = $this->roleFeatureFac->update($data, array(
                'ID' => $data['ID']
            ), true);

            return array(
                "State" => $result == 1 ? true : false,
                "Result" => $data
            );
        }

        public function CreateFeaturesRole($PARAM) {
            $data = isset($PARAM['data']) ? $PARAM['data'] : null;
            $data['FeatureID'] = json_encode($data['FeatureID']);

            $result = $this->featureRoleMappingFac->insert($data);

            return array(
                "State" => $result == 1 ? true : false,
                "Result" => $data
            );
        }

        public function UpdateFeaturesRole($PARAM) {
            $data = isset($PARAM['data']) ? $PARAM['data'] : null;
            $data['FeatureID'] = json_encode($data['FeatureID']);
            $result = $this->featureRoleMappingFac->update($data, array(
                'RoleID' => $data['RoleID']
            ), true);

            return array(
                "State" => $result == 1 ? true : false,
                "Result" => $data
            );
        }

        public function DeleteFeatureRole($PARAM) {
            $id = isset($PARAM['id']) ? $PARAM['id'] : null;

            $result = $this->featureRoleMappingFac->delete(array(
                'ID' => $id
            ));

            return array(
                "State" => $result == 1 ? true : false
            );
        }

        public function GetAllFeaturesRole($PARAM)
        {
            $pageIndex = isset($PARAM['pageindex']) ? $PARAM['pageindex'] : -1;
            $limit = isset($PARAM['limit']) ? $PARAM['limit'] : 0;

            $agrs = [];

            if ($limit !== 0) {
                $agrs['limit'] = array(
                    "at" => $pageIndex * $limit,
                    "length" => $limit
                );
            }

            $result = $this->featureRoleMappingFac->query($agrs);

            foreach ($result as $item) {
                $roleID = $item->RoleID;
                $roleName = $this->roleFac->getOneBy(array(
                    "ID" => $roleID
                ))->Name;
                $item->RoleName = $roleName;
                $featureIDs = substr($item->FeatureID, 1, strlen($item->FeatureID) - 2);
                $featureNames = $this->roleFeatureFac->query([
                    'select' => "DisplayName",
                    'filter' => [
                        ["ID in ($featureIDs)", ""]
                    ]
                ], false, ARRAY_N);
                $item->FeatureName = $featureNames;
            }
            return $result;
        }
    }
}
return new APIRoleAndFeature();