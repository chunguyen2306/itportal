<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if(!class_exists('AbstractAjax')){

	import('theme.package.HttpRequest');
	import('theme.package.HttpResponse');
	import('plugin.amHelpdeskConsole.class.factory.AMHelpdeskConsoleLogFactory');

    class AbstractAjax{

        public function callAPIGateway($method, $data = null){
            try{
				$url = 'http://10.199.100.10:8002/graphapi/'.$method;
				$user = 'hd_console';
				$pass = 'd9UdfWrg4-e5';
				
				$requestOptions = array();
				$requestOptions['url'] = $url;
				$requestOptions['certificate'] = array (
					'username' => $user,
					'password' => $pass
				);
				$requestOptions['body'] = $data;
				$requestOptions['method'] = 'POST';
				$requestOptions['header'] = array(
					"Content-Type: application/json",
					"Content-Length: ".strlen(json_encode($data))
				);
				$request = new HttpRequest($requestOptions);

				$response = $request->send();

				$resData = json_decode($response->reponseText);
				
				$result = false;
				$actionUser = $GLOBALS['userDomain']!=null?$GLOBALS['userDomain']:'';
				$user = (isset($data->account)?$data->account:json_encode(data));
				$actionTime = new DateTime();
				if(isset($resData->error) || isset($resData->errors)){
					$result = false;
				} else {
					$result = true;
				}
				$logData = array(
					Action => $method,
					ActionUser => $actionUser,
					User => $user,
					ActionTime => $actionTime->format('Y-m-d H:i:s'),
					Result => $response->reponseText
				);
				$fac = new AMHelpdeskConsoleLogFactory();
				$fac->insert($logData);

                echo $response->reponseText;
				
			} catch(Exception $e){
				error_log($e->getMessage());
				echo '{error: "Exception"}';
			}
        }

    }
}