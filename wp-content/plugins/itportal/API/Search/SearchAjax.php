<?php

if (!class_exists('SearchAjax')) {

    class SearchAjax {
        public function HomeSearch($params) {
           return $this->searchModel($params);
        }

        public function PageSearch($params) {
            $res = $this->searchModel($params);
            if($res->code != AMResponeCode::$ok)
                return $res;

            import('plugin.itportal.Factory.AMAssetModelDetailFactory');
            $fac = new AMAssetModelDetailFactory();
            $models = $res->data;
            foreach ($models as $model){
                if($model->itemType !== '')
                    continue;

                $modelID = $model->ID;
                $modelDes = $fac->getThubmailsAndShortDescription($modelID);

                $model->img = getFullPath('upload.component.'.$modelID, '', 'url').'/'.$modelDes->Thubmails;
                $model->shortDescription = $modelDes->ShortDescription;;
            }

            return new AMRespone($models);
        }

        function searchModel($params){
            try {

                $keyword = $params['keyword'];
                $startindex = $params['startindex'];
                $limit = $params['limit'];
                $category = $params['category'];

                $re = [];
                if($category == null || $category->product){
                    import("plugin.itportal.API.CMDB.CMDB");
                    $cmdb = new CMDB();
                    $result = $cmdb->get("CMDBEX", ["m", "keyword", "startindex", "limit"], ["SearchProduct", $keyword, $startindex, $limit]);
                    $re = am_json_decode($result);
                }

                return new AMRespone($re);
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }
        }

    }
}

return new SearchAjax();


















