<?php


import("plugin.itportal.Business.Search.RequestSearch");
if (!class_exists('RequestSearchAjax')) {

    class RequestSearchAjax {
        public function GetRequestSearchEditData($params) {
            $id = $params["ID"];

            // check parameter
            if (is_numeric($id))
                $res = (new RequestSearch())->getRequestSearchData($id);
            else
                $res = new AMRespone();

            $refer = new stdClass();
            import('plugin.itportal.Factory.AMRequestDefinitionFactory');
            $fac       = new AMRequestDefinitionFactory();
            $workflows = $fac->getDefines();
            if ($workflows == false)
                return AMRespone::dbFailed(null, 'getWorkflowsAndDefines failed');
            $refer->workflows = $workflows;

            $res->refer = $refer;

            return $res;
        }

        public function GetListRequestSearch($params) {
            $bus = new RequestSearch();
            return $bus->getListRequestSearch();
        }

        public function DeleteRequestSearch($params) {
            $id = $params["ID"];
            // check parameter
            if (is_numeric($id) == false)
                return AMRespone::paramsInvalid();

            $bus = new RequestSearch();
            return $bus->deleteRequestSearch($id);
        }

        public function InsertRequestSearchData($params) {
            return $this->saveRequestSearchData($params, false);
        }

        public function UpdateRequestSearchData($params) {
            return $this->saveRequestSearchData($params, true);
        }

        public function Search($params) {
            // dev_upgrade`.. tạm thời cho modul xuất kho
            {
                $rsID = $params["configID"];
                if ($rsID == 6) {
                    $wfStepIDs = $params["wfStepIDs"];
                    if (!$wfStepIDs)
                        return AMRespone::paramsInvalid();

                    import('plugin.amWorkflow.class.includes.RequestDataTypeID');
                    import("plugin.itportal.Factory.AMRequestSearchFactory");
                    import('plugin.amWorkflow.class.includes.RequestStatusID');
                    $fac = new AMRequestSearchFactory();

                    // query request
                    global $wpdb;
                    $whereWFSID = [];
                    foreach ($wfStepIDs as $wfStepID)
                        $whereWFSID[] = 'sd.ID = ' . $wfStepID;
                    $where = 'RequestStatusID = ' . RequestStatusID::$open . ' AND ' . join(' OR ', $whereWFSID);
                    $query = "select rd.ID as RequestDefinitionID, r.ID as ma_don_hang, rd.DisplayName as ten_quy_trinh, sd.DisplayName as ten_buoc, sd.Role from am_requests as r
	INNER JOIN am_request_definition as rd on r.DefinitionID = rd.ID
    INNER JOIN am_request_step_definitions as sd on sd.RequestDefinitionID = rd.ID AND r.CurrentIndex = sd.StepIndex
    WHERE $where
";
                    $requests = $fac->custom_query2($query);
                    $tempTableStoreRequest = 'temp_store_request';
                    $wpdb->query("DROP TABLE IF EXISTS $tempTableStoreRequest");
                    $wpdb->query("CREATE TEMPORARY TABLE IF NOT EXISTS $tempTableStoreRequest AS ($query)");
                    $requests = $fac->custom_query2($query);
                    if ($requests === false)
                        return AMRespone::dbFailed($requests);

                    logAM($query);

                    if(sizeof($requests) === 0)
                        return new AMRespone($requests);

                    // query fields for table, asset model,
                    $wfIDs = [];
                    foreach ($requests as $request) {
                        if (in_array($request->RequestDefinitionID, $wfIDs) == false)
                            $wfIDs[] = $request->RequestDefinitionID;
                    }
                    $where = '(' . join(', ', $wfIDs) . ')';
                    $query = $wpdb->prepare("
select ID, RequestDefinitionID, RequestDataTypeID, MappingName, ParentID from am_request_field_definition 
where RequestDefinitionID in $where 
  and (RequestDataTypeID = %d or RequestDataTypeID = %d)", RequestDataTypeID::$assetModel, RequestDataTypeID::$table);

                    logAM($query);
                    $fields      = $fac->custom_query2($query);
                    $tableFields = [];
                    foreach ($fields as $field) {
                        if ($field->RequestDataTypeID == RequestDataTypeID::$table) {
                            $tableFields[] = $field;
                        }
                        else {
                            foreach ($fields as $tableName) {
                                if ($field->ParentID == $tableName->ID) {
                                    $tableName->modelField = $field;
                                    break;
                                }
                            }
                        }
                    }

                    // builb select query
                    $caseTable  = [];
                    $caseModels = [];
                    foreach ($tableFields as $tableField) {
                        if ($tableField->modelField == null)
                            continue;

                        $mappingName  = $tableField->modelField->MappingName;
                        $caseTable[]  = "when RequestDefinitionID = $tableField->RequestDefinitionID then fv.$tableField->MappingName";
                        $caseModels[] = "when RequestDefinitionID = $tableField->RequestDefinitionID then td.$mappingName";
                     }

                    if (sizeof($caseTable) == 0)
                        return AMRespone::busFailed($fields, 'no table with column is asset or asset model or asset type');

                    $caseTableInQuery  = join(' ', $caseTable);
                    $caseModelsInQuery  = join(' ', $caseModels);
                    $query        = "
select ma_don_hang, \n\t(case $caseModelsInQuery end) as modelName, COUNT(*) as count
from $tempTableStoreRequest as r
  inner join am_request_fields as fv on r.ma_don_hang = fv.RequestID
  inner join am_request_table_detail_data as td on td.TableID = (case $caseTableInQuery end)
group by ma_don_hang, modelName
";
                    logAM($query);
                    $models = $fac->custom_query2($query);
                    if ($models === false)
                        return AMRespone::dbFailed();

                    foreach ($models as $model) {
                        foreach ($requests as $request) {
                            if ($request->ma_don_hang == $model->ma_don_hang) {
                                $request->models[] = $model->modelName . ($model->count > 1 ? " ($model->count)" : '');
                                break;
                            }
                        }
                    }

                    return new AMRespone($requests);
                }
            }

            $rs        = new RequestSearch();
            $condition = $params['q'];
            if (is_string($condition))
                $condition = json_decode($condition);

            if (!$condition)
                return AMRespone::busFailed(null, 'conditions required');

            $res = $rs->search($params["configID"], $condition);
            return $res;
        }

        function saveRequestSearchData($params, $isUpdate) {
            $data    = $params["data"];
            $columns = $params["columns"];
            $filters = $params["filters"];

            // check parameter
            if ($data == null || is_array($columns) === false)
                return AMRespone::paramsInvalid($params);

            if (is_array($filters) === false)
                $filters = [];

            $bus = new RequestSearch();
            return $bus->saveRequestSearchData($data, $columns, $filters, $isUpdate);
        }
    }
}

return new RequestSearchAjax();

