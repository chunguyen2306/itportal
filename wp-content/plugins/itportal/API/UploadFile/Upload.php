<?php

if(!class_exists('Upload')){

    class Upload{
        function UploadFile($_PARAM){
            error_reporting();
            $fileInfo = $_FILES['fileInfo'];
            $isChangeName = ($_PARAM['isChangeName'] == 'true'?true:false);
            $path = $_PARAM['path'];
            $filename = $_PARAM['fileName'];
            $ext = pathinfo($fileInfo['name'],PATHINFO_EXTENSION);
            $types = ['png', 'gif', 'jpg','jpeg', 'pjeg', 'pdf', 'doc', 'docx', 'xls', 'xlsx'];
            if(!in_array($ext,$types)){
                return array(
                    'State' => false,
                    'Mess'=>'Định dạng file không được phép',
                    'Code' => 405
                );
            }
            switch ($_FILES['fileInfo']['error']) {
                case UPLOAD_ERR_OK:
                    $Code=200;
                    $Mess='Upload File thành công';
                    break;
                case UPLOAD_ERR_NO_FILE:
                    $Mess ='Không tìm thấy file upload';
                    $Code = 404;
                    break;
                case UPLOAD_ERR_INI_SIZE:
                    $Mess ='File vươt quá kích thước cho phép';
                    $Code = 494;
                    break;
                case UPLOAD_ERR_EXTENSION:
                    $Mess ='Định dạng file không được phép';
                    $Code = 405;
                    break;
                case UPLOAD_ERR_FORM_SIZE:
                   $Mess ='File vươt quá kích thước cho phép';
                   $Code = 494;
                   break;
                default:
                    throw new RuntimeException('Unknown errors.');
            }
            $fullPath = getFullPath($path,'');
            $fullName = '';

            if($isChangeName) {
                $fullName = $filename . '.' . $ext;
            } else {
                $fullName = $_FILES['fileInfo']['name'];
            }
//            error_log('Is Change Name: '.$isChangeName);
//            error_log('Full Name: '.$fullName);

            if (!file_exists($fullPath)) {
                mkdir($fullPath, 0777, true);
            }

            if (move_uploaded_file($_FILES['fileInfo']['tmp_name'], $fullPath.'/'.$fullName)) {
                return array(
                    State => true,
                    Result => $fullName,
                    'Code'=>$Code,
                    'Mess'=>'Upload thành công'
                );
            } else {
                return array(
                    'State' => false,
                    'Mess' => $Mess,
                    'Code' => $Code
                );
            }
        }

        private function putToServer($file, $des){
            $ftp_host = 'your-ftp-host.com'; /* host */
            $ftp_user_name = 'ftp-username@your-ftp-host.com'; /* username */
            $ftp_user_pass = 'ftppassword'; /* password */
        }
    }
}
return new Upload();