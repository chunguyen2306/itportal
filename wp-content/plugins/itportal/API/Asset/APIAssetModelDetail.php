<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if(!class_exists('APIAssetModelDetail')){

    import('plugin.itportal.Factory.AMAssetModelDetailFactory');


    class APIAssetModelDetail {

        private $fac = null;
        private $apiComponent = null;

        function __construct()
        {
            $this->fac = new AMAssetModelDetailFactory();
            $this->apiComponent = using('plugin.itportal.API.CMDB.Component');
        }

        public function GetAll(){

            $result = $this->fac->query();
            return $result;
        }

        public function GetThubmailsPath($PARAM){

            $ModelID = $PARAM['modelID'];
            $thubmails = $this->fac->getThubmails($ModelID);
            if($thubmails != '' || $thubmails != null) {
                $path = getFullPath('upload.component.' . $ModelID, '') . '/' . $thubmails;
            } else {
                $path = null;
            }

            return $path;
        }

        public function GetThubmails($PARAM){

            $ModelID = $PARAM['ModelID'];
            $thubmails = $this->fac->getThubmails($ModelID);
            $path = getFullPath('upload.component.'.$ModelID, '').'/'.$thubmails;

            $data = readfile($path);
            header("Content-Type: ".mime_content_type($path));
            header('Content-Length: ' . filesize($path));
            header("Cache-Control: max-age=86400");
            header_remove('Link');
            header_remove('X-Powered-By');
            echo $path;
        }

        public function DeleteGallery($PARAM){
            $galleryName = $PARAM['galleryName'];
            $modelID = $PARAM['modelID'];

            unlink(getFullPath('upload.component.'.$modelID, '').'/'.$galleryName);
        }

        public function GetByID($PARAM){
            $ID = $PARAM['ID'];
            $result = $this->fac->getByID($ID);
            return $result;
        }

        public function GetByModelID($PARAM){
            $ID = $PARAM['ModelID'];
            $result = $this->fac->getByModelID($ID);
            return $result;
        }

        public function AddNew($PARAM){
            global $wpdb;
            $data = $PARAM['data'];

            $data['Gallery'] = json_encode($data['Gallery']);
            $data['Description'] = stripcslashes($data['Description']);
            $data['ShortDescription'] = stripcslashes($data['ShortDescription']);
            $data['ManualGuide'] = stripcslashes($data['ManualGuide']);

            $result = $this->fac->insert($data);
            $id = $wpdb->insert_id;
            $data = $this->GetByID(array('ID'=>$id));
            if($result === 1){
                $result = true;
            } else {
                $result = false;
            }
            return array(
                State => $result,
                Record => $data
            );
        }

        public function Update($PARAM){
            global $wpdb;
            $wpdb->show_errors = true;
            $wpdb->suppress_errors = false;
            $data = $PARAM['data'];
            $data['Gallery'] = json_encode($data['Gallery']);
            $data['Description'] = stripcslashes($data['Description']);
            $data['ShortDescription'] = stripcslashes($data['ShortDescription']);
            $data['ManualGuide'] = stripcslashes($data['ManualGuide']);

            $result = $this->fac->update($data, array(
                'ID' => $data['ID']
            ));

            if($result === 1){
                $result = true;
            } else {
                $result = false;
            }
            return array(
                State => $result,
                Record => $data
            );
        }

        public function Delete($PARAM){
            $data = $PARAM['data'];
            $result = $this->fac->delete(array(
                'ID' => $data['ID']
            ));
            if($result === 1){
                $result = true;
            } else {
                $result = false;
            }
            return array(
                State => $result,
                Record => $data
            );
        }

        private function convertArrayToInClause($array){
            $result = json_encode($array);
            $result = str_replace('[', '(', $result) ;
            $result = str_replace(']', ')', $result) ;
            return $result;
        }

        public function GetByMappingList($PARAM){

            $filterMap = [
                'p' => 'TypeID',
                'm' => 'ManufacturerName',
                'l' => 'IsStandard',
                'a' => 'NeedApproval',
                'y' => 'ReNewYear'
            ];
            $filterArr = [];
            $isStock = false;

            $mapping = $PARAM['mapping'];
            $filter = json_decode($PARAM['filter']);
            $pageIndex = $PARAM['pageIndex']*1;
            $limit = $PARAM['limit']*1;

            $mapping = str_replace('[', '(', $mapping) ;
            $mapping = str_replace(']', ')', $mapping) ;

            foreach ($filter as $item){
                $filMap = substr($item, 0,1);
                $val = substr($item, 2);
                if($filMap === 'l'){
                    $isStock = true;
                    continue;
                }
                if(!is_array($filterArr[$filterMap[$filMap]])){
                    $filterArr[$filterMap[$filMap]] = [];
                }

                array_push($filterArr[$filterMap[$filMap]], $val);
            };

            $whereClause = [];

            array_push($whereClause, [
                "IsDisplay = 1", "AND"
            ]);

            if(!isset($filterArr[$filterMap['p']])){
                array_push($whereClause, [ "TypeID in $mapping", "AND" ]);
            }
            foreach ($filterMap as $item){
                if(isset($filterArr[$item])){
                    array_push($whereClause, ["$item in ".$this->convertArrayToInClause($filterArr[$item]) , "AND"]);
                }
            }

            $whereClause[sizeof($whereClause)-1][1] = "";

            $total = $this->fac->getOne([
                'select' => "count(*) 'Total'",
                'filter' => $whereClause,
            ])->Total;

            $result = $this->fac->query([
                'filter' => $whereClause
            ], true);

            $ids = array_map(function ($e){
                return $e->ModelID;
            }, $result);

            $cmdbComponent = json_decode($this->apiComponent->GetByIDsAndFilter([
                'ids' => json_encode($ids),
                'isStock' => $isStock
            ]))->Result;

            $mapResult = [];

            foreach ($result as $componentItem){
                $component = array_filter($cmdbComponent, function ($i) use ($componentItem){
                   return $i->COMPONENTID === $componentItem->ModelID;
                });

                if(sizeof($component) > 0){
                    $component = array_values($component);
                    $componentItem = (object)array_merge((array)$component[0], (array)$componentItem);
                    $componentItem->OtherAsset = 0;

                    array_push($mapResult,$componentItem);
                }
            }

            /*for ($i = 0; $i < sizeof($result); $i++){
                $item = $result[$i];
                $cmdbInfo = json_decode($apiComponent->GetByID([
                    'id' => $item->ModelID
                ]))->Result;
                if($cmdbInfo !== null) {
                    $quantityInHand = $apiComponent->GetQuantityInHand([
                        'modelID' => $item->ModelID
                    ]);
                    $item = (object)array_merge((array)$item, (array)$cmdbInfo);
                    $quantityInHand = json_decode($quantityInHand)->Result;

                    $item->Stock = $quantityInHand * 1;
                    $item->OtherAsset = 0;

                } else {
                    $item->IsDisplay = 0;
                }
                $result[$i] = $item;

            }*/

            return (object)[
                'Result' => (object)[
                    'Products' => array_slice($mapResult, $pageIndex*$limit, $limit),
                    'Total' => sizeof($mapResult),
                    'PageCount' => ceil(sizeof($mapResult)/$limit)
                ],
                'State' => true,
                'Error' => ''
            ];
        }
    }
}
return new APIAssetModelDetail();