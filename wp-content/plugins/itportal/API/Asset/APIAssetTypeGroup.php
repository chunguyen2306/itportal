<?php
/**
 * Created by PhpStorm.
 * User: LAP10623
 * Date: 07/09/2018
 * Time: 17:58
 */

if (!class_exists('APIAssetTypeGroup')) {
    import('plugin.itportal.Factory.AMAssetTypeGroupFactory');
    import('plugin.itportal.Factory.AMAssetDiscountCostPerYearFatory');


    class APIAssetTypeGroup
    {
        private  $fac = null ;
        private $disCountPerYearFac = null;


        function __construct()
        {
            $this->fac = new AMAssetTypeGroupFactory();
            $this->disCountPerYearFac = new AMAssetDiscountCostPerYearFatory();
        }

        public function getByAssetType($PARAM)        {
            $assetType = $PARAM['assettype'];
            return $this->fac->getByAssetType($assetType);
        }

        public function getByDefault()
        {
            return $this->fac->getByDefault();
        }
        public function  getAll($PARAM){
            return $this->fac->query(array('select'));
        }
        public function getByGroupName($PARAM){
            $groupName = $PARAM['groupName'];
            return $this->fac->query(array(
                "filter"=> array(
                    array("groupname =  '$groupName'",'')
                )
            ));
        }
        public function  getAllandPercent($PARAM){
            $table =$this->fac->custom_query2("select tg.id,
tg.groupname,
tg.assettype, 
tg.maxmonth, 
CONCAT('[',GROUP_CONCAT(json_object('Year', dy.Year, 'Percent', dy.Percent)),']') as PercentList
from am_asset_type_group tg 
join am_discountperyear dy on tg.id = dy.AssetTypeGroupID
group by tg.id, tg.assettype, tg.maxmonth,tg.groupname");
            return $table;
        }
        public function update($PARAM)
        {

            $data = $PARAM['data'];

            /*$discountPerYears = json_decode(stripslashes($data['PercentList']), ARRAY_A);
            $typeGroupData = [
                id => $data['id'],
                assettype => $data['assettype'],
                maxmonth => $data['maxmonth'],
                groupname => $data['groupname'],
            ];*/
            $discountPerYears = json_decode(stripslashes($data['PercentList']), ARRAY_A);
            unset($data['PercentList']);
            $typeGroupData = $data;

            $result= $this->fac->update($typeGroupData, [
                "id"=>$typeGroupData['id']
            ]);

            if($result === 0 || $result === 1){

                $this->disCountPerYearFac->delete([
                    "AssetTypeGroupID"=>$typeGroupData['id']
                ]);

                foreach($discountPerYears as $discountPerYear){
                    $discountPerYear['AssetTypeGroupID'] = $typeGroupData['id'];
                    $result += $this->disCountPerYearFac->insert($discountPerYear);
                }
                if($result >= 0){
                    $result = true;
                } else {
                    $result = false;
                }
            } else {
                $result = false;
            }
            return array(
                State => $result,
                Result => $data
            );
        }


        public function AddNew($PARAM)
        {
            global $wpdb;
            $data = $PARAM['data'];
            /*$typeGroupData = [
                assettype => $data['assettype'],
                maxmonth => $data['maxmonth'],
                groupname => $data['groupname'],
            ];*/

            $discountPerYears = json_decode(stripslashes($data['PercentList']), ARRAY_A);
            unset($data['PercentList']);
            $typeGroupData = $data;
            $result = $this->fac->insert($typeGroupData);
            $id = $wpdb->insert_id;
            $data = $this->fac->GetByID(array('id'=>$id));
            if($result === 1){
                foreach($discountPerYears as $discountPerYear){
                    $discountPerYear['AssetTypeGroupID'] = $id;
                    $result += $this->disCountPerYearFac->insert($discountPerYear);
                }
                if($result > 1){
                    $result = true;
                } else {
                    $result = false;
                }
            } else {
                $result = false;
            }
            return array(
                State => $result,
                Result => $data
            );
        }

        public function deletegroup($PARAM){
            $result = $this->fac->delete([
                "id"=>$PARAM['id']
            ]);
            return [
                State=>$result===1?true:false
            ];
        }
    }

}
return new APIAssetTypeGroup();
?>