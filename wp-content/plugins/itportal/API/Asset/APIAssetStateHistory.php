<?php

if(!class_exists('APIAssetStateHistory')){
    import('plugin.itportal.Factory.AMAssetStateHistoryFactory');
    class APIAssetStateHistory
    {

        private $fac = null;

        function __construct()
        {
            $this->fac = new AMAssetStateHistoryFactory();

        }

        public function GetAssetStateHistory($params)
        {
            $assetName = $params['assetName'];

            $result = $this->fac->query(array(
                'filter' => array(
                    array("assetName = '" . $assetName . "'", 'AND'),
                    array("StateId<>0", '')
                )
            ));

            return $result;
        }

        public function GetAssetCommentHistory($params)
        {
            $assetName = $params['assetName'];

            $result = $this->fac->query([
                'filter' => [
                    ["AssetName = '$assetName'", "AND"],
                    ["StateId=0", ""]
                ]
            ]);

            return $result;
        }

        public function GetAssetStateHistoryByCMDBHistoryID($params)
        {
            $cmdbHistoryID = $params['CMDBHistoryID'];

            $result = $this->fac->query(array(
                'filter' => array(
                    array("StateId = " . $cmdbHistoryID, '')
                )
            ));

            return sizeof($result) ? $result : new stdClass();
        }

        public function InsertStateHistory($params)
        {
            global $wpdb;
            try {
                $jsonData = $params['data'];
                if ($jsonData == null)
                    return AMRespone::paramsInvalid();
                $data = am_json_decode($jsonData, 1);
                if ($data == null)
                    return AMRespone::jsonFailed($jsonData);

                $data['sysComment'] = am_json_encode($data['sysComment']);
                $result = $this->fac->insert($data);
                $id = $wpdb->insert_id;
                $data['id'] = $id;
                if ($result || $result === 0)
                    return new AMRespone($data);
                else
                    return AMRespone::dbFailed($data);
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }
        }

        public function UpdateSysComment($params)
        {
            try {
                $data = $params['data'];

               /* if(data['id'] === null|| data['id'] === ''|| data['id'] === undefined|| data['id']===0){
                    return AMRespone::paramsInvalid();
                }*/
                $data['sysComment'] = json_encode($data['sysComment']);
                $result = $this->fac->update($data, array(
                    'ID' => $data['id']
                ));
                if($result === 1){
                    $result = true;
                } else {
                    $result = false;
                }
                return array(
                    State => $result,
                    Record => $data
                );
            } catch (Exception $ex) {
                return AMRespone::exception($data, $ex);
            }

        }

    }
}
return new APIAssetStateHistory();


















