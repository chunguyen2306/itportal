<?php
/**
 * Created by PhpStorm.
 * User: LAP10623
 * Date: 07/10/2018
 * Time: 11:13
 */
if(!class_exists('APIAssetDiscountPercentYear')){
    import ('plugin.itportal.Factory.AMAssetDiscountCostPerYearFatory');
class APIAssetDiscountPercentYear
{
    private $fac;
    public function __construct()
    {
        $this->fac = new AMAssetDiscountCostPerYearFatory();
    }

    public function getTotalPercentByAssetGroupID($PARAM){
        $roundYear = $PARAM['roundyear'];
        $GroupID = $PARAM['groupid'];

        return $this->fac->
        custom_query2("select sum(Percent) TotalPer from am_discountperyear where Year< @Year and AssetTypeGroupID =@AssetTypeGroupID",
            ['@Year'=>$roundYear,
                '@AssetTypeGroupID'=>$GroupID]);
    }
    public function getYearPercentByAssetGroupID($PARAM){
        $roundYear = $PARAM['year'];
        $GroupID = $PARAM['groupid'];

        return $this->fac->query(array(
            'filter' => array(
                array("Year = '$roundYear'",'AND'),
                array("AssetTypeGroupID = '$GroupID'",'')
            )
        ));

    }
    public function getByGroupID($PARAM){
    $groupId = $PARAM['groupid'];
    return $this->fac->query(array(
        'filter'=>array(
            array("AssetTypeGroupID = '$groupId'",'')
        )
    ));
    }
    public function  update($PARAM){
        $groupID = $PARAM['AssetTypeGroupID'];
        $year = $PARAM['Year'];
        $percent = $PARAM["Percent"];
        $result= $this->fac->update($PARAM,
            ["AssetTypeGroupID"=>$groupID,
                "Year"=>$year
            ]);
        return [
            State=>$result!==0?true:false
        ];
    }
    public  function  addNew($PARAM){

        $groupID = $PARAM['AssetTypeGroupID'];
        $year = $PARAM['Year'];
        $percent = $PARAM["Percent"];
        $result= $this->fac->insert($PARAM);
        return [
            State=>$result!==0?true:false
        ];
    }
    public function deletegroup($PARAM){
        $result=$this->fac->delete([
            "AssetTypeGroupID"=>$PARAM['AssetTypeGroupID']
        ]);
        return [
            State=>$result!==0?true:false
        ];
    }
}}
    return new APIAssetDiscountPercentYear();
?>