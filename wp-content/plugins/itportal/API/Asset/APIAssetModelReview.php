<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if(!class_exists('APIAssetModelReview')){

    import('plugin.itportal.Factory.AMAssetModelReviewFactory');

    class APIAssetModelReview {

        private $fac = null;

        function __construct()
        {
            $this->fac = new AMAssetModelReviewFactory();
        }

        public function GetAll(){

            $result = $this->fac->query();
            return $result;
        }


        public function GetByID($PARAM){
            $ID = $PARAM['ID'];
            $result = $this->fac->getByID($ID);
            return $result;
        }

        public function GetByModelID($PARAM){
            $ID = $PARAM['ModelID'];
            $result = $this->fac->getByModelID($ID);
            return $result;
        }
        public function GetByAssetName($PARAM){
            $ID = $PARAM['AssetName'];
            $result = $this->fac->getByAssetName($ID);
            return $result;
        }

        public function AddNew($PARAM){
            global $wpdb;
            $data = $PARAM['data'];

            $data['Comment'] = urldecode($data['Comment']);

            $result = $this->fac->insert($data);

            $id = $wpdb->insert_id;
            $data = $this->GetByID(array('ID'=>$id));
            if($result === 1){
                $result = true;
            } else {
                $result = false;
            }
            return array(
                State => $result,
                Record => $data
            );
        }

        public function Update($PARAM){
            $data = $PARAM['data'];
            $data['Comment'] = urldecode($data['Comment']);

            $result = $this->fac->update($data, array(
                'ID' => $data['ID']
            ));
            if($result === 1){
                $result = true;
            } else {
                $result = false;
            }
            return array(
                State => $result,
                Record => $data
            );
        }

        public function Delete($PARAM){
            $data = $PARAM['data'];
            $result = $this->fac->delete(array(
                'ID' => $data['ID']
            ));
            if($result === 1){
                $result = true;
            } else {
                $result = false;
            }
            return array(
                State => $result,
                Record => $data
            );
        }
    }
}
return new APIAssetModelReview();