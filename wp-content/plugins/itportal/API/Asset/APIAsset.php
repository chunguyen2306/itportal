<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if(!class_exists('APIAsset')){

    import('plugin.itportal.Factory.AMAssetModelDetailFactory');
    import('plugin.itportal.API.CMDB.Component');

    class APIAsset {

        private $assetFac = null;
        private $cmdbAPI = null;

        function __construct()
        {
            $this->assetFac = new AMAssetModelDetailFactory();
            $this->cmdbAPI = new Component();
        }

        function GetProductSlideByType($PARAM){
            $products = json_decode($this->cmdbAPI->GetProductSlideByType($PARAM))->Result;
            if(!is_array($products)){
                $products = array();
            }
            foreach ($products as $product){
                for($i = sizeof($product->Components)-1; $i >= 0; $i--){
                    $component = $product->Components[$i];
                    $detail = $this->assetFac->getByModelID($component->COMPONENTID);
                    if($detail === null){
                        unset($product->Components[$i]);
                        continue;
                    }
                    $stock = $this->cmdbAPI->GetQuantityInHand(['modelID'=>$component->COMPONENTID]);
                    $stock = json_decode($stock);
                    $component->Detail = $detail;
                    $component->Stock = $stock->Result;
                }
            }
            return $products;
        }

        function GetProductByMostUse(){
            $products = json_decode($this->cmdbAPI->GetByMostUse(array()))->Result;
            if(!is_array($products)){
                $products = array();
            }
            foreach ($products as $product){
                foreach ($product->Components as $component) {
                    $detail = $this->assetFac->getByModelID($component->COMPONENTID);
                    $stock = $this->cmdbAPI->GetQuantityInHand(['modelID'=>$component->COMPONENTID]);
                    $stock = json_decode($stock);
                    $component->Detail = $detail;
                    $component->Stock = $stock->Result;
                }
            }

            return $products;
        }

        /**
         * @Permission
         *      Feature: admin
         *      DepHead: true
         */
        function GetAssetModel($params){
            // lấy tên tài sản
            $assetName = $params["assetName"];

        }

    }
}
return new APIAsset();