<?php
if ( ! function_exists( 'import' ) ) {
    require_once get_template_directory() . '/package/import.php';
}

import('theme.package.class.ArrayFilterWithProperty');
import('plugin.amWorkflow.class.includes.RequestDataTypeID');
import('plugin.itportal.Factory.AMRequestDefinitionFactory');
import('plugin.itportal.Factory.AMRequestFieldDefinitionFactory');
import('plugin.itportal.Factory.AMRequestStepDefinitionFactory');
import('plugin.itportal.Factory.AMRequestFieldFactory');
import('plugin.itportal.Factory.AMRequestStepFactory');
import('plugin.itportal.Factory.AMRequestFactory');
import('plugin.itportal.Factory.AMRequestTableDetailData');

if (!class_exists('AMRequestFilterAjax')) {

    class AMRequestFilterAjax{

        private $requestDefinitionFac = null;
        private $requestFieldDefinitionFac = null;
        private $requestStepDefinitionFactory = null;
        private $requestFieldFac = null;
        private $requestStepFactory = null;
        private $requestFac = null;
        private $requestTableDetailDataFac = null;

        public function __construct(){
            $this->requestDefinitionFac = new AMRequestDefinitionFactory();
            $this->requestFieldDefinitionFac = new AMRequestFieldDefinitionFactory();
            $this->requestStepDefinitionFactory = new AMRequestStepDefinitionFactory();
            $this->requestFieldFac = new AMRequestFieldFactory();
            $this->requestStepFactory = new AMRequestStepFactory();
            $this->requestFac = new AMRequest();
            $this->requestTableDetailDataFac = new AMRequestTableDetailData();
        }

        public function getView($param){
            $class = new ReflectionClass('AMRequestFilterAjax');
            return urldecode($class->getMethod('filter')->getDocComment());
        }

        /**
         * Filter request
         *
         * @param $args {
         *   select => ['multiple', 'Danh sÃ¡ch cá»™t'],
         *   requests => ['combobox', 'Loáº¡i yÃªu cáº§u'],
         *   assetName => ['text', 'TÃªn tÃ i sáº£n'],
         *   assetType => ['combobox', 'Loáº¡i tÃ i sáº£n'],
         *   assetModel => ['combobox', 'Model tÃ i sáº£n'],
         *   requestOwner => ['text', 'NgÆ°á»i yÃªu cáº§u'],
         *   requestStatus => ['combobox', 'Trang thÃ¡i yÃªu cáº§u'],
         *   stepStatus => ['combobox', 'Trang thÃ¡i bÆ°á»›c duyá»‡t'],
         *   pendingAt => ['text', 'YÃªu cáº§u Ä‘ang chá»'],
         *   personInFlow => ['text', 'NgÆ°á»i duyá»‡t'],
         *   createTime => ['daterange', 'NgÃ y táº¡o'],
         *   finishTime => ['daterange', 'NgÃ y hoÃ n táº¥t']
         *   isApproved => ['text', 'Đã duyệt hay chưa']
         * }
         * @return object
         */
        public function filter($args){


            global$wpdb;
            $requests = [];
            $fields = [];
            $step = [];
            $where = [];
            $select = '';
            $join = [];
            $from = '';
            $maxStep = 0;
            $filterStye = isset($args['filterType'])?$args['filterType']:'AND';
            $requestTableFieldMap = [];
            $total = 0;
            $pageCount = 0;
            $isOwned = ($args['isOwned']==='true')?'AND':'OR';

            if(isset($args['limit'])) $args['limit'] = $args['limit'] + 0;
            if(isset($args['pageIndex'])) $args['pageIndex'] = $args['pageIndex'] + 0;

            if(isset($args['requests'])){
                $requests = $this->requestDefinitionFac->GetByListIDOrName($args['requests']);
            } else {
                $requests = $this->requestDefinitionFac->GetAll();
            }

            foreach ($requests as $request){
                $listFieldDefinitions = $this->requestFieldDefinitionFac->getListByRequestDefinitionID($request->ID);
                foreach ($listFieldDefinitions as $fieldDefinition){
                    $fieldDefinition->Config = am_json_decode($fieldDefinition->Config);

                }
                $request->Fields = $listFieldDefinitions;
                $fields = array_merge($fields, $listFieldDefinitions);
            }

            $tableFields = array_filter($fields, [
                    new ArrayFilterWithProperty('RequestDataTypeID', RequestDataTypeID::$table),
                    'equal']
            );
            $arr = [];


            if(isset($args['select'])){

                foreach ($args['select'] as $selectItem){
                    $regex = '/^{{(.+)}}$/';
                    preg_match($regex, $selectItem, $matches);
                    if(sizeof($matches) > 0){
                        $assetTypeFields = array_filter($fields, [
                                new ArrayFilterWithProperty('RequestDataTypeID', RequestDataTypeID::$assetType),
                                'equal']
                        );
                        $select = $select.' case';
                        foreach($assetTypeFields as $assetTypeField){
                            $select = $select.' when a.DefinitionID = '.$assetTypeField->RequestDefinitionID.' then reTableData.'.$assetTypeField->MappingName;
                        }
                        $select = $select.' end as `'. $wpdb->_escape($matches[1]) .'`,';
                    } else {
                        $select = $select . '`' . $wpdb->_escape(stripslashes($selectItem)) . '`, ';
                    }
                }
                $select = rtrim($select);
                $select = substr( $select, 0, sizeof($select) - 2);
                $select = 'distinct '.$select;
            } else {
                $select = "DISTINCT reStatus.*, a.*";
                /*$select = "DISTINCT a.ID 'RequestID', " .
                    "a.*, " .
                    "reStep.ID 'StepID', " .
                    "reStep.*, " .
                    "reField.ID 'FieldID', " .
                    "reField.*, " .
                    "reStatus.ID 'StatusID', " .
                    "reStatus.*";*/
            }
            $from = ",am_request_table_data reTable, am_request_table_detail_data reTableData";
            $join = [
                ["LEFT JOIN", "am_request_fields reField", "a.ID", "reField.RequestID"],
                ["LEFT JOIN", "am_request_status reStatus", "a.RequestStatusID", "reStatus.ID"],
                ["LEFT JOIN", "am_request_steps reStep", "a.ID", "reStep.RequestID"],
                ["LEFT JOIN", "am_request_step_definitions reStepDefined", "reStepDefined.ID", "reStep.DefinitionID"],
                ["LEFT JOIN", "am_request_table_data reTable", "a.ID", "reTable.RequestID"],
                ["LEFT JOIN", "am_request_table_detail_data reDetailTable", "reDetailTable.TableID", "reTable.ID"],
                ["LEFT JOIN", "am_request_status reStepStatus", "reStepDefined.RequestStatusID", "reStepStatus.ID"]
            ];
            foreach($tableFields as $tableField){
                /*$joinName = $tableField->RequestDefinitionID."_".$tableField->Name;
                $joinName = preg_replace('/[^a-zA-Z0-9_]/','', $joinName);
                array_push($join, ["LEFT JOIN", "am_request_table_detail_data $joinName",
                    "$joinName.TableID", "reField.$tableField->MappingName"]);*/
                $requestTableFieldMap[$tableField->RequestDefinitionID] = $tableField->MappingName;
                //unset($joinName);
            }

            if(isset($args['assetname'])){

                $assetNameFields = array_filter($fields, [
                        new ArrayFilterWithProperty('RequestDataTypeID', RequestDataTypeID::$asset),
                        'equal']
                );

                $str = '';

                foreach($assetNameFields as $assetNameField){
                    if(isset($assetNameField->ParentID) && $assetNameField->ParentID > 0) {
                        $str = $str." reDetailTable.$assetNameField->MappingName like '%" . $args['assetname'] . "%' OR";
                    } else {
                        $str = $str." reField.$assetNameField->MappingName like '%" . $args['assetname'] . "%' OR";
                    }
                }
                $str = rtrim($str, 'OR');

                array_push($where, ["($str)", $filterStye]);

                unset($str);
            }
            if(isset($args['assetType'])){
                $assetTypeFields = array_filter($fields, [
                        new ArrayFilterWithProperty('RequestDataTypeID', RequestDataTypeID::$assetType),
                        'equal']
                );

                $str = '';

                foreach($assetTypeFields as $assetTypeField){

                    if(isset($assetTypeField->ParentID) && $assetTypeField->ParentID > 0) {
                        $str = $str." reDetailTable.$assetTypeField->MappingName like '%" . $args['assetType'] . "%' OR";
                    } else {
                        $str = $str." reField.$assetTypeField->MappingName like '%" . $args['assetType'] . "%' OR";
                    }
                }

                $str = rtrim($str, 'OR');

                array_push($where, ["($str)", $filterStye]);

                unset($str);
            }
            if(isset($args['assetModel'])){
                $assetModelFields = array_filter($fields, [
                        new ArrayFilterWithProperty('RequestDataTypeID', RequestDataTypeID::$assetModel),
                        'equal']
                );

                $str = '';

                foreach($assetModelFields as $assetModelField){
                    if(isset($assetModelField->ParentID) && $assetModelField->ParentID > 0) {
                        $str = $str." reDetailTable.$assetModelField->MappingName like '%" . $args['assetModel'] . "%' OR";
                    } else {
                        $str = $str." reField.$assetModelField->MappingName like '%" . $args['assetModel'] . "%' OR";
                    }
                }

                $str = rtrim($str, 'OR');

                array_push($where, ["($str)", $filterStye]);

                unset($str);
            }
            if(isset($args['requestOwner'])){
                foreach ($requests as $request){
                    $ownerFields = array_filter($fields, function($field) use ($request){
                        return $field->Name === $request->OwnerField && $field->RequestDefinitionID === $request->ID;
                    });
                    foreach($ownerFields as $ownerField){
                        array_push($where, ["(a.DefinitionID = ". $request->ID ." AND reField.$ownerField->MappingName = '" . $args['requestOwner'] . "')", 'OR']);
                    }
                    unset($ownerFields);
                }
            }

            if(isset($args['requestID'])){
                array_push($where, ["a.ID = ".$args['requestID'], " AND"]);
            }

            if(isset($args['requests'])){
                array_push($where, ["a.DefinitionID IN (".join(',',$args['requests']). ")", "AND"]);
            }

            if (isset($args['pendingAt'])) {
                if ($args['pendingAt'] === '{{me}}') {
                    $args['pendingAt'] = wp_get_current_user()->user_login;
                }
                array_push($where, ["(a.CurrentIndex = reStepDefined.StepIndex" .
                    " AND reStep.Owner = '" . $args['pendingAt'] . "')", $filterStye]);
            } else if (isset($args['personInFlow'])) {
                if ($args['personInFlow'] === '{{me}}') {
                    $args['personInFlow'] = wp_get_current_user()->user_login;
                }
                if (isset($args['isApproved'])) {
                    array_push($where, ["reStep.IsApproved = '1' AND reStepDefined.StepIndex != 0", $filterStye]);
                }
                array_push($where, ["reStep.Owner = '" . $args['personInFlow'] . "'", $filterStye]);
            }

            if(isset($args['pendingTime'])){
                array_push($where, ["(a.CurrentIndex = reStepDefined.StepIndex" .
                    " AND reStep.StartDate >= '".$args['pendingTime'][0].
                    "' AND reStep.StartDate <= '".$args['pendingTime'][1]."')", $filterStye]);
            }

            if(isset($args['createTime'])){
                array_push($where, ["(reField.Createdate >= ".$args['createTime'][0].
                    " AND reField.Createdate <= ".$args['createTime'][1].")", $filterStye]);
            }

            if(isset($args['finishTime'])){
                array_push($where, ["(reStatus.Name = 'done'" .
                    " AND reField.CurrentIndex = reStep.StepIndex" .
                    " AND reStep.ActionDate >= ".$args['finishTime'][0].
                    " AND reStep.ActionDate <= ".$args['finishTime'][1].")", $filterStye]);
            }

            if(isset($args['requestStatus__AND'])){
                array_push($where, ["(reStatus.DisplayName = '".$args['requestStatus__AND']."'" .
                    " OR reStatus.Name = '".$args['requestStatus__AND']."')", "AND"]);
            }else if(isset($args['requestStatus'])){
                array_push($where, ["(reStatus.DisplayName = '".$args['requestStatus']."'" .
                    " OR reStatus.Name = '".$args['requestStatus']."')", "$filterStye"]);
            }

            if(isset($args['stepStatus__AND'])){
                array_push($where, ["(reStepStatus.DisplayName = '".$args['stepStatus__AND']."'" .
                    " OR reStepStatus.Name = '".$args['stepStatus__AND']."'" .
                    " AND reStepDefined.StepIndex = a.CurrentIndex)", "AND"]);
            } else if(isset($args['stepStatus'])){
                array_push($where, ["(reStepStatus.DisplayName = '".$args['stepStatus__AND']."'" .
                    " OR reStepStatus.Name = '".$args['stepStatus__AND']."'" .
                    " AND reStepDefined.StepIndex = a.CurrentIndex)", "$filterStye"]);
            }

            $result = $this->requestFac->query([
                'select'=> $select,
                'join'=> $join,
                'where'=> $where,
                'order'=>"a.CreateDate desc"
            ]);

            $total = sizeof($result);

            if($total === 0){
                return (object)[
                    Result => $result,
                    Total => 0,
                    PageCount => 0
                ];
            }

            $pageCount = ceil($total/$args['limit']);

            $result = array_slice($result, $args['pageIndex']*$args['limit'],$args['limit']);

            $requestIDs = array_map(function ($e){
                return $e->ID;
            }, $result);

            $requestDefineIDs = array_map(function ($e){
                return $e->DefinitionID;
            }, $result);

            $fieldResult = $this->requestFieldFac->query([
                'filter'=>[
                    ["RequestID in (".join(",", $requestIDs).")",""]
                ]
            ]);

            $stepResult = $this->requestStepFactory->query([
                'select'=>'b.*, a.*',
                'join'=>[
                    [ "JOIN" , AMRequestStepDefinitionFactory::$tableName." b", "a.DefinitionID", "b.ID"]
                ],
                'filter'=>[
                    ["RequestID in (".join(",", $requestIDs).")",""]
                ]
            ]);

            $requestDefineResult = $this->requestDefinitionFac->query([
                'filter'=>[
                    ["ID in (".join(",", $requestDefineIDs).")",""]
                ]
            ]);

            $requestDefineResultIDs = array_map(function ($e){
                return $e->ID;
            }, $requestDefineResult);

            $fieldDefineResult = $this->requestFieldDefinitionFac->query([
                'filter'=>[
                    ["RequestDefinitionID in (".join(",", $requestDefineResultIDs).")",""]
                ]
            ]);

            $stepDefineResult = $this->requestStepDefinitionFactory->query([
                'filter'=>[
                    ["RequestDefinitionID in (".join(",", $requestDefineResultIDs).")",""]
                ]
            ]);

            $tableMap = array_map(function ($e) use ($result, $requestTableFieldMap){

                $currRequest = array_values(array_filter($result, function($k) use ($e){
                    return $e->RequestID == $k->ID;
                }))[0];
                $fieldName = $requestTableFieldMap[$currRequest->DefinitionID];
                $e = get_object_vars($e);
                return $e[$fieldName];
            }, $fieldResult);
            $tableMap = array_filter($tableMap);

            $tableDataResult = $this->requestTableDetailDataFac->query([
                'filter'=>[
                    ["TableID in (".join(",", $tableMap).")",""]
                ]
            ]);

            foreach ($result as $item){
                $item->Definition = array_values(array_filter($requestDefineResult, function($e) use ($item){
                    return $e->ID == $item->DefinitionID;
                }));
                if(sizeof($item->Definition) > 0){
                    $item->Definition = $item->Definition[0];
                } else {
                    $item->Definition = null;
                }

                $item->Fields = array_values(array_filter($fieldResult, function($e) use ($item){
                    return $e->RequestID == $item->ID;
                }));

                if(sizeof($item->Fields) > 0){
                    $item->Fields = $item->Fields[0];
                } else {
                    $item->Fields = null;
                }

                $item->FieldDefinitions = array_values(array_filter($fieldDefineResult, function($e) use ($item){
                    return $e->RequestDefinitionID == $item->DefinitionID;
                }));

                $item->Steps = array_values(array_filter($stepResult, function($e) use ($item){
                    return $e->RequestID == $item->ID;
                }));

                $item->TableData = array_values(array_filter($tableDataResult, function($e) use ($item, $requestTableFieldMap){
                    $fieldName = $requestTableFieldMap[$item->DefinitionID];
                    $arrObj = get_object_vars($item->Fields);
                    return intval($arrObj[$fieldName]) === $e->TableID;
                }));

            }

            return (object)[
                Result => $result,
                Total => $total,
                PageCount => $pageCount
            ];
        }
    }
}
return new AMRequestFilterAjax();