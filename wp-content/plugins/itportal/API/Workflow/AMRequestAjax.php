<?php

if (!class_exists('AMRequestAjax')) {
    import('plugin.amWorkflow.class.factory.AMRequestFactory');
    import('plugin.amWorkflow.class.factory.AMRequestApprovalCode');
    import('plugin.amUser.class.includes.RoleID');
    import('plugin.amWorkflow.class.factory.AMRequestFieldFactory');
    import('plugin.amWorkflow.class.factory.AMRequestStepFactory');
    import('plugin.itportal.Factory.AMUserFactory');
    import('plugin.amWorkflow.class.factory.AMRequestUser');
    import('plugin.itportal.Business.Workflow.RequestData');
    import('plugin.amWorkflow.class.includes.RequestDataTypeID');
    import('plugin.amWorkflow.class.factory.AMRequestTableDetailData');
    import('plugin.amWorkflow.class.includes.RequestStatusID');
    import('plugin.amWorkflow.class.includes.ActionManager');
    import('plugin.amWorkflow.class.includes.EventID');
    import("plugin.itportal.API.CMDB.CMDB");


    ini_set('xdebug.var_display_max_depth', 5);
    ini_set('xdebug.var_display_max_children', 256);
    ini_set('xdebug.var_display_max_data', 1024);


    class AMRequestAjax {

        /**
         * @security workflow.request.view|api-custom
         * @param $params
         * @return AMRespone|array|null|object
         */
        public function GetRequestEditData($params) {
            try {
                // check
                $requestID = $params['ID'];
                if ($requestID == null)
                    return AMRespone::paramsInvalid();

                $ID = $requestID + 0;
                if ($ID == 0)
                    return AMRespone::jsonFailed($requestID);

                // request data
                $response = $this->_GetRequestData($requestID);
                if ($response->code != AMResponeCode::$ok)
                    return $response;
                $requestData = $response->data;

                // can comment
                $canComment = false;
                if ($requestData->request->RequestStatusID & RequestStatusID::$active) {
                    import("plugin.amWorkflow.class.factory.AMRequestCommentRoleFactory");
                    $currentStepID = $requestData->listStepDefinition[$requestData->request->CurrentIndex]->step->ID;
                    $currentDomain = $requestData->currentDomain;

                    $canComment = (new AMRequestCommentRoleFactory())->canComment($currentStepID, $currentDomain);
                }

                if ($this->_CheckViewRequestPermission($requestData, $canComment, $params) == false)
                    return AMRespone::denied();

                // get list comment
                import('plugin.amWorkflow.class.factory.AMRequestCommentFactory');
                $listComment = (new AMRequestCommentFactory())->query(["filter" => [["RequestID = " . $response->data->request->ID]]]);
                if ($listComment === false)
                    return AMRespone::dbFailed($requestData->request->ID, "Get list comment failed");
                $requestData->listComment = $listComment;

                // front end action required
                if ($requestData->request->RequestStatusID == RequestStatusID::$open) {
                    $currentStepDef                 = $requestData->getCurrentStep()->definition;
                    $facStepAction                  = new AMRequestStepActionFactory();
                    $frontEndRequiredActions        = $facStepAction->getListByStepDefinitionID($currentStepDef->ID, null, ActionTypeID::$associateAsset);
                    $requestData->currentStepAction = $frontEndRequiredActions;
                }

                // refer data
                $temp = $this->_GetRequestReferData($requestData);
                if ($temp->code != AMResponeCode::$ok)
                    return $temp;
                $response->referData             = $temp->data;
                $response->referData->canComment = $canComment;

                $requestData->removeMapping();
                return $response;
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }
        }

        public function GetRequestEditDataByDomain($params) {
            try {
                // check
                $requestID = $params['ID'];
                if ($requestID == null || isset($params['domain']) == false)
                    return AMRespone::paramsInvalid();

                $ID = $requestID + 0;
                if ($ID == 0)
                    return AMRespone::jsonFailed($requestID);

                // request data
                $response = $this->_GetRequestData($requestID, false);
                if ($response->code != AMResponeCode::$ok)
                    return $response;
                $requestData = $response->data;

                // request onwer and note
                //$requestData->loadUserFieldData();

                $requestData->removeMapping();
                return $response;
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }
        }

        public function GetRequestDefinitionIDByDisplayName($params) {
            import('plugin.amWorkflow.class.includes.WorkflowGet');
            $displayName = $params['displayName'];
            $wfGeter     = new WorkflowGet();
            $wfid        = $wfGeter->GetIDByDisplayName($displayName);
            return $wfid;
        }

        public function GetListRequest($params) {
            import('plugin.amWorkflow.class.includes.WorkflowGet');
            $displayName = $params['displayname'];
            $startIndex  = $params['startindex'];
            $limit       = $params['limit'];
            $wfGeter     = new WorkflowGet();
            return $wfGeter->GetListRequest($displayName, $startIndex, $limit);
        }

        public function GetRequestDefinition($params) {
            $workFlowID = $params['workFlowID'];
            if ($workFlowID == null)
                return AMRespone::paramsInvalid();

            $wfID = $workFlowID + 0;
            if ($wfID == 0)
                return AMRespone::jsonFailed($workFlowID);

            // definition
            $response = $this->_GetRequestDefinition($wfID);
            if ($response->code != AMResponeCode::$ok)
                return $response;

            // field can
            $temp = (new WorkflowGet())->GetFieldCan($response->data->listStepDefinition[0]->ID);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;
            $response->data->currentFieldCan = $temp->data;

            $temp = $this->_GetRequestReferData($response->data);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;
            $response->referData = $temp->data;

            import('plugin.amWorkflow.class.factory.AMRequestDefinitionFactory');
            $fac = new AMRequestDefinitionFactory();
            if (($listWorkfow = $fac->query([])) === false)
                return AMRespone::busFailed(null, "Get list workflow failed");
            $response->referData->listWorkfow = $listWorkfow;

            return $response;
        }

        public function CreateRequest($params) {
            // get json data
            $jsonWorkflowID = $params["workflowID"];
            $jsonField      = $params["field"];  // giá trị các trường
            $comment        = $params["comment"];
            if ($jsonWorkflowID == null || $jsonField == null)
                return AMRespone::paramsInvalid($params);

            // parse json
            $workflowID = $jsonWorkflowID + 0;
            $field      = am_json_decode($jsonField, 1);
            if ($workflowID == 0 || is_array($field == null))
                return AMRespone::jsonFailed(am_json_encode($params));

            return $this->_CreateRequest($workflowID, $field, $comment);
        }

        public function ChatApproval($params) {

            import('plugin.itportal.Business.Workflow.mail-approval');
            $facApprovalCode      = new AMRequestApprovalCode();
            $fnPreParseAttachment = function ($requestID, $stepID, $taskID, $chatAttachments) {
                $now        = microtime(true) * 10000;
                $n          = sizeof($chatAttachments);
                $initDir    = false;
                $attachment = [];
                for ($i = 0; $i < $n; $i++) {
                    $chatAttachment = $chatAttachments[$i];
                    $ext            = pathinfo($chatAttachment->fileName, PATHINFO_EXTENSION);

                    // make info
                    $info                 = new stdClass();
                    $info->url            = "uploads/workflow/request/$requestID/s$stepID-t$taskID-$now-$i.$ext";
                    $info->name           = $chatAttachment->fileName;
                    $chatAttachment->info = $info;
                    $attachment[]         = $info;

                    // save
                    $path = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/' . $info->url;
                    if ($initDir == false) {
                        $dir = pathinfo($path, PATHINFO_DIRNAME);
                        if (!file_exists($dir)) {
                            if (mkdir($dir, 0777, true) === false)
                                return false;
                        }
                        $initDir = true;
                    }

                    if (file_put_contents($path, $chatAttachment->data) === false)
                        return false;
                }

                return $attachment;
            };
            MaillApproval::fetchEmail(function ($approvalData, $mailAttachments) use ($facApprovalCode, $fnPreParseAttachment) {
                $approvalCode = $facApprovalCode->getCode($approvalData->requestID);
                if ($approvalCode != $approvalData->approvalCode)
                    return AMRespone::busFailed($approvalData, 'invalid approval code');

                switch ($approvalData->method) {
                    case 'approve':
                        $temp = $this->_GetRequestData($approvalData->requestID, false);
                        if ($temp->code != AMResponeCode::$ok)
                            return $temp;

                        $requestData = $temp->data;
                        $attachments = $fnPreParseAttachment($approvalData->requestID, $requestData->getCurrentStep()->ID, $approvalData->isApproved ? EventID::$approved : EventID::$reject, $mailAttachments);
                        if ($attachments === false)
                            return AMRespone::busFailed($approvalData, 'Save attachments failed');

                        $result = $this->_ApproveRequest($requestData, $approvalData->domain, $approvalData->isApproved, $approvalData->comment, $attachments);

                        break;

                    case 'ask':
                        $stepID      = (new AMRequestFactory())->getCurrentStepID($approvalData->requestID);
                        $attachments = $fnPreParseAttachment($approvalData->requestID, $stepID, EventID::$ask, $mailAttachments);
                        if ($attachments === false)
                            return AMRespone::busFailed($approvalData, 'Save attachments failed');

                        $result = $this->_RequestComment($approvalData->domain, $stepID, $approvalData->mentionDomains, $approvalData->comment, $attachments);
                        break;

                    case 'answer':
                        $stepID      = (new AMRequestFactory())->getCurrentStepID($approvalData->requestID);
                        $attachments = $fnPreParseAttachment($approvalData->requestID, $stepID, EventID::$answer, $mailAttachments);
                        if ($attachments === false)
                            return AMRespone::busFailed($approvalData, 'Save attachments failed');

                        $result = $this->_Answer($approvalData->domain, $stepID, $approvalData->comment, $attachments);
                        break;

                    case 'entrust':
                        $stepID      = (new AMRequestFactory())->getCurrentStepID($approvalData->requestID);
                        $attachments = $fnPreParseAttachment($approvalData->requestID, $stepID, EventID::$answer, $mailAttachments);
                        if ($attachments === false)
                            return AMRespone::busFailed($approvalData, 'Save attachments failed');

                        $result = $this->_EntrustRequestStep($approvalData->domain, $approvalData->entrustDomain, $stepID, $approvalData->comment, $attachments);
                        break;

                    default:
                        $result = AMRespone::busFailed($approvalData->method, 'method not support');
                }
                return $result;
            });
            return "";
        }

        public function _CreateRequest($workflowID, $field, $comment, $mapStep = null, $parrentRequest = null) {
            // create request data
            $request = [
                "DefinitionID" => $workflowID,
                "CreateDate" => date('Y-m-d H:i:s'),
                "CurrentIndex" => 0,
                "RequestStatusID" => RequestStatusID::$open
            ];

            if ($parrentRequest)
                $request["ParrentID"] = $parrentRequest->ID;

            // do insert data
            global $wpdb;
            try {
                $wpdb->query('START TRANSACTION');
                $re = $this->_InsertRequestAndData($workflowID, $request, $field, $comment, $mapStep, $parrentRequest);
                if ($re->code != AMResponeCode::$ok)
                    $wpdb->query('ROLLBACK');
                else {
                    $wpdb->query('COMMIT');
                }

                return $re;
            } catch (Exception $ex) {
                $wpdb->query('ROLLBACK');
                return AMRespone::exception([$workflowID, $field, $mapStep, $parrentRequest], $ex);
            }
        }

        public function ApproveRequestStep($params) {
            $requestIDJson  = $params['requestID'];
            $stepIndexJson  = $params['stepIndex'];
            $isApprovedJson = $params['isApproved'];
            $comment        = $params['comment'];
            $field          = $params['field'];
            $attachments    = $params['attachments'];

            if ($requestIDJson === null || $stepIndexJson === null)
                return AMRespone::paramsInvalid($params);


            $requestID  = $requestIDJson + 0;
            $stepIndex  = $stepIndexJson + 0;
            $isApproved = am_json_decode($isApprovedJson);
            if ($requestID == 0 || is_numeric($stepIndex) == false || $isApproved === null)
                return AMRespone::jsonFailed($params);

            // get current user
            $currentUser = wp_get_current_user();
            if ($currentUser == 0 || $currentUser->user_login == false)
                return AMRespone::busFailed(null, "not logined");
            $currentUserDomain = $currentUser->user_login;

            $temp = $this->_GetRequestData($requestID);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;

            $requestData = $temp->data;

            return $this->_ApproveRequest($requestData, $currentUserDomain, $isApproved, $comment, $attachments, $field);
        }

        function _ApproveRequest(RequestData $requestData, $domain, $isApproved, $comment, $attachments = null, $field = null) {
            //var_dump($field);
            if ($field) {
                $currentStep = $requestData->getCurrentStep();
                if ($currentStep == null)
                    return AMRespone::busFailed($requestData, "Find current step failed");
                //  $temp = (new WorkflowGet())->GetFieldCan($requestData, $currentStep->definition->ID);
                // if ($temp->code != AMResponeCode::$ok)
                //     return $temp;
                // $requestData->currentFieldCan = $temp->data;
            }

            global $wpdb;
            try {
                $wpdb->query('START TRANSACTION');

                $stepIndex = $currentStep->definition->StepIndex;
                $re        = $this->_DoApproveRequestStep($requestData, $stepIndex, $domain, $isApproved, $comment, $field, $attachments);
                $requestData->removeMapping();

                if ($re->code != AMResponeCode::$ok)
                    $wpdb->query('ROLLBACK');
                else {
                    $wpdb->query('COMMIT');
                }

                return $re;
            } catch (Exception $ex) {
                $wpdb->query('ROLLBACK');
                return AMRespone::exception([$domain, $requestData], $ex);
            }
        }

        public function UpdateRequest($params) {
            $requestIDJson = $params['requestID'];
            $comment       = $params['comment'];
            $field         = $params['field'];

            if ($requestIDJson === null || $comment === null)
                return AMRespone::paramsInvalid($params);


            $requestID = $requestIDJson + 0;
            if ($requestID == 0)
                return AMResponie::jsonFailed($params);

            $temp = $this->_GetRequestData($requestID);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;

            $requestData             = $temp->data;
            $currentStepDefinitionID = $requestData->getCurrentStep()->definition->ID;
            if ($currentStepDefinitionID == null)
                return AMRespone::busFailed($requestData, "Find current step failed");
            //            $temp = (new WorkflowGet())->GetFieldCan($requestData, $currentStepDefinitionID);
            //            if ($temp->code != AMResponeCode::$ok)
            //                return $temp;
            //            $fieldCan = $temp->data;

            //$temp = $this->_UpdateField($requestData, $fieldCan, $field);
            $temp = $this->_SaveFieldData($requestData, $field);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;

            return new AMRespone($requestData->getField());
        }

        public function EntrustRequestStep($params) {

            $stepIDJson    = $params['stepID'];
            $entrustDomain = $params['domain'];
            $comment       = $params['comment'];
            $attachments   = $params['attachments'];

            if ($stepIDJson === null || $entrustDomain === null)
                return AMRespone::paramsInvalid($params);

            $stepID = $stepIDJson + 0;
            if ($stepID == 0 || $entrustDomain === null)
                return AMRespone::jsonFailed($params);


            $currentUser = wp_get_current_user();
            if ($currentUser == 0 || $currentUser->user_login == false)
                return AMRespone::busFailed(null, "not logined");
            $ownerDomain = $currentUser->user_login;

            return $this->_EntrustRequestStep($ownerDomain, $entrustDomain, $stepID, $comment, $attachments);
        }

        public function CancelRequest($params) {
            $id          = $params['ID'];
            $comment     = $params['comment'];
            $attachments = $params['attachments'];
            if ($id === null)
                return AMRespone::paramsInvalid($params);

            $id = $id + 0;
            if ($id == 0)
                return AMRespone::jsonFailed($params);

            $re = null;
            try {
                global $wpdb;
                $wpdb->query('START TRANSACTION');
                $re = $this->_CancelRequest($id, $comment, $attachments);
            } catch (Exception $ex) {
                $re = AMRespone::exception($params, $ex);
            }
            if ($re->code != AMResponeCode::$ok)
                $wpdb->query('ROLLBACK');
            else
                $wpdb->query('COMMIT');
            return $re;
        }

        public function PickupStepApproval($params) {
            $stepID      = $params['stepID'];
            $comment     = $params['comment'];
            $attachments = $params['attachments'];
            if ($stepID === null)
                return AMRespone::paramsInvalid($params);

            $stepID = $stepID + 0;
            if ($stepID == 0)
                return AMRespone::jsonFailed($params);

            $re = null;
            try {
                global $wpdb;
                $wpdb->query('START TRANSACTION');
                $re = $this->_PickupStepApproval($stepID, $comment, $attachments);
            } catch (Exception $ex) {
                $re = AMRespone::exception($params, $ex);
            }
            if ($re->code != AMResponeCode::$ok)
                $wpdb->query('ROLLBACK');
            else
                $wpdb->query('COMMIT');
            return $re;
        }

        public function RaiseRequestStep($params) {
            $id          = $params['ID'];
            $comment     = $params['comment'];
            $attachments = $params['attachments'];
            if ($id === null)
                return AMRespone::paramsInvalid($params);

            $id = $id + 0;
            if ($id == 0)
                return AMRespone::jsonFailed($params);

            $re = null;
            try {
                global $wpdb;
                $wpdb->query('START TRANSACTION');
                $re = $this->_RaiseRequestStep($id, $comment, $attachments);
            } catch (Exception $ex) {
                $re = AMRespone::exception($params, $ex);
            } finally {
                if ($re->code != AMResponeCode::$ok)
                    $wpdb->query('ROLLBACK');
                else
                    $wpdb->query('COMMIT');
            }
            return $re;
        }

        public function GetListRequestStatus($params) {
            import('plugin.amWorkflow.class.factory.AMRequestStatusFactory');
            $re = (new AMRequestStatusFactory())->query([]);
            if ($re === false)
                return AMRespone::dbFailed(null, "Get list request status  failed");

            return new AMRespone($re);
        }

        function _GetListSubStepData($rootRequestID) {
            $rootID = $rootRequestID;

            import('plugin.amWorkflow.class.factory.AMRequestStepFactory');
            import('plugin.amWorkflow.class.factory.AMRequestStepDefinitionFactory');
            $facStep           = new AMRequestStepFactory();
            $facStepDefinition = new AMRequestStepDefinitionFactory();
            $fac               = new AMRequestFactory();

            $childRequests = $fac->getChildRequestWithWorkflowName($rootID);
            if (sizeof($childRequests) == 0)
                return new AMRespone($childRequests);

            $inRequest = $this->_GetWhereIn($childRequests, "ID");
            $listStep  = $facStep->query(["filter" => [["RequestID IN $inRequest"]]]);
            if ($listStep === false)
                return AMRespone::dbFailed($inRequest, "getStep for child requests failed");

            $inStep             = $this->_GetWhereIn($listStep, "DefinitionID");
            $listStepDefinition = $facStepDefinition->query(["filter" => [["ID IN $inStep"]]]);
            if ($listStepDefinition === false)
                return AMRespone::dbFailed($inStep, "getStepDefinition for Step failed");

            foreach ($listStep as $step) {
                foreach ($listStepDefinition as $stepDefinition) {
                    if ($step->DefinitionID == $stepDefinition->ID) {
                        $stepDefinition->step = $step;
                        break;
                    }
                }

                foreach ($childRequests as $request) {
                    if ($step->RequestID == $request->ID) {
                        $request->listStepDefinition[] = clone $stepDefinition;
                        break;
                    }
                }
            }

            return new AMRespone($childRequests);
        }

        public function GetRequestsForApproval($params) {
            $currentUser = wp_get_current_user();
            if ($currentUser == 0 || $currentUser->user_login == false)
                return AMRespone::busFailed(null, "not logined");

            $fac = new AMRequestFactory();
            $re  = $fac->getRequestsForApproval($currentUser->user_login);
            return $this->_SetRequestsDescription($re, $fac);
        }

        public function GetUserRequestsByStatus($params) {
            $currentUser = wp_get_current_user();
            if ($currentUser == 0 || $currentUser->user_login == false)
                return AMRespone::busFailed(null, "not logined");

            $statusID = $params['statusID'];
            $fac      = new AMRequestFactory();
            $re       = $fac->getUserRequestsByStatus($currentUser->user_login, $statusID);

            return $this->_SetRequestsDescription($re, $fac);
        }

        public function SearchUserRequestsByStatus($params) {
            $currentUser = wp_get_current_user();
            if ($currentUser == 0 || $currentUser->user_login == false)
                return AMRespone::busFailed(null, "not logined");

            $statusIDs = $params['statusIDs'];
            if (is_array($statusIDs) == false)
                return AMRespone::paramsInvalid($params);

            $fac = new AMRequestFactory();
            $re  = $fac->getUserRequestsByStatusIDs($currentUser->user_login, $statusIDs);

            return $this->_SetRequestsDescription($re, $fac);
        }

        public function GetUserRecentRequests($params) {
            $currentUser = wp_get_current_user();
            if ($currentUser == 0 || $currentUser->user_login == false)
                return AMRespone::busFailed(null, "not logined");

            $statusID = $params['statusID'];
            $fac      = new AMRequestFactory();
            $temp     = $fac->getUserRequestsByStatus($currentUser->user_login, $statusID);

            $re    = [];
            $count = 1;
            foreach ($temp as $request) {
                if ($count++ > 5)
                    break;

                if ($request->ParrentID)
                    continue;

                $re[] = $request;
            }

            return $this->_SetRequestsDescription($re, $fac);
        }

        public function RequestComment($params) {
            // get json data
            $stepID         = $params["stepID"];
            $comment        = $params["comment"];
            $mentionDomains = $params["mentionDomains"];
            $attachments    = $params["attachments"];
            if ($stepID == null || $mentionDomains == null || $comment == null)
                return AMRespone::paramsInvalid($params);

            // get current user
            $currentUser = wp_get_current_user();
            if ($currentUser == 0 || $currentUser->user_login == false)
                return AMRespone::busFailed(null, "not logined");

            return $this->_RequestComment($currentUser->user_login, $stepID, $mentionDomains, $comment, $attachments);
        }

        public function AnswerComment($params) {

            $stepID      = $params["stepID"];
            $comment     = $params["comment"];
            $attachments = $params["attachments"];
            if ($stepID == null || $comment == null)
                return AMRespone::paramsInvalid($params);

            // get current user
            $currentUser = wp_get_current_user();
            if ($currentUser == 0 || $currentUser->user_login == false)
                return AMRespone::busFailed(null, "not logined");
            $currentDomain = $currentUser->user_login;

            return $this->_Answer($currentDomain, $stepID, $comment, $attachments);
        }

        public function Comment($params) {
            $stepID         = $params["stepID"];
            $comment        = $params["comment"];
            $mentionDomains = $params["mentionDomains"];
            $attachments    = $params["attachments"];
            if ($stepID == null || $comment == null)
                return AMRespone::paramsInvalid($params);

            // get current user
            $currentUser = wp_get_current_user();
            if ($currentUser == 0 || $currentUser->user_login == false)
                return AMRespone::busFailed(null, "not logined");
            $currentDomain = $currentUser->user_login;

            return $this->_Comment($currentDomain, $stepID, $comment, $mentionDomains, $attachments);
        }

        /**
         * @param $params
         * @return mixed
         */
        public function NotLoginApprove($params) {
            try {
                $fieldString     = $params['field'];
                $fieldString     = stripslashes($fieldString);
                $params['field'] = json_decode($fieldString, JSON_UNESCAPED_SLASHES);
                $method          = $params['method'];
                $data            = (object)$params;

                // foreach ($params['field'] as $name => $value){
                //     if(is_array($value)){
                //         $newValue = [];
                //         foreach ($value as $item)
                //             $newValue[] = (object)$item;
                //
                //         $data->{$name} = $newValue;
                //     }
                // }
                // var_dump($data);
                // $data->field     = (object)$data->field;
                // foreach ($params['field'] as $name => $value){
                //     if(is_array($value)){
                //         $newValue = [];
                //         foreach ($value as $item)
                //             $newValue[] = (object)$item;
                //
                //         $data->{$name} = $newValue;
                //     }
                // }

                return $this->_NotLoginApprove($method, $data);
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }
        }

        public function _CancelRequest($requestID, $comment, $attachments = null) {
            $temp = $this->_GetRequestData($requestID);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;


            $requestData = $temp->data;
            $step        = $requestData->getCurrentStep();
            $canCancel   = $step->definition->CanCancel;
            if (!$canCancel) {
                $mes = $requestData->workflow->DisplayName;
                $mes .= " can not cancel at ";
                $mes .= $step->definition;
                return new AMRespone($step, AMResponeCode::$denied, $mes);
            }

            $requestOwnerDomain = $requestData->getOwnerDomain();
            $stepOwnerDomain    = $step->Owner;
            $editDomain         = $requestData->currentDomain;


            $canCancel = $editDomain == $requestOwnerDomain || $editDomain == $stepOwnerDomain;
            if (!$canCancel) {
                global $security;
                if ($security->checkFeature('workflow.request.cancel') == false)
                    return new AMRespone($editDomain, AMResponeCode::$denied, "$editDomain can not cancel request");
            }

            if ($step->Owner != $editDomain) {
                $step->Owner = $editDomain;

                $updateData = [
                    'Owner' => $step->Owner
                ];
                $facStep    = new AMRequestStepFactory();
                if ($facStep->update($updateData, ['ID' => $step->ID]) == false) {
                    return AMRespone::dbFailed($step, 'Update step Owner failed');
                }
            }

            $params = [
                'stepID' => $requestData->getCurrentStep()->ID,
                'domain' => $requestData->currentDomain,
                'comment' => $comment,
                'attachments' => $attachments,
                'stepDefinition' => $requestData->getCurrentStep()->definition
            ];

            $response = $this->_DoRequestTask($requestData, EventID::$cancel, $params);
            if ($response->code != AMResponeCode::$ok)
                return $response;

            $temp = $this->_UpdateRequestData($requestData->request, RequestStatusID::$cancel);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;

            return $response;
        }

        public function _PickupStepApproval($stepID, $comment, $attachments = null) {
            $temp = $this->_GetRequestDataByStepID($stepID);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;


            $requestData = $temp->data;
            $editDomain         = $requestData->currentDomain;
            $step        = $requestData->getCurrentStep();

            if ($step->definition->StepIndex != $requestData->request->CurrentIndex) {
                return new AMRespone($step, AMResponeCode::$denied, 'Can not pickup approval to not current step');
            }

            if (!is_numeric($step->definition->Role)) {
                return new AMRespone($step, AMResponeCode::$denied, 'Can not pickup approval without service role');
            }

            if ($step->Owner == $editDomain) {
                return new AMRespone($step, AMResponeCode::$denied, 'Can not self pickup approval');
            }


            $facStep     = new AMRequestStepFactory();
            $step->Owner = $editDomain;
            $stepInArray = ['Owner' => $editDomain];
            if ($facStep->update($stepInArray, ["ID" => $stepID]) === false)
                return AMRespone::dbFailed($stepInArray, "Update step owner failed");

            $params = [
                'stepID' => $requestData->getCurrentStep()->ID,
                'domain' => $requestData->currentDomain,
                'comment' => $comment,
                'attachments' => $attachments,
                'stepDefinition' => $requestData->getCurrentStep()->definition
            ];

            $response = $this->_DoRequestTask($requestData, EventID::$pickup, $params);
            if ($response->code != AMResponeCode::$ok)
                return $response;

            return $response;
        }

        public function _RaiseRequestStep($stepID, $comment, $attachments = null) {
            $temp = $this->_GetRequestDataByStepID($stepID);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;
            $requestData = $temp->data;
            $ownerDomain = $requestData->getOwnerDomain();
            $editDomain  = $requestData->currentDomain;

            $canRaise = $ownerDomain != $editDomain;
            if (!$canRaise) {
                $step     = $requestData->getCurrentStep();
                $canRaise = $step->definition->CanRaise;
            }

            if (!$canRaise)
                return new AMRespone($editDomain, AMResponeCode::$denied, "$editDomain can not raise request");

            $paramms = [
                'stepID' => $stepID,
                'domain' => $editDomain,
                'mentionDomains' => [],
                'comment' => $comment,
                'attachments' => $attachments,
                'stepDefinition' => $step->definition,
            ];

            return $this->_DoRequestTask($requestData, EventID::$raise, $paramms);
        }

        function _CheckViewRequestPermission(RequestData $requestData, $canComment, $params) {
            global $security;
            if ($security->allow)
                return true;

            $currentDomain = $requestData->currentDomain;
            if ($currentDomain) {
                if ($canComment)
                    return true;

                foreach ($requestData->listStepDefinition as $stepDefinition) {
                    if ($stepDefinition->step->Owner == $currentDomain)
                        return true;
                }

                if ($currentDomain == $requestData->getOwnerDomain())
                    return true;
            }
        }

        function _EntrustRequestStep($actionDomain, $entrustDomain, $stepID, $comment, $attachments = null, $allowNotCurrentStep = false) {

            // load data
            $facStep = new AMRequestStepFactory();
            $step    = $facStep->getByID($stepID);
            if ($step === false)
                return AMRespone::dbFailed($stepID, "Get step by ID failed");

            import('plugin.amWorkflow.class.factory.AMRequestStepDefinitionFactory');
            $stepDefinition = (new AMRequestStepDefinitionFactory())->getByID($step->DefinitionID);
            if ($stepDefinition === false)
                return AMRespone::dbFailed($stepID, "Get step definition by ID failed");

            // check security
            if ($step->AssignedOwner != $actionDomain) {
                global $security;
                if ($security->checkFeature('delegate') == false)
                    return AMRespone::denied(get_defined_vars());

                $step->Owner = $actionDomain;
            }

            // check workflow role
            // if(is_numeric($stepDefinition->Role)){
            //     $suggestDomain = ((new AMRequestFactory())->getSuggestDomainByRole($stepDefinition->Role, null, $entrustDomain));
            //     if ($suggestDomain === false)
            //         return AMRespone::dbFailed($stepID, "Check domain role failed");
            //
            //     if ($suggestDomain != $entrustDomain)
            //         return AMRespone::busFailed($stepID, "Role not matched. Can not delegate for $entrustDomain");
            // }

            // check allow current step role
            $allowDoRequestTask = true;
            if ($allowNotCurrentStep) {
                $facRequest = new AMRequestFactory();
                $request    = $facRequest->getByID($step->RequestID);
                if ($request->CurrentIndex != $stepDefinition->StepIndex)
                    $allowDoRequestTask = false;
            }

            // do task
            $commentData = null;
            if ($allowDoRequestTask) {
                $args = [
                    'stepID' => $stepID,
                    'domain' => $actionDomain,
                    'comment' => $comment,
                    'attachments' => $attachments,
                ];
                $temp = $this->_DoRequestTask(null, EventID::$entrust, $args);
                if ($temp->code != AMResponeCode::$ok)
                    return $temp;

                $commentData       = $temp->data;
                $updateDataComment = ['Data' => $entrustDomain];
                $facComment        = new AMRequestCommentFactory();
                $facComment->update($updateDataComment, ['ID' => $commentData->ID]);
            }

            $facStep     = new AMRequestStepFactory();
            $step->Owner = $entrustDomain;
            $stepInArray = ['Owner' => $entrustDomain];
            if ($facStep->update($stepInArray, ["ID" => $stepID]) === false)
                return AMRespone::dbFailed($stepInArray, "Update step owner failed");

            $re = (object)[
                'step' => $step,
                'comment' => $commentData
            ];

            return new AMRespone($re);
        }

        function _NotLoginApprove($method, $data) {
            switch ($method) {
                case 'approve':
                    $temp = $this->_GetRequestData($data->requestID, false);
                    if ($temp->code != AMResponeCode::$ok)
                        return $temp;

                    $requestData = $temp->data;
                    return $this->_ApproveRequest($requestData, $data->domain, $data->isApproved, $data->comment, null, $data->field);

                case 'ask':
                    $stepID = (new AMRequestFactory())->getCurrentStepID($data->requestID);
                    return $this->_RequestComment($data->domain, $stepID, $data->mentionDomains, $data->comment);

                case 'answer':
                    $stepID = (new AMRequestFactory())->getCurrentStepID($data->requestID);
                    /// $domain, $stepID, $comment
                    return $this->_Answer($data->domain, $stepID, $data->comment);

                default:
                    return AMRespone::busFailed($method, 'method not support');
            }
        }

        function _RequestComment($domain, $stepID, $mentionDomains, $questionComment, $attachments = null, RequestData $requestData = null) {
            global $wpdb;
            try {
                $wpdb->query('START TRANSACTION');

                import("plugin.amWorkflow.class.factory.AMRequestStepFactory");
                $facStep = new AMRequestStepFactory();

                // get requestData if no pass
                if ($requestData == null)
                    $requestData = $this->_GetRequestData($facStep->getRequest($stepID)->ID, false)->data;

                // get step data
                $step = $requestData->getCurrentStep();
                if ($step == null)
                    return AMRespone::busFailed($requestData);

                // get definition
                $stepDefinition = $step->definition;

                // check permision
                if ($step->Owner != $domain) {
                    $logData = [$domain, $step];
                    return new AMRespone($logData, AMResponeCode::$denied);
                }

                // check permision
                if ($stepDefinition->CanRequestInfo == false)
                    return new AMRespone($stepDefinition, AMResponeCode::$denied);

                $paramms = [
                    'stepID' => $stepID,
                    'domain' => $domain,
                    'mentionDomains' => $mentionDomains,
                    'comment' => $questionComment,
                    'attachments' => $attachments,
                    'stepDefinition' => $stepDefinition,
                ];
                $re      = $this->_DoRequestTask($requestData, EventID::$ask, $paramms);
                if ($re->code != AMResponeCode::$ok)
                    return $re;

                $wpdb->query('COMMIT');
                $comment = $re->data;
                return new AMRespone($comment);

            } catch (Exception $ex) {
                $wpdb->query('ROLLBACK');
                return AMRespone::exception([$stepID, $domain, $mentionDomains, $questionComment], $ex);
            }
        }

        function _Answer($domain, $stepID, $comment, $attachments = null) {
            // check permission
            import("plugin.amWorkflow.class.factory.AMRequestCommentRoleFactory");
            $facComentRole = new AMRequestCommentRoleFactory();
            $commentRole   = $facComentRole->canComment($stepID, $domain);
            if ($commentRole == null) {
                $args = [$domain, $stepID, $comment];
                return new AMRespone($args, AMResponeCode::$denied);
            }

            global $wpdb;
            $response = null;
            try {
                $wpdb->query('START TRANSACTION');
                $params   = [
                    'stepID' => $stepID,
                    'domain' => $domain,
                    'comment' => $comment,
                    'attachments' => $attachments,
                ];
                $response = $this->_DoRequestTask(null, EventID::$answer, $params);
                return $response;
            } catch (Exception $ex) {
                return ($response = AMRespone::exception([$domain, $stepID, $comment], $ex));
            } finally {
                if ($response->code == AMResponeCode::$ok)
                    $wpdb->query('COMMIT');
                else
                    $wpdb->query('ROLLBACK');
            }

            return new AMRespone();
        }

        function _Comment($domain, $stepID, $comment, $mentionDomains, $attachments = null) {

            global $wpdb;
            try {
                $facStep = new AMRequestStepFactory();

                $temp = $this->_GetRequestData($facStep->getRequest($stepID)->ID);
                if ($temp->code != AMResponeCode::$ok)
                    return $temp;
                $requestData = $temp->data;

                if ($requestData->isInProcess() == false)
                    return AMRespone::busFailed($requestData, 'Can not comment on request not in process');

                $canComment = false;
                foreach ($requestData->listStepDefinition as $stepDef) {
                    if ($stepDef->step->Owner == $requestData->currentDomain || $stepDef->step->AssignedOwner == $requestData->currentDomain) {
                        $canComment = true;
                        break;
                    }
                }

                if (!$canComment) {
                    $canComment = $requestData->currentDomain == $requestData->getOwnerDomain();
                }

                if (!$canComment)
                    return AMRespone::denied(get_defined_vars());

                $paramms = [
                    'stepID' => $stepID,
                    'domain' => $domain,
                    'mentionDomains' => $mentionDomains,
                    'comment' => $comment,
                    'attachments' => $attachments,
                ];


                $wpdb->query('START TRANSACTION');
                $re = $this->_DoRequestTask($requestData, EventID::$comment, $paramms);
                if ($re->code != AMResponeCode::$ok)
                    return $re;

                $wpdb->query('COMMIT');
                $comment = $re->data;
                return new AMRespone($comment);

            } catch (Exception $ex) {
                $wpdb->query('ROLLBACK');
                return AMRespone::exception(get_defined_vars(), $ex);
            }
        }

        /**
         * Insert comment and do step action
         * @param $requestData
         * @param $eventID
         * @param $params
         */
        function _DoRequestTask($requestData, $eventID, $params = []) {
            $request        = null;
            $stepID         = $params['stepID'];
            $domain         = $params['domain'];
            $stepDefinition = $params['stepDefinition'];
            $comment        = $params['comment'];
            $attachments    = $params['attachments'];
            if ($requestData) {
                if ($requestData->isInProcess() == false)
                    return AMRespone::busFailed($requestData, 'Can not task request in not process');

                $requestData->setCurTask($eventID, $comment);
                $request = $requestData->request;

                if ($stepDefinition == null)
                    $params['stepDefinition'] = $stepDefinition = $requestData->getCurrentStep()->definition;

                if ($stepDefinition && $stepDefinition->step && $stepDefinition->step->ID)
                    $params['stepID'] = $stepID = $stepDefinition->step->ID;

                if ($domain == null)
                    $params['domain'] = $domain = $requestData->currentDomain;

                $actionManager = new ActionManager();
                $temp          = $actionManager->DoActionForRequestData($requestData, $eventID, $params);
                if ($temp->code != AMResponeCode::$ok)
                    return $temp;

                if ($requestData->taskEventID() !== null)
                    $eventID = $requestData->taskEventID();
            }

            return $this->_InsertComment($stepID, $eventID, $domain, $params['mentionDomains'], $comment, $attachments, $params['stepDefinition'], $request);
        }

        function _InsertComment($stepID, $evntID, $domain, $mentionDomains, $content, $attachments = null, $stepDefinition = null, $request = null) {
            import("plugin.amWorkflow.class.factory.AMRequestStepFactory");
            $facStep = new AMRequestStepFactory();

            // get request
            if ($request == null) {
                $request = $facStep->getRequest($stepID);
                if ($request == false)
                    return AMRespone::dbFailed($request, "get Request by stepID failed");
            }

            if ($stepDefinition == null) {
                // get definition
                $stepDefinition = $facStep->getDefinition($stepID);
                if ($stepDefinition == false)
                    return AMRespone::dbFailed($stepDefinition, "get StepDefinition by stepID failed");
            }

            if (!$content)
                $content = '';

            if ($request->CurrentIndex != $stepDefinition->StepIndex)
                return AMRespone::busFailed($stepID, "Can not comment into not current step");

            import("plugin.amWorkflow.class.factory.AMRequestCommentFactory");
            $comment = (new AMRequestCommentFactory())->insertComment($request->ID, $stepID, $domain, $evntID, $content, $attachments);

            if ($comment == false)
                return AMRespone::dbFailed($comment, "Insert comment failed");

            // insert role for mention
            if ($mentionDomains) {
                import("plugin.amWorkflow.class.factory.AMRequestCommentRoleFactory");
                $facCommentRole = new AMRequestCommentRoleFactory();

                foreach ($mentionDomains as $mentionDomain) {
                    $insertData = [
                        "Domain" => $mentionDomain,
                        "StepID" => $stepID,
                        "Done" => 0,
                        "CommentID" => $comment->ID
                    ];

                    if ($facCommentRole->insert($insertData) == false)
                        return AMRespone::dbFailed($insertData, "RequestComemnt failed");
                }
            }
            return new AMRespone($comment);
        }

        function _SetRequestsDescription(&$requests, $facRequest) {
            if ($requests === false)
                return AMRespone::dbFailed(null, "Get list request be false  failed");

            if ($facRequest->setRequestsDescription($requests) == false)
                return AMRespone::dbFailed(null, "setRequestsDescription failed");

            return new AMRespone($requests);
        }

        function _InsertRequestAndData($workflowID, &$request, &$field, $comment, &$mapStep, $parrentRequest) {
            // get definition
            import('plugin.amWorkflow.class.includes.WorkflowGet');
            $geter = new WorkflowGet();
            $temp  = $geter->GetWorkflowData($workflowID);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;
            $definition = $temp->data;

            // insert request
            $request["Creator"] = $definition->currentDomain;
            $temp               = $this->_InsertRequest($request);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;
            $request = (object)$request;

            // create requestData
            $definition->request = $request;
            $requestData         = new RequestData($definition, 1);

            // field can
            //            $temp = (new WorkflowGet())->GetFieldCan($definition->listStepDefinition[0]->ID);
            //            if ($temp->code != AMResponeCode::$ok)
            //                return $temp;
            $requestData->currentFieldCan = $requestData->getFieldCan();

            // insert field
            $field["RequestID"] = $request->ID;
            // $temp               = $this->_InsertField($requestData, $field, $comment);
            $temp = $this->_SaveFieldData($requestData, $field);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;

            //security
            global $security;
            $owner = $requestData->getOwnerDomain();
            $allow = $security->allow || $security->checkFeature('workflow.request.create') || $security->checkOwner($owner) || $security->checkReportingLine($owner) || $security->checkDeptHead($owner);
            if (!$allow)
                return AMRespone::denied();

            // insert step
            $temp = $this->_InsertStep($requestData, $mapStep);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;

            // clone comment
            if ($parrentRequest) {
                $facComment  = new AMRequestCommentFactory();
                $listComment = $facComment->query(["filter" => [["RequestID = $parrentRequest->ID"]]]);
                if ($listComment === false)
                    return AMRespone::dbFailed($request, "Get list requestComment failed");

                foreach ($listComment as $comment) {
                    $comment->RequestID = $request->ID;
                    unset($comment->ID);
                    $commentInArray = (array)$comment;
                    if ($facComment->insert($commentInArray) === false)
                        return AMRespone::dbFailed($request, "Insert requestComment failed");
                }
            }

            // dev_upgrade cheat chờ làm lại step action
            $requestData->taskEventID(EventID::$create);

            // approve
            if ($parrentRequest) {
                $stepCount = sizeof($requestData->listStepDefinition);
                for ($i = $requestData->request->CurrentIndex; $i < $stepCount; $i++) {
                    $stepDefinition = $requestData->listStepDefinition[$i];
                    $step           = $stepDefinition->step;
                    $stepInit       = $mapStep[$stepDefinition->Name];
                    if (is_string($stepInit)) {
                        $temp = $this->_DoApproveRequestStep($requestData, $stepDefinition->StepIndex, $stepInit, 1, '');
                        if ($temp->code != AMResponeCode::$ok)
                            return $temp;
                    }
                    else {
                        break;
                    }
                }
            }
            else {
                $temp = $this->_DoApproveRequestStep($requestData, 0, $request->Creator, 1, $comment);
                if ($temp->code != AMResponeCode::$ok)
                    return $temp;

                //                if ($insertFieldRe->updateLasters) {
                //                    $fieldValuesUpdate = [];
                //                    foreach ($insertFieldRe->updateLasters as $fieldDefinition) {
                //                        if ($fieldDefinition->RequestDataTypeID == RequestDataTypeID::$note) {
                //                            $fieldValuesUpdate[$fieldDefinition->MappingName] = $comment;
                //                            $requestData->setFieldvalueByDefinition($fieldDefinition, $comment);
                //                        }
                //                    }
                //
                //                    $facField = new AMRequestFieldFactory();
                //                    $temp     = $facField->update($fieldValuesUpdate, ["ID" => $insertFieldRe->fieldValuesID]);
                //                    if ($temp === false)
                //                        return AMRespone::dbFailed($fieldValuesUpdate, "Update field laster failed");
                //                }
            }

            $requestData->removeMapping();

            return new AMRespone($requestData);
        }

        function _InsertRequest(&$request) {
            global $wpdb;
            // insert request
            $request['RequestStatusID'] = RequestStatusID::$open;
            $temp                       = (new AMRequestFactory())->insert($request);
            if ($temp === false)
                return AMRespone::dbFailed($request, "Insert resquest");

            $request["ID"] = $wpdb->insert_id;

            return new AMRespone();
        }

        function _InsertStep(RequestData $requestData, &$mapStep = null) {
            $request     = $requestData->request;
            $delegateBus = $requestData->getDelegateBus();

            global $wpdb;

            $ownerDomain           = $requestData->getOwnerDomain();
            $ownerInfo             = (new AMUserFactory())->GetUserInformationByDomain($ownerDomain);
            $facStep               = new AMRequestStepFactory();
            $requestID             = $request->ID;
            $listStep              = [];
            $requestData->listStep = &$listStep;
            $startTimeMin          = new DateTime();
            $startTimeMax          = new DateTime();

            $childRequestCurrentStep = 0;
            foreach ($requestData->listStepDefinition as $stepDefinition) {
                $init = $mapStep ? $mapStep[$stepDefinition->Name] : null;
                $step = $requestData->createStep($requestID, $stepDefinition, $init);
                if ($init == null) {
                    $temp = $this->_SetStepOwner($step, $requestData, $ownerInfo->Site, $request->Creator);
                    if ($temp->code != AMResponeCode::$ok)
                        return $temp;

                    $step->AssignedOwner = $step->Owner;
                    $step->MinIntendTime = $startTimeMin->format('Y-m-d H:i:s');
                    $step->MaxIntendTime = $startTimeMax->format('Y-m-d H:i:s');

                    if ($stepDefinition->MinIntendDuration)
                        $startTimeMin->modify("+$stepDefinition->MinIntendDuration hours");
                    if ($stepDefinition->MaxIntendDuration)
                        $startTimeMin->modify("+$stepDefinition->MaxIntendDuration hours");

                    // delegate
                    if ($requestData->currentDomain != $step->AssignedOwner && $stepDefinition->StepIndex) {
                        $temp = $delegateBus->CheckDelegate($step->AssignedOwner);
                        if ($temp->code == AMResponeCode::$ok && $temp->data) {
                            $delegateConf   = $temp->data;
                            $setDelegateRes = $this->_SetStepOwner($step, $requestData, null, $delegateConf->DelegatedDomain);

                            if ($setDelegateRes->code == AMResponeCode::$ok) {
                                $step->Owner        = $delegateConf->DelegatedDomain;
                                $step->DelegateMode = $delegateConf->DelegateMode;
                            }
                            else {
                                $reporting = using('plugin.itportal.config.mail-template.AutoDelegateError');
                                $reporting->send($requestData, $step);
                            }
                        }
                    }
                }
                else {
                    if (is_object($init))
                        $childRequestCurrentStep++;
                }

                if ($facStep->insert($requestData->toStepInsertData($step)) == false)
                    return AMRespone::dbFailed($stepDefinition, "Insert request step failed");

                $step->ID = $wpdb->insert_id;
            }

            if ($childRequestCurrentStep > 0) {
                // đặt lại bước hiện tại và trạng thái cho request
                $requestData->request->CurrentIndex = $childRequestCurrentStep;
                $updateData                         = ['CurrentIndex' => $childRequestCurrentStep];
                $facRequest                         = new AMRequestFactory();
                $facRequest->update($updateData, ['ID' => $requestData->request->ID]);
            }

            return new AMRespone($listStep);
        }

        /**
         *  return table id
         */
        function _InsertTableDetail($fieldDefinition, &$listDetail, $requestID) {
            global $wpdb;
            import('plugin.amWorkflow.class.factory.AMRequestTableData');
            import('plugin.amWorkflow.class.factory.AMRequestTableDetailData');
            $facTable       = new AMRequestTableData();
            $facTableDetail = new AMRequestTableDetailData();

            $tableID = $facTable->createID($requestID, $fieldDefinition->MappingName);
            if ($tableID === false)
                return AMRespone::dbFailed(null, "Create table detail data id");

            foreach ($listDetail as $detail) {
                $isEmpty    = true;
                $insertData = [];
                foreach ($fieldDefinition->listChild as $childDefinition) {
                    $value                                     = $detail[$childDefinition->Name];
                    $insertData[$childDefinition->MappingName] = $value;
                    if ($value)
                        $isEmpty = false;
                }

                $insertData["TableID"] = $tableID;
                $insertData["ID"]      = null;
                if (!$isEmpty) {
                    if ($facTableDetail->insert($insertData) == false)
                        return AMRespone::dbFailed($insertData, "Insert table request field");
                    $detail["ID"] = $wpdb->insert_id;
                }
            }

            return new AMRespone($tableID);
        }

        function _InsertField(RequestData $requestData, &$field) {
            global $wpdb;

            import('plugin.amWorkflow.class.includes.RequestDataTypeID');
            $listFieldDefinition = &$requestData->listFieldDefinition;
            $insertData          = ["RequestID" => $requestData->request->ID];
            $owner               = null;
            $updateLasters       = [];

            foreach ($listFieldDefinition as $fieldDefinition) {
                $name  = $fieldDefinition->Name;
                $value = $field[$name];

                $insertValue = null;
                $fieldValue  = $value;
                switch ($fieldDefinition->RequestDataTypeID) {
                    case RequestDataTypeID::$table:
                        if ($value == null)
                            continue;

                        // get table detail id
                        $temp = $this->_InsertTableDetail($fieldDefinition, $value, $requestData->request->ID);
                        if ($temp->code != AMResponeCode::$ok)
                            return $temp;

                        $insertValue = $temp->data;
                        $listChild   = [];
                        foreach ($value as $item)
                            $listChild[] = (object)$item;

                        $fieldValue = $listChild;
                        break;

                    case RequestDataTypeID::$user:
                        if ($fieldValue) {
                            $fieldValue = (object)$fieldValue;
                            $domain     = $fieldValue->Domain;
                            if ($domain) {
                                $temp = $this->_GetUserInfo($requestData->request, $fieldValue->Domain, true);
                                if ($temp->code != AMResponeCode::$ok)
                                    return $temp;

                                $fieldValue  = $temp->data;
                                $insertValue = $fieldValue->Domain;
                            }
                        }
                        break;

                    default:
                        $insertValue = $value;
                        break;
                }

                $insertData[$fieldDefinition->MappingName] = $insertValue;
                $requestData->setFieldvalueByDefinition($fieldDefinition, $fieldValue);
            }

            $re = new stdClass();
            import('plugin.amWorkflow.class.factory.AMRequestFieldFactory');
            $facField = new AMRequestFieldFactory();
            if ($facField->insert($insertData) == false)
                return AMRespone::dbFailed($insertData);
            $re->fieldValuesID = $wpdb->insert_id;
            $re->updateLasters = $updateLasters;

            return new AMRespone($re);
        }

        function _GetRequestDefinition($wfID, $getCurrentDomain = true) {

            // declare workflow geter
            import('plugin.amWorkflow.class.includes.WorkflowGet');
            $wfGeter = new WorkflowGet();

            // get workflow definition
            $temp = $wfGeter->GetWorkflowData($wfID, false, $getCurrentDomain);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;
            $re = $temp->data;

            return new AMRespone($re);
        }

        function _GetRequestReferData($requestData) {
            $re = new stdClass();

            // get list expected user for list request step
            if (!$requestData->request) {
                $mapExpectedDomain = [];
                $facRequest        = new AMRequestFactory();
                foreach ($requestData->listStepDefinition as $stepDefinition) {
                    $role = $stepDefinition->Role;
                    if (is_numeric($role) == false)
                        continue;

                    $result = $facRequest->getSuggestDomainByRole($role, null, null, $requestData->currentDomain);
                    if ($result === false)
                        return AMRespone::dbFailed($role);
                    else if ($result == null) {
                        $res                = AMRespone::busFailed($role, "no user for roleID");
                        $wfDisplayName      = $requestData->workflow->DisplayName;
                        $stepDefDisplayName = $stepDefinition->DisplayName;
                        $res->report("Bugs - Undefined user for role", "Workflow: $wfDisplayName<br>Step: $stepDefDisplayName");
                        return $res;
                    }

                    $mapExpectedDomain[$role] = $result;
                }
                $re->mapExpectedDomain = $mapExpectedDomain;
            }

            // sub step
            $temp = $this->_GetListSubStepData($requestData->request->ID);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;
            $re->subRequests = $temp->data;

            //list role
            import('plugin.amRolePermission.class.factory.AMRoleManagementFactory');
            $temp = (new AMRoleManagementFactory())->query(array());
            if (is_array($temp) == false)
                return AMRespone::dbFailed();
            $re->listRole = $temp;

            return new AMRespone($re);
        }

        function _UpdateField(RequestData $requestData, $fieldCan, $field) {
            import("plugin.amWorkflow.class.factory.AMRequestFieldFactory");
            import('plugin.amWorkflow.class.factory.AMRequestTableData');
            import('plugin.amWorkflow.class.factory.AMRequestTableDetailData');
            if (function_exists("updateTableDetail") == false) {
                function updateTableDetail(RequestData $requestData, $fieldCan, $name, $newTableData) {
                    if (!$fieldCan)
                        return AMRespone::busFailed($fieldCan, 'FieldCan be null, field data can not be saved');

                    // declare
                    $facTableDetail  = new AMRequestTableDetailData();
                    $tableDefinition = $requestData->getFieldDefinition($name);
                    $oldTableData    = $tableDefinition->value;
                    $finalTableData  = [];
                    $tableID         = null;
                    global $wpdb;

                    // preparse
                    if (sizeof($oldTableData) == 0)
                        $tableID = (new AMRequestTableData())->createID($requestData->request->ID, $tableDefinition->MappingName);
                    else
                        $tableID = $oldTableData[0]->TableID;

                    foreach ($newTableData as $newRow) {
                        // finding exists row
                        $newRowID = $newRow["ID"];
                        $row      = null;
                        if ($newRowID !== null) {
                            foreach ($oldTableData as $item) {
                                if ($item->ID == $newRowID) {
                                    $row = $item;
                                    break;
                                }
                            }
                        }

                        if ($row == null) {
                            // if not exists, create new
                            $insertData = [];
                            foreach ($tableDefinition->listChild as $colDefinition)
                                $insertData[$colDefinition->MappingName] = $newRow[$colDefinition->Name];

                            if ($facTableDetail->insert($insertData) === false)
                                return AMRespone::dbFailed($insertData, "Insert table detail failed");

                            $row              = (object)$newRow;
                            $row->ID          = $wpdb->insert_id;
                            $finalTableData[] = $row;
                        }
                        else {
                            $updateData = null;
                            foreach ($newRow as $name => $value) {
                                $can = $fieldCan->{$name};
                                if ($can->edit) {
                                    if ($updateData == null)
                                        $updateData = ["TableID" => $tableID];
                                    $fieldDefinition                           = $requestData->getFieldDefinition($name);
                                    $updateData[$fieldDefinition->MappingName] = $value;
                                    $row->{$name}                              = $value;
                                }
                            }
                            if ($updateData == null) {
                                AMRespone::dbFailed([$newRow, $can], "Update table detail data null, be skiped");
                                continue;
                            }

                            if ($facTableDetail->update($updateData, ["ID" => $newRowID]) === false)
                                return AMRespone::dbFailed($updateData, "Update table detail failed");
                        }
                    }
                    return new AMRespone($tableID);
                }

            }

            $insertData = [];
            foreach ($field as $name => $value) {
                $canEdit = $fieldCan->{$name};
                if ($canEdit == false)
                    continue;

                $mappingName = $requestData->getFieldDefinition($name)->MappingName;
                if (is_array($value)) {
                    $field = $requestData->getFieldDefinition($name);
                    if ($field->RequestDataTypeID == RequestDataTypeID::$user) {
                        $insertData[$mappingName] = $value->Domain;
                    }
                    else {
                        $temp = updateTableDetail($requestData, $canEdit, $name, $value);
                        if ($temp->code != AMResponeCode::$ok)
                            return $temp;
                        $insertData[$mappingName] = $temp->data;
                    }
                }
                else {
                    $requestData->setFieldValue($name, $value);
                    $insertData[$mappingName] = $value;
                }
            }

            $request = $requestData->request;
            if ($insertData) {
                $facField = new AMRequestFieldFactory();
                if ($facField->update($insertData, ["RequestID" => $request->ID]) === false)
                    return AMRespone::dbFailed($insertData, "update request field failed");
            }

            return new AMRespone();
        }

        function _SaveFieldData(RequestData $requestData, $field) {
            $fieldCan = $requestData->currentFieldCan;
            if (!$fieldCan)
                return AMRespone::busFailed($fieldCan, 'FieldCan be null, field data can not be saved');

            import("plugin.amWorkflow.class.factory.AMRequestFieldFactory");
            import('plugin.amWorkflow.class.factory.AMRequestTableData');
            import('plugin.amWorkflow.class.factory.AMRequestTableDetailData');
            if (function_exists("saveTableDetail") == false) {
                function saveFieldData(RequestData $requestData, $fieldCan, $listFieldDefinition, $dataModel, $insertData, $fac, $thisContext) {
                    $returnData = new stdClass();

                    foreach ($listFieldDefinition as $fieldDefinition) {
                        if (WorkflowGet::isNoneDataType($fieldDefinition->RequestDataTypeID))
                            continue;

                        // init
                        $name = $fieldDefinition->Name;
                        $can  = $fieldCan->{$name};

                        // check
                        if (!$can->update)
                            continue;
                        $fieldValue = $dataModel[$fieldDefinition->Name];

                        if (!$fieldValue && is_numeric($fieldValue) == false) {
                            if (!$can->empty)
                                return AMRespone::busFailed($dataModel, "Field '$name' can not be null");
                            else
                                continue;
                        }


                        $insertValue = $fieldValue;
                        $returnValue = $fieldValue;
                        // addition data
                        switch ($fieldDefinition->RequestDataTypeID) {
                            case RequestDataTypeID::$table:
                                // get table detail id
                                $temp = saveTableDetail($requestData, $fieldCan, $fieldDefinition, $fieldValue, $thisContext);
                                if ($temp->code != AMResponeCode::$ok)
                                    return $temp;

                                $insertValue = $temp->data;
                                $dataValue   = $fieldDefinition->value;
                                break;

                            case RequestDataTypeID::$user:
                                if ($fieldValue) {
                                    $domain = is_string($fieldValue) ? $fieldValue : $fieldValue['Domain'];
                                    if ($domain) {
                                        $temp = $thisContext->_GetUserInfo($requestData->request, $domain, true);
                                        if ($temp->code != AMResponeCode::$ok)
                                            return $temp;

                                        $fieldValue  = $temp->data;
                                        $insertValue = $fieldValue->Domain;
                                        $dataValue   = $fieldValue;
                                    }
                                }
                                break;

                            default:
                                $insertValue = $fieldValue;
                                $dataValue   = $fieldValue;
                                break;
                        }

                        $insertData[$fieldDefinition->MappingName] = $insertValue;
                        $returnData->{$fieldDefinition->Name}      = $dataValue;

                    }

                    if ($insertData['ID']) {
                        if (sizeof($insertData) > 1)
                            $fac->update($insertData, ['ID' => $insertData['ID']]);
                    }
                    else {
                        unset($insertData['ID']);
                        if (sizeof($insertData) < 2) {
                            return AMRespone::busFailed([$listFieldDefinition, $dataModel], 'Can not insert empty data record');
                        }
                        global $wpdb;
                        $fac->insert($insertData);
                        $insertData['ID'] = $wpdb->insert_id;
                        $returnData->ID   = $wpdb->insert_id;
                    }

                    return new AMRespone($returnData);
                }

                function saveTableDetail(RequestData $requestData, $fieldCan, $tableDefinition, $dataModels, $thisContext) {
                    // declare
                    $facTableDetail = new AMRequestTableDetailData();
                    $oldTableData   = $tableDefinition->value;
                    $finalTableData = [];
                    $can            = $fieldCan->{$tableDefinition->Name};
                    $tableID        = sizeof($oldTableData) == 0 ? (new AMRequestTableData())->createID($requestData->request->ID, $tableDefinition->MappingName) : $oldTableData[0]->TableID;

                    global $wpdb;
                    // del old
                    if ($can->edit && $oldTableData) {
                        $delRowID = [];
                        foreach ($oldTableData as $oldRow) {
                            foreach ($dataModels as $newRow) {
                                if ($oldRow->ID == $newRow['ID'])
                                    continue 2;
                            }

                            $delRowID[] = $oldRow->ID;
                        }

                        if ($delRowID)
                            $facTableDetail->deleteByIDs($delRowID);

                    }

                    foreach ($dataModels as $newRow) {
                        // finding exists row
                        $newRowID = $newRow["ID"];
                        $oldRow   = null;
                        if ($newRowID !== null) {
                            foreach ($oldTableData as $item) {
                                if ($item->ID == $newRowID) {
                                    $oldRow = $item;
                                    break;
                                }
                            }
                        }

                        if ($oldRow == null && !$can->edit)
                            continue;

                        $saveData = [
                            "TableID" => $tableID,
                            'ID' => $oldRow->ID
                        ];

                        $temp = saveFieldData($requestData, $fieldCan, $tableDefinition->listChild, $newRow, $saveData, $facTableDetail, $thisContext);
                        if ($temp->code != AMResponeCode::$ok)
                            return $temp;

                        $data     = $temp->data;
                        $finalRow = $oldRow;
                        if ($oldRow == null) {
                            $oldRow = $temp->data;
                        }
                        else {
                            foreach ($data as $key => $value) {
                                $oldRow->{$key} = $value;
                            }
                        }

                        $finalTableData[] = $oldRow;
                    }

                    $tableDefinition->value = $finalTableData;
                    return new AMRespone($tableID);
                }
            }

            $requestID = $requestData->request->ID;
            $facField  = new AMRequestFieldFactory();

            $temp = $facField->query(['filter' => [["RequestID = $requestID"]]]);

            $fieldDataID = $temp ? $temp[0]->ID : null;

            $saveData = [
                'RequestID' => $requestID,
                'ID' => $fieldDataID
            ];

            $temp = saveFieldData($requestData, $fieldCan, $requestData->listFieldDefinition, $field, $saveData, $facField, $this);

            if ($temp->code != AMResponeCode::$ok)
                return $temp;

            $saveData = $temp->data;
            foreach ($saveData as $name => $value) {
                foreach ($requestData->listFieldDefinition as $fieldDefinition) {
                    if ($name == $fieldDefinition->Name)
                        $fieldDefinition->value = $value;
                }
            }

            return new AMRespone();
        }

        function _UpdateRequestData($request, $newStatusID, AMRequestFactory $facRequest = null) {
            if ($newStatusID == RequestStatusID::$open) {
                return AMRespone::busFailed([$request, $newStatusID], "update requestStatusID to open is not support");
            }

            if ($facRequest == null)
                $facRequest = new AMRequestFactory();

            $requestInArray = (array)$request;
            unset($requestInArray['RequestStatusID']);
            if ($newStatusID && $request->RequestStatusID != $newStatusID) {
                $canUpdateStatus = true;
                $parrentID       = $request->ParrentID;
                if ($parrentID && $facRequest->isOpening($parrentID)) {
                    // done parrent request
                    $canDoneParrent = $facRequest->isHasSubOpening($parrentID, $request->ID) == false;
                    $canDoneParrent = $canDoneParrent && $facRequest->isDoneAllStep($parrentID);
                    if ($canDoneParrent) {
                        $updateData = ["RequestStatusID" => RequestStatusID::$done];
                        if ($facRequest->update($updateData, ["ID" => $parrentID]) === false) {
                            return AMRespone::dbFailed($updateData, "update parrent request failed");
                        }
                    }
                }

                if ($newStatusID == RequestStatusID::$done && $facRequest->isHasSubOpening($request->ID))
                    $canUpdateStatus = false;
            }

            if ($canUpdateStatus) {
                $request->RequestStatusID          = $newStatusID;
                $requestInArray['RequestStatusID'] = $newStatusID;
            }

            $facRequest->standardizeData($requestInArray);
            $result = $facRequest->update($requestInArray, ["ID" => $request->ID]);
            if ($result === false)
                return AMRespone::dbFailed($requestInArray, "Update request failed");

            return new AMRespone($request);
        }

        function _DoApproveRequestStep(RequestData $requestData, $stepIndex, $approverDomain, $isApproved, $comment = null, $field = null, $attachments = null) {
            if ($requestData->request->CurrentIndex == 0 && $requestData->taskEventID() === null) {
                $requestData->taskEventID(EventID::$reCreate);
            }

            if ($field) {
                //$temp = $this->_UpdateField($requestData, $fieldCan, $field);
                $temp = $this->_SaveFieldData($requestData, $field);
                if ($temp->code != AMResponeCode::$ok)
                    return $temp;

                $requestData->userInputFieldData = $field;
            }

            if (function_exists("approveRequestStep") == false) {
                import('plugin.amWorkflow.class.factory.AMRequestCommentFactory');
                function approveRequestStep(RequestData $requestData, $stepIndex, $approverDomain, $isApproved, $comment, $attachments, $thisContext, $isRecursive = false, &$commentID = null) {
                    // get current step
                    $listStepDefinition = $requestData->listStepDefinition;
                    $request            = $requestData->request;
                    $currentStep        = $requestData->getCurrentStep();

                    if ($currentStep == null) {
                        if ($isRecursive)
                            return new AMRespone();
                        else
                            return AMRespone::busFailed($requestData, "Get current step failed");
                    }
                    $currentStepDefinition = $currentStep->definition;
                    $canApproval           = $currentStep->Owner == $approverDomain;
                    if ($canApproval == false && (is_numeric($currentStepDefinition->Role))) {
                        $temp = $thisContext->_SetStepOwner($currentStep, $requestData, null, $approverDomain);
                        if ($temp->code != AMResponeCode::$ok)
                            return $temp;

                        $canApproval = $currentStep->Owner == $approverDomain;
                    }

                    /**
                     * bỏ auto approve next step
                     * remove auto approve next step:
                     * tránh trường hợp người cài đặt, chuyển giao và người nhận hàng là 1,
                     * khi đó cài đặt chưa xong mà đã confirm là nhận hàng
                     */
                    $canApproval = $canApproval && $isRecursive == false;

                    if ($canApproval) {
                        $currentStep->ActionDate = date('Y-m-d H:i:s');
                        $currentStep->Owner      = $approverDomain;
                        $currentStep->IsApproved = $isApproved;

                        import("plugin.amWorkflow.class.factory.AMRequestCommentRoleFactory");
                        $facComentRole = new AMRequestCommentRoleFactory();
                        if ($facComentRole->removeRole($currentStep->ID) === false)
                            return AMRespone::dbFailed($currentStep->ID, "Remove requestCommentRole failed");
                    }
                    else {
                        if ($isRecursive) {
                            $currentStep->StartDate  = date('Y-m-d H:i:s');
                            $currentStep->IsApproved = null;
                            $currentStep->ActionDate = null;
                        }
                        else {
                            return new AMRespone($currentStepDefinition, AMResponeCode::$denied, "Access denied: '$approverDomain' can not edit step");
                        }
                    }

                    // update step
                    $facStep     = new AMRequestStepFactory();
                    $stepInArray = (array)$currentStep;
                    unset($stepInArray["definition"]);
                    $result = $facStep->update($stepInArray, ["ID" => $currentStep->ID]);
                    if ($result === false)
                        return AMRespone::dbFailed($stepInArray, "Update step failed");

                    if ($canApproval == false)
                        return new AMRespone();

                    // find next step
                    $autoAprroveNext  = true;
                    $newRequestStatus = null;
                    $nextStepIndex    = $request->CurrentIndex;
                    if ($isApproved) {
                        $max = sizeof($requestData->listStepDefinition) - 1;
                        if ($nextStepIndex >= $max) {
                            $autoAprroveNext  = false;
                            $nextStepIndex    = $max;
                            $newRequestStatus = RequestStatusID::$done;
                        }
                        else {
                            $n = sizeof($listStepDefinition);
                            for ($i = $nextStepIndex + 1; $i < $n; $i++) {
                                $stepDefinition = $listStepDefinition[$i];
                                $step           = $stepDefinition->step;
                                if ($step->IsApproved !== null) {
                                    $timeMin = new DateTime($currentStep->MinIntendTime);
                                    $timeMax = new DateTime($currentStep->MaxIntendTime);
                                    $timeMin->modify("+$stepDefinition->MinIntendDuration hours");
                                    $timeMax->modify("+$stepDefinition->MaxIntendDuration hours");

                                    $step->MinIntendTime = $timeMin->format('Y-m-d H:i:s');
                                    $step->MaxIntendTime = $timeMax->format('Y-m-d H:i:s');
                                    $step->IsApproved    = null;
                                    $step->ActionDate    = null;
                                    $step->StartDate     = null;
                                    $stepInArray         = (array)$step;
                                    unset($stepInArray["definition"]);

                                    if ($facStep->update($stepInArray, ["ID" => $step->ID]) === false)
                                        AMRespone::dbFailed($stepInArray, "Update requestStep failed");
                                }
                            }
                            $nextStepIndex += 1;
                        }
                    }
                    else {
                        if ($nextStepIndex <= 0) {
                            $nextStepIndex    = -1;
                            $newRequestStatus = RequestStatusID::$cancel;
                            $autoAprroveNext  = false;
                        }
                        else {
                            $stepDefinition = null;
                            $backStepName   = $currentStepDefinition->BackStep;
                            if ($backStepName == null) {
                                $stepDefinition = $listStepDefinition[0];
                            }
                            else {
                                foreach ($listStepDefinition as $item) {
                                    if ($item->Name == $backStepName) {
                                        $stepDefinition = $item;
                                        break;
                                    }
                                }
                                if ($stepDefinition == null)
                                    return AMRespone::busFailed("BackStep not found");
                            }
                            $nextStepIndex = $stepDefinition->StepIndex;
                        }
                    }
                    $requestData->setNextStepIndex($nextStepIndex);

                    $params = [
                        'stepID' => $requestData->getCurrentStep()->step->ID,
                        'comment' => $comment,
                        'domain' => $approverDomain,
                        'attachments' => $attachments
                    ];
                    $temp   = $thisContext->_DoRequestTask($requestData, $isApproved ? EventID::$approved : EventID::$reject, $params);
                    if ($temp->code != AMResponeCode::$ok)
                        return $temp;

                    global $wpdb;
                    $commentID = $wpdb->insert_id;

                    // update request
                    $facRequest            = new AMRequestFactory();
                    $request->CurrentIndex = $nextStepIndex;
                    $re                    = $thisContext->_UpdateRequestData($request, $newRequestStatus, $facRequest);
                    if ($re->code != AMResponeCode::$ok)
                        return $re;

                    if ($autoAprroveNext)
                        return approveRequestStep($requestData, $stepIndex, $approverDomain, $isApproved, null, $attachments, $thisContext, true);
                    else
                        return new AMRespone();
                }
            }

            // remove approval code
            $facApprovalCode = new AMRequestApprovalCode();
            $facApprovalCode->delete(['RequestID' => $requestData->request->ID]);

            // approval
            $commentID = null;
            $response  = approveRequestStep($requestData, $stepIndex, $approverDomain, $isApproved, $comment, $attachments, $this, false, $commentID);
            if ($response->code == AMResponeCode::$ok) {
                $re             = new stdClass();
                $re->commentID  = $commentID;
                $response->data = $re;
            }

            return $response;
        }

        function _GetRequestData($requestID, $getCurrentDomain = true) {

            // get request data
            $request = (new AMRequestFactory())->getByID($requestID);
            if ($request === false)
                return AMRespone::dbFailed($requestID);

            if ($request === null)
                return AMRespone::notFound($requestID);

            // get define data
            $temp = $this->_GetRequestDefinition($request->DefinitionID, $getCurrentDomain);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;
            $temp->data->request = $request;
            $re                  = new RequestData($temp->data);

            // get field data
            $field = $this->_GetFields($re);
            if (is_object($field) == false)
                return AMRespone::dbFailed($requestID);
            $re->setField($field);
            $re->setRequestOwner($field->__RequestOwner);

            // get step data
            $temp = (new AMRequestStepFactory())->query(["filter" => [["RequestID = $requestID"]]]);
            if (is_array($temp) == false)
                return AMRespone::dbFailed($requestID);
            $re->setListStep($temp);

            // get current editable
            $re->currentFieldCan = $re->getFieldCan();
            return new AMRespone($re);
        }

        function _GetRequestDataByStepID($stepID, $getCurrentDomain = true) {
            $facStep = new AMRequestStepFactory();
            $step    = $facStep->getByID($stepID);

            if ($step == false)
                return AMRespone::dbFailed($stepID, 'get step by ID failed');

            return $this->_GetRequestData($step->RequestID, $getCurrentDomain);
        }

        function _SetStepOwner($step, RequestData $requestData, $prioritySite = null, $priorityDomain = null) {
            $facRequest     = new AMRequestFactory();
            $stepDefinition = $step->definition;
            $stepIndex      = $stepDefinition->StepIndex;

            if ($stepIndex < $requestData->StepIndex)
                return new AMRespone();

            $stepRole   = $step->definition->Role;
            $stepDomain = null;

            if ($stepRole === null) {
                if ($stepIndex == 0) {
                    if ($step->Owner)
                        $stepDomain = $step->Owner;
                    else
                        $stepDomain = $priorityDomain;
                }
                else {
                    return AMRespone::busFailed($step->definition, "Missing role step");
                }
            }
            else if ($stepRole === RoleID::$user) {  // get domain by user
                return AMRespone::busFailed($step->definition, "Can not identify user for role 'User'. Please change step role");
            }
            else if (is_string($stepRole)) { // get domain for user of deapt head
                if (strlen($stepRole) < 3)
                    return AMRespone::busFailed($stepRole, "Role invalidate");

                $roleID         = $stepRole[0] + 0;
                $fieldName      = substr($stepRole, 2);
                $fieldefinition = $requestData->getFieldDefinition($fieldName);

                $useData = $fieldefinition->value;
                if ($useData == null)
                    return AMRespone::busFailed($stepRole, "Field '$fieldName' can not be null");

                if (is_string($useData)) {
                    // dev_upgrade`
                    // tránh lỗi trước mắt, khi remove kiểu dữ liệu domain đi rồi thì không cần nữa
                    $useData = $this->_GetUserInfo(null, $useData);
                }

                if ($roleID == RoleID::$user) {
                    $stepDomain = $useData->Domain;

                    $cheatConfig = using('plugin.itportal.config.Cheat')['wfTask'];
                    if ($stepDomain == $cheatConfig['user'])
                        $stepDomain = $cheatConfig['delegate'];
                }
                else if ($roleID == RoleID::$deptHead) {
                    $roleAPI    = using('plugin.itportal.API.RoleAndPermission.APIRoleAndPermission');
                    $role       = $roleAPI->GetDeptHeadByDeptName(['deptName' => $useData->Department]);
                    $stepDomain = $role->Domain;
                }
            }
            else {
                // get domain by roleID
                $domain = $facRequest->getSuggestDomainByRole($stepRole, $prioritySite, $step->Owner, $priorityDomain);

                if ($domain === false)
                    return AMRespone::dbFailed($stepRole);
                else if ($domain == null)
                    return AMRespone::busFailed($stepRole, "No user for roleID");

                $stepDomain = $domain;
            }

            $step->Owner = $stepDomain;

            return new AMRespone();
        }

        function _GetWhereIn($list, $attr) {
            $listIn = [];
            foreach ($list as $item)
                $listIn[] = $item->{$attr};

            return "(" . join(", ", $listIn) . ")";
        }

        function _GetFields($requestData) {
            $fac       = new AMRequestFieldFactory();
            $fieldData = $fac->getFieldByRequestID($requestData->request->ID);

            if (function_exists('parseDatabaseFieldValue') == false) {
                function parseDatabaseFieldValue($value, $fieldDefinition, $requestData, $thisConext) {
                    if ($value == "")
                        return $value;

                    switch ($fieldDefinition->RequestDataTypeID) {
                        case RequestDataTypeID::$table:
                            $facTableData = new AMRequestTableDetailData();

                            $where         = ["filter" => [["TableID = " . $value]]];
                            $listChildData = $facTableData->query($where);
                            if (is_array($listChildData) == false) {
                                logAM("Get request table data failed");
                                return false;
                            }

                            $listChild = [];
                            foreach ($listChildData as $row) {
                                $child          = new stdClass();
                                $listChild[]    = $child;
                                $child->ID      = $row->ID;
                                $child->TableID = $row->TableID;
                                foreach ($fieldDefinition->listChild as $childDefinition) {
                                    if (!$childDefinition->MappingName)
                                        continue;

                                    $jsonColValue                    = $row->{$childDefinition->MappingName};
                                    $child->{$childDefinition->Name} = parseDatabaseFieldValue($jsonColValue, $childDefinition, $requestData, $thisConext);
                                }
                            }

                            return $listChild;

                        case RequestDataTypeID::$number:
                            return am_json_decode($value);


                        case RequestDataTypeID::$user:
                            $temp = $thisConext->_GetUserInfo($requestData->request, $value);
                            if ($temp->code != AMResponeCode::$ok)
                                return null;
                            else
                                return $temp->data;

                        default:

                            return $value;
                    }
                }
            }

            $field     = new stdClass();
            $field->ID = $fieldData->ID;
            foreach ($requestData->listFieldDefinition as $fieldDefinition) {
                if (!$fieldDefinition->MappingName)
                    continue;

                $jsonValue                       = $fieldData->{$fieldDefinition->MappingName};
                $field->{$fieldDefinition->Name} = parseDatabaseFieldValue($jsonValue, $fieldDefinition, $requestData, $this);
            }

            $field->__RequestOwner = $fieldData->Field0;
            return $field;

        }

        function _GetUserInfo($request, $domain, $updateRequestUser = false) {
            $re = null;
            if ($request == null || $request->RequestStatusID & RequestStatusID::$active) {
                $re = (new AMUserFactory())->GetUserInformationByDomain($domain);
                //preg_match('/\d+$/', $domain, $matches);
                //$number     = $matches ? " ($matches[0])" : "";

                if (!$re)
                    return AMRespone::dbFailed([$request, $domain], "GetUserInfo failed");

                $re->Domain    = $domain;
                $re->RequestID = $request ? $request->ID : null;

                // $re->Name           = $result->FullName;
                // $re->JobTitle       = $result->Title;
                // $re->Department = $result->Department;
                // $re->Seat           = $result->UserPosition->Table;
                // $re->Location       = $result->UserPosition->Site;
                if ($updateRequestUser) {
                    $fac  = new AMRequestUser();
                    $temp = $fac->getOne([
                        'filter' => [
                            ["Domain = '$domain'", 'AND'],
                            ["RequestID = $request->ID"],

                        ]
                    ]);

                    $data = (array)$re;
                    $fac->standardizeData($data);

                    if ($temp)
                        $fac->update($data, ["Domain" => "'$domain'", "RequestID" => $request->ID]);
                    else
                        $fac->insert($data);
                }

            }
            else {
                $facRequestOwner = new AMRequestUser();
                $re              = $facRequestOwner->getOne([
                    'filter' => [
                        ["Domain = '$domain'", 'AND'],
                        ["RequestID = $request->ID"],
                    ]
                ]);

                if ($re == false)
                    return AMRespone::dbFailed([$request, $domain], "GetUserInfo failed");
            }

            return new AMRespone($re);
        }

    }
}

return new AMRequestAjax();





