<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if (!class_exists('APIRequestFeatureMapping')) {

    import('plugin.itportal.Factory.AMRequestFeatureMappingFactory');

    class APIRequestFeatureMapping
    {

        private $fac = null;

        function __construct()
        {
            $this->fac = new AMRequestFeatureMappingFactory();
        }

        public function GetAll()
        {
            $result = $this->fac->query();
            return $result;
        }

        public function Insert($param)
        {
            $data = $param['data'];
            $data['Mapping'] = urldecode(stripslashes($data['Mapping']));
            $result = $this->fac->insert($data);
            return [
                State=>$result===1?true:false
            ];
        }

        public function Update($param)
        {
            $data = $param['data'];
            $data['Mapping'] = urldecode(stripslashes($data['Mapping']));
            $result = $this->fac->update($data, [
                "ID"=>$data['ID']
            ]);
            return [
                State=>$result===1?true:false
            ];
        }

        public function GetByName($param)
        {
            $Name = $param['featureName'];
            $result = $this->fac->getOne(array(
                filter => array( array("Name = '".$Name."'", "") )
            ));
            return $result;
        }
        public function GetRequestID($param)
        {
            $ID = $param['requestID'];
            $result = $this->fac->getOne(array(
                filter => array( array("RequestID = ".$ID, "") )
            ));
            return $result;
        }

        public function GetPaging($param)
        {
            $keyword = isset($param['keyword']) ? $param['keyword'] : null;
            $pageIndex = isset($param['pageIndex']) ? $param['pageIndex'] : null;
            $limit = isset($param['limit']) ? $param['limit'] : null;

            $filter = array();
            $pagging = array();

            if ($keyword !== null && $keyword != '') {
                array_push($filter, array("a.Name like '%" . $keyword . "%'", "OR"));
                array_push($filter, array("b.DisplayName like '%" . $keyword . "%'", ""));
            }

            if ($limit != null && $limit != 0) {
                $pagging['at'] = $pageIndex * $limit;
                $pagging['length'] = $limit;
            }

            $query = array();
            $query['select'] = "a.ID 'ID', " .
                "a.Name 'Name', " .
                "a.Output 'Output', " .
                "a.Mapping 'Mapping', " .
                "a.RequestID 'RequestID', " .
                "b.DisplayName 'RequestDisplayName'," .
                "b.Name 'RequestName'";
            $query['join'] = array(
                array(
                    "JOIN", "am_request_definition b", "a.RequestID", "b.ID"
                )
            );

            if (sizeof($filter) > 0) {
                array_push($query, $filter);
            }
            if (sizeof($pagging) > 0) {
                array_push($query, $pagging);
            }
            $count = $this->fac->getOne(array(
                'select' => "count(ID) Count"
            ));
            $mapping = $this->fac->query($query);
            $result = array();
            $result['PageCount'] = ($count->Count == 0) ? 0 : $count->Count / $limit;
            $result['Result'] = $mapping;
            return $result;
        }
    }
}
return new APIRequestFeatureMapping();