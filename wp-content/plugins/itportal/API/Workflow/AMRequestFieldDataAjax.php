<?php

if (class_exists('AMRequestFieldDataAjax') == false) {
    import("plugin.itportal.API.CMDB.CMDB");
    ini_set('xdebug.var_display_max_depth', 5);
    ini_set('xdebug.var_display_max_children', 256);
    ini_set('xdebug.var_display_max_data', 1024);

    class AMRequestFieldDataAjax {
        public function Search($params) {

            $searchData = $params['dataSearch'];

            if($searchData['SearchValue'])
                $searchData['SearchValue'] = stripslashes($searchData['SearchValue']);

            if($searchData['Constrains']){
                $newArray = [];
                foreach ($searchData['Constrains'] as $constrain){
                    $temp = $constrain;
                    if(is_array($temp['SearchValue'])){
                        var_dump($searchData);
                        var_dump($temp['SearchValue']);
                    }
                    $temp['SearchValue'] = stripslashes($temp['SearchValue']);
                    $newArray[] = $temp;

                }

                $searchData['Constrains'] = $newArray;
            }

            $searchDataInJson = am_json_encode($searchData);
            $cmdb = new CMDB();
            $result = $cmdb->get(
                'CMDBEX',
                ['m', 'input'],
                ['DataConstrainSearch', $searchDataInJson]
            );

            $re =  json_decode($result);
            if(is_array($re) == false)
                return AMRespone::busFailed($result, 'CMDB request field search failed');

            return new AMRespone($re);
        }

    }
}
return new AMRequestFieldDataAjax();


















