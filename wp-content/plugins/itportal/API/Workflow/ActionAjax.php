<?php
if ( ! function_exists( 'import' ) ) {
    require_once get_template_directory() . '/package/import.php';
}
import('plugin.amWorkflow.class.includes.ActionManager');

if (!class_exists('ActionAjax')) {
    class ActionAjax{
        private $actionManager;

        public function __construct(){
            $this->actionManager = new ActionManager();
        }

        public function getActions(){
            echo json_encode($this->actionManager->GetActions());
        }

        public function getMethods($_PARAM){
            $action = $_PARAM['actionName'];
            echo json_encode($this->actionManager->GetMethods($action));
        }

        public function doAction($_PARAM)
        {
            $action = $_PARAM['actionName'];
            $method = $_PARAM['method'];
            echo json_encode($this->actionManager->DoAction($action, $method, $_PARAM));
        }
    }
}
return new ActionAjax();