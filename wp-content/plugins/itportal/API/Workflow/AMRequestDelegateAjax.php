<?php
if (!class_exists('AMRequestDelegateAjax')) {
    import('plugin.itportal.Business.Workflow.RequestDelegateBus');
    class AMRequestDelegateAjax {
        private $bus;

        public function __construct() {
            $this->bus = new RequestDelegateBus();

        }

        /**
         * @security delegate|owner:domain
         */
        public function GetDelegate($params) {
            return $this->bus->GetDelegate($params['domain']);
        }

        /**
         * @security delegate|api-custom
         */
        public function CanSettingForAnotherUser($params) {
            global $security;
            return new AMRespone($security->allow);
        }

        /**
         * @security delegate|api-custom
         */
        public function UpdateDelegate($params) {
            $temp = $this->getSaveData($params);
            if($temp->code != AMResponeCode::$ok)
                return $temp;

            return $this->bus->UpdateDelegate($temp->data);
        }

        /**
         * @security delegate|api-custom
         */
        public function InsertDelegate($params) {
            $temp = $this->getSaveData($params);
            if($temp->code != AMResponeCode::$ok)
                return $temp;

            return $this->bus->InsertDelegate($temp->data);
        }

        /**
         * @security delegate|owner:domain
         */
        public function DeleteDelegate($params) {
            return $this->bus->DeleteDelegate($params['domain']);
        }

        /**
         * @security delegate|api-custom
         */
        public function DelegateRequests($params){
            if(!isset($params['stepIDs']) || !isset($params['delegatedDomain']))
                return AMRespone::paramsInvalid($params);

            $stepIDs = $params['stepIDs'];
            $delegatedDomain = $params['delegatedDomain'];

            return $this->bus->DelegateRequestSteps($stepIDs, $delegatedDomain, $params['comment'], $params['attachments']);
        }

        function getSaveData($params){
            if(!isset($params['data']))
                return AMRespone::paramsInvalid($params);

            $data = $params['data'];
            $domain = $this->checkRoleAndGetDomain($data);
            if(!$domain)
                return AMRespone::denied();

            $data['Domain'] = $domain;
            return new AMRespone($data);
        }

        function checkRoleAndGetDomain($params){
            $domain = isset($params['Domain']) ? $params['Domain'] : null;
            if($domain){
                global $security;
                if($security->allow)
                    return $domain;

                if($security->checkOwner($domain))
                    return $domain;

                return null;
            }else{
                $currentUser = wp_get_current_user();
                if ($currentUser == 0 || $currentUser->user_login == false)
                    return null;
                $domain = $currentUser->user_login;
            }

            return $domain;
        }
    }
}

return new AMRequestDelegateAjax();