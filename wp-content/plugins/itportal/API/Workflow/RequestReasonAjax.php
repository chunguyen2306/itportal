<?php
if ( ! function_exists( 'import' ) ) {
    require_once get_template_directory() . '/package/import.php';
}
import('plugin.amWorkflow.class.factory.AMRequestReasonFactory');

if (!class_exists('RequestReasonAjax')) {
    class RequestReasonAjax
    {
        private $db;

        public function __construct()
        {
            $this->db = new AMRequestReasonFactory();
        }

        public function GetAll(){
            return $this->db->query();
        }
    }
}
return new RequestReasonAjax();