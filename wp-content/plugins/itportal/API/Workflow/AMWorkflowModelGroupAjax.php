<?php

if (!class_exists('AMWorkflowModelGroupAjax')) {
    import('plugin.itportal.Factory.AMRequestDefinitionModelGroupFactory');
    import('plugin.itportal.Factory.AMRequestDefinitionModelGroupDetailFactory');


    class AMWorkflowModelGroupAjax {
        public function GetListWorkflowModelGroup($params) {

            $fac = new AMRequestDefinitionModelGroupFactory();
            $re  = $fac->query([]);
            if ($re === false)
                return AMRespone::dbFailed($params);

            return new AMRespone($re);
        }

        public function GetWorkflowModelGroupEditData($params) {
            $id        = $params['ID'] + 0;
            $referData = new stdClass();

            if ($id)
                $response = $this->getWorkflowModelGroupData($id);
            else
                $response = new AMRespone();

            // get asset models from cmdb
            import("plugin.itportal.API.CMDB.CMDB");
            $cmdb          = new CMDB();
            $result        = $cmdb->get("CMDBEX", ["m"], ["GetListAssetModel"]);
            $allAssetModel = json_decode($result, true);
            if (is_array($allAssetModel) == false)
                return AMRespone::dbFailed($result, 'Get GetListAssetModel from CMDB failed');
            $referData->allAssetModel = $allAssetModel;
            $response->referData      = $referData;

            return $response;
        }

        public function InsertWorkflowModelGroup($params) {
            return $this->saveWorkflowModelGroup($params['group'], $params['Names'], true);
        }

        public function UpdateWorkflowModelGroup($params) {
            return $this->saveWorkflowModelGroup($params['group'], $params['Names'], false);
        }

        public function DeleteWorkflowModelGroup($params) {
            $id = $params['ID'] + 0;
            if ($id == 0)
                return AMRespone::paramsInvalid($params);

            $facDetails = new AMRequestDefinitionModelGroupDetailFactory();
            $facGroup   = new AMRequestDefinitionModelGroupFactory();
            global $wpdb;
            $res = null;
            try {
                $wpdb->query('START TRANSACTION');
                if ($facDetails->delete(['GroupID' => $id]) === false)
                    return $res = AMRespone::dbFailed($id, 'DeleteWorkflowModelGroupDetail failed');

                if ($facGroup->delete(['ID' => $id]) === false)
                    return $res = AMRespone::dbFailed($id, 'DeleteWorkflowModelGroup failed');
            } catch (Exception $ex) {
                return $res = AMRespone::exception($id, $ex);
            } finally {
                if ($res->code == AMResponeCode::$ok)
                    $wpdb->query('COMMIT');
                else
                    $wpdb->query('ROLLBACK');
            }

            return new AMRespone();
        }

        function getWorkflowModelGroupData($id) {

            // group
            $group = (new AMRequestDefinitionModelGroupFactory())->getByID($id);
            if (!$group)
                return AMRespone::dbFailed($id, 'Get GetWorkflowModelGroup failed');

            // details
            $modelNames = (new AMRequestDefinitionModelGroupDetailFactory())->getByGroupID($id);
            if (is_array($modelNames) == false)
                return AMRespone::dbFailed($id, 'Get GetWorkflowModelGroupDetail failed');

            // response data
            $data           = new stdClass();
            $data->group    = $group;
            $data->modelNames = $modelNames;
            return new AMRespone($data);
        }

        function saveWorkflowModelGroup($group, $modelNames, $isInsert) {
            if ($group == null || $modelNames == null)
                return AMRespone::paramsInvalid([$group, $modelNames, $isInsert]);

            $facGroup   = new AMRequestDefinitionModelGroupFactory();
            $facDetails = new AMRequestDefinitionModelGroupDetailFactory();
            global $wpdb;
            try {
                $wpdb->query('START TRANSACTION');

                $groupID = $group['ID'];
                if ($isInsert) {
                    $facGroup->insert($group);
                    $groupID = $wpdb->insert_id;
                }
                else {
                    $facGroup->update($group, ['ID' => $groupID]);
                    $facDetails->delete(['GroupID' => $groupID]);
                }

                foreach ($modelNames as $modelName) {
                    $insertData = ['GroupID' => $groupID,
                        'ModelID' => $modelName];
                    $facDetails->insert($insertData);
                }

                $wpdb->query('COMMIT');

                return $this->getWorkflowModelGroupData($groupID);
            } catch (Exception $ex) {
                $wpdb->query('ROLLBACK');
                return AMRespone::exception([$group, $modelNames, $isInsert], $ex);
            }
        }
    }
}

return new AMWorkflowModelGroupAjax();


















