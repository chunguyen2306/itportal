<?php
if ( ! function_exists( 'import' ) ) {
    require_once get_template_directory() . '/package/import.php';
}
import('plugin.amWorkflow.class.factory.AMRequestDataTypeFactory');

if (!class_exists('RequestDataTypeAjax')) {
    class RequestDataTypeAjax
    {
        private $db;

        public function __construct()
        {
            $this->db = new AMRequestDataTypeFactory();
        }

        public function GetAll(){
            return $this->db->query();
        }
    }
}
return new RequestDataTypeAjax();