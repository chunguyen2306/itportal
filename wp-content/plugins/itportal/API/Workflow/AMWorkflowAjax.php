<?php

if (!class_exists('AMWorkflowAjax')) {
    import('plugin.amWorkflow.class.factory.AMRequestDefinitionFactory');

    class AMWorkflowAjax {
        public function GetListWorkflow($params) {
            import('plugin.amWorkflow.class.factory.AMRequestDefinitionFactory');
            $fac         = new AMRequestDefinitionFactory();
            $listWorkfow = $fac->query([]);
            if ($listWorkfow === false)
                return AMRespone::busFailed(null, "Get list workflow failed");

            foreach ($listWorkfow as $wf) {
                $temp = $this->_CheckWorkflowFullEditAble($wf->ID);
                if ($temp->code != AMResponeCode::$ok)
                    return $temp;

                $wf->canFullEdit = $temp->data;
            }


            $referData = new stdClass();
            //list workflow reason
            import('plugin.amWorkflow.class.factory.AMRequestReasonFactory');
            $listWorkflowReason = (new AMRequestReasonFactory())->query([]);
            if (is_array($listWorkflowReason) == false)
                return AMRespone::dbFailed();
            $referData->listWorkflowReason = $listWorkflowReason;


            $res            = new AMRespone($listWorkfow);
            $res->referData = $referData;

            return $res;
        }

        public function GetListWorkflowField($params) {
            $workflowID = $params['ID'] + 0;
            if ($workflowID == 0)
                return AMRespone::paramsInvalid($params);

            import('plugin.amWorkflow.class.includes.WorkflowGet');
            $geter = new WorkflowGet();
            $temp  = $geter->GetWorkflowFieldEditData($workflowID);
            return $temp;
        }

        public function DeleteWorkflow($params) {
            $workflowID = $params['ID'] + 0;
            if ($workflowID == 0)
                return AMRespone::paramsInvalid($params);

            $temp = $this->_CheckWorkflowFullEditAble($workflowID);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;

            if ($temp->data == false)
                return AMRespone::busFailed($workflowID, "Delete workflow failed: Workflow in use");

            $fac    = new AMRequestDefinitionFactory();
            $reslut = $fac->delete(["ID" => $workflowID]);
            if ($reslut === false)
                return AMRespone::dbFailed($workflowID, "Delete workflow failed: Workflow in use");

            return new AMRespone();
        }

        /**
         * @param $params
         *      - data[object, required] WF data
         *      - listField[array, optional, null|array] list field of WF
         *      - listStep[array, optional, null|array] list step of WF
         * @return AMRespone
         *      - data
         *          + data WF data
         *          + listField list field of WF
         *          + listStep list step of WF
         */
        public function GetWorkflowEditData($params) {

            $id        = $params['ID'] + 0;
            $data      = null;
            $referData = new stdClass();
            if ($id != 0) {
                import('plugin.amWorkflow.class.includes.WorkflowGet');
                $geter = new WorkflowGet();
                $temp  = $geter->GetWorkflowData($id, true);
                if ($temp->code != AMResponeCode::$ok)
                    return $temp;
                $requestData = $temp->data;

                foreach ($requestData->listStepDefinition as $stepDefinition) {
                    $temp = $geter->GetFieldCan($stepDefinition->ID);

                    if ($temp->code != AMResponeCode::$ok)
                        return $temp;

                    $stepDefinition->fieldCan = $temp->data;
                }
            }
            else {
                $requestData = new stdClass();

                // get current user
                $currentUser = wp_get_current_user();
                if ($currentUser == 0 || $currentUser->user_login == false)
                    return AMRespone::busFailed(null, "not logined");
                $requestData->currentDomain = $currentUser->user_login;
            }

            //list role
            import('plugin.amRolePermission.class.factory.AMRoleManagementFactory');
            $temp = (new AMRoleManagementFactory())->query(array());
            if (is_array($temp) == false)
                return AMRespone::dbFailed();

            $referData->listRole = $temp;

            //list data type
            import('plugin.amWorkflow.class.factory.AMRequestDataTypeFactory');
            $temp = (new AMRequestDataTypeFactory())->query(array());
            if (is_array($temp) == false)
                return AMRespone::dbFailed();
            $referData->listDataType = $temp;

            //list workflow reasion
            import('plugin.amWorkflow.class.factory.AMRequestReasonFactory');
            $temp = (new AMRequestReasonFactory())->query([]);
            if (is_array($temp) == false)
                return AMRespone::dbFailed();
            $referData->listReason = $temp;

            // list workflow model group
            import('plugin.itportal.Factory.AMRequestDefinitionModelGroupFactory');
            $referData->workdlowModelGroups = (new AMRequestDefinitionModelGroupFactory())->query([]);

            $response            = new AMRespone($requestData);
            $response->referData = $referData;
            return $response;
        }

        /**
         * @param $params
         *      - data[object, required] WF data
         *      - listField[array, optional, null|array] list field of WF
         *      - listStep[array, optional, null|array] list step of WF
         * @return AMRespone
         *      - data
         *          + data WF data
         *          + listField list field of WF
         *          + listStep list step of WF
         */
        public function GetWorkflowData($params) {

            $id   = $params['ID'] + 0;
            $data = null;
            import('plugin.amWorkflow.class.includes.WorkflowGet');
            $geter = new WorkflowGet();
            $temp  = $geter->GetWorkflowData($id);
            return $temp;
        }

        public function GetWorkflow($params) {
            $wfID = $params["ID"];

            if(!$wfID || is_numeric($wfID) == false)
                return AMRespone::paramsInvalid();

            //data
            $workflow = (new AMRequestDefinitionFactory())->getByID($wfID);
            if ($workflow === false)
                return AMRespone::dbFailed($wfID, "Get Workflow by ID failed");

            return new AMRespone($workflow);
        }

        /**
         * @param $params
         *      - data[object, required] WF data
         *      - listField[array, optional, null|array] list field of WF
         *      - listStep[array, optional, null|array] list step of WF
         * @return AMRespone
         *      - data
         *          + data WF data
         *          + listField list field of WF
         *          + listStep list step of WF
         */
        public function InsertWorkflow($params) {
            return $this->_SaveWorkflow($params, true);
        }

        /**
         * @param $params
         *      - data[object, required] WF data
         *      - listField[array, optional, null|array] list field of WF
         *      - listStep[array, optional, null|array] list step of WF
         * @return AMRespone
         *      - data
         *          + data WF data
         *          + listField list field of WF
         *          + listStep list step of WF
         */
        public function UpdateWorkflow($params) {
            return $this->_SaveWorkflow($params, false);
        }

        function _SaveWorkflow(&$params, $isInsert) {
            import('plugin.amWorkflow.class.factory.AMRequestFieldDefinitionFactory');
            import('plugin.amWorkflow.class.factory.AMRequestStepDefinitionFactory');
            import('plugin.amWorkflow.class.factory.AMRequestStepActionFactory');
            import('plugin.amWorkflow.class.factory.AMRequestStepPermissionFactory');
            import('plugin.amWorkflow.class.factory.AMRequestSubFactory');

            $logDataUpdate = $isInsert ? '' : json_encode($params);

            try {

                $jsonData                = $params['workflow'];
                $jsonListFieldDefinition = $params['listFieldDefinition'];
                $jsonListStepDefinition  = $params['listStepDefinition'];


                if ($jsonData == null || $jsonListFieldDefinition == null || $jsonListStepDefinition == null)
                    return AMRespone::paramsInvalid();

                $workflow            = am_json_decode($jsonData, 1);
                $listFieldDefinition = am_json_decode($jsonListFieldDefinition, 1);
                $listStepDefinition  = am_json_decode($jsonListStepDefinition, 1);

                if ($workflow == null || is_array($listFieldDefinition) == null || is_array($listStepDefinition) == null)
                    return AMRespone::jsonFailed($params);

                $domain = wp_get_current_user()->user_login;
                if (!$domain)
                    return AMRespone::dbFailed($domain, 'login required');

                $data                      = new stdClass();
                $data->workflow            = $workflow;
                $data->listFieldDefinition = $listFieldDefinition;
                $data->listStepDefinition  = $listStepDefinition;
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }

            // save data
            global $wpdb;
            try {
                $wpdb->query('START TRANSACTION');

                //save workflow
                $facWorkflow  = new AMRequestDefinitionFactory();
                $wfSaveReslut = null;
                if ($isInsert) {
                    $workflow["Creator"] = $domain;
                    $wfSaveReslut        = $facWorkflow->insert($workflow);
                    if ($wfSaveReslut){
                        $workflow["ID"] = $wpdb->insert_id;
                        $data->workflow["ID"] = $wpdb->insert_id;
                    }
                }
                else {
                    $workflowID = $workflow["ID"];
                    if ($workflowID === null)
                        return AMRespone::busFailed($workflow, "Missing workflowID");

                    $wfSaveReslut = $facWorkflow->update($workflow, array("ID" => $workflow["ID"]));
                }
                if ($wfSaveReslut === false) {
                    $wpdb->query('ROLLBACK');
                    return AMRespone::dbFailed($workflow);
                }
                $wfID = $workflow["ID"];

                // save workflow detail
                $reInsert = $this->_InsertWorkflowDetail($wfID, $listFieldDefinition, $listStepDefinition, $isInsert == false);
                if ($reInsert->code != AMResponeCode::$ok) {
                    $wpdb->query('ROLLBACK');
                    return $reInsert;
                }

                // log update
                if($logDataUpdate){
                    $logDataUpdate = "Workflow update by $domain, data: $logDataUpdate";
                    error_log($logDataUpdate);
                }

                // finish
                $wpdb->query('COMMIT');

                return new AMRespone($data);
            } catch (Exception $ex) {
                // rollback by failed
                $wpdb->query('ROLLBACK');
                return AMRespone::exception($data, $ex);
            }
        }

        function _InsertWorkflowDetail($id, &$listFieldDefinition, &$listStepDefinition, $deleteOld) {
            import('plugin.amWorkflow.class.includes.RequestDataTypeID');
            import('plugin.amWorkflow.class.includes.WorkflowGet');

            global $wpdb;
            $facField = new AMRequestFieldDefinitionFactory();
            $facStep  = new AMRequestStepDefinitionFactory();

            if ($deleteOld) {
                $delRe = $this->_DeleteWorkflowDetail($id);
                if ($delRe->code != AMResponeCode::$ok)
                    return $delRe;
            }

            if (function_exists("insertListFieldDefinition") == false) {
                function insertListFieldDefinition(AMRequestFieldDefinitionFactory $fac, $wfID, $parentID, &$list, &$idTreeByName) {
                    global $wpdb;
                    $n = sizeof($list);

                    if ($n > 10)
                        return AMRespone::busFailed($n, "overload amount of field");

                    $mappingNameUsed = [];
                    $iMapping        = $parentID ? 4 : 8;
                    for ($i = 0; $i < $n; $i++) {
                        $item                        = $list[$i];
                        $item["RequestDefinitionID"] = $wfID;
                        $item["ParentID"]            = $parentID;
                        $item["Config"] = am_json_encode($item["cf"]);
                        $item["Constrain"] = am_json_encode($item["constrain"]);

                        if (isset($item["RequestDataTypeID"]) == false)
                            $item["RequestDataTypeID"] = 1;
                        if(WorkflowGet::isNoneDataType($item["RequestDataTypeID"]) == false){
                            $mappingName = null;
                            switch ($item["RequestDataTypeID"]) {
                                case RequestDataTypeID::$user:
                                    if (in_array('Field1', $mappingNameUsed) == false)
                                        $mappingName = 'Field1';
                                    else if (in_array('Field5', $mappingNameUsed) == false)
                                        $mappingName = 'Field5';
                                    break;
                                case RequestDataTypeID::$intendTime:
                                    if (in_array('Field2', $mappingNameUsed) == false)
                                        $mappingName = 'Field2';
                                    break;
                                case RequestDataTypeID::$note:
                                    if (in_array('Field3', $mappingNameUsed) == false)
                                        $mappingName = 'Field3';
                                    break;
                                case RequestDataTypeID::$table:
                                    if (in_array('Field4', $mappingNameUsed) == false)
                                        $mappingName = 'Field4';
                                    else if (in_array('Field7', $mappingNameUsed) == false)
                                        $mappingName = 'Field7';
                                    break;
                                case RequestDataTypeID::$domain:
                                    if (in_array('Field3', $mappingNameUsed) == false)
                                        $mappingName = 'Field3';
                                    else if (in_array('Field6', $mappingNameUsed) == false)
                                        $mappingName = 'Field6';
                                    break;


                                case RequestDataTypeID::$asset:
                                    if (in_array('Field0', $mappingNameUsed) == false)
                                        $mappingName = 'Field0';
                                    break;
                                case RequestDataTypeID::$assetType:
                                    if (in_array('Field1', $mappingNameUsed) == false)
                                        $mappingName = 'Field1';
                                    break;
                                case RequestDataTypeID::$assetModel:
                                    if (in_array('Field2', $mappingNameUsed) == false)
                                        $mappingName = 'Field2';
                                    break;
                                case RequestDataTypeID::$string:
                                    if (in_array('Field3', $mappingNameUsed) == false)
                                        $mappingName = 'Field3';
                                    break;
                            }
                            if ($mappingName == null) {
                                $maxFieldNum = $parentID ? 10 : 15;
                                for(; $iMapping < $maxFieldNum; $iMapping++){
                                    $temp = 'Field' . $iMapping;
                                    if(in_array($temp, $mappingNameUsed) == false){
                                        $mappingName = $temp;
                                        break;
                                    }
                                }
                            }

                            if($mappingName == null)
                                return AMRespone::busFailed(0, 'out of field number');

                            $item["MappingName"] = $mappingName;
                            $mappingNameUsed[] = $mappingName;
                        }

                        $listChild = $item["listChild"];
                        $fac->standardizeData($item);

                        if ($fac->insert($item) == false)
                            return AMRespone::dbFailed($item, "InsertListFieldDefinition");

                        $fieldDefinitionID           = $wpdb->insert_id;
                        $idTreeByName[$item["Name"]] = $fieldDefinitionID;
                        $item["ID"]                  = $fieldDefinitionID;
                        if ($item["RequestDataTypeID"] == RequestDataTypeID::$table) {
                            if (is_array($listChild)) {
                                $newTree                     = [];
                                $re                          = insertListFieldDefinition($fac, $wfID, $fieldDefinitionID, $listChild, $newTree);
                                $idTreeByName[$item["Name"]] = $newTree;
                                if ($re->code != AMResponeCode::$ok)
                                    return $re;
                            }
                            else {
                                return AMRespone::busFailed(null, "RequestDataType is table, require at least a column");
                            }
                        }
                    }
                    return new AMRespone();
                }

                function insertStepPermession($facStepPermission, $stepDefinitionID, $fieldCan) {
                    foreach ($fieldCan as $name => $can) {
                        $insertData = [
                            "StepDefinitionID" => $stepDefinitionID,
                            "FieldName" => $name,
                            "CanEmpty" => $can['empty'],
                            "CanEdit" => $can['edit'],
                            "CanView" => $can['view'],
                            "CanUpdate" => $can['update'],
                        ];
                        $result     = $facStepPermission->insert($insertData);
                        if ($result == false) {
                            logAM("Insert StepFieldPermission failed: $stepDefinitionID, $name");
                            return false;
                        }
                    }
                    return true;
                }
            }

            $idTreeByName = [];
            $re           = insertListFieldDefinition($facField, $id, null, $listFieldDefinition, $idTreeByName);
            if ($re->code != AMResponeCode::$ok)
                return $re;

            $facStepAction     = new AMRequestStepActionFactory();
            $facStepPermission = new AMRequestStepPermissionFactory();
            foreach ($listStepDefinition as $stepDefinition) {
                $stepDefinition["RequestDefinitionID"] = $id;

                if ($stepDefinition["StepIndex"] == 0)
                    unset($stepDefinition["Role"]);

                $listAction   = $stepDefinition["listAction"];
                $fieldCan = $stepDefinition["fieldCan"];

                $facStep->standardizeData($stepDefinition);
                if ($facStep->insert($stepDefinition) == false)
                    return AMRespone::dbFailed($stepDefinition, "InsertListStep");

                $stepID = $stepDefinition["ID"] = $wpdb->insert_id;
                $temp = insertStepPermession($facStepPermission, $stepID, $fieldCan);
                if ($temp == false)
                    return AMRespone::busFailed([$fieldCan, $idTreeByName], "Insert step permission failed");

                foreach ($listAction as $action) {
                    $action["RequestStepDefinitionID"] = $stepID;

                    if ($facStepAction->insert($action) === false)
                        return AMRespone::dbFailed($action, "Insert step action failed");
                }
            }

            return new AMRespone();
        }

        function _DeleteWorkflowDetail($id) {

            $facField = new AMRequestFieldDefinitionFactory();
            if ($facField->delete(array('RequestDefinitionID' => $id)) === false)
                return AMRespone::dbFailed($id, "Delete old field");

            $facStep    = new AMRequestStepDefinitionFactory();
            $listStep   = $facStep->query(["filter" => [["RequestDefinitionID = $id"]]]);
            $listStepID = [];
            foreach ($listStep as $step)
                $listStepID[] = $step->ID;

            if (sizeof($listStepID) > 0) {
                $temp              = join(",", $listStepID);
                $facStepAction     = new AMRequestStepActionFactory();
                $facStepPermission = new AMRequestStepPermissionFactory();
                $facSubWF          = new AMRequestSubFactory();
                $facStepAction->custom_delete([["RequestStepDefinitionID in (" . $temp . ")"]]);
                $facStepPermission->custom_delete([["StepDefinitionID in (" . $temp . ")"]]);
                $facSubWF->custom_delete([["StepDefinitionID in (" . $temp . ")"]]);
            }

            if ($facStep->delete(array('RequestDefinitionID' => $id)) === false)
                return AMRespone::dbFailed($id, "Delete old step");
            else
                return new AMRespone();
        }

        function _CheckWorkflowFullEditAble($workflowID) {
            import('plugin.amWorkflow.class.factory.AMRequestFactory');
            $listRequest = (new AMRequestFactory())->query(["filter" => [["DefinitionID = $workflowID"]]]);
            if ($listRequest === false)
                return AMRespone::busFailed($workflowID, "Get request by workflow id failed");

            $re = sizeof($listRequest) == 0;
            return new AMRespone($re);
        }

    }
}

return new AMWorkflowAjax();


















