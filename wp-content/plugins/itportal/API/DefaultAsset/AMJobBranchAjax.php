<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if(!class_exists('AMJobBranchAjax')){

    import('plugin.amDefaultAsset.class.factory.AMJobBranchFactory');

    class AMJobBranchAjax{
        public function getAll(){
            $branchFac = new AMJobBranchFactory();

            $result = $branchFac->query();
            return $result;
        }
    }
}
return new AMJobBranchAjax();