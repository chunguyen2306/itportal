<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 11:21 AM
 */
if(!class_exists('AMJobGroupAjax')){

    if ( ! function_exists( 'import' ) ) {
        require_once get_template_directory() . '/package/import.php';
    }

    import('plugin.amDefaultAsset.class.factory.AMJobGroupsFactory');
    import('plugin.amDefaultAsset.class.factory.AMJobBranchFactory');

    class AMJobGroupAjax{
        public function getByBranchID($_PARAM){
            $id = $_PARAM['id'];

            $jobGroupFac = new AMJobGroupsFactory();

            $result = $jobGroupFac->query(array(
               'join' => array(
                   array('JOIN', AMJobBranchFactory::$table_name.' b', 'a.BranchID', 'b.ID')
               ),
                'filter' => array(
                    array("b.ID = ".$id ,'')
                )
            ));

            return $result;
        }
    }
}
return new AMJobGroupAjax();