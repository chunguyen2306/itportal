<?php
/**
 * Created by PhpStorm.
 * User: lacha
 */
if(!class_exists('EditPage')){
    // can sua
    import('plugin.itportal.Factory.AMPageContentFactory');
    import('plugin.itportal.API.CMDB.Component');

    class EditPage {

        private $fac = null;

        function __construct()
        {
            $this->fac = new AMPageContentFactory();
        }

        function getValuePagecontent($name, $page){
            $result = $this->fac->getValuePageContent($name, $page);
            return $result;
        }

        function getPageContentRaw ($page) {
            $file = file_get_contents('wp-content/themes/IT Portal/pages/'.$page, true);
            // Lay attribute của pagecontent
            preg_match_all("/pagecontent=\"[a-z,A-Z,0-9,-]*\"/", $file,$match, PREG_PATTERN_ORDER);
            $result = null;
            $name_pagecontent = null;
            $value_pagecontent = null;
            foreach ($match[0] as $key => $value){
                $name_pagecontent[$key] = substr($match[0][$key], 13, strlen ($match[0][$key])-14);
                // Lay noi dung cua name_pagecontent tu DB
                $value_pagecontent[$key] = $this->getValuePagecontent($name_pagecontent[$key], $page);
            }
            if ($value_pagecontent === null) {
                return $result;
            }
            foreach ($value_pagecontent as $key => $value){
                if ($value_pagecontent[$key][0]->value) {
                    $temp = array(
                        'name' => $name_pagecontent[$key],
                        'value' => $value_pagecontent[$key][0]->value
                    );
                    $result[$key] = $temp;

                } else {
                    $temp = array(
                        'name' => $name_pagecontent[$key],
                        'value' => 'Chua co du lieu'
                    );
                    $result[$key] = $temp;
                }
            }
            return $result;
        }

        function convertToBinding ($data) {
            $result = null;
            foreach ($data as $key => $value) {
                $result[$data[$key].name] = $data[$key].value;
            }
            return $result;
        }

        public function GetPageContentForBinding($_PARAM){
            $page = $_PARAM['page'];
            $temp = $this->getPageContentRaw($page);
            $result = null;
            foreach ($temp as $key => $value) {
                $result[$value['name']] = $value['value'];
            }
            return $result;
        }

        public function GetPageContent($_PARAM){
            $page = $_PARAM['page'];
            $result = $this->getPageContentRaw($page);

            return $result;
        }

        public function isExistPageContent($_PARAM){
            $data = $_PARAM['data'];
            $page = $data['page'];
            $name = $data['name'];
            $result = false;
            $resultSelect = $this->fac->getOne([
                'filter' => [
                    ["name = '$name'", "AND"],
                    ["page = '$page'", ""]
                ]
            ]);
            if ($resultSelect != null)
                $result = true;
            return array(
                Result => $result
            );
        }

        public function GetAll(){
            $result = $this->fac->query();
            return $result;
        }

        public function GetByID($PARAM){
            $ID = $PARAM['ID'];
            $result = $this->fac->getByID($ID);
            return $result;
        }

        public function GetByName($PARAM){
            $menuName = $PARAM['menuName'];
            $result = $this->fac->getOne([
                'filter' => [
                    ["Name = '$menuName'", ""]
                ]
            ]);
            return $result;
        }

        public function GetChildByName($PARAM){
            $menuName = $PARAM['menuName'];
            $result = $this->fac->query([
                'select' => 'a.*',
                'join' => [
                    [ 'JOIN' , AMMainMenuFactory::$table_name.' b', 'a.Parent', 'b.ID' ]
                ],
                'filter' => [
                    [ "b.name = '$menuName'", "AND" ],
                    [ "a.ID <> a.Parent", ""]
                ],
                'order'=> 'Name'
            ]);
            return $result;
        }

        public function AddNew($PARAM){
            global $wpdb;
            $data = $PARAM['data'];
            $result = $this->fac->insert($data);
            $id = $wpdb->insert_id;
            $data = $this->GetByID(array('ID'=>$id));
            if($result === 1){
                $result = true;
            } else {
                $result = false;
            }
            return array(
                State => $result,
                Record => $data
            );
        }

        public function Update($PARAM){
            $data = $PARAM['data'];
            $result = $this->fac->update($data, array(
                'name' => $data['name'],
                'page' => $data['page']
            ));

            if($result === 1){
                $result = true;
            } else {
                $result = false;
            }
            return array(
                State => $result,
                Record => $data
            );
        }

        public function Delete($PARAM){
            $data = $PARAM['data'];
            $result = $this->fac->delete(array(
                'ID' => $data['ID']
            ));
            if($result === 1){
                $result = true;
            } else {
                $result = false;
            }
            return array(
                State => $result,
                Record => $data
            );
        }
    }
}
return new EditPage();