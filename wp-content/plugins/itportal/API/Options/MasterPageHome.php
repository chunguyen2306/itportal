<?php
/**
 * Created by PhpStorm.
 * User: lacha
 * Date: 20/4/2018
 * Time: 9:30 AM
 */
if(!class_exists('MasterPageHome')) {

    import('plugin.itportal.Factory.AMPageContentFactory');
    import('plugin.itportal.API.CMDB.Component');

    class MasterPageHome
    {
        private $fac = null;

        function __construct()
        {
            $this->fac = new AMPageContentFactory();
        }

        public function GetAll(){
            $result = $this->fac->query();
            return $result;
        }

        public function getContent(){
            $result = $this->fac->query([
                'filter' => [
                    ["page = 'master-page-home'"]
                ]
            ]);
            return $result;
        }

        public function GetByID($PARAM){
            $ID = $PARAM['ID'];
            $result = $this->fac->getByID($ID);
            return $result;
        }

    }
}

return new MasterPageHome();