<?php
/**
 * Created by PhpStorm.
 * User: LAP10883-local
 * Date: 10/31/2018
 * Time: 4:02 PM
 */

class WhiteListUpload
{
    function __construct()
    {
    }

    public function GetAll()
    {
        $result = using('theme.package.Includes.User.WhiteListUpload');
        return $result;
    }

    public function GetByName($param)
    {
        $types = $param['type'];
        $result = using("theme.package.Includes.$types");
        return $result;
    }
    public function GetImageType(){
        $result = using('theme.package.Includes.User.WhiteListUpload');
        $res = [];
        foreach($result as $type){
            if($type['Image']){
                array_push(  $res,$type);
            }
        }
        return $res;
    }

    public function Update($param){
        try {
            $type = $param['data'];
            $types = isset($param['type'])?$param['type']:'User.WhiteListUpload';
            //var_dump($menu);
            $file = getFullPath("theme.package.Includes.$types", 'php', 'path');

            $type = am_json_encode($type);
            $type = preg_replace('/{/', '[', $type);
            $type = preg_replace('/}/', ']', $type);
            $type = preg_replace('/\\\/', '', $type);
            $type = preg_replace('/\:/', '=>', $type);

            $result = file_put_contents($file, '<?php return ' . $type . ';');
            return $result>0;
        } catch (Exception $e) {
            return false;
        }
    }
}
return new WhiteListUpload();