<?php
/**
 * Created by PhpStorm.
 * User: LAP10883-local
 * Date: 10/31/2018
 * Time: 11:47 AM
 */

class MenuNotes
{
    function __construct()
{
}

    public function GetAll()
    {
        $result = using('theme.package.Includes.User.MenuNote');
        return $result;
    }

    public function GetByName($param)
    {
        $menuName = $param['menuName'];
        $result = using("theme.package.Includes.$menuName");
        return $result;
    }

    public function Update($param){
        try {
            $menu = $param['data'];
            $menuName = isset($param['menuName'])?$param['menuName']:'User.MenuNote';
            //var_dump($menu);
            $file = getFullPath("theme.package.Includes.$menuName", 'php', 'path');

            $menu = am_json_encode($menu);
            $menu = preg_replace('/{/', '[', $menu);
            $menu = preg_replace('/}/', ']', $menu);
            $menu = preg_replace('/\\\/', '', $menu);
            $menu = preg_replace('/\:/', '=>', $menu);

            $result = file_put_contents($file, '<?php return ' . $menu . ';');
            return $result>0;
        } catch (Exception $e) {
            return false;
        }
    }
}
return new MenuNotes();