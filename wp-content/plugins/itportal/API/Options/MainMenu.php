<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if(!class_exists('MainMenu')){

    import('plugin.itportal.Factory.AMMainMenuFactory');
    import('plugin.itportal.API.CMDB.Component');
    import('plugin.itportal.API.Asset.APIAssetModelDetail');

    class MainMenu {

        private $fac = null;

        function __construct()
        {
            $this->fac = new AMMainMenuFactory();
        }

        public function GetAll(){
            $result = $this->fac->query();
            return $result;
        }

        public function GetByID($PARAM){
            $ID = $PARAM['ID'];
            $result = $this->fac->getByID($ID);
            return $result;
        }

        public function GetByName($PARAM){
            $menuName = $PARAM['menuName'];
            $result = $this->fac->getOne([
               'filter' => [
                   ["Name = '$menuName'", ""]
               ]
            ]);
            return $result;
        }

        public function GetChildByName($PARAM){
            $menuName = $PARAM['menuName'];
            $result = $this->fac->query([
                'select' => 'a.*',
                'join' => [
                    [ 'JOIN' , AMMainMenuFactory::$table_name.' b', 'a.Parent', 'b.ID' ]
                ],
                'filter' => [
                    [ "b.name = '$menuName'", "AND" ],
                    [ "a.ID <> a.Parent", ""]
                ],
                'order'=> 'Name'
            ]);
            return $result;
        }

        public function AddNew($PARAM){
            global $wpdb;
            $data = $PARAM['data'];
            $result = $this->fac->insert($data);
            $id = $wpdb->insert_id;
            $data = $this->GetByID(array('ID'=>$id));
            if($result === 1){
                $result = true;
            } else {
                $result = false;
            }
            return array(
                State => $result,
                Record => $data
            );
        }
        public function Update($PARAM){
            $data = $PARAM['data'];
            $result = $this->fac->update($data, array(
                'ID' => $data['ID']
            ));

            if($result === 1){
                $result = true;
            } else {
                $result = false;
            }
            return array(
                State => $result,
                Record => $data
            );
        }
        public function Delete($PARAM){
            $data = $PARAM['data'];
            $result = $this->fac->delete(array(
                'ID' => $data['ID']
            ));
            if($result === 1){
                $result = true;
            } else {
                $result = false;
            }
            return array(
                State => $result,
                Record => $data
            );
        }
        public function UpdateMapping($_PARAM){
            $ID = $_PARAM['ID'];
            $mapping = $_PARAM['mapping'];
            $data = $this->GetByID(array('ID'=>$ID));
            $data = json_decode(json_encode($data), True);
            $data['Mapping'] = $mapping;//json_encode($mapping);


            return $this->Update(array(data=>$data));
        }

        public function GetAsSubMenu($_PARAM){
            $name = $_PARAM['parentName'];
            $tableName = $this->fac->tableInfo()['table_name'];
            $menu = $this->fac->query(array(
                'select' => 'a.*',
                'join' => array(
                    array('join', $tableName.' b', 'a.Parent', 'b.ID')
                ),
                'filter' => array(
                    array( "b.Name = '".$name."'",'AND' ),
                    array( "a.ID != a.Parent", '')
                )
            ));

            return $menu;
        }

        public function GetAsMenu($_PARAM){
            $menu = $this->fac->query(array(
                'oder' => 'a.ID',
                'filter' => array(
                    array( 'a.ID = a.Parent','' )
                )
            ));

            foreach ($menu as $item){
                $item->Childs = $this->fac->query(array(
                    'filter' => array(
                        array( 'Parent = '.$item->ID,' AND' ),
                        array( 'ID != Parent', '')
                    )
                ));
            }

            return $menu;
        }

        public function GetProducts($_PARAM){
            $parentName = $_PARAM['parentName'];
            $subName = $_PARAM['subName'];
            if(isset($_PARAM['filter'])) {
                $filter = $_PARAM['filter'];
            } else {
                $filter = [];
            }
            $mappingName = '';
            $pageIndex = $_PARAM['pageIndex'];
            $limit = $_PARAM['limit'];

            if($subName === ''){
                $mappingName = $parentName;
            } else{
                $mappingName = $subName;
            }

            $mappingList = $this->fac->getOne(array(
                'filter' => array(
                    array( sprintf("Name = '%s'", $mappingName),'' )
                )
            ));

            $apiComponent = new Component();
            $apiAssetModelDetail = new APIAssetModelDetail();
            /*$result = $apiComponent->GetByMappingList(array(
                'mapping' => $mappingList->Mapping,
                'filter' => json_encode($filter),
                'pageIndex' => $pageIndex,
                'limit' => $limit
            ));*/

            $result = $apiAssetModelDetail->GetByMappingList(array(
                'mapping' => $mappingList->Mapping,
                'filter' => json_encode($filter),
                'pageIndex' => $pageIndex,
                'limit' => $limit
            ));

            return $result;
        }
    }
}
return new MainMenu();