<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if(!class_exists('PersonalMenu')){

    class PersonalMenu
    {

        function __construct()
        {
        }

        public function GetAll()
        {
            $result = using('theme.package.Includes.User.MainMenu');
            return $result;
        }

        public function GetByName($param)
        {
            $menuName = $param['menuName'];
            $result = using("theme.package.Includes.$menuName");
            return $result;
        }

        public function Update($param){
            try {
                $menu = $param['data'];
                $menuName = isset($param['menuName'])?$param['menuName']:'User.MainMenu';
                $file = getFullPath("theme.package.Includes.$menuName", 'php', 'path');

                $menu = am_json_encode($menu);
                $menu = preg_replace('/{/', '[', $menu);
                $menu = preg_replace('/}/', ']', $menu);
                $menu = preg_replace('/\\\/', '', $menu);
                $menu = preg_replace('/\:/', '=>', $menu);

                $result = file_put_contents($file, '<?php return ' . $menu . ';');
                return $result>0;
            } catch (Exception $e) {
                return false;
            }
        }
    }
}
return new PersonalMenu();