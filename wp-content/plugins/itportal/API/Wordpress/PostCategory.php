<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if (!class_exists('PostCategory')) {

    import('plugin.itportal.Factory.AMWPPostFactory');

    class PostCategory
    {

        private $fac;

        function __construct()
        {
            $this->fac = new AMWPPostFactory();
        }

        function GetAll()
        {
            return get_categories(array(
                'hide_empty' => 0
            ));
        }

        function FilterPaging($param)
        {
            $keyword = $param['keyword'];
            $pageIndex = $param['pageIndex'];
            $limit = $param['limit'];
            $result = $this->fac->query([
                'select' => "distinct a.*, d.name 'CateName'",
                'join' => [
                    [ "JOIN" , "wp_term_relationships b" , "a.ID", "b.object_id"],
                    [ "JOIN" , "wp_term_taxonomy c" , "b.term_taxonomy_id", "c.term_taxonomy_id"],
                    [ "JOIN" , "wp_terms d" , "c.term_id", "d.term_id"],
                    [ "JOIN" , "wp_term_relationships ta" , "a.ID", "ta.object_id"],
                    [ "JOIN" , "wp_term_taxonomy tb" , "ta.term_taxonomy_id", "tb.term_taxonomy_id"],
                    [ "JOIN" , "wp_terms tc" , "tc.term_id", "tb.term_id"],
                ],
                'filter' => [
                    ["(c.taxonomy = 'category'", "AND"],
                    ["tb.taxonomy = 'post_tag'", "AND"],
                    ["a.post_type='post'", "AND"],
                    ["a.post_status='publish'", ") AND"],
                    ["(a.post_title like '%$keyword%'", "OR"],
                    ["d.name like '%$keyword%'", "OR"],
                    ["tc.name like '%$keyword%'", "OR"],
                    ["a.post_content like '%$keyword%'", ")"]
                ]
            ]);
            $total = sizeof($result);
            $pageCount = ceil($total / $limit);
            $result = array_slice($result, $pageIndex * $limit, $limit);
            return (object)[
                Result => $result,
                Total => $total,
                PageCount => $pageCount,
            ];
        }
    }
}
return new PostCategory();