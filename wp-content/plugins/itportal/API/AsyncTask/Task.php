<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */

if (!class_exists('Task')) {
    import('theme.package.HttpRequest');

    class Task {

        public function DoTask($_PARAM) {

            ob_start();

            // Get the size of the output.
            $size = ob_get_length();

            // Disable compression (in case content length is compressed).
            header("Content-Encoding: none");

            // Set the content length of the response.
            header("Content-Length: {$size}");

            // Close the connection.
            header("Connection: close");

            // Flush all output.
            ob_end_flush();
            ob_flush();
            flush();

            // Close current session (if it exists).
            if (session_id())
                session_write_close();

            $_PARAM = stripslashes_deep($_PARAM);
            $action = $_PARAM['action'];
            $args   = $_PARAM['data'];

            if (isset($_PARAM['action']) && $action !== '') {
                $result = call_user_func_array(array( $this, $action), $args);
                error_log("Async Task: Do [$action] [$args[0]] => " . json_encode($result) . " <= END");
            }
        }

        function mail($to, $subject, $message, $headers = '', $attachments = []) {
            $result = wp_mail($to, $subject, $message, $headers = '', $attachments);
            if (!$result)
                AMRespone::busFailed([$to, $subject, $message, $headers], 'Send mail failed');

            return $result;
        }

        function sms($phoneNumer, $message, $serviceName = 'IT Portal', $method = 'SendMessage') {
            $service  = using('plugin.itportal.config.ServiceUrl')['SMS'];
            $data = [
                'url' => $service,
                'method' => 'POST',
                'data' => [
                    'ToMobileNumber' => $phoneNumer,
                    'm' => $method,
                    'RequestedService' => $serviceName,
                    'Message' => $message,
                ]
            ];

            $request  = new HttpRequest($data);
            $response = $request->send();

            $responseData   = json_decode($response->reponseText);
            $result = $responseData != null && $responseData->IsSuccess != false;
            if (!$result)
                AMRespone::busFailed([$response->reponseText, $data], 'Send SMS failed ');

            return $result;
        }

        function push_data($domains, $message) {
            error_log('COM push_data is disabled');
            return true;
            

            $service  = using('plugin.itportal.config.ServiceUrl')['COM'];
            $service = $service . '/API/chat.ashx';
            $data = [
                'url' => $service,
                'method' => 'POST',
                'data' => [
                    'domains' => json_encode($domains),
                    'message' => $message
                ]
            ];

            $request  = new HttpRequest($data);
            $response = $request->send();
            $result = $response->reponseText == 'success';
            if (!$result){
                AMRespone::busFailed([$data, $response->reponseText], 'Communication chat failed');
            }

            return $result;
        }

        function api() {

        }
    }
}
return new Task();