<?php
import('theme.package.HttpRequest');
import('theme.package.HttpResponse');

if(!class_exists('APICMDBCommon')) {

    class APICMDBCommon
    {
        function request($method, $_PARAM){
            $serviceUrl = using('plugin.itportal.config.ServiceUrl');
            $request = new HttpRequest(array(
                url=>$serviceUrl['CMDB'],
                method=>'POST',
                data=>$_PARAM,
                header=>array(
                    "U-RequestType:CMDBBusiness",
                    "U-RequestAction:".$method
                )
            ));
            $response = $request->send();
            return $response->reponseText;
        }

        function GetResourceStateHistoryByResourceName($_PARAM){
            return $this->request('GetResourceStateHistoryByResourceName', $_PARAM);
        }

        function GetAllResourceStateHistoryByResourceName($_PARAM){
            return $this->request('GetAllResourceStateHistoryByResourceName', $_PARAM);
        }
    }
}
return new APICMDBCommon();