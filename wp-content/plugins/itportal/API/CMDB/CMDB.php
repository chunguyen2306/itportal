<?php
import('theme.package.HttpRequest');
import('theme.package.HttpResponse');

if (!class_exists('CMDB')) {

    class CMDB {

        function CallCMDB($_PARAM) {
            $service = $_PARAM["u"];
            $pData   = $_PARAM["p"];
            $vData   = $_PARAM["v"];

            $postData = [];
            if (isset($pData)) {

                if (($p = json_decode(stripslashes($pData), true)) == false) {
                    echo "JSON decode failed: p = ";
                    exit ($pData);
                }

                if (($v = json_decode(stripslashes($vData), true)) == false) {
                    echo "JSON decode failed: u = ";
                    exit ($vData);
                }
            }

            return $this->get($service, $p, $v);

        }

        public function get($service, $p, $v){
            $n = sizeof($p);
            if ($n == 0)
                exit ("Missing required parameter (no parameter)");

            for ($i = 0; $i < $n; $i++) {
                $postData[$p[$i]] = $v[$i];
            }
            $serviceUrl = using('plugin.itportal.config.ServiceUrl');
            $request    = new HttpRequest([
                url => $serviceUrl[$service],
                method => 'POST',
                data => $postData]);
            $response   = $request->send();
            return $response->reponseText;
        }

    }
}
return new CMDB();