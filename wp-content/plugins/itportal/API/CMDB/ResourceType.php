<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
import('theme.package.HttpRequest');
import('theme.package.HttpResponse');

if(!class_exists('ResourceType')){

    class ResourceType{
        function request($method, $_PARAM){
            $serviceUrl = using('plugin.itportal.config.ServiceUrl');
            $request = new HttpRequest(array(
                url=>$serviceUrl['CMDB'],
                method=>'POST',
                data=>$_PARAM,
                header=>array(
                    "U-RequestType:ResourceTypes",
                    "U-RequestAction:".$method
                )
            ));
            $response = $request->send();
            return $response->reponseText;
        }

        function GetAll($_PARAM){
            return $this->request('GetAll', $_PARAM);
        }

        function CreateNew($_PARAM){
            return $this->request('CreateNew', $_PARAM);
        }

        function Update($_PARAM){
            return $this->request('Update', $_PARAM);
        }

        function GetAsMenu($_PARAM){
            return $this->request('GetAsMenu', $_PARAM);
        }

        function GetAsSubMenu($_PARAM){
            return $this->request('GetAsSubMenu', $_PARAM);
        }

        function GetAllWithComponentType($_PARAM){
            return $this->request('GetAllWithComponentType', $_PARAM);
        }
    }
}
return new ResourceType();