<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
import('theme.package.HttpRequest');
import('theme.package.HttpResponse');

if(!class_exists('ComponentType')){

    class ComponentType{
        function request($method, $_PARAM){
            $serviceUrl = using('plugin.itportal.config.ServiceUrl');
            $request = new HttpRequest(array(
                url=>$serviceUrl['CMDB'],
                method=>'POST',
                data=>$_PARAM,
                header=>array(
                    "U-RequestType:ComponentTypes",
                    "U-RequestAction:".$method
                )
            ));
            $response = $request->send();
            return $response->reponseText;
        }

        function Paging($_PARAM){
            return $this->request('Paging', $_PARAM);
        }

        function CreateNew($_PARAM){
            $_PARAM['newData'] = json_encode($_PARAM['newData']);
            return $this->request('CreateNew', $_PARAM);
        }

        function Update($_PARAM){
            $_PARAM['newData'] = json_encode($_PARAM['newData']);
            return $this->request('Update', $_PARAM);
        }

        function Delete($_PARAM){
            return $this->request('Delete', $_PARAM);
        }

        function GetAllWithComponent($_PARAM){
            return $this->request('GetAllWithComponent', $_PARAM);
        }

        function GetByIDs($_PARAM){
            return $this->request('GetByIDs', $_PARAM);
        }
    }
}
return new ComponentType();