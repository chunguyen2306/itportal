<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
import('theme.package.HttpRequest');
import('theme.package.HttpResponse');

if(!class_exists('Component')){

    class Component{
        function request($method, $_PARAM){
            $serviceUrl = using('plugin.itportal.config.ServiceUrl');
            $request = new HttpRequest(array(
                url=>$serviceUrl['CMDB'],
                method=>'POST',
                data=>$_PARAM,
                header=>array(
                    "U-RequestType:Components",
                    "U-RequestAction:".$method
                )
            ));
            $response = $request->send();
            return $response->reponseText;
        }

        function Paging($_PARAM){
            return $this->request('Paging', $_PARAM);
        }

        function CreateNew($_PARAM){
            $_PARAM['newData'] = json_encode($_PARAM['newData']);
            return $this->request('CreateNew', $_PARAM);
        }

        function Update($_PARAM){
            $_PARAM['newData']['COMMENTS'] = json_encode($_PARAM['newData']['COMMENTS']);
            $_PARAM['newData'] = json_encode($_PARAM['newData']);
            return $this->request('Update', $_PARAM);
        }
        function Delete($_PARAM){
            return $this->request('Delete', $_PARAM);
        }
        function GetByTypeAndName($_PARAM){
            return $this->request('GetByTypeAndName', $_PARAM);
        }
        function GetByID($_PARAM){
            return $this->request('GetByID', $_PARAM);
        }
        function GetByIDsAndFilter($_PARAM){
            return $this->request('GetByIDsAndFilter', $_PARAM);
        }
        function  GetByMappingList($_PARAM){
            return $this->request('GetByMappingList', $_PARAM);
        }
        function GetQuantityInHand($_PARAM){
            return $this->request('GetQuantityInHand', $_PARAM);
        }
        function GetByMostUse($_PARAM){
            return $this->request('GetByMostUse', $_PARAM);
        }
        function GetProductSlideByType($_PARAM){
            return $this->request('GetProductSlideByType', $_PARAM);
        }
        function GetManufactureByComponentType($_PARAM){
            return $this->request('GetManufactureByComponentType', $_PARAM);
        }
        function GetMostUserByType($_PARAM){
            return $this->request('GetMostUserByType', $_PARAM);
        }
        function SearchPaging($_PARAM){
            return $this->request('SearchPaging', $_PARAM);
        }
    }
}
return new Component();