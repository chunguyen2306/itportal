<?php

import('theme.package.HttpRequest');
import('theme.package.HttpResponse');

if(!class_exists('Resources')){

    class Resources{
        function request($method, $_PARAM){
            $serviceUrl = using('plugin.itportal.config.ServiceUrl');
            $request = new HttpRequest(array(
                url=>$serviceUrl['CMDB'],
                method=>'POST',
                data=>$_PARAM,
                header=>array(
                    "U-RequestType:Resources",
                    "U-RequestAction:".$method
                )
            ));
            $response = $request->send();
            return $response->reponseText;
        }

        /**
         * @security resource.asset.view|owner:username
         * @param $_PARAM
         * @return bool|string
         */
        function GetUserAsset($_PARAM){
            return $this->request('GetUserAsset', $_PARAM);
        }

        function GetAssetByName($_PARAM){
            return $this->request('GetAssetByName', $_PARAM);
        }
    }
}
return new Resources();