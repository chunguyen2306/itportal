<?php
if (!function_exists('import')) {
    require_once get_template_directory() . '/package/import.php';
}

import('plugin.itportal.Factory.AMUserRoleFactory');
import('plugin.itportal.Factory.AMUserFactory');

if (!class_exists('AMUserAjax')) {
    class AMUserAjax {

        private $fac = null;

        function __construct() {
            $this->fac = new AMUserFactory();
        }

        public function GetUserInformation($params) {
            $keyword = $params['keyword'];
            $re      = null;
            if ($keyword) {
                $keyword = preg_replace('/\s+/', ' ', $keyword);
                $words   = explode(' ', $keyword, 2);
                if (sizeof($words) > 1) {
                    $filter   = [["firstName = '$words[0]'", "AND"]];
                    $filter[] = ["lastName like '$words[1]%'", ""];
                }
                else {
                    $filter = [
                        ["domainAccount like '$words[0]%'"]                       
                    ];
                }

                $start = $params['start'];

                $limit = $params['limit'];
                if ($limit > 10)
                    $limit = 10;

                $re = $this->fac->query([
                    'filter' => $filter,
                    'limit' => ['at' => $start, 'length' => $limit]
                ]);
                if ($re === false)
                    return AMRespone::dbFailed($params, 'get user info failed');
            }
            else {
                $re = [];
            }
            return new AMRespone($re);
        }

        public function SearchUser($params) {
            try {
                return 1;
                $domain = $params["domain"];

                if ($domain == null) {
                    $re = [];
                    return new AMRespone($re);
                }


                //Get current user
                $currentUser = wp_get_current_user();
                if ($currentUser == 0 || $currentUser->user_login == false)
                    return AMRespone::notFound();

                $username = $currentUser->user_login;
                $result   = (new AMUserRoleFactory())->query(["filter" => [["Domain = '$username'", ""]]]);
                if ($result == false)
                    return AMRespone::dbFailed($username, "Get user role info");

                if (is_array($result) != 1)
                    return AMRespone::busFailed($username, "Data is weirdo");

                return new AMRespone($result[0]);
            } catch (Exception $ex) {
                return AMRespone::exception(null, $ex);
            }
        }

        public function GetUserByDomain($_PARAM) {

            if (isset($_PARAM['domainAccount'])) {
                $domainAccount = $_PARAM['domainAccount'];
                return $this->fac->getOne(['filter' => [["domainAccount = '$domainAccount'", ""]]]);

            }
            else {
                return null;
            }
        }

    }
}
return new AMUserAjax();

