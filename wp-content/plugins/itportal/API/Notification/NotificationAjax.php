<?php

if (!class_exists('NotificationAjax')) {
    import('plugin.itportal.Factory.AMRequestNotificationFactory');

    class NotificationAjax
    {
        function GetListForPreview($params)
        {
            try {
                $user = wp_get_current_user();
                if (!$user->ID) {
                    $re = [];
                    return new AMRespone($re);
                }

                $domain = $user->user_login;
                $fac = new AMRequestNotificationFactory();
                $list = $fac->getListForPreview($domain);

                if (is_array($list) == false)
                    return AMRespone::dbFailed($list, "get list notification for prevew failed");
                else
                    return new AMRespone($list);
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }
        }

        function DeleteNotification($params)
        {
            try {
                $ids = $params['ids'];

                if ($ids == null)
                    return AMRespone::paramsInvalid();

                $domain = wp_get_current_user()->user_login;
                if (!$domain)
                    return new AMRespone();

                $fac = new AMRequestNotificationFactory();

                if ($fac->deleteNotification($domain, $ids) == false)
                    return AMRespone::dbFailed(null, "delete as seen failed");
                else
                    return new AMRespone();
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }
        }

        function CountUnseen($params)
        {
            try {
                $user = wp_get_current_user();
                $re = 0;
                if ($user && $user->user_login) {
                    $domain = $user->user_login;
                    $fac = new AMRequestNotificationFactory();
                    $re = $fac->countUnseen($domain);

                    if ($re === false)
                        return AMRespone::dbFailed($re, "count unseen notification failed");
                }

                return new AMRespone($re);
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }
        }

        function GetList($params)
        {
            try {
                $start = $params['start'];
                $count = $params['limit'];
                $isRead = $params['isRead'];

                if ($start && is_numeric($count) == false)
                    return AMRespone::paramsInvalid();

                $domain = wp_get_current_user()->user_login;
                if (!$domain) {
                    $re = [];
                    return new AMRespone($re);
                }

                $fac = new AMRequestNotificationFactory();

                $list = $fac->getList($domain, $isRead, $start, $count);

                if (is_array($list) == false)
                    return AMRespone::dbFailed($list, "get list notification failed");
                else
                    return new AMRespone($list);
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }
        }

        function GetQuantity($params)
        {
            try {
                $domain = wp_get_current_user()->user_login;
                $isRead = $params['isRead'];
                if (!$domain) {
                    return null;
                }

                $fac = new AMRequestNotificationFactory();
                $quantity = $fac->getQuantity($domain, $isRead);

                return new AMRespone($quantity);
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }
        }

        function MarkAsSeen($params)
        {
            try {
                $ids = $params['ids'];

                if ($ids == null)
                    return AMRespone::paramsInvalid();

                $domain = wp_get_current_user()->user_login;
                if (!$domain)
                    return new AMRespone();

                $fac = new AMRequestNotificationFactory();

                if ($fac->maskAsSeen($domain, $ids) == false)
                    return AMRespone::dbFailed(null, "mark as seen failed");
                else
                    return new AMRespone();
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }
        }

        function MarkAsRead($params)
        {
            try {
                $ids = $params['ids'];
                $isRead = $params['isRead'];

                if ($ids == null || isset($params['isRead']) == false)
                    return AMRespone::paramsInvalid();

                $domain = wp_get_current_user()->user_login;
                if (!$domain)
                    return new AMRespone();

                $fac = new AMRequestNotificationFactory();
                if ($fac->maskAsRead($domain, $ids, $isRead ? 1 : 0) == false)
                    return AMRespone::dbFailed(null, "mark as read failed");
                else
                    return new AMRespone();
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }
        }

        function ListenNew($params)
        {
            try {
                $signalrID = $params['signalrID'];
                if ($signalrID == null)
                    return AMRespone::paramsInvalid();

                $domain = wp_get_current_user()->user_login;
                if (!$domain)
                    return new AMRespone();

                import('plugin.itportal.Business.Notification.Notification');
                $bus = new Notification();
                return $bus->mapping($domain, $signalrID);
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }
        }
    }
}

return new NotificationAjax();


















