<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if (!class_exists('APIRoleAndPermission')) {
    import('plugin.itportal.Factory.AMRoleManagementFactory');
    import('plugin.itportal.Factory.AMUserRoleMappingFactory');
    import('plugin.amUser.class.includes.RoleID');

    class APIRoleAndPermission
    {

        private $roleFac = null;
        private $userRoleFac = null;

        function __construct()
        {
            $this->roleFac = new AMRoleManagementFactory();
            $this->userRoleFac = new AMUserRoleMappingFactory();
        }

        public function GetAllRole($PARAM)
        {
            $pageIndex = isset($PARAM['pageindex']) ? $PARAM['pageindex'] : -1;
            $limit = isset($PARAM['limit']) ? $PARAM['limit'] : 0;
            $keyword = isset($PARAM['keyword']) ? $PARAM['keyword'] : '';

            $agrs = [];

            if ($keyword !== '') {
                $agrs['filter'] = array(
                    array("Name like '%" . $keyword . "%'", "")
                );
            }

            if ($limit !== 0) {
                $agrs['limit'] = array(
                    "at" => $pageIndex * $limit,
                    "length" => $limit
                );
            }

            return $this->roleFac->query($agrs);
        }

        function checkUserPermission($PARAM){
            $security = new AMSecurity();
            $featureName = $PARAM['feature'];
            return array('Result'=>$security->checkBySecurityDoc($featureName, []));
        }

        public function GetUserRoleByDomain($PARAM)
        {
            $domain = $PARAM['domain'];

            $permission = $this->userRoleFac->getOne([
                'filter' => [
                    ["Domain = '$domain'", ""]
                ]
            ]);
            if (sizeof($permission) != 0) {
                $permisIDs = substr($permission->Permission, 1, strlen($permission->Permission) - 2);
                $permissionNames = $this->roleFac->query([
                    'select' => "Name",
                    'filter' => [
                        ["ID in ($permisIDs)", ""]
                    ]
                ], false, ARRAY_A);
                $permission->PermissionName = array_column($permissionNames, 'Name');
            }

            return $permission;
        }

        public function GetDeptHeadByDeptName($PARAM){
            $deptName = $PARAM['deptName'];
            $deptHeadRoleID = RoleID::$deptHead;

            $permission = $this->userRoleFac->getOne([
                'filter' => [
                    ["Department='$deptName'", "AND"],
                    ["Permission like '%\"$deptHeadRoleID\"%'", ""],
                ]
            ]);

            // dev_upgrade no depthead
            if(!$permission){
                $permission = new stdClass();
                $permission->Domain = 'tult';
            }

            return $permission;
        }

        public function GetUserByRole($PARAM){
            $roleName = isset($PARAM['roleName'])?$PARAM['roleName']:'';
            $roleID = isset($PARAM['roleID'])?$PARAM['roleID']: '';

            $where = [];
            if(isset($PARAM['roleName'])){
                array_push($where, [ "b.Name = '$roleName'", "" ]);
            }else if(isset($PARAM['roleID'])){
                array_push($where, [ "b.ID = $roleID", "" ]);
            }

            $result = $this->userRoleFac->getOne([
                'select'=>'DISTINCT a.ID, a.Domain, a.Permission as PermissionName',
                'filter'=>$where,
                'join'=>[
                    [ "LEFT JOIN", "am_roles b", "JSON_CONTAINS(a.Permission, concat('\"',b.ID,'\"'), '$')", "" , "" ]
                ]
            ]);

            return $result;
        }

        public function GetAllUserRole($PARAM)
        {
            $pageIndex = isset($PARAM['pageindex']) ? $PARAM['pageindex'] : -1;
            $limit = isset($PARAM['limit']) ? $PARAM['limit'] : 0;
            $keyword = isset($PARAM['keyword']) ? $PARAM['keyword'] : '';

            $agrs = [];

            if ($keyword !== '') {
                $agrs['filter'] = array(
                    array("Domain like '%" . $keyword . "%'", "")
                );
            }

            $result = $this->userRoleFac->query($agrs);
            $total = count($result);
            if (($pageIndex + 1) * $limit < $total) {
                $result = array_slice($result, $pageIndex * $limit, $limit);
            } else {
                $result = array_slice($result, $pageIndex * $limit);
            }

            /*if ($limit !== 0) {
                $agrs['limit'] = array(
                    "at" => $pageIndex * $limit,
                    "length" => $limit
                );
            }*/
            foreach ($result as $item) {
                $permisIDs = substr($item->Permission, 1, strlen($item->Permission) - 2);
                $permissionNames = $this->roleFac->query([
                    'select' => "Name",
                    'filter' => [
                        ["ID in ($permisIDs)", ""]
                    ]
                ], false, ARRAY_N);
                $item->PermissionName = $permissionNames;
            }
            return array(
                "Total" => $total,
                "Result" => $result
            );
        }

        public function GetDomainsByRole($param){
            $roleID = $param['RoleID'];

            $result = $this->userRoleFac->query([
                'filter' => [
                    ["Permission like '%$roleID%'"]
                ]
            ]);

            if(!$result)
                return [];

            $re = [];
            foreach ($result as $item){
                $re[] = $item->Domain;
            }

            return $re;
        }

        public function CreateRole($PARAM)
        {
            $data = isset($PARAM['data']) ? $PARAM['data'] : null;

            $result = $this->roleFac->insert($data);

            return array(
                "State" => $result == 1 ? true : false,
                "Result" => $data
            );
        }

        public function UpdateRole($PARAM)
        {
            $data = isset($PARAM['data']) ? $PARAM['data'] : null;

            $result = $this->roleFac->update($data, array(
                'ID' => $data['ID']
            ), true);

            return array(
                "State" => $result == 1 ? true : false,
                "Result" => $data
            );
        }

        public function DeleteRole($PARAM)
        {
            $id = isset($PARAM['id']) ? $PARAM['id'] : null;

            $result = $this->roleFac->delete(array(
                'ID' => $id
            ));

            return array(
                "State" => $result == 1 ? true : false
            );
        }

        public function CreateUserRole($PARAM)
        {
            $data = isset($PARAM['data']) ? $PARAM['data'] : null;
            $data['Permission'] = json_encode($data['Permission']);

            $result = $this->userRoleFac->insert($data);

            return array(
                "State" => $result == 1 ? true : false,
                "Result" => $data
            );
        }

        /**
         * @security admin.permissions.grant
         * @param $PARAM
         * @return array
         */
        public function UpdateUserRole($PARAM)
        {
            $data = isset($PARAM['data']) ? $PARAM['data'] : null;
            $data['Permission'] = json_encode($data['Permission']);
            $result = $this->userRoleFac->update($data, array(
                'ID' => $data['ID']
            ), true);

            return array(
                "State" => $result == 1 ? true : false,
                "Result" => $data
            );
        }

        public function DeleteUserRole($PARAM)
        {
            $id = isset($PARAM['id']) ? $PARAM['id'] : null;

            $result = $this->userRoleFac->delete(array(
                'ID' => $id
            ));

            return array(
                "State" => $result == 1 ? true : false
            );
        }
    }
}
return new APIRoleAndPermission();