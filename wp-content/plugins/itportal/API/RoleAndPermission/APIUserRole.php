<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if (!class_exists('APIUserRole')) {

    import('plugin.itportal.Factory.AMRoleManagementFactory');
    import('plugin.itportal.Factory.AMUserRoleFactory');

    class APIUserRole
    {

        private $roleFac = null;
        private $userRoleFac = null;

        function __construct()
        {
            $this->roleFac = new AMRoleManagementFactory();
            $this->userRoleFac = new AMUserRoleFactory();
        }

        public function GetAllRole($PARAM)
        {
            $pageIndex = isset($PARAM['pageindex']) ? $PARAM['pageindex'] : -1;
            $limit = isset($PARAM['limit']) ? $PARAM['limit'] : 0;
            $keyword = isset($PARAM['keyword']) ? $PARAM['keyword'] : '';

            $agrs = [];

            if ($keyword !== '') {
                $agrs['filter'] = array(
                    array("Domain like '%" . $keyword . "%'", "OR"),
                    array("Name like '%" . $keyword . "%'", "")
                );
            }

            if ($limit !== 0) {
                $agrs['limit'] = array(
                    "at" => $pageIndex * $limit,
                    "length" => $limit
                );
            }

            return $this->roleFac->query($agrs);
        }

        public function GetUserRoleByDomain($PARAM)
        {
            $domain = $PARAM['domain'];

            $permission = $this->userRoleFac->getOne([
                'filter' => [
                    ["Domain='$domain'", ""]
                ]
            ]);
            if (isset($permission)) {
                $permissionNames = $this->roleFac->query([
                    'select' => "Name",
                    'filter' => [
                        ["ID in ($permission->RoleID)", ""]
                    ]
                ], false, ARRAY_A);
                $permission->RoleName = array_column($permissionNames, 'Name');
            }

            return $permission;
        }

        public function GetAllUserRole($PARAM)
        {
            $pageIndex = isset($PARAM['pageindex']) ? $PARAM['pageindex'] : -1;
            $limit = isset($PARAM['limit']) ? $PARAM['limit'] : 0;
            $keyword = isset($PARAM['keyword']) ? $PARAM['keyword'] : '';

            $agrs = [];


            if ($keyword !== '') {
                $agrs['filter'] = array(
                    array("Domain like '%" . $keyword . "%'", "OR"),
                    array("Name like '%" . $keyword . "%'", "")
                );
            }

            if ($limit !== 0) {
                $agrs['limit'] = array(
                    "at" => $pageIndex * $limit,
                    "length" => $limit
                );
            }

            $result = $this->userRoleFac->query($agrs);
            foreach ($result as $item) {
                $RoleName = $this->roleFac->getOne([
                    'select' => "Name",
                    'filter' => [
                        ["ID in ($item->RoleID)", ""]
                    ]
                ]);
                $item->RoleName = $RoleName->Name;
            }
            return $result;
        }

        public function CreateRole($PARAM)
        {
            $data = isset($PARAM['data']) ? $PARAM['data'] : null;

            $result = $this->roleFac->insert($data);

            return array(
                "State" => $result == 1 ? true : false,
                "Result" => $data
            );
        }

        public function UpdateRole($PARAM)
        {
            $data = isset($PARAM['data']) ? $PARAM['data'] : null;

            $result = $this->roleFac->update($data, array(
                'ID' => $data['ID']
            ), true);

            return array(
                "State" => $result == 1 ? true : false,
                "Result" => $data
            );
        }

        public function DeleteRole($PARAM)
        {
            $id = isset($PARAM['id']) ? $PARAM['id'] : null;

            $result = $this->roleFac->delete(array(
                'ID' => $id
            ));

            return array(
                "State" => $result == 1 ? true : false
            );
        }

        public function CreateUserRole($PARAM)
        {
            $data = isset($PARAM['data']) ? $PARAM['data'] : null;
            unset($data['RoleName']);
            $result = $this->userRoleFac->insert($data);

            return array(
                "State" => $result == 1 ? true : false,
                "Result" => $data
            );
        }

        public function UpdateUserRole($PARAM)
        {
            $data = isset($PARAM['data']) ? $PARAM['data'] : null;
            unset($data['RoleName']);
            $result = $this->userRoleFac->update($data, array(
                'ID' => $data['ID']
            ), true);

            return array(
                "State" => $result == 1 ? true : false,
                "Result" => $data
            );
        }

        public function DeleteUserRole($PARAM)
        {
            $id = isset($PARAM['id']) ? $PARAM['id'] : null;

            $result = $this->userRoleFac->delete(array(
                'ID' => $id
            ));

            return array(
                "State" => $result == 1 ? true : false
            );
        }
    }
}
return new APIUserRole();