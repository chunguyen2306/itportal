<?php
/**
 * Created by PhpStorm.
 * User: lacha
 * Date: 19/4/2018
 * Time: 2:32 PM
 */
if (!class_exists('LoadPageContentAjax')) {
    import('plugin.itportal.Factory.AMRequestNotificationFactory');

    class LoadPageContentAjax {

        function GetAllPageContent($params){
            try {
                $user = wp_get_current_user();

                $domain = $user->user_login;
                $fac = new AMPageContentFactory();
                $list = $fac->getAllPageContent($domain);

                if(is_array($list) == false)
                    return AMRespone::dbFailed($list, "get list notification for prevew failed");
                else
                    return new AMRespone($list);
            } catch (Exception $ex) {
                return AMRespone::exception($params, $ex);
            }
        }

    }
}