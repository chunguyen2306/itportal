Chan<?php
/**
 * Action Document
 * Name: Asset Action
 * Description: test test
 */
if (!class_exists('AssetAction')) {
    class AssetAction
    {
        /**
         * Name: DoTest
         * Description: Test
         */
        function DoTest()
        {
            return 'DoTest';
        }

        /**
         * Name: DoTest2
         * Description: Test2
         * @param $p1 (displayName: Param1, type: TextArea)
         * @param $p2
         * @return string
         */
        function DoTest2($p1, $p2)
        {
            return 'DoTest ' . $p1 . ' ' . $p2;
        }

        /**
         * Name: GetAssetType
         * Description: Get asset type from CMDB
         * @param $keyword
         * @param $startindex
         * @param $limit
         * @return string
         */
        function GetAssetType($keyword = '', $startindex, $limit)
        {
            return 'GetAssetType';
        }

        /**
         * Name: GetAssetModel
         * Description: Get asset model from CMDB
         * @param $keyword
         * @param $type
         * @param $startindex
         * @param $limit
         * @return string
         */
        function GetAssetModel($keyword = '', $type = '', $startindex, $limit)
        {
            return 'GetAssetModel';
        }

        /**
         * Name: GetAsset
         * Description: Get asset list
         * @param $keyword
         * @param $model
         * @param $startindex
         * @param $limit
         * @return string
         */
        function GetAsset($keyword = '', $model = '', $startindex, $limit)
        {
            return 'GetAsset';
        }
    }
}
return new AssetAction();
