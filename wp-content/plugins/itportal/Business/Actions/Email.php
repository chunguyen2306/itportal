<?php
/**
 * Action Document
 * Name: Email Action
 */
if (!class_exists('NotificationAction')) {
    class NotificationAction
    {
        /**
         * Name: ManualSend
         * Description: Gửi mail thủ công
         * @param $subject
         * @param $body
         * @param $mail_to
         * @param $mail_cc
         */
        function ManualSend($subject, $body, $mail_to, $mail_cc)
        {

        }

        /**
         * Name: ServiceSend
         * Description: Gửi mail thông qua service mail
         * @param $template_name
         * @param $data
         */
        function ServiceSend($template_name, $data)
        {

        }
    }
}
return new NotificationAction();