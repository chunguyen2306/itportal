<?php
/**
 * Created by PhpStorm.
 * User: tienseuit
 * Date: 26-Mar-18
 * Time: 10:13
 */

class WFWhereBuilder {
    protected $map = [];
    protected $wfW;
    public function __construct(SQLWhereBuilder $whereBuilder) {
        $this->wfW = $whereBuilder;
    }

    public function add($wfID, $type, $col, $operator, $value) {
        $this->getWhere($wfID)->add($type, $col, $operator, $value);
    }

    public function getWhere($wfID){
        $re = $this->map[$wfID];
        if($re == null){
            $re = $this->wfW->addSub("OR");
            $this->map[$wfID] = $re;
        }

        return $re;
    }
}
