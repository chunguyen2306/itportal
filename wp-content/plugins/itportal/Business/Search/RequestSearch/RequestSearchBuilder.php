<?php

include_once 'FieldBuilderUtil.php';
include_once 'SQLParamaterBuilder.php';
include_once 'SQLQueryBuilder.php';
include_once 'SQLWhereBuilder.php';
include_once 'StepBuilderUtil.php';
include_once 'WFWhereBuilder.php';

class RequestSearchBuilder {
    public $hasStep  = false;

    /**
     *
     * pha 1: giới hạn record, chỉ lọc dựa trên các thông tin cơ bản, và bước duyệt hiện tại của request
     * pha 2: lấy các thông tin về step, field
     * pha 3: lấy thông tin cho các fiel table
     * pha 3: lấy thông tin từ CMDB
     *
     * @param $rsID
     * @param $conditions
     * @return AMRespone
     */
    public function build($rsID, $conditions) {
        import('plugin.amWorkflow.class.includes.RequestDataTypeID');
        ini_set("xdebug.var_display_max_children", -1);
        ini_set("xdebug.var_display_max_data", -1);
        ini_set("xdebug.var_display_max_depth", -1);

        // get request search columns
        $temp = $this->loadSearchColumns($rsID, $conditions);
        if ($temp->code !== AMResponeCode::$ok)
            return $temp;
        $columns = $temp->data;

        // preparse build SQL
        $b1        = new SQLQueryBuilder(); // build query step 1
        $b1->from("am_requests AS r");
        $hasPhase2 = false;

        // build phase1 query
        foreach ($columns as $col) {
            switch ($col->TypeID) {
                case RequestSearchTypeID::$baseValue:
                    //var_dump($columns);
                    $b1->select('r.' . $col->src, $col->Name);

                    if ($col->filters)
                        $b1->where->addFilters("AND", "r.$col->src", $col->filters);

                    break;

                case RequestSearchTypeID::$curStepValue:
                    if ($this->hasStep == false) {
                        $this->hasStep = true;
                        $b1->join("INNER JOIN", "am_request_definition", "AS rd ON rd.ID = r.DefinitionID");
                        $b1->join("LEFT JOIN", "am_request_step_definitions", "AS sd ON (sd.RequestDefinitionID = rd.ID AND sd.StepIndex = r.CurrentIndex)");
                        $b1->join("LEFT JOIN", "am_request_steps", "AS s ON (s.RequestID = r.ID AND s.DefinitionID = sd.ID)");
                    }
                    $b1->select("s.$col->src as $col->Name");
                    if ($col->filters)
                        $b1->where->addFilters("AND", "s.$col->src", $col->filters);
                    break;

                case RequestSearchTypeID::$stepValue:
                case RequestSearchTypeID::$fieldValue:
                     $hasPhase2 = true;
                    break;
            }
        }

        // dev_test
        $wpdb = new SQLLog();
        $b1->querySetParams();

        $requestQuery = $b1->toString();

        // to resutl
        if ($hasPhase2)
            $requests = $this->build_phase2($requestQuery);
        else
            $requests = $wpdb->get_results($requestQuery);

        //   SQLLog::save();

        return new AMRespone($requests);
    }

    function build_phase2($requestQuery, $columns){
        $wpdb = new SQLLog();

        $tempTable = "temp_request_search_phase1";
        $wpdb->query("DROP TABLE IF EXISTS $tempTable");
        $wpdb->query("CREATE TEMPORARY TABLE IF NOT EXISTS $tempTable AS ($requestQuery)");

        $rootB = new SQLQueryBuilder();
        $fieldBU = new FieldBuilderUtil($rootB->where->addSub());
        $stepBU= new StepBuilderUtil($rootB);

        $rootB->from($tempTable, 'r');
        foreach ($columns as $col) {
            switch ($col->TypeID) {
                case RequestSearchTypeID::$stepValue:
                    $fieldBU->build($col);
                    break;

                case RequestSearchTypeID::$fieldValue:
                    $stepBU->build($col);
                    break;
            }
        }

        $requestQuery = $rootB->toString();
        if($fieldBU->hasChildData())
            return $this->build_phase3($requestQuery, $fieldBU);
        else
            return $wpdb->get_results($requestQuery);
    }

    function build_phase3($requestQuery, FieldBuilderUtil $fieldBuilderUtil){
        $wpdb = new SQLLog();

        $tempTable = "temp_request_search_phase2";
        $wpdb->query("DROP TABLE IF EXISTS $tempTable");
        $wpdb->query("CREATE TEMPORARY TABLE IF NOT EXISTS $tempTable AS ($requestQuery)");

        $requests = $wpdb->get_results("SELECT * FROM $tempTable");
        $fieldBuilderUtil->queryChildData($requests);

        return $requests;
    }

    function loadSearchColumns($rsID, $conditions) {
        $facCol = new AMRequestSearchColumnFactory();
        $re     = $facCol->query(["filter" => [["SearchID = $rsID"]]]);
        if ($re === false)
            return AMRespone::dbFailed("getRequestSearchColumn failed", $rsID);

        // set filter
        $filterMap      = $this->conditionToFilterMap($conditions);
        $colRequestTime = null;
        foreach ($re as $col) {
            $filters = $filterMap[$col->Name];
            if ($filters)
                $col->filters = $filters;

            if ($col->TypeID === RequestSearchTypeID::$baseValue && $col->src === 'CreateDate')
                $colRequestTime = $col;
        }

        $colRequestTimeOpera = '>';
        foreach ([$filterMap['_start'], $filterMap['_end']] as $filters) {
            if ($filters) {
                if ($colRequestTime === null) {
                    $colRequestTime         = new stdClass();
                    $colRequestTime->Name   = 'CreateDate';
                    $colRequestTime->src    = 'CreateDate';
                    $colRequestTime->TypeID = RequestSearchTypeID::$baseValue;
                    $re[]                   = $colRequestTime;
                }
                foreach ($filters as $item) {
                    $item->ope                 = $colRequestTimeOpera;
                    $colRequestTime->filters[] = $item;
                }
            }

            $colRequestTimeOpera = '<';
        }

        return new AMRespone($re);
    }

    function conditionValueToFilter($conditionValue) {
        if (is_array($conditionValue)) {
            $re = [];
            foreach ($conditionValue as $item) {
                $result = $this->conditionValueToFilter($item);;
                if ($result)
                    $re[] = $result;
            }
            return $re;
        }
        else {
            $conVal    = trim($conditionValue);
            $lenConVal = strlen($conVal);

            // prev check
            if ($lenConVal < 1)
                return null;

            // parse operator
            $operator = $conVal[0];
            if ($operator === "<" || $operator === ">") {
                if ($lenConVal < 2)
                    return null;

                if ($conVal[1] === "=") {
                    if ($lenConVal < 3)
                        return null;
                    $conVal   = substr($conVal, 2);
                    $operator = $operator . "=";
                }
                else {
                    $conVal = substr($conVal, 1);
                }
            }
            else if (strpos($conVal, "*") !== false) {
                $operator = "LIKE";
                $conVal   = str_replace("*", "%", $conVal);
            }
            else {
                $operator = "=";
            }

            // done
            $filter      = new stdClass();
            $filter->ope = $operator;
            $filter->val = $conVal;

            return $filter;
        }
    }

    function conditionToFilterMap($conditions) {
        $re = [];
        foreach ($conditions as $name => $conVal) {

            // get value
            $name = trim($name);
            if ($name === '')
                continue;

            $filter = $this->conditionValueToFilter($conVal);

            if ($filter) {
                if (is_array($filter))
                    $re[$name] = $filter;
                else
                    $re[$name][] = $filter;
            }
        }

        return $re;
    }
}
