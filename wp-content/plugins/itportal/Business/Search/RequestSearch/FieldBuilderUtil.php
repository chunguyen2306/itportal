<?php
/**
 * Created by PhpStorm.
 * User: tienseuit
 * Date: 26-Mar-18
 * Time: 10:12
 */

class FieldBuilderUtil {
    protected     $r; // am_request as r
    protected     $f; // am_request_field as r
    protected     $wfB;
    protected     $rootB;
    public static $wfMap;

    private $tableFields;
    private $cmdbFields;

    public function __construct(SQLQueryBuilder $rootBuilder, $requestAs = "r", $requestFieldAs = "f") {
        $this->wfWB = new WFWhereBuilder($rootBuilder->where->addSub('AND'));
        $this->rootB = $rootBuilder;
        $this->r       = $requestAs;
        $this->f       = $requestFieldAs;

        $this->begin();
        if (FieldBuilderUtil::$wfMap)
            return $this;

        $fac     = new AMRequestSearchFactory();
        $columns = $fac->custom_query2("SELECT `Name`, MappingName, RequestDataTypeID, RequestDefinitionID, ParentID  FROM am_request_field_definition");
        $map     = [];
        $childs  = [];
        foreach ($columns as $col) {
            if ($col->ParrentID)
                $childs[] = $col;
            else
                $map[$col->RequestDefinitionID][$col->Name] = $col;
        }

        foreach ($childs as $child) {
            foreach ($map as $parrent) {
                if ($child->ParrentID == $parrent->ID) {
                    $parrent->listChild[] = $child;
                    break;
                }
            }
        }

        FieldBuilderUtil::$wfMap = $map;
    }

    public function build($searchColumn) {
        if (sizeof($searchColumn->src) == 0)
            return;

        if ($this->__isFirst == null) {
            $this->__isFirst = true;
            $this->rootB->join("INNER JOIN", "am_request_fields", "AS f ON f.RequestID = r.ID");
            $this->rootB->select("r.DefinitionID");
        }

        $firstWFConfig = $searchColumn->src[0];
        $isDirectData  = $this->isDirectData($this->getWFColumn($firstWFConfig));
        $cases         = [];
        foreach ($searchColumn->src as $cf) {
            $wfID  = $cf->wfID;
            $wfCol = $this->getWFColumn($cf);

            $mappingName = $this->f . '.' . $this->getMappingName($cf);
            $cases[]     = "WHEN $wfID THEN $mappingName";

            if ($searchColumn->filters && $isDirectData) {
                $where = $this->wfWB->getWhere($wfID);
                $where->addFilters("AND", $mappingName, $searchColumn->filters);
            }
        }

        $caseContent = 'CASE ' . join(' ', $cases) . ' END';
        if ($searchColumn->src[0]->child) {
            $field = $this->tableFields[$caseContent];
            if ($field == null) {
                $field                           = new stdClass();
                $field->name                     = 'table' . $searchColumn->Name;
                $this->tableFields[$caseContent] = $field;
                $this->rootB->select($caseContent, $field->name);
            }

            $field->columns[] = $searchColumn;
        }
        else {
            $this->rootB->select($caseContent, $searchColumn->Name);
            if ($isDirectData == false)
                $this->addCMDBField($searchColumn, $wfCol);
        }
    }

    public function begin() {
        $this->tableFields = [];
        $this->cmdbFields  = [];
    }

    public function end() {

    }

    public function queryChildData(&$requests, $tempSqlTableName) {
        if (sizeof($requests) == 0)
            return;


        foreach ($this->tableFields as $field)
            $this->queryChildDataForField($requests, $field, $tempSqlTableName);

        if (sizeof($this->cmdbFields) > 0)
            $this->loadCMDBData($requests);
    }

    public function getMappingName($wfConfig) {
        return FieldBuilderUtil::$wfMap[$wfConfig->wfID][$wfConfig->name]->MappingName;
    }

    public function getWFColumn($wfConfig) {
        return FieldBuilderUtil::$wfMap[$wfConfig->wfID][$wfConfig->name];
    }

    public function getWFColumnByChild($wfConfig) {
        return FieldBuilderUtil::$wfMap[$wfConfig->wfID][$wfConfig->child];
    }

    public function hasChildData() {
        return sizeof($this->tableFields) > 0 || sizeof($this->cmdbFields) > 0;
    }

    function loadCMDBData(&$requests) {
        $requestCount = sizeof($requests);

        $cover = [];
        $mapID = [];
        foreach ($this->cmdbFields as $cmdbField) {
            $idGroups   = [];
            $columnName = $cmdbField->name;

            for ($i = 0; $i < $requestCount; $i++) {
                $request = $requests[$i];
                $colVal  = $request->{$columnName};
                if (!$colVal)
                    continue;

                $idGroup = new stdClass();
                if (is_array($colVal)) {
                    $idGroup->id          = $i;
                    $idGroup->ids         = [];
                    $cmdbField->idIsGroup = true;
                    foreach ($colVal as $id) {
                        if (in_array($id, $idGroup->ids) === false)
                            $idGroup->ids[] = $id;
                    }
                }
                else {
                    foreach ($idGroups as $item) {
                        if ($item->id === $colVal) {
                            $colVal = null;
                            break;
                        }
                    }
                    if ($colVal)
                        $idGroup->id = $colVal;
                }
                $idGroups[] = $idGroup;
            }

            $cover[]                 = $cmdbField;
            $mapID[$cmdbField->name] = $idGroups;
        }

        import("plugin.itportal.API.CMDB.CMDB");
        $cmdb           = new CMDB();
        $mapReferInJson = $cmdb->get("CMDBEX", ["m", "covers", "mapId"], ["Search", am_json_encode($cover), am_json_encode($mapID)]);
        $mapRefer       = am_json_decode($mapReferInJson);

        foreach ($this->cmdbFields as $cmdbField) {
            $columnName  = $cmdbField->name;
            $mapReferVal = $mapRefer->{$columnName};
            $groupIds    = $mapReferVal->groupIds;
            $mapModel    = $mapReferVal->map;
            $hasGroupIDs = is_array($groupIds);
            $noFilter    = $cmdbField->filters ? false : true;
            $re          = [];
            for ($i = 0; $i < $requestCount; $i++) {
                if ($hasGroupIDs && in_array($i, $groupIds) === false)
                    continue;

                $request = $requests[$i];
                $colVal  = $request->{$columnName};
                if (!$colVal) {
                    if ($noFilter)
                        $re[] = $request;
                }

                if (is_array($colVal)) {
                    $colData = [];
                    foreach ($colVal as $id)
                        $colData[] = $mapModel->{$id}->name;
                }
                else {
                    $colData = $mapModel->{$colVal}->name;
                }

                $request->{$columnName} = $colData;
                $re[]                   = $request;
            }
            $requests = $re;
        }
    }

    function queryChildDataForField(&$requests, $field, $tempSqlTableName) {
        $b = new SQLQueryBuilder();
        $b->select("td.TableID");
        $b->from($tempSqlTableName, 'r');
        $b->join("INNER JOIN", 'am_request_table_detail_data', "as td ON r.$field->name = td.TableID");

        $bFilter         = null;
        $tableFilterName = 'temp_request_search_filter';

        foreach ($field->columns as $searchColumn) {
            if (sizeof($searchColumn->src) == 0)
                continue;

            $firstWFColmn = $this->getWFColumnByChild($searchColumn->src[0]);
            $isDirectData = $this->isDirectData($firstWFColmn);
            $wfWhere      = null;
            if ($isDirectData) {
                if ($searchColumn->filters) {
                    if ($bFilter == null) {
                        $bFilter = new SQLQueryBuilder();
                        $bFilter->select("DISTINCT td.TableID");
                        $bFilter->from($tempSqlTableName, 'r');
                        $bFilter->join("INNER JOIN", 'am_request_table_detail_data', "as td ON r.$field->name = td.TableID");

                        $b->join('INNER JOIN', $tableFilterName, "as tdf ON td.TableID = tdf.TableID");
                    }
                    $wfWhere = $bFilter->where->addSub('AND');
                }
            } else {
                $this->addCMDBField($searchColumn, $firstWFColmn);
            }

            $cases = [];
            foreach ($searchColumn->src as $wfCf) {
                $wfID                = $wfCf->wfID;
                $wfColumnMappingName = $this->getWFColumnByChild($wfCf)->MappingName;
                $cases[]             = "WHEN $wfID THEN td.$wfColumnMappingName";

                if ($wfWhere) {
                    $where = $wfWhere->addSub('OR');
                    $where->add('', 'DefinitionID', '=', $wfID);
                    $where->addFilters("AND", $wfColumnMappingName, $searchColumn->filters);
                }
            }

            $b->select("CASE " . join(" ", $cases) . " END", $searchColumn->Name);
        }


        $wpdb = new SQLLog();// dev_test
        if ($bFilter) {
            foreach ($bFilter->where->paramBuilder->toStrings() as $item)
                $wpdb->query($item);
            $wpdb->query("DROP TABLE IF EXISTS $tableFilterName");
            $wpdb->query("CREATE TEMPORARY TABLE IF NOT EXISTS $tableFilterName AS " . $bFilter->toString());
        }
        $childDatas = $wpdb->get_results($b->toString());

        $fieldName = $field->name;
        $rLength   = sizeof($requests);
        $irEnd     = 0;
        $ir        = 0;
        $request   = $requests[$ir];
        foreach ($childDatas as $childData) {
            while ($request->{$fieldName} != $childData->TableID) {
                $ir++;
                if ($ir >= $rLength)
                    $ir = 0;

                $request = $requests[$ir];
                if ($ir == $irEnd)
                    continue 2;
            }
            $irEnd = $ir;

            foreach ($field->columns as $searchColumn) {
                $val                              = $childData->{$searchColumn->Name};
                $request->{$searchColumn->Name}[] = $val;
            }
            $request->_is_ok = 1;
        }

        $re = [];
        foreach ($requests as $rq) {
            if ($rq->_is_ok != 1)
                continue;

            $re[] = $rq;
            unset($rq->{$fieldName});
            unset($rq->_is_ok);
        }
        $requests = $re;
    }

    function addCMDBField($searchColumn, $wfCol) {

        $item       = new stdClass();
        $item->name = $searchColumn->Name;
        if ($searchColumn->filters)
            $item->filters = $searchColumn->filters;

        switch ($wfCol->RequestDataTypeID) {
            case RequestDataTypeID::$assetModel;
                $item->method       = "searchModelNameByID";
                $this->cmdbFields[] = $item;
                break;
        }
    }

    function isDirectData($wfColumn) {
        switch ($wfColumn->RequestDataTypeID) {
            case RequestDataTypeID::$assetModel:
            case RequestDataTypeID::$table:
            case RequestDataTypeID::$assetType:
            case RequestDataTypeID::$user:
                return false;

            default:
                return true;
        }
    }

    function getTableName($caseContent, $searchColumn) {
        foreach ($this->tableFields as $tableName => $data) {
            if ($data->caseContent === $caseContent)
                return $data->tableName;
        }

        $data               = new stdClass();
        $data->caseContent  = $caseContent;
        $data->searchColumn = $searchColumn;
        $data->tableName    = $searchColumn->Name;

        $this->tableFields[] = $data;
        return $data->tableName;
    }

    function createCheckFunction($filter) {

        //var_dump("removing");
        die;

        $conVal = $filter->val;

        switch ($filter->ope) {
            case "=":
                return function ($val) use ($conVal) {
                    return $val == $conVal;
                };
                break;
            case ">":
                return function ($val) use ($conVal) {
                    return $val > $conVal;
                };
                break;
            case "<":
                return function ($val) use ($conVal) {
                    return $val < $conVal;
                };
                break;
            case ">=":
                return function ($val) use ($conVal) {
                    return $val >= $conVal;
                };
                break;
            case "<=":
                return function ($val) use ($conVal) {
                    return $val <= $conVal;
                };
                break;
            case "LIKE":
                $conVals = explode("%", $conVal);
                return function ($val) use ($conVals) {
                    $iStart = 0;
                    foreach ($conVals as $conValItem) {
                        if ($conValItem == "")
                            continue;

                        $iPos = strpos($val, $conValItem, $iStart);

                        if ($iPos === false)
                            return false;

                        $iStart = $iPos;
                    }
                    return true;
                };
                break;
        }
    }

}
