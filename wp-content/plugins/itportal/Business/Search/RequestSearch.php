<?php

import("plugin.itportal.Factory.AMRequestSearchFactory");
import("plugin.itportal.Factory.AMRequestSearchColumnFactory");
import("plugin.itportal.Factory.AMRequestSearchFilterFactory");


class RequestSearch {

    public function getRequestSearchData($id) {
        $fac    = new AMRequestSearchFactory();
        $facCol = new AMRequestSearchColumnFactory();
        $facFil = new AMRequestSearchFilterFactory();
        $re     = new stdClass();

        // get request search
        $temp = $fac->getByID($id);
        if ($temp == false)
            return AMRespone::dbFailed("getRequestSearch failed", $id);
        $re->data = $temp;

        // create where
        $where = ["filter" => [["SearchID = $id"]]];

        // get request search columns
        $temp = $facCol->query($where);
        if ($temp === false)
            return AMRespone::dbFailed("getRequestSearchColumn failed", $id);
        $re->columns = $temp;

        // get request search coditions
        $temp = $facFil->query($where);
        if ($temp === false)
            return AMRespone::dbFailed("getRequestSearchFilters failed", $id);
        $re->filters = $temp;

        return new AMRespone($re);
    }

    public function getListRequestSearch() {
        $fac    = new AMRequestSearchFactory();
        $result = $fac->query([]);

        if ($result === false)
            return AMRespone::dbFailed();
        else
            return new AMRespone($result);
    }

    public function saveRequestSearchData($data, $columns, $conditions, $isUpdate = false) {
        $rID = null;
        $re  = null;
        global $wpdb;

        try {
            $wpdb->query('START TRANSACTION');
            // insert request search
            $fac = new AMRequestSearchFactory();

            if ($isUpdate) {
                if ($fac->update($data, ["ID" => $rID]) === false)
                    return AMRespone::busFailed("Update RequestSearch failed");
                $rID = $data["ID"];
            }
            else {
                if ($fac->insert($data) === false)
                    return AMRespone::busFailed("Insert RequestSearch failed");
                $rID = $data["ID"] = $wpdb->insert_id;
            }

            $res = $this->saveDetail($rID, $columns, $conditions, $isUpdate);
            if ($res->code != AMResponeCode::$ok)
                return $res;

            $res->data->data = (object)$data;
            $re              = $res;
        } catch (Exception $ex) {
            $re = AMRespone::exception([$data, $columns, $conditions], $ex);
        } finally {
            if ($re && $re->code == AMResponeCode::$ok)
                $wpdb->query('COMMIT');
            else
                $wpdb->query('ROLLBACK');

            return $re;
        }
    }

    public function deleteRequestSearch($id) {
        $fac = new AMRequestSearchFactory();
        if ($fac->delete(['ID' => $id]) === false)
            return AMRespone::dbFailed($id, 'deleteRequestSearch');

        $facCol = new AMRequestSearchColumnFactory();
        if ($facCol->delete(['SearchID' => $id]) === false)
            return AMRespone::dbFailed($id, 'deleteRequestSearchColumn');

        $facFil = new AMRequestSearchFilterFactory();
        if ($facFil->delete(['SearchID' => $id]) === false)
            return AMRespone::dbFailed($id, 'deleteRequestSearchFilter');

        return new AMRespone();
    }

    public function search($rsID, $conditions) {
        include_once 'RequestSearch/RequestSearchBuilder.php';
        $builder = new RequestSearchBuilder();
        return $builder->build($rsID, $conditions);
    }

    function searchAdvance($conditions) {
        $b = new SQLQueryBuilder();

    }

    function saveDetail($rsID, $columns, $conditions, $isUpdate = false) {
        $facCol = new AMRequestSearchColumnFactory();
        $facFil = new AMRequestSearchFilterFactory();
        $re     = new stdClass();

        // remove old item
        if ($isUpdate) {
            $whereDel = ["SearchID" => $rsID];
            if ($facCol->delete($whereDel) === false)
                return AMRespone::dbFailed("Delete RequestSearchColumn failed", $rsID);

            if ($facFil->delete($whereDel) === false)
                return AMRespone::dbFailed("Delete RequestSearchCondition failed", $rsID);
        }

        // insert coluns
        $re->columns = [];
        foreach ($columns as $column) {
            $column["SearchID"] = $rsID;
            if ($facCol->insert($column) === false)
                return AMRespone::busFailed($column, "Insert RequestSearchColumn failed");

            $re->columns[] = (object)$column;
        }

        // insert conditions
        $re->conditions = [];
        foreach ($conditions as $condition) {
            $condition["SearchID"] = $rsID;
            if ($facFil->insert($condition) === false)
                return AMRespone::busFailed("Insert RequestSearchCondition failed");

            $re->conditions[] = $condition;
        }

        return new AMRespone($re);
    }
}


class SQLLog {
    public static $list = ["\n"];

    public function query($query) {
        global $wpdb;
        SQLLog::$list[] = $query;
                //var_dump("\n$query\n");
        return $wpdb->query($query);
    }


    public function get_results($query) {
        global $wpdb;
        SQLLog::$list[] = $query;
                //var_dump("\n$query\n");
        return $wpdb->get_results($query);
    }

    public static function save() {
        $query = join(";\n\n", SQLLog::$list);
                error_log($query);
        //var_dump("\n$query\n");
    }
}













