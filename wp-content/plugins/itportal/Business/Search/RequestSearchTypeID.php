<?php

class RequestSearchTypeID {
    public static $baseValue       = 1;
    public static $fieldValue      = 2;
    public static $curStepValue      = 3;
    public static $stepValue      = 4;
    public static $combined        = 5;

}
