<?php
/**
 * Created by PhpStorm.
 * User: LAP11400-local
 * Date: 7/24/2017
 * Time: 3:54 PM
 */
if ( ! function_exists( 'import' ) ) {
    require_once get_template_directory() . '/package/import.php';
}

if (!class_exists('ActionManager')) {
    class ActionManager
    {
        public function GetActions(){
            $path = 'plugin.itportal.Business.Actions';
            $files = array_diff(scandir(getFullPath($path, '')), array('..', '.'));
            $result = array();
            foreach ($files as $file){
                $fileName = substr($file,0, strrpos($file, '.php'));
                $fileContent = getContent($path.'.'.$fileName);

                preg_match('/\/\*\*([^\/]*)\*\//s',$fileContent,$matches);

                $Document = $matches[0];
                $Document = preg_replace('/[\/\*]/s', '', $Document);
                $Document = preg_replace('/Action Document/s', '', $Document);

                preg_match_all('/([^\\n].+: .+)\\n/', $Document, $Document);
                $actionDocument = array();
                foreach ($Document[0] as $DocumentItem){
                    $DocumentItem = preg_replace('/^[\\r\\n\\t ]+/', '', $DocumentItem);
                    $DocumentItem = preg_replace('/[\\r\\n\\t ]$/', '', $DocumentItem);

                    if(strrpos($DocumentItem, ':') >= 0) {
                        $itemName = substr($DocumentItem, 0, strrpos($DocumentItem, ':'));
                        $itemValue = substr($DocumentItem, strrpos($DocumentItem, ':') + 2);
                        $actionDocument[$itemName] = $itemValue;
                    }
                }

                array_push($result, $actionDocument);
            }

            return $result;
        }

        public function GetMethods($action){
            $path = 'plugin.itportal.Business.Actions.'.$action;
            $result = array();
            $fileContent = getContent($path);

            //Get all document
            preg_match_all('/\/\*\*([^\/]*)\*\//s',$fileContent,$matches);

            foreach ($matches[0] as $document){
                //Skip action document
                if(!strrpos($document,'Action Document')){
                    //Get all method attribute(Name, Description v.v.)
                    preg_match_all('/(\* [^\\n@].+: .+)\\n/', $document, $docAttributes);
                    $methodInfo = array();

                    foreach ($docAttributes[0] as $docAttribute){
                        //Remove new line
                        $docAttribute = preg_replace('/^[\*\\r\\n\\t ]+/', '', $docAttribute);
                        $docAttribute = preg_replace('/[\\r\\n\\t ]$/', '', $docAttribute);

                        if(strrpos($docAttribute, ':') >= 0) {
                            $itemName = substr($docAttribute, 0, strrpos($docAttribute, ':'));
                            $itemValue = substr($docAttribute, strrpos($docAttribute, ':') + 2);
                            $methodInfo[$itemName] = $itemValue;
                        }
                    }

                    //Get all parameter
                    preg_match_all('/([^\\n]@param .+)\\n/', $document, $docParams);
                    $methodInfo['params'] = array();

                    foreach ($docParams[0] as $docParam){
                        //Remove newline
                        $docParam = preg_replace('/^[\\r\\n\\t ]+/', '', $docParam);
                        $docParam = preg_replace('/[\\r\\n\\t ]$/', '', $docParam);
                        $paramItem = array();
                        preg_match('/\((.+)\)/', $docParam, $paramAttributes);

                        //Get parameter name
                        preg_match('/\$([^ ]*)/', $docParam, $paramName);
                        if($paramName != null){
                            $paramItem['name'] = trim($paramName[1]);
                        }

                        if($paramAttributes != null){
                            $paramAttributes = split(',', $paramAttributes[1]);

                            foreach ($paramAttributes as $paramAttribute){
                                $itemName = substr($paramAttribute, 0, strrpos($paramAttribute, ':'));
                                $itemValue = substr($paramAttribute, strrpos($paramAttribute, ':')+1);

                                $paramItem[trim($itemName)] = trim($itemValue);
                            }
                        }
                        array_push($methodInfo['params'], $paramItem);
                    }
                    array_push($result, $methodInfo);
                }
            }
            return $result;
        }

        public function DoAction($action, $method, $param){
            $path = 'plugin.itportal.Business.Actions.'.$action;
            $actionClass = using($path);

            unset($param['action']);
            unset($param['t']);
            unset($param['m']);
            unset($param['actionName']);
            unset($param['method']);
            $result = call_user_func_array(array($actionClass, $method),$param);

            return $result;
        }
    }
}