<?php
/**
 * Created by PhpStorm.
 * User: tienseuit
 * Date: 26-Mar-18
 * Time: 10:11
 */

class SQLQueryBuilder {
    protected $selects = [];
    protected $froms   = [""];
    public    $where;

    public function __construct(SQLParamaterBuilder $paramBuilder = null) {
        $this->where = new SQLWhereBuilder($paramBuilder ? $paramBuilder : new SQLParamaterBuilder());
    }

    public function select($name, $as = null) {
        $this->selects[] = $name . ($as ? " AS $as" : "");
    }

    public function from($name, $as = null) {
        $this->froms[0] = [$name, $as];
    }

    /**
     * @param SQLQueryBuilder|string $with
     * @param string $type type of join
     * @param string $as new name
     */
    public function join($type, $with, $adition) {
        $this->froms[] = [$type, $with, $adition];
    }

    public function toString() {
        // select
        $this->selects = array_unique($this->selects);
        $select        = join(",\n\t", $this->selects);
        if ($select == "")
            $select = "SELECT * ";
        else
            $select = "SELECT " . $select;

        // from
        $froms = [];
        foreach ($this->froms as $f) {
            $table   = $f[1];
            $table   = is_object($table) ? $table->toString() : $table;
            $froms[] = $f[0] . " " . $table . " " . $f[2];
        }
        $froms = array_unique($froms);
        $from  = join("\n\t", $froms);

        // where
        $where = $this->where->toString();
        if ($where)
            $where = "WHERE " . $where;

        return "$select\nFROM $from\n$where";
    }

    public function querySetParams() {
        $wpdb = new SQLLog();
        foreach ($this->where->paramBuilder->toStrings() as $item)
            $wpdb->query($item);
    }
}
