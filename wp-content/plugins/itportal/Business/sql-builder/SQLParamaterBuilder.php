<?php
/**
 * Created by PhpStorm.
 * User: tienseuit
 * Date: 26-Mar-18
 * Time: 10:12
 */

class SQLParamaterBuilder {
    protected static $count = 0;
    protected        $map   = [];

    public function add($value) {
        $name = $this->map[$value];
        if ($name == null) {
            $name = "@_P" . SQLParamaterBuilder::$count;
            SQLParamaterBuilder::$count++;
            $this->map[$value] = $name;
        }

        return $name;
    }

    public function toStrings() {
        $re = [];
        foreach ($this->map as $value => $name)
            $re[] = "set $name = '$value'";

        return $re;
    }
}
