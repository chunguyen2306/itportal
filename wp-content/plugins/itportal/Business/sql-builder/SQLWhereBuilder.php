<?php
/**
 * Created by PhpStorm.
 * User: tienseuit
 * Date: 26-Mar-18
 * Time: 10:12
 */

class SQLWhereBuilder {
    protected $filters = [];
    protected $wfBuilder;
    public    $paramBuilder;

    public function __construct($paramBuilder) {
        $this->paramBuilder = $paramBuilder;
    }

    /**
     * @param string|SQLWhereBuilder $express
     * @param $type : "or" || "and"
     * @param $type : "or" || "and"
     */
    public function add($type, $col, $operator, $value) {
        $this->filters[] = [$type, $col . " " . $operator . " " . $this->paramBuilder->add($value)];
    }

    public function addString($type,$str){
        $this->filters[] = [$type, $str];
    }

    public function addFilters($type, $col, $filters){
        foreach ($filters as $filter){
            $this->filters[] = [$type, $col . " " . $filter->operator . " " . $this->paramBuilder->add($filter->value)];
        }
    }

    /**
     * @param $type
     * @return SQLWhereBuilder
     */
    public function addSub($type, $whereBulder) {
        if ($whereBulder == null)
            $whereBulder = new SQLWhereBuilder($this->paramBuilder);
        $this->filters[] = [$type, $whereBulder];
        return $whereBulder;
    }

    public function toString($glue = "\n\t") {
        if (sizeof($this->filters) == 0)
            return "";
        $this->filters[0][0] = "";
        $result              = [];
        foreach ($this->filters as $c) {
            $express = $c[1];
            if (is_object($express)) {
                $express = $express->toString(" ");
                if ($express)
                    $express = "(" . $express . ")";
                else
                    continue;
            }
            $result[] = $c[0] . " " . $express;
        }

        $result = array_unique($result);
        return join($glue, $result);
    }

    public function getWFBuilder() {
        if ($this->wfBuilder == null)
            $this->wfBuilder = new StepBuilderUtil($this->paramBuilder);

        return $this->wfBuilder;
    }
}
