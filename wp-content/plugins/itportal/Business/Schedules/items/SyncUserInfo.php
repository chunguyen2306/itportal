<?php
/**
 * <b>Author:</b> TienTM2
 * <b>Description:</b> Đồng bộ tất cả thông tin của tất cả nhân viên từ VHCS
 * <b>Process:</b>
 * [1] ITAM3 Wordpress
 * [2] vHRS
 * [3] ITAM3 Wordpress
 * [4] ITAM3 DB
 */

if(!class_exists('SyncUserInfo')){
    //import your package here
    import('theme.package.HttpRequest');
    import ('plugin.amUser.class.factory.AMUserFactory');

    class SyncUserInfo{
        //Your code here
        public function run(){
            $db = new AMUserFactory();

            $syncSetting = using('plugin.amUser.class.includes.schedules.ScheduleSetting');

            $request = new HttpRequest(array(
                'url' => $syncSetting['sync_url'].$syncSetting['sync_methods']['getEmployeeByCriteria'],
                'certificate' => $syncSetting['sync_certificate'],
                'data' => array(
                    'pageSize' => '1',
                    'pageNum' => '0',
                    'active' => 'true'
                )
            ));

            $response = $request->send();

            $succCount = 0;
            $errCount = 0;
            $emps = json_decode($response->reponseText);
            foreach($emps as $emp){
                $existed = $db->getOne(array(
                   'filter' => array(
                       array( "domainAccount = '$emp->domainAccount'", '' )
                   )
                ));

                $newInfo = array(
                    'active' => $emp->active,
                    'departmentCode' => $emp->departmentCode,
                    'departmentLead'  => $emp->departmentLead,
                    'departmentName' => $emp->departmentName,
                    'divisionCode' => $emp->divisionCode,
                    'divisionLead' => $emp->divisionLead,
                    'divisionName' => $emp->divisionName,
                    'domainAccount' => $emp->domainAccount,
                    'empCode' => $emp->empCode,
                    'ext' => $emp->ext,
                    'firstName' => $emp->firstName,
                    'fullName' => $emp->fullName,
                    'fullyCode' => $emp->fullyCode,
                    'gender' => $emp->gender,
                    'jobTitle' => $emp->jobTitle,
                    'lastName' => $emp->lastName,
                    'location' => $emp->location,
                    'phone' => $emp->phone,
                    'reportingLine' => $emp->reportingLine,
                    'seat' => $emp->seat,
                    'teamLead' => $emp->teamLead,
                    'teamName' => $emp->teamName,
                    'workingEmail' => $emp->workingEmail,
                );

                if($existed == null){
                    //$array = json_decode(json_encode($emp), true);
                    $result = $db->insert($newInfo);
                    if($result){
                        $succCount++;
                    } else {
                        $errCount++;
                    }
                } else {
                    $result = $db->update($newInfo, array(
                        'domainAccount' => $emp->domainAccount
                    ));
                    if($result){
                        $succCount++;
                    } else {
                        $errCount++;
                    }
                }
                unset($existed);
            }
            error_log('Success: '.$succCount.', Error: '.$errCount);
        }
    }
}

return new SyncUserInfo();
?>