<?php
/**
 * Created by Administrator on 3/10/2017
 */

if(!class_exists('scheduleManager')){
    //import your package here
    import ('plugin.amUser.class.factory.AMUserSyncSettingFactory');

    class scheduleManager{
        //Your code here

        public function run_one($sync)
        {
            $objClass = using('plugin.amUser.class.includes.schedules.items.'.$sync->Class);
            error_log('Run '.$sync->Name.' [class: '.$sync->Class.']');
            $objClass->run();
        }

        public function run($type){
            $setting = using ('plugin.amUser.class.includes.schedules.ScheduleSetting');
            $db = new AMUserSyncSettingFactory();
            $syncs = $db->query(array(
               'filter' => array(
                   array(
                       "Type = '".$type."'", ""
                   )
               )
            ));

            foreach ($syncs as $sync) {
                if($setting[$sync->Type]['type'] == 'daily') {
                    $currDate = date('H');
                    $syncDate = date('H', strtotime($sync->Time));
                    if ($syncDate == $currDate) {
                        $this->run_one($sync);
                    }
                } else {
                    $this->run_one($sync);
                }
            }
        }
    }
}
return new scheduleManager();
?>