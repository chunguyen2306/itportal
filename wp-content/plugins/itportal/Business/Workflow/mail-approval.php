<?php

set_time_limit(1000);
include_once 'imap.php';

function __maDebug($message){
    echo '<br>';
    echo date('m-d H:i:s.');
    echo explode('.', microtime(true))[1];
    echo $message;
}

class MaillApproval {
    static $dirNew       = 'auto-approval/new';
    static $dirDone      = 'auto-approval/done';
    static $dirFailed    = 'auto-approval/failed';
    static $dirException = 'auto-approval/exception';
    static $spam         = 'auto-approval/spam';
    static $errorEmail   = null;

    public static function fetchEmail($callAble) {
        ini_set('xdebug.var_display_max_depth', 555);
        ini_set('xdebug.var_display_max_children', 2556);
        ini_set('xdebug.var_display_max_data', 10024);

        $config     = using('plugin.itportal.config.Email')['imap'];
        $host       = $config['host'];
        $port       = $config['port'];
        $encryption = $config['encryption'];
        $username   = $config['username'];
        $password   = $config['password'];

        __maDebug('Start connect');

        $mailbox = '{' . $host . ':' . $port . '/imap/' . $encryption . '}' . MaillApproval::$dirNew;
        $imap = new IMAPMailbox($mailbox, $username, $password);

        $emails = $imap->search('UNSEEN');
        if ($emails) {
            foreach ($emails as $email) {
                $overview     = $email->getDefaultOverview();
                $plainMessage = $email->getPlainTextMessage();

                if (preg_match('/([\w]+)@vng\.com\.vn/', $overview->from, $matches) !== 1) {
                    AMRespone::busFailed($overview->from, 'Chat approval mask as spam for external email');

                    if ($email->move(MaillApproval::$spam) == false)
                        AMRespone::busFailed($email->uid, 'IMAP move email failed: ' . MaillApproval::$spam);

                    continue;
                }

                $reportError = false;

                try {
                    $mailApproveData = MaillApproval::parseMail($plainMessage);
                    $taskRes = null;
                    if ($mailApproveData->code === AMResponeCode::$ok) {

                        $mailApproveData->data->domain = strtolower($matches[1]);
                        $taskRes                          = $callAble($mailApproveData->data, $email->getAttachments());
                        if ($taskRes->code == AMResponeCode::$ok) {
                            if ($email->move(MaillApproval::$dirDone) == false)
                                AMRespone::busFailed($email->uid, 'IMAP move email failed: ' . MaillApproval::$dirDone);
                        }
                        else {

                            echo '<div style="border-top: 25px silver solid; border-bottom: 25px silver solid;">';
                            echo $plainMessage;
                            echo '<div style="border-top: 5px silver solid">';
                            echo '</div>';
                            echo '<div style="border-top: 5px silver solid">';
                            echo '</div>';
                            echo '</div>';

                            if ($email->move(MaillApproval::$dirFailed) == false)
                                AMRespone::busFailed($email->uid, 'IMAP move email failed: ' . MaillApproval::$dirFailed);
                        }
                        __maDebug('approval');
                        var_dump($mailApproveData);
                        var_dump($taskRes);
                    }

                    if ($mailApproveData->code != AMResponeCode::$ok || $taskRes->code != AMResponeCode::$ok){
                        $reportError = true;
                        if ($email->move(MaillApproval::$dirFailed) == false)
                            AMRespone::busFailed($email->uid, 'IMAP move email failed: ' . MaillApproval::$dirFailed);
                    }
                } catch (Exception $ex) {
                    $reportError = true;
                    if ($email->move(MaillApproval::$dirException) == false)
                        AMRespone::busFailed($email->uid, 'IMAP move email failed: ' . MaillApproval::$dirException);
                }

                // báo lỗi
                if ($reportError) {
                    if (defined('devmode'))
                        MaillApproval::reportError('tiennv6');
                    else if ($mailApproveData->data && $mailApproveData->data->domain)
                        MaillApproval::reportError($mailApproveData->data->domain);
                }
            }
        }

        $imap->dispose();
    }

    protected static function parseMail($body) {
        $body        = strip_tags($body);
        $matchResult = preg_match('/========================================================\n*\s*Đây là mã nhận diện xử lý đơn hàng, vui lòng không xóa\.(\n*\s*)(\d{12})([\dA-Z]{40})/', $body, $matches);

        // check mail format
        if ($matchResult !== 1) {
            $res                    = AMRespone::paramsInvalid($body, 'Mail format invalid: missing role code');
            $res->isParseCodeFailed = true;
            return $res;
        }

        $index_end = strpos($body, $matches[0]);

        // checkout approval content
        if ($index_end < 10)
            return AMRespone::busFailed($body, 'Mail format invalid: invalid content');
        $approvalContent = substr($body, 0, $index_end);

        // checkout method and data
        $re = null;
        if (($index_start = strpos($approvalContent, "Duyệt:")) || $index_start === 0) {
            $covers     = [
                'approval' => 'Duyệt:',
                'comment' => 'Ghi chú:'
            ];
            $re         = MaillApproval::checkoutData($approvalContent, $covers);
            $re->method = 'approve';

            if ($re->approval === 'đồng ý')
                $re->isApproved = true;
            else if ($re->approval === 'từ chối')
                $re->isApproved = false;
            else
                return AMRespone::busFailed($re, 'Approval data invalid');

            if (!$re->isApproved && !$re->comment)
                return AMRespone::paramsInvalid();
        }
        else if (($index_start = strpos($approvalContent, "Hỏi thêm thông tin từ:")) || $index_start === 0) {
            $covers = [
                'mentionDomains' => 'Hỏi thêm thông tin từ:',
                'comment' => 'Câu hỏi:'
            ];
            $re     = MaillApproval::checkoutData($approvalContent, $covers);

            preg_match_all('/[\w\d_\-\.]+/', $re->mentionDomains, $domainMatch);
            if ($domainMatch)
                $re->mentionDomains = $domainMatch[0];
            else
                return AMRespone::busFailed($body, 'Missing mentionDomains');

            if (!$re->mentionDomains || !$re->comment)
                return AMRespone::paramsInvalid();

            foreach ($re->mentionDomains as $mentionDomain){
                if(strpos($re->comment, $mentionDomain) === false)
                    $re->comment = '@' . $mentionDomain . ' ' . $re->comment;
            }

            $re->method = 'ask';
        }
        else if (($index_start = strpos($approvalContent, "Cung cấp thông tin cho:")) || $index_start === 0) {
            $covers     = ['comment' => 'Trả lời:'];
            $re         = MaillApproval::checkoutData($approvalContent, $covers);
            $re->method = 'answer';
            if (!$re->comment)
                return AMRespone::paramsInvalid();
        }
        else if (($index_start = strpos($approvalContent, "Ủy quyền cho:")) || $index_start === 0) {
            $covers = [
                'entrustDomain' => 'Ủy quyền cho:',
                'comment' => 'Ghi chú:'
            ];
            $re     = MaillApproval::checkoutData($approvalContent, $covers);

            preg_match_all('/[\w\d_\-\.]+/', $re->entrustDomain, $domainMatch);
            if ($domainMatch)
                $re->entrustDomain = $domainMatch[0][0];
            else
                return AMRespone::busFailed($body, 'Missing entrustDomain');
            $re->method = 'entrust';

            if (!$re->entrustDomain)
                return AMRespone::paramsInvalid();
        }
        else {
            return AMRespone::busFailed($body, 'Missing method');
        }

        $re->requestID    = intval($matches[2]);
        $re->approvalCode = $matches[3];

        return new AMRespone($re);
    }

    protected static function checkoutData($source, $covers) {
        $index_start = 0;
        $index_end   = null;
        $curAttrName = null;
        $re          = new stdClass();
        foreach ($covers as $attrName => $mark) {
            $index_end = strpos($source, $mark, $index_start);
            if (is_numeric($index_end) === false)
                continue;

            if ($curAttrName)
                $re->{$curAttrName} = trim(substr($source, $index_start, $index_end - $index_start));

            $index_start = $index_end + strlen($mark);
            $curAttrName = $attrName;
        }

        if ($curAttrName)
            $re->{$curAttrName} = trim(substr($source, $index_start));

        return $re;
    }

    static function reportError($domain) {
        if (!$domain)
            return;

        $errorEmail = MaillApproval::$errorEmail;
        if (!$errorEmail) {
            MaillApproval::$errorEmail = using('plugin.itportal.config.mail-template.ApprovalError');
            $errorEmail                = MaillApproval::$errorEmail;
        }
        wp_mail("$domain@vng.com.vn", $errorEmail['subject'], $errorEmail['body']);
    }
}
