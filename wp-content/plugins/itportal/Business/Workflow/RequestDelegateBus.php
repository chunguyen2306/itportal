<?php
import('plugin.itportal.Factory.AMRequestDelegateFactory');

class RequestDelegateBus {

    private $fac;

    public function __construct() {
        $this->fac = new AMRequestDelegateFactory();
    }

    public function GetDelegate($domain) {
        $re = $this->fac->getByDomain($domain);
        return $re === false ? AMRespone::dbFailed($re) : new AMRespone($re);
    }

    public function InsertDelegate($data) {
        if (isset($data['ID']))
            unset($data['ID']);

        $re = $this->fac->insert($data);
        global $wpdb;
        $data['ID'] = $wpdb->insert_id;
        return $re === false ? AMRespone::dbFailed($data) : new AMRespone($data);
    }

    public function UpdateDelegate($data) {
        if (isset($data['ID']))
            unset($data['ID']);

        $re = $this->fac->update($data, ['Domain' => $data['Domain']]);
        return $re === false ? AMRespone::dbFailed($data) : new AMRespone($data);
    }

    public function DeleteDelegate($domain) {
        $re = $this->fac->delete([
            'Domain' => $domain
        ]);
        return $re === false ? AMRespone::dbFailed($domain, 'DeleteDelegate failed') : new AMRespone($re);
    }

    public function CheckDelegate($domain) {
        $res = $this->GetDelegate($domain);
        if ($res->code != AMResponeCode::$ok)
            return $res;

        $re = null;
        if ($res->data) {
            $data = $res->data;
            $now  = date('Y-m-d H:i:s');
            if ($data->DelegatedDomain && $data->StartTime < $now && $now < $data->EndTime) {
                $re = $data;
            }
        }

        return new AMRespone($re);
    }

    public function DelegateRequestSteps($stepIDs, $delegatedDomain, $comment, $attachments = null) {
        if (!is_array($stepIDs) || !$delegatedDomain)
            return AMRespone::paramsInvalid([$stepIDs, $delegatedDomain]);

        $currentUser = wp_get_current_user();
        if ($currentUser == 0 || $currentUser->user_login == false)
            return AMRespone::busFailed(null, "not logined");
        $ownerDomain = $currentUser->user_login;

        $api = using('plugin.itportal.API.Workflow.AMRequestAjax');
        $re = new AMRespone();
        global $wpdb;
        $wpdb->query('START TRANSACTION');
        try {

            foreach ($stepIDs as $stepID) {
                $args['stepID'] = $stepID;
                $temp           = $api->_EntrustRequestStep($ownerDomain, $delegatedDomain, $stepID, $comment, $attachments, true);

                if ($temp->code != AMResponeCode::$ok){
                    $re = $temp;
                    return $re;
                }
            }

            return $re;
        } catch (Exception $ex) {
            $re = AMRespone::exception(get_defined_vars(), $ex);
            return $re;
        } finally {
            if($re->code != AMResponeCode::$ok)
                $wpdb->query('ROLLBACK');
            else
                $wpdb->query('COMMIT');
        }
    }
}
