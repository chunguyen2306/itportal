<?php

class IMAP {

    public static $TYPE_TEXT        = 0;
    public static $TYPE_MULTIPART   = 1;
    public static $TYPE_APPLICATION = 3;
}

class IMAPMailbox implements IteratorAggregate, Countable {
    private $stream;

    public function __construct($hostname, $username, $password) {
        $stream = imap_open($hostname, $username, $password);
        if (FALSE === $stream) {
            AMres('Connect failed: ' . imap_last_error());
        }
        $this->stream = $stream;
    }

    public function getStream() {
        return $this->stream;
    }

    /**
     * @return stdClass
     */
    public function check() {
        $info = imap_check($this->stream);
        if (FALSE === $info) {
            throw new Exception('Check failed: ' . imap_last_error());
        }
        return $info;
    }

    /**
     * @param string $criteria
     * @param int $options
     * @param int $charset
     * @return IMAPMessage[]
     * @throws Exception
     */
    public function search($criteria = null) {
        $emails = imap_sort($this->stream, SORTDATE, 0 ,SE_UID, $criteria);
        if (FALSE === $emails) {
            throw new Exception('IMAP sort failed: ' . imap_last_error());
        }
        foreach ($emails as &$email) {
            $email = $this->getMessageByNumber($email);
        }
        return $emails;
    }

    /**
     * @param int $number
     * @return IMAPMessage
     */
    public function getMessageByNumber($number) {
        return new IMAPMessage($this, $number);
    }

    public function getIterator() {
        return $this->getOverview()->getIterator();
    }

    public function count() {
        return $this->check()->Nmsgs;
    }

    public function dispose() {
        imap_close($this->stream);
    }
}

class IMAPMessage {
    public $uid;

    private $mailbox;
    private $stream;
    private $_lazy;

    public function __construct(IMAPMailbox $mailbox, $uid) {
        $this->mailbox = $mailbox;
        $this->uid     = $uid;
        $this->stream  = $mailbox->getStream();
        $this->_lazy   = new stdClass();
    }

    public function getPlainTextMessage() {
        if ($this->_lazy->plainText)
            return $this->_lazy->plainText;

        $items = $this->fectDatas(function ($part) {
            return $part->subtype == 'PLAIN';
        });
        if (!$items)
            throw new Exception('Can not fetch plain message');

        $this->_lazy->plainText = $items[0]->data;
        return $this->_lazy->plainText;
    }

    public function getAttachments() {
        if ($this->_lazy->attachments)
            return $this->_lazy->attachments;

        $items = $this->fectDatas(function ($part) {
            return $part->disposition == 'ATTACHMENT';
        });


        foreach ($items as $item) {
            $this->additionAttachmentInfo($item, $item->part);
            unset($item->part);
        }

        $this->_lazy->attachments = $items;
        return $this->_lazy->attachments;
    }

    public function getDefaultOverview() {
        if ($this->_lazy->overviews)
            return $this->_lazy->overviews[0];

        $this->_lazy->overviews = imap_fetch_overview($this->stream, $this->uid, FT_UID);
        return $this->_lazy->overviews[0];
    }

    public function move($dir) {
        return imap_mail_move($this->stream, $this->uid, $dir, CP_UID);
    }

    public function getStructure() {
        if ($this->_lazy->structure)
            return $this->_lazy->structure;

        $structure = imap_fetchstructure($this->stream, $this->uid, FT_UID);
        if (FALSE === $structure) {
            throw new Exception('IMAP FetchStructure failed: ' . imap_last_error());
        }

        $this->_lazy->structure = $structure;
        return $structure;
    }

    public function __toString() {
        return (string)$this->uid;
    }

    function fectDatas($fnFilter, $section = '', $parts = null) {
        $isRoot = $parts === null;

        if ($isRoot) {
            $parts = [$this->getStructure()];
        }

        $re = [];

        if (!$parts)
            return $re;

        $n = sizeof($parts);
        for ($i = 0; $i < $n; $i++) {
            $part = $parts[$i];

            if ($part->type == IMAP::$TYPE_MULTIPART) {
                if (!$part->parts)
                    return $re;

                if (!$isRoot) {
                    $subSelection = $section;
                    $subSelection = $section ? $section . ($i + 1) . '.' : ($i + 1) . '.';
                }

                $subRes = $this->fectDatas($fnFilter, $subSelection, $part->parts);
                foreach ($subRes as $item)
                    $re[] = $item;
            }
            else if ($fnFilter($part)) {
                $item          = new stdClass();
                $item->rawdata = imap_fetchbody($this->stream, $this->uid, $section . ($i + 1), FT_UID);
                $item->data    = $this->decryptData($item->rawdata, $part->encoding);
                $item->part    = $part;

                $re[] = $item;
            }
        }

        return $re;
    }

    function additionAttachmentInfo($messageItem, $part) {
        $fileName = null;
        if ($part->ifdescription) {
            $fileName = $this->checkAndGetFileName($part->description);
            if ($fileName){
                $messageItem->fileName = $fileName;
                return;
            }
        }

        if ($part->ifdparameters) {
            foreach ($part->dparameters as $dparameter) {
                if ($dparameter->attribute == 'FILENAME') {
                    $fileName = $this->checkAndGetFileName($dparameter->value);
                    if ($fileName){
                        $messageItem->fileName = $fileName;
                        return;
                    }
                }
            }
        }

        if ($part->ifparameters) {
            foreach ($part->parameters as $parameter) {
                if ($parameter->attribute == 'NAME') {
                    $fileName = $this->checkAndGetFileName($parameter->value);
                    if ($fileName){
                        $messageItem->fileName = $fileName;
                        return;
                    }
                }
            }
        }

        throw new Exception('Can not specify attachment file name');
    }

    function checkAndGetFileName($text) {
        $fileName = imap_mime_header_decode($text)[0]->text;
        $fileName = trim($fileName, "\" \t\0\x0B");

        $fileName = preg_replace('/[\\\\\\/:?*"<>|]/', '', $fileName);
        if ($fileName && pathinfo($fileName, PATHINFO_EXTENSION))
            return $fileName;
        else
            return null;
    }

    function decryptData($rawdata, $encoding) {
        if ($encoding == 3) {
            return imap_base64($rawdata);
        }
        else if ($encoding == 1) {
            return imap_8bit($rawdata);
        }
        else if ($encoding == 4) {
            return quoted_printable_decode($rawdata);
        }
        else {
            return imap_qprint($rawdata);
        }
    }
}

class IMAPAttachment {
    private $attachment;
    private $message;

    public function __construct(IMAPMessage $message, $attachment) {
        $this->message    = $message;
        $this->attachment = $attachment;
    }

    /**
     * @return string;
     */
    public function getBody() {
        return $this->message->fetchBody($this->attachment->number);
    }

    /**
     * @return int
     */
    public function getSize() {
        return (int)$this->attachment->bytes;
    }

    /**
     * @return string
     */
    public function getExtension() {
        return pathinfo($this->getFilename(), PATHINFO_EXTENSION);
    }

    public function getFilename() {
        $filename = $this->attachment->filename;
        NULL === $filename && $filename = $this->attachment->name;
        return $filename;
    }

    public function __toString() {
        $encoding = $this->attachment->encoding;
        switch ($encoding) {
            case 0: // 7BIT
            case 1: // 8BIT
            case 2: // BINARY
                return $this->getBody();

            case 3: // BASE-64
                return base64_decode($this->getBody());

            case 4: // QUOTED-PRINTABLE
                return imap_qprint($this->getBody());
        }
        throw new Exception(sprintf('Encoding failed: Unknown encoding %s (5: OTHER).', $encoding));
    }
}
