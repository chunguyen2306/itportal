<?php

class RequestData {
    private   $mapFieldDefinition;
    private   $mapStepDefinition;
    protected $__lazy;

    /**
     * RequestData constructor.
     * @param $requestData
     * @param $preventMapping ngăn không cho mapping definition với data, data với definition
     */
    public function __construct($requestData, $preventMapping = false) {
        $this->__lazy = new stdClass();

        foreach ($requestData as $pro => $value)
            $this->{$pro} = $value;

        foreach ($this->listFieldDefinition as $df) {
            $this->mapFieldDefinition[$df->Name] = $df;
            if ($df->listChild) {
                foreach ($df->listChild as $colDefinition) {
                    $this->mapFieldDefinition[$colDefinition->Name] = $colDefinition;
                }
            }
        }

        $this->mapStepDefinition = [];
        foreach ($this->listStepDefinition as $df)
            $this->mapStepDefinition[$df->Name] = $df;
    }

    function depthCopy($data) {
        $re = $data;
        if (is_object($data)) {
            $re = new stdClass();
            foreach ($data as $key => $val)
                $re->{$key} = $this->depthCopy($val);
        }
        else if (is_array($data)) {
            $re = [];
            foreach ($data as $key => $val)
                $re[$key] = $this->depthCopy($val);
        }

        return $re;
    }

    public function setField($field) {
        foreach ($this->listFieldDefinition as $df) {
            $this->mapFieldDefinition[$df->Name] = $df;
            $df->value                           = $field->{$df->Name};

            if ($df->listChild) {
                foreach ($df->listChild as $colDefinition) {
                    $this->mapFieldDefinition[$colDefinition->Name] = $colDefinition;
                }
            }
        }
    }

    public function setRequestOwner($value) {
        $this->request->Owner = $value;
        $field                = $this->getFieldDefinition($this->workflow->OwnerField);
        if ($field->RequestDataTypeID == RequestDataTypeID::$domain)
            $field->value = $value;
    }

    public function setFieldvalueByDefinition($definition, $value) {
        $definition->value                           = $value;
        $this->mapFieldDefinition[$definition->Name] = $definition;
    }

    public function setListStep($listStep) {
        $this->listStep = $listStep;
        foreach ($this->listStepDefinition as $stepDefinition) {
            foreach ($listStep as $step) {
                if ($stepDefinition->ID === $step->DefinitionID) {
                    $stepDefinition->step = $step;
                    $step->definition     = $stepDefinition;
                    break;
                }
            }
        }
    }

    public function getFieldvalue($name) {
        return $this->mapFieldDefinition[$name]->value;
    }

    public function getFieldDefinition($name) {
        return $this->mapFieldDefinition[$name];
    }

    public function getStepDefinition($name) {
        return $this->mapStepDefinition[$name];
    }

    public function getStepDefinitionByID($id) {
        foreach ($this->listStepDefinition as $stepDefinition) {
            if ($stepDefinition->ID == $id)
                return $stepDefinition;
        }

        return null;
    }

    public function getField() {
        $re = new stdClass();
        foreach ($this->listFieldDefinition as $fieldDefinition) {
            $re->{$fieldDefinition->Name} = $fieldDefinition->value;
        }

        return $re;
    }

    public function setFieldValue($name, $value) {
        $df        = $this->mapFieldDefinition[$name];
        $df->value = $value;
    }

    public function createStep($requestID, $stepDefinition, $init) {
        $step = is_object($init) ? clone $init : new stdClass();

        if ($init) {
            if (is_string($init))
                $step->Owner = $init;

            $step->MinIntendTime               = date('Y-m-d H:i:s');
            $step->MaxIntendTime               = $step->MinIntendTime;
            $stepDefinition->MinIntendDuration = 0;
            $stepDefinition->MaxIntendDuration = 0;

            if ($stepDefinition->StepIndex == 0)
                $step->StartDate = $step->MinIntendTime;
        }

        $step->DefinitionID   = $stepDefinition->ID;
        $step->RequestID      = $requestID;
        $stepDefinition->step = $step;
        $step->definition     = $stepDefinition;
        $this->listStep[]     = $step;

        return $step;
    }

    public function getCurrentStep() {
        $currentStep = $this->listStepDefinition[$this->request->CurrentIndex]->step;
        if ($currentStep == null)
            AMRespone::busFailed($this, "Get current step failed", 1);

        return $currentStep;
    }

    public function setNextStepIndex($stepIndex) {
        $this->__lazy->nexStepIndex = $stepIndex;
    }

    public function getNextStep() {
        return $this->listStepDefinition[$this->__lazy->nexStepIndex]->step;
    }

    public function toStandardRequestData() {
    }

    public function toStepInsertData($step) {
        $insertData = (array)$step;
        unset($insertData["definition"]);
        return $insertData;
    }

    public function removeMapping() {
        foreach ($this->listStepDefinition as $stepDefinition) {
            $step = $stepDefinition->step;
            unset($step->definition);
        }
    }

    public function loadPKData() {
        if ($this->__lazy->_loadPKData)
            return;

        $fieldCan = $this->getFieldCan();
        foreach ($this->listFieldDefinition as $fieldDefinition) {
            if ($fieldCan->{$fieldDefinition->Name}->view == false)
                continue;

            if ($fieldDefinition->RequestDataTypeID == RequestDataTypeID::$intendTime) {
                $fieldDefinition->fkvalue = $this->listStepDefinition[sizeof($this->listStepDefinition) - 1]->step->MaxIntendTime;
            }
            //            else if ($fieldDefinition->RequestDataTypeID == RequestDataTypeID::$table) {
            //                $countAttr = '_count';
            //                $viewCols  = [];
            //                foreach ($fieldDefinition->listChild as $childDefinition) {
            //                    if ($fieldCan->{$childDefinition->Name}->view == false)
            //                        continue;
            //
            //                    $viewCols []             = $childDefinition;
            //                    $fkListChildDefinition[] = $childDefinition;
            //                    if ($childDefinition->RequestDataTypeID == RequestDataTypeID::$excel) {
            //                        if ($childDefinition->constrain->count) {
            //                            $countAttr = $childDefinition->Name;
            //                        }
            //                    }
            //                }
            //            }
            else {
                $fieldDefinition->fkvalue = $this->depthCopy($fieldDefinition->value);
            }
        }

        $this->processExcelColumn();

        $this->__lazy->_loadPKData = true;
    }

    public function loadUserFieldData($field) {
        //        if ($this->__lazy->doneloadUserFieldData)
        //            return;
        //
        //        foreach ($this->listFieldDefinition as $fieldDefinition) {
        //            switch ($fieldDefinition->RequestDataTypeID) {
        //                case RequestDataTypeID::$user:
        //                    $domain = $this->getFieldValue($this->workflow->OwnerField);
        //                    import('plugin.amWorkflow.class.factory.AMRequestUser');
        //                    $facRequestOwner        = new AMRequestUser();
        //                    $currentInfo            = $facRequestOwner->getOne(["filter" => [["Domain = '$domain' AND IsCurrent = 1"]]]);
        //                    $fieldDefinition->value = $currentInfo;
        //                    break;
        //            }
        //        }
        //        $this->__lazy->doneloadUserFieldData = true;
        //
        //        if ($this->request && $this->request->RequestStatusID != RequestStatusID::$open) {
        //            // request chưa hoàn thành, check thông tin thay đổi
        //
        //
        //        }
        //        else {
        //            // request đã hoàn thành, lấy thông tin cuối cùng
        //        }
        //
        //        $ownerField = $requestData->workflow->OwnerField;
        //        $domain     = $field[$ownerField];
        //        import('plugin.amUser.class.factory.AMUserFactory');
        //
        //        $newInfo = (new AMUserFactory())->GetUserInformationByDomain($domain);
        //        import('plugin.amWorkflow.class.factory.AMRequestUser');
        //        $facRequestOwner = new AMRequestUser();
        //        $currentInfo     = $facRequestOwner->getOne(["filter" => [["Domain = '$domain' AND IsCurrent = 1"]]]);
        //        preg_match('/\d+$/', $domain, $matches);
        //        $number = $matches ? " ($matches[0])" : "";
        //        $name   = $newInfo->firstName . '. ' . $newInfo->lastName . $number;
        //        $hasNew = $currentInfo == null || $currentInfo->JobTitle != $newInfo->jobTitle || $currentInfo->Department != $newInfo->departmentName || $currentInfo->Seat != $newInfo->seat || $currentInfo->Name != $name || $currentInfo->Location != $newInfo->location;
        //        if ($hasNew) {
        //            if ($currentInfo) {
        //                $requestOwnerUpdateInArray = [IsCurrent => 0];
        //                if ($facRequestOwner->update($requestOwnerUpdateInArray, [ID => $currentInfo->ID]) == false)
        //                    return AMRespone::dbFailed($requestOwnerUpdateInArray, "Update request owner info failed");
        //            }
        //
        //            $requestOwnerInArray = [Domain => $domain,
        //                Name => $name,
        //                JobTitle => $newInfo->jobTitle,
        //                Department => $newInfo->Department,
        //                Seat => $newInfo->seat,
        //                Location => $newInfo->location,
        //                Iscurrent => 1];
        //
        //            if ($facRequestOwner->insert($requestOwnerInArray) == false)
        //                return AMRespone::dbFailed($requestOwnerInArray, "Insert request owner info failed");
        //            global $wpdb;
        //            $insertValue = $fieldValue = $wpdb->insert_id;
        //        }
        //        else {
        //            $insertValue = $fieldValue = $currentInfo->ID;
        //        }

    }

    public function getApprovalCode() {
        if ($this->__lazy->ApprovalCode)
            return $this->__lazy->ApprovalCode;

        $fac  = new AMRequestApprovalCode();
        $code = $fac->getCode($this->request->ID);
        if ($code === false)
            return $code;

        $this->__lazy->ApprovalCode = $code;
        return $code;
    }

    public function loadOrCreateApprovalCode() {
        $code = $this->getApprovalCode();
        if (!$code) {
            $fac  = new AMRequestApprovalCode();
            $list = [chr(rand(48, 57)), $this->request->CurrentIndex % 10];
            for ($i = 2; $i < 40;) {
                $val = rand(48, 90);
                if ($val > 57 && $val < 65)
                    continue;

                $list[$i] = chr($val);
                $i++;
            }

            $code       = join('', $list);
            $insertData = [
                'RequestID' => $this->request->ID,
                'Code' => $code
            ];

            if ($fac->insert($insertData) === false) {
                logAM('getApprovalCode failed');
                return false;
            }
            else {
                $code = $this->getApprovalCode();
            }
        }

        return $code;
    }

    public function getFieldValueAsDomain($field) {
        if ($this->workflow->OwnerField == $field->Name)
            return $this->request->Owner;

        if ($field->RequestDataTypeID == RequestDataTypeID::$users) {
            $this->loadUserFieldData();
            return $field->value->domain;
        }
        else if ($field->RequestDataTypeID == RequestDataTypeID::$users) {
            return $field->value;
        }
        else {
            AMRespone::busFailed($field, 'getFieldValueAsDomain with unsupport field');
            return null;
        }
    }

    public function getOwnerDomain() {
        $ownerField = $this->getFieldDefinition($this->workflow->OwnerField);
        return $ownerField->RequestDataTypeID == RequestDataTypeID::$domain ? $ownerField->value : $ownerField->value->Domain;
    }

    /**
     * @param $stepDefinitionID , or null for current step
     * @return AMRespone|int
     */
    public function getFieldCan($stepDefinitionID = null) {
        if ($stepDefinitionID == null) {
            if ($this->request)
                $stepDefinitionID = $this->listStepDefinition[$this->request->CurrentIndex]->ID;
            else
                $stepDefinitionID = $this->listStepDefinition[0]->ID;
        }

        if ($this->__lazy->stepFieldCan) {
            $re = $this->__lazy->stepFieldCan[$stepDefinitionID];
            if ($re)
                return $re;
        }
        else {
            $this->__lazy->stepFieldCan = [];
        }


        $geter  = new WorkflowGet();
        $result = $geter->GetFieldCan($stepDefinitionID);
        if ($result->code != AMResponeCode::$ok)
            return $result;

        $re                                            = $result->data;
        $this->__lazy->stepFieldCan[$stepDefinitionID] = $re;

        return $re;
    }

    public function getParentFieldDefinition($name) {
        $fieldDefinition = $this->getFieldDefinition($name);
        $parentID        = $fieldDefinition->ParentID;

        foreach ($this->listFieldDefinition as $parentDefinition) {
            if ($parentDefinition->ID == $parentID)
                return $parentDefinition;
        }
    }

    public function getCurTask() {
        return $this->__lazy->curTask ? $this->__lazy->curTask : new stdClass();
    }

    public function setCurTask($taskID, $comment) {
        $this->__lazy->curTask = (object)[
            'taskID' => $taskID,
            'comment' => $comment
        ];
    }

    /**
     * dev_upgrade cheat để push được taskID đúng vào request_comment
     * request_comment sau này sẽ đổi thành request_history
     * @param null $taskID
     */
    public function taskEventID($taskID = null) {
        if ($taskID !== null)
            $this->__lazy->taskID = $taskID;

        return $this->__lazy->taskID;
    }

    public function isInProcess() {
        return $this->request->RequestStatusID == RequestStatusID::$open;
    }

    public function getDelegateBus() {
        if ($this->__lazy->delegateBus)
            return $this->__lazy->delegateBus;

        import('plugin.itportal.Business.Workflow.RequestDelegateBus');
        $re                        = new RequestDelegateBus();
        $this->__lazy->delegateBus = $re;
        return $re;
    }

    public function getRawData() {
        $data = new stdClass();
        $data->workflow = $this->depthCopy($this->workflow);
        $data->request = $this->depthCopy($this->request);
        $data->listFieldDefinition = $this->depthCopy($this->listFieldDefinition);

        $listStep = [];
        foreach ($this->listStepDefinition as $stepDef){
            $stepDefData = new stdClass();
            foreach ($stepDef as $pro => $value){
                $stepDefData->{$pro} = $value;
            }

            $stepData = new stdClass();
            foreach ($stepDef->step as $pro => $value){
                $stepData->{$pro} = $value;
            }
            $stepDef->step = $stepData;

            $listStep[] = $stepDefData;
        }

        $data->listStepDefinition = $listStep;

        return $data;
    }

    function getNameColumnExcel($listChild) {
        $excelCount = null;
        for ($i = 0; $i < count($listChild); $i++) {
            $name = $listChild[$i]->Name;
            if ($listChild[$i]->RequestDataTypeID == RequestDataTypeID::$excel) {
                if ($listChild[$i]->constrain->count != null) {
                    $excelCount = $name;
                }
            }
        }
        return $excelCount;
    }

    function compare2Rows($row1, $row2, $listChild) {
        $obj1 = (object)[];
        $obj2 = (object)[];
        for ($i = 0; $i < count($listChild); $i++) {
            $name = $listChild[$i]->Name;
            if ($listChild[$i]->RequestDataTypeID != RequestDataTypeID::$excel) {
                if ($row1->$name != null) {
                    $obj1->$name = $row1->$name;
                }
                if ($row2->$name != null) {
                    $obj2->$name = $row2->$name;
                }
            }
        }
        return ($obj1 == $obj2);
    }

    // HAM XU LY DEM SO LUONG SAN PHAM
    function processExcelColumn()
    {
        $requestData         = $this;
        $listFieldDefinition = $requestData->listFieldDefinition;

        foreach ($listFieldDefinition as $field) {
            if ($field->RequestDataTypeID == RequestDataTypeID::$table) {
                $fkvalue    = &$field->fkvalue;
                $listChild  = $field->listChild;
                $excelCount = $this->getNameColumnExcel($listChild);
                if (!$excelCount)
                    continue;

                $numOfRows = count($fkvalue);
                for ($i = 0; $i < $numOfRows; $i++) {
                    if ($fkvalue[$i]->$excelCount) {
                        $count = $fkvalue[$i]->$excelCount;
                    }
                    else {
                        $count = 1;
                    }
                    for ($j = $i + 1; $j < $numOfRows;) {
                        $resultCompare = $this->compare2Rows($fkvalue[$i], $fkvalue[$j], $listChild);
                        if ($resultCompare) {
                            array_splice($fkvalue, $j, 1);
                            $numOfRows--;
                            if ($fkvalue[$j]->$excelCount) {
                                $count += $fkvalue[$j]->$excelCount;
                            }
                            else {
                                $count += 1;
                            }

                        }
                        else {
                            $j++;
                        }
                    }

                    $fkvalue[$i]->$excelCount = $count;
                }
            }
        }
    }
}









