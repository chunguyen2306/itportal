<?php

if(class_exists('SMS') == false){
//    import('theme.package.HttpRequest');
    import('plugin.amUser.class.factory.AMUserFactory');
    import('theme.package.class.AMAsyncTask');

    class SMS {
     protected $facUser;
        public function __construct(){
            $this->facUser = new AMUserFactory();
        }

        public function send($toDomains, $content){
            $service = using('plugin.itportal.config.ServiceUrl')['SMS'];
            $content = str_replace('<br>', "\n", $content);
            $content = strip_tags($content);
            $content = str_replace('&nbsp;', " ", $content);
            $content = trim($content);
            $content = $this->toUVi($content);

            $sendata = [
                'url' => $service,
                'method' => 'POST',
                'data' => [
                    'message' => strip_tags($content)
                ]
            ];

            $args = ['', $content];

            foreach ($toDomains as $domain){


                $userInfo = $this->facUser->GetUserInformationByDomain($domain);
                if($userInfo == null)
                    continue;

                foreach($userInfo->Contact as $ct){
                    if(is_numeric($ct->Phone) == false)
                        continue;

//                    $sendata['data']['phoneNumer'] = $ct->Phone;

//                    $request    = new HttpRequest($sendata);
//                    $response   = $request->send();
//                    $result = json_decode($response->reponseText);
//                    if($result == null || $result->IsSuccess == false)
//                        AMRespone::busFailed($sendata, 'Send SMS failed ' . $response->reponseText);

//                    wp_remote_post($sendata['url'] , array(
//                        'blocking' => fasle,
//                        'timeout' => 0,
//                        'body' => $sendata['data']
//                    ));

                    $args[0] = $ct->Phone;
                    $aTask = new AMAsyncTask([
                        'action' => 'sms',
                        'data' => $args
                    ]);
                    $aTask->RunTask();
                }
            }

            return new AMRespone();
        }

        /**
         * Convert 1 chuỗi unicode sang tiếng việt không dấu
         * @param $str
         */
        function toUVi($utf8){

            if(!$utf8)
                return $utf8;

            $map = array(

                'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

                'd'=>'đ|Đ',

                'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

                'i'=>'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',

                'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

                'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

                'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',

            );

            foreach($map as $ascii=>$uni)
                $utf8 = preg_replace("/($uni)/i",$ascii,$utf8);

            return $utf8;
        }
    }
}