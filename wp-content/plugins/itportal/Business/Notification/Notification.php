<?php

if (class_exists('Notification') == false) {
    import('plugin.itportal.Factory.AMRequestNotificationFactory');
    import('plugin.amWorkflow.class.includes.NotificationType');

    class Notification {
        protected $comHost;

        public function __construct() {
            $serviceUrl    = using('plugin.itportal.config.ServiceUrl');
            $this->comHost = $serviceUrl['COM'];
        }

        public function push($domains, $title, $content, $typeID) {

            $fac = new AMRequestNotificationFactory();
            $mes = [
                "Title" => $title,
                "Content" => $content,
                "IsSeen" => 0,
                "IsRead" => 0,
                "TypeID" => $typeID,
                "Time" => date('Y-m-d H:i:s')
            ];

            foreach ($domains as $domain) {
                $insertData           = $mes;
                $insertData["Domain"] = $domain;
                $result               = $fac->insert($insertData);
                if ($result == false)
                    return AMRespone::dbFailed($insertData, "IT Portal notify acion failed");

            }

            //            return new AMRespone();

            $signalrMethod_HasNew = 1;
            $mesInJson            = json_encode([
                'method' => $signalrMethod_HasNew,
                'message' => $mes
            ]);


            $aTask = new AMAsyncTask([
                'action' => 'push_data',
                'data' => [$domains,$mesInJson]
            ]);
            $aTask->RunTask();

            //            wp_remote_post($this->comHost . '/API/chat.ashx' , array(
            //                    'blocking' => fasle,
            //                    'timeout' => 0,
            //                    'body' => [
            //                        'domains' => json_encode($domains),
            //                        'message' => $mesInJson
            //                    ]
            //                ));

            //            $request = new HttpRequest([
            //                'url' => $this->comHost . '/API/chat.ashx',
            //                'method' => 'POST',
            //                'data' => [
            //                    'domains' => json_encode($domains),
            //                    'message' => $mesInJson
            //                ]]);
            //            $response   = $request->send();
            //            if($response->reponseText != 'success')
            //                logAM('Communication chat failed:\n' . $response->reponseText . '\n');

            return new AMRespone();
        }

        public function mapping($domain, $signalrID) {
            return new AMRespone();
            $sendata = [
                'url' => $this->comHost . '/API/mapping.ashx',
                'method' => 'POST',
                'data' => [
                    'domain' => $domain,
                    'signalrID' => $signalrID
                ]
            ];

            $request  = new HttpRequest($sendata);
            $response = $request->send();
            if ($response->reponseText != 'success')
                return AMRespone::dbFailed($sendata, $response->reponseText);

            return new AMRespone();
        }
    }
}