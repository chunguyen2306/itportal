<?php
/*
  Plugin Name: IT Portal
  Plugin URI: https://vngms.sharepoint.com/sites/ITAM3Development/Development%20Wiki/ITAM3%20Wiki.aspx
  Description: Quản lý quy trình
  Version: 1.0
  Author: IT Dev Team
  Author URI: https://vngms.sharepoint.com/sites/ITAM3Development
  License: ITDEV-2017
  Text Domain: itportal
*/
if (!class_exists('ITPortal')) {
    if (!function_exists('import')) {
        require_once get_template_directory() . '/package/import.php';
    }

    class ITPortal
    {
        private $adminMenu = array();

        public function __construct()
        {
            add_action('init', array($this, 'init'), 0);
        }

        private function init_hook()
        {
            add_action('admin_menu', array($this, 'init_admin_menu'));
            add_filter('tiny_mce_before_init', array($this, 'myformatTinyMCE'));
            add_filter('admin_init', 'my_general_settings_register_fields');
        }

        public function my_general_settings_register_fields()
        {
            register_setting('general', 'my_field', 'esc_attr');
            add_settings_field('my_field', '<label for="my_field">' . __('My Field', 'my_field') . '</label>', 'my_general_settings_fields_html', 'general');
        }

        public function my_general_settings_fields_html()
        {
            $value = get_option('my_field', '');
            echo '<input type="text" id="my_field" name="my_field" value="' . $value . '" />';
        }

        public function myformatTinyMCE($in)
        {

            $in['wordpress_adv_hidden'] = true;

            return $in;
        }

        private function register_hook()
        {
            //add_action( 'am_ajax_portal', array($this, 'doAjaxAction') );
        }

        public function init()
        {
            $this->register_hook();
            $this->init_hook();
        }

        public function doAjaxAction()
        {
            $_Param = null;
            if (empty($_POST)) {
                $_Param = $_GET;
            } else {
                $_Param = $_POST;
            }

            $requestClass = isset($_SERVER['HTTP_U_CLASS']) ? $_SERVER['HTTP_U_CLASS'] : null;
            $requestType = isset($_SERVER['HTTP_U_TYPE']) ? $_SERVER['HTTP_U_TYPE'] : null;
            $requestMethod = isset($_SERVER['HTTP_U_METHOD']) ? $_SERVER['HTTP_U_METHOD'] : null;

            echo($requestClass);
            echo($requestType);
            echo($requestMethod);

            if ($requestClass != null && $requestType != null && $requestMethod != null) {
                $ajaxClass = using('plugin.itportal.Business.' . $requestClass . '.' . $requestType);
                $result = call_user_func(array($ajaxClass, $requestMethod), $_Param);
                echo json_encode($result);
            } else {
                echo 'method not found';
            }
        }

        public function init_admin_menu()
        {

            $this->adminMenu = apply_filters('init_itportal_admin_menu', $this->adminMenu);

            foreach ($this->adminMenu as $menuItem) {
                add_menu_page(
                    $menuItem['pageTitle'],
                    $menuItem['menuTitle'],
                    $menuItem['capability'],
                    $menuItem['menuSlug'],
                    $menuItem['handler'],
                    $menuItem['icon']
                );

                if (isset($menuItem['child'])) {
                    foreach ($menuItem['child'] as $subMenu) {
                        if (isset($subMenu['default']) && $subMenu['default'] == true) {
                            $subMenu['menuSlug'] = $menuItem['menuSlug'];
                            $subMenu['handler'] = null;
                        }
                        add_submenu_page(
                            $menuItem['menuSlug'],
                            $subMenu['pageTitle'],
                            $subMenu['menuTitle'],
                            $subMenu['capability'],
                            $subMenu['menuSlug'],
                            $subMenu['handler']
                        );
                    }
                }
            }
        }
    }
}