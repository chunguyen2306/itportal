<?php
if (class_exists('am_error_reporting_delegate') == false){
    import('plugin.itportal.Factory.AMRoleManagementFactory');
    import('theme.package.class.AMAsyncTask');

    class am_error_reporting_delegate{
        public $subject;
        public $requestData;

        public function __construct() {
            $this->subject = '[IT Portal] Ủy quyền không thành công';
        }

        public function send(RequestData $requestData, $errorStep){
            $this->subject = $requestData;

            $to = $errorStep->AssignedOwner . '@vng.com.vn';

            $wfDisplayName = @$this->requestData->workflow->DisplayName;
            $delegatedDomain = $errorStep->Owner;

            $roleID = @$errorStep->definition->Role + 0;
            $fac = new AMRoleManagementFactory();
            $roleData = $fac->getByID($roleID);
            $roleName  = $roleData ? $roleData->Name : '';
            $message = "<div style=\"font-family: Calibri, Helvetica, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255); margin-top: 0px; margin-bottom: 0px;\"><font face=\"Times New Roman,serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\">Chào bạn,</span></font><font face=\"Times New Roman,serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\"><br>Ủy quyền duyệt đơn hang tự động cho&nbsp;</span></font><font face=\"Times New Roman,serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\"><b>$delegatedDomain</b></span></font><font face=\"Times New Roman,serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\">&nbsp;gặp lỗi với đơn hàng&nbsp;</span></font><font face=\"Times New Roman,serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\"><b>$wfDisplayName</b></span></font><font face=\"Times New Roman,serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\">&nbsp;#12323. Nguyên nhân có thể do&nbsp;</span></font><font face=\"Times New Roman,serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\"><b>$delegatedDomain</b></span></font><font face=\"Times New Roman,serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\">&nbsp;không có quyền&nbsp;</span></font><font face=\"Times New Roman,serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\"><b>$roleName</b></span></font><font face=\"Times New Roman,serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\">.</span></font><font face=\"Times New Roman,serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\"><b>&nbsp;</b></span></font><font face=\"Times New Roman,serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\">Bạn vui long truy cập</span></font><font face=\"Arial,sans-serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\">&nbsp;</span></font><a href=\"http://itportal:8080/user-page/order/list-order/\" target=\"_blank\" rel=\"noopener noreferrer\" data-auth=\"NotApplicable\"><font face=\"Arial,sans-serif\" size=\"4\"><span style=\"font-size: 15.5pt;\"><font size=\"3\" color=\"blue\"><span style=\"font-size: 12pt;\">IT Portal</span></font></span></font></a><font face=\"Arial,sans-serif\" size=\"4\" color=\"black\"><span style=\"font-size: 15.5pt;\">&nbsp;</span></font><font face=\"Arial,sans-serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\">để kiểm tra và xử lý đơn hàng hoặc liên hệ IT Helpdesk để được hỗ trợ.</span></font></div><div style=\"font-family: Calibri, Helvetica, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255); margin-top: 0px; margin-bottom: 0px;\"><font face=\"Arial,sans-serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\"><br></span></font></div><div style=\"font-family: Calibri, Helvetica, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255); margin-top: 0px; margin-bottom: 0px;\"><font face=\"Arial,sans-serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\"><br></span></font></div><div style=\"font-family: Calibri, Helvetica, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255); margin-top: 0px; margin-bottom: 0px;\"><font face=\"Arial,sans-serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\"><b>Phòng CNTT</b></span></font><font face=\"Arial,sans-serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\"><br>Email và Teams chat:&nbsp;</span></font><a href=\"mailto:helpdesk@vng.com.vn\" target=\"_blank\" rel=\"noopener noreferrer\" data-auth=\"NotApplicable\"><font face=\"Arial,sans-serif\" size=\"3\"><span style=\"font-size: 12pt;\"><font color=\"blue\">helpdesk@vng.com.vn</font><font color=\"blue\"><br></font></span></font></a><font face=\"Arial,sans-serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\">Zalo:&nbsp;+84934088588</span></font><font face=\"Arial,sans-serif\" size=\"3\" color=\"black\"><span style=\"font-size: 12pt;\"><br>IT Portal:&nbsp;</span></font><a href=\"https://it.vng.com.vn/\" target=\"_blank\" rel=\"noopener noreferrer\" data-auth=\"NotApplicable\"><font face=\"Arial,sans-serif\" size=\"3\"><span style=\"font-size: 12pt;\"><font color=\"blue\">https://it.vng.com.vn</font></span></font></a></div>";

            if(defined('devmode'))
                $to = 'tiennv6@vng.com.vn';

            $task = new AMAsyncTask([
                action=>'mail',
                data=> [
                    $to,
                    $this->subject,
                    $message
                ]
            ]);
            $task->RunTask();
        }
    }
}

return new am_error_reporting_delegate();