<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMUserFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');
    import("plugin.itportal.API.CMDB.CMDB");

    class AMUserFactory extends AbstractDatabase {

        public static $tableName = 'am_users';

        protected function tableInfo(){
            return array(
                'table_name' => AMUserFactory::$tableName
            );
        }

        protected function searchDomain($keyword){
            $tableName = AMUserFactory::$tableName;
            $result = $this->custom_query2(
                "SELECT domainAcount, firstName, lastName FROM $tableName WHERE domainAcount LIKE '@keyword%' LIMIT 7",
                ['@keyword' => $keyword]
            );

            if($result == false)
                return $result;

            $re = [];
            foreach ($result as $item){
                $user = new stdClass();
                $user->Domain = $item->domainAcount;

                preg_match('/\d+$/', $user->Domain, $matches);
                $number = $matches ? " ($matches[0])" : "";

                $user->Name = $item->firstName . ". " . $item->lastName . $number;
            }
        }

        public function GetUserInformationByDomain($domain){
            $cmdb = new CMDB();
            $re = $cmdb->get(
                "CMDB2",
                ['m', 'username', 'startindex', 'limit'],
                ['GetUserInformation', $domain, 0, 10]
            );

            $userInfo = am_json_decode($re);
            if($userInfo == null){
                AMRespone::dbFailed($domain, 'Call CMDB.GetUserInformation failed');
                return null;
            }
            if($userInfo){
                $userInfo->JobTitle = $userInfo->Title;
                $userInfo->Site = $userInfo->UserPosition->Site;
                $userInfo->Department = $userInfo->Department ? $userInfo->Department : $userInfo->Deparment;

                unset($userInfo->UserPosition);
                unset($userInfo->Title);
                unset($userInfo->Deparment);
            }

            return $userInfo;
        }

        public function GetReportingLineDomain($empDomain){
            $cmdb = new CMDB();
            $re = $cmdb->get(
                "CMDBEX",
                ['m', 'empDomain'],
                ['GetReportingLine', $empDomain]
            );

            return json_decode($re);
        }
    }
}
?>