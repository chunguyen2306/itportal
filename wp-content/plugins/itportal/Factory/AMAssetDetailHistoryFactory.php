<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMAssetDetailHistoryFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMAssetDetailHistoryFactory extends AbstractDatabase {

        public static $tableName = 'amAssetDetailHistory';

        protected function tableInfo(){
            return array(
                'table_name' => AMAssetDetailHistoryFactory::$tableName
            );
        }
    }
}
?>