<?php

import('theme.package.Abstracts.AbstractDatabase');

class AMRequestDelegateFactory extends AbstractDatabase {

    public static $tableName = 'am_request_delegate';

    protected function tableInfo() {
        return array(
            'table_name' => AMRequestDelegateFactory::$tableName,
            'columns' => [
                'Domain',
                'DelegatedDomain',
                'StartTime',
                'EndTime',
                'delegateMode',
            ]
        );
    }

    public function getByDomain($domain){
        $results = $this->query([
            'filter' => [
                ["Domain = '$domain'"]
            ]
        ]);

        if($results === false)
            return $results;

        return sizeof($results) > 0 ? $results[0] : null;
    }
}
