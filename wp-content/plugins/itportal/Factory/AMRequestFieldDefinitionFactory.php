<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMRequestFieldDefinitionFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestFieldDefinitionFactory extends AbstractDatabase {

        public static $tableName = 'am_request_field_definition';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestFieldDefinitionFactory::$tableName,
                "columns" => array(
                    "ID",
                    "Name",
                    "DisplayName",
                    "MappingName",
                    "RequestDataTypeID",
                    "RequestDefinitionID",
                    "ParentID",
                    "Config",
                    "Constrain",
                )
            );
        }

        public function getListByRequestDefinitionID($requestDefinitionID){
            return $this->query(
                array('filter' => array(array("RequestDefinitionID = '$requestDefinitionID'", '')))
            );
        }
    }
}
?>