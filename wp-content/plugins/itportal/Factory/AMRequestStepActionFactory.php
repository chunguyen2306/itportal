<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMRequestStepActionFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestStepActionFactory extends AbstractDatabase {

        public static $tableName = 'am_request_step_actions';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestStepActionFactory::$tableName,
                'columns' => [
                    "ID",
                    "Name",
                    "Config",
                    "IsRollback",
                    "IsApproval",
                    "RequestStepDefinitionID",
                    "IsFrontRequired"
                ],
            );
        }

        public function getListByStepDefinitionID($stepID, $eventID = null, $isFrontEndRequired = null){

            $filter = null;
            if($eventID !== null){
                $filter = [
                    ["RequestStepDefinitionID = '$stepID'", 'AND'],
                    ["EventID = $eventID"]
                ];
            }else{
                $filter = [["RequestStepDefinitionID = '$stepID'", '']];
            }

            if($isFrontEndRequired !== null){
                $filter[] = ["IsFrontRequired = '$isFrontEndRequired'", ''];
            }

            $re = $this->query(['filter' => $filter]);
            if($re == false)
                return $re;

            foreach ($re as $action){;
                $action->cf = json_decode($action->Config);
                if($action->cf === null){
                    logAM("Parse step action config failed");
                    return false;
                }

                unset($action->Config);
            }

            return $re;
        }
    }
}
?>