<?php
/**
 * Created by PhpStorm.
 * User: lacha
 * Date: 19/4/2018
 * Time: 2:34 PM
 */

if (!class_exists('AMPageContentFactory')) {
    import('theme.package.Abstracts.AbstractDatabase');

    class AMPageContentFactory extends AbstractDatabase {
        public static $table_name = 'am_page_content';
        public function tableInfo(){
            return array(
                'table_name' => AMPageContentFactory::$table_name
            );
        }

        public function getValuePageContent($name, $page) {
            return $this->query(array(
                'select' => 'name, value',
                'filter' => [
                    ["name='$name'", 'AND'],
                    ["page='$page'", ''],
                ]
            ));

        }


    }
}