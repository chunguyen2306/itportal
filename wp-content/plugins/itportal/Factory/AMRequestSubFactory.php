<?php

if(!class_exists('AMRequestSubFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestSubFactory extends AbstractDatabase {

        public static $tableName = 'am_request_sub';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestStepDefinitionFactory::$tableName,
                'columns' => [
                    "ID",
                    "StepDefinitionID",
                    "RequestDefinitionID",
                    "FieldMapping",
                    "Condition",
                ]
            );
        }

        public function getListByRequestDefinitionID($requestDefinitionID){
            return $this->query(
                array('filter' => array(array("RequestDefinitionID = '$requestDefinitionID'", '')))
            );
        }
    }
}
?>