<?php
/**
 * Created by PhpStorm.
 * User: AnLac
 * Date: 26/9/2018
 * Time: 1:48 PM
 */
if (!class_exists('AMFeatureRoleMappingFactory')) {
    import('theme.package.Abstracts.AbstractDatabase');

    class AMFeatureRoleMappingFactory extends AbstractDatabase
    {
        public static $table_name = 'am_role_feature_mapping';

        public function tableInfo()
        {
            return array(
                'table_name' => AMFeatureRoleMappingFactory::$table_name,
                'table_columns' => array(
                    'ID' => 'Role Feature Code',
                    'RoleID' => 'Role Code',
                    'FeatureID' => 'Feature Code'
                ),
                'table_columns_type' => array(
                    'ID' => 'int',
                    'RoleID' => 'int',
                    'FeatureID' => 'int'
                )
            );
        }
    }
}
?>