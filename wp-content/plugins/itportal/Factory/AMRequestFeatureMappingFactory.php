<?php
if(!class_exists('AMRequestFeatureMappingFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestFeatureMappingFactory extends AbstractDatabase {

        public static $table_name = 'am_request_feature_mapping';

        public function tableInfo(){
            return array(
                'table_name' => AMRequestFeatureMappingFactory::$table_name
            );
        }
        public function getByID($id){
            return $this->getOneBy(array(
                "ID" => $id
            ));
        }

        public function getByName($name){
            return $this->getOneBy(array(
                "Name" => $name
            ));
        }
    }
}
?>