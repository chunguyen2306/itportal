<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMRequestDefinitionModelGroupFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestDefinitionModelGroupFactory extends AbstractDatabase {

        public static $tableName = 'am_request_definition_model_group';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestDefinitionModelGroupFactory::$tableName
            );
        }
    }
}
?>