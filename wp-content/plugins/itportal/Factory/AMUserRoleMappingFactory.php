<?php
if(!class_exists('AMUserRoleMappingFactory')) {
    import('theme.package.Abstracts.AbstractDatabase');
    class AMUserRoleMappingFactory extends AbstractDatabase {

        public static $table_name = 'am_user_role_mapping';

        public function tableInfo(){
            return array(
                'table_name' => AMUserRoleMappingFactory::$table_name
            );
        }
    }
}
?>