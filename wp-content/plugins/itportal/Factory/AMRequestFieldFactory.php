<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMRequestFieldFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestFieldFactory extends AbstractDatabase {

        public static $tableName = 'am_request_fields';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestFieldFactory::$tableName,
                "columns" => array(
                    "ID",
                    "RequestID",
                    "Field0",
                    "Field1",
                    "Field2",
                    "Field3",
                    "Field4",
                    "Field5",
                    "Field6",
                    "Field7",
                    "Field8",
                    "Field9",
                    "Field10",
                    "Field11",
                    "Field12",
                    "Field13",
                    "Field14",
                ),
                "data_columns" =>[
                    "Field0",
                    "Field1",
                    "Field2",
                    "Field3",
                    "Field4",
                    "Field5",
                    "Field6",
                    "Field7",
                    "Field8",
                    "Field9",
                    "Field10",
                    "Field11",
                    "Field12",
                    "Field13",
                    "Field14"
                ]
            );
        }

//        public function getByRequestID($requestID){
//
//            $tableInfo = $this->tableInfo();
//            $dataColumns = $tableInfo["data_columns"];
//            $tableName = $tableInfo["table_name"];
//
//            $query = "SELECT "
//                . join($dataColumns, ", ")
//                . " FROM $tableName"
//                . " WHERE RequestID = $requestID";
//
//            $result = $this->query($query);
//            if(count($result) != 1)
//                return null;
//
//            $re  = new stdClass();
//            $n = sizeof($dataColumns);
//            for($i = 0; $i < $n; $i++){
//                $re->{$dataColumns[$i]}  = $result[$i];
//            }
//
//            return $re;
//        }
    }
}
?>