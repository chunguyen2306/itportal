<?php
if(!class_exists('AMMainMenuFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMMainMenuFactory extends AbstractDatabase {

        public static $table_name = 'am_main_menu';

        public function tableInfo(){
            return array(
                'table_name' => AMMainMenuFactory::$table_name
            );
        }
        public function getByID($id){
            return $this->getOneBy(array(
                "ID" => $id
            ));
        }

        public function getByName($name){
            return $this->getOneBy(array(
                "Name" => $name
            ));
        }
    }
}
?>