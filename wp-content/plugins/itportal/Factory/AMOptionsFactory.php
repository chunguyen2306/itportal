<?php
if(!class_exists('AMOptionsFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMOptionsFactory extends AbstractDatabase {

        public static $table_name = 'am_options';

        public function tableInfo(){
            return array(
                'table_name' => AMOptionsFactory::$table_name
            );
        }
        public function getByID($id){
            return $this->getOneBy(array(
                "ID" => $id
            ));
        }

        public function getByName($name){
            return $this->getOneBy(array(
                "OptionName" => $name
            ));
        }

        public function getByOptionCode($code){
            return $this->getBy(array(
                "OptionCode" => $code
            ));
        }
    }
}
?>