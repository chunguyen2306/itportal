<?php
if(!class_exists('AMWPPostFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMWPPostFactory extends AbstractDatabase {

        public static $table_name = 'wp_posts';

        public function tableInfo(){
            return array(
                'table_name' => AMWPPostFactory::$table_name
            );
        }

    }
}
?>