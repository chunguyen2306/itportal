<?php
if(!class_exists('AMRequestSearchConditionFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestSearchConditionFactory extends AbstractDatabase {

        public static $table_name = 'am_request_search_condition';

        public function tableInfo(){
            return array(
                'table_name' => AMRequestSearchConditionFactory::$table_name
            );
        }
    }
}
?>