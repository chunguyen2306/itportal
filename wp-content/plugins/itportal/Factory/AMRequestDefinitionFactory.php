<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMRequestDefinitionFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestDefinitionFactory extends AbstractDatabase {

        public static $tableName = 'am_request_definition';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestDefinitionFactory::$tableName
            );
        }

        public function GetByListIDOrName($list){
            $result = $this->query([
                'filter'=>[
                    ["ID in ('".implode('\',\'', $list)."')", "OR"],
                    ["Name in ('".implode('\',\'', $list)."')", ""]
                ]
            ]);
            return $result;
        }

        public function GetAll(){
            return $this->query([]);
        }

        public function getDefines(){
            $workflows = $this->custom_query2('SELECT ID, Name, DisplayName FROM am_request_definition WHERE IsDelete=b\'0\'');
            if($workflows == false)
                return $workflows;

            foreach ($workflows as $wf){
                // query data
                $fields = $this->custom_query2(
                    'SELECT ID, Name, DisplayName, ParentID, RequestDataTypeID FROM am_request_field_definition WHERE RequestDefinitionID = ' .
                    $wf->ID
                );
                if($fields == false)
                    continue;

                $steps = $this->custom_query2(
                    'SELECT Name, DisplayName, StepIndex, Role FROM am_request_step_definitions WHERE RequestDefinitionID = ' . $wf->ID
                );
                if($steps == false)
                    continue;

                $wf->fields = $fields;
                $wf->steps = $steps;
            }
            return $workflows;
        }
    }
}
?>