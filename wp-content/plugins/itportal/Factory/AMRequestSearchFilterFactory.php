<?php
if(!class_exists('AMRequestSearchFilterFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestSearchFilterFactory extends AbstractDatabase {

        public static $table_name = 'am_request_search_filter';

        public function tableInfo(){
            return array(
                'table_name' => AMRequestSearchFilterFactory::$table_name
            );
        }

        public function query($agr = array(), $log = false) {
            $re = parent::query($agr, $log);
            if($re){
                foreach ($re as $item){
                    $item->style = am_json_decode($item->Style);
                    unset($item->Style);
                }
            }

            return $re;
        }

        public function insert(&$data, $isLog = false) {
            $insertData = $data;
            $insertData['Style'] = am_json_decode($insertData['style']);
            unset($insertData['style']);

            $data['ID'] = $insertData['ID'];
            return parent::insert($insertData, $isLog);
        }
    }
}
?>

