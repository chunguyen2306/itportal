<?php
if(!class_exists('AMAssetModelReviewFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMAssetModelReviewFactory extends AbstractDatabase {

        public static $table_name = 'am_asset_model_review';

        public function tableInfo(){
            return array(
                'table_name' => AMAssetModelReviewFactory::$table_name
            );
        }
        public function getByID($id){
            return $this->getOneBy(array(
                "ID" => $id
            ));
        }

        public function getByModelID($id){
            return $this->query(array(
               'filter' => array(
                   array('ModelID = '.$id, '')
               )
            ));
        }
        public function getByAssetName($PARAM){
            $AssetName = $PARAM['AssetName'];
            $result = $this->query(array(
                'filter' => array(
                    array("AssetName = '$AssetName'", 'AND'),
                    array('ModelID = 0','' )
                )
            ));
            return $result;
        }
    }
}
?>