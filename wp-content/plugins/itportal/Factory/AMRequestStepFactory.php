<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMRequestStepFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestStepFactory extends AbstractDatabase {

        public static $tableName = 'am_request_steps';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestStepFactory::$tableName,
            );
        }

    }
}
?>