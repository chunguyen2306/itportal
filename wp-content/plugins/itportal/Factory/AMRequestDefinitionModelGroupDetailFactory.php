<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMRequestDefinitionModelGroupDetailFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestDefinitionModelGroupDetailFactory extends AbstractDatabase {

        public static $tableName = 'am_request_definition_model_group_detail';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestDefinitionModelGroupDetailFactory::$tableName
            );
        }

        public  function getByGroupID($groupID){
            global $wpdb;

            $tableName = AMRequestDefinitionModelGroupDetailFactory::$tableName;
            $sql = $wpdb->prepare("select ModelName from $tableName where GroupID = %d", $groupID);
            return $wpdb->get_col($sql);
        }

        public function getByModelNames($modelNames){
            global $wpdb;

            $arrayIn = [];
            foreach ($modelNames as $item){
                $arrayIn[] = $wpdb->_escape($item);
            }

            $tableName = AMRequestDefinitionModelGroupDetailFactory::$tableName;
            $sql = "select * from $tableName where ModelName IN @array";
            return $this->custom_query2($sql, ['@array' => $arrayIn]);
        }
    }
}
?>