<?php
if (!class_exists('AMRequestNotificationFactory')) {
    import('theme.package.Abstracts.AbstractDatabase');

    class AMRequestNotificationFactory extends AbstractDatabase
    {

        public static $table_name = 'am_request_notification';
        const LIMIT_ITEM_PREVIEW = 5;

        public function tableInfo()
        {
            return array('table_name' => AMRequestNotificationFactory::$table_name);
        }

        public function getListForPreview($domain)
        {
            $updateData = [
                'IsSeen' => 1
            ];
            $this->update($updateData, ["Domain" => $domain]);

            global $wpdb;
            $prepare = $wpdb->prepare(
                'SELECT * FROM am_request_notification WHERE IsDelete=0 AND Domain=\'%s\' ORDER BY ID DESC LIMIT 5',
                $domain);
            return $wpdb->get_results($prepare);
        }

        public function getList($domain, $isRead, $start, $count)
        {
            global $wpdb;
            $query = 'SELECT * FROM am_request_notification WHERE IsDelete=0 AND Domain=\'%s\'';
            $prepare = null;
            if ($start == null) {
                if ($isRead == null) {
                    $query = $query . 'ORDER BY ID DESC';
                    $prepare = $wpdb->prepare($query, $domain);
                } else {
                    $query = $query . ' AND IsRead=%d ORDER BY ID DESC';
                    $prepare = $wpdb->prepare($query, $domain, $isRead);
                }
            } else {
                if ($isRead == null) {
                    $query = $query . 'ORDER BY ID DESC';
                    if ($count) {
                        $query = $query . ' LIMIT %d, %d';
                        $prepare = $wpdb->prepare($query, $domain, $start, $count);
                    } else {
                        $query = $query . ' LIMIT %d';
                        $prepare = $wpdb->prepare($query, $domain, $start);
                    }
                } else {
                    $query = $query . ' AND IsRead=%d ORDER BY ID DESC';
                    if ($count) {
                        $query = $query . ' LIMIT %d, %d';
                        $prepare = $wpdb->prepare($query, $domain, $isRead, $start, $count);
                    } else {
                        $query = $query . ' LIMIT %d';
                        $prepare = $wpdb->prepare($query, $domain, $isRead, $start);
                    }
                }
            }
            return $this->custom_query2($prepare, null);
        }

        public function getQuantity($domain, $isRead)
        {
            global $wpdb;
            $query = 'SELECT * FROM am_request_notification WHERE IsDelete=0 AND Domain=\'%s\'';
            if ($isRead == null) {
                $prepare = $wpdb->prepare($query, $domain);
            } else {
                $query = $query . ' AND IsRead=%d';
                $prepare = $wpdb->prepare($query, $domain, $isRead);
            }
            return count($this->custom_query2($prepare));
        }

        public function maskAsRead($domain, $ids, $isRead)
        {
            foreach ($ids as $id) {
                $data = [
                    'Domain' => $domain,
                    'IsRead' => $isRead
                ];

                if ($this->update($data, ['ID' => $id]) === false)
                    return false;
            }

            return true;
        }

        public function maskAsSeen($domain, $ids)
        {
            foreach ($ids as $id) {
                $data = [
                    'Domain' => $domain,
                    'IsSeen' => 1
                ];

                if ($this->update($data, ['ID' => $id]) === false)
                    return false;
            }

            return true;
        }

        public function countUnseen($domain)
        {
            $re = $this->query(['filter' => [
                ["Domain = '$domain'", 'AND'],
                ["IsSeen = 0"],]
            ]);

            if (is_array($re))
                return sizeof($re);
            else
                return $re;
        }

        public function deleteNotification($domain, $ids)
        {
            foreach ($ids as $id) {
                $data = [
                    'Domain' => $domain,
                    'IsDelete' => 1
                ];
                if ($this->update($data, ['ID' => $id]) === false)
                    return false;
            }

            return true;
        }
    }
}
?>
























