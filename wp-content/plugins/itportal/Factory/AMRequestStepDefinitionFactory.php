<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMRequestStepDefinitionFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestStepDefinitionFactory extends AbstractDatabase {

        public static $tableName = 'am_request_step_definitions';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestStepDefinitionFactory::$tableName,
                'columns' => [
                    "ID",
                    "StepIndex",
                    "Name",
                    "DisplayName",
                    "RequestDefinitionID",
                    "Role",
                ],
            );
        }

        public function getListByRequestDefinitionID($requestDefinitionID){
            return $this->query(
                array('filter' => array(array("RequestDefinitionID = '$requestDefinitionID'", '')))
            );
        }
    }
}
?>