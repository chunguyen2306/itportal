<?php
/**
 * Created by PhpStorm.
 * User: CPU10722-local
 * Date: 6/9/2017
 * Time: 11:36 AM
 */
if(!class_exists('AMRequestCategoryFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestCategoryFactory extends AbstractDatabase {

        public static $tableName = 'am_request_categories';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestCategoryFactory::$tableName
            );
        }
    }
}
?>