<?php
/**
 * Created by PhpStorm.
 * User: CPU10722-local
 * Date: 6/12/2017
 * Time: 5:01 PM
 */
if(!class_exists('AMRequestTableData')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestTableData extends AbstractDatabase {

        public static $tableName = 'am_request_table_data';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestTableData::$tableName
            );
        }
    }
}
?>