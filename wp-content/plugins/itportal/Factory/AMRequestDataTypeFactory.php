<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMRequestDataTypeFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestDataTypeFactory extends AbstractDatabase {

        public static $tableName = 'am_request_data_types';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestDataTypeFactory::$tableName
            );
        }

    }
}
?>