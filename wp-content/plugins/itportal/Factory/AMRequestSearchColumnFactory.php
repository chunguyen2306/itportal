<?php
if(!class_exists('AMRequestSearchColumnFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');
    import("plugin.itportal.Business.Search.RequestSearchTypeID");

    class AMRequestSearchColumnFactory extends AbstractDatabase {

        public static $table_name = 'am_request_search_column';

        public function tableInfo(){
            return array(
                'table_name' => AMRequestSearchColumnFactory::$table_name
            );
        }

        public function query($agr = array(), $log = false) {
            $re = parent::query($agr, $log);
            if($re == false)
                return $re;

            foreach ($re as $item){
                $item->src = am_json_decode($item->Source);
                $item->style = am_json_decode($item->Style);
                unset($item->Source);
                unset($item->Style);
            }

            return $re;
        }

        public function insert(&$data, $isLog = false) {
            $insertData = $data;
            unset($insertData["src"]);
            $insertData["Source"] = am_json_encode($data["src"]);
            unset($insertData["style"]);
            $insertData["Style"] = am_json_encode($data["style"]);

            return parent::insert($insertData, $isLog);
        }
    }
}
?>