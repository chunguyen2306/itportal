<?php
if (!class_exists('AMRoleFeatureFactory')) {
    import('theme.package.Abstracts.AbstractDatabase');
    import('plugin.itportal.Factory.AMRoleFeatureFactory');
    import('plugin.itportal.Factory.AMFeatureRoleMappingFactory');

    class AMRoleFeatureFactory extends AbstractDatabase
    {

        public static $table_name = 'am_features';
        private $featureRoleMappingFac;

        public function tableInfo()
        {
            return array(
                'table_name' => AMRoleFeatureFactory::$table_name,
                'table_columns' => array(
                    'ID' => 'Feature code',
                    'FeatureName' => 'Feature name',
                    'DisplayName' => 'Feature display name',
                    'Description' => 'Feature description'
                ),
                'table_columns_type' => array(
                    'ID' => 'int',
                    'FeatureName' => 'string',
                    'DisplayName' => 'string',
                    'Description' => 'string'
                )
            );
        }

        function __construct()
        {
            $this->featureRoleMappingFac = new AMFeatureRoleMappingFactory();
        }

        public function checkFeaturePermissionByRoleIDs($featureName, $roles)
        {
            $featureID = (string)$this->getOneBy(array(
                "FeatureName" => $featureName
            ))->ID;
            foreach ($roles as $roleItem) {
                $featureIDs = (string)$this->featureRoleMappingFac->getOneBy(array(
                    "RoleID" => $roleItem
                ))->FeatureID;
                if ($featureIDs == '' || $featureID == '') {
                    continue;
                }

                if (strpos($featureIDs, $featureID) !== false) {
                    return true;
                }
            }
            return false;
        }

    }
}
?>