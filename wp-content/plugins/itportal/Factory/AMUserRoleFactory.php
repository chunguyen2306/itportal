<?php
if(!class_exists('AMUserRoleFactory')) {
    import('theme.package.Abstracts.AbstractDatabase');
    class AMUserRoleFactory extends AbstractDatabase {

        public static $table_name = 'am_user_role';

        public function tableInfo(){
            return array(
                'table_name' => AMUserRoleFactory::$table_name,
                'table_columns' => array(
                    'ID' => 'User Code',
                    'Domain' => 'User Domain',
                    'Name' => 'User Name',
                    'RoleID' => 'User Role Code',
                    'empCode' => 'Employee Code'
                ),
                'table_columns_type' => array(
                    'ID' => 'int',
                    'Domain' => 'String',
                    'Name' => 'String',
                    'RoleID' => 'int',
                    'empCode' => 'string'
                )
            );
        }
    }
}
?>