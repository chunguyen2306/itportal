<?php
if(!class_exists('AMAssetCostVariableFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMAssetCostVariableFactory extends AbstractDatabase {

        public static $table_name = 'am_assetcostvariables';

        public function tableInfo(){
            return array(
                'table_name' => AMAssetCostVariableFactory::$table_name
            );
        }
        public function getByID($id){
            return $this->getOneBy(array(
                "ID" => $id
            ));
        }

        public function getByName($name){
            return $this->getOneBy(array(
                "Name" => $name
            ));
        }
    }
}
?>