<?php
if(!class_exists('AMMenuTypeFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMMenuTypeFactory extends AbstractDatabase {

        public static $table_name = 'am_menu_type';

        public function tableInfo(){
            return array(
                'table_name' => AMMenuTypeFactory::$table_name
            );
        }
        public function getByID($id){
            return $this->getOneBy(array(
                "ID" => $id
            ));
        }

        public function getByName($name){
            return $this->getOneBy(array(
                "Name" => $name
            ));
        }
    }
}
?>