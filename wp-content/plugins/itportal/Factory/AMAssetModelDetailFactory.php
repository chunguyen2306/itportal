<?php
if(!class_exists('AMAssetModelDetailFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMAssetModelDetailFactory extends AbstractDatabase {

        public static $table_name = 'am_asset_model_detail';

        public function tableInfo(){
            return array(
                'table_name' => AMAssetModelDetailFactory::$table_name
            );
        }
        public function getByID($id){
            return $this->getOneBy(array(
                "ID" => $id
            ));
        }

        public function getByModelID($modelID){
            return $this->getOneBy(array(
                "ModelID" => $modelID
            ));
        }

        public function getThubmails($modelID){
            return $this->getOneBy(array(
                "ModelID" => $modelID
            ))->Thubmails;
        }

        public function getThubmailsAndShortDescription($modelID){
            $re = $this->custom_query2(
                "SELECT Thubmails, ShortDescription FROM am_asset_model_detail WHERE ModelID = @modelID",
                ['@modelID' => @$modelID]
            );
            return sizeof($re) > 0 ? $re[0] : false;
        }
    }
}
?>