<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 3/24/2017
 * Time: 4:02 PM
 */
if(!class_exists('AMReportManagementFactory')) {
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMReportManagementFactory extends AbstractDatabase {

        public static $table_name = 'am_report';

        public function tableInfo(){
            return array(
                'table_name' => AMReportManagementFactory::$table_name,
                'table_columns' => array(
                    'ID' => 'Report Code',
                    'reportTitle' => 'Report Title',
                    'reportType' => 'Report Type',
                    'reportGroup' => 'Report Group',
                    'reportData' => 'Report Query',
                    'Description' => 'Description',
                    'createBy' => 'Create By',
                    'createTime' => 'Create Time'
                ),
                'table_columns_type' => array(
                    'ID' => 'int',
                    'reportTitle' => 'string',
                    'reportType' => 'string',
                    'reportGroup' => 'string',
                    'reportData' => 'string',
                    'Description' => 'string',
                    'createBy' => 'string',
                    'createTime' => 'string'
                )
            );
        }
    }
}