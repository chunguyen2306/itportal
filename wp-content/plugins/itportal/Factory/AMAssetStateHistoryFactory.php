<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMAssetStateHistoryFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMAssetStateHistoryFactory extends AbstractDatabase {

        public static $tableName = 'amassetstatehistory';

        protected function tableInfo(){
            return array(
                'table_name' => AMAssetStateHistoryFactory::$tableName
            );
        }
    }
}
?>