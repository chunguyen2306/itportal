<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMHelpdeskConsoleLogFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMHelpdeskConsoleLogFactory extends AbstractDatabase {

        public static $tableName = 'am_helpdeskconsole_log';

        protected function tableInfo(){
            return array(
                'table_name' => AMHelpdeskConsoleLogFactory::$tableName
            );
        }
    }
}
?>