<?php
if(!class_exists('AMRequestSearchFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestSearchFactory extends AbstractDatabase {

        public static $table_name = 'am_request_search';

        public function tableInfo(){
            return array(
                'table_name' => AMRequestSearchFactory::$table_name
            );
        }
    }
}
?>