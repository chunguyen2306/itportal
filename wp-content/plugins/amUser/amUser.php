<?php
/*
  Plugin Name: AM User
  Plugin URI:
  Description: Chứa tất cả các chức năng quản lý user.
  Version: 1.0
  Author: tientm2
  Author URI:
  License:
  Text Domain: am-usersdf
*/
if(!class_exists('AMUser')){

    function do_scheduleUser($type){
        global $amUserScheduleManager;
        $amUserScheduleManager->run($type);
    }

    function add_cron_schedulesUser( $schedules ) {
        // add a 'weekly' schedule to the existing set
        $schedules['minute'] = array(
            'interval' => 60,
            'display' => __('Once Minute')
        );
        return $schedules;
    }

    function active_amuser_plugin(){
        $settings = using('plugin.amUser.class.includes.schedules.ScheduleSetting');
        foreach ($settings['sync_schedule'] as $name => $setting) {
            if (!wp_get_schedule($name)) {
                wp_schedule_event(strtotime($setting['time']), $setting['type'], $name, array($setting['name']));
            }
        }
    }


    if ( ! function_exists( 'import' ) ) {
        require_once get_template_directory() . '/package/import.php';
    }

    import ('plugin.amUser.class.controller.AMUserSyncSetting');
    import ('plugin.amUser.class.controller.AmUserInfo');
    import ('theme.package.AMPlugin');
    import ('theme.package.Abstracts.AbstractPlugin');

    class AMUser extends AbstractPlugin {

        public $db = array();
        public $model = array();

        public function __construct() {
            $this->ajaxName = 'amUser';
            $this->pluginName = 'amUser';
            $this->adminMenu = array(
                array(
                    'pageTitle' => 'Người dùng',
                    'menuTitle' => 'Người dùng',
                    'capability'=> 'manage_options',
                    'menuSlug' => 'am_user',
                    'handler' => array(new AMUserInfo(array(
                        'slug' => 'am_user'
                    )), 'index'),
                    'icon' => plugin_dir_url(__FILE__) . 'user-icon.png',
                    'child' => array(
                        array(
                            'pageTitle' => 'Sync Setting',
                            'menuTitle' => 'Sync Setting',
                            'capability'=> 'manage_options',
                            'menuSlug' => 'sync_setting',
                            'handler' => array(new AMUserSyncSetting(array(
                                'slug' => 'sync_setting'
                            )), 'index')
                        ),
                    )
                )
            );

            parent::__construct();
        }

        function add_admin_menu(){
            /*add_menu_page(
                'AM Users',
                'AM Users',
                'manage_options',
                'am_users',
                array(new AMUserInfo(array(
                    'slug' => 'am_users'
                )), 'index'),
                plugin_dir_url(__FILE__) . 'user-icon.png'
            );
            add_submenu_page(
                    'am_users',
                    'User Info',
                    'User Info',
                    'manage_options',
                    'am_users'
                );
            add_submenu_page(
                    'am_users',
                    'Sync Setting',
                    'Sync Setting',
                    'manage_options',
                    'sync_setting',
                    array(new AMUserSyncSetting(array(
                        'slug' => 'sync_setting'
                    )), 'index')
                );*/
        }

        public function init_custom_hook(){}
        public function custom_init(){}
    }
}

$amUser = new AMUser();
$GLOBALS['amUser'] = $amUser;
$amUserScheduleManager = using('plugin.amUser.class.includes.schedules.ScheduleManager');
$GLOBALS['amUserScheduleManager'] = $amUserScheduleManager;

$settings = using('plugin.amUser.class.includes.schedules.ScheduleSetting');

foreach ($settings['sync_schedule'] as $name => $setting) {
    if (!has_action($name)) {
        add_action($name, 'do_scheduleUser');
    }
}
add_filter( 'cron_schedules',  'add_cron_schedulesUser');

function deactive_amuser_plugin(){
    $settings = using('plugin.amUser.class.includes.schedules.ScheduleSetting');
    foreach ($settings['sync_schedule'] as $name => $setting) {
        wp_clear_scheduled_hook($name);
    }
}

register_activation_hook(__FILE__, 'active_amuser_plugin');
register_deactivation_hook(__FILE__, 'deactive_amuser_plugin');

