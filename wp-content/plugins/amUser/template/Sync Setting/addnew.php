<?php
    $typeName = '';
    $title = '';
    $syncType = $parent->get_sync_type();

    if(isset($model['isUpdate'])){
        $typeName = '&type=update&id='.$_REQUEST['id'];
        $title = 'Update Sync Setting';

    } else {
        $typeName = '&type=new';
        $title = 'Add New Sync Setting';
    }

    $syncSetting = $parent->get_sync_setting();
?>
<div class="wrap">
    <h1 class="am-header"><?php echo $title; ?> <a href="<?php echo esc_url('?page='.$model['slug']); ?>" class="page-title-action" title="Cancel" >Cancel</a></h1>
    <form class="form-horizontal" method="POST" action="<?php echo esc_url('?page='.$model['slug'].$typeName); ?>">
        <?php if(isset($model['isUpdate'])) { ?>
           <input type="hidden" class="form-control" name="F-ID"
                    value="<?php echo $model['syncItem']->ID; ?>" />
        <?php } ?>
        <div class="form-group col-sm-12">
            <div class="col-sm-7">
                <input placeholder="Enter name here" style="font-size: 30px;" type="text" class="control-full" name="F-Name"
                value="<?php if(isset($model['isUpdate'])) {
                    echo $model['syncItem']->Name;
                } else {
                    if(isset($_POST['F-Name'])) {
                        echo $_POST['F-Name'];
                    }
                } ?>" />
            </div>
            <div class="col-sm-5">
                <select onchange="this.form.submit()" class="control-full" name="F-Class">
                    <option value="null">- Select an action -</option>
                    <?php foreach( $parent->get_sync_class() as $class ) {
                        if($class != '.' && $class != '..'){
                            $class = $parent->getClassName($class);
                            if(isset($_POST['F-Class']) && $_POST['F-Class'] == $class) {
                                echo '<option selected value="'.$class.'">'.$class.'</option>';
                            } else if(isset($model['isUpdate']) && $model['syncItem']->Class == $class){
                                echo '<option selected value="'.$class.'">'.$class.'</option>';
                            } else {
                                echo '<option value="'.$class.'">'.$class.'</option>';
                            }
                        }
                    }?>
                </select>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <div class="col-sm-7">
                <select class="control-full" name="F-Type">
                    <option value="null">- Select a schedule -</option>
                    <?php foreach ($syncSetting as $name => $setting) {
                        if(isset($_POST['F-Type']) && $_POST['F-Type'] == $name) {
                            echo '<option selected value="'.$setting['name'].'">'.$setting['displayName'].'</option>';
                        } else if(isset($model['isUpdate']) && $model['syncItem']->Type == $name){
                            echo '<option selected value="'.$setting['name'].'">'.$setting['displayName'].'</option>';
                        } else {
                            echo '<option value="'.$setting['name'].'">'.$setting['displayName'].'</option>';
                        }
                    } ?>
                </select>
            </div>
            <div class="col-sm-5" style="position: absolute; right: 0;">
                <?php
                    $className = '';
                    if(isset($model['isUpdate'])) {
                        $className = $model['syncItem']->Class;
                    } else if(isset($_POST['F-Class'])){
                        $className = $_POST['F-Class'];
                    }
                    if($className != '') {
                        $info = $parent->getSyncInfo($className);
                        if ($info != null) {
                            echo $info->document;
                        }
                    }
                ?>
            </div>
        </div>
        <div class="form-group  col-sm-7">
            <div class="col-sm-12">
                <?php if(isset($model['isUpdate'])) { ?>
                <input type="submit" class="button button-primary" name="btnAction" value="Update" />
                <?php } else { ?>
                <input type="submit" class="button button-primary" name="btnAction" value="Add new" />
                <input type="submit" class="button" name="btnAction" value="Reset" />
                <?php } ?>
            </div>
        </div>
    </form>
</div>