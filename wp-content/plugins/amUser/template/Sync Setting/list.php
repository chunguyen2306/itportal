<?php
    $syncSetting = $parent->get_sync_setting();
    var_dump(wp_get_schedule('am_daily_5h'));
 ?>
<div class="wrap">

    <h1>User Sync Setting <a href="<?php echo esc_url('?page='.$model['slug'].'&type=new'); ?>" class="page-title-action" title="Add new sync schedule" >Add New</a></h1>

    <table class="wp-list-table widefat fixed striped pages">
    	<thead>
    	    <tr>
    	        <td id="cb" class="manage-column column-cb check-column">
                    <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                    <input id="cb-select-all-1" type="checkbox">
    	        </td>
    	        <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="">
                        <span>Name</span>
                        <span class="sorting-indicator"></span>
                    </a>
    	        </th>
    	        <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="">
                        <span>Type</span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="">
                        <span>Status</span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                </th>
    	    </tr>
    	</thead>
        <tbody>
            <?php
                if($model['listSetting'] == null) { ?>
                    <tr><td colspan="6">Not found any matched record</td></tr>
                <?php }else{
                foreach($model['listSetting'] as $settingItem){ ?>
                <tr>
                    <th id="cb" class="manage-column column-cb check-column">
                        <label class="screen-reader-text" for="cb-select-<?php echo $settingItem->ID; ?>"><?php echo $settingItem->ID; ?></label>
                        <input id="cb-select-<?php echo $settingItem->ID; ?>" type="checkbox">
                    </th>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <a href="<?php echo '?page='.$model['slug'].'&type=update&id='.$settingItem->ID; ?>">
                            <span><?php echo $settingItem->Name; ?></span>
                        </a>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <a href="">
                            <span><?php echo $syncSetting[$settingItem->Type]['displayName']; ?></span>
                        </a>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <form method="POST" action="<?php echo esc_url('/wp-admin/admin.php?page='.$model['slug'].'&action=apply'); ?>">
                            <input type="hidden" name="ID" id="ID" value="<?php echo $settingItem->ID; ?>" />
                            <?php if($settingItem->Status == 0) { ?>
                            <button title="Apply sync setting" type="submit" name="btnAction" value="active" class="btn btn-flat danger"/>
                               <span class="glyphicon glyphicon-off"></span>
                            </button>
                            <?php } else { ?>
                            <button title="Apply sync setting" type="submit" name="btnAction" value="deactive" class="btn btn-flat success"/>
                                <span class="glyphicon glyphicon-ok"></span>
                            </button>
                            <?php } ?>
                        </form>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <form method="POST" action="<?php echo esc_url('/wp-admin/admin.php?page='.$model['slug'].'&type=del'); ?>">
                            <input type="hidden" name="ID" id="ID" value="<?php echo $settingItem->ID; ?>" />
                            <button title="Delete sync setting" type="submit" class="btn btn-flat danger"><span class="glyphicon glyphicon-trash"></span></button>
                        </form>
                        <?php if($settingItem->Status != 0) { ?>
                            <form method="POST" action="<?php echo esc_url('/wp-admin/admin.php?page='.$model['slug'].'&action=runmanual'); ?>">
                                <input type="hidden" name="ID" id="ID" value="<?php echo $settingItem->ID; ?>" />
                                <button title="Run manual" type="submit" class="btn btn-flat success"><span class="glyphicon glyphicon-play"></span></button>
                            </form>
                        <?php } ?>
                    </td>
                </tr>
                <?php } }
            ?>
        </tbody>
    	<tfoot>
    	    <tr>
                <td id="cb" class="manage-column column-cb check-column">
                    <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                    <input id="cb-select-all-1" type="checkbox">
                </td>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="">
                        <span>Name</span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="">
                        <span>Type</span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="">
                        <span>Status</span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                </th>
            </tr>
    	</tfoot>

    </table>
</div>
