<?php

?>
<div class="wrap">
    <h1>User Info</h1>

    <table class="wp-list-table widefat fixed striped pages">
        <thead>
        <tr>
            <td id="cb" class="manage-column column-cb check-column">
                <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                <input id="cb-select-all-1" type="checkbox">
            </td>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Employee Code</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Domain Name</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Full Name</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Department</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th style="width: 150px;" scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <form class="table-form-search" method="post" action="/wp-admin/admin.php?page=<?php echo $model['slug']; ?>">
                    <input type="text" name="keyword" placeholder="Account Name" />
                    <input type="submit" value="Find" name="btnAction" />
                </form>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(is_array($model['users']) && sizeof($model['users']) > 0){
        foreach ($model['users'] as $user){ ?>
        <tr>
            <th id="cb" class="manage-column column-cb check-column">
                <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                <input id="cb-select-all-1" type="checkbox">
            </th>
            <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <?php echo $user->empCode; ?>
            </td>
            <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <?php echo $user->domainAccount; ?>
            </td>
            <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <?php echo $user->fullName; ?>
            </td>
            <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <?php echo $user->departmentName; ?>
            </td>
            <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
            </td>
        </tr>
        <?php }
        } else { ?>
            <tr>
                <td colspan="6">
                    Not any matched user for '<?php echo $_POST['keyword']; ?>'
                </td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div>