<?php
if ( ! function_exists( 'import' ) ) {
    require_once get_template_directory() . '/package/import.php';
}

import('plugin.amRolePermission.factory.AMUserRoleFactory');

if (!class_exists('AMUserAjax')) {
    class AMUserAjax{


        public function GetCurrentUser(){
            try{
                //Get current user
                $currentUser = wp_get_current_user();
                if($currentUser == 0 || $currentUser->user_login == false)
                    return AMRespone::notFound();

                $username = $currentUser->user_login;
                $result = (new AMUserRoleFactory())->query([
                    "filter" => [["Domain = '$username'", ""]]
                ]);
                if($result == false)
                    return AMRespone::dbFailed($username, "Get user role info");

                if(is_array($result) != 1)
                    return AMRespone::busFailed($username, "Data is weirdo");

                return new AMRespone($result[0]);
            }catch (Exception $ex){
                return AMRespone::exception(null, $ex);
            }
        }
    }
}
return new AMUserAjax();

