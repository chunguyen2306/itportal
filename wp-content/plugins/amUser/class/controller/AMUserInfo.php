<?php
/**
 * Created by Administrator on 3/9/2017
 */

if(!class_exists('AMUserInfo')){
    //import your package here
    import ('theme.package.Abstracts.AbstractController');
    import ('plugin.amUser.class.factory.AMUserFactory');

    class AMUserInfo extends AbstractController {
        protected function init(){
            $this->db = new AMUserFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__)-strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'User Info/list',
                'update' => 'User Info/addnew',
                'delete' => 'User Info/list',
                'new' => 'User Info/addnew',
            );
        }

        private function pagging($pageSize, $pageNum, $filter){
        $query = array();
        if($filter != null){
            $query['filter'] = array(
                array(
                    "domainAccount like '%".$filter."%'", ""
                )
            );
        }
        $query['limit'] = array(
            'at' => $pageSize*($pageNum-1),
            'length' => $pageSize,
        );
        $users = $this->db->query($query);
        if(!is_array($users)){
            $users = array();
        }
        $this->_model('users', $users);
    }

        public function list_action($req, $post){
            //Do something here
            $pageIndex = 1;
            $limit = 20;
            $query = null;
            if(isset($req['pageIndex'])){
                $pageIndex = $req['pageIndex'];
            }
            if(isset($req['limit'])){
                $limit = $req['limit'];
            }
            if(isset($post['keyword']) && isset($post['btnAction'])){
                if($post['btnAction'] == 'Find' && $post['keyword'] != ''){
                    $query = $post['keyword'];
                }
            }
            $this->pagging($limit, $pageIndex, $query);
        }

        public function update_action($req, $post){
            //Do something here

        }

        public function delete_action($req, $post){
            //Do something here

        }

        public function new_action($req, $post){
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}
?>