<?php
/**
 * Created by Administrator on 3/9/2017
 */

if(!class_exists('AMUserSyncSetting')){
    //import your package here
    import ('theme.package.Abstracts.AbstractController');
    import ('plugin.amUser.class.factory.AMUserSyncSettingFactory');

    class AMUserSyncSetting extends AbstractController {

        protected function init($args){
            $this->db = new AMUserSyncSettingFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__)-strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'Sync Setting/list',
                'update' => 'Sync Setting/addnew',
                'delete' => 'Sync Setting/list',
                'new' => 'Sync Setting/addnew',
            );
        }

        private function get_all(){
            $listSetting = $this->db->query(array());
            $this->_model('listSetting', $listSetting);
        }

        public function list_action($req, $post){
            //Do something here
            $this->get_all();
        }

        private function applyProcess($id, $status){
            $ScheduleManager = using ('plugin.amUser.class.includes.schedules.ScheduleManager');
            $syncItem = $this->db->getOne(array(
                'filter' => array(
                    array( "ID = ".$id, '' )
                )
            ));

            $syncName = str_replace(' ','',$syncItem->Name);

            if($status == 0){
                $ScheduleManager->removeSchedule($syncItem->Type);
            } else {
                $ScheduleManager->createSchedule($syncItem->Type, $syncItem->Class);
            }
        }

        public function do_apply($req, $post){
            $id = $req['ID'];
            $newStatus = 0;
            $actionType = $post['btnAction'];

            if($actionType == 'active'){
                $newStatus = 1;
            } else {
                $newStatus = 0;
            }

            $this->db->update(array(
                'Status' => $newStatus
            ), array(
                'ID' => $id
            ));

            $this->get_all();
        }

        public function do_runmanual($req, $post)
        {
            global $amUserScheduleManager;
            $id = $req['ID'];
            $syncSetting = $this->db->getOne(array(
                'filter' => array(
                    array("ID = " . $id, "")
                )
            ));
            if ($syncSetting->Status != 0) {

                $amUserScheduleManager->run_one($syncSetting);
            }
            $this->get_all();
        }

        public function update_action($req, $post){
            $id = $req['id'];
            $action = $post['btnAction'];
            $this->_model('isUpdate',true);

            if($action == 'Update'){
                $newData = array(
                    'Name' => $post['F-Name'],
                    'Time' => $post['F-Date'].' '.$post['F-Time'],
                    'Type' => $post['F-Type'],
                    'Status' => 0,
                    'Class' => $post['F-Class']
                );
                $updateResult = $this->db->update($newData, array(
                    "ID" => $id
                ));
                $this->applyProcess($id, $newData['Status']);
                wp_redirect( $this->_getPath() );
                exit();
            } else {
                if(isset($id)){
                    $syncItem = $this->db->query(array(
                        'filter' => array(
                            array( "ID = ".$id, '')
                        )
                    ))[0];
                    $this->model['syncItem'] = $syncItem;
                }
            }
        }

        public function new_action($req, $post){
            $syncSetting = using('plugin.amUser.class.includes.schedules.ScheduleSetting');
            if($post['F-Status'] == '' && $post['F-Status'] == null){
                $post['F-Status'] = 0;
            }

            if($post['F-Date'] == '' && $post['F-Date'] == null){
                $post['F-Date'] = $syncSetting[$post['F-Type']]['time'];
            }

            $newData = array(
                'Name' => $post['F-Name'],
                'Time' => $post['F-Date'],
                'Type' => $post['F-Type'],
                'Status' => $post['F-Status'],
                'Class' => $post['F-Class']
            );

            $action = $post['btnAction'];
            if($action == 'Add new'){
                $this->db->insert($newData);
                wp_redirect( $this->_getPath() );
                exit();
            } else if($action == 'Reset'){
                $post['F-Name'] = '';
                $post['F-Time'] = '';
                $post['F-Type'] = '';
                $post['F-Status'] = '';
                $post['F-Class'] = '';
            }
        }

        public function del_action($req, $post){
            $id = $req['ID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }

        public function get_sync_type(){
            global $AMPlugin;
           return include( $AMPlugin->getPluginDir().'/amUser/class/i18n/en/SyncSettingTypeList.php' );
        }

        public function get_sync_setting(){
            return using ('plugin.amUser.class.includes.schedules.ScheduleSetting');
        }

        public function get_sync_class(){
            global $AMPlugin;
            return scandir( $AMPlugin->getPluginDir().'/amUser/class/includes/schedules/items' );
        }

        public function getClassName($fileName){
            return substr($fileName, 0, strpos($fileName, '.'));
        }

        public function getSyncInfo($path){
            if($path != 'null'){
                global $AMPlugin;
                $info = new StdClass();
                $info->className = $path;
                $classPath = $AMPlugin->getPluginDir().'/amUser/class/includes/schedules/items/'.$path.'.php';
                $classFile = fopen($classPath, "r");
                $fileContent = fread($classFile, filesize($classPath));

                $pattern = "/((?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:\/\/.*))/";
                $matches = array();
                preg_match_all($pattern, $fileContent, $matches);
                $info->document = $matches[1][0];

                $info->document = preg_replace('/\n/', '<br>', $info->document);

                fclose($classFile);
                return $info;
            } else {
                return null;
            }
        }

        public function get_display_time_with_sync_type($time, $type){
            $timeFormat = array(
                'daily' => 'h:i A',
                'weekly' => 'l',
                'hourly' => 'h:i A',
                'monthly' => 'F d',
                'yearly' => 'd/m/Y'
            );

            return date($timeFormat[$type], strtotime($time));
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}
?>