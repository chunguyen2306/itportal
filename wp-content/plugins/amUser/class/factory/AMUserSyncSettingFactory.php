<?php
/**
 * Created by Administrator on 3/9/2017
 */

if(!class_exists('AMUserSyncSettingFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMUserSyncSettingFactory extends AbstractDatabase {

        public static $tableName = 'am_user_sync_setting';

        protected function tableInfo(){
            return array(
                'table_name' => AMUserSyncSettingFactory::$tableName
            );
        }
    }
}
?>