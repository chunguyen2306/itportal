<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMUserFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMUserFactory extends AbstractDatabase {

        public static $tableName = 'am_users';

        protected function tableInfo(){
            return array(
                'table_name' => AMUserFactory::$tableName
            );
        }

        public function GetUserInformationByDomain($domain){

            $cmdb = new CMDB();
            $re = $cmdb->get(
                'CMDBEX',
                ['m', 'domain'],
                ['GetUserInformation', $domain]
            );


            $userInfo = am_json_decode($re);
            if($userInfo){
                $userInfo->JobTitle = $userInfo->Title;
                $userInfo->Site = $userInfo->UserPosition->Site;
                $userInfo->Department = $userInfo->Department ? $userInfo->Department : $userInfo->Deparment;
            }

            return $userInfo;
        }

        public function GetReportingLineDomain($empDomain){

            $cmdb = new CMDB();
            $re = $cmdb->get(
                "CMDBEX",
                ['m', 'empDomain'],
                ['GetReportingLine', $empDomain]
            );

            return json_decode($re);
        }
    }
}
?>