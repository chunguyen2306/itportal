<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/14/2017
 * Time: 2:16 PM
 */
    return  array(
        'sync_schedule' => array(
            'am_daily_5h' => array(
                'name' => 'am_daily_5h',
                'time' => '2017-01-01 5:00:00',
                'displayName' => 'Daily schedule at 5:00 AM',
                'type' => 'daily'
            ),
            'am_daily_12h' => array(
                'name' => 'am_daily_12h',
                'time' => '2017-01-01 12:00:00',
                'displayName' => 'Daily schedule at 12:00 PM',
                'type' => 'daily'
            ),
            'am_daily_20h' => array(
                'name' => 'am_daily_20h',
                'time' => '2017-01-01 20:00:00',
                'displayName' => 'Daily schedule at 8:00 PM',
                'type' => 'daily'
            ),
            'am_daily_23h' => array(
                'name' => 'am_daily_23h',
                'time' => '2017-01-01 23:00:00',
                'displayName' => 'Daily schedule at 11:00 PM',
                'type' => 'daily'
            )
        ),
        'sync_url' => 'http://esb.itdev.vng.com.vn',
        'sync_certificate' => array(
            'username' => 'misesb',
            'password' => 'Vng@123'
        ),
        'sync_methods' => array(
            'getEmployeeByCriteria' => '/MISESB/http/mis/getEmployeeByCriteria'
        ),
        'sync_params' => array(
            'PARAM_FROM' => 'from',
            'PARAM_TO' => 'to',
            'PARAM_REQUESTER' => 'requester',
            'PARAM_STATUS' => 'status',
            'PARAM_ACTIVE' => 'active',
            'PARAM_PAGESIZE' => 'pageSize',
            'PARAM_PAGENUM' => 'pageNum',
        ),
        'sync_values' => array(
            'REQUESTER' => 'SeatManagement',
        )
    )
?>