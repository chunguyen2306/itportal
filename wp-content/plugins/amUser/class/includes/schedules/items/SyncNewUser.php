<?php
/**
 * <b>Author:</b> TienTM2
 * <b>Description:</b> Cập nhật thông tin nhân viên mới, thông báo và tạo biên bản
 * <b>Process:</b>
 * [1] ITAM3 Wordpress
 * [2] vHRS
 * [3] ITAM3 Wordpress
 * [4] ITAM3 DB
 */

if(!class_exists('SyncNewUser')){
    //import your package here
    import('theme.package.HttpRequest');

    class SyncNewUser{
        //Your code here
        public function run(){
            $syncSetting = using('plugin.amUser.class.includes.schedules.ScheduleSetting');
            if(date_default_timezone_get() == 'UTC'){
                date_default_timezone_set('Asia/Bangkok');
            }
            $currDate = new DateTime();
            $intervalDate = date_add(new DateTime(), date_interval_create_from_date_string('7 days'));

            $request = new HttpRequest(array(
                'url' => $syncSetting['sync_url'].$syncSetting['sync_methods']['getEmployeeByCriteria'],
                'certificate' => $syncSetting['sync_certificate'],
                'data' => array(
                    'status' => 'NEW',
                    'from' => date('Y-d-m', $currDate->getTimestamp()),
                    'to' => date('Y-d-m', $intervalDate->getTimestamp()),
                    'requester' => $syncSetting['sync_values']['REQUESTER']
                )
            ));

            $response = $request->send();

            $emps = json_decode($response->reponseText);
            error_log(sizeof($emps));
        }
    }
}
return new SyncNewUser();
?>