<?php
/**
 * <b>Author:</b> TienTM2
 * <b>Description:</b> Cập nhật thông tin nhân viên nghỉ việc tạo biên bản, thông báo các bên liên quan
 * <b>Process:</b>
 * [1] ITAM3 Wordpress
 * [2] vHRS
 * [3] ITAM3 Wordpress
 * [4] ITAM3 DB
 */

if(!class_exists('SyncOffWorkUser')){
    //import your package here
    import('theme.package.HttpRequest');
    import ('plugin.amUser.class.factory.AMUserFactory');
    
    class SyncOffWorkUser{
        //Your code here
        public function run(){
            $db = new AMUserFactory();
            $syncSetting = using('plugin.amUser.class.includes.schedules.ScheduleSetting');
            if(date_default_timezone_get() == 'UTC'){
                date_default_timezone_set('Asia/Bangkok');
            }
            $currDate = new DateTime();
            $intervalDate = date_add(new DateTime(), date_interval_create_from_date_string('7 days'));

            $request = new HttpRequest(array(
                'url' => $syncSetting['sync_url'].$syncSetting['sync_methods']['getEmployeeByCriteria'],
                'certificate' => $syncSetting['sync_certificate'],
                'data' => array(
                    'status' => 'EXIT',
                    'from' => date('Y-d-m', $currDate->getTimestamp()),
                    'to' => date('Y-d-m', $intervalDate->getTimestamp()),
                    'requester' => $syncSetting['sync_values']['REQUESTER']
                )
            ));

            $response = $request->send();

            $emps = json_decode($response->reponseText);

            foreach($emps as $emp){
                $existed = $db->getOne(array(
                    'filter' => array(
                        array( "domainAccount = '$emp->domainAccount'", '' )
                    )
                ));

                if($existed != null){
                    $result = $db->delete(array(
                        'domainAccount' => $emp->domainAccount
                    ));
                }
            }

            error_log(sizeof($emps));
        }
    }
}

return new SyncOffWorkUser();
?>