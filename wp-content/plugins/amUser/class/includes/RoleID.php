<?php
if (!class_exists('RoleID')) {
    class RoleID{
        public static $deptHead = 7;
        public static $user = 2;
        public static $helpDesk = 3;
        public static $storeKeeper = 4;
        public static $technicians = 10;
        public static $admin = 1;
    }
}