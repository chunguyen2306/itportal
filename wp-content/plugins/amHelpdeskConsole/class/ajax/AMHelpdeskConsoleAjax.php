<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if(!class_exists('AMHelpdeskConsoleAjax')){

    class AMHelpdeskConsoleAjax{

        private function callAPIGateway($data){
            $url = 'http://10.60.35.35:8002/graphapi/';
            $user = 'hd_console';
            $pass = 'd9UdfWrg4-e5';
            $auth = array (
                'user' => $user,
                'pass' => $pass
            );

            $request = new HttpRequest(array(
                'url' => $url,
                'header' => array(
                    'auth' => json_encode($auth)
                ),
                'data' => $data
            ));

            $response = $request->send();
            echo $response->reponseText;
            exit();
        }

        public function change_user_license($_PARAM){
            $account = $_PARAM['account'];
            $licenses  = $_PARAM['licenses'];
            $this->callAPIGateway(array(
                'account' => $account,
                'licenses' => $licenses
            ));
        }

        public function create_service_mail($_PARAM){
            $accountEnabled = $_PARAM['accountEnabled'];
            $displayName  = $_PARAM['displayName'];
            $mailNickname = $_PARAM['mailNickname'];
            $userPrincipalName = $_PARAM['userPrincipalName'];
            $owner = $_PARAM['owner'];
            $this->callAPIGateway(array(
                'accountEnabled' => $accountEnabled,
                'displayName' => $displayName,
                'mailNickname' => $mailNickname,
                'userPrincipalName' => $userPrincipalName,
                'owner' => $owner
            ));
        }

        public function get_user($_PARAM){
            $account = $_PARAM['account'];
            $this->callAPIGateway(array(
                'account' => $account
            ));
        }

    }

    return new AMHelpdeskConsoleAjax();
}