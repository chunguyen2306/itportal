<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if(!class_exists('GroupAjax')){

	if ( ! function_exists( 'import' ) ) {
        require_once get_template_directory() . '/package/import.php';
    }
	
	import('plugin.amHelpdeskConsole.class.ajax.AbstractAjax');

    class GroupAjax extends AbstractAjax{

        public function get_group_by_id($_PARAM){
            $id = $_PARAM['id'];
            $this->callAPIGateway('get_group_by_id',array(
                'id' => $id
            ));
        }
		
		public function get_group_by_name($_PARAM){
            $name = $_PARAM['groupName'];
            $this->callAPIGateway('get_group_by_name',array(
                'name' => $name
            ));
        }
		
		public function get_group_owner($_PARAM){
            $id = $_PARAM['id'];
            $this->callAPIGateway('get_group_owner',array(
                'id' => $id
            ));
        }
		
		public function update_group($_PARAM){
            $id = $_PARAM['id'];
			$data = $_PARAM['data'];
			
			$updateData = array(
				'id' => $id
			);

            $updateData = array_merge($updateData, $data);
			
            $this->callAPIGateway('update_group',$updateData);
        }
		
		public function update_group_owner($_PARAM){
            $id = $_PARAM['id'];
			$owner = $_PARAM['owner'];
			
            $this->callAPIGateway('update_group_owner',array(
				'id' => $id,
				'owner' => $owner
			));
        }

    }

    return new GroupAjax();
}