<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:46 AM
 */
if(!class_exists('UserAjax')){

	if ( ! function_exists( 'import' ) ) {
        require_once get_template_directory() . '/package/import.php';
    }
	
	import('plugin.amHelpdeskConsole.class.ajax.AbstractAjax');

    class UserAjax extends AbstractAjax{

        public function change_user_license($_PARAM){
            $account = $_PARAM['account'];
            $licenses  = $_PARAM['licenses'];

            //var_dump($licenses);

            $this->callAPIGateway('change_user_license',array(
                'account' => $account,
                'licenses' => $licenses
            ));
        }

        public function create_service_mail($_PARAM){
            $accountEnabled = $_PARAM['accountEnabled'];
            $displayName  = $_PARAM['displayName'];
            $mailNickname = $_PARAM['mailNickname'];
            $userPrincipalName = $_PARAM['userPrincipalName'];
            $owner = $_PARAM['owner'];
            $this->callAPIGateway('create_service_mail',array(
                'accountEnabled' => $accountEnabled,
                'displayName' => $displayName,
                'mailNickname' => $mailNickname,
                'userPrincipalName' => $userPrincipalName,
                'owner' => $owner
            ));
        }

        public function get_user($_PARAM){
            $account = $_PARAM['account'];
            $this->callAPIGateway('get_user',array(
                'account' => $account
            ));
        }
		
		public function get_user_manager($_PARAM){
            $account = $_PARAM['account'];
            $this->callAPIGateway('get_user_manager',array(
                'account' => $account
            ));
        }
		
		public function get_user_member_of($_PARAM){
            $account = $_PARAM['account'];
            $this->callAPIGateway('get_user_memberof',array(
                'account' => $account
            ));
        }
		
		public function reset_user_password($_PARAM){
            $account = $_PARAM['account'];
            $this->callAPIGateway('reset_user_password',array(
                'account' => $account
            ));
        }
		
		public function update_account_status($_PARAM){
            $account = $_PARAM['account'];
			$status = $_PARAM['status'];
            $this->callAPIGateway('update_account_status',array(
                'account' => $account,
				'status' => $status
            ));
        }
		
		public function update_service_mail_owner($_PARAM){
            $account = $_PARAM['account'];
			$owner = $_PARAM['owner'];
            $this->callAPIGateway('update_service_mail_owner',array(
                'account' => $account,
				'owner' => $owner
            ));
        }
		
		public function update_user($_PARAM){
            $account = $_PARAM['account'];
			$data = $_PARAM['data'];

			$updateData = array(
				'account' => $account
			);

            $updateData = array_merge($updateData, $data);

            $this->callAPIGateway('update_user',$updateData);
        }



		public function get_domains(){
            $this->callAPIGateway('get_domains',array());
        }

        public function get_licenses(){
            $this->callAPIGateway('get_licenses',array());
        }

    }

    return new UserAjax();
}