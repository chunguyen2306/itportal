<?php
/*
  Plugin Name: AM Helpdesk Console
  Plugin URI:
  Description: Quản lý thông tin account AD
  Version: 1.0
  Author: tientm2
  Author URI:
  License:
  Text Domain: am-helpdeskconsole
*/
if(!class_exists('AmHelpdeskConsole')) {

    if ( ! function_exists( 'import' ) ) {
        require_once get_template_directory() . '/package/import.php';
    }
    import ('theme.package.Abstracts.AbstractPlugin');

    class AmHelpdeskConsole extends AbstractPlugin{

        public function __construct()
        {
            $this->ajaxName = 'amHelpdeskConsole';
            $this->pluginName = 'amHelpdeskConsole';
            parent::__construct();
        }

        protected function init_custom_hook()
        {
            // TODO: Implement init_custom_hook() method.
            add_shortcode( 'am_helpdesk_console_page', array($this, '') );
        }

        protected function custom_init()
        {
            // TODO: Implement custom_init() method.
        }
    }

    $amHelpdeskConsole = new AmHelpdeskConsole();

}