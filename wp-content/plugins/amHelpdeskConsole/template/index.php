<?php
/**
 * Created by PhpStorm.
 * User: LAP10266-local
 * Date: 5/9/2017
 * Time: 1:50 PM
 */?>
<div class="section-helpdeskconsole">
    <div class="header">
        <div>
            <button>Tạo Tài Khoản</button>
            <button>Quản Lý Nhóm</button>
        </div>
        <input type="text" id="txtKeyword"/>
    </div>

    <div class="content">
        <div comtype="TabContainer" id="tabAccountInfo">
            <tabs>
                <a tab="tabcAccountInfo">Thông tin tài khoản</a>
                <a tab="tabcPersonInfo">Thông tin cá nhân</a>
            </tabs>
            <tabcontents>
                <div id="tabcAccountInfo" class="form">
                    <div class="form-line">
                        <span class="form-label">Ngày tạo</span>
                        <input type=""/>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Lần đăng nhập cuối</span>
                        <input type=""/>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Ngày hết hạn</span>
                        <input type=""/>
                        <button>Đặt lại</button>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Disable</span>
                        <input type=""/>
                        <button>Disable/Enable</button>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Lock</span>
                        <input type=""/>
                        <button>Lock/UnLock</button>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Mật khẩu hết hạn</span>
                        <input type=""/>
                        <button>Đặt lại</button>
                    </div>
                </div>
                <div id="tabcPersonInfo" class="form">
                    <div class="form-line">
                        <span class="form-label">Tên đầy đủ</span>
                        <input type=""/>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Mã nhân viên</span>
                        <input type=""/>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Chức vụ</span>
                        <input type=""/>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Phòng ban</span>
                        <input type=""/>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Bộ phận</span>
                        <input type=""/>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Mã nhân viên</span>
                        <input type=""/>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Số điện thoại</span>
                        <input type=""/>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Số nội bộ</span>
                        <input type=""/>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Quản lý</span>
                        <input type=""/>
                    </div>
                </div>
            </tabcontents>
        </div>
    </div>

    <div comtype="Popup" id="popCreateEmail">
        <div class="form">
            <div class="form-line">
                <span class="form-label">Địa chỉ mail</span>
                <input type=""/>
            </div>
            <div class="form-line">
                <span class="form-label">Tài khoản</span>
                <input type=""/>
            </div>
            <div class="form-line">
                <button>Đồng ý</button>
                <button>Đóng</button>
            </div>
        </div>
    </div>

    <div comtype="Popup" id="popGroupManager">
        <div class="form">
            <div class="form-line">
                <span class="form-label">Tên nhóm</span>
                <input type=""/>
            </div>
            <div class="form-line">
                <span class="form-label">Tài khoản sở hữu</span>
                <input type=""/>
            </div>
            <div class="form-line">
                <button>Đồng ý</button>
                <button>Đóng</button>
            </div>
        </div>
    </div>
</div>