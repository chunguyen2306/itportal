var txtKeyword = $('#txtKeyword');
var content = $('.content');
var comPopup = $('.popup');

txtKeyword.on('keypress', function(e){
	if(e.key === 'Enter'){
		showAccountInfo();
	}
});

var showAccountInfo = function(){
	content.show();
}

var tabComponents = $('*[comtype="TabContainer"]');

tabComponents.each(function(tabIndex, tab){
	var tab = $(tab);
	var tabHeaders = tab.find('tabs a');
	var tabContents = $(tab.find('tabcontents>div'));
	
	tabHeaders.each(function(headerIndex, header){
		header = $(header);
		var contentID = header.attr('tab');
		var isDefault = header.attr('default');

		header.on('click', function(){
			tabContents.hide();
			tabHeaders.removeClass('active');
			header.addClass('active');
			$('#'+contentID).show();
		});
		
		if(isDefault === undefined){
			$('#'+contentID).hide();
		} else {
			header.addClass('active');
		}
	});
});

var popups = $('button[popup]');
comPopup.find('.header a').on('click', function(){
	comPopup.hide();
	$('.blank').hide();
});
comPopup.find('.footer a.btnCancel').on('click', function(){
	comPopup.hide();
	$('.blank').hide();
})

popups.each(function (index, popup){
	popup = $(popup);
	popup.on('click', function(){
		
		var popupContent = $('#'+popup.attr('popup'));
		comPopup.find('.content').html(popupContent.html());
		comPopup.find('.header h3').text(popup.text());
		comPopup.show();
		$('.blank').show();
	});
});