<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 5:17 PM
 */
import('plugin.amLogin.libs.cas');

$prefix = 'https://';
$cas_host = get_option('casHost');
$cas_port = intval(get_option('casPort'));
$cas_context = get_option('casContext');
$casLoginURL = get_option('casLoginURL');
$casServiceValidateURL = get_option('casServiceValidateURL');
$casLogoutURL = get_option('casLogoutURL');
$casRedirectURL = get_option('casRedirect');

if($cas_host != '' and $cas_port != 0 and $casServiceValidateURL != '' and $casLogoutURL != '') {
	phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context);
	phpCAS::setServerServiceValidateURL($prefix.$cas_host.$casServiceValidateURL);
	phpCAS::setServerLogoutURL($prefix.$cas_host.$casLogoutURL);

	// if you want to set a cert, replace the line below
	phpCAS::setNoCasServerValidation();
	
	if (isset($_POST['logout'])) {
        if($casRedirectURL != '') {
            session_destroy();
            phpCas::logoutWithRedirectService($casRedirectURL);
        } else {
            phpCas::logout();
        }
	}
	
	if (isset($_POST['login'])) {
		//phpCAS::setFixedServiceURL ('portal.itdev.vng.com.vn');
		phpCAS::forceAuthentication();
	}

    if (phpCAS::isAuthenticated()) {
        $GLOBALS['userDomain'] = phpCAS::getUser();
    }
}