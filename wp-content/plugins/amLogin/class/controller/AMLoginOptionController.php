<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 1:16 PM
 */
if(!class_exists('AMLoginOptionController')){
    //import your package here
    import('theme.package.Abstracts.AbstractController');

    class AMLoginOptionController extends AbstractController
    {
        protected function init()
        {
            $this->sso_option_construct();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'SSO Option/list'
            );
        }

        //Khởi tạo và thêm các cài đặt vào wp_options trong database.
        private function sso_option_construct() {
            if(get_option('casHost') == null) add_option('casHost', '', '', 'yes') ;
            if(get_option('casPort') == null) add_option('casPort', '', '', 'yes');
            if(get_option('casContext') == null) add_option('casContext', '', '', 'yes');
            add_option('casLoginURL', '/login', '', 'yes');
            add_option('casServiceValidateURL', '/serviceValidate', '', 'yes');
            add_option('casLogoutURL', '/logout', '', 'yes');
            if(get_option('casRedirect') == null) add_option('casRedirect', '', 'yes');
        }

        public function list_action($req, $post) {
            $optionData = array(
                'casHost' => get_option('casHost'),
                'casPort' => get_option('casPort'),
                'casContext' => get_option('casContext'),
                'casRedirect' => get_option('casRedirect')
            );

            $this->model['ssoSetting'] = $optionData;

            $newData = array(
                'casHost' => $post['casHost'],
                'casPort' => $post['casPort'],
                'casContext' => $post['casContext'],
                'casLoginURL' => '/login',
                'casServiceValidateURL' => '/serviceValidate',
                'casLogoutURL' => '/logout',
                'casRedirect' => $post['casRedirect']
            );

            $action = $post['btnAction'];

            if($action == 'Save'){
                //update_option
                foreach ($newData as $key => $value) {
                    update_option($key, $value);
                }
                wp_redirect( $this->_getPath() );
                exit();
            }
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}