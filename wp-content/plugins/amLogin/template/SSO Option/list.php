<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 1:24 PM
 */
?>
<div class="wrap">
    <h1 style="margin-left: 15px;">SSO Server Setting</h1>
    <br/>
    <div class="col-sm-8">
        <form class="form-group" method="POST" action="<?php echo esc_url('?page='.$model['slug'].$typeName); ?>">
            <div class="form-group row">
                <label class="col-sm-2" for="casHost">CAS Host: </label>
                <div class="col-sm-10">
                    <input class="col-sm-10 form-control" type="text" placeholder="sso.vng.com.vn" name="casHost"
                           value="<?php if(isset($model['ssoSetting'])) echo $model['ssoSetting']['casHost']; ?>"
                           required />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2" for="">CAS Port: </label>
                <div class="col-sm-10">
                    <input class="form-control" type="number" placeholder="443" name="casPort"
                           value="<?php if(isset($model['ssoSetting'])) echo $model['ssoSetting']['casPort']; ?>"
                           required />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2">CAS Context: </label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" name="casContext"
                           value="<?php if(isset($model['ssoSetting'])) echo $model['ssoSetting']['casContext']; ?>"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2">Redirect URL: </label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" placeholder="http://example.com.vn" name="casRedirect"
                           value="<?php if(isset($model['ssoSetting'])) echo $model['ssoSetting']['casRedirect']; ?>"/>
                </div>
            </div>
            <div class="form-group row">
                <input type="submit" class="button button-primary" name="btnAction" value="Save" style="margin-left: 15px;"/>
            </div>
        </form>
    </div>
</div>