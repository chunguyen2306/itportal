<?php
/**
 * Created by PhpStorm.
 * User: CPU10339-local
 * Date: 16/03/2017
 * Time: 2:04 PM
 */
import('plugin.amLogin.class.includes.AMLoginClient');
?>
<div style = "margin-top: 15px;">
	<form class="form-group" method="POST" action="">
		<?php
		if (phpCAS::isAuthenticated()) {
			echo '<p style="color: #fff;">Hello '.phpCAS::getUser().' ';
			echo '<input type="hidden" name="logout" value="logout"/>
			<button type="submit" class="btn btn-warning">Logout</button>
			</p>';
		} else {
			echo '<input type="hidden" name="login" value="login"/>';
			echo '<button type="submit" class="btn btn-success">Login</button>';
		}
		?>
	</form>
</div>