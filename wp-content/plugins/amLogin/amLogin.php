<?php
/**
 * Plugin Name:       Login
 * Plugin URI:
 * Description:       Widget dùng để đăng nhập.
 * Author:            daopd
 * Version:           1.0
 * License:           GPL-2
 * Text Domain:       am-login
 */

if(!class_exists('AMLogin')){

    if ( ! function_exists( 'import' ) ) {
        require_once get_template_directory() . '/package/import.php';
    }

    import('plugin.amLogin.amLoginOption');
    import('plugin.amLogin.class.includes.AMLoginClient');

    /*class AMLogin extends WP_Widget {

        public function __construct() {
            $this->inithook();
            new amLoginOption();
            parent::__construct (
                'login_widget', // id của widget
                'Login Widget', // tên của widget
                array(
                    'description' => 'Đăng nhập' // mô tả
                )
            );
        }

        private function inithook() {
            add_action( 'widgets_init', array('AMLogin','create_login_widget'), 0);
        }

        function form( $instance ) {
            //Hiển thị form trong option của widget
            echo "<h1>Login Widget Integrate SSO</h1>";
        }

        function widget( $args, $instance ) {
            extract( $args );

            echo $before_widget;
            // Nội dung trong Widget.
            import('plugin.amLogin.template.Login.login');

            // Kết thúc nội dung trong widget
            echo $after_widget;
        }

        function create_login_widget() {
            register_widget('AMLogin');
        }
    }

    add_action( 'amLogin_checklogin', 'amLogin_checklogin', 1, 1);

    function amLogin_checklogin(){
        if (phpCAS::isAuthenticated()) {
            $GLOBALS['userDomain'] = phpCAS::getUser();
            //wp_redirect(site_url().$path);
            //exit();
        } else {
            phpCAS::forceAuthentication();
        }
    }*/

    class AMLogin extends AbstractPlugin
    {

        public function __construct()
        {
            $this->ajaxName = 'amLogin';
            $this->pluginName = 'amLogin';
            parent::__construct();
        }

        protected function init_custom_hook()
        {
            add_action( 'amLogin_checklogin', array(this, 'amLogin_checklogin'), 1, 1);
        }

        public function amLogin_checklogin()
        {
            if(!isset($GLOBALS['userDomain']) && $GLOBALS['userDomain'] == null){
                $current_url = $_SERVER['REQUEST_URI'];//"//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

                import('theme.Module.Login.index');

            } else {
                return;
            }
        }

        protected function custom_init()
        {
            // TODO: Implement custom_init() method.
        }
    }

    $amLogin = new AMLogin();
}