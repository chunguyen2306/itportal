<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 1:06 PM
 */
if(!class_exists('amLoginOption')) {
    if (!function_exists('import')) {
        require_once get_template_directory() . '/package/import.php';
    }

    //Report Class Controller
    import('plugin.amLogin.class.controller.AMLoginOptionController');


    class amLoginOption
    {
        public $db = array();
        public $model = array();

        public function __construct()
        {
            $this->init_hook();
        }

        private function init_hook()
        {
            add_action('init', array($this, 'init'), 0);
        }

        function add_admin_menu()
        {
            add_options_page(
                'SSO Server Options',
                'SSO Server Options',
                'manage_options',
                'am_sso_option',
                array(new AMLoginOptionController(array(
                    'slug' => 'am_sso_option'
                )), 'index')
            );
        }

        public function init()
        {
            add_action('admin_menu', array($this, 'add_admin_menu'));
        }
    }
}