<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 04/04/17
 * Time: 10:08 AM
 */
if(!class_exists('AMProductTypeController')) {
    import('theme.package.Abstracts.AbstractController');
    import('plugin.amProduct.class.factory.AMProductTypeFactory');

    class AMProductTypeController extends AbstractController
    {
        protected function init()
        {
            $this->db = new AMProductTypeFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'Product Type/list',
                'update' => 'Product Type/update',
                'sync' => 'Product Type/sync',
                'del' => 'Product Type/list'
            );
        }

        private function get_all($startIndex, $pageLength) {
            $productType = $this->db->query(array(
                'limit' => array(
                    'at' => $startIndex,
                    'length' => $pageLength
                )
            ));
            $this->model['productType'] = $productType;
        }

        private function getProductTypeByID($id) {
            return $this->db->getOne(array(
                'filter' => array(
                    array("ID = '$id'",'')
                )
            ));
        }

        public function list_action($req, $post) {
            if($req['p'] == null) {
                $pageIndex = 1;
            } else {
                $pageIndex = $req['p'];
            }
            $count = $this->db->query(array(
                'select' => 'count(*) as rows'
            ))[0];
            $total = intval($count->rows);
            $pageLength = 15;
            $startIndex = ($pageIndex-1) * $pageLength;

            $pageCount = ceil($total/$pageLength);
            $this->model['pageCount'] = $pageCount;

            if($pageIndex <= 1) {
                $previousIndex = 1;
            } else {
                $previousIndex = $pageIndex - 1;
            }
            $this->model['previousIndex'] = $previousIndex;
            if($pageIndex >= $pageCount) {
                $nextIndex = $pageCount;
            } else {
                $nextIndex = $pageIndex + 1;
            }
            $this->model['nextIndex'] = $nextIndex;
            $this->get_all($startIndex, $pageLength);
        }

        public function sync_action($req, $post) {
            $action = $post['btnAction'];

            if ($action == 'syncDataFromServer') {
                $productBusiness = using('plugin.amProduct.class.business.AMProductBusiness');
                $productBusiness->getProductByHttpRequest('type');
                $this->model['result'] = 'success';
            }
        }

        public function update_action($req, $post)
        {
            $id = $req['id'];
            $action = $post['btnAction'];
            $this->_model('isUpdate',true);

            if($action == 'Update') {
                $newData = array(
                    'ID' => $id,
                    'DisplayName' => $post['productTypeDisplayName'],
                    'IconURL' => $post['productTypeIconURL']
                );
                $this->db->update($newData, array(
                    "ID" => $id
                ));
                wp_redirect( $this->_getPath() );
                exit();
            } else {
                $productTypeItem = $this->getProductTypeByID($id);
                $this->model['productTypeItem'] = $productTypeItem;
            }
        }

        public function del_action($req, $post) {
            $id = $req['productTypeID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }
    }
}