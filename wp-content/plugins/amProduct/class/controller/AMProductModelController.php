<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 04/04/17
 * Time: 10:08 AM
 */
if(!class_exists('AMProductModelController')) {
    import('theme.package.Abstracts.AbstractController');
    import('plugin.amProduct.class.factory.AMProductModelFactory');
    import('plugin.amProduct.class.factory.AMProductTypeFactory');

    class AMProductModelController extends AbstractController
    {
        protected $amProductTypeFac = array();
        protected function init()
        {
            $this->db = new AMProductModelFactory();
            $this->amProductTypeFac = new AMProductTypeFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'Product Model/list',
                'update' => 'Product Model/update',
                'sync' => 'Product Model/sync',
                'del' => 'Product Model/list'
            );
        }

        private function get_all($startIndex, $pageLength) {
            $productModel = $this->db->query(array(
                'limit' => array(
                    'at' => $startIndex,
                    'length' => $pageLength
                )
            ));
            $this->model['productModel'] = $productModel;
        }

        private function getProductModelByID($id) {
            return $this->db->getOne(array(
                'filter' => array(
                    array("ID = '$id'",'')
                )
            ));
        }

        //use Global.
        public function getProductDataByQuery($filter) {
            return $this->db->query(array(
                'join' => array(
                    array('JOIN', AMProductTypeFactory::$table_name.' b', 'a.Type', 'b.Name')
                ),
                'select' => 'a.Name, a.ImageURL, b.DisplayName, b.Description, b.IconURL',
                'filter' => $filter
            ));
        }

        public function list_action($req, $post) {
            if($req['p'] == null) {
                $pageIndex = 1;
            } else {
                $pageIndex = $req['p'];
            }
            $count = $this->db->query(array(
                'select' => 'count(*) as rows'
            ))[0];
            $total = intval($count->rows);
            $pageLength = 15;
            $startIndex = ($pageIndex-1) * $pageLength;

            $pageCount = ceil($total/$pageLength);
            $this->model['pageCount'] = $pageCount;

            if($pageIndex <= 1) {
                $previousIndex = 1;
            } else {
                $previousIndex = $pageIndex - 1;
            }
            $this->model['previousIndex'] = $previousIndex;
            if($pageIndex >= $pageCount) {
                $nextIndex = $pageCount;
            } else {
                $nextIndex = $pageIndex + 1;
            }
            $this->model['nextIndex'] = $nextIndex;
            $this->get_all($startIndex, $pageLength);
        }

        public function sync_action($req, $post) {
            $action = $post['btnAction'];

            if ($action == 'syncDataFromServer') {
                $productBusiness = using('plugin.amProduct.class.business.AMProductBusiness');
                $productBusiness->getProductByHttpRequest('model');
                $this->model['result'] = 'success';
            }
        }

        public function update_action($req, $post)
        {
            $id = $req['id'];
            $action = $post['btnAction'];
            $this->_model('isUpdate',true);

            if($action == 'Update') {
                $newData = array(
                    'ID' => $id,
                    'ImageURL' => $post['productModelImageURL']
                );
                $this->db->update($newData, array(
                    "ID" => $id
                ));
                wp_redirect( $this->_getPath() );
                exit();
            } else {
                $productModelItem = $this->getProductModelByID($id);
                $this->model['productModelItem'] = $productModelItem;
            }
        }
        public function del_action($req, $post) {
            $id = $req['productModelID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }
    }
    $GLOBALS['amProductModel'] = new AMProductModelController();
}