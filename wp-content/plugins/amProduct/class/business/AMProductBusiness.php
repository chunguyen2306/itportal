<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 04/04/17
 * Time: 5:24 PM
 */
if(!class_exists('AMProductBusiness')) {
    import('plugin.amProduct.class.factory.AMProductTypeFactory');
    import('plugin.amProduct.class.factory.AMProductModelFactory');
    import('theme.package.HttpRequest');

    class AMProductBusiness
    {
        public function getProductByHttpRequest($param)
        {
            /*
             * $param includes: 'type' and 'model'
             */
            $productTypeFac = new AMProductTypeFactory();
            $productModelFac = new AMProductModelFactory();

            $requestSetting = using('plugin.amProduct.class.includes.ProductDataSetting');
            $request = new HttpRequest(array(
                'url' => $requestSetting['cmdburl'],
                'data' => array(
                    'm' => $requestSetting['method'][$param]
                )
            ));
            $response = $request->send();
            $data = json_decode($response->reponseText);

            if(isset($data) and sizeof($data)>0) {
                switch ($param) {
                    case 'type':
                        foreach ($data as $item) {
                            $existed = $productTypeFac->getOne(array(
                                'filter' => array(
                                    array("ID = '$item->ID'", '')
                                )
                            ));

                            $newData = array(
                                'ID' => $item->ID,
                                'Name' => $item->Name,
                                'Description' => $item->Description
                            );

                            if ($existed == null) {
                                $productTypeFac->insert($newData);
                            } else {
                                if ($existed->Name != $item->Name or $existed->Description != $item->Description) {
                                    $productTypeFac->update($newData, array(
                                        'ID' => $item->ID
                                    ));
                                }
                            }
                            unset($existed);
                        }
                        break;
                    case 'model':
                        foreach ($data as $item) {
                            $existed = $productModelFac->getOne(array(
                                'filter' => array(
                                    array("ID = '$item->ID'", '')
                                )
                            ));

                            $newData = array(
                                'ID' => $item->ID,
                                'Name' => $item->Name,
                                'Type' => $item->Type
                            );

                            if ($existed == null) {
                                $productModelFac->insert($newData);
                            } else {
                                if ($existed->Name != $item->Name or $existed->Type != $item->Type) {
                                    $productModelFac->update($newData, array(
                                        'ID' => $item->ID
                                    ));
                                }
                            }
                            unset($existed);
                        }
                        break;
                }
            }
        }
    }
}
return new AMProductBusiness();
?>