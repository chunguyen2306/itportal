<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 04/04/17
 * Time: 10:33 AM
 */
if(!class_exists('AMProductTypeFactory')) {

    import('theme.package.Abstracts.AbstractDatabase');

    class AMProductTypeFactory extends AbstractDatabase
    {
        public static $table_name = 'am_product_type';
        protected function tableInfo()
        {
            // TODO: Implement tableInfo() method.
            return array(
                'table_name' => AMProductTypeFactory::$table_name,
                'table_columns' => array(
                    'ID' => 'Product Type Code',
                    'Name' => 'Product Type Name',
                    'Description' => 'Product Type Description',
                    'DisplayName' => 'Display Name',
                    'IconURL' => 'Icon URL'
                ),
                'table_columns_type' => array(
                    'ID' => 'int',
                    'Name' => 'string',
                    'Description'=> 'string',
                    'DisplayName' => 'string',
                    'IconURL' => 'string'
                )
            );
        }
    }
}