<?php

/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 04/04/17
 * Time: 10:33 AM
 */
if(!class_exists('AMProductModelFactory')) {
    import('theme.package.Abstracts.AbstractDatabase');
    class AMProductModelFactory extends AbstractDatabase
    {
        public static $table_name = 'am_product_model';
        protected function tableInfo()
        {
            // TODO: Implement tableInfo() method.
            return array(
                'table_name' => AMProductModelFactory::$table_name,
                'table_columns' => array(
                    'ID' => 'Product Model Code',
                    'Name' => 'Product Model Name',
                    'Type' => 'Product Model Type',
                    'DisplayName' => 'Display Name',
                    'ImageURL' => 'Image URL'
                ),
                'table_columns_type' => array(
                    'ID' => 'int',
                    'Name' => 'string',
                    'Type' => 'string',
                    'DisplayName' => 'string',
                    'ImageURL' => 'string'
                )
            );
        }
    }
}