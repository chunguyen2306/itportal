<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 07/04/17
 * Time: 4:38 PM
 */
$typeName = '&type=sync';
?>
<div class="wrap">
    <h1>Update Product Model From Server <a href="<?php echo esc_url('?page='.$model['slug']); ?>" class="page-title-action" title="Back" >Back</a></h1>
    <br/>
    <form class="form-horizontal" method="POST" action="<?php echo esc_url('?page='.$model['slug'].$typeName); ?>">
        <div class="form-group">
            <label class="col-sm-2">Start Sync: </label>
            <div class="col-sm-8">
                <input type="submit" class="button button-primary" name="btnAction" value="syncDataFromServer" style="margin-left: 15px;"/>
            </div>
        </div>
    </form>
    <?php
        if ($model['result'] == 'success')
            echo '<h2>Update Finish!</h2>';
    ?>
</div>