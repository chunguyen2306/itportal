<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 04/04/17
 * Time: 10:07 AM
 */
importCSS('AMProductStyle', 'plugin.amProduct.class.includes.AMProductStyle');
?>
<div class="wrap">
    <h1 style="margin-left: 15px;">Product Type List <a href="<?php echo esc_url('?page='.$model['slug'].'&type=sync'); ?>" class="page-title-action" title="Sync Product Type" >Sync Product Type</a></h1>
    <br/>
    <div class="col-sm-12">
        <table class="wp-list-table widefat fixed striped pages">
            <thead>
            <tr>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="">
                        <span>Product Type Name</span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="">
                        <span>Descriptions</span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="">
                        <span>Display Name</span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="">
                        <span>Icon URL</span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
                if(is_array($model['productType']) && sizeof($model['productType']) > 0) {
                    foreach ($model['productType'] as $item) { ?>
                        <tr>
                            <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                                <a href="<?php echo '?page=' . $model['slug'] . '&type=update&id=' . $item->ID; ?>">
                                    <span><?php echo $item->Name; ?></span>
                                </a>
                            </td>
                            <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                                <p><span><?php echo $item->Description; ?></span></p>
                            </td>
                            <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                                <p><span><?php echo $item->DisplayName; ?></span></p>
                            </td>
                            <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                                <?php if($item->IconURL != '') {
                                    echo '<img src="'.$item->IconURL.'" alt="" height="30" width="30">';
                                } else {
                                    echo '<p><span>Not Found</span></p>';
                                } ?>
                            </td>
                            <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                                <form method="POST" action="<?php echo esc_url('?page=' . $model['slug'] . '&type=del'); ?>">
                                    <input type="hidden" name="productTypeID" value="<?php echo $item->ID; ?>"/>
                                    <button type="submit" class="btn btn-flat danger">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    <?php }
                }
            ?>
            </tbody>
        </table>
        <div class="text-center">
            <ul class="pagination">
                <?php
                echo '<li><a href="'.esc_url('?page=' . $model['slug']).'">First</a></li>';

                echo '<li><a href="'.esc_url('?page=' . $model['slug'] . '&p='. $model['previousIndex']).'">&laquo;</a></li>';

                $start = ($_REQUEST['p'] > 0) ? $_REQUEST['p'] : 1 ;
                $end = (($_REQUEST['p'] + 3) < $model['pageCount']) ? ($_REQUEST['p'] + 3) : $model['pageCount'] ;

                if($start > 1) {
                    echo '<li><a href="' . esc_url('?page=' . $model['slug'] . '&p=1') . '">1</a></li>';
                    if($start > 2) echo '<li class="disabled"><span>...</span></li>';
                }

                for ($i = $start; $i <= $end; $i++) {
                    $class = ($_REQUEST['p'] == $i) ? "active" : "";
                    echo '<li class="'.$class.'"><a href="' . esc_url('?page=' . $model['slug'] . '&p=' . $i) . '">'.$i.'</a></li>';
                }

                if($end < $model['pageCount']) {
                    if($end < ($model['pageCount'] - 1)) echo '<li class="disabled"><span>...</span></li>';
                    echo '<li><a href="' . esc_url('?page=' . $model['slug'] . '&p=') . $model['pageCount'] . '">'.$model['pageCount'].'</a></li>';
                }

                echo '<li><a href="'.esc_url('?page=' . $model['slug'] . '&p='. $model['nextIndex']).'">&raquo;</a></li>';
                echo '<li><a href="'.esc_url('?page=' . $model['slug']. '&p='. $model['pageCount']).'">Last</a></li>';
                ?>
            </ul>
        </div>
    </div>
</div>
