<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 04/04/17
 * Time: 10:07 AM
 */
$typeName = '&type=update&id='.$_REQUEST['id'];
$title = 'Update Product Type';
?>
<div class="wrap">
    <h1><?php echo $title.' '; ?><a href="<?php echo esc_url('?page='.$model['slug']); ?>" class="page-title-action" title="Cancel" >Cancel</a></h1>
    <br/>
    <form class="form-horizontal" method="POST" action="<?php echo esc_url('?page='.$model['slug'].$typeName); ?>">
        <input type="hidden" class="form-control" name="productTypeID" value="<?php echo $model['productTypeItem']->ID; ?>" />
        <div class="form-group">
            <label class="col-sm-2">Product Type Name: </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="productTypeName" style="color: #0A246A" value="<?php echo $model['productTypeItem']->Name; ?>" disabled/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">Descriptions: </label>
            <div class="col-sm-8">
                <input class="form-control" name="productDescription" style="color: #0A246A" value="<?php echo $model['productTypeItem']->Description; ?>" disabled/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">Display Name: </label>
            <div class="col-sm-8">
                <input class="form-control" name="productTypeDisplayName" value="<?php echo $model['productTypeItem']->DisplayName; ?>" required/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">Icon URL: </label>
            <div class="col-sm-8">
                <input class="form-control" name="productTypeIconURL" value="<?php echo $model['productTypeItem']->IconURL; ?>" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm- 10">
                <input type="submit" class="button button-primary" name="btnAction" value="Update" style="margin-left: 15px;"/>
            </div>
        </div>
    </form>
</div>
