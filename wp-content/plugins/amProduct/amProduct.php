<?php
/*
  Plugin Name: AM Product Asset
  Plugin URI:
  Description: Chứa các chức năng quản lý Product, Product Type.
  Version: 1.0
  Author: daopd
  Author URI:
  License:
  Text Domain: am-product
*/

if(!class_exists('amProduct')) {
    if (!function_exists('import')) {
        require_once get_template_directory() . '/package/import.php';
    }

    import('theme.package.Abstracts.AbstractPlugin');

    //Report Class Controller
    import('plugin.amProduct.class.controller.AMProductModelController');
    import('plugin.amProduct.class.controller.AMProductTypeController');

    class amProduct extends AbstractPlugin
    {
        public $db = array();
        public $model = array();

        public function __construct()
        {
            $this->ajaxName = '';
            $this->pluginName = 'amProduct';
            $this->adminMenu = array(
                array(
                    'pageTitle' => 'AM Product',
                    'menuTitle' => 'AM Product',
                    'capability' => 'manage_options',
                    'menuSlug' => 'am_product',
                    'handler' => array(new AMProductModelController(array(
                        'slug' => 'am_product'
                    )), 'index'),
                    'icon' => 'dashicons-screenoptions',
                    'child' => array(
                        array(
                            'pageTitle' => 'Product',
                            'menuTitle' => 'Product',
                            'capability' => 'manage_options',
                            'menuSlug' => 'am_products',
                            'default' => true
                        ),
                        array(
                            'pageTitle' => 'Product Type',
                            'menuTitle' => 'Product Type',
                            'capability' => 'manage_options',
                            'menuSlug' => 'am_product_type',
                            'handler' => array(new AMProductTypeController(array(
                                'slug' => 'am_product_type'
                            )), 'index')
                        )
                    )
                )
            );
            parent::__construct();
        }

        private function define($name, $value)
        {
            if (!defined($name)) {
                define($name, $value);
            }
        }

        protected function init_custom_hook()
        {
            // TODO: Implement init_custom_hook() method.
        }

        protected function custom_init()
        {

        }
    }

    $amProduct = new amProduct();
    $GLOBALS['amProduct'] = $amProduct;
}