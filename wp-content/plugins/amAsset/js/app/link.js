_Link = new function (){
    var t = this;
    var base = "/wp-admin/admin.php";
    t.assetDetail = function (assetName) {
        var link = base + "?page=am_asset_detail";
        return assetName ? link + "&assetName=" + assetName : link;
    };
    t.assetDetailHistory = function (assetName) {
        var link = base + "?page=am_asset_detail_history";
        return assetName ? link + "&assetName=" + assetName : link;
    };

    t.assetState = function (assetName) {
        var link = base + "?page=am_asset_state";
        return assetName ? link + "&assetName=" + assetName : link;
    };
    t.assetStateHistory = function (assetName) {
        var link = base + "?page=am_asset_state_history";
        return assetName ? link + "&assetName=" + assetName : link;
    };

    t.redirect = function(getLinkMethod, value){
        window.location.href = getLinkMethod(value);
    };
    t.pushAsset  = function(getLinkMethod, title, assetName){
        var title = assetName + ' | ' + title;
        var link = getLinkMethod(assetName);

        if(location.href.endsWith(link) == false){
            history.pushState(
                {title: title},
                title,
                link);
        }

        document.title = title;
    };

    window.onpopstate = function(e) {
        document.title = e.state == null ? "ITAM3" : e.state.title;
    };
};