
function escape(s) {
    var n = s;
    n = n.replace(/&/g, "&amp;");
    n = n.replace(/</g, "&lt;");
    n = n.replace(/>/g, "&gt;");
    n = n.replace(/"/g, "&quot;");

    return n;
}

var diffString = function( o, n ) {
    o = o.replace(/\s+$/, '');
    n = n.replace(/\s+$/, '');

    var out = diff(o == "" ? [] : o.split(/\s+/), n == "" ? [] : n.split(/\s+/) );
    var str = "";

    var oSpace = o.match(/\s+/g);
    if (oSpace == null) {
        oSpace = ["\n"];
    } else {
        oSpace.push("\n");
    }
    var nSpace = n.match(/\s+/g);
    if (nSpace == null) {
        nSpace = ["\n"];
    } else {
        nSpace.push("\n");
    }

    if (out.n.length == 0) {
        for (var i = 0; i < out.o.length; i++) {
            str += '<del class="asset-del-text">' + escape(out.o[i]) + oSpace[i] + "</del>";
        }
    } else {
        if (out.n[0].text == null) {
            for (n = 0; n < out.o.length && out.o[n].text == null; n++) {
                str += '<del class="asset-del-text">' + escape(out.o[n]) + oSpace[n] + "</del>";
            }
        }

        for ( var i = 0; i < out.n.length; i++ ) {
            if (out.n[i].text == null) {
                str += '<ins class="asset-ins-text">' + escape(out.n[i]) + nSpace[i] + "</ins>";
            } else {
                var pre = "";

                for (n = out.n[i].row + 1; n < out.o.length && out.o[n].text == null; n++ ) {
                    pre += '<del class="asset-del-text">' + escape(out.o[n]) + oSpace[n] + "</del>";
                }
                str += " " + out.n[i].text + nSpace[i] + pre;
            }
        }
    }

    return str;
}

var diff = function( o, n ) {
    var ns = new Object();
    var os = new Object();

    for ( var i = 0; i < n.length; i++ ) {
        if ( ns[ n[i] ] == null )
            ns[ n[i] ] = { rows: new Array(), o: null };
        ns[ n[i] ].rows.push( i );
    }

    for ( var i = 0; i < o.length; i++ ) {
        if ( os[ o[i] ] == null )
            os[ o[i] ] = { rows: new Array(), n: null };
        os[ o[i] ].rows.push( i );
    }

    for ( var i in ns ) {
        if ( ns[i].rows.length == 1 && typeof(os[i]) != "undefined" && os[i].rows.length == 1 ) {
            n[ ns[i].rows[0] ] = { text: n[ ns[i].rows[0] ], row: os[i].rows[0] };
            o[ os[i].rows[0] ] = { text: o[ os[i].rows[0] ], row: ns[i].rows[0] };
        }
    }

    for ( var i = 0; i < n.length - 1; i++ ) {
        if ( n[i].text != null && n[i+1].text == null && n[i].row + 1 < o.length && o[ n[i].row + 1 ].text == null &&
            n[i+1] == o[ n[i].row + 1 ] ) {
            n[i+1] = { text: n[i+1], row: n[i].row + 1 };
            o[n[i].row+1] = { text: o[n[i].row+1], row: i + 1 };
        }
    }

    for ( var i = n.length - 1; i > 0; i-- ) {
        if ( n[i].text != null && n[i-1].text == null && n[i].row > 0 && o[ n[i].row - 1 ].text == null &&
            n[i-1] == o[ n[i].row - 1 ] ) {
            n[i-1] = { text: n[i-1], row: n[i].row - 1 };
            o[n[i].row-1] = { text: o[n[i].row-1], row: i - 1 };
        }
    }

    return { o: o, n: n };
}

FuncAsset = {
    start: function (element, viewModel, valueAccessor) {
        FuncAsset.loadAssetDetail(element, viewModel.resDetail, viewModel);
    },
    loadAssetDetail: function (element, valueAccessor, viewModel) {
        var query = "resourcename=" + $(element).val();
        FuncAsset.processLoadAssetDetails(query, element, valueAccessor, viewModel);
    },
    loadAssetDetailByName: function (resourcename, element, valueAccessor, viewModel) {

        var query = "resourcename=" + resourcename;
        FuncAsset.processLoadAssetDetails(query, element, valueAccessor, viewModel);
    },
    processLoadAssetDetails: function (query, element, valueAccessor, viewModel) {
        //viewModel is asset
        Common.executeApiWithCalbackOnly(Module.CMDB, null, "GetAllAssetDetail", query, loadAssetDetailSuccessCallback);
        function  loadAssetDetailSuccessCallback(jsonData) {
            var detailObject = Utils.jsonToData(jsonData);
            if (detailObject.IsComponent) {
                viewModel.isComponent(true);
            } else {
                viewModel.isComponent(false);
            }
            try {
                if (!Common.isNull(detailObject)) {
                    viewModel.resDetail(detailObject);
                } else {
                    viewModel.resDetail("");
                }
            } catch (e) {
                //do nothing here
            }

        }
    },
    getDetailOnly: function (field) {
        if (!Common.isNull(field)) {
            var data = field.split(':');
            var len = data.length;
            if (len > 0) {
                if (len === 2) {
                    return data[1];
                } else if (len === 1) {
                    return data[0];
                } else {
                    return 'Detail invalid';
                }
            } else {
                return 'unknown';
            }
        } else {
            return 'unknown';
        }
    },
    buildAssetDetailAsHtml: function (detailObject) {
        detailObject = Common.tryToPlainObject(detailObject);
        var fields;
        var title = "Unknown";
        var detail = "";
        var result = $('<table class="asset-table-detail"></table>');
        if ($.isPlainObject(detailObject)) {
            if (Common.parseBoolean(detailObject.IsComponent)) {
                detailObject.WorkstationFieldString = null;
                detailObject.WorkstationField = null;
            }
            //Load manufacture:
            if (!Common.isNull(detailObject.ResourceModel)) {
                var manufactureHeader = _L.table_head_audit_report_asset_model;
                var manufactureRow = $('<tr></tr>').append($('<td class="asset-td-header">' + manufactureHeader + '</td>'));
                $(manufactureRow).append('<td>' + detailObject.ResourceModel + '</td>');
                $(result).append(manufactureRow);
            }

            if (!Common.isNull(detailObject.WorkstationFieldString)) {
                var workStationField = detailObject.WorkstationFieldString;
                title = _L.form_label_asset_cpu;
                if (Common.isNull(workStationField.CentralProcessingUnit)) {
                    fields = ["Supplier", "Model", "Processing"];
                    $(result).append(FuncAsset.builAssetItem.build(title, detailObject.WorkstationField.CentralProcessingUnit, fields));
                } else {
                    detail = FuncAsset.getDetailOnly(workStationField.CentralProcessingUnit);
                    $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, detail));
                }
                title = _L.form_label_asset_ram;
                if (Common.isNull(workStationField.RandomAccessMemory)) {
                    fields = ["Supplier", "Model", "Storage"];
                    $(result).append(FuncAsset.builAssetItem.build(title, detailObject.WorkstationField.RandomAccessMemory, fields));
                } else {
                    detail = FuncAsset.getDetailOnly(workStationField.RandomAccessMemory);
                    $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, detail));
                    //$(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, workStationField.RandomAccessMemory));
                }
                title = _L.form_label_asset_hard_drive;
                if (Common.isNull(workStationField.HardDiskDrive)) {
                    fields = ["Supplier", "Model", "Storage"];
                    $(result).append(FuncAsset.builAssetItem.build(title, detailObject.WorkstationField.HardDiskDrive, fields));
                } else {
                    detail = FuncAsset.getDetailOnly(workStationField.HardDiskDrive);
                    $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, detail));
                    // $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, workStationField.HardDiskDrive));
                }
                //Graphic card
                title = _L.form_label_asset_graphic_card;
                if (Common.isNull(workStationField.HardDiskDrive)) {
                    fields = ["Supplier", "Model", "Storage"];
                    $(result).append(FuncAsset.builAssetItem.build(title, detailObject.WorkstationField.GraphicCard, fields));
                } else {
                    detail = FuncAsset.getDetailOnly(workStationField.GraphicCard);
                    $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, detail));
                    // $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, workStationField.GraphicCard));
                }
                //DVD
                title = _L.form_label_asset_optical_drive;
                if (Common.isNull(workStationField.HardDiskDrive)) {
                    fields = ["Supplier", "Category", "OrtheInfo"];
                    $(result).append(FuncAsset.builAssetItem.build(title, detailObject.WorkstationField.OpticalDrive, fields));
                } else {
                    detail = FuncAsset.getDetailOnly(workStationField.OpticalDrive);
                    $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, detail));
                    // $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, workStationField.OpticalDrive));
                }
                title = _L.form_label_asset_operation;
                if (!Common.isNull(workStationField.Os)) {
                    detail = FuncAsset.getDetailOnly(workStationField.Os);
                    $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, detail));
                    // $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, workStationField.Os));
                } else {
                    $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, "unknown"));
                }
                title = _L.form_label_asset_office;
                if (!Common.isNull(workStationField.Office)) {
                    detail = FuncAsset.getDetailOnly(workStationField.Office);
                    $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, detail));
                    //$(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, workStationField.Office));
                } else {
                    $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, "unknown"));
                }

            } else {
                if (detailObject.IsComponent) {
                    title = _L.form_label_asset_component;
                    fields = ["Model", "Supplier", "Capacity"];
                    var tmpArray = [detailObject.ComponentField];
                    $(result).append(FuncAsset.builAssetItem.build(title, tmpArray, fields));
                } else {


                    //Load WorkstationField
                    // 1. Load CPUs

                    if (!Common.isNull(detailObject.WorkstationField)) {
                        //Build CPU
                        // result += newLine;
                        title = _L.form_label_asset_cpu;
                        var fields = ["Supplier", "Model", "Processing"];
                        $(result).append(FuncAsset.builAssetItem.build(title, detailObject.WorkstationField.CentralProcessingUnit, fields));
                        //Build RAM
                        // result += newLine;
                        title = _L.form_label_asset_ram;
                        fields = ["Supplier", "Model", "Storage"];
                        $(result).append(FuncAsset.builAssetItem.build(title, detailObject.WorkstationField.RandomAccessMemory, fields));
                        //Build HardDiskDrive
                        // result += newLine;
                        title = _L.form_label_asset_hard_drive;
                        $(result).append(FuncAsset.builAssetItem.build(title, detailObject.WorkstationField.HardDiskDrive, fields));
                        //Build Graphic Card
                        // result += newLine;
                        title = _L.form_label_asset_graphic_card;
                        $(result).append(FuncAsset.builAssetItem.build(title, detailObject.WorkstationField.GraphicCard, fields));
                        //Build OpticalDrive
                        //result += newLine;
                        fields = ["Supplier", "Category", "OrtheInfo"];
                        title = _L.form_label_asset_optical_drive;
                        $(result).append(FuncAsset.builAssetItem.build(title, detailObject.WorkstationField.OpticalDrive, fields));
                    }
                }
            }
            //build generalInfos
            var generalObject = detailObject.GeneralAssetInfo;
            if (!Common.isNull(generalObject)) {
                title = _L.form_label_acquition_date;
                var acquitionDate = Common.getDateToString(generalObject.AcquitionDate);
                detail = acquitionDate !== "-" ? acquitionDate : "unknown";
                $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, detail));

                title = _L.form_label_warranty_date;
                var warrantyDate = Common.getDateToString(generalObject.WarrantyExpiryDate);
                detail = warrantyDate !== "-" ? warrantyDate : "unknown";
                $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, detail));

                title = _L.form_label_expiry_date;
                var expiryDate = Common.getDateToString(generalObject.ExpiryDate);
                detail = expiryDate !== "-" ? expiryDate : "unknown";
                $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, detail));
                title = _L.form_label_serial_number;
                var serialNumber = generalObject.SerialNumber;
                if (Common.isNull(serialNumber) || serialNumber === "-") {
                    detail = "unknown";
                } else {
                    detail = serialNumber;
                }

                $(result).append(FuncAsset.builAssetItem.buildSimpleRow(title, detail));
            }
            var childrenDetails = FuncAsset.buildChildrensAssetDetail(detailObject.childrens);
            var finalResult = $('<div></div>');
            //var mainDetailHeader=$('<div class="asset_detail_main_detail_header"></div>').html(_L.header_main_asset_detail);

            if (!Common.isNull(childrenDetails)) {
                var childrensDetailHeader = $('<div class="asset_detail_main_detail_header"></div>').html(_L.header_childrens_assets_detail);
                var finalTable = $('<table style="margin:0 10px"></table>');
                var finalTr = $('<tr></tr>');
                var mainDetail = $('<td style="vertical-align: top !important;" class="asset_detail_td_container"></td>');
                $(mainDetail).append($(result));
                var childrenDetail = $('<td style="vertical-align: top !important;" class="asset_detail_td_container asset_detail_td_container_children"></td>').append($(childrensDetailHeader));
                $(childrenDetail).append($(childrenDetails));
                $(finalTr).append(mainDetail);
                $(finalTr).append(childrenDetail);
                $(finalTable).append($(finalTr));
                $(finalResult).append(finalTable);

                return $(finalResult).html();
            } else {
                // $(finalResult).append($(mainDetailHeader));
                $(finalResult).append($(result));
                return $(finalResult).html();
            }

        } else {
            return detailObject;
        }

    },
    buildGeneralInfo: function (detailObject) {
        if (!Common.isNull(detailObject)) {

        }
    },
    buildChildrensAssetDetail: function (childrens) {
        if (!Common.isNull(childrens) && childrens.length > 0) {
            var listChildrenItems = $('<div></div>');
            for (var i = 0; i < childrens.length; i++) {
                var item = $('<div class="asset_children_detail_container"></div>');
                var link = $('<a target="blank" href="/wp-admin/admin.php?page=am_asset_state&assetName=' + childrens[i].name + '">' + childrens[i].name + '</a>');
                $(item).append('<div class="asset_children_detail_header"></div>').append($(link));
                var itemDetail = FuncAsset.buildAssetDetailAsHtml(childrens[i].detail);
                $(item).append(itemDetail);
                $(listChildrenItems).append(item);
            }
            return $(listChildrenItems);
        } else {
            return null;
        }


    },
    builAssetItem: {
        buildSimpleRow: function (title, content) {
            if (content === 'unknown') {
                return "";
            }
            var newRow = $("<tr></tr>");
            var tdHeader = $("<td class='asset-td-header'>" + title + "</td>");
            $(newRow).append(tdHeader).append($('<td class="asset-td-content">' + content + '</td>'));
            return $(newRow);
        },
        build: function (title, items, fields) {
            var newRow = $("<tr></tr>");
            var tdHeader = $("<td class='asset-td-header'>" + title + "</td>");
            var tdContent = "";
            if (Common.isNull(items) || items.length === 0) {
                return $(newRow).append(tdHeader).append($('<td>-</td>'));
            } else {
                var assetDetails = FuncAsset.builAssetItem.groupItems(items);
                tdHeader = $("<td class='asset-td-header'>" + title + " (" + assetDetails.count + ") " + "</td>");
                for (var i = 0; i < assetDetails.details.length; i++) {
                    if (i > 0) {
                        tdContent += ", ";
                    }
                    for (var j = 0; j < fields.length; j++) {
                        var newhtmlItem = assetDetails.details[i][fields[j]];
                        tdContent += " " + newhtmlItem;
                    }
                }
            }
            return $(newRow).append(tdHeader).append($('<td>' + tdContent + '</td>'));
        },
        groupItems: function (items) {
            var results = {
                count: 1,
                details: [items[0]]
            };
            var tempData;
            var count = items.length;
            if (count > 1) {
                for (var i = 1; i < items.length; i++) {
                    tempData = FuncAsset.builAssetItem.groupItemHelper(items[i], results);
                }
                results = tempData;
            }
            return results;
        },
        groupItemHelper: function (item, results) {
            var tempDetail = results;
            var count = results.details.length;
            for (var i = 0; i < count; i++) {

                if (Common.getIndexOfObject(item, results.details) === -1) {
                    tempDetail.details.push(item);
                    tempDetail.count += 1;
                }
            }
            return tempDetail;
        }

    }
};