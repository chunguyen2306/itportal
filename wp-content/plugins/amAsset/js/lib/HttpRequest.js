/**
 * Created by Administrator on 11/21/2016.
 */

Libs = window.Libs || {};
Libs.Http = Libs.Http || { };

Libs.Http.exe = function (url, data, optionCallback, isAsync) {
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        async: isAsync,
        success: function (data) {
            Libs.JS.callBack(optionCallback.success, data);
        },
        error: function (data) {
          Libs.JS.callBack(optionCallback.error, data);
        }
    });
};

Libs.Http.getScript = function (url, optionCallback) {
    $.getScript( url, function( script, textStatus ) {
      Libs.JS.callBack(optionCallback.success, script);
    });
};

Libs.ajaxService = function (url, method, pNames, pValues, onSuccess, onError) {
    pNames.push('m');
    pValues.push(method);
    var data = {
        u: url,
        p: JSON.stringify(pNames),
        v: JSON.stringify(pValues)
    }
    $.ajax({
        type: 'POST',
        url: Common.BaseUrl,
        data: data,
        success: function (re) {
            Common.callBack(onSuccess, re);
        },
        error: function (re) {
            onError && onError(re);
            console.log({ url: url, method: method, pNames: pNames, pValues: pValues, re: re });
            PopupBusiness.viewConfirm(null, 'Không thể kết nối tới máy chủ');
        }
    });
};





