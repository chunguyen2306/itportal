

APIData.CMDB.UpdateAssetState = function(componentType, componentType, ciName, state,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "UpdateAssetState",
        ["componentType", "componentType", "ciName", "state"],
        [componentType, componentType, ciName, state],
        onSuccess,
        onError
    );
};
APIData.CMDB.UpdateAssetOwner = function(componentType, componentType, ciName, userName,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "UpdateAssetOwner",
        ["componentType", "componentType", "ciName", "userName"],
        [componentType, componentType, ciName, userName],
        onSuccess,
        onError
    );
};
APIData.CMDB.UpdateAssetDetail = function(componentType, componentType, ciName, detailstr,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "UpdateAssetDetail",
        ["componentType", "componentType", "ciName", "detailstr"],
        [componentType, componentType, ciName, detailstr],
        onSuccess,
        onError
    );
};
APIData.CMDB.UpdateAssociateAsset = function(componentType, componentType, ciName, parent,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "UpdateAssociateAsset",
        ["componentType", "componentType", "ciName", "parent"],
        [componentType, componentType, ciName, parent],
        onSuccess,
        onError
    );
};


APIData.CMDB.GetListComponentType = function(keyword, keyword, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetListComponentType",
        ["keyword", "keyword", "startindex", "limit"],
        [keyword, keyword, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetListComponentModel = function(keyword, keyword, type, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetListComponentModel",
        ["keyword", "keyword", "type", "startindex", "limit"],
        [keyword, keyword, type, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetListAssetState = function(keyword, keyword, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetListAssetState",
        ["keyword", "keyword", "startindex", "limit"],
        [keyword, keyword, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetResourceOwnerByName = function(resourceName, resourceName,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetResourceOwnerByName",
        ["resourceName", "resourceName"],
        [resourceName, resourceName],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetChildrensOfAsset = function(resourceName, resourceName,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetChildrensOfAsset",
        ["resourceName", "resourceName"],
        [resourceName, resourceName],
        onSuccess,
        onError
    );
};
APIData.CMDB.getAssetOfUser = function(LoginName, LoginName,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "getAssetOfUser",
        ["LoginName", "LoginName"],
        [LoginName, LoginName],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetAssetOfUser = function(username, username, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetAssetOfUser",
        ["username", "username", "startindex", "limit"],
        [username, username, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetAccountName = function(name, name,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetAccountName",
        ["name", "name"],
        [name, name],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetUserFullName = function(name, name,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetUserFullName",
        ["name", "name"],
        [name, name],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetValue = function(value, value,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetValue",
        ["value", "value"],
        [value, value],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetUserInformation = function(username, username, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetUserInformation",
        ["username", "username", "startindex", "limit"],
        [username, username, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.UpdateCustomAssetDetail = function(resourcename, resourcename, CPU, RAM, VGA, HDD, DVD,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "UpdateCustomAssetDetail",
        ["resourcename", "resourcename", "CPU", "RAM", "VGA", "HDD", "DVD"],
        [resourcename, resourcename, CPU, RAM, VGA, HDD, DVD],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetLastStatusHistory = function(resourcename, resourcename,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetLastStatusHistory",
        ["resourcename", "resourcename"],
        [resourcename, resourcename],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetResourceStateById = function(id, id,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetResourceStateById",
        ["id", "id"],
        [id, id],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetResourceStateHistoryByResourceName = function(resourcename, resourcename,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetResourceStateHistoryByResourceName",
        ["resourcename", "resourcename"],
        [resourcename, resourcename],
        onSuccess,
        onError
    );
};
APIData.CMDB.getResourceOwnerHistoryInfo = function(STATEHISTORYID, STATEHISTORYID,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "getResourceOwnerHistoryInfo",
        ["STATEHISTORYID", "STATEHISTORYID"],
        [STATEHISTORYID, STATEHISTORYID],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetNumberFromStr = function(str, str,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetNumberFromStr",
        ["str", "str"],
        [str, str],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetAssetType = function(ResourceName, ResourceName,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetAssetType",
        ["ResourceName", "ResourceName"],
        [ResourceName, ResourceName],
        onSuccess,
        onError
    );
};
APIData.CMDB.CheckAndUpdateAssociationAsset = function(resourceName, resourceName, parentAsset, updateField,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "CheckAndUpdateAssociationAsset",
        ["resourceName", "resourceName", "parentAsset", "updateField"],
        [resourceName, resourceName, parentAsset, updateField],
        onSuccess,
        onError
    );
};
APIData.CMDB.UpdateWorkstationTextFieldByName = function(columnName, columnName, value, resourceName,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "UpdateWorkstationTextFieldByName",
        ["columnName", "columnName", "value", "resourceName"],
        [columnName, columnName, value, resourceName],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetAllAssetDetail = function(resourcename, resourcename,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetAllAssetDetail",
        ["resourcename", "resourcename"],
        [resourcename, resourcename],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetResourceStateByAssetName = function(resourcename, resourcename,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetResourceStateByAssetName",
        ["resourcename", "resourcename"],
        [resourcename, resourcename],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetListAsset = function(resourcename, resourcename, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetListAsset",
        ["resourcename", "resourcename", "startindex", "limit"],
        [resourcename, resourcename, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetAssetStateList = function(state, state, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetAssetStateList",
        ["state", "state", "startindex", "limit"],
        [state, state, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetListUser = function(username, username, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetListUser",
        ["username", "username", "startindex", "limit"],
        [username, username, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetAssetByType = function(typeID, typeID, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetAssetByType",
        ["typeID", "typeID", "startindex", "limit"],
        [typeID, typeID, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetAssetByModel = function(modelID, modelID, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetAssetByModel",
        ["modelID", "modelID", "startindex", "limit"],
        [modelID, modelID, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetAssetByTypeAndModel = function(typeID, typeID, modelID, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetAssetByTypeAndModel",
        ["typeID", "typeID", "modelID", "startindex", "limit"],
        [typeID, typeID, modelID, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetAssetByTypeAndModelName = function(type, type, modelName, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetAssetByTypeAndModelName",
        ["type", "type", "modelName", "startindex", "limit"],
        [type, type, modelName, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetAssetInStoreByTypeAndModelAndName = function(type, type, modelName, name, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetAssetInStoreByTypeAndModelAndName",
        ["type", "type", "modelName", "name", "startindex", "limit"],
        [type, type, modelName, name, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetAssetInStoreByTypeAndModel = function(typeID, typeID, modelID, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetAssetInStoreByTypeAndModel",
        ["typeID", "typeID", "modelID", "startindex", "limit"],
        [typeID, typeID, modelID, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.checkAssetInStore = function(resourceName, resourceName, type,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "checkAssetInStore",
        ["resourceName", "resourceName", "type"],
        [resourceName, resourceName, type],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetAssetByTypeAndModelAndName = function(type, type, modelName, name, startindex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetAssetByTypeAndModelAndName",
        ["type", "type", "modelName", "name", "startindex", "limit"],
        [type, type, modelName, name, startindex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetAssetCost = function(assetname, assetname,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetAssetCost",
        ["assetname", "assetname"],
        [assetname, assetname],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetDefaultUserBySite = function(listUser, listUser, site,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetDefaultUserBySite",
        ["listUser", "listUser", "site"],
        [listUser, listUser, site],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetPersonInfoByUsername = function(userName, userName,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetPersonInfoByUsername",
        ["userName", "userName"],
        [userName, userName],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetListSite = function(siteName, siteName, startIndex, limit,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetListSite",
        ["siteName", "siteName", "startIndex", "limit"],
        [siteName, siteName, startIndex, limit],
        onSuccess,
        onError
    );
};
APIData.CMDB.getUserAccountByEmployeeId = function(employeeId, employeeId,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "getUserAccountByEmployeeId",
        ["employeeId", "employeeId"],
        [employeeId, employeeId],
        onSuccess,
        onError
    );
};
APIData.CMDB.GetPositionOfUser = function(username, username,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "GetPositionOfUser",
        ["username", "username"],
        [username, username],
        onSuccess,
        onError
    );
};
APIData.CMDB.getListEmployeeTitle = function(name, name,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "getListEmployeeTitle",
        ["name", "name"],
        [name, name],
        onSuccess,
        onError
    );
};
APIData.CMDB.updateGeneralInformation = function(resourceName, resourceName, detailJson,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "updateGeneralInformation",
        ["resourceName", "resourceName", "detailJson"],
        [resourceName, resourceName, detailJson],
        onSuccess,
        onError
    );
};
APIData.CMDB.getGeneralInfo = function(resourceName, resourceName,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "getGeneralInfo",
        ["resourceName", "resourceName"],
        [resourceName, resourceName],
        onSuccess,
        onError
    );
};
APIData.CMDB.getUserADInformation = function(username, username,  onSuccess, onError){
    Libs.ajaxService(
        APIData.CMDB.ServiceURL,
        "getUserADInformation",
        ["username", "username"],
        [username, username],
        onSuccess,
        onError
    );
};
