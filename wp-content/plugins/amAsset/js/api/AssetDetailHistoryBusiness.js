var AssetDetailHistoryBusiness = {
    insertHistory: function (assetName, newDetail, previousDetail, comment, sysComment) {

        var obj = {
            AssetName: assetName,
            NewDetail: newDetail,
            PreviousDetail: previousDetail,
            UserAction: userAction || Common.getCurrentUser(),
            Comment: comment,
            SysComment: sysComment

        };
       // HttpUtils.CallAjax("insertHistory", Module.ITAAPService, SubPath.AssetDetailHistory, submitObj, options);
        Libs.ajaxService(
            "asset",
            "insertHistory",
            ["data"],
            [JSON.stringify(submitObj)],
            insertHistorySuccessCallback
        );
        // APIData.Asset.InsertHistory(obj, );

    },
    viewDetailHistory: function () {
        var assetName = MODEL.assetName();
        var query = "assetName=" + assetName;
        var options = {
            success: viewDetailHistorySuccessCallback
        };
        HttpUtils.CallAjaxWithQueryString("getDetailHistory", Module.ITAAPService, SubPath.AssetDetailHistory, query, options);
        function viewDetailHistorySuccessCallback(jsonData) {
            var historyObject = Common.getKoObject(jsonData);
            if (historyObject == null) {
                var totalRecords = historyObject().length;
                for (var i = 0; i < totalRecords; i++) {
                    var oldDetailPlainObject = Common.tryToPlainObject(historyObject[i].PreviousDetail);
                    var newDetailPlainObject = Common.tryToPlainObject(historyObject[i].NewDetail);
                    Utils.buildChangedOfTwoObject(oldDetailPlainObject, newDetailPlainObject);                 
                    var oldDetail = FuncAsset.buildAssetDetailAsHtml(oldDetailPlainObject);
                    var newDetail = FuncAsset.buildAssetDetailAsHtml(newDetailPlainObject);
                    //var htmlChanged = Utils.detectTableContentChanges(oldDetail, newDetail);                  
                    historyObject[i].previousDetail = oldDetail;
                    historyObject[i].newDetail = newDetail;
                }
                MODEL.assetDetailHistory(historyObject);
            } else {
                MODEL.assetDetailHistory(null);
            }
        }
    }
};
loadModel = function () {
    var keyword = MODEL.externalTask.urlParameters().keyword;
    if (!Common.isNull(keyword)) {
        MODEL.assetName(keyword);
        AssetDetailHistoryBusiness.viewDetailHistory();
    }

};
