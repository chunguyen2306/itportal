APIData = window.APIData || {};

APIData.CMDB = {};
APIData.CMDB.ServiceURL = "cmdb";

///
APIData.CMDB.GetListAsset = function(resourcename, startindex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetListAsset",
        ["resourcename", "startindex", "limit"],
        [resourcename, startindex || 0, limit || 10],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetListUser = function(username, startindex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetListUser",
        ["username", "startindex", "limit"],
        [username, startindex || 0, limit || 10],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.buildAssetDetail = function (assetObject) {
    var realValue = Common.getValueOfObject(assetObject);
    var totals = realValue.length;
    var details = totals;
    for (var m = 0; m < totals; m++) {
        details += "-";
        var props = Object.getOwnPropertyNames(realValue[m]);
        for (var i = 0; i < props.length; i++) {
            var propName = props[i];
            var value = realValue[m][propName];
            if (i === props.length - 1) {
                details += value;
            } else {
                details += value + ":";
            }
        }
    }
    return details;
};
APIData.CMDB.buildAssetStateModel = function (searModel, listAssetState) {
    var list = [];
    for (var i = 0; i < listAssetState.length; i++) {
        var description = listAssetState[i].DisplayDescription;
        description = description ? listAssetState[i].DisplayState + ' - ' + description
            : listAssetState[i].DisplayState;
        var item = {
            name:  description ,
            value: listAssetState[i].DisplayState
        };
        list.push(item);
    }

    var searchMember = [
        {Name: 'value', IsUnicode: false},
        {Name: 'name', IsUnicode: true}
    ];
    searModel.setDataSource(list, searchMember);
}

// auto generate
APIData.CMDB.UpdateAssetState = function(componentType, ciName, state, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "UpdateAssetState",
        ["componentType", "ciName", "state"],
        [componentType, ciName, state],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.UpdateAssetOwner = function(componentType, ciName, userName, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "UpdateAssetOwner",
        ["componentType", "ciName", "userName"],
        [componentType, ciName, userName],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.UpdateAssetDetail = function(componentType, ciName, detailstr, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "UpdateAssetDetail",
        ["componentType", "ciName", "detailstr"],
        [componentType, ciName, detailstr],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.UpdateAssociateAsset = function(componentType, ciName, parent, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "UpdateAssociateAsset",
        ["componentType", "ciName", "parent"],
        [componentType, ciName, parent],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetListComponentType = function(keyword, startindex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetListComponentType",
        ["keyword", "startindex", "limit"],
        [keyword, startindex, limit],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetListComponentModel = function(keyword, type, startindex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetListComponentModel",
        ["keyword", "type", "startindex", "limit"],
        [keyword, type, startindex, limit],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetResourceOwnerByName = function(resourceName, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetResourceOwnerByName",
        ["resourceName"],
        [resourceName],
        APIData.Ajax.CmdbCallback.string,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetChildrensOfAsset = function(resourceName, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetChildrensOfAsset",
        ["resourceName"],
        [resourceName],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.getAssetOfUser = function(LoginName, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "getAssetOfUser",
        ["LoginName"],
        [LoginName],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetAssetOfUser = function(username, startindex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetAssetOfUser",
        ["username", "startindex", "limit"],
        [username, startindex, limit],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetAccountName = function(name, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetAccountName",
        ["name"],
        [name],
        APIData.Ajax.CmdbCallback.string,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetUserFullName = function(name, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetUserFullName",
        ["name"],
        [name],
        APIData.Ajax.CmdbCallback.string,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetUserInformation = function(username, startindex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetUserInformation",
        ["username", "startindex", "limit"],
        [username, startindex, limit],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.UpdateCustomAssetDetail = function(resourcename, CPU, RAM, VGA, HDD, DVD, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "UpdateCustomAssetDetail",
        ["resourcename", "CPU", "RAM", "VGA", "HDD", "DVD"],
        [resourcename, CPU, RAM, VGA, HDD, DVD],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetLastStateHistory = function(resourcename, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetLastStateHistory",
        ["resourcename"],
        [resourcename],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetResourceStateById = function(id, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetResourceStateById",
        ["id"],
        [id],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetResourceStateHistoryByResourceName = function(resourcename, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetResourceStateHistoryByResourceName",
        ["resourcename"],
        [resourcename],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.getResourceOwnerHistoryInfo = function(STATEHISTORYID, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "getResourceOwnerHistoryInfo",
        ["STATEHISTORYID"],
        [STATEHISTORYID],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetNumberFromStr = function(str, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetNumberFromStr",
        ["str"],
        [str],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetAssetType = function(ResourceName, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetAssetType",
        ["ResourceName"],
        [ResourceName],
        APIData.Ajax.CmdbCallback.string,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.CheckAndUpdateAssociationAsset = function(resourceName, parentAsset, updateField, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "CheckAndUpdateAssociationAsset",
        ["resourceName", "parentAsset", "updateField"],
        [resourceName, parentAsset, updateField],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.UpdateWorkstationTextFieldByName = function(columnName, value, resourceName, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "UpdateWorkstationTextFieldByName",
        ["columnName", "value", "resourceName"],
        [columnName, value, resourceName],
        APIData.Ajax.CmdbCallback.string,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetAllAssetDetail = function(resourcename, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetAllAssetDetail",
        ["resourcename"],
        [resourcename],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetResourceStateByAssetName = function(resourcename, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetResourceStateByAssetName",
        ["resourcename"],
        [resourcename],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetAssetStateList = function(state, startindex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetAssetStateList",
        ["state", "startindex", "limit"],
        [state, startindex, limit],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};

APIData.CMDB.GetAssetByType = function(typeID, startindex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetAssetByType",
        ["typeID", "startindex", "limit"],
        [typeID, startindex, limit],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetAssetByModel = function(modelID, startindex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetAssetByModel",
        ["modelID", "startindex", "limit"],
        [modelID, startindex, limit],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetAssetByTypeAndModel = function(typeID, modelID, startindex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetAssetByTypeAndModel",
        ["typeID", "modelID", "startindex", "limit"],
        [typeID, modelID, startindex, limit],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetAssetByTypeAndModelName = function(type, modelName, startindex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetAssetByTypeAndModelName",
        ["type", "modelName", "startindex", "limit"],
        [type, modelName, startindex, limit],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetAssetInStoreByTypeAndModelAndName = function(type, modelName, name, startindex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetAssetInStoreByTypeAndModelAndName",
        ["type", "modelName", "name", "startindex", "limit"],
        [type, modelName, name, startindex, limit],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetAssetInStoreByTypeAndModel = function(typeID, modelID, startindex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetAssetInStoreByTypeAndModel",
        ["typeID", "modelID", "startindex", "limit"],
        [typeID, modelID, startindex, limit],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.checkAssetInStore = function(resourceName, type, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "checkAssetInStore",
        ["resourceName", "type"],
        [resourceName, type],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetAssetByTypeAndModelAndName = function(type, modelName, name, startindex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetAssetByTypeAndModelAndName",
        ["type", "modelName", "name", "startindex", "limit"],
        [type, modelName, name, startindex, limit],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetAssetCost = function(assetname, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetAssetCost",
        ["assetname"],
        [assetname],
        APIData.Ajax.CmdbCallback.float,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetDefaultUserBySite = function(listUser, site, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetDefaultUserBySite",
        ["listUser", "site"],
        [listUser, site],
        APIData.Ajax.CmdbCallback.string,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetPersonInfoByUsername = function(userName, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetPersonInfoByUsername",
        ["userName"],
        [userName],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetListSite = function(siteName, startIndex, limit, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetListSite",
        ["siteName", "startIndex", "limit"],
        [siteName, startIndex, limit],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.getUserAccountByEmployeeId = function(employeeId, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "getUserAccountByEmployeeId",
        ["employeeId"],
        [employeeId],
        APIData.Ajax.CmdbCallback.string,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.GetPositionOfUser = function(username, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "GetPositionOfUser",
        ["username"],
        [username],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.getListEmployeeTitle = function(name, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "getListEmployeeTitle",
        ["name"],
        [name],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.updateGeneralInformation = function(resourceName, detailJson, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "updateGeneralInformation",
        ["resourceName", "detailJson"],
        [resourceName, detailJson],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.getGeneralInfo = function(resourceName, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "getGeneralInfo",
        ["resourceName"],
        [resourceName],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDB.getUserADInformation = function(username, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDB.ServiceURL,
        "getUserADInformation",
        ["username"],
        [username],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};




/*
 CMDBEX: ______________________________________________________________________________________
 */

APIData.CMDBEX = {};
APIData.CMDBEX.ServiceURL = "cmdbex";
APIData.CMDBEX.UpdateAssetState = function(componentType, ciName, state, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDBEX.ServiceURL,
        "UpdateAssetState",
        ["componentType", "ciName", "state"],
        [componentType, ciName, state],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDBEX.UpdateAssetOwner = function(componentType, ciName, userName, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDBEX.ServiceURL,
        "UpdateAssetOwner",
        ["componentType", "ciName", "userName"],
        [componentType, ciName, userName],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDBEX.UpdateAssetDetail = function(componentType, ciName, detailstr, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDBEX.ServiceURL,
        "UpdateAssetDetail",
        ["componentType", "ciName", "detailstr"],
        [componentType, ciName, detailstr],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDBEX.UpdateAssociateAsset = function(componentType, ciName, parent, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDBEX.ServiceURL,
        "UpdateAssociateAsset",
        ["componentType", "ciName", "parent"],
        [componentType, ciName, parent],
        APIData.Ajax.CmdbCallback.object,
        onSuccess,
        onError, dontSkip
    );
};
APIData.CMDBEX.GetListAssetDetailHistory = function(resourceName, onSuccess, onError, dontSkip){
    APIData.Ajax.CMDB(
        APIData.CMDBEX.ServiceURL,
        "GetListAssetDetailHistory",
        ["resourceName"],
        [resourceName],
        APIData.Ajax.CmdbCallback.array,
        onSuccess,
        onError, dontSkip
    );
};









