function ASSET_MODEL() {

    var t = this;
    t.assetName = ko.observable(Utils.getURLValue("assetName")).extend({notify: 'always'});
    t.currentDetail = ko.observable("");
    t.assetDetail = {
        CPU: ko.observableArray(),
        RAM: ko.observableArray(),
        VGA: ko.observableArray(),
        HDD: ko.observableArray(),
        CD_DVD: ko.observableArray()
    };
    t.note = ko.observable("");
    t.isLoadAsset = ko.observable(false);
    t.assetName.subscribe(function (assetName) {
        _Link.pushAsset(_Link.assetDetail, _L.menu_asset_detail, assetName);

        if (assetName == false){
            Popup.warning(_L.warning_asset_name_is_empty);
            return;
        }

        t.isLoadAsset(false);
        function loadAssetDetailSuccessCallback(detailObject, jsonData) {
            if(detailObject == false){
                Popup.warning(_L.warning_asset_detail_not_found);
                return;
            }

            t.currentDetail(jsonData);
            if (detailObject.WorkstationField) {
                t.isLoadAsset(true);
                function setDetail(ko, value) {
                    if (Array.isArray(value))
                        ko(value);
                    else
                        ko([]);
                }
                var wf = detailObject.WorkstationField;
                setDetail(t.assetDetail.CPU, wf.CentralProcessingUnit);
                setDetail(t.assetDetail.RAM, wf.RandomAccessMemory);
                setDetail(t.assetDetail.VGA, wf.GraphicCard);
                setDetail(t.assetDetail.HDD, wf.HardDiskDrive);
                setDetail(t.assetDetail.CD_DVD, wf.OpticalDrive);

            } else {
                Popup.warning(_L.warning_asset_detail_not_found);
            }
        }
        APIData.CMDB.GetAllAssetDetail(assetName, loadAssetDetailSuccessCallback, null, 1);
    });

    t.redirectState = function () {
        _Link.redirect(_Link.assetState, t.assetName());
    }
    t.redirectHistory = function () {
        _Link.redirect(_Link.assetDetailHistory, t.assetName());
    }
    t.redirect = function(el, getLinkMethod){
        var link = getLinkMethod(t.assetName());
       $(el).attr("href", link);
        return true;
    }

    t.save = function () {
        var note = t.note();
        if (!note) {
            Popup.error(_L.warning_empty_note_change_detail);
            $('#form_new_change_detail_note').focus();
            return;
        }

        var assetName = t.assetName();
        var cpus = APIData.CMDB.buildAssetDetail(t.assetDetail.CPU());
        var rams = APIData.CMDB.buildAssetDetail(t.assetDetail.RAM());
        var vgas = APIData.CMDB.buildAssetDetail(t.assetDetail.VGA());
        var hdds = APIData.CMDB.buildAssetDetail(t.assetDetail.HDD());
        var dvds = APIData.CMDB.buildAssetDetail(t.assetDetail.CD_DVD());

        APIData.CMDB.UpdateCustomAssetDetail(assetName, cpus, rams, vgas, hdds, dvds,
            function (data, jsonData) {
                var newDetail = jsonData;
                var previousDetail = t.currentDetail();
                var comment = t.note();
                APIData.Asset.InsertAssetDetailHistory(
                    assetName, newDetail, previousDetail, comment, "Successfully", null,
                    function (obj) {
                        if(obj.code == APISTATUSCODE.ok)
                            t.currentDetail(obj.post_title.NewDetail);
                    }
                );
                Popup.info( _L.warning_update_successfull);

            });
    };

    t.deleteDVD = function (data) {
        t.assetDetail.CD_DVD.remove(data);
    };
    t.deleteRAM = function (data) {
        t.assetDetail.RAM.remove(data);
    };
    t.deleteHDD = function (data) {
        t.assetDetail.HDD.remove(data);
    };
    t.deleteVGA = function (data) {
        t.assetDetail.VGA.remove(data);
    };
    t.deleteCPU = function (data) {
        t.assetDetail.CPU.remove(data);
    };

    //Add
    t.addDVD = function () {
        t.assetDetail.CD_DVD.push( {
            Supplier: "",
            Category: "",
            OrtheInfo: ""
        });
    };
    t.addRAM = function () {
        t.assetDetail.RAM.push({
            Supplier: "",
            Model: "",
            Storage: ""
        });
    };
    t.addHDD = function () {
        t.assetDetail.HDD.push({
            Supplier: "",
            Model: "",
            Storage: ""
        });
    };
    t.addVGA = function () {
        t.assetDetail.VGA.push({
            Supplier: "",
            Model: "",
            Storage: ""
        });
    };
    t.addCPU = function () {
        t.assetDetail.CPU.push({
            Supplier: "",
            Model: "",
            Processing: ""
        });
    };
};
MODEL = new ASSET_MODEL();
ko.applyBindings(MODEL, $("#ko-asset-detail-history")[0]);