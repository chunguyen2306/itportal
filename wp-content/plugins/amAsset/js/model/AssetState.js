function ASSET_STATE_MODEL(){
    var t = this;
    var searModelState = new SearchModel();

    t.asset = {
        resDetail: ko.observable(""),
        isComponent: ko.observable(false),
        ResId: ko.observable(""),
        ResType: ko.observable(""),
        ResModel: ko.observable(""),
        State: ko.observable(""),
        StateDescription: ko.observable(""),
        Parent: ko.observable(""),
        Owner: ko.observable(""),
        Department: ko.observable(""),
        MACAddress: ko.observable("")
    };
    t.assetName = ko.observable(Utils.getURLValue("assetName")).extend({notify: 'always'});
    t.toUser = ko.observable("");
    t.toAsset = ko.observable("");
    t.note = ko.observable("");
    t.canEditState = ko.observable(false);
    t.isLoadAsset = ko.observable(false);
    t.target = ko.observable("none");
    t.newState = ko.observable("");
    t.displayOption = ko.observable(false);
    t.assetName.subscribe(function(assetName) {
        console.log("change");
        _Link.pushAsset(_Link.assetState, _L.title_asset_state, assetName);
        if (assetName == false) {
            t.asset.resDetail(_L.warning_asset_detail_not_found);
            return;
        }

        try {
            APIData.CMDB.GetAllAssetDetail(assetName, function (detailObject) {
                if (detailObject == null) {
                    t.asset.resDetail(_L.warning_asset_detail_not_found);
                    t.isLoadAsset(false);
                } else {
                    t.isLoadAsset(true);
                    t.asset.MACAddress(detailObject.MacAddress);
                    t.asset.resDetail(FuncAsset.buildAssetDetailAsHtml(detailObject));
                }
            });

            APIData.CMDB.GetResourceStateByAssetName(assetName, function(state) {
                if (state == null) {
                    t.isLoadAsset(false);
                    Popup.warning("Không tìm thấy thông tin trạng thái tài sản");
                } else {
                    t.isLoadAsset(true);
                    t.canEditState(true);
                    t.asset.State(state.State);
                    t.asset.StateDescription(state.StateDescription || _L.warning_no_asset_state_description);
                    t.asset.isComponent(state.isComponent);
                    t.asset.ResId(state.ResId);
                    t.asset.ResType(state.ResType);
                    t.asset.ResModel(state.ResModel);
                    t.asset.Parent(state.Parent);
                    t.asset.Owner(state.Owner);
                    t.asset.Department(state.Department);
                }
            }, 0, 1);
        } catch (e) {
            Popup.failed();
        }
    });
    t.searchState = function(keyword, start, limit, endSearch){
        searModelState.search(keyword, endSearch);
    }
    t.newState.subscribe(function (stateKey){
        t.displayOption(stateKey == AssetState.InUse);
    });
    t.onUserSelected = function (accountName) {
        t.toUser(accountName);
    };
    t.redirectDetail = function () {
        _Link.redirect(_Link.assetDetail, t.assetName());
    }
    t.redirectHistory = function () {
        _Link.redirect(_Link.assetStateHistory, t.assetName());
    }
    t.changeAssetState = function () {
        var componentType = t.asset.ResModel();
        var ciName = t.asset.ResId();
        var state = t.newState();
        var note = t.note();
        var error = "";
        var toTarget = "";
        var isChangeOwner = false;
        var isChangeParent = false;

        (state == false) && (error += _L.warning_empty_asset_state + "<br/>");
        (note == false) && (error += _L.warning_note_is_empty + "<br/>");

        if (state === AssetState.InUse) {
            var target = t.target();
            if (target === 'none') {
                error += _L.warning_undefined_target_parent + "<br/>";
            } else if (target === 'User') {
                var toUser = t.toUser();
                if (toUser) {
                    isChangeOwner = true;
                    toTarget = toUser;
                }else{
                    error += _L.warning_undefined_target_user + "<br/>";
                }
            } else if (target === 'Asset') {
                var toAsset = t.toAsset();
                if (!toAsset)
                    error += _L.warning_undefined_target_asset + "<br/>";
                if (ciName === toAsset)
                    error += _L.warning_tartget_same_with_object + "<br/>";

                isChangeParent = true;
                toTarget = toAsset;
            }
        }
        if (error === "") {
            function updateStastusSuccessfully(obj, jsonData) {
                if(obj && obj.code == APISTATUSCODE.ok && obj.LastHistory){
                    //Save History to ITAM DB
                    var lastHistory = obj.LastHistory;
                    APIData.Asset.InsertStateHistory(
                        lastHistory.StateHistoryId,
                        ciName,
                        lastHistory.NewState,
                        lastHistory.PreviousState,
                        new Date(), // actionDate
                        Common.getCurrentUser(),
                        toTarget,
                        note,
                        "On tool ITAM3",
                        function (obj, jsonData) {
                            if(obj && obj.code == APISTATUSCODE.ok){
                                Popup.success();
                                lastHistory.NewState &&  t.asset && t.asset.State(lastHistory.NewState);
                            }
                            else{
                                Popup.warning(_L.popup_content_incomplete_save_history);
                                obj ? API.log(obj) : API.log(jsonData);
                            }
                        },
                        null,
                        1
                    );
                } else {
                    obj && API.log(obj);
                    Popup.failed();
                }
            }
            if (isChangeOwner) {
                APIData.CMDBEX.UpdateAssetOwner(componentType, ciName, toTarget,updateStastusSuccessfully, null, 1);
            } else if (isChangeParent) {
                APIData.CMDBEX.UpdateAssociateAsset(componentType, ciName, toTarget,updateStastusSuccessfully, null, 1);
            } else{
                APIData.CMDBEX.UpdateAssetState(componentType, ciName, state,updateStastusSuccessfully, null, 1);
            }
        } else {
            Popup.error(error);
        }
    };
    t.userForcus = function () {
        t.target("User");
    };
    t.assetForcus = function () {
        t.target("Asset");
    };

    APIData.CMDB.GetAssetStateList('', '', '', function(assetStateList){
        APIData.CMDB.buildAssetStateModel(searModelState, assetStateList);
    });
};
MODEL = new ASSET_STATE_MODEL();
ko.applyBindings(MODEL, $("#ko-assest-state")[0]);

















