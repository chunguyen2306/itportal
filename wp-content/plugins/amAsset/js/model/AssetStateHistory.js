function ASSET_STATE_HISTORY_MODEL() {
    var t = this;
    t.assetStateHistory = ko.observableArray();

    function getTimeIfValidate(time) {
        if (Object.prototype.toString.call(time) === "[object Date]") {
            // it is a date
            if (isNaN(time.getTime())) {  // d.valueOf() could also work
            }
            else {
                // date is valid
                return time;
            }
        }
        else {
            // not a date
        }


        return null;
    }

    function mysqlStringTimeToJSTime(mysqlStr) {
        try {
            var array = mysqlStr.split(".");
            var re = array.length > 1 ? new Date(array[0]) : new Date(mysqlStr);
            return getTimeIfValidate(re);
        } catch (ex) {
            return null;
        }
    }

    t.assetName = ko.observable(Utils.getURLValue("assetName")).extend({notify: 'always'});
    t.assetName.subscribe(function (assetName) {
        t.assetName(assetName);
        _Link.pushAsset(_Link.assetStateHistory, _L.title_asset_state_history, assetName);
        t.assetStateHistory([]);

        var cmdbHis;
        var atmHis;

        function merger() {
            if (cmdbHis == null || atmHis == null)
                return;
            console.log(cmdbHis);
            console.log(atmHis);

            for (var i = 0; i < cmdbHis.length; i++) {
                var cm = cmdbHis[i];
                var time = getTimeIfValidate(new Date(cm.StartTime));
                cm.time = time ? time.getTime() : 0;
            }

            for (var iAM = 0; iAM < atmHis.length; iAM++) {
                var am = atmHis[iAM];
                am.ActionDate = mysqlStringTimeToJSTime(am.ActionDate);
                am.time = am.ActionDate ? am.ActionDate.getTime() : 0;

                var cmData;
                var duration = 30000;

                for (var iCM = 0; iCM < cmdbHis.length; iCM++) {
                    var cm = cmdbHis[iCM];
                    var d = am.time - cm.time;

                    if (d > 0 && d < duration
                        && am.NewState === cm.StateName
                        && am.PreviousState === cm.PrvStateName) {
                        cmData = cm;
                        duration = d;
                    }
                }

                if (cmData) {
                    am.SysComment = cmData.Comment;
                    am.OwnerAssetname = cmData.OwnerAssetname;
                    am.OnwerUser = cmData.OnwerUser;

                    am.cm = cmData;

                    var index = cmdbHis.indexOf(cmData);
                    ( index > -1) && cmdbHis.splice(index, 1);
                }
            }

            atmHis.sort(function (x, y) {
                return x.time - y.time;
            });


            console.log(atmHis);
        };
        APIData.CMDBEX.GetListAssetDetailHistory(assetName, function (listHis) {
            cmdbHis = listHis;
            merger();
        });
        APIData.Asset.GetAssetStateHistory(assetName, function (listHis) {
            atmHis = listHis;
            merger();
        });

        // APIData.CMDB.GetResourceStateHistoryByResourceName(assetName, function(his) {
        //     var listHistory = [];
        //     for (var i = 0; i < his.length; i++) {
        //         var item = {
        //             cmdb: his[i],
        //             userAction: "",
        //             aapComment: "",
        //             assignTo: "-",
        //             sysComment: "",
        //             actionDate: null
        //         };
        //         listHistory.push(item);
        //         t.processBuildStateHistoryItem(his[i], i);
        //     }
        //     t.assetStateHistory(listHistory);
        // });
    });

    t.redirectState = function () {
        _Link.redirect(_Link.assetState, t.assetName());
    };

    t.processBuildStateHistoryItem = function (cmdbHistory, index) {
        APIData.Asset.GetAssetStateHistoryByCMDBHistoryID(cmdbHistory.StateHistoryId, function (historyObject) {

            var userAction = " -- ";
            var aapComment = " -- ";
            var sysComment = _L.warning_action_is_performed_by_another_tool;
            var assignTo = " -- ";
            var actionDate;
            if (cmdbHistory.Owner) {
                userAction = cmdbHistory.Owner.TaskUser;
                var assignNote = cmdbHistory.Owner.Details.Comment;
                if (assignNote) {
                    assignNote = " (" + assignNote + ")";
                } else {
                    assignNote = "";
                }
                assignTo = cmdbHistory.Owner.Details.Name + assignNote;
            }
            if (historyObject) {
                userAction = historyObject.UserAction;
                aapComment = historyObject.Comment;
                sysComment = historyObject.SysComment;
                actionDate = historyObject.ActionDate;
                //assignTo=historyObject.targetUser;
            } else {
                aapComment = cmdbHistory.Comment;
                if (cmdbHistory)
                    actionDate = cmdbHistory.StartDate;
            }

            try {
                var d = new Date(actionDate);
                if (Object.prototype.toString.call(d) === "[object Date]" && isNaN(d.getTime()) == false) {
                    actionDate = d.toLocaleTimeString() + ", " + d.toLocaleDateString();
                } else {
                    actionDate = ' -- ';
                }
            } catch (ex) {
                actionDate = ' -- ';
            }

            var item = {
                cmdb: cmdbHistory,
                userAction: userAction,
                assignTo: assignTo,
                aapComment: aapComment,
                sysComment: sysComment,
                actionDate: actionDate
            };
            t.assetStateHistory()[index] = item;
            t.assetStateHistory.update(item);
        });
    };
}
MODEL = new ASSET_STATE_HISTORY_MODEL();
ko.applyBindings(MODEL, $("#ko-assest-state-history")[0]);