var AssetStatusHistoryBusiness = {
    listID: [],
    viewStatusHistory: function () {
        //remove all histories
        MODEL.assetStatusHistory([]);
        var query = "resourcename=" + MODEL.assetName();
        var options = {
            success: getResourceStateHistoryByResourceNameCallback
        };
        HttpUtils.CallAjaxWithQueryString("GetResourceStateHistoryByResourceName", Module.CMDB, "", query, options, true);
        function getResourceStateHistoryByResourceNameCallback(historyJson) {
            var historyObject = Utils.jsonToData(historyJson);
            if (historyObject) {
                for (var i = 0; i < historyObject.length; i++) {
                    var item = {
                        cmdb: historyObject[i],
                        userAction: "",
                        aapComment: "",
                        assignTo: "-",
                        sysComment: "",
                        actionDate: null
                    };
                    MODEL.assetStatusHistory().push(item);
                }
                for (var i = 0; i < historyObject.length; i++) {
                    AssetStatusHistoryBusiness.processBuildStatusHistoryItem(historyObject[i], i);
                }
            }

            if (AssetStatusHistoryBusiness.listID.length === 0) {
                HttpUtils.CallAjaxWithQueryString("getByAssetName",
                    Module.ITAAPService,
                    SubPath.AssetStatusHistory,
                    'assetName=' + MODEL.assetName(),
                    { success: AssetStatusHistoryBusiness.processITAMHistory },
                    true);
            } else {
                HttpUtils.CallAjaxWithQueryString("getByAssetNameAndNotInListID",
                    Module.ITAAPService,
                    SubPath.AssetStatusHistory,
                    'assetName=' + MODEL.assetName() + 'IDs=' + Utils.dataToJson(AssetStatusHistoryBusiness.listID),
                    { success: AssetStatusHistoryBusiness.processITAMHistory },
                    true);
            }

            AssetStatusHistoryBusiness.listID = [];
        }

    },
    processITAMHistory: function (historyData) {
        historyData = Utils.jsonToData(historyData);
        for (var i = 0; i < historyData.length; i++) {
            var hisItem = historyData[i];
            var item = {
                cmdb: {
                    Comment: null,
                    EndDate: null,
                    LoginName: hisItem.userAction,
                    NewStatus: hisItem.newStatus,
                    Owner: hisItem.targetUser,
                    PreviousStatus: hisItem.previousStatus,
                    StartDate: hisItem.actionDate,
                    StausHistoryId: hisItem.statusId,
                    UserFullName: hisItem.userAction
                },
                userAction: hisItem.userAction,
                aapComment: hisItem.comment,
                assignTo: hisItem.targetUser,
                sysComment: hisItem.sysComment,
                actionDate: hisItem.actionDate
            };
            MODEL.assetStatusHistory().push(item);
        }
        MODEL.assetStatusHistory().sort(function (a, b) {
            return b.actionDate - a.actionDate;
        });

        MODEL.assetStatusHistory(MODEL.assetStatusHistory());
    },
    processBuildStatusHistoryItem: function (cmdbHistory, index) {
        var StausHistoryId = cmdbHistory.StausHistoryId;
        var query = "id=" + StausHistoryId;
        var options = {
            success: getLastStatusHistoryCallback
        };
        HttpUtils.CallAjaxWithQueryString("getAssetStatusHistoryById", Module.ITAAPService, SubPath.AssetStatusHistory, query, options, true);
        function getLastStatusHistoryCallback(assetStatusHistoryJson) {
            var historyObject = Utils.jsonToData(assetStatusHistoryJson);
            var userAction = " - ";
            var aapComment = " - ";
            var sysComment = CommonModelFunctions.getMessageSource('warning_action_is_performed_by_another_tool');
            var assignTo = " - ";
            var actionDate;
Utils
            var ITAMDate = historyObject ? AssetStatusHistoryBusiness.getDateFromDB(historyObject.actionDate) : null ;
            var AMDate = AssetStatusHistoryBusiness.getDateFromDB(cmdbHistory.StartDate);

            if (ITAMDate !== null && ITAMDate === AMDate &&
                historyObject.newStatus === cmdbHistory.NewStatus &&
                historyObject.previousStatus === cmdbHistory.PreviousStatus) {
                AssetStatusHistoryBusiness.listID.push(historyObject.id);
                if (cmdbHistory.Owner) {
                    userAction = cmdbHistory.Owner.TaskUser;
                    var assignNote = cmdbHistory.Owner.Details.Comment;
                    if (assignNote) {
                        assignNote = " (" + assignNote + ")";
                    } else {
                        assignNote = "";
                    }
                    assignTo = cmdbHistory.Owner.Details.Name + assignNote;
                }
                if (historyObject) {
                    userAction = historyObject.userAction;
                    aapComment = historyObject.comment;
                    sysComment = historyObject.sysComment;
                    actionDate = historyObject.actionDate;
                } else {
                    aapComment = cmdbHistory.Comment;
                    if (cmdbHistory)
                        actionDate = cmdbHistory.StartDate;
                }
            } else {
                if (historyObject) {
                    userAction = historyObject.userAction;
                    aapComment = historyObject.comment;
                    sysComment = historyObject.sysComment;
                    actionDate = historyObject.actionDate;
                } else {
                    actionDate = 0;
                }
            }


            var item = {
                cmdb: cmdbHistory,
                userAction: userAction,
                assignTo: assignTo,
                aapComment: aapComment,
                sysComment: sysComment,
                actionDate: actionDate
            };
            MODEL.assetStatusHistory()[index] = item;
            MODEL.assetStatusHistory(MODEL.assetStatusHistory());
        }
    },
    procesSaveStatusHistory: function (resourcename, state, target, note, newState) {
        var options = {
            success: getLastStatusHistory
        };
        var query = "resourcename=" + resourcename;
        HttpUtils.CallAjaxWithQueryString("GetLastStatusHistory", Module.CMDB, "", query, options, true, false);
        function getLastStatusHistory(lastHistoryJson) {
            var lastHistoryObject = Common.getJsonData(lastHistoryJson);

            if (!Common.isNull(lastHistoryObject)) {
                //Save History to local DB
                var satusHistory = {
                    stausHistoryId: lastHistoryObject.StausHistoryId,
                    assetName: resourcename,
                    newStatus: lastHistoryObject.NewStatus,
                    previousStatus: lastHistoryObject.PreviousStatus,
                    targetUser: target,
                    comment: note,
                    sysComment: state
                };
                AssetStatusHistoryBusiness.saveStatusHistory(satusHistory, newState);
            }
        }
    },
    saveStatusHistory: function (statusHistory, newState) {
        var submitObj = {
            stausHistoryId: statusHistory.stausHistoryId,
            assetName: statusHistory.assetName,
            newStatus: statusHistory.newStatus,
            previousStatus: statusHistory.previousStatus,
            userAction: Common.getCurrentUser(),
            targetUser: statusHistory.targetUser,
            comment: statusHistory.comment,
            sysComment: statusHistory.sysComment
        };
        var options = {
            success: insertStatusHistoryCallback
        };
        HttpUtils.CallAjax("insertStatusHistory", Module.ITAAPService, SubPath.AssetStatusHistory, submitObj, options, null, false);
        function insertStatusHistoryCallback(response) {
            if (Common.isNull(response)) {
                PopupBusiness.viewAutoWarning($("#btn-save-status-id"), CommonModelFunctions.getMessageSource('warning_save_status_history_fail'), 100);
            } else {
                PopupBusiness.viewAutoWarning($("#btn-save-status-id"), CommonModelFunctions.getMessageSource('warning_save_satus_history_successfull'));
                if (!Common.isNull(newState)) {
                    if (!Common.isNull(MODEL.asset())) {
                        MODEL.asset().Status(newState);
                    }

                }
            }
        }
    },
    getDateFromDB: function (time) {
        var date = new Date(time);
        return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes();
    }
};
loadModel = function () {
    var keyword = MODEL.externalTask.urlParameters().keyword;
    if (!Common.isNull(keyword)) {
        MODEL.assetName(keyword);
        AssetStatusHistoryBusiness.viewStatusHistory();
    }

};
