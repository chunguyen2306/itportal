function ASSET_DETTAIL_HISTORY_MODEL(){
    var t = this;

    t.assetName = ko.observable(Utils.getURLValue("assetName")).extend({notify: 'always'});
    t.assetDetailHistory = ko.observableArray();
    t.assetName.subscribe(function (assetName) {
        t.assetDetailHistory(null);
        _Link.pushAsset(_Link.assetDetailHistory, _L.title_asset_detail_history, assetName);

        APIData.Asset.GetAssetDetailHistory(assetName, function (res) {
            var arrHistory = res.data;

            var totalRecords = arrHistory.length;
            for (var i = 0; i < totalRecords; i++) {
                var oldDetailPlainObject = Utils.jsonToData(arrHistory[i].PreviousDetail);
                var newDetailPlainObject = Utils.jsonToData(arrHistory[i].NewDetail);
                Utils.buildChangedOfTwoObject(oldDetailPlainObject, newDetailPlainObject);
                var oldDetail = FuncAsset.buildAssetDetailAsHtml(oldDetailPlainObject);
                var newDetail = FuncAsset.buildAssetDetailAsHtml(newDetailPlainObject);
                //var htmlChanged = Utils.detectTableContentChanges(oldDetail, newDetail);
                arrHistory[i].PreviousDetail = oldDetail;
                arrHistory[i].NewDetail = newDetail;
            }

            t.assetDetailHistory(arrHistory);
        });

    });
    t.showDetail = function (sender, e) {
        var detail = $(e.currentTarget).next();
        var detailShowing = t.showDetail.showing;

        if(detailShowing == null){
            detail.show();
            detail.next().show();
            t.showDetail.showing = detail;
        } else {
            if(detail[0] == detailShowing[0]){
                detail.hide();
                detail.next().hide();
                t.showDetail.showing = null;
            } else {
                detail.show();
                detail.next().show();
                detailShowing.hide();
                detailShowing.next().hide();
                t.showDetail.showing = detail;
            }
        }
    };

    t.redirectDetail = function () {
        _Link.redirect(_Link.assetDetail, t.assetName());
    }

};

MODEL = new ASSET_DETTAIL_HISTORY_MODEL();
ko.applyBindings(MODEL, $("#ko-assest-detail-editor")[0]);
