/**
 * Created by LAP10266-local on 4/19/2017.
 */
(function(){

    var me = this;

    var lisVariable =  $('#lisVariable');
    var txbFormular =  $('textarea[name="txbFormular"]');
    var txbName =  $('input[name="txbName"]');

    this.initVariableBox = function(){
        for(var i =0; i < lisVariable.children().length; i++){
            var item = $(lisVariable.children()[i]);
            item.on('click', me.variableIten_OnClick)
        }
    };

    this.variableIten_OnClick = function () {

        txbFormular.val(txbFormular.val() + ' '+$(this).find('b').text());
    };

    this.init = function(){
        this.initVariableBox();
    };

    this.init();

})();
