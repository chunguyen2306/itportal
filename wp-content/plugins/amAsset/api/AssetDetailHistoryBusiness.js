var AssetDetailHistoryBusiness = {
    insertHistory: function (assetName, newDetail, previousDetail, comment, sysComment) {
//        var query = "assetname=" + assetName
//                + "&newDetail=" + newDetail
//                + "&previousDetail=" + previousDetail
//                + "&userAction=" + Common.getCurrentUser()
//                + "&comment=" + comment
//                + "&cmdbStatus=" + sysComment;
        var options = {
            success: insertHistorySuccessCallback
        };
        var submitObj = {
            forcePostParam: true,
            assetname: assetName,
            newDetail: newDetail,
            previousDetail: previousDetail,
            userAction: Common.getCurrentUser(),
            comment: comment,
            cmdbStatus: sysComment

        };
        HttpUtils.CallAjax("insertHistory", Module.ITAAPService, SubPath.AssetDetailHistory, submitObj, options);
        // HttpUtils.CallAjaxWithQueryString("insertHistory", Module.ITAAPService, SubPath.AssetDetailHistory, query, options);
        function insertHistorySuccessCallback(res) {

        }
    },
    viewDetailHistory: function () {
        var assetName = MODEL.assetName();
        var query = "assetName=" + assetName;
        var options = {
            success: viewDetailHistorySuccessCallback
        };
        HttpUtils.CallAjaxWithQueryString("getDetailHistory", Module.ITAAPService, SubPath.AssetDetailHistory, query, options);
        function viewDetailHistorySuccessCallback(jsonData) {
            var historyObject = Common.getKoObject(jsonData);
            if (!Common.isNull(historyObject())) {
                var totalRecords = historyObject().length;
                for (var i = 0; i < totalRecords; i++) {
                    var oldDetailPlainObject = Common.tryToPlainObject(historyObject()[i].previousDetail());
                    var newDetailPlainObject = Common.tryToPlainObject(historyObject()[i].newDetail());
                    Utils.buildChangedOfTwoObject(oldDetailPlainObject, newDetailPlainObject);                 
                    var oldDetail = FuncAsset.buildAssetDetailAsHtml(oldDetailPlainObject);
                    var newDetail = FuncAsset.buildAssetDetailAsHtml(newDetailPlainObject);
                    //var htmlChanged = Utils.detectTableContentChanges(oldDetail, newDetail);                  
                    historyObject()[i].previousDetail(oldDetail);
                    historyObject()[i].newDetail(newDetail);
                }
                MODEL.assetDetailHistory(historyObject);
            } else {
                MODEL.assetDetailHistory(null);
            }
        }
    }
};
loadModel = function () {
    var keyword = MODEL.externalTask.urlParameters().keyword;
    if (!Common.isNull(keyword)) {
        MODEL.assetName(keyword);
        AssetDetailHistoryBusiness.viewDetailHistory();
    }

};
