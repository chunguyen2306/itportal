var AssetDetailBusiness = {
    save: function (obj) {
        var note = obj.note();
        if (Common.isNull(note)) {
            PopupBusiness.viewConfirm(null, CommonModelFunctions.getMessageSource('warning_empty_note_change_detail'), null);
            $('#form_new_change_detail_note').focus();
        } else {
            var assetType = obj.assetModel();
            var assetName = Common.getValueOfObject(obj.assetName);
            if (Common.isNull(assetType)) {
                PopupBusiness.viewConfirm(null, CommonModelFunctions.getMessageSource('warning_asset_detail_is_invalid'), null);
            } else {
                var cpus = Common.getValueOfObject(obj.assetDetail.CPU);
                var rams = Common.getValueOfObject(obj.assetDetail.RAM);
                var hdds = Common.getValueOfObject(obj.assetDetail.HDD);
                var vgas = Common.getValueOfObject(obj.assetDetail.VGA);
                var dvds = Common.getValueOfObject(obj.assetDetail.CD_DVD);
                var obj = {
                    resourcename: assetName,
                    CPU: Utils.buildAssetDetail(cpus),
                    RAM: Utils.buildAssetDetail(rams),
                    VGA: Utils.buildAssetDetail(vgas),
                    HDD: Utils.buildAssetDetail(hdds),
                    DVD: Utils.buildAssetDetail(dvds)
                };
                var options = {
                    success: updateAssetDetailSuccessCallback,
                    error: updateAssetDetailErrorCallback
                };
                HttpUtils.CallAjax("UpdateCustomAssetDetail", Module.CMDB, "", obj, options);
                function updateAssetDetailSuccessCallback(jsonData) {
                    var link = '<a href="?view=assets/assetDetailHistory&keyword=' + assetName + '">'
                        + CommonModelFunctions.getMessageSource('link_title_asset_detail_history') + '</a>';

                    //Insert history
                    var newDetail = jsonData;
                    var previousDetail = MODEL.currentDetail();
                    var comment = MODEL.note();
                    AssetDetailHistoryBusiness.insertHistory(assetName, newDetail, previousDetail, comment, "Successfully");
                    PopupBusiness.viewConfirm(null, CommonModelFunctions.getMessageSource('warning_update_successfull') + "<br/>"
                        + link, null);

                }
                function updateAssetDetailErrorCallback() {
                    //Insert history
                    var detailObject = Utils.jsonToData(jsonData);
                    var newDetail = FuncAsset.buildAssetDetailAsHtml(detailObject);
                    var previousDetail = MODEL.currentDetail();
                    var comment = MODEL.note();
                    AssetDetailHistoryBusiness.insertHistory(assetName, newDetail, previousDetail, comment, "Error");
                    PopupBusiness.viewConfirm(null, CommonModelFunctions.getMessageSource('warning_update_fail'), null);
                }
            }
        }

    },
    searchAsset: function () {
        var assetName = Common.getValueOfObject(MODEL.assetName);
        if (Common.isNull(assetName)) {
            PopupBusiness.viewConfirm(null, CommonModelFunctions.getMessageSource('warning_asset_name_is_empty'), null);
        } else {
            try {
                var query = "resourcename=" + assetName;
                var options = {
                    success: loadAssetDetailSuccessCallback,
                    error: loadAssetDetailErrorCallback
                };
                HttpUtils.CallAjaxWithQueryString("GetAllAssetDetail", Module.CMDB, "", query, options);
                function loadAssetDetailSuccessCallback(jsonData) {
                    if (Common.isNull(jsonData)) {
                        PopupBusiness.viewConfirm(null, CommonModelFunctions.getMessageSource('warning_asset_detail_not_found'), null);
                        MODEL.isLoadAsset(false);
                    } else {
                        MODEL.isLoadAsset(true);
                        var detailObject = Utils.jsonToData(jsonData);
                        MODEL.currentDetail(jsonData);
                        if (!Common.isNull(detailObject) && !Common.isNull(detailObject.WorkstationField)) {
                            var CPU = detailObject.WorkstationField.CentralProcessingUnit;
                            var RAM = detailObject.WorkstationField.RandomAccessMemory;
                            var VGA = detailObject.WorkstationField.GraphicCard;
                            var HDD = detailObject.WorkstationField.HardDiskDrive;
                            var DVD = detailObject.WorkstationField.OpticalDrive;
                            MODEL.assetDetail.CPU(CPU);
                            MODEL.assetDetail.RAM(RAM);
                            MODEL.assetDetail.VGA(VGA);
                            MODEL.assetDetail.HDD(HDD);
                            MODEL.assetDetail.CD_DVD(DVD);
                        } else {
                            PopupBusiness.viewConfirm(null, CommonModelFunctions.getMessageSource('warning_asset_detail_not_found'), null);
                        }
                        //Load status
                        var getAssetStatusOption = {
                            success: getAssetStatusSuccess
                        };

                        HttpUtils.CallAjaxWithQueryString("GetResourceStateByAssetName", Module.CMDB, "", query, getAssetStatusOption, true);
                        function getAssetStatusSuccess(statusJson) {
                            if (!Common.isNull(statusJson)) {
                                MODEL.isLoadAsset(true);
                                var status = Common.getKoObject(statusJson);
                                MODEL.assetModel(Common.getValueOfObject(status.ResModel));
                            } else {
                                MODEL.isLoadAsset(false);
                                PopupBusiness.viewConfirm(null, CommonModelFunctions.getMessageSource('warning_asset_detail_is_invalid'), null);
                            }
                        }
                    }
                }
                function loadAssetDetailErrorCallback() {
                    PopupBusiness.viewConfirm(null, CommonModelFunctions.getMessageSource('warning_asset_detail_not_found'), null);

                }
            } catch (e) {
                PopupBusiness.viewConfirm(null, CommonModelFunctions.getMessageSource('warning_asset_detail_not_found'), null);
            }
        }
    }
};
loadModel = function () {
    var keyword = MODEL.externalTask.urlParameters().keyword;
    if (!Common.isNull(keyword)) {
        MODEL.assetName(keyword);
        AssetDetailBusiness.searchAsset();
    }

};

