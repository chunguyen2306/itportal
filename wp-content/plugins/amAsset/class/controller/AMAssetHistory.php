<?php

if(!class_exists('AMAssetHistory')){
    
    // MUST USE
    import ('theme.package.Abstracts.AbstractController');
    //import your package here
    
    class AMAssetHistory extends AbstractController {
        protected function init(){
                        
            $this->dir = substr(__DIR__, 0, strlen(__DIR__)-strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'Asset History/list'
            );
        }

        public function list_action($req, $post){
             //Do something here
            importScript('mAssetHistory', 'plugin.amAsset.js.api.AssetDetailHistoryBusiness');
            importScript('mAssetDetail', 'plugin.amAsset.js.model.AssetDetailHistory');
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}
?>