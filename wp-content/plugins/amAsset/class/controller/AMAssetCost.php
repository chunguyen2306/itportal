<?php

if(!class_exists('AMAssetCost')){
    
    // MUST USE
    import ('theme.package.Abstracts.AbstractController');
    //import your package here
        
    import ('plugin.amAsset.class.factory.AMAssetCostVariableFactory');
    import ('plugin.amAsset.class.factory.AMAssetCostFormularFactory');
    
    
    class AMAssetCost extends AbstractController {
        protected function init(){
            $this->variables = new AMAssetCostVariableFactory();
            $this->formulars = new AMAssetCostFormularFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__)-strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'Asset Cost/variables',
                'variable' => 'Asset Cost/variables',
                'formular' => 'Asset Cost/formular',
                'formular_addnew' => 'Asset Cost/formularAddNew'
            );

            $this->_model('subMenu', array(
                array( 'type' => '', 'name' => 'My Variables' ),
                array( 'type' => 'formular', 'name' => 'My Formular' ),
            ));
        }

        public function do_formular_newupdate($req, $post){
            if(isset($post['btnCancel'])){
                wp_redirect( $this->_getPath().'&type=formular' );
                exit();
            }

            $txbName = $post['txbName'];
            $txbFormular = $post['txbFormular'];
            $result = null;
            //$chbIsShow = $post['chbIsShow'];

            $data = array(
                Name => $txbName,
                Formular => $txbFormular,
                IsShowOnAsset => false
            );

            if(isset($req['ID'])) {
                //Update Action
                $result = $this->formulars->update($data, array(
                   ID => $req['ID']
                ));
            } else {
                //Add New Action
                $result = $this->formulars->insert($data);
            }

            if($result != false){

                $tmpFile = FROpen('plugin.amAsset.');

                wp_redirect( $this->_getPath().'&type=formular' );
            }
        }

        public function do_variable_edit($req, $post){
            if(isset($post['btnEdit'])) {
                $ID = isset($post['txbID']) ? (int)$post['txbID'] : 0;
                if ($ID > 0) {
                    $this->_model('editID', $ID);
                }
            } else if(isset($post['btnDelete'])) {
                $ID = isset($post['txbID']) ? (int)$post['txbID'] : 0;
                if ($ID > 0) {
                    $this->variables->delete(array(
                        'ID' => $ID
                    ));
                }
            }
        }

        public function do_variable_addNew($req, $post){
            $name = isset($post['txbName'])?$post['txbName']:"";
            $value = isset($post['txbValue'])?$post['txbValue']:"";
            $description = isset($post['txbDescription'])?$post['txbDescription']:"";
            if($name != "" && $value != ""){
                $this->variables->insert(array(
                    Name => $name,
                    Value => $value,
                    Description => $description
                ));
            }
        }

        public function do_variable_update($req, $post){
            $id = isset($post['txbID'])?$post['txbID']:"";
            $name = isset($post['txbName'])?$post['txbName']:"";
            $value = isset($post['txbValue'])?$post['txbValue']:"";
            $description = isset($post['txbDescription'])?$post['txbDescription']:"";
            if($name != "" && $value != ""){
                $this->variables->update(array(
                    Name => $name,
                    Value => $value,
                    Description => $description
                ), array(
                    'ID' => $id
                ));
            }
        }

        public function list_action($req, $post){
             //Do something here
            $this->_model('variables', $this->variables->getAllBy());
        }

        public function formular_action($req, $post){
            //Do something here
            $this->_model('formulars', $this->formulars->getAllBy());
        }

        public function formular_addnew_action($req, $post){
            $this->_model('variables', $this->variables->getAllBy());
        }

        protected function beforeGetView($req, $post)
        {

        }
    }
}

add_filter('init_amAsset_admin_menu', 'init_amAssetCost_admin_menu');
function init_amAssetCost_admin_menu($adminMenu){
    array_push($adminMenu[0]['child'], array(
            'pageTitle' => 'Asset Cost',
            'menuTitle' => 'Asset Cost',
            'capability'=> 'manage_options',
            'menuSlug' => 'am_asset_using_cost',
            'handler' => array(new AMAssetCost(array(
                'slug' => 'am_asset_using_cost'
            )), 'index')
        ));

    return $adminMenu;
}
?>