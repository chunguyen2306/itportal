<?php

class AMAssetDetail extends AbstractController {

    public $packet;
    public $dir;

    public function __construct() {
        global $amAsset;
        $this->dir = $amAsset->dir;
        $this->packet = "plugin." . basename(plugin_dir_path($amAsset->file));
    }

    public function importCore(){
        importCSS('pCss', $this->packet . '.css.style');
        importScript('lHttpRequest', 'theme.js.Libs.HttpRequest');
        importScript('lAjax', 'theme.js.Data.ajax');
        importScript('lPopupBusiness', 'theme.js.PopupBusiness');
        importScript('lkoCustom', $this->packet . '.js.lib.ko-custom');
        importScript('li18n', $this->packet . '.js.api.i18n');
        importScript('lCommon', $this->packet . '.js.lib.Common');
        importScript('lCMDB', $this->packet . '.js.api.APIData.CMDB');
        importScript('aITAM', $this->packet . '.js.api.APIData.ITAM');
        importScript('aLink', $this->packet . '.js.app.util-function');
        importScript('lMoment', $this->packet . '.js.lib.moment');
    }

    public function index() {
        $this->importCore();
        importScript('mAssetHistory', $this->packet . '.js.api.AssetDetailHistoryBusiness');
        importScript('mAssetDetail', $this->packet . '.js.model.AssetDetail');

        echo file_get_contents($this->dir . 'view/asset-detail-editor.php');
    }

    public function detailHistory(){
        $this->importCore();
        importScript('mAssetHistory', $this->packet . '.js.api.AssetDetailHistoryBusiness');
        importScript('mAssetDetail', $this->packet . '.js.model.AssetDetailHistory');

    }

    public function assetState(){
        $this->importCore();
        importScript('mAssetState', $this->packet . '.js.model.AssetState');

        echo file_get_contents($this->dir . 'view/asset-state.php');
    }
}