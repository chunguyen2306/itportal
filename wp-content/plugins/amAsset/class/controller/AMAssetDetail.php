<?php

if (!class_exists('AMAssetDetail')) {

    // MUST USE
    import('theme.package.Abstracts.AbstractController');

    //import your package here

    class AMAssetDetail extends AbstractController {
        protected function init() {

            $this->dir         = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array('list' => 'assetDetail/list');
        }

        public function list_action($req, $post) {
            importScript('mAssetDetail', 'plugin.amAsset.js.model.AssetDetail');
        }

        protected function beforeGetView($req, $post) {
        }
    }
}
?>