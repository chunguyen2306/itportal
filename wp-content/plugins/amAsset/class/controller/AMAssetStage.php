<?php

if(!class_exists('AMAssetStage')){
    
    // MUST USE
    import ('theme.package.Abstracts.AbstractController');
    //import your package here
    
    class AMAssetStage extends AbstractController {
        protected function init(){
                        
            $this->dir = substr(__DIR__, 0, strlen(__DIR__)-strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'Asset Stage/list'
            );
        }

        public function list_action($req, $post){
             //Do something here
            importScript('mAssetState', 'plugin.amAsset.js.model.AssetState');
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}
?>