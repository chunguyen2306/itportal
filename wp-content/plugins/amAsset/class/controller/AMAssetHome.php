<?php

if(!class_exists('AMAssetHome')){
    
    // MUST USE
    import ('theme.package.Abstracts.AbstractController');
    //import your package here
    
    class AMAssetHome extends AbstractController {
        protected function init(){
                        
            $this->dir = substr(__DIR__, 0, strlen(__DIR__)-strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'assetHome/assetHome'
            );
        }

        public function list_action($req, $post){
            importScript('mAssetDetail', 'plugin.amAsset.js.model.assetHome');
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}
?>