<?php

if(!class_exists('AMAssetDetailHistory')){
    
    // MUST USE
    import ('theme.package.Abstracts.AbstractController');
    //import your package here
    
    class AMAssetDetailHistory extends AbstractController {
        protected function init(){
            $this->dir = substr(__DIR__, 0, strlen(__DIR__)-strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'assetDetailHistory/list'
            );
        }

        public function list_action($req, $post){
            importScript('mAssetDetail', 'plugin.amAsset.js.model.AssetDetailHistory');
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}
?>