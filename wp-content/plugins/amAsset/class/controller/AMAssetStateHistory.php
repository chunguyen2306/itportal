<?php

if(!class_exists('AMAssetStateHistory')){
    
    // MUST USE
    import ('theme.package.Abstracts.AbstractController');
    //import your package here
    
    class AMAssetStateHistory extends AbstractController {
        protected function init(){
                        
            $this->dir = substr(__DIR__, 0, strlen(__DIR__)-strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'assetStateHistory/listAssetStateHistory'
            );
        }

        public function list_action($req, $post){
             //Do something here
            importScript('mAssetState', 'plugin.amAsset.js.model.AssetStateHistory');
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}
?>