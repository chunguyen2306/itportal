<?php
if(!class_exists('AMAssetCostFormularFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMAssetCostFormularFactory extends AbstractDatabase {

        public static $table_name = 'am_assetcostformular';

        public function tableInfo(){
            return array(
                'table_name' => AMAssetCostFormularFactory::$table_name
            );
        }
        public function getByID($id){
            return $this->getOneBy(array(
                "ID" => $id
            ));
        }

        public function getByName($name){
            return $this->getOneBy(array(
                "Name" => $name
            ));
        }
    }
}
?>