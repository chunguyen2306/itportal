<?php

if(!class_exists('AMAssetAjax')){
    class AMAssetAjax{
        public function ServiceBus($params){
            $uData = $params["u"];
            $pData = $params["p"];
            $vData = $params["v"];

            if($uData == null){
                http_response_code(403); // Forbidden
                exit("Missing required parameter (null)");
            }

            $postData = [];
            if(isset($pData)){

                if(($p = json_decode(stripslashes($pData), true)) == false){
                    echo "JSON decode failed: p = ";
                    exit ($pData);
                }

                if(($v = json_decode(stripslashes($vData), true)) == false){
                    echo"JSON decode failed: u = ";
                    exit ($vData);
                }

                $n = sizeof($p);
                if($n == 0)
                    exit ("Missing required parameter (no parameter)");

                for($i = 0 ; $i < $n; $i++){
                    $postData[$p[$i]] = $v[$i];
                }
            }

            $_URL = [
                "cmdb" => "http://localhost:8175/API/cmdb.ashx",
                "cmdbex" => "http://localhost:8175/API/cmdbex.ashx",
                "asset" => "http://localhost:8176/API/asset.ashx"
            ];

            $u = $_URL[$uData];
            if($u == null){
                http_response_code(404); // Forbidden
                exit("Server not found: " . $uData);
            }

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($postData)
                )
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($u, false, $context);
            if ($result === FALSE) {
                http_response_code(403); // Forbidden
                exit("Missing require parameter");
            }

            return $result;
        }
    }
}

return new AMAssetAjax();


















