<?php

if(!class_exists('AMAssetStateHistoryAjax')){
    import('plugin.amAsset.class.factory.AMAssetStateHistoryFactory');
    class AMAssetStateHistoryAjax{
        public function GetAssetStateHistory($params){
            $assetName = $params['assetName'];
            $fac = new AMAssetStateHistoryFactory();
            $result = $fac->query(array(
                'filter' => array(
                    array("assetName = '".$assetName ."'",'')
                )
            ));

            return $result;
        }
        public function GetAssetStateHistoryByCMDBHistoryID($params){
            $cmdbHistoryID = $params['CMDBHistoryID'];
            $fac = new AMAssetStateHistoryFactory();
            $result = $fac->query(array(
                'filter' => array(
                    array("StateId = ".$cmdbHistoryID ,'')
                )
            ));

            //var_dump(sizeof($result) ? $result[0] : new stdClass());
            return sizeof($result) ? $result[0] : new stdClass() ;
        }
        public function InsertStateHistory($params){
            try{
                $jsonData= $params['data'];
                if($jsonData == null)
                    return AMRespone::paramsInvalid();
                $data = am_json_decode($jsonData, 1);
                if($data == null)
                    return  AMRespone::jsonFailed($jsonData);

                $fac = new AMAssetStateHistoryFactory();
                $result = $fac->insert($data);

                if($result || $result === 0)
                    return new AMRespone($data);
                else
                    return AMRespone::dbFailed($data);
            }catch (Exception $ex){
                return AMRespone::exception($params, $ex);
            }
        }

    }
}

return new AMAssetStateHistoryAjax();


















