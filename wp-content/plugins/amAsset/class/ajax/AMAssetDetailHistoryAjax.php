<?php

if(!class_exists('AssetDetailHistoryAjax')){
    import('plugin.amAsset.class.factory.AMAssetDetailHistoryFactory');
    class AssetDetailHistoryAjax{

        public function GetAssetDetailHistory($params){
            $assetName = $params['assetName'];
            $fac = new AMAssetDetailHistoryFactory();

            $result = $fac->query(array(
                'filter' => array(
                    array("AssetName = '".$assetName ,"'")
                )
            ));

            return $result === false ? AMRespone::dbFailed($assetName) : new AMRespone($result );
        }

        public function InsertAssetDetailHistory($params){
            try{
                $jsonData= $params['data'];
                if($jsonData == null)
                    return AMRespone::paramsInvalid();

                $data = am_json_decode($jsonData, 1);
                if($data == null)
                    return AMRespone::jsonFailed($jsonData);

                $fac = new AMAssetDetailHistoryFactory();
                $result = $fac->insert($data);

                if($result || $result === 0)
                    return new AMRespone($data);
                else
                    return AMRespone::dbFailed($data);
            } catch (Exception $ex){
                return AMRespone::exception($params, $ex);
            }
        }

    }
}

return new AssetDetailHistoryAjax();


















