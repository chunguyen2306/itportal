<?php
if(!class_exists('AMAssetCostAjax')){

    import('plugin.amDefaultAsset.class.factory.AMAssetCostVariableFactory');
    import('theme.package.AJAX.AjaxResult');

    class AMAssetCostAjax {

        private $db = null;

        public function __construct($args = array()) {
            $this->db = new AMAssetCostVariableFactory();
        }

        public function getAll(){
            return new AjaxResult($this->db->getAllBy());
        }

        public function getById($agrs){
            $id = $agrs['id'];
            $result = $this->db->getOneBy(array(
               "ID" => $id
            ));

            return new AjaxResult($result);
        }

        public function deleteById($agrs){
            $id = $agrs['id'];
            $result = new AjaxResult();
            if(is_numeric($id)){
                $this->db->deleteBy(array(
                    'ID' => $id
                ));
            } else {
                $result->isError = true;
                $result->message = "Cannot delete all record";
            }
        }

        public function updateById($agrs){
            $id = $agrs['id'];
            $result = new AjaxResult();
            if(is_numeric($id)){
                $this->db->updateBy(array(
                    'ID' => $id
                ));
            } else {
                $result->isError = true;
                $result->message = "Cannot delete all record";
            }
        }
    }

    return new AMJobBranchAjax();
}