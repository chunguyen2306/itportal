<?php
if(!class_exists('AMCostFormular')) {

    import ('plugin.amAsset.class.factory.AMAssetCostVariableFactory');

    class AMCostFormular {
        private $variables;

        public function __construct()
        {
            $variables = new AMAssetCostVariableFactory();
        }

        function getUsedTime($resourceName){
            return 0;
        }

        function getInvestTime($resourceName){
            return 0;
        }

        function getVariable($name){
            $var = $this->variables->getOneBy(array(
               Name => $name
            ));

            if($var != null){
                if($var->Description == 'System'){
                    return call_user_func('get'.$var->Name);
                } else {
                    return $var->Value;
                }
            } else {
                return $var;
            }
        }
    }
}