<?php

/*
  Plugin Name: Asset Detail
  Plugin URI:
  Description: Quản lý cấu hình tài sản
  Version: 1.0
  Author: TienNV6
  Author URI:
  License:
  Text Domain: am-user
*/
if(!class_exists('AMAsset')) {
    if (!function_exists('import')) {
        require_once get_template_directory() . '/package/import.php';
    }
    import('theme.package.Abstracts.AbstractPlugin');
    import('plugin.amAsset.class.controller.AMAssetHome');
    import('plugin.amAsset.class.controller.AMAssetDetail');
    import('plugin.amAsset.class.controller.AMAssetDetailHistory');
    import('plugin.amAsset.class.controller.AMAssetState');
    import('plugin.amAsset.class.controller.AMAssetStateHistory');

    class AMAsset extends AbstractPlugin
    {

        public function __construct()
        {
            $this->ajaxName = 'amAsset';
            $this->pluginName = 'amAsset';
            $this->adminMenu = array(
                array(
                    'pageTitle' => 'Tài sản',
                    'menuTitle' => 'Tài sản',
                    'capability'=> 'manage_options',
                    'menuSlug' => 'am_asset',
                    'handler' => array(new AMAssetHome(array(
                        'slug' => 'am_asset'
                    )), 'index'),
                    'icon' => 'dashicons-list-view',
                    'child' => array(
                        array(
                            'pageTitle' => 'Tìm tài sản',
                            'menuTitle' => 'Tìm tài sản',
                            'capability'=> 'manage_options',
                            'menuSlug' => 'am_asset',
                            'defeault' => true
                        ),
                        array(
                            'pageTitle' => 'Cấu hình tài sản',
                            'menuTitle' => 'Cấu hình',
                            'capability'=> 'manage_options',
                            'menuSlug' => 'am_asset_detail',
                            'handler' => array(new AMAssetDetail(array(
                                'slug' => 'am_asset_detail'
                            )), 'index')
                        ),
                        array(
                            'pageTitle' => 'Trạng thái tài sản',
                            'menuTitle' => 'Tình trạng',
                            'capability'=> 'manage_options',
                            'menuSlug' => 'am_asset_state',
                            'handler' => array(new AMAssetState(array(
                                'slug' => 'am_asset_state'
                            )), 'index')
                        ),
                        array(
                            'pageTitle' => 'Nhật ký cấu hình tài sản',
                            'menuTitle' => 'Nhật ký cấu hình',
                            'capability'=> 'manage_options',
                            'menuSlug' => 'am_asset_detail_history',
                            'handler' => array(new AMAssetDetailHistory(array(
                                'slug' => 'am_asset_detail_history'
                            )), 'index')
                        ), // detail history
                        array(
                            'pageTitle' => 'Nhật ký trạng thái tài sản',
                            'menuTitle' => 'Nhật ký trạng thái',
                            'capability'=> 'manage_options',
                            'menuSlug' => 'am_asset_state_history',
                            'handler' => array(new AMAssetStateHistory(array(
                                'slug' => 'am_asset_state_history'
                            )), 'index')
                        ) // state history
                    )
                ), // manager
            );
            parent::__construct();
        }

        function add_admin_menu()
        {
            require_once $this->dir . 'class/controller/class_asset_detail.php';
            $img = plugin_dir_url(__FILE__) . "/images/icon.png";
            $gui = new AMAssetDetail();

            add_menu_page('Tài sản', 'Tài sản', 'manage_options', "asset", array($gui, 'index'), $img, 20);
            add_submenu_page('asset', 'Cấu hình tài sản', 'Cấu hình', 'manage_options', 'asset', array($gui, 'index'));
            add_submenu_page('asset', 'Lịch sử thay đổi cấu hình', 'Lịch sử cấu hình', 'manage_options', 'asset-history', array($gui, 'detailHistory'));
            add_submenu_page('asset', 'Trạng thái tài sản', 'Trạng thái', 'manage_options', 'asset-state', array($gui, 'assetState'));
        }

        protected function init_custom_hook(){
            // TODO: Implement init_custom_hook() method.
        }

        protected function custom_init() {
            importScript('lHttpRequest', 'theme.js.Libs.HttpRequest');
            importScript('lAjax', 'theme.js.Data.ajax');
            importScript('lPopupBusiness', 'theme.js.PopupBusiness');
            importScript('lkoCustom', 'theme.js.Libs.ko-custom');
            importScript('lkasdasoCustom', 'theme.js.Libs.test');
            importScript('li18n', 'plugin.amAsset.js.api.i18n');
            importScript('lMoment', 'theme.js.Libs.moment');
            importScript('lCommon', 'theme.js.Libs.Common');
            importScript('aUtil', 'plugin.amAsset.js.app.util-function');
            importScript('aLink', 'plugin.amAsset.js.app.link');
            importScript('api', 'plugin.amAsset.js.api.API');
            importScript('lCMDB', 'plugin.amAsset.js.api.APICMDB');
            importScript('aITAM', 'plugin.amAsset.js.api.APIITAM');

        }
    }



}
$amAsset            = new AMAsset();
$GLOBALS['amAsset'] = $amAsset;








// hide update notifications
function remove_core_updates(){
    global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}
add_filter('pre_site_transient_update_core','remove_core_updates'); //hide updates for WordPress itself
add_filter('pre_site_transient_update_plugins','remove_core_updates'); //hide updates for all plugins
add_filter('pre_site_transient_update_themes','remove_core_updates'); //hide updates for all themes
