<div id="ko-asset-detail-history">
    <div class="row form-group form-horizontal">
        <label for="asset-name-id" class="col-sm-3 control-label" data-bind="text: _L.form_label_asset_name"></label>
        <div class="col-sm-6 search-box" data-bind="searchAPI: APIData.CMDB.GetListAsset, search: onSearchSelected">
            <input class="form-control search-component">
            <button class="search-component btn btn-primary"></button>
            <ul class="search-component"></ul>
        </div>
    </div>
    <div class="row panel panel-body">
        <div data-bind="if: CommonModelFunctions.checkArrayBinding(assetDetailHistory)">
            <div class="panel-heading text-uppercase asset-sub-heading"
                 data-bind="text: _L.title_asset_detail_history +' ('+assetDetailHistory().length+')'"></div>
            <table class="table" id="asset-detail-history-id">
                <thead>
                <th style="width: 5%" data-bind="text: _L.table_head_index"></th>
                <th style="width: 20%" data-bind="text: _L.table_head_action_user"></th>
                <th style="width: 25%" data-bind="text: _L.table_head_note"></th>
                <th style="width: 25%" data-bind="text: _L.table_head_action_date"></th>
                <th style="width: 25%" data-bind="text: _L.table_head_result"></th>
                </thead>
                <tbody data-bind="foreach: assetDetailHistory()">
                <tr data-bind="click: $root.showDetail" class="asset-action-row">
                    <td data-bind="text: $index()+1"></td>
                    <td data-bind="text: UserAction"></td>
                    <td data-bind="text: Comment"></td>
                    <td data-bind="assetDate: ActionDate"></td>
                    <td data-bind="text: SysComment"></td>
                </tr>
                <tr class="histoy-driff-head">
                    <th colspan="3" data-bind="text: _L.header_asset_detail_prv"></th>
                    <th colspan="2" data-bind="text: _L.header_as;lset_detail_new"></th>
                </tr>
                <tr class="histoy-driff">
                    <th colspan="3" data-bind="html: PreviousDetail"></th>
                    <th colspan="2" data-bind="html: NewDetail"></th>
                </tr>
                </tbody>
            </table>
        </div>
        <div data-bind="ifnot: CommonModelFunctions.checkArrayBinding(assetDetailHistory)">
            <div class="alert alert-warning" data-bind="text: _L.warning_only_display_asset_has_changed"></div>
        </div>
    </div>
</div>