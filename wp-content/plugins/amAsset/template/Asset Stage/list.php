
<div id="ko-assest-status">
    <div class="form-group form-horizontal">
        <label for="asset-name-id" class="col-sm-2 control-label" data-bind="text: __L.form_label_asset_name"></label>
        <div class="col-sm-6 search-box" data-bind="doSearch: APICMDB.GetListAsset, onSelect: onSearchSelected, default: 'cpu10106'">
            <input class="form-control search-component">
            <button class="search-component btn btn-primary">Xem</button>
            <ul class="search-component"></ul>
        </div>

    </div>
    <div  data-bind="if: isLoadAsset ">
        <div class="col-md-12 panel panel-body">
            <div class="panel-default">
                <div class="col-md-12">
                    <div class="panel-heading text-uppercase asset-sub-heading"
                         data-bind="text: __L.title_asset_current_status"></div>
                    <table class="table">
                        <tr data-bind="ifnot: asset().isComponent()">
                            <td style="width: 120px;" class="asset-font-bold" data-bind="text: __L.table_head_ownner"></td>
                            <td data-bind="text: asset().Owner"></td>
                            <td style="width: 120px;" class="asset-font-bold" data-bind="text: __L.table_head_deparment"></td>
                            <td data-bind="text: asset().Department"></td>
                        </tr>
                        <tr>
                            <td class="asset-font-bold" data-bind="text: __L.table_head_asset_type"></td>
                            <td data-bind="text: asset().ResModel"></td>
                            <td class="asset-font-bold" data-bind="text: __L.table_head_asset_model"></td>
                            <td data-bind="text: asset().ResType"></td>

                        </tr>
                        <tr>
                            <td class="asset-font-bold" data-bind="text: __L.table_head_asset_parent"></td>
                            <td data-bind="text: asset().Parent"></td>
                            <td class="asset-font-bold" data-bind="text: __L.table_head_asset_status"></td>
                            <td data-bind="text: asset().Status"></td>
                        </tr>

                    </table>
                    <div data-bind="checkPrivilege:'admin,helpdesk,storekeeper'" style="text-align: right">
                        <a data-bind="html: __L.link_asset_status_history,
                            attr:{'href':'?view=assets/assetStatusHistory&keyword='+asset().ResId()}" ></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel-heading text-uppercase asset-sub-heading"
                         data-bind="text: __L.table_header_view_asset_detail"></div>
                    <div data-bind="html: asset().resDetail">
                    </div>
                    <div>
                        <table class="asset-table-detail">
                            <tr>
                                <td class="asset-td-header">MAC Address</td>
                                <td>
                                    <form class="form-inline">
                                        <textarea style="width: 400px; height:200px; float: left; margin-right: 20px;" class="form-control" data-bind="value: asset().MACAddress" ></textarea>
                                        <input type="button" data-bind="roleAction: updateMACAddress,
                                    checkValidUser: 'default',
                                    role: 'helpdesk,storekeeper,audit,admin'"
                                               class="btn btn-primary" value="Thay đổi" />
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div data-bind="checkPrivilege:'admin,helpdesk,storekeeper'" style="text-align: right">
                        <a data-bind="html: __L.title_asset_detail,attr:{'href':'?view=assets/assetDetail&keyword='+asset().ResId()}" ></a>
                    </div>

                </div>

                <div class="col-md-12" data-bind="checkPrivilege:'admin,helpdesk,storekeeper'">
                    <div class="panel-heading text-uppercase asset-sub-heading"
                         data-bind="text: __L.title_asset_change_status"></div>
                    <div data-bind="ifnot: canEditStatus">
                        <div data-bind="html: __L.warning_can_not_edit_status"></div>

                    </div>
                    <div data-bind="if: canEditStatus">
                        <div  class="asset-panel-body form-horizontal panel-default col-md-12">
                            <div class="col-md-6 asset-vertical-middle">
                                <table class="table borderless asset-table-center">
                                    <tr>
                                        <td></td>

                                        <td  data-bind="text: __L.table_head_asset_status"></td>

                                        <td>
                                            <input id="btn-save-status-id" class="form-control" data-bind="vngSearch: newState,value:newState,
                                     searchFunction: CommonModelFunctions.koExtensions.searchAssetState,
                                     changedFunction: CommonModelFunctions.koExtensions.changeAssetStatus"/>
                                        </td>
                                    </tr>
                                    <tr data-bind="if: displayOption">
                                        <td style="width: 12px;"><input value="User" type="radio" name="targetOwner" data-bind="checked:target "/></td>
                                        <td style="width: 150px;" dat data-bind="text: __L.title_assign_to_user"></td>

                                        <td>
                                            <input  class="form-control" data-bind="event:{'focus': userForcus},
                                            vngSearch: toUser, value: toUser,
                                            searchFunction: CommonModelFunctions.koExtensions.searchUser"/>
                                        </td>
                                    </tr>
                                    <tr data-bind="if: displayOption">
                                        <td ><input id="form_new_note" type="radio" value="Asset"  name="targetOwner" data-bind="checked: target"/></td>

                                        <td   data-bind="text: __L.title_assign_to_asset"></td>

                                        <td>
                                            <input  class="form-control" data-bind="event:{'focus': assetForcus},
                                        vngSearch: toAsset,value: toAsset,
                                        searchFunction: CommonModelFunctions.koExtensions.searchAsset"/>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                            <div class="col-md-6"><div class="panel panel-info info ">
                                    <div class="panel-heading" data-bind="text: __L.header_change_status_note"></div>
                                    <textarea  rows="5" class="form-control asset-no-border-radius asset-no-border" data-bind="value: note"></textarea>
                                </div></div>
                            <div class="asset_center_text">
                                <input type="button" class="btn btn-primary"
                                       data-bind="attr:{'value':__L.button_save},
                               roleAction: changeAssetStatus,
                               checkValidUser: 'default',
                               role: 'storekeeper,admin'"/>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 15px;" data-bind="ifnot: isLoadAsset ">
        <div class="alert alert-info" data-bind="text: __L.warning_no_asset_status_detail"></div>
    </div>
</div>
<!--<script src="structures/business/AssetStatusHistoryBusiness.js" type="text/javascript"></script>-->
<!--<script src="structures/business/AssetStatusBusiness.js" type="text/javascript"></script>-->