

<div id="ko-assest-state-history">

    <div class="search-form">
        <div class="row">
            <h3 data-bind="text: _L.title_asset_state_history"></h3>
        </div>
        <div class="row">
            <div class="col-sm-5 col-xs-8 search-box"
                 data-bind="search: assetName, searchAPI: APIData.CMDB.GetListAsset">
                <input class="form-control search-component" data-bind="attr: {placeholder: _L.placeholder_search_asset}">
                <button class="search-component"></button>
                <ul class="search-component"></ul>
            </div>
            <nav class="navbar navbar-default">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a data-bind="attr: {href: _Link.assetDetail(assetName())}">Cấu hình</a>
                    </li>
                    <li class="">
                        <a data-bind="attr: {href: _Link.assetState(assetName())}">Trạng thái</a>
                    </li>
                    <li class="dropdown-hover active">
                        <a>Lịch sử</a>
                        <ul class="dropdown-menu">
                            <li>
                                <a data-bind="attr: {href: _Link.assetDetailHistory(assetName())}">Lịch sử cấu hình</a>
                            </li>
                            <li class="active">
                                <a data-bind="attr: {href: _Link.assetStateHistory(assetName())}">Lịch sử trạng thái</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="background"></div>
    </div>
    <div class="row panel panel-body">
        <div data-bind="if: assetStateHistory().length < 1">
            <span data-bind="text: _L.warning_empty_state_history"></span>
        </div>
        <div data-bind="if: assetStateHistory().length > 0">
            <div class="panel-default">
                <div class="panel-heading text-uppercase asset-sub-heading"
                     data-bind="text: _L.title_asset_state_history"></div>
                <div data-bind="foreach: assetStateHistory">
                    <div class="asset-state-detail-item-header">
                        <span data-bind="text:($index()+1) + '. '"></span>
                        <span data-bind="text: _L.info_time_action"></span>
                        <b data-bind="text: actionDate"></b>
                        <span data-bind="text: _L.info_by_user"></span>
                        <b style="font-weight: bold" data-bind="text: userAction"></b>
                    </div>
                    <ul class="asset-state-info">
                        <li>
                            <span data-bind="text:_L.info_change_state_from"></span>
                            <span style="font-weight: bold; color: red" data-bind="text: cmdb.PreviousState"></span>
                            <span data-bind="text: _L.info_change_to"></span>
                            <span style="font-weight: bold; color: green" data-bind="text:cmdb.NewState"></span>
                        </li>
                        <li>
                            <span data-bind="text:_L.info_assign_to"></span>
                            <span style="font-weight: bold; " data-bind="text: assignTo"></span>
                        </li>
                        <li>
                            <span data-bind="text:_L.info_state_note"></span>
                            <span style="font-weight: bold; " data-bind="text: aapComment"></span>
                        </li>
                        <li data-bind="checkPrivilege:'admin,helpdesk,storekeeper'">
                            <span data-bind="text:_L.info_system_note"></span>
                            <span style="font-style: italic; " data-bind="html: sysComment"></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
