<?php include(__DIR__.'/header.php'); ?>

<?php include(__DIR__.'/menu.php'); ?>

<a href="<?php echo esc_url('?page='.$model['slug'].'&type=formular_addnew'); ?>" class="button button-primary">Add New Formular</a>

<table width="100%" class="wp-list-table widefat fixed striped pages">
    <thead>
    <tr>
        <td id="cb" class="manage-column column-cb check-column">
            <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
            <input id="cb-select-all-1" type="checkbox">
        </td>
        <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
            <a href="">
                <span>Name</span>
                <span class="sorting-indicator"></span>
            </a>
        </th>
        <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
            <a href="">
                <span>Formular</span>
                <span class="sorting-indicator"></span>
            </a>
        </th>
        <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
            <a href="">
                <span>Show On Asset</span>
                <span class="sorting-indicator"></span>
            </a>
        </th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    if(sizeof($model['formulars']) > 0){
        foreach($model['formulars'] as $formular){ ?>
            <tr>
                <th id="cb" class="manage-column column-cb check-column">
                    <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                    <input id="cb-select-all-1" type="checkbox">
                </th>
                <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <?php echo $formular->Name; ?>
                </td>
                <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <?php echo $formular->Formular; ?>
                </td>
                <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <input type="checkbox" <?php if($formular->isShowOnAsset == true) { echo 'checked'; } ?> />
                </td>
                <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <form method="POST"
                          action="<?php echo esc_url('?page='.$model['slug'].'&action=formular_edit'); ?>">
                        <input type="hidden" value="<?php echo $formular->ID; ?>" name="txbID"/>
                        <input type="submit" class="button" name="btnEdit" value="Edit"/>
                        <input type="submit" class="button" name="btnDelete" value="Delete"/>
                    </form>
                </td>
            </tr>
        <?php }
    } else { ?>
        <tr>
            <td colspan="5">
                Not any variable added
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<?php include(__DIR__.'/footer.php'); ?>