<?php include(__DIR__.'/header.php'); ?>

<?php include(__DIR__.'/menu.php'); ?>

<table width="100%" class="wp-list-table widefat fixed striped pages">
    <thead>
    <tr>
        <td id="cb" class="manage-column column-cb check-column">
            <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
            <input id="cb-select-all-1" type="checkbox">
        </td>
        <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
            <a href="">
                <span>Name</span>
                <span class="sorting-indicator"></span>
            </a>
        </th>
        <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
            <a href="">
                <span>Value</span>
                <span class="sorting-indicator"></span>
            </a>
        </th>
        <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
            <a href="">
                <span>Description</span>
                <span class="sorting-indicator"></span>
            </a>
        </th>
        <th></th>
    </tr>
    </thead>
    <tbody>
        <?php
        if(sizeof($model['variables']) > 0){
        foreach($model['variables'] as $variable){
            if($model['editID'] > 0 && $model['editID'] == $variable->ID) {?>
                <form method="POST"
                      action="<?php echo esc_url('?page='.$model['slug'].'&action=variable_update'); ?>">
                    <tr>
                        <th id="cb" class="manage-column column-cb check-column">
                            <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                            <input id="cb-select-all-1" type="checkbox">
                            <input type="hidden" name="txbID" value="<?php echo $variable->ID; ?>" />
                        </th>
                        <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                            <input type="text" name="txbName" value="<?php echo $variable->Name; ?>" />
                        </td>
                        <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                            <input type="number" step="any" name="txbValue" value="<?php echo $variable->Value; ?>" />
                        </td>
                        <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                            <input type="text" name="txbDescription" value="<?php echo $variable->Description; ?>" />
                        </td>
                        <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                            <input type="submit" class="button button-primary"  name="btnEdit" value="Save"/>
                            <input type="submit" class="button button-cancel"  name="btnCancel" value="Cancel"/>
                        </td>
                    </tr>
                </form>
            <?php } else { ?>
                <tr>
                    <th id="cb" class="manage-column column-cb check-column">
                        <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                        <input id="cb-select-all-1" type="checkbox">
                    </th>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <?php echo $variable->Name; ?>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <?php if($variable->Description != 'system'){ echo $variable->Value ;} ?>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <?php if($variable->Description != 'system'){ echo $variable->Description ;} ?>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">

                        <form method="POST"
                              action="<?php echo esc_url('?page='.$model['slug'].'&action=variable_edit'); ?>">
                            <input type="hidden" value="<?php echo $variable->ID; ?>" name="txbID"/>
                            <input <?php if($variable->Description == 'system'){ echo 'disabled';} ?> type="submit" class="button" name="btnEdit" value="Edit"/>
                            <input <?php if($variable->Description == 'system'){ echo 'disabled';} ?> type="submit" class="button" name="btnDelete" value="Delete"/>
                        </form>
                    </td>
                </tr>

            <?php } ?>
        <?php } } else { ?>
        <tr>
            <td colspan="4">
                Not any variable added
            </td>
        </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <form style="display: none;" method="POST" action="<?php echo esc_url('?page='.$model['slug'].'&action=variable_addNew'); ?>">
            <tr>
                <th id="cb" class="manage-column column-cb check-column">
                </th>
                <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <input placeholder="New Variable Name" style="height: 30px;width: 70%; padding-left: 5px" type="text" name="txbName" value="" />
                </td>
                <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <input placeholder="New Variable Value" style="height: 30px;width: 70%; padding-left: 5px"  type="number" step="any" name="txbValue" value="" />
                </td>
                <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <input placeholder="Short Description" style="height: 30px;width: 70%; padding-left: 5px"  type="text" step="any" name="txbDescription" value="" />
                </td>
                <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <input type="submit" class="button button-primary" name="btnEdit" value="Add New"/>
                </td>
            </tr>
        </form>
    </tfoot>
</table>

<?php include(__DIR__.'/footer.php'); ?>