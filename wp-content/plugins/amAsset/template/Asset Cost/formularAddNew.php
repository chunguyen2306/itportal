<?php include(__DIR__.'/header.php'); ?>

<?php include(__DIR__.'/menu.php'); ?>

<h2>New Formular</h2>
<form method="post" action="<?php echo esc_url('?page='.$model['slug'].'&action=formular_newupdate'); ?>" >
<div class="col-sm-8">
    <div class="col-sm-12 form-group">
        <input name="txbName" placeholder="Formular name here" class="title form-control" type="text" />
    </div>
    <div class="col-sm-12 form-group">
        <textarea name="txbFormular" style="height: 300px" placeholder="Your formular here" class="content form-control"></textarea>
    </div>
</div>
<div class="col-sm-4">
    <div class="formAction">
        <input type="submit" value="Save" name="btnSave" class="button button-large button-primary" />
        <input type="submit" value="Cancel" name="btnCancel" class="button button-large" />
    </div>
    <div class="groupBox">
    <span>Variable List</span>
    <ul class="col-sm-12" class="box" id="lisVariable">
        <?php foreach ($model['variables'] as $variable){ ?>
            <li><?php echo '<b>$'.$variable->Name.'</b>'.(($variable->Description != 'system')?' ('.$variable->Description.')':''); ?></li>
        <?php }?>
    </ul>
    </div>
</div>
</form>
<script type="text/javascript" src="<?php echo plugins_url();?>/amAsset/js/model/AssetCost.js"></script>

<?php include(__DIR__.'/footer.php'); ?>
