<ul class="submenu-tab">
    <?php foreach ($model['subMenu'] as $menu){ ?>
        <li class="submenu-tab-item <?php if($_REQUEST['type'] == $menu['type']){ echo 'active'; } ?>" >
            <a href="<?php echo esc_url('?page='.$model['slug'].'&type='.$menu['type']); ?>">
                <?php echo $menu['name'] ?>
            </a>
        </li>
    <?php } ?>
</ul>