
<div id="ko-assest-state">

    <div class="search-form">
        <div class="row">
            <h3 data-bind="text: _L.title_asset_state"></h3>
        </div>
        <div class="row">
            <div class="col-sm-5 col-xs-8 search-box"
                 data-bind="search: assetName, searchAPI: APIData.CMDB.GetListAsset, searchConfig: {passNull: 0}">
                <input class="form-control search-component" data-bind="attr: {placeholder: _L.placeholder_search_asset}">
                <button class="search-component"></button>
                <ul class="search-component"></ul>
            </div>
            <nav class="navbar navbar-default">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a data-bind="attr: {href: _Link.assetDetail(assetName())}">Cấu hình</a>
                    </li>
                    <li  class="active">
                        <a data-bind="attr: {href: _Link.assetState(assetName())}">Trạng thái</a>
                    </li>
                    <li class="dropdown-hover">
                        <a>Lịch sử</a>
                        <ul class="dropdown-menu">
                            <li>
                                <a data-bind="attr: {href: _Link.assetDetailHistory(assetName())}">Lịch sử cấu hình</a>
                            </li>
                            <li>
                                <a data-bind="attr: {href: _Link.assetStateHistory(assetName())}">Lịch sử trạng thái</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="background"></div>
    </div>
    <p></p>
    <div  class="row"  data-bind="if: isLoadAsset ">
        <div class="col-md-12 panel panel-body">
            <div class="panel-default">
                <div class="col-md-12">
                    <div class="panel-heading text-uppercase asset-sub-heading"
                         data-bind="text: _L.title_asset_current_state"></div>
                    <table class="table">
                        <tr data-bind="ifnot: asset.isComponent()">
                            <td style="width: 120px;" class="asset-font-bold" data-bind="text: _L.table_head_ownner"></td>
                            <td data-bind="text: asset.Owner"></td>
                            <td style="width: 120px;" class="asset-font-bold" data-bind="text: _L.table_head_deparment"></td>
                            <td data-bind="text: asset.Department"></td>
                        </tr>
                        <tr>
                            <td class="asset-font-bold" data-bind="text: _L.table_head_asset_type"></td>
                            <td data-bind="text: asset.ResModel"></td>
                            <td class="asset-font-bold" data-bind="text: _L.table_head_asset_model"></td>
                            <td data-bind="text: asset.ResType"></td>

                        </tr>
                        <tr>
                            <td class="asset-font-bold" data-bind="text: _L.table_head_asset_parent"></td>
                            <td data-bind="text: asset.Parent"></td>
                            <td class="asset-font-bold" data-bind="text: _L.table_head_asset_state"></td>
                            <td data-bind="text: asset.State"></td>
                        </tr>

                    </table>
                </div>
                <div class="col-md-12">
                    <div class="panel-heading text-uppercase asset-sub-heading"
                         data-bind="text: _L.table_header_view_asset_detail"></div>
                    <div data-bind="html: asset.resDetail">
                    </div>
<!--                <div>-->
<!--                    <table class="asset-table-detail">-->
<!--                        <tr>-->
<!--                            <td class="asset-td-header">MAC Address</td>-->
<!--                            <td>-->
<!--                                <form class="form-inline">-->
<!--                                    <textarea style="width: 400px; height:200px; float: left; margin-right: 20px;" class="form-control" data-bind="value: asset.MACAddress" ></textarea>-->
<!--                                    <input type="button" data-bind="roleAction: updateMACAddress,-->
<!--                                checkValidUser: 'default',-->
<!--                                role: 'helpdesk,storekeeper,audit,admin'"-->
<!--                                           class="btn btn-primary" value="Thay đổi" />-->
<!--                                </form>-->
<!--                            </td>-->
<!--                        </tr>-->
<!--                    </table>-->
<!--                </div>-->
                </div>
                <div class="col-md-12" data-bind="checkPrivilege:'admin,helpdesk,storekeeper'">
                    <div class="panel-heading text-uppercase asset-sub-heading"
                         data-bind="text: _L.title_asset_change_state"></div>
                    <div class="row" data-bind="ifnot: canEditState">
                        <div data-bind="html: _L.warning_can_not_edit_state"></div>
                    </div>
                    <div class="row" data-bind="if: canEditState">
                        <div  class="asset-panel-body form-horizontal panel-default col-md-12">
                            <div class="col-md-6 asset-vertical-middle">
                                <table class="table borderless asset-table-center">
                                    <tr>
                                        <td data-bind="text: _L.table_head_asset_state"></td>
                                        <td colspan="2">
                                            <div class="col-sm-12 search-box" data-bind="search: newState, searchAPI: searchState, searchConfig:{valueName: 'value', displayName: 'name'}">
                                                <input class="form-control search-component">
                                                <button class="search-component btn btn-primary" style="display: none">Xem</button>
                                                <ul class="search-component"></ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr data-bind="if: displayOption">
                                        <td style="width: 12px;">
                                            <input value="User" type="radio" name="targetOwner" data-bind="checked:target"/>
                                        </td>
                                        <td style="width: 150px;" dat data-bind="text: _L.title_assign_to_user"></td>
                                        <td>
                                            <div class="search-box col-sm12" data-bind="search: toUser, searchAPI: APIData.CMDB.GetListUser, searchConfig:{valueName: 'AccountName', displayName: 'AccountName'}">
                                                <input class="form-control search-component" data-bind="event:{'focus': userForcus}">
                                                <button class="search-component" style="display: none"></button>
                                                <ul class="search-component"></ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr data-bind="if: displayOption">
                                        <td >
                                            <input type="radio" value="Asset"  name="targetOwner" data-bind="checked: target"/>
                                        </td>
                                        <td data-bind="text: _L.title_assign_to_asset"></td>
                                        <td>
                                            <div class="search-box col-sm12" data-bind="search: toAsset, searchAPI: APIData.CMDB.GetListAsset">
                                                <input class="form-control search-component" data-bind="event:{'focus': assetForcus}">
                                                <button class="search-component" style="display: none"></button>
                                                <ul class="search-component"></ul>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6"><div class="panel panel-info info ">
                                    <div class="panel-heading" data-bind="text: _L.header_change_state_note"></div>
                                    <textarea  rows="5" class="form-control asset-no-border-radius asset-no-border" data-bind="value: note"></textarea>
                                </div></div>
                            <div class="asset_center_text">
                                <input class="search-component btn btn-primary" type="button" data-bind="value: _L.button_save, click: changeAssetState"/>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 15px;" data-bind="ifnot: isLoadAsset ">
        <div class="alert alert-info" data-bind="text: _L.warning_no_asset_state_detail"></div>
    </div>
</div>
