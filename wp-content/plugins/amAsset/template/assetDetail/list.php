<?php
/**
 * Created by PhpStorm.
 * User: CPU11642-local
 * Date: 02-Mar-17
 * Time: 14:31
 */

?>


<div id="ko-assest-detail-editor">

    <div class="search-form">
        <div class="row">
            <h3 data-bind="text: _L.title_asset_detail"></h3>
        </div>
        <div class="row">
            <div class="col-sm-5 col-xs-8 search-box"
                 data-bind="search: assetName, searchAPI: APIData.CMDB.GetListAsset, searchConfig: {passNull: 1}">
                <input class="form-control search-component" data-bind="attr: {placeholder: _L.placeholder_search_asset}">
                <button class="search-component"></button>
                <ul class="search-component"></ul>
            </div>
            <nav class="navbar navbar-default">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                        <a data-bind="attr: {href: _Link.assetDetail(assetName())}">Cấu hình</a>
                    </li>
                    <li class="">
                        <a data-bind="attr: {href: _Link.assetState(assetName())}">Trạng thái</a>
                    </li>
                    <li class="dropdown-hover">
                        <a>Lịch sử</a>
                        <ul class="dropdown-menu">
                            <li>
                                <a data-bind="attr: {href: _Link.assetDetailHistory(assetName())}">Lịch sử cấu hình</a>
                            </li>
                            <li>
                                <a data-bind="attr: {href: _Link.assetStateHistory(assetName())}">Lịch sử trạng thái</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="background"></div>
    </div>

    <p></p>
    <div class="row" data-bind="if: isLoadAsset">
        <div class="col-md-12 panel panel-body">
            <div class="panel-default asset-panel">
                <div class="panel-heading text-uppercase asset-sub-heading" data-bind="text: _L.title_asset_cpu_detail"></div>
                <table class="table" style="margin-bottom: 0px !important;">
                    <thead>
                    <tr>
                        <th data-bind="text: _L.form_label_asset_manufacture"></th>
                        <th data-bind="text: _L.table_head_asset_model"></th>
                        <th data-bind="text: _L.table_head_cpu_speed"></th>
                        <th style="width: 20px;"></th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: assetDetail.CPU">
                    <tr>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Supplier"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Model"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Processing"/>
                        </td>
                        <td>
                            <span data-bind="click: $root.deleteCPU " style="font-size: 18px;" class="glyphicon glyphicon-remove"
                                  aria-hidden="true"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div style="text-align: right; padding-right: 20px;">
                    <span data-bind="click: $root.addCPU,text: _L.button_add" style="font-size: 18px;" class="glyphicon glyphicon-plus"
                          aria-hidden="true"></span>
                </div>
            </div>
            <!--Chinh sua thong tin RAM-->
            <div class="panel-default asset-panel">
                <div class="panel-heading text-uppercase asset-sub-heading" data-bind="text: _L.title_asset_ram_detail"></div>
                <table class="table" style="margin-bottom: 0px !important;">
                    <thead>
                    <tr>
                        <th data-bind="text: _L.form_label_asset_manufacture"></th>
                        <th data-bind="text: _L.table_head_asset_model"></th>
                        <th data-bind="text: _L.table_head_storage"></th>
                        <th style="width: 20px;"></th>
                    </tr>

                    </thead>
                    <tbody data-bind="foreach: assetDetail.RAM">
                    <tr>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Supplier"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Model"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Storage"/>
                        </td>
                        <td>
                            <span data-bind="click: $root.deleteRAM " style="font-size: 18px;" class="glyphicon glyphicon-remove"
                                  aria-hidden="true"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div style="text-align: right; padding-right: 20px;">
                    <span data-bind="click: $root.addRAM,text: _L.button_add" style="font-size: 18px;" class="glyphicon glyphicon-plus"
                          aria-hidden="true"></span>
                </div>
            </div>
            <!--Chinh sua thong tin HDD-->
            <div class="panel-default asset-panel">
                <div class="panel-heading text-uppercase asset-sub-heading" data-bind="text: _L.title_asset_hdd_detail"></div>
                <table class="table" style="margin-bottom: 0px !important;">
                    <thead>
                    <tr>
                        <th data-bind="text: _L.form_label_asset_manufacture"></th>
                        <th data-bind="text: _L.table_head_asset_model"></th>
                        <th data-bind="text: _L.table_head_storage"></th>
                        <th style="width: 20px;"></th>
                    </tr>

                    </thead>
                    <tbody data-bind="foreach: assetDetail.HDD">
                    <tr>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Supplier"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Model"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Storage"/>
                        </td>
                        <td>
                            <span data-bind="click: $root.deleteHDD " style="font-size: 18px;" class="glyphicon glyphicon-remove"
                                  aria-hidden="true"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div style="text-align: right; padding-right: 20px;">
                    <span data-bind="click: $root.addHDD, text: _L.button_add" style="font-size: 18px;" class="glyphicon glyphicon-plus"
                          aria-hidden="true"></span>
                </div>
            </div>
            <!--Chinh sua thong tin VGA-->
            <div class="panel-default asset-panel">
                <div class="panel-heading text-uppercase asset-sub-heading"
                     data-bind="text: _L.title_asset_vga_detail"></div>
                <table class="table" style="margin-bottom: 0px !important;">
                    <thead>
                    <tr>
                        <th data-bind="text: _L.form_label_asset_manufacture"></th>
                        <th data-bind="text: _L.table_head_asset_model"></th>
                        <th data-bind="text: _L.table_head_storage"></th>
                        <th style="width: 20px;"></th>
                    </tr>

                    </thead>
                    <tbody data-bind="foreach: assetDetail.VGA">
                    <tr>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Supplier"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Model"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Storage"/>
                        </td>
                        <td>
                            <span data-bind="click: $root.deleteVGA " style="font-size: 18px;" class="glyphicon glyphicon-remove"
                                  aria-hidden="true"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div style="text-align: right; padding-right: 20px;">
                    <span data-bind="click: $root.addVGA,text: _L.button_add" style="font-size: 18px;" class="glyphicon glyphicon-plus"
                          aria-hidden="true"></span>
                </div>
            </div>
            <!--Chinh sua thong tin CD-DVD-->
            <div class="panel-default asset-panel">
                <div class="panel-heading text-uppercase asset-sub-heading"
                     data-bind="text: _L.title_asset_cd_detail"></div>
                <table class="table" style="margin-bottom: 0px !important;">
                    <thead>
                    <tr>
                        <th data-bind="text: _L.form_label_asset_manufacture"></th>
                        <th data-bind="text: _L.table_head_asset_category"></th>
                        <th data-bind="text: _L.table_head_other_info"></th>
                        <th style="width: 20px;"></th>
                    </tr>

                    </thead>
                    <tbody data-bind="foreach: assetDetail.CD_DVD">
                    <tr>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Supplier"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Category"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: OrtheInfo"/>
                        </td>
                        <td>
                            <span data-bind="click: $root.deleteDVD " style="font-size: 18px;" class="glyphicon glyphicon-remove"
                                  aria-hidden="true"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div style="text-align: right; padding-right: 20px;">
                    <span data-bind="click: $root.addDVD,text: _L.button_add" style="font-size: 18px;" class="glyphicon glyphicon-plus"
                          aria-hidden="true"></span>
                </div>
            </div>
            <div class="panel panel-info info">
                <div class="panel-heading" data-bind="text: _L.header_form_note"></div>
                <textarea id="form_new_change_detail_note" rows="5" class="form-control asset-no-border-radius asset-no-border"
                          data-bind="value: note"></textarea>
            </div>
            <div class="asset_center_text">
                <input type="button" class="btn btn-primary"
                       data-bind="click: save, attr:{'value':_L.button_save},
                               roleAction: $root.save,
                               checkValidUser: 'default',
                               role: 'storekeeper,admin'"/>
            </div>
        </div>
    </div>
</div>