
<div id="ko-asset-detail-history">

    <div class="search-form">
        <div class="row">
            <h3 data-bind="text: _L.title_asset_detail_history"></h3>
        </div>
        <div class="row">
            <div class="col-sm-5 col-xs-8 search-box"
                 data-bind="searchAPI: APIData.CMDB.GetListAsset, search: assetName">
                <input class="form-control search-component" data-bind="attr: {placeholder: _L.placeholder_search_asset}">
                <button class="search-component"></button>
                <ul class="search-component"></ul>
            </div>
            <nav class="navbar navbar-default">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a data-bind="attr: {href: _Link.assetDetail(assetName())}">Cấu hình</a>
                    </li>
                    <li class="">
                        <a data-bind="attr: {href: _Link.assetState(assetName())}">Trạng thái</a>
                    </li>
                    <li class="dropdown-hover active">
                        <a>Lịch sử</a>
                        <ul class="dropdown-menu">
                            <li class="active">
                                <a data-bind="attr: {href: _Link.assetDetailHistory(assetName())}">Lịch sử cấu hình</a>
                            </li>
                            <li>
                                <a data-bind="attr: {href: _Link.assetStateHistory(assetName())}">Lịch sử trạng thái</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="background"></div>
    </div>

    <div class="row panel panel-body">
        <div data-bind="if: assetDetailHistory()">
            <div class="panel-heading text-uppercase asset-sub-heading"
                 data-bind="text: _L.title_asset_detail_history +' ('+assetDetailHistory().length+')'"></div>
            <table class="table" id="asset-detail-history-id">
                <thead>
                <th style="width: 5%" data-bind="text: _L.table_head_index"></th>
                <th style="width: 20%" data-bind="text: _L.table_head_action_user"></th>
                <th style="width: 25%" data-bind="text: _L.table_head_note"></th>
                <th style="width: 25%" data-bind="text: _L.table_head_action_date"></th>
                <th style="width: 25%" data-bind="text: _L.table_head_result"></th>
                </thead>
                <tbody data-bind="foreach: assetDetailHistory()">
                <tr data-bind="click: $root.showDetail" class="asset-action-row">
                    <td data-bind="text: $index()+1"></td>
                    <td data-bind="text: UserAction"></td>
                    <td data-bind="text: Comment"></td>
                    <td data-bind="assetDate: ActionDate"></td>
                    <td data-bind="text: SysComment"></td>
                </tr>
                <tr class="histoy-driff-head">
                    <th colspan="3" data-bind="text: _L.header_asset_detail_prv"></th>
                    <th colspan="2" data-bind="text: _L.header_asset_detail_new"></th>
                </tr>
                <tr class="histoy-driff">
                    <th colspan="3" data-bind="html: PreviousDetail"></th>
                    <th colspan="2" data-bind="html: NewDetail"></th>
                </tr>
                </tbody>

            </table>
        </div>
        <div data-bind="ifnot: CommonModelFunctions.checkArrayBinding(assetDetailHistory)">
            <div class="alert alert-warning" data-bind="text: _L.warning_only_display_asset_has_changed"></div>
        </div>
    </div>
</div>