<br><br><br><br><br>
<style>
    .p54{
        left: 50%;
        top: 40%;
        transform: translateX(-50%);
    }

</style>
<div class="container-f">
    <div class="p54 col-sm-5 col-xs-8">
        <div class="row">
            <div class="search-box"
                 data-bind="searchAPI: APIData.CMDB.GetListAsset, search: assetName">
                <input class="form-control search-component" data-bind="attr: {placeholder: _L.placeholder_search_asset}">
                <button class="search-component"></button>
                <ul class="search-component"></ul>
            </div>
        </div>
        <div class="row">
            <ul>
                <li><button class="btn">Xem cấu hình</button></li>
                <li><button class="btn">Xem trạng thái</button></li>
                <li><button class="btn">Xem lịch sử cấu hình</button></li>
                <li><button class="btn">Xem lịch sử trạng thái</button></li>
            </ul>
        </div>
    </div>
</div>