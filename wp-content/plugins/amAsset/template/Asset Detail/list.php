<?php
/**
 * Created by PhpStorm.
 * User: CPU11642-local
 * Date: 02-Mar-17
 * Time: 14:31
 */


?>

<div id="ko-assest-detail-editor">
    <div class="form-group form-horizontal">
        <label for="asset-name-id" class="col-sm-2 control-label" data-bind="text: __L.form_label_asset_name"></label>

        <div class="col-sm-6 search-box" data-bind="doSearch: APICMDB.GetListAsset, onSelect: onSearchSelected, buttonText: 'Xem'">
            <input class="form-control search-component">
            <button class="search-component btn btn-primary">Xem</button>
            <ul class="search-component"></ul>
        </div>
        <!---->
        <!--    <div class="col-sm-6">-->
        <!--        <input id="asset-name-id" class="form-control" data-bind="vngSearch: assetName,-->
        <!--                value: assetName,-->
        <!--                searchFunction: externalTask.koExtensions.searchAsset"/>-->
        <!--    </div>-->
        <!--    <div class="col-sm-1">-->
        <!--        <input type="button" data-bind="roleAction: searchAsset,-->
        <!--                        checkValidUser: 'default',-->
        <!--                        role: 'helpdesk,storekeeper,audit,admin'" value="Xem" class="btn btn-primary"/>-->
        <!--    </div>-->
        <div class="col-sm-2">
            <a data-bind="html: __L.title_asset_status,attr:{'href':'?view=assets/assetStatus&keyword='+assetName()}" ></a>
        </div>
    </div>
    <div data-bind="if: isLoadAsset">
        <div class="col-md-12 panel panel-body">
            <div class="panel-default">
                <div class="panel-heading text-uppercase asset-sub-heading" data-bind="text: __L.title_asset_cpu_detail"></div>
                <table class="table" style="margin-bottom: 0px !important;">
                    <thead>
                    <tr>
                        <th data-bind="text: __L.form_label_asset_manufacture"></th>
                        <th data-bind="text: __L.table_head_asset_model"></th>
                        <th data-bind="text: __L.table_head_cpu_speed"></th>
                        <th style="width: 20px;"></th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: assetDetail.CPU">
                    <tr>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Supplier"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Model"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Processing"/>
                        </td>
                        <td>
                            <span data-bind="click: $root.deleteCPU " style="font-size: 18px;" class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div style="text-align: right; padding-right: 20px;">
                    <span data-bind="click: $root.addCPU,text: __L.button_add"  style="font-size: 18px;" class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </div>
            </div>
            <!--Chinh sua thong tin RAM-->
            <div class="panel-default">
                <div class="panel-heading text-uppercase asset-sub-heading" data-bind="text: __L.title_asset_ram_detail"></div>
                <table class="table" style="margin-bottom: 0px !important;">
                    <thead>
                    <tr>
                        <th data-bind="text: __L.form_label_asset_manufacture"></th>
                        <th data-bind="text: __L.table_head_asset_model"></th>
                        <th data-bind="text: __L.table_head_storage"></th>
                        <th style="width: 20px;"></th>
                    </tr>

                    </thead>
                    <tbody data-bind="foreach: assetDetail.RAM">
                    <tr>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Supplier"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Model"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Storage"/>
                        </td>
                        <td>
                            <span data-bind="click: $root.deleteRAM " style="font-size: 18px;" class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div style="text-align: right; padding-right: 20px;">
                    <span data-bind="click: $root.addRAM,text: __L.button_add" style="font-size: 18px;" class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </div>
            </div>
            <!--Chinh sua thong tin HDD-->
            <div class="panel-default">
                <div class="panel-heading text-uppercase asset-sub-heading" data-bind="text: __L.title_asset_hdd_detail"></div>
                <table class="table" style="margin-bottom: 0px !important;">
                    <thead>
                    <tr>
                        <th data-bind="text: __L.form_label_asset_manufacture"></th>
                        <th data-bind="text: __L.table_head_asset_model"></th>
                        <th data-bind="text: __L.table_head_storage"></th>
                        <th style="width: 20px;"></th>
                    </tr>

                    </thead>
                    <tbody data-bind="foreach: assetDetail.HDD">
                    <tr>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Supplier"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Model"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Storage"/>
                        </td>
                        <td>
                            <span data-bind="click: $root.deleteHDD " style="font-size: 18px;" class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div style="text-align: right; padding-right: 20px;">
                    <span data-bind="click: $root.addHDD, text: __L.button_add" style="font-size: 18px;" class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </div>
            </div>
            <!--Chinh sua thong tin VGA-->
            <div class="panel-default">
                <div class="panel-heading text-uppercase asset-sub-heading"
                     data-bind="text: __L.title_asset_vga_detail"></div>
                <table class="table" style="margin-bottom: 0px !important;">
                    <thead>
                    <tr>
                        <th data-bind="text: __L.form_label_asset_manufacture"></th>
                        <th data-bind="text: __L.table_head_asset_model"></th>
                        <th data-bind="text: __L.table_head_storage"></th>
                        <th style="width: 20px;"></th>
                    </tr>

                    </thead>
                    <tbody data-bind="foreach: assetDetail.VGA">
                    <tr>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Supplier"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Model"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Storage"/>
                        </td>
                        <td>
                            <span data-bind="click: $root.deleteVGA " style="font-size: 18px;" class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div style="text-align: right; padding-right: 20px;">
                    <span data-bind="click: $root.addVGA,text: __L.button_add" style="font-size: 18px;" class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </div>
            </div>
            <!--Chinh sua thong tin CD-DVD-->
            <div class="panel-default">
                <div class="panel-heading text-uppercase asset-sub-heading"
                     data-bind="text: __L.title_asset_cd_detail"></div>
                <table class="table" style="margin-bottom: 0px !important;">
                    <thead>
                    <tr>
                        <th data-bind="text: __L.form_label_asset_manufacture"></th>
                        <th data-bind="text: __L.table_head_asset_category"></th>
                        <th data-bind="text: __L.table_head_other_info"></th>
                        <th style="width: 20px;"></th>
                    </tr>

                    </thead>
                    <tbody data-bind="foreach: assetDetail.CD_DVD">
                    <tr>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Supplier"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: Category"/>
                        </td>
                        <td>
                            <input class="form-control" type="text" data-bind="value: OrtheInfo"/>
                        </td>
                        <td>
                            <span data-bind="click: $root.deleteDVD " style="font-size: 18px;" class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div style="text-align: right; padding-right: 20px;">
                    <span data-bind="click: $root.addDVD,text: __L.button_add" style="font-size: 18px;" class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </div>
            </div>
            <div class="panel panel-info info">
                <div class="panel-heading" data-bind="text: __L.header_form_note"></div>
                <textarea id="form_new_change_detail_note" rows="5" class="form-control asset-no-border-radius asset-no-border" data-bind="value: note"></textarea>
            </div>
            <div class="asset_center_text">
                <input type="button" class="btn btn-primary"
                       data-bind="click: save, attr:{'value':__L.button_save},
                               roleAction: $root.save,
                               checkValidUser: 'default',
                               role: 'storekeeper,admin'"/>
            </div>
        </div>
    </div>
</div>