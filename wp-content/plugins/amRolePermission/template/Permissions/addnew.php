<?php
$typeName = '';
$title = '';
if(isset($model['isUpdate'])){
    $typeName = '&type=update&id='.$_REQUEST['id'];
    $title = 'Update Permission';

} else {
    $typeName = '&type=new';
    $title = 'Add New Permission';
}
?>
<div class="wrap">
    <h1><?php echo $title; ?> <a href="<?php echo esc_url('?page='.$model['slug']); ?>" class="page-title-action" title="Cancel" >Cancel</a></h1>

    <form class="form-horizontal" method="POST" action="<?php echo esc_url('?page='.$model['slug'].$typeName); ?>">
        <?php if(isset($model['isUpdate'])) { ?>
            <input type="hidden" class="form-control" name="perID"
                   value="<?php echo $model['perItem']->ID; ?>" />
        <?php } ?>
        <div class="form-group">
            <label class="col-sm-2">Name: </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="perName" value="<?php if(isset($model['isUpdate'])) { echo $model['perItem']->Name; } else { echo ''; } ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">Descriptions: </label>
            <div class="col-sm-8">
                <input class="form-control" name="perDescription" value="<?php if(isset($model['isUpdate'])) { echo $model['perItem']->Descriptions; } else { echo ''; } ?>" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm- 10">
                <?php if(isset($model['isUpdate'])) { ?>
                    <input type="submit" class="button button-primary" name="btnAction" value="Update" />
                <?php } else { ?>
                    <input type="submit" class="button button-primary" name="btnAction" value="Add new" />
                    <input type="submit" class="button button-primary" name="btnAction" value="Reset" />
                <?php } ?>
            </div>
        </div>
    </form>
</div>