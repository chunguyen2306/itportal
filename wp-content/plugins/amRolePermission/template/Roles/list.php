<div class="wrap">

    <h1>Roles Management <a href="<?php echo esc_url('?page='.$model['slug'].'&type=new'); ?>" class="page-title-action" title="Add New Role" >Add New</a></h1>
    <br/>
    <table class="wp-list-table widefat fixed striped pages">
    	<thead>
    	    <tr>
    	        <td id="cb" class="manage-column column-cb check-column">
                    <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                    <input id="cb-select-all-1" type="checkbox">
    	        </td>
    	        <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="">
                        <span>Name</span>
                        <span class="sorting-indicator"></span>
                    </a>
    	        </th>
    	        <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="">
                        <span>Descriptions</span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                </th>
    	    </tr>
    	</thead>
        <tbody>
            <?php
            if(is_array($model['listRole']) && sizeof($model['listRole']) > 0) {
                foreach ($model['listRole'] as $roleItem) { ?>
                    <tr>
                        <th id="cb" class="manage-column column-cb check-column">
                            <label class="screen-reader-text"
                                   for="cb-select-<?php echo $roleItem->ID; ?>"><?php echo $roleItem->ID; ?></label>
                            <input id="cb-select-<?php echo $roleItem->ID; ?>" type="checkbox">
                        </th>
                        <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                            <a href="<?php echo '?page=' . $model['slug'] . '&type=update&id=' . $roleItem->ID; ?>">
                                <span><strong><?php echo $roleItem->Name; ?></strong></span>
                            </a>
                        <?php if($roleItem->Name != 'Admin' and $roleItem->Name != 'Administrator') { ?>
                            <div class="row-actions"><span class="Edit">
                            <a href="<?php echo '?page=' . $model['slug'] . '&type=updatepers&id=' . $roleItem->ID . '&name=' . $roleItem->Name; ?>">Permission of <?php echo $roleItem->Name; ?></a></span>
                            </div>
                        <?php } ?>
                        </td>
                        <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                            <p>
                                <span><?php echo $roleItem->Description; ?></span>
                            </p>
                        </td>
                        <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                            <form method="POST"
                                  action="<?php echo esc_url('?page=' . $model['slug'] . '&type=del'); ?>">
                                <input type="hidden" name="roleID" value="<?php echo $roleItem->ID; ?>"/>
                                <button type="submit" class="btn btn-flat danger"><span class="glyphicon glyphicon-trash"></span></button>
                            </form>
                        </td>
                    </tr>
                <?php }
            }
            ?>
        </tbody>
    	<tfoot>
    	    <tr>
                <td id="cb" class="manage-column column-cb check-column">
                    <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                    <input id="cb-select-all-1" type="checkbox">
                </td>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="">
                        <span>Name</span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                    <a href="">
                        <span>Descriptions</span>
                        <span class="sorting-indicator"></span>
                    </a>
                </th>
                <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                </th>
            </tr>
    	</tfoot>

    </table>
</div>
