<?php
$typeName = '&type=updatepers&id='.$_REQUEST['id'].'&name='.$_REQUEST['name'];
$title = 'Update Permission of '.$_REQUEST['name'];
?>

<div class="wrap">
    <h1><?php echo $title; ?></h1>

    <form class="form-horizontal" method="POST" action="<?php echo esc_url('?page='.$model['slug'].$typeName); ?>">
        <?php foreach($model['currrentPermission'] as $perItem): ?>
            <div class="checkbox">
                <label><input type="checkbox" name="selected[]" value="<?php echo $perItem->ID; ?>" for="select-<?php echo $perItem->Name; ?>"
                    <?php if($perItem->RoleID == NULL) echo ''; else echo 'checked'; ?>
                    <p><span><?php echo $perItem->Name; ?></span></p>
                </label>
            </div>
        <?php endforeach; ?>
        <div class="form-group">
            <div class="col-sm- 10">
                <input type="submit" class="button button-primary" name="btnAction" value="Update" />
                <input type="submit" class="button button-primary" name="btnAction" value="Back" />
            </div>
        </div>
    </form>
</div>