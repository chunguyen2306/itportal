<?php
importCSS('AMUserRoleStyle', 'plugin.amRolePermission.class.includes.AMUserRoleStyle');
?>
<div class="wrap">
    <h1>User Role <a href="<?php echo esc_url('?page='.$model['slug'].'&type=sync'); ?>" class="page-title-action" title="Sync Role for New User" >Sync Role for New User</a></h1>
    <br/>
    <table class="wp-list-table widefat fixed striped pages">
        <thead>
        <tr>
            <td id="cb" class="manage-column column-cb check-column">
                <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                <input id="cb-select-all-1" type="checkbox">
            </td>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Name</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Domain Name</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Employee Code</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Role of Users</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(is_array($model['listUser']) && sizeof($model['listUser']) > 0) {
            foreach ($model['listUser'] as $userItem) { ?>
                <tr>
                    <th id="cb" class="manage-column column-cb check-column">
                        <label class="screen-reader-text"
                               for="cb-select-<?php echo $userItem->ID; ?>"><?php echo $userItem->ID; ?></label>
                        <input id="cb-select-<?php echo $userItem->ID; ?>" type="checkbox">
                    </th>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <a href="<?php echo '?page=' . $model['slug'] . '&type=update&id=' . $userItem->ID; ?>">
                            <span><?php echo $userItem->Name; ?></span>
                        </a>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <p>
                            <span><?php echo $userItem->Domain; ?></span>
                        </p>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <p>
                            <span><?php echo $userItem->empCode; ?></span>
                        </p>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <p>
                            <span><?php echo $userItem->RoleName; ?></span>
                        </p>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <form method="POST" action="<?php echo esc_url('?page=' . $model['slug'] . '&type=del'); ?>">
                            <input type="hidden" name="ID" value="<?php echo $userItem->ID; ?>"/>
                            <button type="submit" class="btn btn-flat danger">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </form>
                    </td>
                </tr>
            <?php }
        }
        ?>
        </tbody>
    </table>
    <div class="text-center">
        <ul class="pagination">
            <?php
                echo '<li><a href="'.esc_url('?page=' . $model['slug']).'">First</a></li>';

                echo '<li><a href="'.esc_url('?page=' . $model['slug'] . '&p='. $model['previousIndex']).'">&laquo;</a></li>';

                $start = ($_REQUEST['p'] > 0) ? $_REQUEST['p'] : 1 ;
                $end = (($_REQUEST['p'] + 3) < $model['pageCount']) ? ($_REQUEST['p'] + 3) : $model['pageCount'] ;

                if($start > 1) {
                    echo '<li><a href="' . esc_url('?page=' . $model['slug'] . '&p=1') . '">1</a></li>';
                    if($start > 2) echo '<li class="disabled"><span>...</span></li>';
                }

                for ($i = $start; $i <= $end; $i++) {
                    $class = ($_REQUEST['p'] == $i) ? "active" : "";
                    echo '<li class="'.$class.'"><a href="' . esc_url('?page=' . $model['slug'] . '&p=' . $i) . '">'.$i.'</a></li>';
                }

                if($end < $model['pageCount']) {
                    if($end < ($model['pageCount'] - 1)) echo '<li class="disabled"><span>...</span></li>';
                    echo '<li><a href="' . esc_url('?page=' . $model['slug'] . '&p=') . $model['pageCount'] . '">'.$model['pageCount'].'</a></li>';
                }

                echo '<li><a href="'.esc_url('?page=' . $model['slug'] . '&p='. $model['nextIndex']).'">&raquo;</a></li>';
                echo '<li><a href="'.esc_url('?page=' . $model['slug']. '&p='. $model['pageCount']).'">Last</a></li>';
            ?>
        </ul>
    </div>
</div>
