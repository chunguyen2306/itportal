<?php
    $typeName = '&type=update&id='.$_REQUEST['id'];
    $title = 'Update Role of ';
?>
<div class="wrap">
    <h1><?php echo $title.$model['userItem']->Name; ?> <a href="<?php echo esc_url('?page='.$model['slug']); ?>" class="page-title-action" title="Cancel" >Cancel</a></h1>
    <br/>
    <form class="form-horizontal" method="POST" action="<?php echo esc_url('?page='.$model['slug'].$typeName); ?>">
        <?php if(isset($model['isUpdate'])) { ?>
           <input type="hidden" class="form-control" name="ID" value="<?php echo $model['userItem']->ID; ?>" />
        <?php } ?>
        <div class="form-group">
            <label class="col-sm-2">Domain: </label>
            <div    class="col-sm-8">
                <input type="text" class="form-control" style="color: #0A246A;" name="userDomain" value="<?php echo $model['userItem']->Domain; ?>" disabled/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">Name: </label>
            <div    class="col-sm-8">
                <input type="text" class="form-control" style="color: #0A246A;" name="userName" value="<?php echo $model['userItem']->Name; ?>" disabled/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">Employee Code: </label>
            <div    class="col-sm-8">
                <input type="text" class="form-control" style="color: #0A246A;" name="empCode" value="<?php echo $model['userItem']->empCode; ?>" disabled/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">Role Name: </label>
            <div class="col-sm-8">
                <div class="alignleft actions">
                    <label class="screen-reader-text" for="userRole"><?php echo 'Select Role'; ?></label>
                    <select name="userRole" id="userRole">
                        <option value="<?php echo $model['userItem']->RoleID; ?>" <?php if(isset($model['isUpdate'])) { echo 'selected'; } ?>><?php if(isset($model['isUpdate'])) { echo $model['userItem']->RoleName; } else { echo 'Select Role'; } ?></option>
                        <?php foreach ($model['lstRole'] as $listRole):
                            if($listRole->ID != $model['userItem']->RoleID) {?>
                                <option value="<?php echo $listRole->ID; ?>"><?php echo $listRole->Name; ?></option>
                        <?php } endforeach;?>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm- 10">
                <input type="submit" class="button button-primary" name="btnAction" value="Update" />
            </div>
        </div>
    </form>
</div>