<?php
if(!class_exists('AMRoleManagement')) {

    //import your package here
    import('theme.package.Abstracts.AbstractController');
    import('plugin.amRolePermission.class.factory.AMPermissionManagementFactory');
    import('plugin.amRolePermission.class.factory.AMRoleManagementFactory');
    import('plugin.amRolePermission.class.factory.AMRolePermissionFactory');

    class AMRoleManagement extends AbstractController
    {
        private $permissionTable = array();
        private $rolePermissionTable = array();

        protected function init()
        {
            $this->db = new AMRoleManagementFactory();
            $this->permissionTable = new AMPermissionManagementFactory();
            $this->rolePermissionTable = new AMRolePermissionFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'Roles/list',
                'new' => 'Roles/addnew',
                'update' => 'Roles/addnew',
                'delete' => 'Roles/del',
                'updatepers' => 'Roles/updatepermission'
            );
        }
        public function get_all(){

            $listRole = $this->db->query(array(
            ));
            $this->_model('listRole', $listRole);
        }

        public function list_action(){
            $this->get_all();
        }

        public function update_action($req, $post){
            $id = $req['id'];
            $action = $post['btnAction'];
            $this->_model('isUpdate',true);

            if($action == 'Update'){
                $newData = array(
                    'ID' => $id,
                    'Name' => $post['roleName'],
                    'Description' => $post['roleDescription']
                );
                $updateResult = $this->db->update($newData, array(
                    "ID" => $id
                ));
                wp_redirect( $this->_getPath() );
                exit();
            } else {
                if(isset($id)){
                    $roleItem = $this->db->query(array(
                        'filter' => array(
                            array( "ID = ".$id, '')
                        )
                    ))[0];
                    $this->model['roleItem'] = $roleItem;
                }
            }
        }

        public function new_action($req, $post){
            global $amRolePermission;
            $newData = array(
                'Name' => $post['roleName'],
                'Description' => $post['roleDescription'],
            );

            $action = $post['btnAction'];
            if($action == 'Add new'){
                $this->db->insert($newData);
                wp_redirect( $this->_getPath() );
                exit();
            } else if($action == 'Reset'){
                $post['roleName'] = '';
                $post['roleDescription'] = '';
            }
        }

        public function del_action($req, $post){
            $id = $req['roleID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }

        private function get_current_selected_permission($roleID){

            return $this->permissionTable->query(array(
                'join' => array(
                    array('LEFT JOIN', '(select * from '.AMRolePermissionFactory::$table_name.' where RoleID = '.$roleID.') b', 'a.ID', 'b.PerID')
                ),
                'select' => 'a.ID, a.Name, b.RoleID',
                'filter' => array(
                    array('b.RoleID = '.$roleID, 'OR'),
                    array('b.RoleID is null', ''),
                )
            ));
        }

        //update permission
        public function updatepers_action ($req, $post) {
            $id = $req['id'];

            //permission is currrentPermission of role in Role Permission Table

            $currentPermission = $this->get_current_selected_permission($id);
            $this->model['currrentPermission'] = $currentPermission;

            $action = $post['btnAction'];

            if($action == 'Update'){
                $selected = $post['selected'];

                if(is_array($selected)) {
                    //Delete all
                    $this->rolePermissionTable->custom_delete(array(
                        array( "RoleID = ".$id, "" )
                    ));
                    //Push
                    foreach($selected as $key => $value){
                        $newData = array(
                            'RoleID' => $id,
                            'PerID' => $value
                        );
                        $this->rolePermissionTable->insert($newData);
                    }
                } else {
                    //Delete all
                    $this->rolePermissionTable->custom_delete(array(
                        array( "RoleID = ".$id, "" )
                    ));
                }

                $currentPermission = $this->get_current_selected_permission($id);
                $this->model['currrentPermission'] = $currentPermission;
            }
            if($action == 'Back') {
                wp_redirect( $this->_getPath() );
                exit();
            }
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}
?>