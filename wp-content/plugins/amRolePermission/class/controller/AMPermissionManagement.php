<?php

if(!class_exists('AMPermissionManagement')) {

    //import your package here
    import('theme.package.Abstracts.AbstractController');
    import('plugin.amRolePermission.class.factory.AMPermissionManagementFactory');

    class AMPermissionManagement extends AbstractController
    {
        protected function init()
        {
            $this->db = new AMPermissionManagementFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'Permissions/list',
                'update' => 'Permissions/addnew',
                'delete' => 'Permissions/list',
                'new' => 'Permissions/addnew',
            );
        }
        public function get_all(){

            $listPermission = $this->db->query(array(
            ));
            $this->_model('listPermission', $listPermission);
        }

        public function list_action(){
            $this->get_all();
        }

        public function update_action($req, $post){
            $id = $req['id'];
            $action = $post['btnAction'];
            $this->_model('isUpdate',true);

            if($action == 'Update'){
                $newData = array(
                    'ID' => $id,
                    'Name' => $post['perName'],
                    'Descriptions' => $post['perDescription']
                );
                $updateResult = $this->db->update($newData, array(
                    "ID" => $id
                ));
                wp_redirect( $this->_getPath() );
                exit();
            } else {
                if(isset($id)){
                    $perItem = $this->db->query(array(
                        'filter' => array(
                            array( "ID = ".$id, '')
                        )
                    ))[0];
                    $this->model['perItem'] = $perItem;
                }
            }
        }

        public function new_action($req, $post){
            global $amRolePermission;
            $newData = array(
                'Name' => $post['perName'],
                'Descriptions' => $post['perDescription']
            );

            $action = $post['btnAction'];
            if($action == 'Add new'){
                $this->db->insert($newData);
                wp_redirect( $this->_getPath() );
                exit();
            } else if($action == 'Reset'){
                $post['perName'] = '';
                $post['perDescription'] = '';
            }
        }

        public function del_action($req, $post){
            $id = $req['perID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}