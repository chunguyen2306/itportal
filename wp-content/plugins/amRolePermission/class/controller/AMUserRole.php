<?php
if(!class_exists('AMUserRole')) {
    //import your package here
    import('theme.package.Abstracts.AbstractController');
    import('plugin.amRolePermission.class.factory.AMUserRoleFactory');
    import('plugin.amRolePermission.class.factory.AMRoleManagementFactory');
    import('plugin.amRolePermission.class.factory.AMRolePermissionFactory');
    import('plugin.amRolePermission.class.factory.AMUserInfoFactory');

    class AMUserRole extends AbstractController
    {
        private $roleTable = array();
        private $userInfoTable = array();

        protected function init()
        {
            $this->db = new AMUserRoleFactory();
            $this->roleTable = new AMRoleManagementFactory();
            $this->userInfoTable = new AMUserInfoFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'UserRole/list',
                'sync' => 'UserRole/sync',
                'update' => 'UserRole/addnew',
                'delete' => 'UserRole/del',
            );
        }

        public function get_list_role() {
            return $this->roleTable->query(array());
        }

        private function get_user_role($start, $length) {
            $data = $this->db->query(array(
                'join' => array(
                    array( 'JOIN', AMRoleManagementFactory::$table_name.' b', 'a.RoleID', 'b.ID' )
                ),
                'select' => 'a.ID, a.Domain, a.Name, a.RoleID, a.empCode, b.Name as RoleName',
                'limit' => array(
                    'at' => $start,
                    'length' => $length
                )
            ));
            return $data;
        }

        private function sync_user_and_role() {
            $userInfo = $this->userInfoTable->query(array());
            foreach ($userInfo as $value) {
                $existed = $this->db->getOne(array(
                    'filter' => array(
                        array( "Domain = '$value->domainAccount'", '' )
                    )
                ));

                if($existed == null) {
                    $newData = array(
                        'Domain' => $value->domainAccount,
                        'Name' => $value->fullName,
                        'RoleID' => 2,
                        'empCode' => $value->empCode
                    );
                    $this->db->insert($newData);
                } else {

                }
                unset($existed);
            }
        }

        private function get_user_role_update($id) {
            return $this->db->query(array(
                'join' => array(
                    array( 'JOIN', AMRoleManagementFactory::$table_name.' b', 'a.RoleID', 'b.ID' )
                ),
                'select' => 'a.ID, a.Domain, a.Name, a.empCode, a.RoleID, b.Name as RoleName',
                'filter' => array(
                    array('a.ID = "'.$id.'"', '')
                )
            ))[0];
        }

        public function get_all($startIndex, $pageLength){
            $listUser = $this->get_user_role($startIndex, $pageLength);
            $this->_model('listUser', $listUser);
        }

        public function list_action($req, $post){
            if($req['p'] == null) {
                $pageIndex = 1;
            } else {
                $pageIndex = $req['p'];
            }
            $count = $this->db->query(array(
                'select' => 'count(*) as rows'
            ))[0];
            $total = intval($count->rows);
            $pageLength = 15;
            $startIndex = ($pageIndex-1) * $pageLength;

            $pageCount = ceil($total/$pageLength);
            $this->model['pageCount'] = $pageCount;

            if($pageIndex <= 1) {
                $previousIndex = 1;
            } else {
                $previousIndex = $pageIndex - 1;
            }
            $this->model['previousIndex'] = $previousIndex;
            if($pageIndex >= $pageCount) {
                $nextIndex = $pageCount;
            } else {
                $nextIndex = $pageIndex + 1;
            }
            $this->model['nextIndex'] = $nextIndex;
            $this->get_all($startIndex, $pageLength);
        }

        public function sync_action($req, $post) {
            $action = $post['btnAction'];

            if ($action == 'syncUserRole') {
                //Sync New User with Role: Users.
                $this->sync_user_and_role();
                $this->model['result'] = 'success';
            }
        }

        public function update_action($req, $post){
            $id = $req['id'];
            $action = $post['btnAction'];
            $this->_model('isUpdate',true);

            if($action == 'Update'){
                $newData = array(
                    'RoleID' => $post['userRole']
                );
                $this->db->update($newData, array(
                    "ID" => $id
                ));
                wp_redirect( $this->_getPath() );
                exit();
            } else {
                if(isset($id)){
                    $userItem = $this->get_user_role_update($id);
                    $this->model['userItem'] = $userItem;
                    $lstRole = $this->get_list_role();
                    $this->model['lstRole'] = $lstRole;
                }
            }
        }

        public function del_action($req, $post){
            $id = $post['ID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}
?>
