<?php
if(!class_exists('AMRolePermissionBusiness')) {
    if ( ! function_exists( 'import' ) ) {
        require_once get_template_directory() . '/package/import.php';
    }

    import ('plugin.amRolePermission.class.factory.AMRoleManagementFactory');
    import ('plugin.amRolePermission.class.factory.AMPermissionManagementFactory');
    import ('plugin.amRolePermission.class.factory.AMRolePermissionFactory');
    import ('plugin.amRolePermission.class.factory.AMUserRoleFactory');

    class AMRolePermissionBusiness
    {
        function get_user_permission($roleID) {
            $rolePermissionTable = new AMRolePermissionFactory();
            $userPerName = $rolePermissionTable->query(array(
                'join' => array(
                    array('JOIN', AMPermissionManagementFactory::$table_name . ' b', 'a.PerID', 'b.ID'),
                ),
                'filter' => array(
                    array("a.RoleID = '$roleID'", ''),
                ),
                'select' => 'b.Name'
            ));

            $arrPerName = array();
            foreach ($userPerName as $key => $value) {
                $arrPerName[] = $value->Name;
            }
            return $arrPerName;
        }

        public function get_user_role($domain) {
            $userRoleTable = new AMUserRoleFactory();
            return $userRoleTable->getOne(array(
                'join' => array(
                    array('JOIN', AMRoleManagementFactory::$table_name.' b', 'a.RoleID', 'b.ID')
                ),
                'filter' => array(
                    array("a.Domain = '$domain'",'')
                ),
                'select' => 'a.Domain, a.RoleID, b.Name as RoleName'
            ));
        }

        function checkPermission($arrPerName) {
            /*
             * Get user Role.
             * Get user Permission.
             */
            global $userDomain;
            $userInfo = $this->get_user_role($userDomain);
            $userRoleName = $userInfo->RoleName;
            $userRoleID = $userInfo->RoleID;
            $userPerName = $this->get_user_permission($userRoleID);

            if ($userRoleName == 'Admin') {
                //Admin have all permission.
                return true;
            }
            else if(is_array($arrPerName) and sizeof($arrPerName) > 0) {
                //Check input have in_array user Permission.
                $checked = false;
                foreach ($arrPerName as $key => $value) {
                    if(in_array($value, $userPerName)) {
                        $checked = true;
                    } else {
                        $checked = false;
                        break;
                    }
                }
                return $checked;
            } else {
                return false;
            }
        }

        //Check Role
        function checkRole($arrRoleName) {
            /*
             * Lấy thông tin role, permission hiện tại của User.
             * Kiểm tra xem user'role có trong input hay không.
             */
            global $userDomain;
            $userInfo = $this->get_user_role($userDomain);
            $userRoleName = $userInfo->RoleName;
            if($userRoleName == 'Admin')
                return true;
            else if(in_array($userRoleName, $arrRoleName))
                return true;
            else
                return false;
        }

        function FilterMenu ($menu, $method) {
            /*
             * Lấy thông tin role, permission hiện tại của User
             * method = {'role', 'permission'}
             */

            if((is_array($menu) and sizeof($menu) > 0)) {
                if($method === "Role") {
                    foreach ($menu as $key => $value) {
                        $checked = $this->checkRole($value['Role']);
                        if(!$checked) {
                            //Delete if not have role.
                            $newkey = array_search($value, $menu);
                            array_splice($menu, $newkey, 1);
                        }
                    }
                } else if($method === "Permission") {
                    foreach ($menu as $key => $value) {
                        $checked = $this->checkPermission($value['Permission']);
                        if(!$checked) {
                            //Delete if not have permission.
                            $newkey = array_search($value, $menu);
                            array_splice($menu, $newkey, 1);
                        }
                    }
                }
            }
            return $menu;
        }

        function FilterPage($method, $url, $option) {
            if($method === "Role") {
                $checked = $this->checkRole($option);
                if(!$checked) {
                    //Redirect Error Page.
                    //wp_redirect();
                } else {
                    wp_redirect($url);
                }
            } else if($method === "Permission") {
                $checked = $this->checkPermission($option);
                if(!$checked) {
                    //Redirect Error Page.
                    //wp_redirect();
                } else {
                    wp_redirect($url);
                }
            }
        }
    }
}
?>
