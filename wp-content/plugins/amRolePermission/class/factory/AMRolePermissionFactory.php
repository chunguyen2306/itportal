<?php
if(!class_exists('AMRolePermissionFactory')) {
    import('theme.package.Abstracts.AbstractDatabase');
    class AMRolePermissionFactory extends AbstractDatabase {

        public static $table_name = 'am_role_permission';

        public function tableInfo(){
            return array(
                'table_name' => AMRolePermissionFactory::$table_name,
                'table_columns' => array(
                    'ID' => 'Role Permission Code',
                    'RoleID' => 'Role Code',
                    'PerID' => 'Permission Code'
                ),
                'table_columns_type' => array(
                    'ID' => 'int',
                    'RoleID' => 'int',
                    'PerID' => 'int'
                )
            );
        }
    }
}
?>