<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMUserInfoFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMUserInfoFactory extends AbstractDatabase {

        public static $tableName = 'am_users';

        protected function tableInfo(){
            return array(
                'table_name' => AMUserInfoFactory::$tableName,
                'table_columns' => array(
                    'ID',
                    'domainAccount',
                    'empCode',
                    'fullName',
                    'workingEmail'
                ),
                'table_columns_type' => array(
                    'ID' => 'int',
                    'domainAccount' => 'string',
                    'empCode' => 'string',
                    'fullName' => 'string',
                    'workingEmail' => 'string'

                )
            );
        }
    }
}
?>