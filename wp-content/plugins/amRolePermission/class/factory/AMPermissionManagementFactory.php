<?php
if(!class_exists('AMPermissionManagementFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMPermissionManagementFactory extends AbstractDatabase {

        public static $table_name = 'am_permission';

        public function tableInfo(){
            return array(
                'table_name' => AMPermissionManagementFactory::$table_name,
                'table_columns' => array(
                    'ID' => 'Permission Code',
                    'Name' => 'Permission Name',
                    'Descriptions' => 'Descriptions'
                ),
                'table_columns_type' => array(
                    'ID' => 'int',
                    'Name' => 'string',
                    'Descriptions' => 'string'
                )
            );
        }
    }
}
?>