<?php
if(!class_exists('AMRoleManagementFactory')) {
    import('theme.package.Abstracts.AbstractDatabase');
    class AMRoleManagementFactory extends AbstractDatabase {

        public static $table_name = 'am_roles';

        public function tableInfo(){
            return array(
                'table_name' => AMRoleManagementFactory::$table_name,
                'table_columns' => array(
                    'ID' => 'Role Code',
                    'Name' => 'Role Name',
                    'Description' => 'Description'
                ),
                'table_columns_type' => array(
                    'ID' => 'int',
                    'Name' => 'string',
                    'Description' => 'string'
                )
            );
        }
    }
}
?>