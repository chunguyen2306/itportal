<?php
/*
  Plugin Name: AM Role and Permission
  Plugin URI:
  Description: Chứa tất cả các chức năng quản lý quyền và phân quyền.
  Version: 1.0
  Author: daopd
  Author URI:
  License:
  Text Domain: am-role-permission
*/

if(!class_exists('AMRolePermission')) {
    if ( ! function_exists( 'import' ) ) {
        require_once get_template_directory() . '/package/import.php';
    }

    //Role Class Controller
    import ('plugin.amRolePermission.class.controller.AMRoleManagement');
    import ('plugin.amRolePermission.class.controller.AMPermissionManagement');
    import ('plugin.amRolePermission.class.controller.AMUserRole');
    import ('plugin.amRolePermission.class.business.AMRolePermissionBusiness');
    class AMRolePermission
    {
        public $db = array();

        public $model = array();

        public function __construct()
        {
            $this->define_constants();
            $this->init_hook();
        }

        private function init_hook()
        {
            add_action('init', array($this, 'init'), 0);
        }

        private function define_constants() {
        }

        private function define($name, $value)
        {
            if (!defined($name)) {
                define($name, $value);
            }
        }

        function add_admin_menu()
        {
            add_menu_page(
                'AM Roles',
                'AM Roles',
                'manage_options',
                'am_roles',
                array(new AMRoleManagement(array(
                    'slug' => 'am_roles'
                )), 'index'),
                'dashicons-groups'
            );


            add_submenu_page(
                'am_roles',
                'Roles Management',
                'Roles',
                'manage_options',
                'am_roles'
            );

            add_submenu_page(
                'am_roles',
                'Permissions Management',
                'Permissions',
                'manage_options',
                'am_permissions',
                array(new AMPermissionManagement(array(
                    'slug' => 'am_permissions'
                )), 'index')
            );

            add_submenu_page(
                'am_roles',
                'User Role',
                'User Role',
                'manage_options',
                'am_user_role',
                array(new AMUserRole(array(
                    'slug' => 'am_user_role'
                )), 'index')
            );
        }

        public function init()
        {
            add_action('admin_menu', array($this, 'add_admin_menu'));
        }
    }
    $amRolePermission = new AMRolePermission();
    $GLOBALS['amRolePermission'] = $amRolePermission;

    $amRole = new AMRolePermissionBusiness();
    $GLOBALS['amRole'] = $amRole;
}