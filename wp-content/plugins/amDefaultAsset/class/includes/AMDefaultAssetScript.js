/**
 * Created by LAP10637-local on 4/3/2017.
 */
(function(){
    var viewModel = {
        lstBranch: ko.observableArray([]),
        lstGroup: ko.observableArray([]),
        AssetType: ko.observable(''),
        AssetModel: ko.observable(''),
        Quantity: ko.observable(),
        selectedBranch: ko.observable(),
        selectedBranchEvent: function (model, event) {
            $.ajax({
                url: ajaxurl,
                method: 'POST',
                data: {
                    'action': 'amDefaultAsset',
                    't': 'AMJobGroup',
                    'm': 'getByBranchID',
                    'id': viewModel.selectedBranch()
                },
                success: function(result){
                    result = JSON.parse(result);
                    viewModel.lstGroup(result);
                }
            });
        },
        selectedGroup: ko.observable(),
    };

    $.ajax({
        url: ajaxurl,
        method: 'POST',
        data: {
            'action': 'amDefaultAsset',
            't': 'AMJobBranch',
            'm': 'getAll'
        },
        success: function(result){
            result = JSON.parse(result);
            viewModel.lstBranch(result);
        }
    });

    ko.applyBindings(viewModel, $('div[amid="section-new-default-asset"]')[0]);
})();

