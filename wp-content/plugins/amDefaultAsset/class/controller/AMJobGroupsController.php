<?php
if(!class_exists('AMJobGroupsController')) {

    //import your package here
    import('theme.package.Abstracts.AbstractController');
    import('plugin.amDefaultAsset.class.factory.AMJobGroupsFactory');
    import('plugin.amDefaultAsset.class.factory.AMJobBranchFactory');

    class AMJobGroupsController extends AbstractController
    {
        protected $jobBranchTable= array();
        protected function init()
        {
            $this->db = new AMJobGroupsFactory();
            $this->jobBranchTable = new AMJobBranchFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'Job Groups/list',
                'update' => 'Job Groups/addnew',
                'delete' => 'Job Groups/list',
                'new' => 'Job Groups/addnew',
            );
        }

        public function get_all() {
            $listJobGroup = $this->get_list_job_group();
            $this->_model('listJobGroup', $listJobGroup);
        }

        private function get_list_branch() {
            return $this->jobBranchTable->query(array());
        }

        private function get_list_job_group (){
            return $this->db->query(array(
                'join' => array(
                    array('JOIN', AMJobBranchFactory::$table_name.' b', 'a.BranchID', 'b.ID')
                ),
                'select' => 'a.ID, a.GroupName, a.BranchID, a.Description, b.BranchName'
            ));
        }

        private function get_list_job_group_update ($ID){
            return $this->db->query(array(
                'join' => array(
                    array('JOIN', AMJobBranchFactory::$table_name.' b', 'a.BranchID', 'b.ID')
                ),
                'select' => 'a.ID, a.GroupName, a.BranchID, a.Description, b.BranchName',
                'filter' => array(
                    array('a.ID = '.$ID, '')
                )
            ))[0];
        }

        public function list_action(){
            $this->get_all();
        }

        public function update_action($req, $post){
            $id = $req['id'];
            $action = $post['btnAction'];
            $this->_model('isUpdate',true);

            if($action == 'Update'){
                $newData = array(
                    'ID' => $id,
                    'GroupName' => $post['groupName'],
                    'BranchID' => $post['groupBranch'],
                    'Description' => $post['groupDescription']
                );
                $updateResult = $this->db->update($newData, array(
                    "ID" => $id
                ));
                wp_redirect( $this->_getPath() );
                exit();
            } else {
                if(isset($id)){
                    $groupItem = $this->get_list_job_group_update($id);
                    $this->model['groupItem'] = $groupItem;
                    $lstBranch = $this->get_list_branch();
                    $this->model['lstBranch'] = $lstBranch;
                }
            }
        }

        public function new_action($req, $post){
            global $amDefaultAsset;
            $lstBranch = $this->get_list_branch();
            $this->model['lstBranch'] = $lstBranch;

            $newData = array(
                'GroupName' => $post['groupName'],
                'BranchID' => $post['groupBranch'],
                'Description' => $post['groupDescription']
            );

            $action = $post['btnAction'];
            if($action == 'Add new'){
                $this->db->insert($newData);
                wp_redirect( $this->_getPath() );
                exit();
            } else if($action == 'Reset'){
                $post['groupName'] = '';
                $post['groupDescription'] = '';
            }
        }

        public function del_action($req, $post){
            $id = $req['groupID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}