<?php
if(!class_exists('AMJobBranchController')) {

    //import your package here
    import('theme.package.Abstracts.AbstractController');
    import('plugin.amDefaultAsset.class.factory.AMJobBranchFactory');

    class AMJobBranchController extends AbstractController
    {
        protected function init()
        {
            $this->db = new AMJobBranchFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'Job Branch/list',
                'update' => 'Job Branch/addnew',
                'delete' => 'Job Branch/list',
                'new' => 'Job Branch/addnew',
            );
        }
        public function get_all(){

            $listJobBranch = $this->db->query(array(
            ));
            $this->_model('listJobBranch', $listJobBranch);
        }

        public function list_action(){
            $this->get_all();
        }

        public function update_action($req, $post){
            $id = $req['id'];
            $action = $post['btnAction'];
            $this->_model('isUpdate',true);

            if($action == 'Update'){
                $newData = array(
                    'ID' => $id,
                    'BranchName' => $post['branchName'],
                    'Description' => $post['branchDescription']
                );
                $updateResult = $this->db->update($newData, array(
                    "ID" => $id
                ));
                wp_redirect( $this->_getPath() );
                exit();
            } else {
                if(isset($id)){
                    $jobBranchItem = $this->db->query(array(
                        'filter' => array(
                            array( "ID = ".$id, '')
                        )
                    ))[0];
                    $this->model['jobBranchItem'] = $jobBranchItem;
                }
            }
        }

        public function new_action($req, $post){
            global $amRolePermission;
            $newData = array(
                'BranchName' => $post['branchName'],
                'Description' => $post['branchDescription']
            );

            $action = $post['btnAction'];
            if($action == 'Add new'){
                $this->db->insert($newData);
                wp_redirect( $this->_getPath() );
                exit();
            } else if($action == 'Reset'){
                $post['branchName'] = '';
                $post['branchDescription'] = '';
            }
        }

        public function del_action($req, $post){
            $id = $req['branchID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}