<?php
if(!class_exists('AMDefaultAssetController')) {

    //import your package here
    import('theme.package.Abstracts.AbstractController');
    import('plugin.amDefaultAsset.class.factory.AMDefaultAssetFactory');
    import('plugin.amDefaultAsset.class.factory.AMJobBranchFactory');

    class AMDefaultAssetController extends AbstractController
    {
        protected $jobBranchFactory = array();
        protected $jobGroupFactory = array();
        protected function init()
        {
            $this->db = new AMDefaultAssetFactory();
            $this->jobBranchFactory = new AMJobBranchFactory();
            $this->jobGroupFactory = new AMJobGroupsFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__) - strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'Default Asset/list',
                'update' => 'Default Asset/addnew',
                'delete' => 'Default Asset/list',
                'new' => 'Default Asset/addnew',
            );
        }

        private function get_list_branch() {
            $listBranch = $this->jobBranchFactory->query(array(
                'join' => array(
                    array('JOIN', AMJobGroupsFactory::$table_name.' b', 'a.ID', 'b.BranchID')
                ),
                'select' => 'a.BranchName, b.ID'
            ));
            $branchItem = array();
            foreach ($listBranch as $key => $value) {
                $branchItem[$value->ID] = $value->BranchName;
            }
            return $branchItem;
        }

        private function get_list_default_asset (){
            return $this->db->query(array(
                'join' => array(
                    array('JOIN', AMJobGroupsFactory::$table_name.' b', 'a.JobGroupID', 'b.ID')
                ),
                'select' => 'a.ID, a.AssetType, a.AssetModel, a.JobGroupID, a.Quantity, b.GroupName'
            ));
        }

        private function get_list_default_asset_update ($ID){
            return $this->db->query(array(
                'join' => array(
                    array('JOIN', AMJobGroupsFactory::$table_name.' b', 'a.JobGroupID', 'b.ID')
                ),
                'select' => 'a.ID, a.AssetType, a.AssetModel, a.JobGroupID, a.Quantity, b.GroupName',
                'filter' => array(
                    array('a.ID = '.$ID, '')
                )
            ))[0];
        }

        public function list_action(){
            $this->model['branchItem'] = $this->get_list_branch();
            $this->model['listAsset'] = $this->get_list_default_asset();
        }

        public function update_action($req, $post){
            $id = $req['id'];
            $action = $post['btnAction'];
            $this->_model('isUpdate',true);

            if($action == 'Update'){
                $newData = array(
                    'ID' => $id,
                    'JobGroupID' => $post['assetGroup'],
                    'AssetType' => $post['assetType'],
                    'AssetModel' => $post['assetModel'],
                    'Quantity' => $post['assetQuantity']
                );
                $updateResult = $this->db->update($newData, array(
                    "ID" => $id
                ));
                wp_redirect( $this->_getPath() );
                exit();
            } else {
                if(isset($id)){
                    $this->model['assetItem'] = $this->get_list_default_asset_update($id);
                }
            }
        }

        public function new_action($req, $post){
            global $amDefaultAsset;

            $newData = array(
                'JobGroupID' => $post['assetGroup'],
                'AssetType' => $post['assetType'],
                'AssetModel' => $post['assetModel'],
                'Quantity' => $post['assetQuantity']
            );

            $action = $post['btnAction'];

            if($action == 'Add new'){
                $this->db->insert($newData);
                wp_redirect( $this->_getPath() );
                exit();
            } else if($action == 'Reset'){
                $post['JobGroupID'] = '';
                $post['AssetType'] ='';
                $post['AssetModel']='';
                $post['Quantity'] = '';
            }
        }

        public function del_action($req, $post){
            $id = $req['assetID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }

        protected function beforeGetView($req, $post)
        {
            // TODO: Implement beforeGetView() method.
        }
    }
}