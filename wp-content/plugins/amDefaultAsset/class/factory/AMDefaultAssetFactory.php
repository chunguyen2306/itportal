<?php
if(!class_exists('AMDefaultAssetFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMDefaultAssetFactory extends AbstractDatabase {

        public static $table_name = 'am_default_asset';

        public function tableInfo(){
            return array(
                'table_name' => AMDefaultAssetFactory::$table_name,
                'table_columns' => array(
                    'ID' => 'Default Asset Code',
                    'JobGroupID' => 'Job Group Code',
                    'AssetType' => 'Asset Type',
                    'AssetModel' => 'Asset Model',
                    'Quantity' => 'Quantity'
                ),
                'table_columns_type' => array(
                    'ID' => 'int',
                    'JobGroupID' => 'int',
                    'AssetType' => 'string',
                    'AssetModel' => 'string',
                    'Quantity' => 'int'
                )
            );
        }
    }
}
?>