<?php
if(!class_exists('AMJobGroupsFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMJobGroupsFactory extends AbstractDatabase {

        public static $table_name = 'am_job_groups';

        public function tableInfo(){
            return array(
                'table_name' => AMJobGroupsFactory::$table_name,
                'table_columns' => array(
                    'ID' => 'Job Group Code',
                    'GroupName' => 'Job Group Name',
                    'BranchID' => 'Job Branch Code',
                    'Description' => 'Description'
                ),
                'table_columns_type' => array(
                    'ID' => 'int',
                    'GroupName' => 'string',
                    'BranchID' => 'int',
                    'Description' => 'string'
                )
            );
        }
    }
}
?>