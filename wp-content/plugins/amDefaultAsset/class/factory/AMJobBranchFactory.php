<?php
if(!class_exists('AMJobBranchFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMJobBranchFactory extends AbstractDatabase {

        public static $table_name = 'am_job_branch';

        public function tableInfo(){
            return array(
                'table_name' => AMJobBranchFactory::$table_name,
                'table_columns' => array(
                    'ID' => 'Job Branch Code',
                    'BranchName' => 'Branch Name',
                    'Description' => 'Description'
                ),
                'table_columns_type' => array(
                    'ID' => 'int',
                    'BranchName' => 'string',
                    'Description' => 'string'
                )
            );
        }
    }
}
?>