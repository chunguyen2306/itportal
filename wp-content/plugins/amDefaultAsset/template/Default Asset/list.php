<?php
/**
 * Created by PhpStorm.
 * User: CPU10339-local
 * Date: 17/03/2017
 * Time: 3:01 PM
 */
?>
<div class="wrap">

    <h1>Default Asset <a href="<?php echo esc_url('?page='.$model['slug'].'&type=new'); ?>" class="page-title-action" title="Add New Default Asset" >Add New</a></h1>
    <br/>
    <table class="wp-list-table widefat fixed striped pages">
        <thead>
        <tr>
            <td id="cb" class="manage-column column-cb check-column">
                <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                <input id="cb-select-all-1" type="checkbox">
            </td>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Asset Type</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Asset Model</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Job Branch</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Job Groups</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Quantity</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(is_array($model['listAsset']) && sizeof($model['listAsset']) > 0) {
            foreach ($model['listAsset'] as $assetItem) { ?>
                <tr>
                    <th id="cb" class="manage-column column-cb check-column">
                        <label class="screen-reader-text"
                               for="cb-select-<?php echo $assetItem->ID; ?>"><?php echo $assetItem->ID; ?></label>
                        <input id="cb-select-<?php echo $assetItem->ID; ?>" type="checkbox">
                    </th>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <a href="<?php echo '?page=' . $model['slug'] . '&type=update&id=' . $assetItem->ID; ?>">
                            <span><?php echo $assetItem->AssetType; ?></span>
                        </a>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <p>
                            <span><?php echo $assetItem->AssetModel; ?></span>
                        </p>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <p>
                            <span><?php echo $model['branchItem'][$assetItem->JobGroupID]; ?></span>
                        </p>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <p>
                            <span><?php echo $assetItem->GroupName; ?></span>
                        </p>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <p>
                            <span><?php echo $assetItem->Quantity; ?></span>
                        </p>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <form method="POST" action="<?php echo esc_url('?page=' . $model['slug'] . '&type=del'); ?>">
                            <input type="hidden" name="assetID" value="<?php echo $assetItem->ID; ?>"/>
                            <button type="submit" class="btn btn-flat danger"><span
                                    class="glyphicon glyphicon-trash"></span></button>
                        </form>
                    </td>
                </tr>
            <?php }
        }
        ?>
</tbody>
<tfoot>
<tr>
    <td id="cb" class="manage-column column-cb check-column">
        <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
        <input id="cb-select-all-1" type="checkbox">
    </td>
    <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
        <a href="">
            <span>Asset Type</span>
            <span class="sorting-indicator"></span>
        </a>
    </th>
    <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
        <a href="">
            <span>Asset Model</span>
            <span class="sorting-indicator"></span>
        </a>
    </th>
    <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
        <a href="">
            <span>Job Branch</span>
            <span class="sorting-indicator"></span>
        </a>
    </th>
    <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
        <a href="">
            <span>Job Groups</span>
            <span class="sorting-indicator"></span>
        </a>
    </th>
    <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
        <a href="">
            <span>Quantity</span>
            <span class="sorting-indicator"></span>
        </a>
    </th>
    <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
    </th>
</tr>
</tfoot>

</table>
</div>