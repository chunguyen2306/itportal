<?php
/**
 * Created by PhpStorm.
 * User: CPU10339-local
 * Date: 17/03/2017
 * Time: 3:01 PM
 */
$typeName = '';
$title = '';
if(isset($model['isUpdate'])){
    $typeName = '&type=update&id='.$_REQUEST['id'];
    $title = 'Update Information of ';
} else {
    $typeName = '&type=new';
    $title = 'Add New Default Asset';
}
?>
<div amid="section-new-default-asset" class="wrap">
    <h1><?php echo $title.$model['assetItem']->AssetType; ?> <a href="<?php echo esc_url('?page='.$model['slug']); ?>" class="page-title-action" title="Cancel" >Cancel</a></h1>
    <br/>
    <form class="form-horizontal" method="POST" action="<?php echo esc_url('?page='.$model['slug'].$typeName); ?>">
        <?php if(isset($model['isUpdate'])) { ?>
            <input type="hidden" class="form-control" name="assetID"
                   value="<?php echo $model['assetItem']->ID; ?>" />
        <?php } ?>
        <div class="form-group">
            <label class="col-sm-2">Asset Type: </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="assetType"
                    <?php if(isset($model['isUpdate'])) { echo 'value="'.$model['assetItem']->AssetType.'"'; }
                    else {echo 'data-bind="value: AssetType"';} ?>
                />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">Asset Model: </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="assetModel"
                    <?php if(isset($model['isUpdate'])) { echo 'value = "'.$model['assetItem']->AssetModel.'"'; }
                    else echo 'data-bind="value: AssetModel"';?>
                />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">Quantity: </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="assetQuantity"
                    <?php if(isset($model['isUpdate'])) { echo 'value = "'.$model['assetItem']->Quantity.'"'; }
                    else { echo 'data-bind="value: Quantity"';}?>
                />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">Job Branch: </label>
            <div class="col-sm-8">
                <select name="branchID" data-bind="
                      options: lstBranch,
                      value: selectedBranch,
                      optionsCaption: 'Select Job Branch',
                      optionsText: 'BranchName',
                      optionsValue: 'ID',
                      event: {
                            change: selectedBranchEvent
                      }
                      "></select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">Job Group: </label>
            <div class="col-sm-8">
                <select  name="assetGroup" data-bind="
                      options: lstGroup,
                      value: selectedGroup,
                      optionsValue: 'ID',
                      optionsText: 'GroupName',
                      optionsCaption: 'Select Job Group'
                      "></select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm- 10">
                <?php if(isset($model['isUpdate'])) { ?>
                    <input type="submit" class="button button-primary" name="btnAction" value="Update" style="margin-left: 15px"/>
                <?php } else { ?>
                    <input type="submit" class="button button-primary" name="btnAction" value="Add new" style="margin-left: 15px"/>
                    <input type="submit" class="button button-primary" name="btnAction" value="Reset" />
                <?php } ?>
            </div>
        </div>
    </form>
</div>
<script>
    <?php importScript('CustomReportAddNew', 'plugin.amDefaultAsset.class.includes.AMDefaultAssetScript'); ?>
</script>