<?php
/**
 * Created by PhpStorm.
 * User: CPU10339-local
 * Date: 17/03/2017
 * Time: 3:02 PM
 */
?>
<div class="wrap">
    <h1>Job Group Management <a href="<?php echo esc_url('?page='.$model['slug'].'&type=new'); ?>" class="page-title-action" title="Add New Job Group" >Add New</a></h1>
    <br/>
    <table class="wp-list-table widefat fixed striped pages">
        <thead>
        <tr>
            <td id="cb" class="manage-column column-cb check-column">
                <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                <input id="cb-select-all-1" type="checkbox">
            </td>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Job Group Name</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Job Branch</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Description</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(is_array($model['listJobGroup']) && sizeof($model['listJobGroup']) > 0) {
            foreach ($model['listJobGroup'] as $jobGroupItem) { ?>
                <tr>
                    <th id="cb" class="manage-column column-cb check-column">
                        <label class="screen-reader-text"
                               for="cb-select-<?php echo $jobGroupItem->ID; ?>"><?php echo $jobGroupItem->ID; ?></label>
                        <input id="cb-select-<?php echo $jobGroupItem->ID; ?>" type="checkbox">
                    </th>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <a href="<?php echo '?page=' . $model['slug'] . '&type=update&id=' . $jobGroupItem->ID; ?>">
                            <span><?php echo $jobGroupItem->GroupName; ?></span>
                        </a>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <p>
                            <span><?php echo $jobGroupItem->BranchName; ?></span>
                        </p>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <p>
                            <span><?php echo $jobGroupItem->Description; ?></span>
                        </p>
                    </td>
                    <td scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                        <form method="POST" action="<?php echo esc_url('?page=' . $model['slug'] . '&type=del'); ?>">
                            <input type="hidden" name="groupID" value="<?php echo $jobGroupItem->ID; ?>"/>
                            <button type="submit" class="btn btn-flat danger"><span
                                    class="glyphicon glyphicon-trash"></span></button>
                        </form>
                    </td>
                </tr>
            <?php }
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <td id="cb" class="manage-column column-cb check-column">
                <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                <input id="cb-select-all-1" type="checkbox">
            </td>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Job Group Name</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Job Branch</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                <a href="">
                    <span>Description</span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
            </th>
        </tr>
        </tfoot>

    </table>
</div>