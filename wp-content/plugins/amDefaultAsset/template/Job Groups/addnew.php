<?php
/**
 * Created by PhpStorm.
 * User: CPU10339-local
 * Date: 17/03/2017
 * Time: 3:02 PM
 */
$typeName = '';
$title = '';
if(isset($model['isUpdate'])){
    $typeName = '&type=update&id='.$_REQUEST['id'];
    $title = 'Update Job Group of ';
} else {
    $typeName = '&type=new';
    $title = 'Add New Job Group';
}
?>
<div class="wrap">
    <h1><?php echo $title.$model['groupItem']->GroupName; ?> <a href="<?php echo esc_url('?page='.$model['slug']); ?>" class="page-title-action" title="Cancel" >Cancel</a></h1>
    <br/>
    <form class="form-horizontal" method="POST" action="<?php echo esc_url('?page='.$model['slug'].$typeName); ?>">
        <?php if(isset($model['isUpdate'])) { ?>
            <input type="hidden" class="form-control" name="groupID"
                   value="<?php echo $model['groupItem']->ID; ?>" />
        <?php } ?>
        <div class="form-group">
            <label class="col-sm-2">Job Group Name: </label>
            <div    class="col-sm-8">
                <input type="text" class="form-control" name="groupName" value="<?php if(isset($model['isUpdate'])) { echo $model['groupItem']->GroupName; } else { echo ''; } ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">Descriptions: </label>
            <div    class="col-sm-8">
                <input type="text" class="form-control" name="groupDescription" value="<?php if(isset($model['isUpdate'])) { echo $model['groupItem']->Description; } else { echo ''; } ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">Job Branch: </label>
            <div class="col-sm-8">
                <label class="screen-reader-text" for="new_role"><?php echo 'Select Job Branch'; ?></label>
                <select name="groupBranch" id="groupBranch" class="form-control" style="width: 100%; height: 35px">
                    <option value="<?php echo $model['groupItem']->BranchID; ?><?php if(isset($model['isUpdate'])) { echo 'selected'; } ?>"><?php if(isset($model['isUpdate'])) { echo $model['groupItem']->BranchName; } else { echo 'Select Job Branch'; } ?></option>
                    <?php foreach ($model['lstBranch'] as $listBranch):
                        if($listBranch->ID != $model['groupItem']->BranchID) {?>
                            <option value="<?php echo $listBranch->ID; ?>"><?php echo $listBranch->BranchName; ?></option>
                        <?php } endforeach;?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm- 10">
                <?php if(isset($model['isUpdate'])) { ?>
                    <input type="submit" class="button button-primary" name="btnAction" value="Update" style="margin-left: 15px"/>
                <?php } else { ?>
                    <input type="submit" class="button button-primary" name="btnAction" value="Add new" style="margin-left: 15px"/>
                    <input type="submit" class="button button-primary" name="btnAction" value="Reset" />
                <?php } ?>
            </div>
        </div>
    </form>
</div>