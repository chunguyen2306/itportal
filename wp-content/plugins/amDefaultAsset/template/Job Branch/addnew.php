<?php
/**
 * Created by PhpStorm.
 * User: CPU10339-local
 * Date: 17/03/2017
 * Time: 3:02 PM
 */
$typeName = '';
$title = '';
if(isset($model['isUpdate'])){
    $typeName = '&type=update&id='.$_REQUEST['id'];
    $title = 'Update Job Branch';

} else {
    $typeName = '&type=new';
    $title = 'Add New Job Branch';
}
?>
<div class="wrap">
    <h1><?php echo $title; ?> <a href="<?php echo esc_url('?page='.$model['slug']); ?>" class="page-title-action" title="Cancel" >Cancel</a></h1>

    <form class="form-horizontal" method="POST" action="<?php echo esc_url('?page='.$model['slug'].$typeName); ?>">
        <?php if(isset($model['isUpdate'])) { ?>
            <input type="hidden" class="form-control" name="branchID"
                   value="<?php echo $model['jobBranchItem']->ID; ?>" />
        <?php } ?>
        <div class="form-group">
            <label class="col-sm-2">Name: </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="branchName" value="<?php if(isset($model['isUpdate'])) { echo $model['jobBranchItem']->BranchName; } else { echo ''; } ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">Descriptions: </label>
            <div class="col-sm-8">
                <input class="form-control" name="branchDescription" value="<?php if(isset($model['isUpdate'])) { echo $model['jobBranchItem']->Description; } else { echo ''; } ?>" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm- 10">
                <?php if(isset($model['isUpdate'])) { ?>
                    <input type="submit" class="button button-primary" name="btnAction" value="Update" style="margin-left: 15px"/>
                <?php } else { ?>
                    <input type="submit" class="button button-primary" name="btnAction" value="Add new" style="margin-left: 15px"/>
                    <input type="submit" class="button button-primary" name="btnAction" value="Reset" />
                <?php } ?>
            </div>
        </div>
    </form>
</div>