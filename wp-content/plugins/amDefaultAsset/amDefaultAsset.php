<?php
/*
  Plugin Name: AM Default Asset
  Plugin URI:
  Description: Chứa tất cả các chức năng quản lý Default Asset.
  Version: 1.0
  Author: daopd
  Author URI:
  License:
  Text Domain: am-default-asset
*/

if(!class_exists('AMDefaultAsset')) {
    if ( ! function_exists( 'import' ) ) {
        require_once get_template_directory() . '/package/import.php';
    }
    import ('theme.package.Abstracts.AbstractPlugin');
    //Role Class Controller
    import ('plugin.amDefaultAsset.class.controller.AMJobBranchController');
    import ('plugin.amDefaultAsset.class.controller.AMJobGroupsController');
    import ('plugin.amDefaultAsset.class.controller.AMDefaultAssetController');

    class AMDefaultAsset extends AbstractPlugin
    {

        public function __construct()
        {
            $this->ajaxName = 'amDefaultAsset';
            $this->pluginName = 'amDefaultAsset';
            $this->adminMenu = array(
                array(
                    'pageTitle' => 'AM Default Asset',
                    'menuTitle' => 'AM Default Asset',
                    'capability'=> 'manage_options',
                    'menuSlug' => 'am_default_asset',
                    'handler' => array(new AMDefaultAssetController(array(
                        'slug' => 'am_default_asset'
                    )), 'index'),
                    'icon' => 'dashicons-awards',
                    'child' => array(
                        array(
                            'pageTitle' => 'Default Asset',
                            'menuTitle' => 'Default Asset',
                            'capability'=> 'manage_options',
                            'menuSlug' => 'am_default_asset',
                            'default' => true
                        ),
                        array(
                            'pageTitle' => 'Job Branch',
                            'menuTitle' => 'Job Branch',
                            'capability'=> 'manage_options',
                            'menuSlug' => 'am_job_branch',
                            'handler' => array(new AMJobBranchController(array(
                                'slug' => 'am_job_branch'
                            )), 'index')
                        ),
                        array(
                            'pageTitle' => 'Job Groups',
                            'menuTitle' => 'Job Groups',
                            'capability'=> 'manage_options',
                            'menuSlug' => 'am_job_groups',
                            'handler' => array(new AMJobGroupsController(array(
                                'slug' => 'am_job_groups'
                            )), 'index')
                        )
                    )
                )
            );
            parent::__construct();
        }

        private function define($name, $value)
        {
            if (!defined($name)) {
                define($name, $value);
            }
        }

        protected function init_custom_hook()
        {
            // TODO: Implement init_custom_hook() method.
        }

        protected function custom_init()
        {

        }
    }
    $amDefaultAsset = new AMDefaultAsset();
    $GLOBALS['amDefaultAsset'] = $amDefaultAsset;
}