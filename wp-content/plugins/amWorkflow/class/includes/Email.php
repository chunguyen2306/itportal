<?php
import('theme.package.class.AMAsyncTask');
    function __mail_send_asyn(){
        $args = func_get_args();

        // Remove the random number that was added to the arguments
        array_pop( $args );

        $re = call_user_func_array( 'wp_mail', $args );
        error_log(json_encode($re));
        error_log($re  ? 'true':  'false');
    }
class Email {

    public static function send_asyn($to, $subject, $message, $headers = '', $attachments = array()) {
        // Get the args passed to the wp_mail function
        $args = func_get_args();

        // Add a random value to work around that fact that identical events scheduled within 10 minutes of each other
        // will not work. See: http://codex.wordpress.org/Function_Reference/wp_schedule_single_event
        $args[] = mt_rand();

        // Schedule the email to be sent
        /*$re = wp_schedule_single_event( time() + 1, 'mail_send_asyn', $args );
        if($re === false){
            logAM("wp_schedule_single_event failed");
            return $re;
        }*/
        $task = new AMAsyncTask([
            action=>'mail',
            data=> $args
        ]);
        $task->RunTask();
        return true; //add_action( 'mail_send_asyn', '__mail_send_asyn', 10, 10 );
    }

}