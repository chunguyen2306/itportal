<?php
/**
 * Action Document
 * Name: Email Action
 */
if (!class_exists('EmailAction')) {
    import('plugin.amUser.class.factory.AMUserFactory');
    import('plugin.amWorkflow.class.includes.Email');

    class EmailAction
    {
        function sendMail($requestData, $actionConfig)
        {
            $requestData->loadPKData();
            $menVal = new MentionValue($requestData);
            $cheatConfig = using('plugin.itportal.config.Cheat')['wfNoti'];

            // reciver
            $toDomains = [];
            $toData = MentionUtil::builDocument($actionConfig->to, null, function ($refer) use ($menVal, $actionConfig, &$failed, &$toDomains, &$cheatConfig) {
                $role = $refer->value;
                $roleDomain = $menVal->getRoleDomain($role);

                if ($roleDomain){
//                    if($roleDomain == $cheatConfig['user'])
//                        $roleDomain = $cheatConfig['delegate'];
                    $toDomains[] = $roleDomain;
                }
            });
            if ($failed)
                return AMRespone::busFailed($failed, "Failed to get domain");
            $to = join('@vng.com.vn, ', $toDomains) . '@vng.com.vn';

            // subject
            $referOption = new stdClass();
            $referOption->getFullName = 1;
            $subject = MentionUtil::builDocument($actionConfig->subject, $referOption, function ($refer) use ($requestData, $menVal, $actionConfig, &$failed) {
                $value = $menVal->of($refer);
                if ($value == null) {
                    logAM("Mention get valued failed : " . json_encode($refer));
                    $failed = 1;
                    return;
                }

                if ($refer->srcKey == 'info' && $refer->value == 'ID')
                    $value = str_pad($value, 10, "0", STR_PAD_LEFT);

                return $value;
            });

            // body
            $referOption->mailTo = $to;
            $referOption->mailSubject = $subject;
            $referOption->mailToDomains = $toDomains;
            $referOption->mailToQuestion = $actionConfig->mailToQuestion;
            $body = MentionUtil::builDocument($actionConfig->body, $referOption, function ($refer) use ($requestData, $menVal, $actionConfig, &$failed, $subject) {

                $value = $menVal->of($refer);
                if ($value == null) {
                    logAM("Mention get valued failed : " . $value . 'refer: ' . json_encode($refer));
                    $failed = 1;
                }
                return $value;
            });


            $result = Email::send_asyn($to, $subject, $body);

            if ($result == false)
                return AMRespone::busFailed("Mail acion failed");

            return new AMRespone();
        }
    }
}
return new EmailAction();