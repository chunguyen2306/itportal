<?php
/**
 * Action Document
 * Name: Asset Action
 * Description: test test
 */
if (!class_exists('AssetAction')) {
    import("plugin.amAsset.class.ajax.AMAssetAjax");
    import("plugin.itportal.API.CMDB.CMDB");

    class AssetAction {
        public function changeAssetState(RequestData $requestData, $actionConfig) {

            $lrAsset = MentionUtil::getListRefer($actionConfig->list);
            if ($lrAsset === false)
                return AMRespone::busFailed($actionConfig, "Parse mention failed");
            $updateInfos = [];
            $template = new stdClass();
            $domain = null;
            if($actionConfig->newState == 'In Use'){
                if($actionConfig->Owner == null)
                    return AMRespone::busFailed($actionConfig, 'Missing owner config for changeAssetState action');

                $field = $requestData->getFieldDefinition($actionConfig->Owner);
                $value  = $field->value;
                if($value == null)
                    return AMRespone::busFailed([$field, $actionConfig], "Field '$field->DisplayName' can not be null, It is required by changeAssetState");

                if(is_object($value))
                    $domain = $value->Domain;

                if($domain == null)
                    return AMRespone::busFailed($field, "Can not get domain from field '$field->DisplayName'");

                $template->method = 'UpdateAssetOwner';
                $template->data = $domain;
            }else{
                $template->method = 'UpdateAssetState';
                $template->data = $actionConfig->newState;
            }

            foreach ($lrAsset as $referAsset) {
                $fieldName = $referAsset->value;
                if ($fieldName == null)
                    return AMRespone::busFailed($referAsset, "Refer surprise: missing value");

                $fieldDefinition = $requestData->getFieldDefinition($fieldName);
                $assetValue = null;

                if($fieldDefinition->ParentID){
                    $tableDefinition =  $requestData->getParentFieldDefinition($fieldName);
                    $assetValue = $tableDefinition->value;
                }else{
                    $assetValue = $fieldDefinition->value;
                }

                $fieldDefinition->value;
                if ($assetValue === null)
                    continue;

                if(is_array($assetValue)){
                    foreach ($assetValue as $row){
                        $assetName = $row->{$fieldName};
                        if(!$assetName)
                            continue;

                        $temp = clone $template;
                        $temp->assetName = $assetName;
                        $updateInfos[] = $temp;
                    }
                }else{

                    $temp = clone $template;
                    $temp->assetName = $assetValue;
                    $updateInfos[] = $temp;
                }
            }

            // dev_upgrade white list asset
//            if(false && $_SERVER['REMOTE_ADDR'] != '127.0.0.1'){
//                //$whileListAsset = [
//                //    'MOU10884',
//                //    'CPU11699',
//                //    'CPU11700',
//                //    'CPU11701',
//                //    'CPU11702',
//                //    'CPU11703',
//                //    'CPU11704',
//                //    'CPU11705',
//                //    'RAM13678',
//                //    'RAM13743'
//                //];
//                //$newUpdateInfos = [];
//                //foreach ($updateInfos as $item){
//                //    if(in_array($item->assetName, $whileListAsset))
//                //        $newUpdateInfos[] = $item;
//                //}
//                //$updateInfos = $newUpdateInfos;
//            }
            if(!$updateInfos)
                AMRespone::busFailed(null, 'no asset for change asset state');

            return $this->updateByCMDB($requestData, $actionConfig, $updateInfos);
        }

        public function associateAsset(RequestData $requestData, $actionConfig) {

            $lrAsset = MentionUtil::getListRefer($actionConfig->list);
            if ($lrAsset === false)
                return AMRespone::busFailed($actionConfig, "Parse mention failed");
            $updateInfos = [];
            $template = new stdClass();
            $template->method = 'UpdateAssociateAsset';
            $domain = null;

            foreach ($lrAsset as $referAsset) {
                $fieldName = $referAsset->value;
                if ($fieldName == null)
                    return AMRespone::busFailed($referAsset, "Refer surprise: missing value");

                $fieldDefinition = $requestData->getFieldDefinition($fieldName);
                $assetValue = null;

                if($fieldDefinition->ParentID){
                    $tableDefinition =  $requestData->getParentFieldDefinition($fieldName);
                    $assetValue = $tableDefinition->value;
                }else{
                    $assetValue = $fieldDefinition->value;
                }

                $fieldDefinition->value;
                if ($assetValue === null)
                    continue;

                if(is_array($assetValue)){
                    $userInputData = $requestData->userInputFieldData[$tableDefinition->Name];
                    foreach ($assetValue as $row){
                        $assetName = $row->{$fieldName};
                        if(!$assetName)
                            continue;

                        $userInputItem = null;
                        foreach($userInputData as $i => $value){
                            if($userInputData[$i][$fieldName] == $assetName){
                                $userInputItem = $userInputData[$i];
                                break;
                            }
                        }
                        
                        if(!$userInputItem || !$userInputItem['_action_asset_parent'])
                            continue;

                        $temp = clone $template;
                        $temp->assetName = $assetName;
                        $temp->data = $userInputItem['_action_asset_parent'];
                        $updateInfos[] = $temp;
                    }
                }else{
                    $userInputData = $requestData->userInputFieldData;

                    $temp = clone $template;
                    $temp->assetName = $assetValue;
                    $temp->data = $userInputData['_action_asset_parent'];
                    $updateInfos[] = $temp;
                }
            }

            if(!$updateInfos)
                return new AMRespone();

            return $this->updateByCMDB($requestData, $actionConfig, $updateInfos);
        }

        function updateByCMDB(RequestData $requestData, $actionConfig, $updateInfos){
            $cmdb = new CMDB();
            $dataInJson = am_json_encode($updateInfos);


            $start = microtime(true);
            $resultInJson = $cmdb->get(
                "CMDBEX",
                ['m', "updateInfos"],
                ['UpdateAsset', $dataInJson]
            );
            $end = microtime(true);

            $responses = am_json_decode($resultInJson);

            if($responses == null)
                return AMRespone::busFailed([$updateInfos, $dataInJson,  $actionConfig, $resultInJson], "Call CMDB failed");

            $api = using('plugin.itportal.API.Asset.APIAssetStateHistory');

            $requestID = $requestData->request->ID;
            $wfDisplayName = $requestData->workflow->DisplayName;
            $stepDefDisplayName = $requestData->getCurrentStep()->definition->DisplayName;
            $sysComment = "[Workflow Asset State Action][Request: $requestID][Workflow: $wfDisplayName][Step: $stepDefDisplayName]";

            foreach ($responses as $res){
                if($res->code < 200 || $res->code > 299)
                    return AMRespone::busFailed($responses, "CMDB bus failed");

                if($res->code == 200 ){
                    $data = $res->data;
                    $historyData = [
                        "stateId" => $data->ID,
                        "assetName" => $data->AssetName,
                        "newState" => $data->NewState,
                        "previousState" => $data->PreviousState,
                        "actionDate" => date('Y-m-d H:i:s', $data->StartDate),
                        "userAction" => $requestData->currentDomain,
                        "comment" => $requestData->getCurTask()->comment,
                        "sysComment" => $sysComment,

                    ];
                    $data = [
                        'data' => am_json_encode($historyData)
                    ];

                    $historyRes = $api->InsertStateHistory($data);
                    if($historyRes->code != AMResponeCode::$ok)
                        return $historyRes;
                }
            }

            return new AMRespone($responses);
        }
    }

    class CMDBEXRequest {
        public $u = "cmdbex";

        public function __construct() {
        }

        public function send($method, $listParam, $listValue){
            $listParam[] = "m";
            $listValue[] = $method;
            return (new AMAssetAjax())->ServiceBus([
                "u" => $this->u,
                "p" => json_encode($listParam),
                "v" => json_encode($listValue)
            ]);
        }
    }
}
return new AssetAction();
