<?php
/**
 * Action Document
 * Name: Asset Action
 * Description: test test
 */


ini_set('xdebug.var_display_max_depth', 5555);
ini_set('xdebug.var_display_max_children', 5555);
ini_set('xdebug.var_display_max_data', 5555);


if (!class_exists('RequestAction')) {

    import("plugin.amAsset.class.ajax.AMAssetAjax");

    class RequestAction {
        function createSubOrder(RequestData $requestData, $actionConfig) {
            if (function_exists("getDataByCondition") == false) {
                import("plugin.itportal.API.CMDB.CMDB");

                function getDataByCondition($requestData, $condition) {
                    if (sizeof($condition->expressions) === 0)
                        return null;

                    // filter table field
                    $operatorIsAnd = $condition->operator == 2 || $condition->operator == 4;
                    $operatorGetTrueVal = $condition->operator == 1 || $condition->operator == 2;
                    $mapLeft  = [];
                    $field = [];
                    foreach ($condition->expressions as $exp) {
                        if (isset($mapLeft[$exp->pro])) {
                            $left = $mapLeft[$exp->pro];
                        }
                        else {
                            $left               = getLeftValue($requestData, $exp);
                            $mapLeft[$exp->pro] = $left;
                        }

                        $checkValue = $exp->checkValue;
                        $trues = [];
                        foreach ($left->value as $item) {
                            if ($operatorGetTrueVal) {
                                // lấy giá trị đúng
                                if ($item[0] == $checkValue)
                                    $trues[] = $item[1];
                            }
                            else {
                                // lấy giá trị sai
                                if ($item[0] != $checkValue)
                                    $trues[] = $item[1];
                            }
                        }

                        $isOK = sizeof($trues) > 0;
                        if ($isOK == false){
                            if($operatorIsAnd)
                                return null;
                            else
                                continue;
                        }

                        $oldValue = $field[$left->fieldName];
                        if ($oldValue) {
                            if ($operatorIsAnd) {
                                $field[$left->fieldName] = array_intersect($oldValue, $trues);
                            }else{
                                $temp                    = array_merge($oldValue, $trues);
                                $temp                    = array_unique($temp, SORT_REGULAR);
                                $field[$left->fieldName] = $temp;
                            }
                        }
                        else {
                            $field[$left->fieldName] = $trues;
                        }
                    }
                    $re = new stdClass();
                    $re->field = &$field;
                    return $re;
                }

                function getLeftValue($requestData, $exp) {
                    $re = new stdClass();
                    switch ($exp->type) {
                        case "field":
                            $fieldName         = $exp->pro;
                            $checkType         = $exp->checkType;
                            $fieldDefinition   = $requestData->getFieldDefinition($fieldName);
                            $requestDataTypeID = $fieldDefinition->RequestDataTypeID;

                            // parrent value
                            $parrent = null;
                            if ($fieldDefinition->ParentID) {
                                foreach ($requestData->listFieldDefinition as $parrentDefinition) {
                                    if ($parrentDefinition->ID == $fieldDefinition->ParentID) {
                                        $parrent = $parrentDefinition;
                                        break;
                                    }
                                }
                                if ($parrent == null)
                                    return AMRespone::busFailed([$requestData->listFieldDefinition,
                                        $fieldDefinition->ParentID], 'find parentFieldDefinition by parentID failed');
                            }

                            // child
                            $reValue = []; // struct [['value', 'record|value']]
                            if ($parrent) {
                                $listValue = [];
                                foreach ($parrent->value as $record) {
                                    $val         = $record->{$fieldName};
                                    $reValue[]   = [$val, $record];
                                    $listValue[] = $val;
                                }
                            }
                            else {
                                $listValue[] = $fieldDefinition->value;
                                $reValue     = [$reValue, $reValue];
                            }

                            // attr
                            if ($checkType) {
                                $listValue = array_values(array_unique($listValue));
                                if (isAssetCategory($requestDataTypeID) == false) {
                                    logAM("not support not assetCategory");
                                    return null;
                                }

                                $value = call_user_func("getAttr_" . $checkType, "asset", $requestDataTypeID, $listValue);
                                foreach ($reValue as &$item)
                                    $item[0] = $value->{$item[0]};
                            }

                            // struct $reValue: [['type-value(type-to-compare)', 'record|value']]
                            $re->value     = $reValue;
                            $re->fieldName = $parrent->Name;
                            break;
                        default:
                            logAM("Not support '$exp->leftType'");
                            //                                foreach ($requestData->listStepDefinition as $stepDefinition) {
                            //                                    if ($stepDefinition->Name == $exp->leftValue){
                            //                                        $stepDefinition->step
                            //                                    }
                            //                                }
                            break;
                    }

                    return $re;
                }

                function getAttr_assetType($category, $type, $list) {
                    switch ($category) {
                        case "asset":
                            $cmdb     = new CMDB();
                            $response = $cmdb->get("CMDBEX", ["m", "list", "requestDataTypeID"], ["GetAssetTypeID", json_encode($list), $type]);
                            break;

                        default:
                            logAM("not support not assetCategory");
                    }
                    return json_decode($response);
                }

                function getAttr_modelGroup($category, $type, $list) {
                    import('plugin.itportal.Factory.AMRequestDefinitionModelGroupDetailFactory');
                    $fac  = new AMRequestDefinitionModelGroupDetailFactory();
                    $list = $fac->getByModelNames($list);
                    $map  = new stdClass();
                    foreach ($list as $item)
                        $map->{$item->ModelName} = $item->GroupID;

                    return $map;
                }

                function isAssetCategory($fieldType) {
                    return true; // updating
                }
            }

            $condition = $actionConfig->condition;
            $re        = getDataByCondition($requestData, $condition);

            if ($re === null || sizeof($re->field) == 0)
                return new AMRespone();

            import('plugin.amWorkflow.class.includes.RequestDataTypeID');

            // mapping field
            $desField               = [];
            $desFieldInit           = $re->field;
            $listDesFieldDefinition = null;
            foreach ($actionConfig->fieldMapping as $desMapping => $srcMapping) {
                if (is_object($srcMapping)) {
                    if ($srcMapping->__type == RequestDataTypeID::$table) {
                        $srcName = $srcMapping->__value;
                        $listSrc = isset($desFieldInit[$srcName]) ? $desFieldInit[$srcName] : $requestData->getFieldvalue($srcName);
                        $listDes = [];
                        foreach ($listSrc as $src) {
                            $des   = [];
                            $multi = 1;
                            foreach ($srcMapping as $childDesMapping => $childSrcMapping) {
                                if ($childDesMapping == "__parrent")
                                    continue;
                                $srcValue = $src->{$childSrcMapping};
                                $doMulti  = false;
                                if ($requestData->getFieldDefinition($childSrcMapping)->RequestDataTypeID == RequestDataTypeID::$number) {
                                    // check des type is not number
                                    $desDataTypeIsNumber = (new AMRequestFieldDefinitionFactory())->getOne(["filter" => [["RequestDefinitionID = $actionConfig->workflowID",
                                        'AND'],
                                        ["Name = '$childDesMapping'", 'AND'],
                                        ["RequestDataTypeID = " . RequestDataTypeID::$number],]]);

                                    if (!$desDataTypeIsNumber) {
                                        $multi   *= $srcValue;
                                        $doMulti = true;
                                    }
                                }

                                if ($doMulti == false)
                                    $des[$childDesMapping] = $srcValue;
                            }

                            for ($i = 0; $i < $multi; $i++)
                                $listDes[] = $des;
                        }

                        $desField[$desMapping] = &$listDes;
                    }
                    else {
                        $desField[$desMapping] = $srcMapping->__value;
                    }
                }
                else {
                    $value                 = isset($desFieldInit[$srcMapping]) ? $desFieldInit->{$srcMapping} : $requestData->getFieldvalue($srcMapping);
                    $desField[$desMapping] = $value;
                }
            }

            // mapping step
            $srcStep = $requestData->getCurrentStep();
            $mapStep = [];
            foreach ($actionConfig->stepMapping as $desMapping => $srcMapping) {
                if (is_object($srcMapping)) {
                    $step        = $srcMapping->__value;
                }
                else {
                    $srcStep = $requestData->getStepDefinition($srcMapping)->step;
                    $step    = clone $srcStep;
                }
                unset($step->definition);
                unset($step->ID);
                $mapStep[$desMapping] = $step;
            }

            return (new AMRequestAjax())->_CreateRequest($actionConfig->workflowID, $desField, null, $mapStep, $requestData->request);

        }
    }
}
return new RequestAction();
