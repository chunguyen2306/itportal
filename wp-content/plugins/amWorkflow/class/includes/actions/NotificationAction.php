<?php
/**
 * Action Document
 * Name: Email Action
 */
if (!class_exists('NotificationAction')) {
    import('plugin.amUser.class.factory.AMUserFactory');
    import('plugin.amWorkflow.class.includes.Email');
    import('plugin.itportal.Business.Notification.Notification');
    import('plugin.itportal.Business.Notification.SMS');

    class NotificationAction {
        function notify($requestData, $actionConfig) {

            $requestData->loadPKData();
            $menVal = new MentionValue($requestData);

            // reciver
            $toDomains = [];
            if ($actionConfig->to != null) {
                MentionUtil::builDocument($actionConfig->to, null, function ($refer) use ($menVal, $actionConfig, &$failed, &$toDomains) {
                    $role       = $refer->value;
                    $roleDomain = $menVal->getRoleDomain($role);
                    if ($roleDomain) {
                        if ($roleDomain == 'minhlh')
                            $roleDomain = '';
                    }
                    $toDomains[] = $roleDomain;
                });
                if ($failed)
                    return AMRespone::busFailed($failed, "Failed to get domain");

            }
            else {
                $toDomains = $actionConfig->toDomains;
            }

//            $toDomains[] = 'tiennv6';

//            $cheatConfig = using('plugin.itportal.config.Cheat')['wfNoti'];
//            if ($cheatConfig && $toDomains) {
//                $cheatDomain    = $cheatConfig['user'];
//                $delegateDomain = $cheatConfig['delegate'];
//                while (($i = array_search($cheatDomain, $toDomains)) !== false) {
//                    $toDomains[$i] = $delegateDomain;
//                }
//            }

            // tạo local while list
            if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
                $newToDomain = [];
                $whileListTo = [
                    'tiennv6',
                    //'tult',
                    //'tientm2',
                    //'lacha',
                    //'vandt',
                    //'phatbg',
                    //'doanhvtm',
                    //'khoalt',
                    //'cucth',
                    //'thiennh',

                ];
                foreach ($toDomains as $item) {
                    if (in_array(strtolower($item), $whileListTo))
                        $newToDomain[] = $item;
                }
                $toDomains = $newToDomain;
            }

            // allway list on server
//            if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1')
//                $toDomains[] = 'tult';

            if (!$toDomains)
                return new AMRespone();
            $to = join('@vng.com.vn, ', $toDomains) . '@vng.com.vn';

            // subject
            $referOption              = new stdClass();
            $referOption->getFullName = 1;
            $subject                  = MentionUtil::builDocument($actionConfig->subject, $referOption, function ($refer) use ($requestData, $menVal, $actionConfig, &$failed) {
                $value = $menVal->of($refer);
                if ($value == null) {
                    logAM("Mention get valued failed : " . json_encode($refer));
                }

                return $value;
            });

            // body
            $referOption->mailTo         = $to;
            $referOption->mailSubject    = $subject;
            $referOption->mailToDomains  = $toDomains;
            $referOption->mailToQuestion = $actionConfig->mailToQuestion;
            $body                        = MentionUtil::builDocument($actionConfig->body, $referOption, function ($refer) use ($requestData, $menVal, $actionConfig, &$failed, $subject) {

                $value = $menVal->of($refer);
                if ($value === null)
                    logAM("Mention get valued failed : " . $value . 'refer: ' . json_encode($refer));

                //if($refer->srcKey == 'info' && value == 'Creator')
                //   $value = $menVal->getUserFullname($value);

                return $value;
            });

            ini_set('xdebug.var_display_max_depth', 1115);
            ini_set('xdebug.var_display_max_children', 111256);
            ini_set('xdebug.var_display_max_data', 1111024);

            if ($actionConfig->channel->mail) {
                if (Email::send_asyn($to, $subject, $body) == false)
                    return AMRespone::busFailed([$to, $subject, $body], "Mail acion failed");
            }

            if ($actionConfig->channel->itPortal) {
                $bus = new Notification();
                $bus->push($toDomains, $subject, $requestData->request->ID, NotificationType::$workflow);
            }

            if ($actionConfig->channel->sms) {
                $bus = new SMS();
                $bus->send($toDomains, $body);
            }

            return new AMRespone();
        }
    }
}
return new NotificationAction();