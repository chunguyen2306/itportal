<?php

/**
 * Created by PhpStorm.
 * User: LAP10599-local
 * Date: 09/26/2017
 * Time: 16:56
 */
import('plugin.amUser.class.factory.AMUserFactory');

class MentionUtil {

    public static function getPattern() {
        return "/@ref\({((?!}\)).)*}\)/";
    }

    public static function getListRefer($mentionDoc) {
        $re = [];
        preg_match_all(MentionUtil::getPattern(), $mentionDoc, $listMatch);
        if ($listMatch == false)
            return false;

        $listMatch = &$listMatch[0];
        foreach ($listMatch as $str) {
            $refer = MentionUtil::stringToReferObject($str);
            if ($refer != null)
                $re[] = $refer;
        }

        return $re;
    }

    public static function builDocument($mentionDoc, $option, $callBack) {
        return preg_replace_callback(MentionUtil::getPattern(), function ($match) use (&$callBack, $option) {

            if ($match == null)
                return;

            $jsonRefer = $match[0];
            if ($jsonRefer == null)
                return;

            $refer           = MentionUtil::stringToReferObject($jsonRefer);
            $refer->__option = $option;
            return $callBack($refer);
        }, $mentionDoc);
    }

    static function stringToReferObject($str) {
        if ($str == null)
            return false;

        $start = 5;
        $count = strlen($str) - $start - 1;
        if ($count < 3)
            return false;
        $json = substr($str, $start, $count);
        return json_decode($json);
    }
}

class MentionValue {
    public $requestData;
    public $facUser;

    public function __construct(RequestData $requestData) {
        $this->requestData = $requestData;
    }

    public function of($refer) {
        switch ($refer->srcKey) {
            case 'user':
                $domain = $this->getRoleDomain($refer->value);
                if ($refer->__option && $refer->__option->getFullName)
                    return $this->getUserFullname($domain);
                else
                    return $domain;
            case 'wf':
                return $this->getWFInfo($refer->value);
            case 'info':
                return $this->getRequestInfo($refer);
            case 'field':
                return $this->getFieldValue($refer);
            case 'task':
                return $this->getCurTask($refer);
        }

        return '';
    }

    public function getUserFullname($userInfo) {
        // get user number

        $domain = $userInfo;
        if (is_object($userInfo)) {
            if ($userInfo->FullName)
                return $userInfo->FullName;

            $domain = $userInfo->Domain;
        }

        if (!$userInfo || is_string($userInfo) == false) {
            AMRespone::busFailed('', 'Can not get user full name by null');
            return '';
        }


        // get user fulname
        if ($this->facUser == null)
            $this->facUser = new AMUserFactory();

        $result = $this->facUser->GetUserInformationByDomain($domain);

        return $result ? $result->FullName : '';
    }

    public function getRoleDomain($role) {
        $requestData = $this->requestData;
        if (is_numeric($role)) { // default value
            if ($role < 0) {
                switch ($role) {
                    case -1:
                        return $requestData->getCurrentStep()->Owner;
                        break;
                    case -2:
                        return $requestData->getNextStep()->Owner;
                        break;
                    case -3:
                        if ($requestData->request->CurrentIndex <= 0)
                            return '';

                        return $requestData->listStepDefinition[$requestData->request->CurrentIndex - 1]->step->Owner;
                        break;
                    case -4:
                        return $requestData->listStepDefinition[0]->step->Owner;
                        break;
                }
            }
            else {
                foreach ($requestData->listStepDefinition as $stepDefinition) {
                    if ($stepDefinition->Role == $role)
                        return $stepDefinition->step->Owner;
                }
            }
        }

        if (is_string($role)) { // get domain for deapt head
            if (strlen($role) < 3) {
                AMRespone::busFailed($role, "Role invalidate");
                return '';
            }

            $roleID     = $role[0] + 0;
            $fieldName  = substr($role, 2);
            $fieldValue = $requestData->getFieldvalue($fieldName);
            if ($fieldValue == null) {
                AMRespone::busFailed($role, "Field '$fieldName' be null");
                return '';
            }

            if ($roleID == RoleID::$user)
                return $fieldValue->Domain;

            if ($roleID == RoleID::$deptHead) {
                $roleAPI = using('plugin.itportal.API.RoleAndPermission.APIRoleAndPermission');
                $role    = $roleAPI->GetDeptHeadByDeptName(['deptName' => $fieldValue->Domain]);
                return $role->Domain;
            }
        }

        AMRespone::busFailed([
            'role' => $role,
            'requestData' => $requestData->getRawData(),
        ], "MentionValue getRoleDomain failed");
        return '';
    }

    function getCurTask($refer) {
        switch ($refer->value) {
            case 'comment':
                $commentContent = $this->requestData->getCurTask()->comment;
                return $commentContent ? $commentContent : '';
        }
    }

    function getWFInfo($attrName) {
        return $this->requestData->workflow->{$attrName};
    }

    function getRequestInfo($refer) {
        switch ($refer->value) {
            case '_orderContent':
                return $this->createOrderContent();
            case '_approval':
                return $this->createApprovalButton($refer);
            case '_link':
                return $this->createLink();
            default:
                return $this->requestData->request->{$refer->value};
        }
    }

    function getFieldValue($refer) {
        $fieldDefinition = $this->requestData->getFieldDefinition($refer->value);
        $value           = $fieldDefinition->value;
        if (is_string($value))
            return $value;

        if (is_object($value) && $fieldDefinition->RequestDataTypeID == RequestDataTypeID::$user) {
            return $refer->__option->getFullName ? $value->FullName : $value->Domain;
        }

        AMRespone::busFailed([$fieldDefinition, $refer], 'Mention value can not get field value');
    }

    function createOrderContent() {
        $rd   = $this->requestData;
        $list = [];

        // head
        $list[] = '<div>   
   <span style="font-family: Roboto, sans-serif; box-sizing: border-box; outline: none; border: 1px solid rgb(200, 200, 200); position: relative; display: block; border-radius: 5px; margin: 20px 0px; background-color: rgb(255, 255, 255);">
	<h3 style="outline:none;padding:0px;font-size:16px;margin-top: 6px;margin-left: 10px;">Tiến độ xử lý</h3>
	<div style="outline:none;max-width:100%;overflow-x:auto;font-size:unset;">
		<ul style="outline:none;overflow-x:auto;text-align: center;white-space: nowrap;margin: 0;">';

        // steps
        foreach ($rd->listStepDefinition as $stepDefinition) {
            $isApproved     = $stepDefinition->step->IsApproved;
            $isCurrentIndex = ($rd->request->CurrentIndex) == $stepDefinition->StepIndex;
            $list[]         = '
			<li style="outline:none;display: inline-block;list-style-type:none;vertical-align:middle;padding-top:25px;">
				<div style="box-sizing: border-box; outline: none; text-align: center; height: 35px; min-width: 250px; padding-top: 3px;';

            // display name
            if ($isApproved)
                $list[] = ' color: rgb(104, 104, 104);">';
            else if ($isCurrentIndex)
                $list[] = ' color: rgb(196, 196, 196);">';
            else
                $list[] = '">';
            $list[] = $stepDefinition->DisplayName;
            $list[] = '</div>
				<div style="font-family: FontAwesome; box-sizing: border-box; outline: none; text-align: center; height: 35px; min-width: 180px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 30px; line-height: 1; z-index: 1;">
					<span style="font-family: Roboto, sans-serif;box-sizing: border-box;outline: none;cursor: pointer;border-radius: 50%;padding: 4.3px 20px;position: relative;z-index: 1;background: rgb(';

            // icon
            if ($isApproved)
                $list[] = '115, 206, 136';
            else if ($isCurrentIndex)
                $list[] = '226, 212, 120';
            else
                $list[] = '196, 196, 196';
            $list[] = ');"></span>
				</div>
				<div style="box-sizing: border-box; outline: none; text-align: center; height: 55px; min-width: 180px; font-size: 14px; padding-top: 3px;';

            // status
            if ($isApproved)
                $list[] = ' color: rgb(104, 104, 104)';
            else if ($isCurrentIndex)
                $list[] = '';
            else
                $list[] = ' color: rgb(196, 196, 196)';

            // owner
            $list[] = '">
					<span style="box-sizing: border-box; outline: none;">';
            $list[] = $stepDefinition->step->Owner;

            // intend
            $list[] = '</span>&nbsp;<br style="box-sizing: border-box; outline: none;">
					<span style="box-sizing: border-box; outline: none;">';

            if ($isApproved) {
                if ($stepDefinition->StepIndex > 0)
                    $list[] = 'đã duyệt';
            }
            else if ($isApproved == null) {
                $list[] = 'dự kiến ';
                $list[] = $stepDefinition->MinIntendDuration;
                $list[] = ' - ';
                $list[] = $stepDefinition->MaxIntendDuration;
                $list[] = ' giờ';
            }
            else {
                $list[] = 'từ chối';
            }

            // end
            $list[] = '</span>
					<span style="box-sizing: border-box; outline: none;"></span>
				</div>
			</li>';
        }
        $list[] = '
		</ul>
	</div>
 </span>';


        // dev_upgrade tạm bỏ phần step, chờ sửa gui cho đẹp
        $list = ['<div>   '];


        // field
        $list[] = ' 
   
   <div style="font-family: Roboto, sans-serif; box-sizing: border-box; outline: none; overflow: hidden; margin-bottom: 20px; background-color: 
   rgb(255, 255, 255);">';

        $fieldCan = $rd->getFieldCan();
        foreach ($rd->listFieldDefinition as $fieldDefinition) {
            $fieldBeHiden = !$fieldCan->{$fieldDefinition->Name}->view;
            if ($fieldBeHiden)
                continue;

            $fieldValue = $fieldDefinition->fkvalue;
            $list[]     = '
      <div style="box-sizing: border-box;outline: none;float: left;position: relative;background-image: initial;background-position: initial;background-size: initial;background-repeat: initial;background-attachment: initial;background-origin: initial;background-clip: initial;min-width: 200px;padding: 10px;';
            $width      = $fieldDefinition->cf->style->width;
            if ($width) {
                if ($width == 31 || $width == 32 || $width == 33)
                    $width = 30;

                if (is_string($width) && strlen($width) > 2) {
                    if (is_numeric(substr($width, strlen($width) - 1)))
                        $width = $width . '%';
                }
                else if (is_numeric($width)) {
                    $width = $width . '%';
                }
                $list[] = 'width: ';
                $list[] = $width;
                $list[] = ';';
            }
            $list[] = ';"><strong style="box-sizing: border-box; outline: none; width: 213.177px; background: inherit; border: none; margin-bottom: 5px; font-size: 16px; display: block;">';
            $list[] = $fieldDefinition->DisplayName;
            $list[] = '</strong>';

            switch ($fieldDefinition->RequestDataTypeID) {
                case RequestDataTypeID::$user:
                    $list[] = '
         <div style="box-sizing: border-box; outline: none; width: 213.177px; background: inherit;">';
                    if ($fieldValue->FullName) {
                        $list[] = $fieldValue->FullName;
                        $list[] = ' - ';
                        $list[] = $fieldValue->JobTitle;
                    }
                    else {
                        $list[] = '...';
                    }
                    $list[] = '</div>
         <div style="box-sizing: border-box; outline: none; width: 213.177px; background: inherit;">Phòng ban: ';
                    if ($fieldValue->Department) {
                        $list[] = $fieldValue->Department;
                    }
                    else {
                        $list[] = '...';
                    }
                    $list[] = '</div>
         <div style="box-sizing: border-box; outline: none; width: 213.177px; background: inherit;">Địa chỉ: ';
                    if ($fieldValue->Site) {
                        $list[] = $fieldValue->Site;
                    }
                    else {
                        $list[] = '...';
                    }
                    $list[] = '</div>';
                    break;

                case RequestDataTypeID::$intendTime:
                    $list[] = '
         <div style="box-sizing: border-box; outline: none; width: 213.177px; background: inherit;">';
                    if ($fieldDefinition->RequestDataTypeID == RequestDataTypeID::$intendTime)
                        $list[] = 'Yêu cầu dự kiến sẽ hoàn thành trước ';
                    $list[] = $fieldValue;
                    $list[] = '</div>';
                    break;

                case RequestDataTypeID::$table:
                    $list[] = '
         <table style="box-sizing: border-box;outline: none;border-collapse: collapse;width: 100%;background: inherit;">
            <thead style="box-sizing: border-box; outline: none;">
            <tr style="box-sizing: border-box; outline: none;">';

                    $showCols = [];
                    foreach ($fieldDefinition->listChild as $childDefinition) {
                        $fieldBeHiden = !$fieldCan->{$childDefinition->Name}->view;
                        if ($fieldBeHiden)
                            continue;

                        $showCols[] = $childDefinition;

                        $list[] = '
               <th style="box-sizing: border-box;outline: none;text-align: left;background-color: rgb(204, 204, 204);padding: 6px;';
                        $list[] = '"><strong style="box-sizing: border-box; outline: none; border: none; margin-bottom: 5px; font-size: 16px; display: block;">';
                        $list[] = $childDefinition->DisplayName;
                        $list[] = '</strong></th>';
                    }

                    $list[] = '
                                </tr>
            </thead>
            <tbody style="box-sizing: border-box; outline: none;">';
                    foreach ($fieldDefinition->fkvalue as $row) {

                        $list[] = '
            <tr style="box-sizing: border-box; outline: none;">';
                        foreach ($showCols as $childDefinition) {
                            $list[] = '
               <td style="box-sizing: border-box;outline: none;border-top: none;border-bottom: 1px solid rgb(221, 221, 221);border-right: 1px solid rgb(220, 220, 221);position: relative;padding: 6px;background-color: rgba(198, 221, 242, 0.3);">';
                            $list[] = $row->{$childDefinition->Name};
                            $list[] = '</td>';

                        }
                        $list[] = '
            </tr>';
                    }

                    $list[] = '
            </tbody>
         </table>';
                    break;

                default:
                    $list[] = '<div>';
                    $list[] = $fieldValue;
                    $list[] = '</div>';
                    break;
            }

            $list[] = '
      </div>';
        }

        $list[] = '
   </div>
</div>';

        return join('', $list);
    }

    function createApprovalButton($refer) {
        $option = $refer->__option;
        if (isset($option->mailSubject) == false || isset($option->mailToDomains) == false) {
            logAM('MentionValue createApprovalButton option required');
            return;
        }

        $requestData = $this->requestData;
        $nextStep    = $requestData->getNextStep();
        if (!$nextStep && $option->mailToQuestion == null) {
            logAM('MentionValue: no next step');
            return;
        }

        $canAllDomainApproval = true; // nếu có 1 domain không có quyền là failed thì log lại
        $canOneApproval       = false; // 1 trong các domain được quyền duyệt

        // check quyền approval của các domain được gởi mail
        foreach ($option->mailToDomains as $domain) {
            if ($domain === $nextStep->Owner) {
                if ($canOneApproval == false)
                    $canOneApproval = true;
            }
            else {
                if ($canAllDomainApproval)
                    $canAllDomainApproval = false;
            }
        }

        if ($canOneApproval == false && $option->mailToQuestion == null)// nếu không có ai được phép duyệt hay bình luận
            return '';
        // ghi log
        if ($canAllDomainApproval == false) {
            logAM('Có vẻ approval email engine đang gởi mail đến một email không có quyền approval\n\t' . am_json_encode($requestData->request));
        }

        // mail to
        $mailToStart = 'href="mailto:itportal@vng.com.vn?subject=' . rawurlencode($option->mailSubject) . '&body=';
        $mailToEnd   = rawurlencode("\n\n\n" . $this->orderApprovalCodeContent()) . '"';

        $lContent[] = '<div style="font-family: Roboto, sans-serif; box-sizing: border-box; outline: none; text-align: right; background-color: rgb(255, 255, 255);">';

        if ($option->mailToQuestion) {
            // reply question
            $lContent[] = '
   <a style="font-family: Roboto, sans-serif; outline: none; padding: 10px 25px; color: rgb(255, 255, 255); background: #f2884b; border-width: 0px; border-style: solid; border-color: initial; font-size: 14px; cursor: pointer; border-radius: 1px; font-weight: bold; min-width: 85px; text-decoration: none;text-decoration: none;" ';
            $lContent[] = $mailToStart;
            $stepOwner  = strtoupper($this->requestData->getCurrentStep()->Owner);
            $lContent[] = rawurlencode("Cung cấp thông tin cho: $stepOwner\n\nCâu hỏi: $option->mailToQuestion\n" . 'Trả lời: ');
            $lContent[] = $mailToEnd;
            $lContent[] = '>Trả lời</a>';
        }
        else {
            // request info button
            if ($nextStep->definition->CanRequestInfo) {
                $lContent[] = '
   <a style="font-family: Roboto, sans-serif; outline: none; padding: 10px 8px; color: rgb(255, 255, 255); background-color: rgb(242,136,75); border-width: 0px; border-style: solid; border-color: initial; font-size: 14px; cursor: pointer; border-radius: 1px; font-weight: bold; min-width: 85px; text-decoration: none;" ';
                $lContent[] = $mailToStart;
                $lContent[] = rawurlencode("Hỏi thêm thông tin từ: domain_người_cần_hỏi\n" . 'Câu hỏi: đặt_câu_hỏi_tại_đây');

                $lContent[] = $mailToEnd;
                $lContent[] = '>Yêu cầu thông tin</a>';
            }

            // accept button
            $lContent[] = '
   <a style="font-family: Roboto, sans-serif; outline: none; padding: 10px 8px; color: rgb(255, 255, 255); background: rgb(115, 206, 136); border-width: 0px; border-style: solid; border-color: initial; font-size: 14px; cursor: pointer; border-radius: 1px; font-weight: bold; min-width: 85px; text-decoration: none;" ';
            $lContent[] = $mailToStart;
            $lContent[] = rawurlencode("Duyệt: đồng ý\n" . 'Ghi chú: ');
            $lContent[] = $mailToEnd;
            $lContent[] = '>Đồng ý</a>';

            // reject button
            $lContent[] = '
   <a style="font-family: Roboto, sans-serif; outline: none; padding: 10px 8px; color: rgb(255, 255, 255); background: silver; border-width: 0px; border-style: solid; border-color: initial; font-size: 14px; cursor: pointer; border-radius: 1px; font-weight: bold; min-width: 85px;text-decoration: none;" ';
            $lContent[] = $mailToStart;
            $lContent[] = rawurlencode("Duyệt: từ chối\n" . 'Ghi chú: ');
            $lContent[] = $mailToEnd;
            $lContent[] = '>Từ chối</a>';

            // entrust button
            /** Tạm ẩn ủy quyền trên mail
             *
             * $lContent[] = '
             * <a style="font-family: Roboto, sans-serif; outline: none; padding: 10px 8px; color: rgb(255, 255, 255); background: silver; border-width: 0px; border-style: solid; border-color: initial; font-size: 14px; cursor: pointer; border-radius: 1px; font-weight: bold; min-width: 85px;text-decoration: none;" ';
             * $lContent[] = $mailToStart;
             * $lContent[] = rawurlencode("Ủy quyền cho: domain_của_người_được_ủy_quyền\n" . 'Ghi chú: ');
             * $lContent[] = $mailToEnd;
             * $lContent[] = '>Ủy quyền</a>';
             */
        }


        $lContent[] = '
</div>';

        return join('', $lContent);
    }

    function createLink() {
        return '<a href="' . $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/user-page/order/detail/?ID=' . $this->requestData->request->ID . '" style="font-family: Roboto, sans-serif;outline: none;border-width: 0px; border-style: solid;border-color: initial;font-size: 14px;cursor: pointer;border-radius: 1px;font-weight: bold; min-width: 85px; color: #f2884b; text-decoration: none;">Xem tại đây</a>';
    }

    function orderApprovalCodeContent() {
        return "========================================================\nĐây là mã nhận diện xử lý đơn hàng, vui lòng không xóa.\n" . str_pad($this->requestData->request->ID, 12, '0', STR_PAD_LEFT) . $this->requestData->loadOrCreateApprovalCode() . "\n========================================================\n";
    }

}