<?php
/**
 * Created by PhpStorm.
 * User: LAP11400-local
 * Date: 7/24/2017
 * Time: 3:54 PM
 */
if (!function_exists('import')) {
    require_once get_template_directory() . '/package/import.php';
}

if (!class_exists('NotificationType')) {
    class NotificationType {
        public static $workflow = 1;
    }
}