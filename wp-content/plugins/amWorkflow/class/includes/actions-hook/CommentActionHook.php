<?php
class CommentActionHook {
    private $mentionVal;
    private $requestData;
    private $requestParams;
    public function __construct(RequestData $requestData, $requestParams) {
        $this->requestData = $requestData;
        $this->mentionVal = new MentionValue($requestData);
        $this->requestParams = $requestParams;
    }

    public function hook(&$listAction, $eventID){

        $mentionVal = $this->mentionVal;
        $params = $this->requestParams;
        $requestID = $this->requestData->request->ID;

        $mentionDomains  = $params['mentionDomains'];
        $questionComment = $params['comment'];
        $domain = $params['domain'];
        $approvalFullName   = $mentionVal->getUserFullname($domain);

        $actionMailConfig          = new stdClass();
        $actionMailConfig->mailToAsker      = $domain;
        $actionMailConfig->toDomains      = $mentionDomains;

        if($eventID == EventID::$ask){
            $actionMailConfig->subject = "Yêu cầu thông tin từ $approvalFullName cho đơn hàng #$requestID";
        }else{
            $actionMailConfig->subject = "Ghi chú đề cập đến tên bạn từ $approvalFullName cho đơn hàng #$requestID";
        }

        $bodyItems          = ['Chào bạn,<br><br>'];
        //            $mentionDomainCount = sizeof($mentionDomains);
        //            for ($i = 0; $i < ($mentionDomainCount - 1); $i++) {
        //                $bodyItems[] = $mentionVal->getUserFullname($mentionDomains[$i]);
        //                $bodyItems[] = ', ';
        //            }

        $bodyItems[] = $approvalFullName;
        if($eventID == EventID::$ask){
            $bodyItems[] = ' cần bạn cung cấp thông tin sau cho đơn hàng ';
        }else{
            $bodyItems[] = ' đề cập đến tên bạn trong một ghi chú cho đơn hàng ';
        }
        $bodyItems[] = '<b style="text-align: left;"><span style="font-family: Arial, sans-serif; font-size: 10pt; line-height: 14.2667px;"><font color="#f2884b">#@ref({"srcKey":"info","value":"ID","type":"text"})</font></span><span style="font-family: Arial, sans-serif; font-size: 10pt; line-height: 14.2667px; color: rgb(242, 136, 75);">&nbsp;(@ref({"srcKey":"info","value":"_link","type":"text"}))&nbsp;</span></b>';
        $bodyItems[] = '<br style="line-height: 42px;">';
        $bodyItems[] = '<span style="font-family: Arial, sans-serif, serif, EmojiFont; font-size: 10pt; line-height: 20px;"><b><span style="font-family: Arial, sans-serif, serif, EmojiFont; font-size: 12pt; line-height: 17.12px;">';
        if($eventID == EventID::$ask){
            $bodyItems[] = 'NỘI DUNG CÂU HỎI:';
        }else{
            $bodyItems[] = 'NỘI DUNG GHI CHÚ:';
        }
        $bodyItems[] = '</span></b><br>
</span>';
        $bodyItems[] = ' &nbsp; ';
        $bodyItems[] = $questionComment;
        $bodyItems[] = '<br><br>';
        $bodyItems[] = '<span style="font-family: Arial, sans-serif; font-size: 10pt; line-height: 20px;"><b><span style="font-family: Arial, sans-serif; font-size: 12pt; line-height: 17.12px;">CHI TIẾT ĐƠN HÀNG:</span></b><br></span>';
        $bodyItems[] = '@ref({"srcKey":"info","value":"_orderContent","type":"text"})';
        $bodyItems[]                      = '@ref({"srcKey":"info","value":"_approval","type":"text"})';
        $bodyItems[] = '<span style="font-family: Arial, sans-serif; font-size: 10pt; line-height: 20px;">&nbsp;<br><span style="font-family: Arial, sans-serif; text-align: justify; font-size: 10pt; line-height: 14.2667px;">Bạn có thể xem lại đơn hàng của mình tại&nbsp;<a href="https://it.vng.com.vn/user-page/order/list-order/">IT Portal</a></span><span style="font-size: medium; text-align: justify;">.&nbsp;</span>Trân trọng,<br><b style="font-size: medium;"><span style="font-family: Arial, sans-serif; font-size: 10pt;">Phòng CNTT</span></b><span style="font-family: Arial, sans-serif; font-size: 10pt;"><br>Email và Teams chat:&nbsp;<a href="mailto:helpdesk@vng.com.vn">helpdesk@vng.com.vn<br></a>Zalo:&nbsp;+84934088588<br></span><span style="font-family: Arial, sans-serif; font-size: 10pt;">IT Portal:&nbsp;</span><a href="https://it.vng.com.vn/" style="font-size: 10pt;">https://it.vng.com.vn</a></span>';
        //            $bodyItems[]                      = $mentionVal->getUserFullname($mentionDomains[$i]);
        //            $bodyItems[]                      = '.<br>';
        //            $bodyItems[]                      = $approvalFullName;
        //            $bodyItems[]                      = ' hỏi: ';
        //            $bodyItems[]                      = $questionComment;


        //            $bodyItems[]                      = '<br><br>Nội dung đơn hàng:</b><br>@ref({"srcKey":"info","value":"_orderContent","type":"text"})<br>';
        //            $bodyItems[]                      = '@ref({"srcKey":"info","value":"_approval","type":"text"})';
        //            $bodyItems[]                      = '<br><br>@ref({"srcKey":"info","value":"_link","type":"text"})<br><br>Xin cảm ơn!';


        $actionMailConfig->body           = join('', $bodyItems);
        $actionMailConfig->mailToQuestion = $questionComment;
        $actionMailConfig->channel = (object)['mail' => true];

        $listAction[] =  (object)[
            'ActionTypeID' => ActionTypeID::$notification,
            'cf' => $actionMailConfig
        ];

    }
}