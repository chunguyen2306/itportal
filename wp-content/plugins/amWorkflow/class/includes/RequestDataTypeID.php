<?php
class RequestDataTypeID{

    public static $intendTime = 10;
    public static $note = 9;
    public static $user = 8;
    public static $table = 5;
    public static $asset = 3;
    public static $number = 2;
    public static $string = 1;
    public static $domain = 6;
    public static $assetType = 7;
    public static $assetModel = 4;
    public static $excel = 11;
    public static $default = -1;
}