<?php

class QueryUtil{
    public static function in ($list, $attrName, $beDuplicate = false){
        $listIn = [];
        if($beDuplicate){
            foreach ($list as $item){
                $value = $item->{$attrName};
                if(in_array($value, $listIn) == false)
                    $listIn[] = $value;
            }
        }else{
            foreach ($list as $item)
                $listIn[] = $item->{$attrName};
        }

        return "(" . join(", ", $listIn) . ")";
    }

}