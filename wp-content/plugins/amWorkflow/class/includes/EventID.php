<?php
/**
 * Created by PhpStorm.
 * User: LAP11400-local
 * Date: 7/24/2017
 * Time: 3:54 PM
 */
if (!function_exists('import')) {
    require_once get_template_directory() . '/package/import.php';
}

if (!class_exists('EventID')) {
    class EventID {
        public static $reject = 0;
        public static $approved = 1;
        public static $ask = 2;
        public static $answer = 3;
        public static $cancel = 4;
        public static $raise = 5;
        public static $create = 6;
        public static $reCreate = 7;
        public static $done = 8;
        public static $entrust = 9;
        public static $comment = 10;
        public static $pickup = 11;
    }
}