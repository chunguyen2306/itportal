<?php

if (!class_exists('WorkflowGet')) {
    import('plugin.amWorkflow.class.factory.AMRequestDefinitionFactory');
    import('plugin.amWorkflow.class.factory.AMRequestFieldDefinitionFactory');
    import('plugin.amWorkflow.class.factory.AMRequestStepDefinitionFactory');
    import('plugin.amWorkflow.class.factory.AMRequestStepActionFactory');
    import('plugin.amWorkflow.class.factory.AMRequestStepPermissionFactory');
    import('plugin.amWorkflow.class.factory.AMRequestSubFactory');

    class WorkflowGet {
        /**
         * Lấy dữ liệu workflow (dữ liệu định nghĩa biên bản)
         * @param $wfID
         * @param bool $getActionStep true nếu muốn lấy cả dữ liệu Action của các step, mặc dịnh là true
         * @return AMRespone
         */
        public function GetWorkflowData($wfID, $getActionStep = false, $getCurrentDomain = true) {
            $re = new stdClass();
            //data
            $workflow = (new AMRequestDefinitionFactory())->getByID($wfID);;
            if ($workflow === false)
                return AMRespone::dbFailed($wfID, "GetWorkflow");
            $re->workflow = $workflow;

            //list step definition
            $temp = $this->GetWorkflowStepEditData($wfID, $getActionStep);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;
            $re->listStepDefinition = $temp->data;

            // list field definition
            $temp = $this->GetWorkflowFieldEditData($wfID);
            if ($temp->code != AMResponeCode::$ok)
                return $temp;
            $re->listFieldDefinition = $temp->data;

            // get current user
            if ($getCurrentDomain) {
                $currentUser = wp_get_current_user();
                if ($currentUser == 0 || $currentUser->user_login == false)
                    return AMRespone::busFailed(null, "not logined");

                $re->currentDomain = $currentUser->user_login;
            }

            return new AMRespone($re);
        }

        public function GetWorkflowFieldEditData($wfID) {
            // list feild
            $fac = new AMRequestFieldDefinitionFactory();

            $list = $fac->getListByRequestDefinitionID($wfID);
            if ($list === false)
                return AMRespone::dbFailed($wfID, "GetWorkflowField");

            $re = [];
            foreach ($list as $item) {
                $fieldConfig = am_json_decode($item->Config);
                if ($fieldConfig == null)
                    return AMRespone::busFailed($item->Config, "Parse field config json");

                $item->cf = $fieldConfig;
                unset($item->Config);


                $item->constrain = am_json_decode($item->Constrain);;
                unset($item->Constrain);

                if ($item->ParentID == null) {
                    $re[] = $item;
                }
                else {
                    foreach ($list as $parent) {
                        if ($item->ParentID == $parent->ID) {
                            if (isset($parent->listChild) == false)
                                $parent->listChild == [];

                            $parent->listChild[] = $item;
                        }
                    }
                }
            }

            return new AMRespone($re);
        }

        public function GetListRequest($displayName, $startIndex, $limit) {
            $fac  = new AMRequestDefinitionFactory();
            $args = array();
            if (isset($displayName) && $displayName != '') {
                $args['filter'] = array(array("DisplayName like '%" . $displayName . "%'", ""));
            }

            if (isset($limit) && $limit != 0) {
                $args['limit'] = array(
                    "at" => $startIndex,
                    "length" => $limit);
            }

            return $fac->query($args);

        }

        public function GetWorkflowStepEditData($wfID, $isEditMode) {
            $listStep = (new AMRequestStepDefinitionFactory())->getListByRequestDefinitionID($wfID);
            if ($listStep === false)
                return AMRespone::dbFailed($wfID, "GetWorkflowStep");

            usort($listStep, function ($a, $b) {
                return $a->StepIndex - $b->StepIndex;
            });

            if ($isEditMode) {
                $facAction = new AMRequestStepActionFactory();
                foreach ($listStep as $step) {
                    $listAction = $facAction->getListByStepDefinitionID($step->ID);
                    if ($listAction === false)
                        return AMRespone::dbFailed($step->ID, "GetWorkflowStepAction");

                    $step->listAction = $listAction;
                }
            }

            $facSub = new AMRequestSubFactory();
            foreach ($listStep as $step) {
                $listSub = $facSub->query(["filter" => [["StepDefinitionID = $step->ID"]]]);
                if ($listSub === false)
                    return AMRespone::dbFailed($step->ID, "Get SubWorkflow failed");

                $step->listSubWorkflow = $listSub;
            }
            return new AMRespone($listStep);
        }

        public function GetFieldCan($stepDefinitionID) {
            $fac  = new AMRequestStepPermissionFactory();
            $list = $fac->query(['filter' => [['StepDefinitionID = ' . $stepDefinitionID]]]);
            if ($list === false)
                return AMRespone::dbFailed("get step permission failed");

            $fieldCans = new stdClass();
            foreach ($list as $item) {
                $can         = new stdClass();
                $can->edit   = $item->CanEdit;
                $can->view   = $item->CanView;
                $can->update = $item->CanUpdate;
                $can->empty  = $item->CanEmpty;

                $fieldCans->{$item->FieldName} = $can;
            }

            return new AMRespone($fieldCans);
        }

        public function GetIDByDisplayName($displayName) {
            return (new AMRequestDefinitionFactory())->getOne(array("select" => 'ID',
                "filter" => array(array('DisplayName = \'' . $displayName . '\'', ''))));
        }

        public function GetCurrentUser() {
            try {
                //Get current user
                $currentUser = wp_get_current_user();
                if ($currentUser == 0 || $currentUser->user_login == false)
                    return AMRespone::notFound();

                $username = $currentUser->user_login;
                $result   = (new AMUserRoleFactory())->query(["filter" => [["Domain = '$username'", ""]]]);
                if ($result == false)
                    return AMRespone::dbFailed($username, "Get user role info");

                if (is_array($result) != 1)
                    return AMRespone::busFailed($username, "Data is weirdo");

                return new AMRespone($result[0]);
            } catch (Exception $ex) {
                return AMRespone::exception(null, $ex);
            }
        }

        public static function isNoneDataType($requestDataType){
            return $requestDataType == RequestDataTypeID::$excel
                ||$requestDataType == RequestDataTypeID::$intendTime;
        }
    }
}

