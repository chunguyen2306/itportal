<?php
if (!class_exists('ActionTypeID')) {
    class ActionTypeID {
        public static $notification = 1;
        public static $assetState = 2;
        public static $subOrder = 3;
        public static $associateAsset = 4;
    }
}