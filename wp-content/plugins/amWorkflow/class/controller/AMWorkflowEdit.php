<?php

if(!class_exists('AMWorkflowEdit')){

    // MUST USE
    import ('theme.package.Abstracts.AbstractController');
    //import your package here
    
    class AMWorkflowEdit extends AbstractController {
        protected function init(){

            $this->dir = substr(__DIR__, 0, strlen(__DIR__)-strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'workflow/workflow'
            );
        }

        public function list_action($req, $post){
            importScript('lSearchModel', 'theme.js.Libs.search-model');
            importScript('mfieldRender', 'plugin.amWorkflow.js.model.fieldRender');
            importScript('mWorkflow', 'plugin.amWorkflow.js.model.workflow');
        }
    }
}
?>