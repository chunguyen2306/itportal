<?php

if(!class_exists('AMListWorkflow')){
    import ('theme.package.Abstracts.AbstractController');
    
    class AMListWorkflow extends AbstractController {
        protected function init(){
                        
            $this->dir = substr(__DIR__, 0, strlen(__DIR__)-strlen('\class\controller'));
            $this->viewMapping = ['list' => 'workflow/listWorkflow'];
        }

        public function list_action($req, $post){
            importScript('mListWorkflow', 'plugin.amWorkflow.js.model.list-workflow');
        }
    }
}
?>