<?php

if(!class_exists('AMRequestStatusController')){
    
    // MUST USE
    import ('theme.package.Abstracts.AbstractController');
    import ('plugin.amWorkflow.class.factory.AMRequestStatusFactory');
    //import your package here
    
    class AMRequestStatusController extends AbstractController {
        protected function init(){
            $this->db = new AMRequestStatusFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__)-strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'RequestStatus/list',
                'new' => 'RequestStatus/addnew',
                'update' => 'RequestStatus/addnew'
            );
        }

        private function pagging($pageSize, $pageNum, $filter){
            $query = array();
            if($filter != null){
                $query['filter'] = array(
                    array(
                        "Name like '%".$filter."%'", ""
                    )
                );
            }
            $query['limit'] = array(
                'at' => $pageSize*($pageNum-1),
                'length' => $pageSize,
            );
            $data = $this->db->query($query);
            if(!is_array($data)){
                $data = array();
            }
            $this->_model('data', $data);
        }

        public function list_action($req, $post){
            //Do something here
            $pageIndex = 1;
            $limit = 20;
            $query = null;
            if(isset($req['pageIndex'])){
                $pageIndex = $req['pageIndex'];
            }
            if(isset($req['limit'])){
                $limit = $req['limit'];
            }
            if(isset($post['keyword']) && isset($post['btnAction'])){
                if($post['btnAction'] == 'Find' && $post['keyword'] != ''){
                    $query = $post['keyword'];
                }
            }
            $this->pagging($limit, $pageIndex, $query);
        }

        public function update_action($req, $post){
            $id = $req['id'];
            $action = $post['btnAction'];
            $this->_model('isUpdate',true);

            if($action == 'Update'){
                $newData = array(
                    'Name' => $post['F-Name'],
                    'DisplayName' => $post['F-DisplayName'],
                    'Description' => $post['F-Description']
                );

                $isExisted = $this->db->getOneBy(array(
                    Name => $newData['Name']
                ));

                if($isExisted != null) {
                    $this->_model('errors', array(
                        'Trạng thái này đã tồn tại'
                    ));
                } else {
                    $updateResult = $this->db->update($newData, array(
                        "ID" => $id
                    ));
                    wp_redirect($this->_getPath());
                    exit();
                }
            } else {
                if(isset($id)){
                    $data = $this->db->query(array(
                        'filter' => array(
                            array( "ID = ".$id, '')
                        )
                    ))[0];
                    $this->model['data'] = $data;
                }
            }
        }

        public function new_action($req, $post){

            $newData = array(
                'Name' => $post['F-Name'],
                'DisplayName' => $post['F-DisplayName'],
                'Description' => $post['F-Description']
            );

            $action = $post['btnAction'];
            if($action == 'Add new'){

                $isExisted = $this->db->getOneBy(array(
                    Name => $newData['Name']
                ));

                if($isExisted != null) {
                    $this->_model('errors', array(
                        'Tên nhóm này đã tồn tại'
                    ));
                } else {
                    $this->db->insert($newData);
                    wp_redirect( $this->_getPath() );
                    exit();
                }
            } else if($action == 'Reset'){
                $post['F-Name'] = '';
                $post['F-DisplayName'] = '';
                $post['F-Description'] = '';
            }
        }

        public function del_action($req, $post){
            $id = $req['ID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }
    }
}
?>