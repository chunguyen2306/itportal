<?php

if(!class_exists('AMRequest')){
    
    // MUST USE
    import ('theme.package.Abstracts.AbstractController');
    //import your package here
    
    class AMRequest extends AbstractController {
        protected function init(){
                        
            $this->dir = substr(__DIR__, 0, strlen(__DIR__)-strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'request/request'
            );
        }

        public function list_action($req, $post){
            importScript('lSearchModel', 'theme.js.Libs.search-model');
            importScript('mRequest', 'plugin.amWorkflow.js.model.request');
            importScript('mfieldRender', 'plugin.amWorkflow.js.model.fieldRender');
        }
    }
}
?>