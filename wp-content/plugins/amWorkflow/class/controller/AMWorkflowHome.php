<?php

if(!class_exists('AMWorkflowHome')){
    
    // MUST USE
    import ('theme.package.Abstracts.AbstractController');
    //import your package here
    
    class AMWorkflowHome extends AbstractController {
        protected function init(){
                        
            $this->dir = substr(__DIR__, 0, strlen(__DIR__)-strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'workflowHome/workflowHome'
            );
        }

        public function list_action($req, $post){
            importScript('lSearchModel', 'theme.js.Libs.search-model');
            importScript('mfieldRender', 'plugin.amWorkflow.js.model.fieldRender');
            importScript('mWorkflowHome', 'plugin.amWorkflow.js.model.workflowHome');
        }
    }
}
?>