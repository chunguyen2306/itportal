<?php

if(!class_exists('AMRequestTypeController')){
    
    // MUST USE
    import ('theme.package.Abstracts.AbstractController');
    import ('plugin.amWorkflow.class.factory.AMRequestCategoryFactory');
    import ('plugin.amWorkflow.class.factory.AMRequestTypesFactory');
    //import your package here
    
    class AMRequestTypeController extends AbstractController {
        protected function init(){
            $this->db = new AMRequestTypesFactory();
            $this->categories = new AMRequestCategoryFactory();
            $this->dir = substr(__DIR__, 0, strlen(__DIR__)-strlen('\class\controller'));
            $this->viewMapping = array(
                'list' => 'RequestType/list',
                'new' => 'RequestType/addnew',
                'update' => 'RequestType/addnew'
            );

            $this->_model('categories', $this->categories->query(array()));
        }

        private function pagging($pageSize, $pageNum, $filter){
            $query = array();
            $query['select'] = "a.ID 'ID', a.Name 'Name', b.Name 'Category', a.Description 'Description'";
            $query['order'] = "ID";
            $query['join'] = array(
                array(
                    'JOIN',
                    AMRequestCategoryFactory::$tableName.' b',
                    'a.RequestCategoryID',
                    'b.ID'
                )
            );
            if($filter != null){
                $query['filter'] = array(
                    array(
                        "a.Name like '%".$filter."%'", "OR"
                    ),
                    array(
                        "b.Name like '%".$filter."%'", ""
                    )
                );
            }
            $query['limit'] = array(
                'at' => $pageSize*($pageNum-1),
                'length' => $pageSize,
            );
            $data = $this->db->query($query);
            if(!is_array($data)){
                $data = array();
            }
            $this->_model('data', $data);
        }

        public function list_action($req, $post){
            //Do something here
            $pageIndex = 1;
            $limit = 20;
            $query = null;
            if(isset($req['pageIndex'])){
                $pageIndex = $req['pageIndex'];
            }
            if(isset($req['limit'])){
                $limit = $req['limit'];
            }
            if(isset($post['keyword']) && isset($post['btnAction'])){
                if($post['btnAction'] == 'Find' && $post['keyword'] != ''){
                    $query = $post['keyword'];
                }
            }
            $this->pagging($limit, $pageIndex, $query);
        }

        public function new_action($req, $post){

            $newData = array(
                'Name' => $post['F-Name'],
                'RequestCategoryID' => intval($post['F-Category']),
                'Description' => $post['F-Description']
            );
            $action = $post['btnAction'];
            if($action == 'Add new'){

                $isExisted = $this->db->getOneBy(array(
                    Name => $newData['Name']
                ));

                if($isExisted != null) {
                    $this->_model('errors', array(
                        'Tên loại này đã tồn tại'
                    ));
                } else {
                    $this->db->insert($newData);
                    wp_redirect($this->_getPath());
                    exit();
                }
            } else if($action == 'Reset'){
                $post['F-Name'] = '';
                $post['F-Category'] = '';
                $post['F-Description'] = '';
            }
        }

        public function update_action($req, $post){
            $id = $req['id'];
            $action = $post['btnAction'];
            $this->_model('isUpdate',true);

            if($action == 'Update'){
                $newData = array(
                    'Name' => $post['F-Name'],
                    'RequestCategoryID' => intval($post['F-Category']),
                    'Description' => $post['F-Description']
                );

                $isExisted = $this->db->getOneBy(array(
                    Name => $newData['Name']
                ));

                if($isExisted != null) {
                    $this->_model('errors', array(
                        'Tên loại này đã tồn tại'
                    ));
                } else {
                    $updateResult = $this->db->update($newData, array(
                        "ID" => $id
                    ));
                    wp_redirect($this->_getPath());
                    exit();
                }
            } else {
                if(isset($id)){
                    $data = $this->db->query(array(
                        'select' => "a.ID 'ID', a.Name 'Name', b.Name 'Category', a.Description 'Description', a.RequestCategoryID 'CategoryID'",
                        'join' => array(
                            array(
                            'JOIN',
                            AMRequestCategoryFactory::$tableName.' b',
                            'a.RequestCategoryID',
                            'b.ID'
                            )
                        ),
                        'filter' => array(
                            array( "a.ID = ".$id, '')
                        )
                    ))[0];
                    $this->model['data'] = $data;
                }
            }
        }

        public function del_action($req, $post){
            $id = $req['ID'];
            $this->_model('isDelete', $this->db->delete(array( 'ID' => $id )));
            wp_redirect( $this->_getPath() );
            exit();
        }
    }
}
?>