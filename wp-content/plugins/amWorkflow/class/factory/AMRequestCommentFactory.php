<?php
/**
 * Created by PhpStorm.
 * User: CPU10722-local
 * Date: 6/9/2017
 * Time: 11:36 AM
 */
if (!class_exists('AMRequestCommentFactory')) {
    import('theme.package.Abstracts.AbstractDatabase');

    class AMRequestCommentFactory extends AbstractDatabase {

        public static $tableName = 'am_request_comments';

        protected function tableInfo() {
            return array(
                'table_name' => AMRequestCommentFactory::$tableName
            );
        }

        public function insertComment($requestID, $stepID, $domain, $eventID, $content, $attachments) {
            $commentInArray = [
                "RequestID" => $requestID,
                "StepID" => $stepID,
                "Owner" => $domain,
                "EventID" => $eventID,
                "Time" => date('Y-m-d H:i:s'),
                "Comment" => $content,
                "Attachments" => json_encode($attachments),
            ];

            $re = $this->insert($commentInArray);

            if ($re === false)
                return $re;

            global $wpdb;
            $commentInArray["ID"] = $wpdb->insert_id;

            unset($commentInArray['Attachments']);
            $re = (object)$commentInArray;
            $re->attachments = $attachments;
            return $re;
        }

        public function query($agr = array(), $log = false, $output = OBJECT) {
            $result = parent::query($agr, $log, $output);
            if ($result) {
                foreach ($result as $item) {
                    $item->attachments = json_decode($item->Attachments);
                    unset($item->Attachments);
                }
            }

            return $result;
        }
    }
}
?>