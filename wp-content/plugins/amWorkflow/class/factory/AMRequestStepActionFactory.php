<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMRequestStepActionFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestStepActionFactory extends AbstractDatabase {

        public static $tableName = 'am_request_step_actions';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestStepActionFactory::$tableName,
                'columns' => [
                    "ID",
                    "Name",
                    "Config",
                    "IsRollback",
                    "EventID",
                    "RequestStepDefinitionID"
                ],
            );
        }

        public function insert(&$data) {
            $insertData = $data;
            $insertData["Config"] = am_json_encode($data["cf"]);
            unset($insertData["cf"]);
            return parent::insert($insertData);
        }

        public function getListByStepDefinitionID($stepID, $eventID = null, $typeID = null){

            $filter = null;
            if($eventID !== null){
                $filter = [
                    ["RequestStepDefinitionID = '$stepID'", 'AND'],
                    ["EventID = $eventID", 'AND']
                ];
            }else{
                $filter = [["RequestStepDefinitionID = '$stepID'", 'AND']];
            }

            if($typeID !== null){
                $filter[] = ["ActionTypeID = '$typeID'", ''];
            }

            $filter[sizeof($filter) - 1][1] = '';
            $re = $this->query(['filter' => $filter]);
            if($re == false)
                return $re;

            foreach ($re as $action){;
                $action->cf = json_decode($action->Config);
                if($action->cf === null){
                    logAM("Parse step action config failed");
                    return false;
                }

                unset($action->Config);
            }

            return $re;
        }

//        protected function getColumnsForStandardizeData() {
//            return [
//                "ID",
//                "Name",
//                "cf",
//                "IsRollback",
//                "EventID",
//                "RequestStepDefinitionID"
//            ];
//        }
    }
}
?>