<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMRequestStepPermissionFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestStepPermissionFactory extends AbstractDatabase {

        public static $tableName = 'am_request_step_permission';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestStepPermissionFactory::$tableName,
                'columns' => [
                    "ID",
                    "StepDefinitionID",
                    "FieldDefinitionID",
                    "Permission"
                ],
            );
        }
    }
}
?>