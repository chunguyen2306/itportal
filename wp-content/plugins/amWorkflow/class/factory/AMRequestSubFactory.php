<?php

if(!class_exists('AMRequestSubFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestSubFactory extends AbstractDatabase {

        public static $tableName = 'am_request_sub_definition';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestSubFactory::$tableName,
                'columns' => [
                    "ID",
                    "StepDefinitionID",
                    "RequestDefinitionID",
                    "Condition",
                ]
            );


        }

        public function insert(&$data, $isLog = false) {

            $insertData = $data;
            $condition = $insertData["condition"];
            $condition = (object)$condition;
            $condition->isAndOperator = intval($condition->isAndOperator);

            if($condition->list){
                $listExp = [];
                foreach ($condition->list as $item)
                    $listExp[] = (object)$item;
                $condition->list = $listExp;
            }else{
                $condition->list = [];
            }

            $mapping = $condition->mapping = (object)$condition->mapping;
            $mapping->step = (object)$mapping->step;
            $field = $mapping->field = (object)$mapping->field;
            foreach ($field as $key => $val){
                if(is_array($val))
                    $field->{$key} = (object)$val;
            }

            $insertData["Condition"] = am_json_encode($condition);
            unset($insertData["condition"]);
            global $wpdb;
            $re = parent::insert($insertData);
            $data["ID"] = $wpdb->insert_id;
            return $re;
        }

        public function query($agr = array(), $log = false) {
            $re = parent::query($agr, $log);
            if(is_array($re)){
                foreach ($re as $item){
                    $condition = am_json_decode($item->Condition);
                    if($condition == null){
                        $condition = new stdClass();
                        $condition->isAndOperator = 0;
                        $condition->list = [];
                        $condition->mapping = new stdClass();
                    }

                    $item->condition = $condition;

                    unset($item->Condition);
                }
            }

            return $re;
        }

        public function getByRequestID($requestID){
            import('plugin.amWorkflow.class.factory.AMRequestStepFactory');
            import('plugin.amWorkflow.class.factory.AMRequestStepDefinitionFactory');
            import('plugin.amWorkflow.class.factory.AMRequestFactory');
            $tn_s = AMRequestStepFactory::$tableName;
            $tn_sd = AMRequestStepDefinitionFactory::$tableName;
            $tn_rsd = AMRequestSubFactory::$tableName;

            return $this->custom_query2("
            SELECT rsd.StepDefinitionID, rsd.RequestDefinitionID FROM $tn_s as s
              INNER JOIN $tn_sd as sd on s.DefinitionID = sd.ID
              INNER JOIN $tn_rsd as rsd on sd.ID = rsd.StepDefinitionID
            WHERE s.RequestID = @requestID
            ", ['@requestID' => $requestID]);

        }
    }
}
?>