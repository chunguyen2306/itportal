<?php
/**
 * Created by PhpStorm.
 * User: CPU10722-local
 * Date: 6/9/2017
 * Time: 11:36 AM
 */
if(!class_exists('AMRequestUser')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestUser extends AbstractDatabase {

        public static $tableName = 'am_request_user';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestUser::$tableName,
                'columns' => [
                    'Domain',
                    'RequestID',
                    'FullName',
                    'JobTitle',
                    'Department',
                    'Seat',
                    'Site'
                ]
            );
        }


    }
}
?>