<?php
/**
 * Created by PhpStorm.
 * User: CPU10722-local
 * Date: 6/9/2017
 * Time: 11:36 AM
 */
if(!class_exists('AMRequestOwner')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestOwner extends AbstractDatabase {

        public static $tableName = 'am_request_owner';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestOwner::$tableName
            );
        }
    }
}
?>