<?php
/**
 * Created by PhpStorm.
 * User: CPU10722-local
 * Date: 6/12/2017
 * Time: 5:01 PM
 */
if(!class_exists('AMRequestTypesFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestTypesFactory extends AbstractDatabase {

        public static $tableName = 'am_request_types';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestTypesFactory::$tableName
            );
        }
    }
}
?>