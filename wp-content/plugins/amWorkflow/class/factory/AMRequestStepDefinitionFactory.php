<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMRequestStepDefinitionFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestStepDefinitionFactory extends AbstractDatabase {

        public static $tableName = 'am_request_step_definitions';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestStepDefinitionFactory::$tableName,
                'columns' => [
                    "ID",
                    "StepIndex",
                    "Name",
                    "DisplayName",
                    "RequestDefinitionID",
                    "Role",
                    "MinIntendDuration",
                    "MaxIntendDuration",
                    "BackStep",
                    "CanRequestInfo",
                    "CanCancel",
                    "CanRaise",
                    "RequestStatusID",
                ],
            );
        }

        public function getListByRequestDefinitionID($requestDefinitionID){
            $re = $this->query(
                array('filter' => array(array("RequestDefinitionID = '$requestDefinitionID'", '')))
            );

            if($re){
                foreach ($re as $step)
                    $step->Role = am_json_decode($step->Role);
            }

            return $re;
        }

        public function insert($data) {
            $insertData = $data;
            $insertData["Role"] = am_json_encode($data["Role"]);
            if(parent::insert($insertData) !== false)
                return $data;
            else
                return false;
        }

    }
}
?>