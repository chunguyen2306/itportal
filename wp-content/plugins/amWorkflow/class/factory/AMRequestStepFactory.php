<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMRequestStepFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestStepFactory extends AbstractDatabase {

        public static $tableName = 'am_request_steps';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestStepFactory::$tableName,
                "columns" => [
                    "ID",
                    "DefinitionID",
                    "ActionDate",
                    "StartDate",
                    "RequestID",
                    "Owner",
                    "IsApproved"
                ]
            );
        }

        public function getListByRequestID($requestID){
            return $this->query(
                array('filter' => ["RequestID = '$requestID'", ''])
            );
        }

        public function getRequest($stepID){
            $re = $this->custom_query2("
              SELECT * FROM am_requests 
                WHERE ID = (SELECT RequestID FROM am_request_steps WHERE ID = @stepID)",
                ['@stepID'=>$stepID]
            );

            if($re === false)
                return $re;

            return sizeof($re) == 1 ? $re[0] : null;
        }

        public function getDefinition($stepID){
            $re = $this->custom_query2("
              SELECT * FROM am_request_step_definitions 
                WHERE ID = (SELECT DefinitionID FROM am_request_steps WHERE ID = @stepID)",
                ['@stepID'=>$stepID]
                );

            if($re === false)
                return $re;

            return sizeof($re) == 1 ? $re[0] : null;
        }
    }
}
?>