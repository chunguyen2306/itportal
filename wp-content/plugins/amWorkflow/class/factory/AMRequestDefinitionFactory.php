<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if (!class_exists('AMRequestDefinitionFactory')) {
    import('theme.package.Abstracts.AbstractDatabase');

    class AMRequestDefinitionFactory extends AbstractDatabase {

        public static $tableName = 'am_request_definition';

        protected function tableInfo() {
            return array('table_name' => AMRequestDefinitionFactory::$tableName);
        }

        public function query($agr = [], $log = false, $output = OBJECT) {
            if ($agr['filter']) {
                array_unshift($agr['filter'], ['IsDelete = 0', 'AND']);
            }
            else {
                $agr['filter'] = [['IsDelete = 0']];
            }

            return parent::query($agr, $log, $output);
        }
    }
}
?>