<?php
/**
 * Created by PhpStorm.
 * User: CPU10722-local
 * Date: 6/12/2017
 * Time: 5:19 PM
 */
if(!class_exists('AMRequestStatusFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestStatusFactory extends AbstractDatabase {

        public static $tableName = 'am_request_status';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestStatusFactory::$tableName
            );
        }
    }
}
?>