<?php
/**
 * Created by PhpStorm.
 * User: CPU10722-local
 * Date: 6/9/2017
 * Time: 11:36 AM
 */
if(!class_exists('AMRequest')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequest extends AbstractDatabase {

        public static $tableName = 'am_requests';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequest::$tableName
            );
        }
    }
}
?>