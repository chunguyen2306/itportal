<?php
/**
 * Created by PhpStorm.
 * User: CPU10722-local
 * Date: 6/12/2017
 * Time: 5:01 PM
 */
if(!class_exists('AMRequestTableDetailData')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestTableDetailData extends AbstractDatabase {

        public static $tableName = 'am_request_table_detail_data';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestTableDetailData::$tableName
            );
        }
    }
}
?>