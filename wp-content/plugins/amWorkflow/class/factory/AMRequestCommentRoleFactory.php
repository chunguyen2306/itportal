<?php
/**
 * Created by PhpStorm.
 * User: CPU10722-local
 * Date: 6/9/2017
 * Time: 11:36 AM
 */
if(!class_exists('AMRequestCommentRoleFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestCommentRoleFactory extends AbstractDatabase {

        public static $tableName = 'am_request_comment_role';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestCommentRoleFactory::$tableName
            );
        }

        public function canComment($stepID, $domain){
            global $wpdb;

            $query = "SELECT ID FROM am_request_comment_role
              WHERE StepID = @stepID AND Domain = '@domain' AND Done = 0";
            return $this->check($query, ['@stepID' => $stepID, '@domain' => $domain]);
        }

        public function getCommentRole($stepID, $domain){
            $query = "SELECT * FROM am_request_comment_role
              WHERE StepID = $stepID AND Domain = '$domain' AND Done = 0";

            $re = $this->query($query);
            if($re === false)
                return $re;

            return sizeof($re) == 1 ? $re[0] : null;
        }

        public function insert(&$data, $isLog = false) {
            if($this->canComment($data["StepID"], $data["Domain"]))
                return true;

            return parent::insert($data, $isLog);
        }

        public function removeRole($stepID){
            return $this->custom_query2(
                "UPDATE am_request_comment_role 
                    SET Done = 1
                    WHERE StepID = @stepID",
                ['@stepID' => $stepID]
            );
        }

        function check($query, $args){
            $re = $this->custom_query2($query, $args);
            if($re === false){
                logAM("check failed with query: $re", 1);
                return false;
            }

            return sizeof($re) > 0;
        }
    }
}
?>