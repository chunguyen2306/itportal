<?php
/**
 * Created by PhpStorm.
 * User: CPU10722-local
 * Date: 6/9/2017
 * Time: 11:36 AM
 */
if (!class_exists('AMRequestFactory')) {
    import('theme.package.Abstracts.AbstractDatabase');


    class AMRequestFactory extends AbstractDatabase {

        public static $tableName = 'am_requests';

        protected function tableInfo() {
            return array('table_name' => AMRequestFactory::$tableName,
                "table_columns" => [
                    "ID",
                    "DefinitionID",
                    "Creator",
                    "CreateDate",
                    "CurrentIndex",
                    "RequestStatusID",
                    "ParrentID"],
                "columns" => [
                    "ID",
                    "DefinitionID",
                    "Creator",
                    "CreateDate",
                    "CurrentIndex",
                    "RequestStatusID",
                    "ParrentID"]);
        }

        /**
         * @param $domain
         * @param int $statusID mặc định bằng -1 (cần user duyệt)
         * @return array|bool|null|object
         */
        //        public function GetListRequestByUSer($domain,$statusID = -1) {
        //
        //            //            error_log("calllll");
        //            if ($statusID == -1) {
        //                $where = " AND r.CurrentIndex = s.StepIndex";
        //            }
        //            else if ($statusID == null) {
        //                $where = "";
        //            }
        //            else {
        //                $where = " AND r.RequestStatusID = $statusID ";
        //            }
        //
        //            $query = "
        //            SELECT r.requestID as ID, s.StartDate as LastUpdate, s.Owner as CurrentUser, sd.StepIndex as CurrentStepIndex,
        //		      sd.DisplayName as CurrentStepName, rd.DisplayName as RequestName, r.workflowID, r.RequestStatusID
        //		    FROM am_request_steps as s
        //            INNER JOIN am_request_step_definitions as sd ON s.DefinitionID = sd.ID
        //            RIGHT JOIN (SELECT r.ID as requestID, r.CurrentIndex, r.DefinitionID as workflowID, r.RequestStatusID
        //                    FROM am_requests AS r
        //                    INNER JOIN am_request_steps AS s ON s.RequestID = r.ID
        //                    WHERE s.Owner = '$domain' $where
        //                    GROUP BY r.ID, r.CurrentIndex, r.DefinitionID, r.RequestStatusID)
        //                AS r ON (s.RequestID = r.requestID AND sd.StepIndex = r.CurrentIndex)
        //	        INNER JOIN am_request_definition as rd ON r.workflowID = rd.ID
        //	        ORDER BY r.requestID DESC
        //                ";
        //
        //            $listRequest = $this->custom_query2($query);
        //            if ($listRequest === false)
        //                return false;
        //
        //            foreach ($listRequest as $request) {
        //                $count = $this->custom_query2("SELECT COUNT(ID) as count FROM am_request_step_definitions
        //                  WHERE RequestDefinitionID = $request->workflowID");
        //
        //                if ($count === false) {
        //                    AMRespone::busFailed($request, "Count step failed");
        //                    return false;
        //                }
        //
        //                $request->StepNumber = $count[0]->count;
        //            }
        //
        //            return $listRequest;
        //        }

        public function getUserRequests($domain, $additionWhere = null) {

            // get request definition owner mapping name
            $query = "
              SELECT rd.ID, fd.MappingName FROM am_request_field_definition as fd
                INNER JOIN am_request_definition as rd ON rd.ID = fd.RequestDefinitionID
                WHERE rd.OwnerField = fd.Name";
            $listMappingName = $this->custom_query2($query);
            if ($listMappingName == false)
                return $listMappingName;
            global $wpdb;
            $domain = $wpdb->_escape($domain);
            return $this->getRequestsByValue($listMappingName, '\''.$domain.'\'', $additionWhere);
        }

        function getRequestsByValue($listMappingName, $value, $additionWhere = null) {
            if (sizeof($listMappingName) === 0)
                return [];

            $qListAnd = [];
            foreach ($listMappingName AS $item)
                $qListAnd[] = "(r.DefinitionID = $item->ID AND f.$item->MappingName = $value)";
            $qWhere = join(" OR ", $qListAnd);
            if ($additionWhere) {
                $aw = [];
                foreach ($additionWhere as $item)
                    $aw[] = "$item[0] $item[1]";

                $additionWhereInString = join(" ", $aw);
                $qWhere                = "$additionWhereInString AND ($qWhere)";
            }

            $colums  = $this->tableInfo()["table_columns"];
            $qColumn = "r." . join(", r.", $colums);

            // get request by value
            $query = "SELECT $qColumn FROM am_requests as r
	            INNER JOIN am_request_fields as f on r.ID = f.RequestID
                WHERE $qWhere
                ORDER BY r.ID DESC";

            $listRequest = $this->custom_query2($query);
            return $listRequest;
         }

        public function updateDescendantStatus($parrentID, $statusID) {
            $listDescendant = $this->getDescendantRequestAsList($parrentID);
            $listID         = [];
            foreach ($listDescendant as $item)
                $listID[] = $item->ID;

            $in = join(", ", $listID);

            $updateData = ["RequestStatusID = $statusID"];
            return $this->update($updateData, ["ID IN ($in)"]);
        }

        public function getDescendantRequestAsList($parrentID) {
            if (function_exists("getRequestFamily") == false) {
                function getRequestFamily($listParrent, &$family, $fac) {
                    if (sizeof($listParrent) == 0)
                        return true;

                    $listParrentID = [];
                    foreach ($listParrent as $parrent) {
                        $listParrentID[] = $parrent->ID;
                        $family[]      = $parrent;
                    }
                    $inListParrent = join(", ", $listParrentID);
                    $listSub       = $fac->query(["filter" => [["ParrentID IN ($inListParrent)"]]]);

                    if ($listSub)
                        return array_merge($listSub, getRequestFamily($family));
                    else
                        return true;
                }
            }

            $listSub = $this->query(["filter" => [["ParrentID = $parrentID"]]]);
            $listAllSub = [];
            getRequestFamily($listSub, $listAllSub, $this);

            return $listAllSub;
        }

        public function setRequestsDescription($requests) {
            if(sizeof($requests) == 0)
                return true;

            import('plugin.amWorkflow.class.includes.QueryUtil');
            import('plugin.amWorkflow.class.includes.RequestStatusID');
            // number step
            $inDefinition = QueryUtil::in($requests, "DefinitionID", true);
            $query = "SELECT RequestDefinitionID, COUNT(ID) as NumberStep FROM am_request_step_definitions
                          WHERE RequestDefinitionID IN $inDefinition
                          GROUP BY RequestDefinitionID";
            $tempList = $this->custom_query2($query);
            if($tempList == false){
                logAM("Query request definition NumberStep failed");
                return $tempList;
            }
            $mapNumberStep = [];
            foreach ($tempList as $item)
                $mapNumberStep[$item->RequestDefinitionID] = $item->NumberStep;

            // step description
            $listWhere = [];
            foreach ($requests as $request){
                $stepIndex = $request->CurrentIndex;
                if($stepIndex < 0)
                    $stepIndex = 0;
                else if($request->RequestStatusID == RequestStatusID::$done)
                    $stepIndex-=1;

                $listWhere[] = "(s.RequestID = $request->ID AND sd.StepIndex = $stepIndex)";
            }
            $where = join(" OR ", $listWhere);
            $steps = $this->custom_query2(
                "SELECT s.RequestID, s.Owner as owner, IF(s.ActionDate > s.StartDate, s.ActionDate, s.StartDate) as lastUpdate 
                  FROM am_request_steps AS s
	              INNER JOIN am_request_step_definitions AS sd ON s.DefinitionID = sd.ID
	              WHERE $where"
            );
            if($steps == false){
                logAM("Query request step description failed");
                return $tempList;
            }

            // request description string
            $listTemp = $this->custom_query2(
                "SELECT ID, DisplayName 
                  FROM am_request_definition 
                  WHERE ID IN $inDefinition"
            );
            $mapDesString = [];
            foreach ($listTemp as $item)
                $mapDesString[$item->ID] = $item->DisplayName;

            foreach ($requests as $request){
                $des = null;
                foreach ($steps as $step){
                    if($step->RequestID == $request->ID){
                        $des = $step;
                        break;
                    }
                }
                $des || ($des = new stdClass());
                $des->totalStep = $mapNumberStep[$request->DefinitionID];
                $des->string = $mapDesString[$request->DefinitionID];
                if($request->RequestStatusID == RequestStatusID::$cancel)
                    $des->status = "Đã hủy bởi $des->owner";
                else if($request->RequestStatusID == RequestStatusID::$done)
                    $des->status = "Đã hoàn tất bởi $des->owner";
                else
                    $des->status = "Đang đợi $des->owner";

                unset($des->owner);
                $request->des = $des;
            }

            return true;
        }

        public function getRequestsForApproval($domain){
            import('plugin.amWorkflow.class.includes.RequestStatusID');
            $openID = RequestStatusID::$open;
            $colmn = "r." . join(", r.", $this->tableInfo()["table_columns"]);
            return $this->custom_query2(
                "SELECT $colmn FROM am_requests as r
                        INNER JOIN am_request_steps as s ON s.RequestID = r.ID
                        INNER JOIN am_request_step_definitions as sd ON s.DefinitionID = sd.ID
                      WHERE r.RequestStatusID =  $openID AND r.CurrentIndex = sd.StepIndex AND s.Owner = '@domain'",
                ['@domain' => $domain]
            );
        }

        public function getUserRequestsByStatus($domain, $statusID = null, $selectChildIfParrentNotDone = true) {
            global $wpdb;
            $statusID = $statusID ? $wpdb->_escape($statusID) : $statusID;


            $additionWhere = null;
            if($statusID){
                $additionWhere = "r.RequestStatusID = $statusID";
                if($selectChildIfParrentNotDone)
                    $additionWhere =  "($additionWhere OR r.ParrentID IS NOT NULL)";
                $additionWhere = [[$additionWhere]];
            }
            $re = $this->getUserRequests($domain, $additionWhere);

            if($selectChildIfParrentNotDone){
                $temp = $re;
                $re = [];
                foreach ($temp as $request){
                    if($request->ParrentID == null){
                        $re[] = $request;
                    }else{
                        foreach ($temp as $item){
                            if($request->ParrentID == $item->ID){
                                $re[] = $request;
                                break;
                            }
                        }
                    }
                }
            }

            return $re;
        }

        public function getUserRequestsByStatusIDs($domain, $statusIDs) {
            if(!$statusIDs)
                return [];
            global $wpdb;

            $isAll = false;
            $ids = [];
            foreach($statusIDs as $item){
                if($item == -1){
                    $isAll = true;
                    break;
                }

                $ids[] = $wpdb->_escape($item);
            }

            $where = '';
            if($isAll == false)
                $where = ' WHERE ID = ' . join(' OR ID = ', $ids);

            $re = $this->getUserRequests($domain, $where);

            return $re;
        }

        public function getCurrentStepUser($requestID){
            $query = "SELECT `Owner` FROM (SELECT DefinitionID, `Owner` FROM am_request_steps WHERE RequestID = @requestID) as s
                INNER JOIN am_request_step_definitions sd ON s.DefinitionID = sd.ID
                WHERE sd.StepIndex = (SELECT CurrentIndex FROM am_requests WHERE ID = @requestID)";
            $result = $this->custom_query2($query, ['@requestID' => $requestID]);
            if($result === false)
                return $result;

            if(sizeof($result) != 1)
                return null;

            return $result[0]->Owner;
        }

        public function getCurrentStepID($requestID){
            $query = "SELECT s.ID FROM (SELECT ID, DefinitionID, `Owner` FROM am_request_steps WHERE RequestID = @requestID) as s
                INNER JOIN am_request_step_definitions sd ON s.DefinitionID = sd.ID
                WHERE sd.StepIndex = (SELECT CurrentIndex FROM am_requests WHERE ID = @requestID)";
            $result = $this->custom_query2($query, ['@requestID' => $requestID]);
            if($result === false)
                return $result;

            if(sizeof($result) != 1)
                return null;

            return $result[0]->ID;
        }

        public function getNumberStep($definitionID){
            $query = "SELECT COUNT(ID) AS NumberStep FROM am_request_step_definitions
		      WHERE RequestDefinitionID = @definitionID";

            $re = $this->custom_query2($query, ['@definitionID' => $definitionID]);
            if($re == false)
                return false;

            return $re[0]->NumberStep;
        }

        public function isHasSubOpening($parrentID, $expectSubID = null){
            import('plugin.amWorkflow.class.includes.RequestStatusID');
            $expect = $expectSubID ? "AND ID != $expectSubID" : "";
            $openID = RequestStatusID::$open;
            $query = "SELECT ID FROM am_requests as r
                WHERE r.ParrentID = @parrentID AND RequestStatusID = $openID $expect
                LIMIT 1";

            return $this->check($query, ['@parrentID' => $parrentID]);
        }

        public function isDoneAllStep($id){
            $query = "SELECT ID FROM am_request_steps as s
		      WHERE s.RequestID = @requestID AND s.IsApproved IS NULL";

            return $this->check($query, ['@requestID' => $id]);
        }

        public function isOpening($id){
            $openStatusID = RequestStatusID::$open;
            $query = "SELECT ID FROM am_requests where ID = @id AND RequestStatusID = $openStatusID";

            return $this->check($query, ['@id'=>$id]);
        }

        public function getChildRequestWithWorkflowName($parentID){
            global $wpdb;

            $tnRequest = AMRequestFactory::$tableName;
            $tnRequestDefinition = AMRequestDefinitionFactory::$tableName;
            $query = $wpdb->prepare("SELECT r.ID, DisplayName as workflowDisplayName, DefinitionID, r.Creator, CurrentIndex, RequestStatusID, ParrentID
                FROM $tnRequest as r INNER JOIN $tnRequestDefinition as rd on r.DefinitionID = rd.ID 
                WHERE r.ParrentID = %d",
                $parentID
            );

            return $this->custom_query2($query);
        }

        function check($query, $arg){
            $re = $this->custom_query2($query, $arg);
            if($re === false){
                logAM("check failed with query: $re", 1);
                return false;
            }

            return sizeof($re) > 0;
        }

        function buildSuggestDomainsByRole($roleID, $limit, $site, $priorityDomain1 = null, $priorityDomain2 = null){
            global $wpdb;
            $roleID = $wpdb->esc_like($wpdb->_escape($roleID));
            $limit = $wpdb->_escape($limit);
            $priorityDomain1 = $priorityDomain1 ? $wpdb->_escape($priorityDomain1) : $priorityDomain1;
            $priorityDomain2 = $priorityDomain2 ? $wpdb->_escape($priorityDomain2) : $priorityDomain2;
            $site = $site ? $wpdb->_escape($site) : $site;


            $allowStatusID = RequestStatusID::$open;

            // chuyển đổi couting trên step thay vì request
            $query = "
select 
  rp.Domain,
  (u.location != '$site') as prio0,
  (rp.Domain != '$priorityDomain1') as prio1,
  (rp.Domain != '$priorityDomain2') as prio2,
  count(rp.Domain) as count
from  am_user_role_mapping as rp
  left join am_users u on rp.Domain = u.domainAccount
  left join am_request_steps s on s.Owner = rp.Domain
where rp.Permission like '%\"$roleID\"%' and s.IsApproved is null
group by rp.Domain, prio0, prio1, prio2
order by prio0, prio1, prio2, count
";
            if($limit)
                $query = $query . "limit $limit";

            return $query;
        }

        function getSuggestDomainByRole($roleID, $site, $priorityDomain1 = null, $priorityDomain2 = null){

            $query = $this->buildSuggestDomainsByRole($roleID, 1, $site, $priorityDomain1, $priorityDomain2);
            $result = $this->custom_query2($query);
            if($result === false){
                AMRespone::dbFailed($query);
                return false;
            }

            return $result ? $result[0]->Domain : null;
        }

        function getListSuggestDomainsByRole($roleID, $site, $priorityDomain1 = null, $priorityDomain2 = null){
            $query = $this->buildSuggestDomainsByRole($roleID, null, $site, $priorityDomain1, $priorityDomain2);
            $result = $this->custom_query2($query);
            if($result === false){
                AMRespone::dbFailed($query);
                return false;
            }

            $re = [];
            foreach ($result as $item){
                $re[] = $item->Domain;
            }

            return $re;
        }
    }

}
