<?php
if(!class_exists('AMRequestApprovalCode')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestApprovalCode extends AbstractDatabase {

        public static $tableName = 'am_request_approval_code';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestApprovalCode::$tableName
            );
        }

        public function getCode($requestID){
            $re = $this->custom_query2(
                "SELECT * FROM am_request_approval_code WHERE RequestID = @requestID",
                ['@requestID' => $requestID]
            );

            if($re === false){
                logAM('getApprovalCode failed');
                return $re;
            }

            // if exist, return
            if(sizeof($re))
                return $re[0]->Code;

            return null;

        }

    }
}
?>