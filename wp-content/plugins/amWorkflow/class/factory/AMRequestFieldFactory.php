<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/22/2017
 * Time: 2:10 PM
 */
if(!class_exists('AMRequestFieldFactory')){
    import ('theme.package.Abstracts.AbstractDatabase');

    class AMRequestFieldFactory extends AbstractDatabase {

        public static $tableName = 'am_request_fields';

        protected function tableInfo(){
            return array(
                'table_name' => AMRequestFieldFactory::$tableName,
                "columns" => array(
                    "ID",
                    "RequestID",
                    "Field0",
                    "Field1",
                    "Field2",
                    "Field3",
                    "Field4",
                    "Field5",
                    "Field6",
                    "Field7",
                    "Field8",
                    "Field9",
                    "Field10",
                    "Field11",
                    "Field12",
                    "Field13",
                    "Field14",
                ),
                "data_columns" =>[
                    "Field0",
                    "Field1",
                    "Field2",
                    "Field3",
                    "Field4",
                    "Field5",
                    "Field6",
                    "Field7",
                    "Field8",
                    "Field9",
                    "Field10",
                    "Field11",
                    "Field12",
                    "Field13",
                    "Field14"
                ]
            );
        }

        public function getFieldByRequestID($requestID){
            $list = $this->query(["filter" => [["RequestID = $requestID"]]]);
            if(is_array($list) == false)
                return false;

            $len = sizeof($list);
            if($len == 0){
                logAM("Missing request field");
                return false;
            }

            if($len > 1){
                logAM("Dữ liệu bất thường: tìm thấy nhiều field data cho 1 request");
                return false;
            }

            $fieldData = $list[0];
            return $fieldData;
        }


    }
}
?>