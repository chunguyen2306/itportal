<?php

/*
  Plugin Name: Workflow
  Plugin URI:
  Description: Quản lý qui trình
  Version: 1.0
  Author: TienTM2 - TienNV6
  Author URI:
  License:
  Text Domain: am-workflow
*/
if (!class_exists('AMWorkflow')) {
    if (!function_exists('import')) {
        require_once get_template_directory() . '/package/import.php';
    }
    import('theme.package.Abstracts.AbstractPlugin');
    import('plugin.amWorkflow.class.controller.AMListWorkflow');
    import('plugin.amWorkflow.class.controller.AMWorkflowEdit');
    import('plugin.amWorkflow.class.controller.AMRequest');
    import('plugin.amWorkflow.class.controller.AMRequestTypeController');
    import('plugin.amWorkflow.class.controller.AMRequestCategoryController');
    import('plugin.amWorkflow.class.controller.AMRequestReasonController');
    import('plugin.amWorkflow.class.controller.AMRequestStatusController');
    import('plugin.amRolePermission.class.factory.AMUserRoleFactory');

    class AMWorkflow extends AbstractPlugin
    {

        public function __construct() {
            $this->ajaxName   = 'amWorkflow';
            $this->pluginName = 'amWorkflow';
            $mainView = new AMListWorkflow(array('slug' => 'am_list_workflow'));
            $this->adminMenu  = array(
                array(
                    'pageTitle' => 'Quy trình',
                    'menuTitle' => 'Quy trình',
                    'capability' => 'manage_options',
                    'menuSlug' => 'am_list_workflow',
                    'handler' => array($mainView, 'index'),
                    'icon' => 'dashicons-list-view',
                    'child' => array(
                        array(
                            'pageTitle' => 'Danh sách quy trình',
                            'menuTitle' => 'Danh sách quy trình',
                            'capability' => 'manage_options',
                            'menuSlug' => 'am_list_workflow',
                            'handler' => array($mainView, 'index')
                        ),
                        array(
                            'pageTitle' => 'Biên tập quy trình',
                            'menuTitle' => 'Biên tập quy trình',
                            'capability' => 'manage_options',
                            'menuSlug' => 'am_workflow',
                            'handler' => array(new AMWorkflowEdit(array('slug' => 'am_workflow')), 'index')
                        ),
                        array(
                            'pageTitle' => 'Làm yêu cầu',
                            'menuTitle' => 'Làm yêu cầu',
                            'capability' => 'manage_options',
                            'menuSlug' => 'am_request',
                            'handler' => array(new AMRequest(array('slug' => 'am_request')), 'index')
                        ),
                        array(
                            'pageTitle' => 'Nhóm quy trình',
                            'menuTitle' => 'Nhóm quy trình',
                            'capability' => 'manage_options',
                            'menuSlug' => 'am_request_category',
                            'handler' => array(new AMRequestCategoryController(array('slug' => 'am_request_category')), 'index')
                        ),
                        array(
                            'pageTitle' => 'Loại quy trình',
                            'menuTitle' => 'Loại quy trình',
                            'capability' => 'manage_options',
                            'menuSlug' => 'am_request_type',
                            'handler' => array(new AMRequestTypeController(array('slug' => 'am_request_type')), 'index')
                        ),
                        array(
                            'pageTitle' => 'Lý do quy trình',
                            'menuTitle' => 'Lý do quy trình',
                            'capability' => 'manage_options',
                            'menuSlug' => 'am_request_reason',
                            'handler' => array(new AMRequestReasonController(array('slug' => 'am_request_reason')), 'index')
                        ),
                        array(
                            'pageTitle' => 'Trạng thái quy trình',
                            'menuTitle' => 'Trạng thái quy trình',
                            'capability' => 'manage_options',
                            'menuSlug' => 'am_request_status',
                            'handler' => array(new AMRequestStatusController(array('slug' => 'am_request_status')), 'index')
                        ),
                    )
                ), // manager
            );
            parent::__construct();
        }

        function add_admin_menu() {
            require_once $this->dir . 'class/controller/class_asset_detail.php';
            $img = plugin_dir_url(__FILE__) . "/images/icon.png";
            $gui = new AMAssetDetail();

            add_menu_page('Tài sản', 'Tài sản', 'manage_options', "asset", array($gui, 'index'), $img, 20);
            add_submenu_page('asset', 'Cấu hình tài sản', 'Cấu hình', 'manage_options', 'asset', array($gui, 'index'));
            add_submenu_page('asset', 'Lịch sử thay đổi cấu hình', 'Lịch sử cấu hình', 'manage_options', 'asset-history', array($gui, 'detailHistory'));
            add_submenu_page('asset', 'Trạng thái tài sản', 'Trạng thái', 'manage_options', 'asset-state', array($gui, 'assetState'));
        }

        protected function init_custom_hook() {
            // TODO: Implement init_custom_hook() method.
        }

        protected function custom_init() {
 
            importScript('lHttpRequest', 'theme.js.Libs.HttpRequest');
            importScript('lAjax', 'theme.js.Data.ajax');
            importScript('lHttpRequest', 'theme.js.Libs.ko-custom');
            importScript('lHttpRequest', 'theme.js.Libs.Common');
            importScript('lPopupBusiness', 'theme.js.PopupBusiness');
            importScript('aAPIData.Workflow', 'plugin.amWorkflow.js.api.APIWORKFLOW');
            importScript('aAPIData.Request', 'plugin.amWorkflow.js.api.REQUEST');
        }
    }

}
$amWorkflow            = new AMWorkflow();
$GLOBALS['amWorkflow'] = $amWorkflow;


function getCurrentUser(){
    try{
        //Get current user
        $currentUser = wp_get_current_user();
        if($currentUser == 0 || $currentUser->user_login == false)
            return;

        $username = $currentUser->user_login;
        $result = (new AMUserRoleFactory())->query([
            "filter" => [["Domain = '$username'", ""]]
        ]);
        if($result == false)
            return;

        if(is_array($result) != 1)
            return;

        $user = $result[0];
        $domain = $user->Domain;
        $roleID = $user->RoleID;
        $name = $user->Name;
        echo "<script>APIDataUser = {domain: '$domain', name: '$name', roleID: $roleID}</script>";
    }catch (Exception $ex){
    }

    //    try{
    //        //Get current user
    //        $currentUser = wp_get_current_user();
    //        if($currentUser == 0 || $currentUser->user_login == false)
    //            return AMRespone::notFound();
    //
    //        $username = $currentUser->user_login;
    //        $result = (new AMUserRoleFactory())->query([
    //            "filter" => [["Domain = '$username'", ""]]
    //        ]);
    //        if($result == false)
    //            return AMRespone::dbFailed($username, "Get user role info");
    //
    //        if(is_array($result) != 1)
    //            return AMRespone::busFailed($username, "Data is weirdo");
    //
    //        return new AMRespone($result[0]);
    //    }catch (Exception $ex){
    //        return AMRespone::exception(null, $ex);
    //    }
}
add_action('wp_head','getCurrentUser');
add_action('admin_head','getCurrentUser');
