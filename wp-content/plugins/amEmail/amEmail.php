<?php
/**
 * Plugin Name:       Email
 * Plugin URI:
 * Description:
 * Author:            tientm2
 * Version:           1.0
 * License:           GPL-2
 * Text Domain:       am-email
 */

if(!class_exists('AMEmail')) {

    if (!function_exists('import')) {
        require_once get_template_directory() . '/package/import.php';
    }

    class AMEmail extends AbstractPlugin
    {
        public function __construct()
        {
            $this->ajaxName = 'amWorkflowEngine';
            $this->pluginName = 'amWorkflowEngine';
            parent::__construct();
        }

        protected function init_custom_hook()
        {
            // TODO: Implement init_custom_hook() method.
        }

        protected function custom_init()
        {
            // TODO: Implement custom_init() method.
        }
    }
}
$GLOBALS['amEmail'] = new AMEmail();