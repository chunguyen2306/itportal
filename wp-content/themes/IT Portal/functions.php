<?php
    if ( ! function_exists( 'import' ) ) {
        require_once get_template_directory() . '/package/import.php';
    }

    import ('theme.package.Common');
    import ('plugin.itportal.functions');

    if(!class_exists('ITAM')){

        class ITAM
        {
            function __construct()
            {
                $this->init_Hoook();
                am_ripJs();
            }

            function init_AdminHook()
            {
                //add_action('admin_head', array($this, 'am_custom_admin_css'));
                add_action('admin_menu', array($this, 'am_add_admin_menu_separator'));
            }

            function init_Hoook()
            {
                add_action('init', array($this, 'do_init'));
                add_action('am_header', array($this, 'do_AddHeaderStyle'));
                add_action('am_footer', array($this, 'do_AddFooterStyle'));
                add_action('am_content', array($this, 'do_ProcessContent'));
                add_action('AM_User_On_Login', array($this, 'update_user_login'));
                add_filter('wp_mail_content_type', [$this,'set_html_mail_content']);

                $this->init_Widget();
                $this->init_AdminHook();
            }

            public function set_html_mail_content(){
                return 'text/html';
            }

            function init_Widget()
            {
                add_action('widgets_init', array($this, 'mytraining_widgets_init'));
                add_action('widgets_init', array($this, 'am_login_widget_init'));
            }

            public function update_user_login($userdata){
                global $wpdb;
                import('plugin.itportal.Factory.AMUserFactory');
                $userFac = new AMUserFactory();
                $user = $userFac->getOne(array(
                    'filter'=>array(
                        array("domainAccount = '".$userdata["mailNickname"]."'", "")
                    )
                ));

                $data = array(
                    "departmentCode" => $userdata["departmentCode"],
                    "departmentName" => $userdata["department"],
                    "divisionCode" => $userdata["divisionCode"],
                    "divisionName" => $userdata["division"],
                    "domainAccount" => $userdata["mailNickname"],
                    "empCode" => $userdata["employeeID"],
                    "firstName" => $userdata["givenName"],
                    "fullName" => $userdata["displayName"],
                    "jobTitle" => $userdata["jobTitle"],
                    "lastName" => $userdata["surname"],
                    "location" => $userdata["officeLocation"],
                    "reportingLine" => $userdata["manager"]["mail"],
                    "seat" => $userdata["streetAddress"],
                    "teamName" => $userdata["team"],
                    "workingEmail" => $userdata["mail"]
                );
                if($user != null){
                    $result = $userFac->update($data, array(
                        'domainAccount' => $userdata["mailNickname"]
                    ));
                } else {
                    $result = $userFac->insert($data);
                }

                if($result != 1){

                    $wpdb->print_error();
                    error_log("[Update Login User] Insert fail for ".json_encode($user));
                }
            }

            function do_init()
            {
                if (date_default_timezone_get() == 'UTC') {
                    date_default_timezone_set('Asia/Bangkok');
                }
                ob_start();
                $routers = array(
                    array('^communication\/(.*)?', '/communication/$matches[1]'),
                    array('^sms\/(.*)?', '/sms/$matches[1]'),
                    array('^am_ajax$', getFullPath('theme.ajax', 'php', 'url')),
                    array('\.php$', 'index.php?am-page=notfound'),
                    array('^user-page$', 'index.php?am-page=index&am-page-dir=user-page'),
                    array('^admin-page$', 'index.php?am-page=index&am-page-dir=admin-page'),
                    array('^user-page\/(.*)\/?', 'index.php?am-page=$matches[1]&am-page-dir=user-page'),
                    array('^admin-page\/(.*)?', 'index.php?am-page=$matches[1]&am-page-dir=admin-page'),
                    array('$', 'index.php?am-page=home'),
                    array('^(?!am_ajax)\/?([^\/]*)\/?', 'index.php?am-page=$matches[1]')

                );
                $this->add_am_rewrite($routers);
            }

            function add_am_rewrite($routers)
            {
                add_rewrite_tag('%am-page%', '([^&]+)');
                add_rewrite_tag('%am-page-dir%', '([^&]+)');
                foreach ($routers as $router) {
                    add_rewrite_rule($router[0], $router[1], 'top');
                }
            }

            function am_custom_admin_css()
            {
                am_add_css_file(get_template_directory_uri() . '/css/admin-css.css?ver=' . date('dhi'));
                am_add_script_file(get_template_directory_uri() . '/js/jquery-3.1.1.min.js');
                am_add_script_file(get_template_directory_uri() . '/js/knockout-3.4.1.js');

                am_add_css_file(includes_url('css/font-awesome.min.css'));
                importCSS('pCss', 'plugin.amAsset.css.style');
            }

            function am_add_admin_menu_separator()
            {

                global $menu;

                $menu[11] = array(
                    0 => '',
                    1 => 'read',
                    2 => 'separator',//. $position,
                    3 => '',
                    4 => 'wp-menu-separator'
                );
            }

            function mytraining_widgets_init()
            {
                register_sidebar(array(
                    'name' => 'Login Sidebar',
                    'id' => 'login_sidebar',
                    'before_widget' => '<div>',
                    'after_widget' => '</div>',
                    'before_title' => '<h2>',
                    'after_title' => '</h2>',));
            }

            function am_login_widget_init()
            {
                register_sidebar(array(
                    'name' => 'Login Sidebar',
                    'id' => 'login_sidebar',
                    'before_widget' => '<div>',
                    'after_widget' => '</div>',
                    'before_title' => '<h2>',
                    'after_title' => '</h2>',));
            }

            function do_AddHeaderStyle()
            {

                $fileList = array_diff(scandir(getFullPath('theme.css.Component', '', 'path')), array('..', '.'));

                foreach ($fileList as $file) {
                    $fileName = pathinfo($file)['filename'];
                    importStyle($fileName, 'theme.css.Component');
                }

                importStyle('font', 'theme.css');
                importStyle('main', 'theme.css');

                //bootstrap
                //importStyle('bootstrap.min', 'theme.css.bootstrap.css');
                importStyle('font-awesome', 'theme.css');

                importJs('jquery-3.1.1.min', 'theme.js');
                importJs('knockout-3.4.1', 'theme.js');
                //importJs('bootstrap.min', 'theme.css.bootstrap.js');

                importJs('jquery.signalR-2.2.2', 'home.communication.js');
                importJs('hubs', 'home.communication.SignalR', false);

                importJs('min', 'theme.js');

                importJs('i18n', 'theme.js.I18N');
                importJs('en', 'theme.js.I18N');
                importJs('vn', 'theme.js.I18N');

                //importStyle('dev_test', 'theme.css.frontend');
            }

            function do_AddFooterStyle()
            {
                importJs('main', 'theme.js');
            }


            function getMenuItemByLink($array, $link){
                if(!$array)
                    return null;

                foreach ($array as $item){
                    $item['Link'] = rtrim($item['Link'], '/');
                    if($item['Link'] == $link)
                        return $item;

                    if(isset($item['Sub'])){
                        $re = $this->getMenuItemByLink($item['Sub'], $link);
                        if($re)
                            return $re;
                    }
                }

                return null;
            }

            function do_ProcessContent()
            {
                $current_user = wp_get_current_user();
                $userRole = get_user_permission($current_user->user_login);

                $amPage = get_query_var('am-page');
                $amPageDir = get_query_var('am-page-dir');
                $path = 'theme.pages';
                $amModulePath = 'theme.Module';
                $menuItem = null;
                $menuItem = $this->getMenuItemByLink(using('theme.package.Includes.User.MainMenu'), "/$amPageDir/$amPage");
                if($menuItem == null)
                    $menuItem = $this->getMenuItemByLink(using('theme.package.Includes.Admin.MainMenu'), "/$amPageDir/$amPage");

                if ($amPageDir) {
                    $path = $path . '.' . $amPageDir;
                    $amModulePath = $amModulePath . '.' . $amPageDir;
                }
                if (!file_exists(getFullPath($path. '.' . $amPage, 'php'))) {
                    $amPage = 'notfound';
                }

                $amPage = str_replace('/', '.', $amPage);
                $path = $path . '.' . $amPage;
                $amModulePath = $amModulePath . '.' . $amPage;

                if(ie_detected()){
                    $path = 'theme.pages.notsupport';
                    $amModulePath = 'theme.Module.notsupport';
                } else if(mobile_detected()){
                    $path = 'theme.pages.developing';
                    $amModulePath = 'theme.Module.developing';
                } else if($menuItem !== null && check_permisson($menuItem['Role']) === false){
                    $path = 'theme.pages.notfound';
                    $amModulePath = 'theme.Module.notfound';
                }

                import($path, array(
                    pageName => $amPage,
                    pageDir => $amPageDir,
                    modulePath => getFullPath($amModulePath, '', 'url')
                ));

                if (file_exists(getFullPath($amModulePath, ''))) {

                    $fileList = array_diff(scandir(getFullPath($amModulePath, '', 'path')), array('..', '.'));

                    foreach ($fileList as $file) {
                        $fileName = pathinfo($file)['filename'];
                        if (strpos($file, 'css') === false) {
                            importJs($fileName, $amModulePath);
                        } else {
                            importStyle($fileName, $amModulePath);
                        }
                    }
                }
            }
        }
    }

    new ITAM();
/*
    am_ripJs();

    am_createFont();

    $RES_PATH = $_SERVER['REQUEST_URI'];

    add_action('admin_head', 'am_custom_admin_css');

    if(date_default_timezone_get() == 'UTC'){
        date_default_timezone_set('Asia/Bangkok');
    }

    function am_custom_admin_css() {
        am_add_css_file(get_template_directory_uri().'/css/admin-css.css?ver='.date('dhi'));
        am_add_script_file(get_template_directory_uri().'/js/jquery-3.1.1.min.js');
        am_add_script_file(get_template_directory_uri().'/js/knockout-3.4.1.js');
        am_add_script_file(includes_url('js/bootstrap.min.js'));
        wp_enqueue_style("lBootStrap", includes_url('css/bootstrap.min.css'));

		am_add_css_file(includes_url('css/font-awesome.min.css'));
        importCSS('pCss', 'plugin.amAsset.css.style');
    }


    add_action('admin_menu', 'am_add_admin_menu_separator');

    function am_add_admin_menu_separator( ) {

    	global $menu;

    	$menu[ 11 ] = array(
    		0	=>	'',
    		1	=>	'read',
    		2	=>	'separator' ,//. $position,
    		3	=>	'',
    		4	=>	'wp-menu-separator'
    	);

    }

    add_action('init', 'am_do_output_buffer');
    function am_do_output_buffer() {
        ob_start();
    }
    //widget area
    function mytraining_widgets_init() {
        register_sidebar( array(
            'name' => 'Login Sidebar',
            'id' => 'login_sidebar',
            'before_widget' => '<div>',
            'after_widget' => '</div>',
            'before_title' => '<h2>',
            'after_title' => '</h2>', ) );
    }
    add_action( 'widgets_init', 'mytraining_widgets_init' );
    function am_login_widget_init() {
      register_sidebar( array(
        'name' => 'Login Sidebar',
        'id' => 'login_sidebar',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>', ) );
    }
    add_action( 'widgets_init', 'am_login_widget_init' );
*/
?>