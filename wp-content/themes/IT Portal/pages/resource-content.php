<?php
    $_Param = null;
    if (empty($_POST)) {
        $_Param = $_GET;
    } else {
        $_Param = $_POST;
    }
    $pathInfo = split('\/', $_SERVER['REQUEST_URI'], 6);

    $requestClass = isset($pathInfo[2]) ? $pathInfo[2] : null;
    $requestType = isset($pathInfo[3]) ? $pathInfo[3] : null;
    $requestMethod = isset($pathInfo[4]) ? str_replace('/', '', $pathInfo[4]) : null;

    if ($requestClass != null && $requestType != null && $requestMethod != null) {
        if (file_exists(getFullPath('plugin.itportal.API.' . $requestClass . '.' . $requestType, 'php'))) {
            $ajaxClass = using('plugin.itportal.API.' . $requestClass . '.' . $requestType);
            $result = call_user_func(array($ajaxClass, $requestMethod), $_Param);
        } else {
            echo 'method not found';
        }
    } else {
        echo 'method not found';
    }
?>