<?php
/**
 * Created by HO An Lac.
 * User: Administrator
 * Date: 2/7/2018
 * Time: 11:20 AM
 */
?>
<?php add_action('content-holder', function () { ?>
    <h1 class="main-title">Đơn hàng chờ xử lý</h1>
    <div class="header-control">
        <div class="result-total">
            <div class="addon-control">
                <input placeholder="Tìm kiếm Ex: #1 or laptop" id="txbUserAccount"
                       data-bind="" type="text"/>
                <button id="btnHelpdeskCheckAccount" data-bind="click: $root.search_request"><i class="fa fa-search"></i></button>
                <button class="btn-request-filter option">
                    <i class="fa fa-filter"></i>
                    <options data-bind="foreach: listRequestDefined">
                        <option data-bind="click: $root.filterRequest_OnClick,text: DisplayName"></option>
                    </options>
                </button>
            </div>
            <span>Bạn đang xem  <span data-bind="text: $root.getPageResult()"></span> kết quả</span>
        </div>
    </div>
    <ul class="request-list" data-bind="foreach: {data: requests, as: 'request'}">
        <?php import('theme.layout.request.list-view-wait-buying'); ?>
    </ul>

    <div component="paging" pageItemLimit="5" name="assetPaging"></div>
<?php }); ?>
<?php import('theme.pages.master-page-user'); ?>
