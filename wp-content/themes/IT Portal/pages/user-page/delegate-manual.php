<?php add_action('content-holder', function () { ?>
   <h1 class="main-title">
      <span data-bind="text: i18n('user-page:delegate-manual:header')"></span>
   </h1>


   <div component="form" align="float" name="ChangeInUseUserForm">
      <div class="form-content">

         <!-- ko if: canSettingForAnotherUser -->
         <item width="50%" text="user-page:delegate:domain" name="Domain" type="search" searchvalue="AccountName" searchtemplate="{FullName}" searchhandler="APIData.CMDB.GetListUser" searchlimit="7" displaytext="FullName"></item>
         <!-- /ko -->

         <!-- ko if: isDataReady() -->
         <!-- ko if: isNoRequest == 0 -->
         <div class="not-have">Chưa có đơn hàng nào cần xử lý</div>
         <!-- /ko -->

         <!-- ko if: !isNoRequest() -->
         <item width="50%" require text="user-page:delegate:delegatedDomain" name="DelegatedDomain" searchvalue="AccountName" searchtemplate="{FullName}" searchhandler="APIData.CMDB.GetListUser" searchlimit="7" displaytext="FullName" type="search"></item>
<!--         <item width="100%" require text="Ghi chú cho các đơn hàng đang chờ duyệt" name="CommentForPending" type="text"></item>-->
         <item type="custom" name="Mapping">
            <button data-bind="click: delegate, text: i18n('user-page:delegate-manual:btnDelegate')" id="btnDelegate" class="green"></button>
         </item>
         <item type="custom" text="user-page:delegate-manual:request-list" name="Mapping">
            <div style="text-align: right">
               <select component="combobox" type="combobox"
                       data-bind="value: filter,
                            options: filter.options,
                            optionsText: 'name',
                            optionsValue: 'value'," >
               </select>
            </div>
            <div data-bind="if: requests().length > 1" class="select-all">
               <input data-bind="click: selectAll, checked: isSelectAll" class="nolabel" component="checkbox" type="checkbox">
               <span data-bind="click: selectAll, text: i18n('component:select-all')"></span>
            </div>
            <ul class="request-list" data-bind="foreach: {data: requests, as: 'request'}">
                <?php import('theme.layout.request.list-view'); ?>
            </ul>
         </item>
         <!-- /ko -->
         <!-- /ko -->
      </div>

   </div>


   <!--   <div class="user-info">--><!--      <div class="search-box" data-bind="--><!--         search: domain,--><!--         searchAPI: APIData.CMDB.GetListUser,--><!--         searchConfig:{displayName: 'FullName', valueName:'AccountName'}">--><!--         <input placeholder="Người ủy quyền" class="search-component">--><!--      </div>--><!--   </div>-->


<?php });

import('theme.pages.master-page-user');
?>
