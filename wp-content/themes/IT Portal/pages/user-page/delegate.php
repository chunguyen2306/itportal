<?php add_action('content-holder', function () { ?>
   <h1 class="main-title">
      <span data-bind="text: i18n('user-page:delegate-manual:header')"></span>
   </h1>
   <div component="form" align="float" name="ChangeInUseUserForm">
      <div class="form-content">
         <!--  ko if: canSettingForAnotherUser -->
         <item width="100%" require text="user-page:delegate:domain" name="Domain" type="search" searchvalue="AccountName" searchtemplate="{FullName}"
               searchhandler="APIData.CMDB.GetListUser" searchlimit="7" displaytext="FullName"></item>
         <!--  /ko -->
         <item width="50%" require text="user-page:delegate:startTime" name="StartTime" type="date-time"></item>
         <item width="50%" require text="user-page:delegate:endTime" name="EndTime" type="date-time"></item>
         <item width="50%" require text="user-page:delegate:delegatedDomain" name="DelegatedDomain" searchvalue="AccountName"
               searchtemplate="{FullName}" searchhandler="APIData.CMDB.GetListUser" searchlimit="7" displaytext="FullName" type="search"></item>
         <item width="50%" text="user-page:delegate:delegateMode" name="DelegateMode" type="checkbox"></item>
      </div>
      <div class="form-control-panel">
         <button class="save" data-bind="click:active, text: i18n('user-page:delegate:btnActive')"></button>
         <!--  ko if: isUpdate -->
         <button class="save" data-bind="click:deactive, text: i18n('user-page:delegate:btnDeactive')"></button>
         <!--  /ko -->
      </div>
   </div>

<?php });

import('theme.pages.master-page-user');
?>
