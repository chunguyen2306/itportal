<?php add_action('content-holder', function () {
    import('plugin.itportal.API.CMDB.Resources');
    import('plugin.itportal.Factory.AMAssetModelDetailFactory');
    $apiModeldetail = new AMAssetModelDetailFactory();
    $current_user = wp_get_current_user();
    $pageIndex = isset($_GET['page']) ? $_GET['page'] : 1;
    $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : "";
    //cpnguyen
    $categoryType = isset($_GET['categoryType']) ? $_GET['categoryType'] : "";
    $asset = isset($_GET['asset']) ? $_GET['asset'] : "";
    $mydept = isset($_GET['mydept']) ? $_GET['mydept'] : "false";
    $username = isset($_GET['username']) ? $_GET['username'] : $current_user->user_login;
    // $viewType = isset($_GET['viewType']) ? $_GET['viewType'] : '';
    $viewType = isset($_GET['viewType']) ? $_GET['viewType'] : 'detail';
    $productType = isset($_GET['productType']) ? $_GET['productType'] : '';
    $requestLink = "/user-page/asset-detail/?viewType=category&productType=Access%20Point&asset=";
    $pageIndex -= 1;
    // $limit = isset($_GET['limit']) ? $_GET['limit'] : 15;
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 5;
    $cmdbResourceAPI = new Resources();
    $assets = array();
    if (!isset($_GET['helpdesk']) || isset($_GET['username'])) {
        $assets = json_decode($cmdbResourceAPI->GetUserAsset(array(
            'username' => ($username == '_helpdesk' ? '' : $username),
            'viewType' => $viewType,
            'mydept' => $mydept,
            'keyword' => $keyword,
            //cpnguyen
            'categoryType' => $categoryType,
            'asset' => $asset,
            'pageIndex' => $pageIndex,
            'productType' => $productType,
            'limit' => $limit,
            'isForHelpDesk' => (check_permisson('search.asset') && isset($_GET['helpdesk']) ? true : false)
        )))->Result;
    }
    $userRole = get_user_permission($current_user->user_login);

    if(count($asset) != 0) {
        $category = array();
        foreach ($assets->Category as $asset) {
            if(!in_array($asset,$category)) {
                array_push($category, $asset);
            }
        }
        sort($category);
        array_unshift($category, 'All');
    }
    ?>

    <?php if (check_permisson('search.asset') && isset($_GET['helpdesk'])) { ?>
        <h2 class="main-title">Tra cứu thông tin sản phẩm</h2>
        <div class="helpdesk-control">
            <div class="addon-control">
                <input placeholder="Nhập vào tên người sử dụng hoặc mã sản phẩm để tìm kiếm" id="txbUserAccount"
                       data-bind="value: userAccount"
                       type="text"/>
                <button id="btnHelpdeskCheckAccount" data-bind="click: btnHelpdeskCheckAccount_OnClick">Xem</button>
            </div>
        </div>
    <?php } else { ?>
        <h2 class="main-title">Sản phẩm tôi đang quản lý</h2>
    <?php } ?>
    <div class="page-content">
        <?php if (sizeof($assets) == 0) { ?>
            <div class="not-have">Không tìm thấy kết quả theo điều kiện tìm kiếm</div>
            <div id="select">
            </div>


            <div component="popup" name="create-request-comment" width="900px" height="265px">
                <h2>Thông thi ghi chú
                    <button class="red close">
                        <i class="fa fa-close"></i>
                    </button>
                </h2>
                <div class="popup-content">

                    <div component="form" align="float" name="CreateRequestForm">
                        <div class="form-content">
                            <item text="Vui lòng nhập thông tin ghi chú để chúng tôi có thể biết rõ thêm về tình hình tài sản "
                                    require width="100%" name="Note" type="ricktext"></item>
                        </div>
                        <div class="form-control-panel">
                            <button data-bind="click: btnSubmitCreateRequest_OnClick, text: 'Đồng ý'"></button>
                            <button data-bind="click: btnCancelCreateRequest_OnClick, text: 'Hủy'"></button>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="top-bar">
                <div>
                    <button id="return_button" data-bind="click: sel_actionSelected" value="ReturnAsset"
                            class="gray">Trả
                    </button>
                    <button id="damage_button" data-bind="click: sel_actionSelected" value="BrokenAsset"
                            class="gray">Báo hư hỏng
                    </button>
                    <button id="lost_button" data-bind="click: sel_actionSelected" value="LostAsset"
                            class="gray">Báo mất
                    </button>
                </div>

                <div class="search">
                    <div class="number-of-product">
                        <span id="quantity_product_noti">Bạn đang chọn</span>
                        <span data-bind="text: quantityAssets"></span>
                        <span id="quantity_product_noti">sản phẩm</span>
                    </div>
                    <div class="search-bar">
                        <div class="search_control">
                                <input type="search" value='<?php echo $keyword ?>' id="txbSearchAsset" data-bind="attr: {placeholder: i18n('cart:popup-return-asset:left-side:search:input:placeholder')}" />
                                <button id="searchBtn">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        <!-- data-bind="value: keyword"  -->
                    </div>
                </div>
            </div>

            <div class="chip-container">
                <i class="fa fa-angle-left" data-bind="event : { click: scroll_left }"></i>
                
                <?php
                    generateCategorySlide($category)?>

                <i class="fa fa-angle-right" data-bind="event : { click: scroll_right }"></i>
                <!-- <i class="fa fa-angle-left" data-bind="event : { click: scroll_left }"></i>
                <div class="chip" data-bind="event : { click: chip_select }">
                    All
                </div>
                <div class="chip" data-bind="event : { click: chip_select }">
                    <img width="20px" height="20px" src="http://itportal:8080/wp-content/uploads/main-menu/may_tinh_xach_tay_0.png?v=1.2.1"/>
                    <span>Máy tính xách tay</span>
                </div>
                <div class="chip" data-bind="event : { click: chip_select }">
                    <img width="20px" height="20px" src="http://itportal:8080/wp-content/uploads/main-menu/may_tinh_de_ban_0.png?v=1.2.1"/>
                    <span>Máy tính để bàn</span>
                </div>
                <div class="chip" data-bind="event : { click: chip_select }">
                    <img width="20px" height="20px" src="http://itportal:8080/wp-content/uploads/main-menu/man_hinh,_tivi_0.png?v=1.2.1"/>
                    <span>Màn hình, Tivi</span>
                </div>
                <div class="chip" data-bind="event : { click: chip_select }">
                    <img width="20px" height="20px" src="http://itportal:8080/wp-content/uploads/main-menu/dien_thoai,_may_tinh_bang_0.png?v=1.2.1"/>
                    <span>Điện thoại, Máy tính bảng</span>
                </div>
                <i class="fa fa-angle-right" data-bind="event : { click: scroll_right }"></i> -->
                <!-- <div class="chip">
                    <img width="20px" height="20px" src="http://itportal:8080/wp-content/uploads/main-menu/thiet_bi_luu_tru_0.png?v=1.2.1"/>
                    <span>Thiết bị lưu trữ</span>
                </div>
                <div class="chip">
                    <img width="20px" height="20px" src="http://itportal:8080/wp-content/uploads/main-menu/thiet_bi_am_thanh_0.png?v=1.2.1"/>
                    <span>Thiết bị âm thanh</span>
                </div> -->
            </div>
            <?php if(count($assets->Assets) != 0) { ?>
                <table name="asset-table" class="datatable" component="tabledata">
                    <thead>
                        <tr class="table-header">
                            <th>
                                <label>
                                    <input id="product_checked_all_id" data-bind="event: { change: chk_SelectAllInPage }"
                                        component="checkbox"
                                        class="nolabel onlycheck" type="checkbox">
                                </label>
                            </th>
                            <th>Loại sản phẩm</th>
                            <th>Hình ảnh sản phẩm</th>
                            <th>Thông tin sản phẩm</th>
                            <th>Thông tin bàn giao</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($assets->Assets as $asset) {
                            $assetInfo = getInFoAssetResource($asset->RESOURCENAME);
                            ?>
                            <tr>
                                <td>
                                    <input type="hidden" value='<?php echo json_encode($asset); ?>'/>
                                    <label>
                                        <input id="<?php echo $asset->RESOURCENAME; ?>"
                                            data-bind="event: { change: chk_Asset }" component="checkbox"
                                            class="nolabel onlycheck" type="checkbox" >
                                    </label>
                                </td>
                                <td class="product_type">
                                    <a href='/search/?st=product&sk= <?php echo $asset->COMPONENTTYPENAME; ?>'>
                                        <strong>
                                            <?php echo $asset->COMPONENTTYPENAME; ?>
                                        </strong>
                                    </a>
                                </td>
                                <td class="product_picture">
                                    <?php
                                        $detail = $apiModeldetail->getByModelID($asset->COMPONENTID);
                                        if ($detail !== null && $detail->Thubmails !== '') {
                                            ?>
                                            <img src="<?php echo getFullPath('upload.component.' . $asset->COMPONENTID, '', 'url') . '/' . $detail->Thubmails; ?>"/>
                                        <?php } else { ?>

                                            <img src="<?php echo getFullPath('upload.component_type', '', 'url') . '/' . $matches[1]; ?>"/>
                                        <?php }
                                    ?>
                                </td>
                                <td class="product_info">
                                    <div>
                                        <a target="_blank" href="<?php echo $requestLink . $asset->RESOURCENAME; ?>">
                                            <strong><?php echo $asset->RESOURCENAME; ?></strong>
                                        </a>
                                    </div>
                                    <div>
                                        <a target="_blank" href="/detail/<?php echo $asset->COMPONENTID; ?>">
                                            <?php echo $asset->COMPONENTNAME; ?>
                                        </a>
                                    </div>
                                </td>
                                <td class="product_paper">
                                    <strong>
                                        <?php if ($assetInfo->order == null || $assetInfo->order == '' || $assetInfo->order == undefined) { ?>
                                            <span>Chưa có thông tin</span>
                                        <?php } else { ?>
                                            <a href='/user-page/order/detail/?ID= <?php echo $assetInfo->order; ?>'>
                                                #<?php echo $assetInfo->order; ?>
                                            </a>
                                        <?php } ?>
                                    </strong>
                                    <div>
                                        <?php if ($assetInfo->orderdate == null || $assetInfo->orderdate == '' || $assetInfo->orderdate == undefined) { ?>
                                            <i>Chưa có thông tin</i>
                                        <?php } else {

                                            echo Date('d/m/Y', $assetInfo->orderdate);
                                        } ?>
                                    </div>
                                </td>
                                <td>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            <?php } else { ?>
                <div class="not-have">Không tìm thấy kết quả theo điều kiện tìm kiếm</div>
            <?php } ?>
          
            <div class="bottom-bar">
                <div>
                    <button id="select_all_bt" class="gray" data-bind="click: $root.select_all_event" <?php  echo ( $assets->PageCount == 1 || $assets->PageCount == 0 ) ? 'disabled' : '' ?>>
                        Chọn tất cả
                    </button>
                    <button id="remove_all_bt" class="gray" data-bind="click: $root.remove_all_event" <?php  echo ( $assets->PageCount == 1 || $assets->PageCount == 0 ) ? 'disabled' : '' ?>>
                        Bỏ chọn tất cả
                    </button>
                </div>

                <div component="paging" currentPage="<?php echo $pageIndex + 1; ?>" pageItemLimit="5"
                    pageCount="<?php echo $assets->PageCount; ?>" total="<?php echo $assets->Total; ?>"
                    name="productPaging">
                </div>
            </div>

            <div id="select">
            </div>


            <div component="popup" name="create-request-comment" width="900px" height="265px">
                <h2>Thông thi ghi chú
                    <button class="red close">
                        <i class="fa fa-close"></i>
                    </button>
                </h2>
                <div class="popup-content">

                    <div component="form" align="float" name="CreateRequestForm">
                        <div class="form-content">
                            <item text="Vui lòng nhập thông tin ghi chú để chúng tôi có thể biết rõ thêm về tình hình tài sản "
                                    require width="100%" name="Note" type="ricktext"></item>
                        </div>
                        <div class="form-control-panel">
                            <button data-bind="click: btnSubmitCreateRequest_OnClick, text: 'Đồng ý'"></button>
                            <button data-bind="click: btnCancelCreateRequest_OnClick, text: 'Hủy'"></button>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php }); ?>
<?php import('theme.pages.master-page-user'); ?>