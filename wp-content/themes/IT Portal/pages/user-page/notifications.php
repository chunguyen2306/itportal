<?php add_action('content-holder', function () { ?>
    <h1 class="main-title">Thông báo của tôi</h1>
    <div class="page-content">
        <table class="all-noti-table">
            <div class="header-control">
                <div class="head-filter" data-bind="visible: pendingMe">
                    <a st="status" searchType="pending" data-bind="click: $root.filterControl_OnClick,
                        attr: {class: (($root.searchKeyword === 'pending')?'active':'')}">
                        Chưa đọc
                        (<span id="pending_quantity">0</span>)
                    </a>
                    <a st="status" searchType="read" data-bind="click: $root.filterControl_OnClick,
                        attr: {class: (($root.searchKeyword === 'read')?'active':'')}">
                        Đã đọc
                        (<span id="read_quantity">0</span>)
                    </a>
                    <a st="status" searchType="all" data-bind="click: $root.filterControl_OnClick,
                        attr: {class: (($root.searchKeyword === 'all')?'active':'')}">
                        Tất cả
                        (<span id="all_quantity">0</span>)
                    </a>
                </div>
                <div id="more" class="result-total">
                    <div class="dropdown">
                        <i id="more_button" class="fa fa-ellipsis-v" aria-hidden="true"></i>
                        <div class="dropdown-content">
                            <!-- ko if: $root.isShowDelete -->
                            <a data-bind="click: $root.maskAllAsRead">Đã đọc toàn bộ</a>
                            <!-- /ko -->
                            <a data-bind="click: $root.remove_all">Xoá toàn bộ</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ko if: list().length > 0 -->
            <tbody class="all-noti" data-bind="foreach: list">
            <tr data-bind="css: {read: IsRead == 1}">
                <td class="img">
                    <img src="/wp-content/themes/IT%20Portal/img/notification/invoice.png">
                </td>
                <td class="title">
                    <a data-bind="event:{mousedown: $root.detailMouseDown}, attr:{href: Notification.orderLink + '?ID=' + Content}">
                        <span data-bind="text: Title"></span>
                        <span data-bind="text: Libs.Workflow.getPendingDate(new Date(Time))"></span>
                    </a>
                </td>
                <td class="mark" data-bind="if: IsRead == 0"><a title="Đánh dấu đã đọc"
                                                                data-bind="click: $root.maskAsRead"><i
                                class="fa fa-eye-slash"></i></a></td>
                <td class="remove"><a data-bind="click: $root.remove">Xóa</a></td>
            </tr>
            </tbody>
            <!-- /ko -->
        </table>
        <div data-bind="if: (Notification.isLoading == false || $root.list().length == 0)"
             class="item no-item" section="no-item">
            <p>Bạn chưa có thông báo</p>
        </div>
        <div component="paging" pageItemLimit="5" name="infoPaging"></div>
    </div>
<?php }); ?>
<?php import('theme.pages.master-page-user'); ?>