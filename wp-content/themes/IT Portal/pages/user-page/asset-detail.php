<?php add_action('content-holder', function () {
    import('plugin.itportal.Factory.AMAssetModelDetailFactory');
    import('plugin.itportal.Factory.AMAssetModelReviewFactory');
    import('plugin.itportal.Factory.AMUserFactory');
    import('plugin.itportal.Factory.AMUserRoleFactory');
    $current_user = wp_get_current_user();

    $assetModel = null;
    $reviewRateAverage = 0;

    $detailFac = new AMAssetModelDetailFactory();
    $reviewFac = new AMAssetModelReviewFactory();
    $assetName = isset($_GET["asset"]) ? $_GET["asset"] : null;
    $userRole = get_user_permission($current_user->user_login);
    if ($assetName == null) {
        return;
    }
    $assetInfo = getInFoAssetResource($assetName);
    $assetInfo->ResourceName = $assetName;

    $modelID = $assetInfo->ResourceModelID;
    $assetModel = $detailFac->getByModelID((double)$modelID);
    $reviewData = $reviewFac->GetByAssetName(['AssetName' => $assetName]);
    foreach ($reviewData as $review) {
        $reviewRateAverage += $review->Rate;
    }
    $reviewRateAverage = $reviewRateAverage / (sizeof($reviewData) > 0 ? sizeof($reviewData) : 1);
    if ($assetModel == null) {
        $assetModel = array(
            Thubmails => '',
            Gallery => array(),
            ShortDescription => '',
            Description => '',
            Cost => 0
        );
    } else {
        $assetModel->Gallery = json_decode($assetModel->Gallery);
    }

    $str = $assetInfo->ResourceTypeDescription;
    $re = '/Icon: (((?!Notes|Icon|Code).)*)/';
    preg_match($re, $str, $matches);

    ?>
    <button class="gray" data-bind="click: btnBack_OnClick" xmlns="http://www.w3.org/1999/html">
        <span class="fa fa-caret-left"></span>
        Trở về
    </button>
    <?php if(check_permisson('resource.asset.view') || $current_user->user_login == $assetInfo->OwnerAccount || $current_user->user_login == $assetInfo->OwnerParentAccount) {
        ?>
    <div class="control-panel" assetname="<?php echo $assetName; ?>"
                  assettype="<?php echo $assetInfo->ResourceType; ?>">
        <button data-bind="click: btnCreateRequest" actiontype="ReturnAsset" class="gray btn-asset">Trả</button>
        <button data-bind="click: btnCreateRequest" actiontype="BrokenAsset" class="gray btn-asset">Báo hư hỏng</button>
        <button data-bind="click: btnCreateRequest" actiontype="LostAsset" class="gray btn-asset">Báo mất</button>
        <!--<button data-bind="click: btnCreateRequest" actiontype="ReplaceAsset" class="gray btn-asset">Yêu cầu thay thế</button>-->
         <?php if (check_permisson('resource.asset.note')) { ?>
        <button data-bind="click: btnAddNotes" actiontype="Note" class="gray">Thêm ghi chú</button>        <?php }
        if (check_permisson('resource.asset.manage')) { ?>
            <button class="option btn-asset gray">
                Thay đổi
                <options>
                    <option data-bind="click: btnChangeAssetState_OnClick">Trạng thái</option>
                    <!--<option data-bind="click: btnChangeAssetDetail_OnClick">Cấu hình</option>-->
                    <option data-bind="click: btnInUseToUser_OnClick">Gắn với nhân viên</option>
                    <?php if($assetInfo->IsComponent){ ?>
                    <option data-bind="click: btnInUseToAsset_OnClick">Gắn với thiết bị</option>
                <?php }?>
                </options>
            </button>
        <?php } ?>
    </div>

    <div section="asset-detail">
        <script type="template" id="assetInfo">
            <?php echo json_encode($assetInfo); ?>
        </script>
        <script type="template" id="assetNoteHistory">
            <?php echo json_encode($assetInfo->assetHistoryComment); ?>
        </script>
        <div class="asset-info">
            <div class="asset-img">
                <?php if ($assetModel->Thubmails !== '' && $assetModel->Thubmails !== null) { ?>
                    <img src="<?php echo getFullPath('upload.component.' . $modelID, '', 'url') . '/' . $assetModel->Thubmails; ?>"/>
                <?php } else if (sizeof($matches) > 1) { ?>
                    <img src="<?php echo getFullPath('upload.component_type', '', 'url') . '/' . $matches[1]; ?>"/>
                <?php } else { ?>
                    <img src="<?php echo getFullPath('upload.component', '', 'url') . '/default.png'; ?>"/>
                <?php } ?>
            </div>
            <div class="asset-detail" style="margin-top: 0px;">
                <div class="asset-name"><?php echo $assetName; ?></div>
                <div>
                    <table>
                        <tr>
                            <td colspan="4"><?php echo $assetInfo->ResourceType; ?>
                                <a href="/detail/<?php echo $assetInfo->ResourceModelID; ?>"><?php echo $assetInfo->ResourceModel; ?></a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4"><b>Serial number:</b><span id="assetSerial"><?php if ($assetInfo->GeneralAssetInfo->SerialNumber == null) {
                                        echo ' Không có thông tin';
                                    } else { ?><b><?php
                                        echo $assetInfo->GeneralAssetInfo->SerialNumber;
                                    } ?></span></b>
                            </td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td width="200px"><span><b>Trang thái:</b></span>
                            <td width="200px"><strong><?php echo $assetInfo->Status; ?></strong></td>
                            <td width="200px" colspan="2">
                                <?php if($assetInfo->Owner == null){ ?>
                                    <a href="/user-page/asset/?helpdesk=true&username=<?php echo $assetInfo->ParentOwnerAccount; ?>">
                                        <?php echo $assetInfo->ParentOwner; ?></a>
                                <?php } else { ?>
                                <a href="/user-page/asset/?helpdesk=true&username=<?php echo $assetInfo->OwnerAccount; ?>">
                                    <?php echo $assetInfo->Owner; ?></a>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php if ($assetInfo->Status == 'In Use' || $assetInfo->Status == 'Delivered Helpdesk' || $assetInfo->Status == 'Delivered User') { ?>
                            <tr>
                                <td><b>Ngày giao:</b></td>
                                <td>
                                    <span>
                                        <?php if ($assetInfo->orderdate == null || $assetInfo->orderdate == '' || $assetInfo->orderdate == undefined) { ?>
                                            <i>Chưa có thông tin</i>
                                        <?php }else{

                                            echo Date('d/m/Y',$assetInfo->orderdate);
                                        } ?>
                                    </span>
                                </td>
                                <td colspan="2">    <i><?php echo $assetInfo->OwnerJobTitle ?></i></td>

                            </tr>
                            <tr>
                                <td><b>Chứng từ bàn giao:</b></td>
                                <td>
                                    <span>
                                        <?php if ($assetInfo->order == null || $assetInfo->order == '' || $assetInfo->order == undefined) { ?>
                                            <i>Chưa có thông tin</i>
                                        <?php } else{?>
                                            <a href='/user-page/order/detail/?ID= <?php echo $assetInfo->order; ?>'> <strong>#<?php echo $assetInfo->order; ?></strong></a>
                                        <?php } ?>
                                    </span>
                                </td>
                                <td colspan="2"><i><?php echo $assetInfo->Deparment; ?></i></td>

                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td><b>Ngày mua hàng:</b></td>
                            <td><?php echo date('d/m/Y', $assetInfo->GeneralAssetInfo->AcquitionDate/1000); ?></td>
                            <td><b>PR mua hàng:</b></td>
                            <td>
                                <?php if ($assetInfo->WorkstationField->PurchaseRequest != null) { ?>
                                    <a><?php echo $assetInfo->WorkstationField->PurchaseRequest; ?></a>
                                <?php } else { ?>
                                    <i>Chưa có thông tin</i>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Hạn bảo hành:</b></td>
                            <td><?php echo date('d/m/Y', $assetInfo->GeneralAssetInfo->WarrantyExpiryDate/1000); ?></td>
                            <td><b>Đã sử dụng:</b></td>
                            <td> <strong><?php echo $assetInfo->assetUsedDate; ?> tháng</strong></td>
                        </tr>
                        <tr>
                            <td><b>Chi phí đầu tư:</b></td>
                            <td>
                                <?php if ($assetInfo->Cost == 0) { ?>
                                    <i>Chưa có thông tin</i>
                                <?php } else { ?>
                                    <strong><?php echo number_format_i18n($assetInfo->Cost); ?> VNĐ</strong>
                                <?php } ?>
                            </td>                        </tr>                    </table>
                </div>
            </div>
        </div>
        <div class="asset-additional-info" component="tab">
            <ul class="tab-item">
                <li class="active" target="assetDetail">Chi tiết sản phẩm</li>
                <li target="assetLifeCycle">Lịch sử thay đổi</li>
                <li target="assetChild">Sản phẩm đính kèm</li>
                <li target="assetNoteHistory">Lịch sử ghi chú</li>
                <li target="assetReview">Đánh giá <b><?php echo round($reviewRateAverage, 1); ?></b>
                    (<?php echo sizeof($reviewData); ?>)
                </li>
            </ul>
            <div class="active tabContent" id="assetDetail">
                <?php
                if ($assetInfo->IsComponent) { ?>
                    <span>Gắn với: </span><b><a
                                href="/user-page/asset-detail/?asset=<?php echo $assetInfo->Parent; ?>">
                            <span id="assetParent"><?php echo $assetInfo->Parent; ?></span></a></b>
                    <br><br>
                    <table >
                        <?php foreach ($assetInfo->ComponentField as $key => $value) { ?>
                            <tr>
                                <td width="150px"><b><?php echo $key; ?>:  </b></td>
                                <td><?php echo $value; ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                <?php } else { ?>
                <table >

                    <?php } if($assetInfo->Specs!='' && $assetInfo->Specs!==null && $assetInfo->Specs!== undefined && $assetInfo->Specs!== 'unkown'){ ?>
                        <tr>
                            <td width="250px;"><b>Thông tin cấu hình: </b></td>
                            <td ><i><?php echo $assetInfo->Specs; ?></i></td>
                        </tr>
                    <?php } ?>

                    <?php if($assetInfo->MacAddress!== null && $assetInfo->MacAddress!== undefined && $assetInfo->MacAddress!==''){?>
                         <tr>
                             <td width="250px;"><b>Địa chỉ MAC: </b></td>
                        <td><i><?php
                            echo $assetInfo->MacAddress; ?></i></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td width="250px;"><b>Vị trí đặt sản phẩm:  </b></td>
                        <td><i><?php if($assetInfo->WorkstationFieldString->Office!=='' && $assetInfo->WorkstationFieldString->Office!==null && $assetInfo->WorkstationFieldString->Office!==undefined && $assetInfo->WorkstationFieldString->Office!==unknown)
                                {
                                    echo $assetInfo->WorkstationFieldString->Office;
                                }
                                else{
                                    echo 'Không có thông tin vị trí';
                                }  ?></i></td>
                    </tr>
                </table>

            </div>
            <div class="tabContent" id="assetChild">
                <ul class="asset-child category-view">
                    <?php if ($assetInfo->childrens == null) {
                        echo 'Không có sản phẩm nâng cấp hoặc lắp đặt thêm';
                    } else {
                        foreach ($assetInfo->childrens as $child) { ?>
                            <li data-bind="click: $root.childDetail_OnClick" type="<?php echo $child->ResourceType; ?>"
                                name="<?php echo $child->name; ?>">
                                <div class="imgVerFrame">
                                        <?php

                                        $str = $child->detail->ResourceTypeDescription;

                                        $re = '/Icon: (((?!Notes|Icon|Code).)*)/';
                                        preg_match($re, $str, $matches);

                                        if ($child->detail->ResourceType === 'Category') { ?>
                                            <img src="<?php echo getFullPath('upload.component_type', '', 'url') . '/' . $matches[1]; ?>"/>
                                        <?php } else {
                                            $detail = $detailFac->getByModelID($child->detail->ResourceModelID);
                                            if($detail !== null && $detail->Thubmails !== ''){
                                                ?>
                                                <img src="<?php echo getFullPath('upload.component.' . $child->detail->ResourceModelID, '', 'url') . '/' . $detail->Thubmails; ?>"/>
                                            <?php } else {  ?><div class="asset-img">
                                                <img src="<?php echo getFullPath('upload.component_type', '', 'url') . '/' . $matches[1]; ?>"/>
                                            </div>
                                            <?php }
                                        } ?>
                                        <span class="helper"></span>

                                    <a href="/user-page/asset-detail/?asset=<?php echo $child->name; ?>"></a>
                                </div>
                                <a href="/user-page/asset-detail/?asset=<?php echo $child->name; ?>"><?php echo $child->name; ?></a>
                                <span><?php echo $child->detail->ResourceModel; ?></span>
                            </li>
                        <?php }
                    } ?>
                </ul>
            </div>
            <div class="tabContent" id="assetLifeCycle">
                <?php  foreach ($assetInfo->HistoryChangeState as $item) {  ?>

                    <div class="history-item">
                        <div data-bind="click: lblTitle_OnClick" class="title">
                            <?php
                            $iconStr = '';
                            $infoStr = '';
                            if ($item->Parent !== null) {
                                $iconStr = '<i class="fa fa-desktop"></i>';
                                $infoStr = 'gắn vào thiết bị <b>' . $item->Parent . '</b>';
                            } else if ($item->OwnerAccount !== null) {
                                $iconStr = '<i class="fa fa-user-circle"></i>';
                                $infoStr = 'cập nhật người quản lý sang <b>' . $item->OwnerAccount . '</b>';
                            } else {
                                $iconStr = '<i class="fa fa-info-circle"></i>';
                                if ($item->PreviousState !== null) {
                                    $infoStr = 'đổi trạng thái từ <b>' . $item->PreviousState . '</b>';
                                    $infoStr = $infoStr . ' sang <b>' . $item->NewState . '</b>';
                                } else {
                                    $infoStr = 'gán trạng thái là <b>' . $item->NewState . '</b>';
                                }
                            } ?>

                            <?php echo $iconStr; ?>
                            <b><?php echo ($item->ITAMActionUser != null && $item->ITAMActionUser != '') ? $item->ITAMActionUser : $item->ActionUserAccount; ?></b>
                            <?php echo $infoStr; ?>

                            <span class="history-date"><i
                                        class="fa fa-calendar"></i> <?php echo date('d/m/Y h:m:s', $item->StartDate); ?></span>
                        </div>
                        <div class="detail">
                            <table class="asset-configuration">
                                <tr>
                                    <td width="20%">Người thao tác:</td>
                                    <td><?php echo ($item->ITAMActionUser != null && $item->ITAMActionUser != null) ? $item->ITAMActionUser : $item->ActionUserFullName; ?></td>
                                </tr>
                                <tr>
                                    <td>Thời gian:</td>
                                    <td><?php echo date('d/m/Y h:m:s', $item->StartDate); ?></td>
                                </tr>
                                <tr>
                                    <td>Trạng thái trước đó:</td>
                                    <td><?php echo $item->PreviousState ?></td>
                                </tr>
                                <tr>
                                    <td>Trạng thái mới:</td>
                                    <td><?php echo $item->NewState ?></td>
                                </tr>
                                <tr>
                                    <td>Người sở hữu:</td>
                                    <td><?php echo $item->OwnerFullName ?></td>
                                </tr>
                                <tr>
                                    <td>Tài sản cha:</td>
                                    <td><?php echo $item->Parent ?></td>
                                </tr>
                                <tr>
                                    <td>Ghi chú:</td>
                                    <td><?php echo ($item->ITAMComment != null && $item->ITAMComment != '') ? $item->ITAMComment : $item->Comment ?></td>
                                </tr>
                                <tr>
                                    <td>Mã đơn hàng:</td>
                                    <td><?php $re = '/\[Request: (\d+)\]\[Workflow: ([^\]]+)\]\[Step: ([^\]]+)\]/m';
                                        preg_match_all($re, $item->SysComment, $matches, PREG_SET_ORDER, 0); if ($matches == null || $matches == '' || $matches == undefined) { ?>
                                            <i>Chưa có thông tin</i>
                                        <?php } else{?>
                                          <span> Đơn hàng <?php echo strtolower($matches[0][2]); ?></span>
                                          <a href='/user-page/order/detail/?ID= <?php echo $matches[0][1]; ?>'> <strong>#<?php echo $matches[0][1]; ?></strong> </a>
                                          <?php } ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="tabContent" id="assetNoteHistory">
                <?php foreach ($assetInfo->assetHistoryComment as $item) { ?>
                    <div class="history-item">
                        <div data-bind="click: lblTitle_OnClick" class="title">

                            <b><?php echo $item->UserAction; ?></b><span> đã ghi chú tình trạng <b><?php echo $item->TargetUser ?></b> của sản phẩm</span>
                            <span class="history-date"><i class="fa fa-calendar"></i>
                                <?php echo ($item->ActionDate); ?></span>
                        </div>
                        <div class="detail">
                            <table class="asset-configuration">
                                <tr>
                                    <td width="20%">Người thao tác:</td>
                                    <td><?php echo $item->UserAction; ?></td>
                                </tr>
                                <tr>
                                    <td>Thời gian:</td>
                                    <td><?php echo ($item->ActionDate); ?></td>
                                </tr>
                                <tr>
                                    <td>Loại ghi chú:</td>
                                    <td><?php echo $item->TargetUser ?></td>
                                </tr>
                                <tr>
                                    <td>Ghi chú:</td>
                                    <td><?php $str = str_replace('<', '&lt;', $item->Comment ); $str = str_replace('>', '&gt;', $item->Comment );
                                        echo ($str != null && $str != '') ? $str : $str ?></td>
                                </tr>
                                <tr>
                                    <td>Hình ảnh tình trạng:</td>
                                    <td><?php $imagearray = json_decode($item->SysComment);
                                        if ($imagearray != null && $imagearray != '' && $imagearray !== undefined) { ?>
                                            <?php
                                            foreach ($imagearray as $image) {
                                                ?>
                                                <img src="/wp-content/uploads/Asset_Note/<?php echo $item->AssetName ?>/<?php echo $image ?>"
                                                     height="120px" data-bind="click: reviewImg_OnClick"/ name='<?php echo $image ?>'>
                                            <?php }
                                        } else {
                                            echo 'Không có ảnh đính kèm';
                                        } ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <?php } ?>

            </div>
            <div class="tabContent" id="assetReview">
                <div class="review-panel">
                    <div component="form" action="AssetModelReview" method="AddNew" align="float" name="ReviewForm">
                        <div class="form-content">
                            <item text=""
                                  name="ModelID"
                                  default="0"
                                  type="hidden">
                            </item>
                            <item text=""
                                  name="AssetName"
                                  default="<?php echo $assetName; ?>"
                                  type="hidden">
                            </item>
                            <item text=""
                                  name="UserName"
                                  default="<?php echo $current_user->user_login; ?>"
                                  type="hidden">
                            </item>
                            <item text=""
                                  name="Date"
                                  default="<?php echo (new DateTime())->getTimestamp(); ?>"
                                  type="hidden">
                            </item>
                            <item text="" name="Rate" max="5" type="rate" symbol="star"></item>
                            <item width="100%" text="Đánh giá" name="Comment" type="ricktext" height="150px"></item>
                        </div>
                        <div class="form-control-panel">
                            <button type="ajax-submit">Gửi</button>
                        </div>
                    </div>
                </div>
                <?php if (sizeof($reviewData) > 0) { ?>
                    <div class="review-list">
                        <h1 class="main-title">Danh sách đánh giá</h1>
                        <?php
                        if (sizeof($reviewData) > 0) {
                            foreach ($reviewData as $review) {

                                $commentDate = new DateTime();
                                $commentDate->setTimestamp($review->Date);
                                ?>
                                <div class="review">
                                    <div class="owner">
                                        <b><span class="name"><?php echo $review->UserName; ?></span></b>
                                        <span class="date"><?php echo $commentDate->format('d-m-Y H:i:s'); ?></span>
                                        <span component="rate" class="tiny disable" max="5" symbol="star"
                                              value="<?php echo $review->Rate; ?>"></span>
                                    </div>
                                    <div class="comment">
                                        <?php $str = str_replace('<', '&lt;', $review->Comment);
                                         $str = str_replace('>', '&gt;', $review->Comment);
                                        echo $str; ?>
                                    </div>
                                </div>
                                <?php
                            }
                        } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php } else { ?>
        <div class="not-have">Bạn không có quyền xem thông tin của sản phẩm này </div>
    <?php } ?>
    <div component="popup" name="add-notes" width="60%" height="450px">
        <h2>Thông tin ghi chú
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">

            <div component="form" align="float" name="AssetNotesDetail" style="margin-bottom:20px; ">
                <div class="form-content">
                    <item disabled="disabled" text="Tên tài sản" name="AssetName" type="hidden"></item>
                    <item disabled="disabled" text="Người thực hiện" name="UserAction" type="hidden"></item>
                    <item  require text="Loại ghi chú" name="Classify" type="combobox"
                          data-text="Name"
                          data-value="Name"
                          data="foreach: Notes" placeholder="- Chọn một -"></item>
                    <item require text="Vui lòng nhập thông tin ghi chú để chúng tôi có thể biết rõ thêm về tình hình sản phẩm"
                          placeholder="gui:resource-type:detailForm:plhNote"
                          name="comment"
                          type="ricktext"></item>
                    <item multiple="" text="Ảnh sản phẩm hoăc tài liệu đính kèm" name="Gallery" accept="image/*,.pdf,.doc, .docx, .xls, .xlsx"
                          placeholder="Ảnh sản phẩm" imagereview="true" type="file" width="80%"></item>
                  <item width="20%"  type="custom"><button data-bind="click: takeAPhoto" style="margin-top: 9%"><i class="control fa fa-camera-retro"  > </i></button></item>
                </div>

            </div>
        </div>
        <div class="popup-bottom">
            <button data-bind="click: do_SubmitNoteDetail, text: 'Đồng ý'"></button>
            <button data-bind="click: cancel_SubmitNoteDetail, text: 'Hủy'"></button>
        </div>
    </div>
    <div component="popup" name="change-in-use-asset" width="60%" height="530px">
        <h2>Thông tin sản phẩm
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <div component="form" align="float" name="ChangeInUseAssetForm">
                <div class="form-content">
                    <item width="96%" disabled="disabled" text="Tên sản phẩm" name="AssetName" type="text"></item>
                    <item width="96%" disabled="disabled" text="Loại sản phẩm" name="AssetType" type="text"></item>
                    <item width="96%"
                          require text="Tên sản phẩm cha"
                          name="AssetNameParent"
                          type="search"
                          searchhandler="APIData.CMDB.GetListAsset"
                          searchlimit="7"></item>
                    <item width="96%" require text="Ghi chú" name="Comment" type="ricktext" height="200px"></item>
                </div>
            </div>

        </div>
        <div class="popup-bottom">
            <button data-bind="click: btnSubmitChangeInUseAsset_OnClick, text: 'Đồng ý'"></button>
            <button data-bind="click: btnCancelChangeInUseAsset_OnClick, text: 'Hủy'"></button>
        </div>
    </div>
    <div component="popup" name="change-in-use-user" width="60%" height="530px">
        <h2>Thông tin sản phẩm
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <div component="form" align="float" name="ChangeInUseUserForm">
                <div class="form-content">
                    <item width="96%" disabled="disabled" text="Tên tài sản" name="AssetName" type="text"></item>
                    <item width="96%" disabled="disabled" text="Loại tài sản" name="AssetType" type="text"></item>
                    <item width="96%"
                          require text="Tên người dùng"
                          name="UserName"
                          group="InUseTo"
                          type="search"
                          searchhandler="APIData.CMDB.GetListUser"
                          searchlimit="7"
                          searchtemplate="{FullName}"
                          displaytext="FullName"
                          searchvalue="FullName"></item>
                    <item width="96%" require text="Ghi chú" name="Comment" type="ricktext" height="200px" ></item>
                </div>

            </div>
        </div>
        <div class="popup-bottom">
            <button data-bind="click: btnSubmitChangeInUseUser_OnClick, text: 'Đồng ý'"></button>
            <button data-bind="click: btnCancelChangeInUseUser_OnClick, text: 'Hủy'"></button>
        </div>
    </div>
    <div component="popup" name="change-asset-state" width="900px" height="500px">
        <h2>Thông tin trạng thái sản phẩm
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <div component="form" align="float" name="ChangeAssetStateForm">
                <div class="form-content">
                    <item width="96%" disabled="disabled" text="Tên sản phẩm" name="AssetName" type="text"></item>
                    <item width="96%" disabled="disabled" text="Loại sản phẩm" name="AssetType" type="text"></item>
                    <item width="96%" require text="Trạng thái" name="AssetState" type="combobox"
                          data-text="Name"
                          data-value="Name"
                          data="foreach: AssetStates" placeholder="- Chọn một -"></item>
                    <item width="96%" require text="Ghi chú" name="Comment" type="ricktext" height="200px"></item>

                </div>
            </div>
        </div>
        <div class="popup-bottom">
            <button data-bind="click: btnSubmitChangeAssetState_OnClick, text: 'Đồng ý'"></button>
            <button data-bind="click: btnCancelChangeAssetState_OnClick, text: 'Hủy'"></button>
        </div>
    </div>
    <div component="popup" name="change-asset-detail" width="900px" height="500px">
        <h2>Thông tin cấu hình sản phẩm
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <div component="form" align="float" name="ChangeAssetDeatailForm">
                <div class="form-content">
                    <item width="96%" disabled="disabled" text="Tên sản phẩm" name="AssetName" type="text"></item>
                    <item width="96%" disabled="disabled" text="Loại sản phẩm" name="AssetType" type="hidden"></item>
                    <item width="96%" type="ricktext" ></item>
                    <item width="96%" require text="Ghi chú" name="Comment" type="ricktext" height="200px"></item>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
            <button data-bind="click: btnSubmitChangeAssetDetail_OnClick, text: 'Đồng ý'"></button>
            <button data-bind="click: btnCancelChangeAssetDetail_OnClick, text: 'Hủy'"></button>
        </div>
    </div>
    <div component="popup" name="create-request-comment" width="900px" height="330px">
        <h2>Thông tin ghi chú
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">

            <div component="form" align="float" name="CreateRequestForm">

                <div class="form-content">
                    <item width="96%" require text="Vui lòng nhập thông tin ghi chú để chúng tôi có thể biết rõ thêm về tình hình sản phẩm" name="Note" type="ricktext"></item>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
            <button data-bind="click: btnSubmitCreateRequest_OnClick, text: 'Đồng ý'"></button>
            <button data-bind="click: btnCancelCreateRequest_OnClick, text: 'Hủy'"></button>
        </div>
    </div>
    <div component="popup" id="reviewIMG" name="reviewIMG" width="40%" height="40%">
        <h2>Hình ảnh chi tiết
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content" style="padding: 0px">
            <img src="" width="100%" height="auto"></img>

        </div>
    </div>

<?php }); ?>
<?php import('theme.pages.master-page-user'); ?>