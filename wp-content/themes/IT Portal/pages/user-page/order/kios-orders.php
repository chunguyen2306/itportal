<?php
?><?php add_action('content-holder', function () { ?>
    <h1 class="main-title">Đơn hàng chờ xuất kho</h1>
    <!-- <label data-bind="foreach: listWorkFlow">
        <input data-bind="" type="checkbox" value="l-1">
        <span id="listWorkFlow_cb" data-bind="text: DisplayName"></span>
    </label> -->
    <div class="header-control">
        <div class="result-total">
            <div class="addon-control">
                <input placeholder="Tìm kiếm Ex: #1 or laptop" id="txbUserAccount"
                       data-bind="" type="text"/>
                <button id="btnHelpdeskCheckAccount" data-bind="click: $root.search_request"><i class="fa fa-search"></i></button>
                <button class="btn-request-filter option">
                    <i class="fa fa-filter"></i>
                    <options data-bind="foreach: listRequestDefined">
                        <option data-bind="click: $root.filterRequest_OnClick,text: DisplayName"></option>
                    </options>
                </button>
            </div>
            <span>Bạn đang xem  <span data-bind="text: $root.getPageResult()"></span> kết quả</span>
        </div>
    </div>
    <ul class="request-list" data-bind="foreach: {data: requests, as: 'request'}">
        <?php import('theme.layout.request.list-view-out-store'); ?>
    </ul>

    <div component="paging" pageItemLimit="5" name="assetPaging"></div>

    <div id="popup_qr_code" component="popup" name="popReturnAsset" width="18.5%" height="600px">
        <h2>Quét mã QR
            <button class="red close" data-bind="click: hideTicket">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div data-bind="if: renderTickets, css:{show: renderTickets}" class="sys-alert-qr-alert">
            <ul data-bind="foreach: selectedRequest">
                <li id="qr_group" class="swiper-slide ticket" data-bind="attr: {componentid: ID}">
                    <div>
                        <img class="qr" data-bind="attr:{src: base64QR}">
                        <div class="info">
                            <div class="order-info">
                                <div class="id">
                                    <span class="name">Mã đơn hàng: </span>
                                    <span class="value" data-bind="text: ID"></span>
                                </div>
                            </div>
                            <div class="user-info">
                                <div class="name">
                                    <span class="name">Người nhận hàng:</span> <span class="value" data-bind="text: Libs.Workflow.getOwnedField($data)"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div component="paging" pageItemLimit="5" name="qrCodePaging"></div>
        <div class="popup-bottom">
            <button data-bind="click: hideTicket">Đóng</button>
            <button data-bind="click: toCellPhone" style="float: left">Gởi sang điện thoại</button>
        </div>
    </div>

<?php }); ?>

<?php
import('theme.pages.master-page-user');
importJs('jquery.qrcode.min', 'theme.js.ExternalLibs');
?>
<!--
<div data-bind="if: renderTickets, css:{show: renderTickets}" class="sys-alert qr-alert">
    <div class="alert-content swiper-container swiper-container-horizontal">
        <ul data-bind="foreach: selectedRequest" class="swiper-wrapper tickets">
            <li class="swiper-slide ticket" data-bind="attr: {componentid: ma_don_hang}">
                <img class="qr" data-bind="attr:{src: base64QR}">
                <div class="info">
                    <div class="order-info">
                        <div class="id">
                            <span class="name">Mã đơn hàng: </span>
                            <span class="value" data-bind="text: ma_don_hang"></span>
                        </div>
                    </div>
                    <div class="user-info">
                        <div class="name">
                            <span class="name">Người nhận hàng:</span> <span class="value" data-bind="">Nguoi nhan hang</span>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div component="paging" pageItemLimit="5" name="qrCodePaging"></div>

</div>
<div class="alert-control">
    <button data-bind="click: hideTicket">Đóng</button> -->
<!--<button data-bind="click: toCellPhone" style="float: left">Gởi sang điện thoại</button> -->
<!--</div>-->
