<?php add_action('content-holder', function () { ?>
   <div data-bind="if: workflowID() == null">
      <h1 class="main-title">Chọn một loại đơn hàng</h1>
      <div class="page-content">
         <table class="workflows">
            <tbody data-bind="foreach: workflows">
            <tr>
               <td><a data-bind="text: DisplayName, click: $root.selectWorkflow"></a></td>
            </tr>
            </tbody>
         </table>
      </div>
   </div>
   <div data-bind="if: request() || workflowID()">
      <h1 class="main-title">
         <span data-bind="if: request() == null">Tạo đơn hàng </span> <span data-bind="if: request">Chi tiết đơn hàng </span>
         <span data-bind="text: workflowName()"></span> <span data-bind="if: request">#<span data-bind="text: request().ID"></span></span>
      </h1>
      <div class="page-content">
         <span class="group-label">
         <h3>
            <span>Tiến độ xử lý: </span>
            <!-- ko if: currentStepDefinition() -->
            <span class="process-status-name" data-bind="text: currentStepDefinition().DisplayName"></span>
            <!-- /ko -->
            <!--ko if: request()-->
            <!-- ko if: request().RequestStatusID == RequestStatusID.done -->
            <span class="process-status-name" style="background: #1a5384">Đã hoàn tất</span>
            <!-- /ko -->
            <!-- ko if: request().RequestStatusID == RequestStatusID.cancel -->
            <span class="process-status-name" style="background: red">Đã hủy bỏ</span>
            <!-- /ko -->
            <!-- /ko -->
         </h3>
         <div class="workflow-step" data-bind="css:slideByMouseDrag($($element))">
                <ul data-bind="foreach: listStepDefinition, css:slideByMouseDrag($($element))">
                   <li data-bind="css: $root.stepClass($data)" class="step-item">
                       <!-- ko if: $data.listSubRequest == null -->
                        <div class="stack name" data-bind="text: $root.getNewStepDefinitionDisplayName($data)"></div>
                        <div class="stack status">
                            <span></span>
                        </div>
                        <div class="stack owner">
                            <span data-bind="text: step.Owner"></span>
                            <br>
                            <span data-bind="text: $root.getStepDefinitionStateName($data)"></span>
                        </div>
                       <!-- /ko -->
                       <!-- ko foreach: $data.listSubRequest -->
                        <ul>
                           <li class="step-item">
                                <div class="stack name"></div>
                                <div class="stack status workflowName">
                                    <span><a data-bind="text: workflowDisplayName, attr: {href: '/user-page/order/detail/?ID=' + ID}"></a></span>
                                </div>
                                <div class="stack owner"></div>
                           </li>
                           <!-- ko foreach: $data.listStepDefinition -->
                           <li data-bind="css: $root.stepClass($data, $parent.CurrentIndex)" class="step-item">
                                <div class="stack name" data-bind="text: DisplayName"></div>
                                <div class="stack status"><span></span></div>
                                <div class="stack owner">
                                    <span data-bind="text: step.Owner"></span>
                                    <span data-bind="if: step.ActionDate == null && step.Owner">
                                        <br>
                                        <span data-bind="text: $root.intendDurationInString($data)"></span>
                                    </span>
                                </div>
                           </li>
                           <!-- /ko -->
                        </ul>
                       <!-- /ko -->
                   </li>
                </ul>
            </div>
         <div data-bind="if: showProcess, showClass: 'show'" class="process">
            <table id="comment" class="datatable no-control history" width="100%" data-bind="if: request">
                <thead>
                <tr>
                    <th>#</th>
                    <th style="min-width:80px">Người xử lý</th>
                    <th style="min-width:90px">Bước xử lý</th>
                    <th style="min-width:100px">Hành động</th>
                    <th>Ghi chú</th>
                    <th style="min-width:170px">Thời gian</th>
                </tr>
                </thead>
                <tbody data-bind="foreach: listComment">
                <tr>
                    <td data-bind="text: $index() + 1"></td>
                    <td data-bind="text: Owner"></td>
                    <td data-bind="text: $root.getHistoryDefinitionDisplayName($data)"></td>
                    <td data-bind="text: $root.getTaskName($data)"></td>
                    <td data-bind="text: Comment" style="width: 50%"></td>
                    <td data-bind="text: Time"></td>
                </tr>
                <!-- ko if: attachments && attachments.length -->
                <tr>
                    <td></td>
                    <td data-bind="foreach: attachments" class="attachments" colspan="5">
                       <span class="item">
                          <span data-bind="text: name, event: {mousedown: $root.attachmentMouseDown}" class="name" title="Tập tin đính kèm"></span>
                          <i data-bind="click: $root.download" class="fa fa-angle-down download" title="Tải xuống tập tin đính kèm"></i>
                       </span>
                    </td>
                </tr>
                <!-- /ko -->
                </tbody>
               <tbody>
               <tr>
                  <td colspan="5"></td>
                  <td class="history-option hide">
                     <span> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span>
                     <a data-bind="click: $root.processShowChange">Ẩn lịch sử xử lý</a>
                  </td>
               </tr>
               </tbody>
            </table>
         </div>
         <div class="history-option show">
<!--            <i class="fa" data-bind="css: {'fa-square-o': showProcess() == false, 'fa-check-square-o': showProcess()}"></i>-->
<!-- ko ifnot: showProcess -->
            <a data-bind="click: $root.processShowChange">Chi tiết lịch sử xử lý</a>
<!-- /ko -->
         </div>
            <!--         <div class="comment">-->
            <!--            <div data-bind="mention: commentData.comment, nohtml: 1, flushOnEnter: 1, searchMeta: mentionUser" contentEditable="true" placeholder="Thêm một-->
            <!--            ghi chú" class="mention-->
            <!--            comment-input"></div>-->
            <!--            <div class="comment-controls">-->
            <!--               <i data-bind="click: task.pickCam" class="control fa fa-camera-retro"></i>-->
            <!--               <i data-bind="click: task.pickFiles" class="control fa fa-paperclip"></i>-->
            <!--            </div>-->
            <!--            <div data-bind="foreach: commentData.attachments" class="attachments">-->
            <!--               <span class="item">-->
            <!--                  <span data-bind="text: name, event: {mousedown: $root.attachmentMouseDown}" class="name" title="Tập tin đính kèm"></span>-->
            <!--                  <i data-bind="click: $root.download" class="fa fa-angle-down download" title="Tải xuống tập tin đính kèm"></i>-->
            <!--               </span>-->
            <!--            </div>-->
            <!--         </div>-->
      </span>
         <div data-bind=" workflowFields: listFieldDefinition, wfRequestData: $root.requestData" class="field-container">
            <div class="field-item"></div>
         </div>
         <div data-bind="if: request() == null" style="text-align:right">
            <button data-bind="click: createRequest" class="btn-create-request">Đặt hàng</button>
         </div>
         <div data-bind="if: request() && currentStepDefinition()" class="text-right btn-approves">
            <!-- ko if: showButton.pickup -->
            <button data-bind="click: $root.pickupStepApproval">Nhận xử lý</button>
            <!-- /ko -->
            <!-- ko if: showButton.comment -->
            <button data-bind="click: $root.comment">Thêm một ghi chú</button>
            <!-- /ko -->
            <!-- ko if: showButton.requestInfo -->
            <button data-bind="click: $root.requestComment">Yêu cầu thông tin</button>
            <!-- /ko -->
            <!-- ko if: showButton.reCreate -->
            <button data-bind="click: $root.reCreate" class="btn-create-request">Đặt hàng lại</button>
            <!-- /ko -->
            <!-- ko if: showButton.cancel -->
            <button data-bind="click: $root.cancelRequest" class="cancel-request none-approve">Hủy đơn hàng</button>
            <!-- /ko -->
            <!-- ko if: showButton.raise -->
            <button data-bind="click: $root.raiseRequestStep, text: showButton.raise"></button>
            <!-- /ko -->
            <!-- ko if: showButton.reply -->
            <button data-bind="click: $root.replyComment">Trả lời bình luận</button>
            <!-- /ko -->
            <!-- ko if: showButton.approves -->
            <button data-bind="click: $root.approve" class="approve">Đồng ý</button>
            <button data-bind="click: $root.reject" class="none-approve">Từ chối</button>
            <!-- /ko -->
            <!-- ko if: showButton.delegate -->
            <button data-bind="click: $root.entrust" class="none-approve">Ủy quyền</button>
            <!-- /ko -->
         </div>
         <!--  <div data-bind="if: request()" style="text-align:center">-->
         <!--     <h3 data-bind="if: request().RequestStatusID == RequestStatusID.done">Biên bản đã hoàn thành</h3>-->
         <!--     <h3 data-bind="if: request().RequestStatusID == RequestStatusID.cancel">Biên bản đã bị hủy</h3>-->
         <!--  </div>-->
         <div class="history" data-bind="if: history().length">
            <strong>Các đơn hàng gần đây</strong>
            <table class="datatable no-control">
               <thead>
               <tr>
                  <th class="am-title">#</th>
                  <th class="am-title">đơn hàng</th>
                  <th class="am-title">Tiến độ</th>
                  <th class="am-title">Tình trạng</th>
                  <th class="am-title">Lần cập nhật cuối</th>
               </tr>
               </thead>
               <tbody data-bind="foreach: history">
               <tr>
                  <td><a data-bind="html: name, attr: {href: $root.requestLink + '&ID=' + ID}"></a></td>
                  <td><a data-bind="text: des.string, attr: {href: $root.requestLink + '&ID=' + ID}"></a></td>
                  <td><strong data-bind="text: des.progress"></strong></td>
                  <td data-bind="text: des.status"></td>
                  <td data-bind="text: des.lastUpdate"></td>
               </tr>
               </tbody>
            </table>
            <div data-bind="if: history().length > 4"><a>Các đơn hàng khác</a></div>
         </div>
      </div>
      <div style="display: none">
         <div class="search-box" contentEditable="false" data-bind="search: function(){}, searchConfig: { setHandle: setSearchHandle, passNull: 0, displayName: 'temp'}">
            <input class="search-component" style="display: none">
            <button class="search-component" style="display: none"></button>
         </div>
         <div data-bind="if: taskData">
            <div data-bind="if: taskData" id="task-gui">
               <span data-bind="html: taskData().title"></span>
               <!-- ko if: taskData().taskID == TaskID.entrust -->
               <div class="search-box" data-bind="search: taskData().domain, searchAPI: APIData.CMDB.GetListUser, searchConfig: {displayName: 'FullName', valueName: 'AccountName'}">
                  <input class="search-component" data-bind="attr: {placeholder: taskData().placeholder}">
                  <!--  placeholder="Liên hệ IT Helpdesk nếu bạn muốn ủy quyền 1 người không có trong danh sách"-->
                  <button class="search-component" style="display: none"></button>
               </div>
               <br> <span>Lý do</span>
               <!-- /ko -->

               <!-- ko if: taskData().contentType == 'mention' -->
               <div data-bind="mention: taskData().comment, nohtml: 1, searchMeta: mentionUser, attr: {placeholder: taskData().placeholder}" contentEditable="true" class="mention comment-input"></div>
               <!-- /ko -->

               <!-- ko if: taskData().contentType != 'mention' -->
               <textarea data-bind="value: taskData().comment, attr: {placeholder: taskData().placeholder}" style="width: 100%" rows="2"> </textarea>
               <!-- /ko -->

               <div data-bind="foreach: task.attachmentInfos()" class="attachments">
                  <div class="attackment">
                     <input data-bind="value: attachment.name" class="name">
                     <i data-bind="click: $root.task.removeAttackment" class="control fa fa-times"></i>
                     <span data-bind="css: status, text: $data.errorMessage" class="status"></span>
                  </div>
               </div>
               <div class="attachment-controls">
                  <i data-bind="click: task.pickFiles" class="control fa fa-paperclip"></i>
                  <i data-bind="click: task.pickCam" class="control fa fa-camera-retro"></i>
               </div>
            </div>
         </div>
      </div>
   </div>
<?php });
importJs('workflow', 'theme.js.framework');
importJs('workflow-field', 'theme.js.framework');
import('theme.pages.master-page-user');
?>