<?php
importJs('jquery.qrcode.min', 'theme.js.ExternalLibs');
?><?php add_action('content-holder', function () { ?>
   <h1 class="main-title">Order QR on mobile</h1>
<?php }); ?>
   <div class="swiper-container swiper-container-horizontal">
      <ul data-bind="foreach: requests" class="swiper-wrapper tickets">
         <li class="swiper-slide ticket" data-bind="attr: {componentid: ma_don_hang}">
            <img class="qr" data-bind="attr:{src: base64QR}">
            <div class="info">
               <div class="order-info">
                  <div class="id">
                     <span class="name">Mã đơn hàng: </span> <span class="value" data-bind="text: ma_don_hang"></span>
                  </div>
                  <div class="step-display-name">
                     <span class="name">Yêu cầu kho:</span> <span class="value" data-bind="text: ten_buoc"></span>
                  </div>
                  <div class="wf-display-name">
                     <span class="name">Theo quy trình: </span> <span class="value" data-bind="text: ten_quy_trinh"></span>
                  </div>
               </div>
               <div class="user-info">
                  <div class="name">
                     <span class="name">Tên:</span> <span class="value">Tiến. Nguyễn Văn</span>
                  </div>
                  <div class="title">
                     <span class="name">Chức vụ:</span> <span class="value">Associate Developer</span>
                  </div>
                  <div class="location">
                     <span class="name">Địa chỉ:</span> <span class="value">Ghế 191, F14, Fleminton</span>
                  </div>
               </div>
            </div>
         </li>
      </ul>
   </div>
   <div class="swiper-pagination"></div>
<?php
import('theme.pages.master-page-home');
?>