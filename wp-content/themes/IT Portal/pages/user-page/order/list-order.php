<?php
//am_add_script_file(includes_url('js/bootstrap.min.js'));
//wp_enqueue_style("lBootStrap", includes_url('css/bootstrap.min.css'));

?><?php add_action('content-holder', function () { ?>
    <h1 class="main-title" data-bind="text: ((pendingMe())?'Đơn hàng tôi xử lý':'Đơn hàng của tôi')"></h1>

    <div section="list-order">
        <div class="header-control">
            <div class="addon-control">
                <input placeholder="Tìm kiếm: #ID" id="txbUserAccount"
                       data-bind="" type="text"/>
                <button data-bind="click: $root.search_request">
                    <i class="fa fa-search"></i>
                </button>
            </div>
            <div class="result-total">
                <!-- ko if: isOnRemoveFilter -->
                <button data-bind="click: remove_filter">
                    <i class="fa fa-close"></i>
                    Bỏ điều kiện lọc
                </button>
                <!-- /ko -->
                <button data-bind="click: $root.do_FilterRequest">
                    <i class="fa fa-filter"></i>
                </button>

                <!-- ko if: $root.pendingMe() == true -->
                <button title="Xuất ra file Excel">
                    <i class="fa fa-file-excel-o" aria-hidden="true" data-bind="click: $root.ExportExcel_OnClick"></i>
                </button>
                <!-- <input title="Tự động nhận đơn hàng mỗi 30s" data-bind="event: { change: chk_real_time }"
                       component="checkbox"
                       class="nolabel onlycheck" type="checkbox">
                <label title="Tự động nhận đơn hàng mỗi 30s">Thời gian thực</label> -->
                <!-- /ko -->
            </div>
        </div>
        <div class="header-control">
            <div class="head-filter">
                <!-- ko if: $root.pendingMe() == true -->
                <a st="status"
                   data-bind="click: $root.filterControl_OnClick,
               attr: {class: (($root.searchKeyword() === 'not_approve')?'active':''), sk: 'not_approve'}">
                    Chưa xử lý
                </a>
                <a st="status"
                   data-bind="click: $root.filterControl_OnClick,
               attr: {class: (($root.searchKeyword() === 'approved')?'active':''), sk: 'approved'}">
                    Đã xử lý
                </a>
                <!-- /ko -->
                <!-- ko if: $root.pendingMe() == false -->
                <a st="status"
                   data-bind="click: $root.filterControl_OnClick,
               attr: {class: (($root.searchKeyword === 'open')?'active':''), sk: 'open'}">
                    Chưa hoàn tất
                </a>
                <a st="status"
                   data-bind="click: $root.filterControl_OnClick,
               attr: {class: (($root.searchKeyword === 'done')?'active':''), sk: 'done'}">
                    Đã hoàn tất
                </a>
                <a st="status"
                   data-bind="click: $root.filterControl_OnClick,
               attr: {class: (($root.searchKeyword === 'done')?'active':''), sk: 'cancel'}">
                    Đã hủy
                </a>
                <!-- /ko -->
            </div>
            <div class="result-text">
                <span data-bind="text: $root.getPageResult()"></span>
            </div>

        </div>
        <!-- ko if: $root.listRequest().length == 0 -->
        <div class="not-have">Không tìm thấy kết quả theo điều kiện tìm kiếm</div>
        <!-- /ko -->
        <!-- ko if: $root.listRequest().length > 0 -->
        <ul class="request-list" data-bind="foreach: {data: listRequest, as: 'request'}">
            <?php import('theme.layout.request.list-view'); ?>
        </ul>
        <!-- /ko -->

    </div>
    <div component="paging" pageItemLimit="7" name="requestsPaging"></div>
    <div class="hidden" id="exporttoExcel">
        <table>

        </table>
    </div>

    <div component="popup" name="popupFilter" width="20%" height="50%">
        <h2>
            <span>Lọc</span>
            <button class="red close" data-bind="">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div name="popupFilter-body">
            <div name="labelFilter">
                <span>
                <b>Loại đơn hàng hoặc yêu cầu</b>
                </span>
            </div>
            <select id="typeWF" component="combobox" placeholder="- Loại đơn hàng hoặc yêu cầu -" data-bind="value: requestFilter,
                                options: listRequestDefined,
                                optionsText: 'DisplayName',
                                optionsValue: 'ID'">
            </select>
            <!-- ko if: $root.pendingMe() == true -->
            <div name="labelFilter">
                <span>
                <b>Trạng thái đơn hàng</b>
                </span>
            </div>
            <select id="statusWF" component="combobox" placeholder="- Trạng thái -" data-bind="value: requestStatusFilter,
                                options: listRequestStatus,
                                optionsText: 'DisplayName',
                                optionsValue: 'Name'">
            </select>
            <!-- /ko -->
            <div name="dateFilter">
                <div>
                    <b>Ngày tạo đơn hàng</b>
                </div>
                <label>Từ: </label>
                <input type="date"
                       data-bind="value: pendingFilterRequest"/>
                <label> đến:</label>
                <input type="date"
                       data-bind="value: pendingToFilterRequest"/>
            </div>
        </div>
        <div class="popup-bottom">
            <button id="ok_bt" data-bind="event: {click: filterRequest_Apply}">Áp dụng</button>
        </div>
    </div>

<?php }); ?>

<?php
import('theme.pages.master-page-user');
?>