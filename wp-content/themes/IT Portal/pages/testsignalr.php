
<?php add_action('place_holder1', function(){?>

    <script src="https://api-it.vng.com.vn/communication/Scripts/jquery-1.6.4.min.js"></script>
    <script src="https://api-it.vng.com.vn/communication/Scripts/jquery.signalR-2.2.2.min.js"></script>
    <script src="https://api-it.vng.com.vn/communication/SignalR/hubs"></script>

    <div class='log'>

    </div>
    <script>
        var log = function(mess){
            $('.log').append('<span>'+ mess +'</span><br>');
        }
        log('Start setup');
        var hubUrl = "https://api-it.vng.com.vn/communication/SignalR/hubs";
        $.connection.hub.url = hubUrl;
        var chatHub = $.connection.NotificationHub;
        var signalRID = 0;
        log('Finish setup');

        chatHub.client.broadcastMessage = function(message) {
            log('Receive mess '+message);
            me.isConnected = 1;

            var hubMessage = JSON.parse(message);
            var methods = onGets[hubMessage.method];

            //console.log('SIGNALR RECEIVE METHOD: ' + hubMessage.method + ", Data: ");
            //console.log(hubMessage.data);

            if (methods){
                for (var i = 0; i < methods.length; i++)
                    methods[i](hubMessage.message);
            }
        }
        log('Res client');
        log('Start connect');

        $.connection.hub.start().done(function () {
            signalRID = $.connection.hub.id;
            log('Connected');
        });

        var sendToServer = function (method, data, ids, typeOfReciver) {
            log('Data for send to server '+method +' '+ data+' '+ ids+' '+ typeOfReciver);
            if (me.isConnected == false) {
                me.onConnected(function () {
                    me.send(method, data, ids, typeOfReciver);
                });
            }

            var temp = {
                method: method,
                message: data,
                reciverIDs: ids,
                typeOfReciverID: typeOfReciver
            };
            chatHub.server.send(JSON.stringify(temp));
            log('Send to sevrer');
        };

    </script>

<?php }); ?>

<?php import('theme.pages.master-page-home'); ?>


