<?php add_action('am_footer', function() {
    importStyle('sub-page', 'theme.css.frontend');
});?>

<?php add_action('place_holder1', function(){ ?>

<div section="product">
    <div class="left-side">
    <?php generate_product_menu(url_param(1)); ?>
    <?php generate_filter_panel(); ?>
    </div>
    <div class="right-side">
        <section product>
            <div class="head-filter">
                <div class="filter-group">
                    <label>
                        <input component="checkbox" class="nolabel onlychecked" data-bind="checked: Filter, event: { change: chkFil_OnClick }" type="checkbox" value="l-1" >
                        <span>Có sẵn</span>
                    </label>
                    <!--<label>
                        <input data-bind="checked: Filter, event: { change: chkFil_OnClick }" type="checkbox" value="l-2" >
                        <span>Mua trong nước</span>
                    </label>
                    <label>
                        <input data-bind="checked: Filter, event: { change: chkFil_OnClick }" type="checkbox" value="l-3" >
                        <span>Mua nước ngoài</span>
                    </label>!-->
                </div>
                <div class="filter-group">
                    <label>
                        <input component="checkbox" class="nolabel onlychecked" data-bind="checked: Filter, event: { change: chkFil_OnClick }" type="checkbox" value="a-1" >
                        <span>Cần phê duyệt</span>
                    </label>
                    <label>
                        <input component="checkbox" class="nolabel onlychecked" data-bind="checked: Filter, event: { change: chkFil_OnClick }" type="checkbox" value="a-0" >
                        <span>Không cần phê duyệt</span>
                    </label>
                </div>
            </div>
            <div class="product-loading">
                <i clafss="fa fa-mobile"></i>
                <i class="fa fa-laptop"></i>
                <i class="fa fa-desktop"></i>
                <i class="fa fa-camera-retro"></i>
            </div>
            <ul class="product-container" data-bind="foreach: Products">
                <!-- ko if: IsDisplay === 1 -->
                    <!-- ko if: OtherAsset === 1 -->
                    <li class="">
                        <a draggable="false" data-bind="" class="fancybox">
                            <span data-bind="style: { backgroundImage: 'url(\'<?php echo getFullPath('upload.component', '', 'url') ?>' + '/' + Thubmails + '\')' }" class="imgstyle">
                            </span>
                            <span class="info-text" >
                                <span>Sản phẩm khác</span>
                                <span class="short-description">Đặt một sản phẩm không có trong danh sách bên dưới</span>
                            </span>
                        </a>
                        <button class="full" data-bind="click: $root.do_AddOtherAsset,text: i18n('front:product:productlist:btnOrderNow')"></button>
                    </li>
                    <!-- /ko -->
                    <!-- ko if: OtherAsset === 0 -->
                    <li class="">
                        <span class="transferDays" ><span data-bind="text: $root.getTransferDays($data)+'D'"></span></span>
                        <span class="needApproval" title="Cần được phê duyệt" data-bind="attr:{ needapproval: NeedApproval }">Cần <b>phê duyệt</b></span>

                        <a draggable="false" data-bind="attr:{ href: '/detail/'+COMPONENTID}" class="fancybox">
                            <!-- ko if: Stock == 0 -->
                            <span class="outStock">Không có sẵn</span>
                            <!-- /ko -->
                                <span data-bind="style: { backgroundImage: 'url(\'<?php echo getFullPath('upload.component', '', 'url') ?>'+ '/' + ModelID + '/' + Thubmails + '\')' }" class="imgstyle">
                                </span>
                            <span class="info-text" >
                                <span data-bind="text: COMPONENTNAME"></span>
                                <!-- ko if: $data['ProductPageDescription'] !== undefined -->
                                <span class="short-description" data-bind="text: $root.inlineShortDescription(ProductPageDescription)"></span>
                                <!-- /ko -->
                                <!-- ko if: $data['ProductPageDescription'] === undefined && $data['ShortDescription'] !== undefined -->
                                <span class="short-description" data-bind="text: $root.inlineShortDescription(ShortDescription)"></span>
                                <!-- /ko -->
                            </span>


                            <!-- ko if: $data['Cost'] !== undefined -->
                            <!--<span data-bind="text: Cost+' VND'" class="price">0₫</span>-->
                            <!-- /ko -->
                            <!-- ko ifnot: $data['Cost'] !== undefined -->
                            <!--<span class="price">0 VND</span>-->
                            <!-- /ko -->

                            <!--<span class="rating">5</span>-->
                        </a>
                        <button class="full" data-bind="click: $root.do_AddToCart,text: i18n('front:product:productlist:btnOrderNow')"></button>
                    </li>
                    <!-- /ko -->
                <!-- /ko -->
            </ul>
            <div component="paging" pageItemLimit="5" name="productPaging"></div>
        </section>

    </div>

    <div component="popup" name="popOtherAsset" width="500px" height="625px">
        <h2>Thông tin sản phẩm
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <div component="form" align="float" name="formOtherAsset">
                <div class="form-content">
                    <item width="100%" require text="Loại sản phẩm " name="ModelType" type="text"></item>
                    <item width="100%" require text="Tên sản phẩm " name="ModelName" type="text"></item>
                    <item text="Số lượng " require name="Quantity" type="number"></item>
                    <item text="Đơn giá (VND) " name="ReferPrice" type="text" currency="true" ></item>
                    <item text="Link tham khảo " require name="LinkDescription" type="text"></item>
                    <item text="Mục đích sử dụng hoặc ghi chú " require name="Note" type="ricktext"></item>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
            <button id="add_to_cart_bt" data-bind="click: do_CreateRequestOrder">Thêm vào giỏ hàng</button>
            <!--<button class="red close" data-bind="">Đóng</button> -->
        </div>
    </div>

</div>
<?php }); ?>

<?php import('theme.pages.master-page-home'); ?>