<?php
    $_Param = null;
    if (empty($_POST)){
        $_Param = $_GET;
    } else{
        $_Param = $_POST;
    }

    $requestClass = isset($_SERVER['HTTP_U_CLASS'])?$_SERVER['HTTP_U_CLASS']:null;
    $requestType = isset($_SERVER['HTTP_U_TYPE'])?$_SERVER['HTTP_U_TYPE']:null;
    $requestMethod = isset($_SERVER['HTTP_U_METHOD'])?$_SERVER['HTTP_U_METHOD']:null;

    if($requestClass != null && $requestType != null && $requestMethod != null) {
        $ajaxClass = using('plugin.itportal.API.' . $requestClass.'.'.$requestType);
        $result = call_user_func(array($ajaxClass, $requestMethod),$_Param);

        echo is_string($result) ? $result : json_encode($result);
    } else {
        echo 'method not found';
    }
    exit();
?>