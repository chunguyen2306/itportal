<?php add_action('am_footer', function () {
    importStyle('admin-page', 'theme.css.frontend');
    importStyle('TableData', 'theme.css.Component');
}) ?>
<div section="admin">
    <div class="header">
        <a class="logo" href="/">
            <img src="<?php echo getFullPath('theme.img.logo', 'png', 'uri') ?>"/>
        </a>
        <div class="userInfo">
            <div class="info">
                <?php $current_user = wp_get_current_user();

                $adminMenu = using('theme.package.Includes.Admin.MainMenu');
                import('plugin.itportal.Factory.AMUserFactory');
                $userFac = new AMUserFactory();

                $user = $userFac->getOne(array(
                    'filter' => array(
                        array("domainAccount = '" . $current_user->user_login . "'", "")
                    )
                ));

                ?>
                <span class="fullname"><?php echo $current_user->display_name; ?></span>
                <span class="title"><?php echo $user->jobTitle; ?></span>
                <span class="role"></span>
            </div>
            <div class="image">
                <img src=""/>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="left-panel">
            <?php am_add_menu_admin($adminMenu);

            unset($adminMenu);

            ?>
        </div>
        <div class="right-panel">
            <?php do_action('content-holder'); ?>
        </div>
    </div>
    <div class="footer">
    </div>
</div>