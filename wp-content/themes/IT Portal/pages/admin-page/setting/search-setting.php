<?php
/**
 * Created by PhpStorm.
 * User: tientm2
 * Date: 8/23/2018
 * Time: 6:09 PM
 */?>
<?php add_action('content-holder', function(){ ?>
    <p>Chọn danh sách bên dưới để chỉ định các role được phép tìm kiếm toàn bộ thông tin</p>

    <div component="form" align="float" name="searchOptions">
        <div class="form-content">
            <item text=""
                  value="owned-role"
                  name="ID"
                  type="hidden">
            </item>
            <item text=""
                  value="owned-role"
                  name="OptionName"
                  type="hidden">
            </item>
            <item text=""
                  value="home-search"
                  name="OptionCode"
                  type="hidden">
            </item>
            <item width="100%" text="Danh sách quyền" data="options: RolesList, optionsText: 'Name', optionsValue: 'ID'" name="OptionValue" type="select" multiple="" >
            </item>
        </div>
        <div class="form-control-panel">
            <button class="green" data-bind="click: btnSubmit">Lưu</button>
        </div>
    </div>
<?php }); ?>
<?php import('theme.pages.master-page-admin'); ?>
