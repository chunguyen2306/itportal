<?php
/**
 * Created by PhpStorm.
 * User: tientm2
 * Date: 9/1/2018
 * Time: 3:10 PM
 */ ?>
<?php add_action('content-holder', function () { ?>
    <span data-bind="visible: SettingValue().length > 0">
        <button class="btn green" data-bind="click: btnSubmit" >Lưu</button>
        <div class="menu-container" data-bind="foreach: SettingValue">
            <div class="item">
                <select cbtype="combobox" component="combobox"
                        data-bind="value: requestID,
                        options: $root.requests,
                        optionsText: 'DisplayName',
                        optionsValue: 'ID'"></select>
                <select cbtype="combobox" component="combobox"
                        data-bind="value: fieldName, foreach: $root.getRequestField(requestID)">

                    <option data-bind="text: DisplayName, attr:{value:Name}"></option>
                    <!-- ko if: Common.isNotNull($data.listChild) -->
                    <!-- ko foreach: listChild -->
                            <option class="sub" data-bind="text: DisplayName, attr:{value:Name}"></option>
                    <!-- /ko -->
                    <!-- /ko -->
                </select>
            </div>
        </div>
        <span class="add-new" data-bind="click: $root.do_AddItem">Thêm mới</span>
    </span>

    <div class="no-item" data-bind="visible: SettingValue().length === 0">
        <span class="add-new" data-bind="click: $root.do_AddItem">Thêm mới</span>
    </div>
<?php }); ?>
<?php import('theme.pages.master-page-admin'); ?>