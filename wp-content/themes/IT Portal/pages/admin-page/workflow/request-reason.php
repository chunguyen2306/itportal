<?php add_action('content-holder', function () { ?>
    <table class="datatable" width="300px">
        <thead>
        <tr>
            <th width="20%">Tên</th>
            <th>Nhóm quy trình</th>
            <th>Ghi chú</th>
            <th width"30px">
            <button data-bind="click: btnAddNew" component="popup-trigger" popupname="Info" popupstatus="show">
                <i class="fa fa-plus"></i>
            </button>
            </th>
        </tr>
        </thead>
        <tbody data-bind="foreach: Data" >
        <tr>
            <td>
                <span data-bind="text: Name"></span>
            </td>
            <td>
                <span data-bind="text: $root.GetNameFormTypeID(RequestTypeID)"></span>
            </td>
            <td>
                <span data-bind="text: Description"></span>
            </td>
            <td>
                <span name="view_control">
                    <a data-bind="click: $root.btnUpdate">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a data-bind="click: $root.btnDelete">
                        <i class="fa fa-trash"></i>
                    </a>
                </span>
            </td>
        </tr>
        </tbody>
    </table>

    <div component="popup" name="Info" width="30%" height="360px">
        <h2>Điền thông tin
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <div component="form" align="float" name="FormData">
                <div class="form-content">
                    <item text=""
                          name="ID"
                          type="hidden">
                    </item>
                    <item width="100%" text="Tên" name="Name" type="text"></item>
                    <item width="100%" text="Nhóm quy trình" name="RequestTypeID" type="combobox" placeholder="- Chọn một -"
                          data="options: RequestTypes, optionsText: 'Name', optionsValue: 'ID'">
                    </item>
                    <item width="100%" text="Ghi chú" name="Description" type="text"></item>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
            <button data-bind="click: btnSubmit">Đồng ý</button>
            <button data-bind="click: btnCancel" component="popup-trigger" popupname="GrantPermissionPopup" popupstatus="hide" class="red">Hủy</button>
        </div>
    </div>
<?php }); ?>
<?php import('theme.pages.master-page-admin'); ?>