<?php add_action('content-holder', function () {
    import('plugin.itportal.API.Workflow.APIRequestFeatureMapping');

    $requestFeatureMappingAPI = new APIRequestFeatureMapping();
    $pageIndex = isset($_GET['page']) ? $_GET['page'] : 1;
    $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : null;

    $pageIndex -= 1;
    $limit = 15;

    $mapping = array();
    $args = array();
    if($keyword != null){
        $args['keyword'] = $keyword;
    }
    $args['pageIndex'] = $pageIndex;
    $args['limit'] = $limit;
    $mapping = $requestFeatureMappingAPI->GetPaging($args);
    ?>
<div>
    <table name="asset-table" class="datatable">
        <thead>
        <tr>
            <th colspan="2" style="text-align: right;">
                <input type="search" id="txbSearchAsset"
                       data-bind="value: keyword" placeholder="Từ khóa"/>
            </th
        </tr>
        <tr>
            <th>Tên tính năng</th>
            <th>Tên yêu cầu</th>
            <th width="5%">
                <button component="popup-trigger" popupname="create-new-mapping" popupstatus="show" class="btn-add-new">
                    <i class="fa fa-plus"></i>
                </button>
            </th>
        </tr>
        </thead>
        <tbody>

        <?php if(sizeof($mapping['Result']) > 0) {
            foreach ($mapping['Result'] as $item) { ?>
                <tr>
                    <td><?php echo $item->Name; ?></td>
                    <td><?php echo $item->RequestDisplayName; ?></td>
                    <td>
                        <span name="view_control">
                            <a data-mapping="<?php echo $item->Name; ?>" component="popup-trigger" popupname="create-new-mapping" popupstatus="show" data-bind="click: $root.do_enableEdit">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </span>
                    </td>
                </tr>
            <?php }
        } else { ?>
            <tr>
                <td colspan="3">Chưa có dữ liệu</td>
            </tr>
        <?php }?>
        </tbody>
    </table>
    <div component="paging" currentPage="<?php echo $pageIndex + 1; ?>" pageItemLimit="5"
         pageCount="<?php echo $mapping['PageCount']; ?>" name="productPaging">
    </div>

    <div component="popup" name="create-new-mapping" width="60%" height="80%" style="top: 15%;" >
        <h2>Thêm mới tham chiếu
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <div component="form" align="float" name="CreateNewMapping">
                <div class="form-content">
                    <item width="100%" name="ID" type="hidden"></item>
                    <item width="100%" text="Tên tính năng:" name="Name" type="text"></item>
                    <item width="100%"
                          placeholder="- Chọn một -"
                          text="Loại yêu cầu:"
                          require
                          name="RequestID"
                          type="combobox"
                          data="options: RequestTypes,
                                optionsText: 'DisplayName',
                                optionsValue: 'ID',
                                optionsCaption: '- Chọn một -', event: { change : load_requestField }"></item>
                    <item type="custom"
                          text="Dữ liệu:"
                          name="Mapping">
                        <ul class="attr-list" data-bind="template: { name: 'ouput-template', foreach: Output }">

                        </ul>
                        <button class="green" data-bind="click: add_OutputAttr" parent="">
                            <i class="fa fa-plus"></i>
                        </button>
                    </item>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
            <button data-bind="click: do_Submit">Lưu</button>
            <button class="red" data-bind="click: do_Cancel">Hủy</button>
        </div>
    </div>
    <script type="template" id="request-field-template">
        <option data-bind="text: DisplayName, attr:{value:Name}"></option>
        <!-- ko if: Common.isNotNull($data.listChild) -->
            <!-- ko foreach: listChild -->
            <option class="sub" data-bind="text: DisplayName, attr:{value:Name}"></option>
            <!-- /ko -->
        <!-- /ko -->
    </script>
    <script type="template" id="ouput-template">
        <li>
            <input data-bind="value: MappingName, event:{change: $root.on_mappingChange} ," />
            <button class="green" data-bind="click: $root.add_OutputAttr, attr: { parent: $data.FieldName }">
                <i class="fa fa-plus"></i>
            </button>
            <button class="red" data-bind="click: $root.do_DeleteItem, attr: { parent: $data.FieldName }" title="Xóa">
                <i class="fa fa-trash"></i>
            </button>
            <select cbtype="combobox"
                    component="combobox"
                    data-bind="value: FieldName,
                                event:{change: $root.on_mappingChange}, foreach: $root.RequestField.listFieldDefinition" >
                    <option data-bind="text: DisplayName, attr:{value:Name}">
                    </option>
                    <!-- ko if: Common.isNotNull($data.listChild) -->
                        <!-- ko foreach: listChild -->
                        <option class="sub" data-bind="text: DisplayName, attr:{value:Name}">
                        </option>
                        <!-- /ko -->
                    <!-- /ko -->
            </select>
            <ul data-bind="template: { name: 'ouput-template', foreach: $data.Detail }">

            </ul>
        </li>
    </script>
</div>
<?php }); ?>
<?php import('theme.pages.master-page-admin'); ?>