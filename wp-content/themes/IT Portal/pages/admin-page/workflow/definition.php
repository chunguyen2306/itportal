<?php
?>
<?php add_action('content-holder', function () { ?>
    <div class="dim-able">
        <ul class="nav nav-tabs" style="display: none">
            <li data-bind="click: function(){tab(Tabs.data)}, }">
                <a href="#">Thông tin</a>
            </li>
            <li data-bind="click: function(){tab(Tabs.field)}, }"><a href="#">Các trường </a></li>
            <li data-bind="click: function(){tab(Tabs.step)}, }"><a href="#">Các bước </a></li>
        </ul>
        <div style="none">
            <div class="search-box" contentEditable="false"
                 data-bind="search: function(){}, searchConfig: { setHandle: mention.setSearchHandle, passNull: 0, displayName: 'temp'}">
                <input class="search-component" style="display: none">
                <button class="search-component" style="display: none"></button>
                <ul class="search-component"></ul>
            </div>
        </div>
        <div class="save-panel">
            <button data-bind="click:save"><i class="fa fa-floppy-o"> Lưu quy trình</i></button>
        </div>
        <h2 class="under">
            <span data-bind="click: showTab.infoChange" class="main-title">Thông tin quy trình</span>
        </h2>
        <div class="wf-info" data-bind="ifanimation: showTab.info">
            <table class="table-form">
                <tr>
                    <th colspan="2">
                        <strong>Tên quy trình:</strong>
                        <input data-bind="value: data().DisplayName, event:{change: function(){onDisplayNameChange(data())}}"
                               placeholder="Tên quy trình" class="info-request-name">
                    </th>
                    <td>
                        <strong>Mục đích:</strong>
                        <select class="form-control"
                                data-bind="options: $root.listReason, value: data().RequestReasonID, optionsValue: 'ID', optionsText: 'Name'"> </select>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">
                        <strong>Mô tả:</strong>
                        <input placeholder="Mô tả quy trình" data-bind="value: data().Description"
                               class="info-description" rows="11"></input>
                    </th>
                    <td>
                        <strong>Ngày tạo:</strong> <input data-bind="date: data().CreateDate" type="date" readonly
                                                          class="form-control">
                    </td>
                </tr>
                <tr>
                    <th>
                        <strong>Trường người nhận hàng:</strong>
                        <!-- ko if: $root.field.userFields().length -->
                        <select class="input-lable" data-bind="
                  options: $root.field.userFields, value: data().OwnerField, optionsValue: 'Name', optionsText:
                  'DisplayName'"></select>
                        <!-- /ko -->
                        <!-- ko if: !$root.field.userFields().length -->
                        <input value="Mặc định" disabled>
                        <!-- /ko -->
                    </th>
                    <th>
                        <strong>Trường ghi chú đơn hàng:</strong>
                        <!-- ko if: $root.field.userFields().length -->
                        <select class="input-lable" data-bind="
                  options: $root.field.noteFields, value: data().NoteField, optionsValue: 'Name', optionsText:
                  'DisplayName'"></select>
                        <!-- /ko -->
                        <!-- ko if: !$root.field.userFields().length -->
                        <input value="Mặc định" disabled>
                        <!-- /ko -->
                    </th>
                    <td>
                        <strong>Người tạo:</strong> <input data-bind="value: data().Creator" readonly
                                                           class="form-control">
                    </td>
                </tr>
            </table>
        </div>
        <h2 class="under">
            <span data-bind="click: showTab.fieldChange" class="main-title">Các trường dữ liệu</span>
        </h2>
        <div class="detail" data-bind="ifanimation: showTab.field">
            <div data-bind="
                   workflowFields: $root.field.list,
                   wfConfigMode: {
                     listDataType: $root.listDataType,
                     onUpdate: $root.field.onUpdate,
                     onDisplayNameChange: $root.onDisplayNameChange
                 }" class="field-container">
                <div class="field-item"></div>
                <div class="field-item add">
                    <i data-bind="click: field.add" class="fa fa-plus" style=""></i>
                </div>
            </div>

            <!--                <div class="dropdown" style="display: block; float: right">-->
            <!--                    <button class="btn btn-primary btn-md dropdown-toggle" data-toggle="dropdown">-->
            <!--                        <strong>Thêm trường</strong>-->
            <!--                    </button>-->
            <!--                    <ul class="dropdown-menu" aria-labelledby="menu1" data-bind="foreach: listDataType">-->
            <!--                        <li data-bind="click: $root.field.add" role="presentation">-->
            <!--                            <a data-bind="text: 'Trường ' + DisplayName"></a>-->
            <!--                        </li>-->
            <!--                    </ul>-->
            <!--                </div>-->
            <div data-bind="visible: $root.field.editing()" class="dim-background"></div>
        </div>
        <h2 class="under">
            <span data-bind="click: showTab.stepChange" class="main-title">Quy trình duyệt</span>
        </h2>
        <div class="detail" data-bind="ifanimation: showTab.step">
            <div class="workflow-step" data-bind="css:slideByMouseDrag($($element))">
                <ul data-bind="css:slideByMouseDrag($($element))">
                    <!-- ko foreach:step.list-->
                    <li data-bind="css: $root.step.stepStyle($data),click: $root.step.edit" class="step-item">
                        <div class="stack name">
                            <i data-bind="click: $root.step.remove" class="fa fa-trash" aria-hidden="true"></i>
                        </div>
                        <div data-bind="" class="stack status"><span></span></div>
                        <div data-bind="text: DisplayName" class="stack owner"></div>
                    </li>
                    <!-- /ko-->

                    <li data-bind="" class="step-item">
                        <div class="stack name"></div>
                        <div class="stack status">
                            <i data-bind="click:step.add" class="fa fa-plus-circle"></i>
                        </div>
                        <div class="stack owner"></div>
                    </li>
                </ul>
            </div>
            <div data-bind="if: step.editing()" class="container">
                <div class="panel-default">
                    <div class="asset-sub-heading">
                        <strong>Thông tin bước: </strong>
                    </div>
                    <table class="table-form step-info-block">
                        <tr>
                            <td colspan="3">
                                <strong>Tên bước duyệt:</strong>
                                <input data-bind="value: step.editing().DisplayName, event:{change: onDisplayNameChange(step.editing(), step.list())}"
                                       class="info-request-name">
                            </td>
                            <td colspan="4" width="30%">
                                <strong>Được phép nhắc xử lý yêu cầu: </strong> <select class="form-control" data-bind="options: stepCanOption,
                                            optionsValue: 'id',
                                            optionsText: 'name',
                                            value: step.editing().CanRaise,
                                          enable: step.editing().StepIndex"> </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%">
                                <strong>Quyền duyệt: </strong> <select
                                        placeholder="- Chọn quyền được thực hiện bước này -" class="form-control"
                                        data-bind="options: listRole,
                                            optionsCaption: '--',
                                            optionsValue: 'ID',
                                            optionsText: 'Name',
                                            value: step.editing().Role,
                                          enable: step.editing().StepIndex"> </select>
                            </td>
                            <td colspan="2" width="20%" class="step-duration">
                                <strong>Thời lượng:</strong>
                                <!--  ko if: $root.step.editing.durationInString-->
                                <input data-bind="value: step.editing().StepIndex ? step.editing.durationInString : '', enable: step.editing().StepIndex,
                           event:{focus: step.showDurationEditor}" class="form-control no-arrow in-string"
                                       title="Thời lượng cần để xử lý bước">
                                <!-- /ko-->
                                <!--  ko if: $root.step.editing.durationInString() == null--><br>
                                <input data-bind="value: step.editing().MinIntendDuration, event:{blur: step.durationEditorBlur}"
                                       class="time-input" title="Thời lượng tối thiểu để xử lý bước" type="number"
                                       min="0">
                                <span> - </span>
                                <input data-bind="value: step.editing().MaxIntendDuration, event:{blur: step.durationEditorBlur}"
                                       class="time-input" title="Thời lượng tối đa để xử lý bước" type="number" min="0">
                                <!-- /ko-->
                            </td>
                            <td colspan="4">
                                <strong>Được phép yêu cầu thông tin: </strong> <select class="form-control" data-bind="options: stepCanOption,
                                            optionsValue: 'id',
                                            optionsText: 'name',
                                            value: step.editing().CanRequestInfo,
                                          enable: step.editing().StepIndex"> </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Khi bị từ chối: </strong>
                                <select class="form-control"
                                        data-bind=" enable: step.editing.canBack().length > 1,
                                            options: step.editing.canBack,
                                            optionsValue: 'name',
                                            optionsText: 'displayName',
                                            value: step.editing().BackStep">
                                </select>
                            </td>
                            <td colspan="2">
                                <strong>Thứ tự: </strong>
                                <div>
                                    <input component="number"
                                           data-bind="enable: step.editing().StepIndex, value: step.editing().StepOrder, attr: {max: step.editing.maxOrder}"
                                           class="form-control" placeholder="Thứ tự thực hiện của bước" min="2"
                                           type="number">
                                </div>
                            </td>
                            <td colspan="3">
                                <strong>Trạng thái:</strong>
                                <select data-bind="options: listStatus, optionsText: 'DisplayName',
                                              value: step.editing().RequestStatusID, optionsValue: 'ID', optionsCaption: '--' ">
                                </select>
                            </td>
                            <td>
                                <strong>Được phép hủy yêu cầu: </strong> <select class="form-control" data-bind="options: stepCanOption,
                                            optionsValue: 'id',
                                            optionsText: 'name',
                                            value: step.editing().CanCancel"> </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <strong>Tùy biến trường dữ liệu: </strong>
                                <textarea rows="20" class="step-permission-field" placeholder="Click để chọn trường"
                                          data-bind="value: step.fieldCan.description, click: step.showSelectFieldEditorClick"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-default">
                    <div class="asset-sub-heading">
                        <strong>Tự động: </strong>
                    </div>
                    <div class="table-border">
                        <table class="table-form step-action">
                            <tbody data-bind="foreach: $root.action.list">
                            <tr data-bind="if:$data != $root.action.editing()">
                                <th data-bind="click: $root.action.edit" class="colllapse">
                                    <strong data-bind="text: Name"></strong>
                                    <div data-bind="mention: $root.action.getDescription(cf), searchMeta: $root.mention.user"></div>
                                </th>
                            </tr>
                            <tr data-bind="if:$data == $root.action.editing()">
                                <td class="action-head">
                                    <table class="table-form">
                                        <tbody>
                                        <tr data-bind="click: $root.action.edit">
                                            <td class="action-title" colspan="4">
                                                <span data-bind="text: $root.action.getTypeName($data)"></span>
                                                <i data-bind="click: $root.action.remove" class="fa fa-times"></i> <i
                                                        class="fa fa-minus"></i>
                                            </td>
                                        </tr>
                                        <tr class="action-info">
                                            <td onclick="focusNext(this)">Mô tả:</td>
                                            <td><input data-bind="value: Name"></td>
                                            <td class="title">Sự kiện:</td>
                                            <td class="event-select">
                                                <select data-bind="value: EventID, options: WFEvents, optionsText: 'name', optionsValue: 'id'"> </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr data-bind="if:$data == $root.action.editing()">
                                <!--  ko if: ActionTypeID == window.ActionTypeID.notification-->
                                <td>
                                    <table class="table-form action-conf">
                                        <tr>
                                            <td onclick="focusNext(this)">Thông báo qua:</td>
                                            <td class="noti-channels">
                                                <input data-bind="checked: cf.channel.mail" type="checkbox">
                                                <span>Mail</span>
                                                <input data-bind="checked: cf.channel.itPortal" type="checkbox"> <span>IT Portal</span>
                                                <input data-bind="checked: cf.channel.chat" type="checkbox">
                                                <span>Chat</span>
                                                <input data-bind="checked: cf.channel.sms" type="checkbox">
                                                <span>SMS</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td onclick="focusNext(this)">Người nhận:</td>
                                            <td>
                                                <div data-bind="mention: cf.to, searchMeta: $root.mention.user, nohtml: 1"
                                                     contentEditable="true" class="mention"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td onclick="focusNext(this)">Tiêu đề:</td>
                                            <td>
                                                <div data-bind="mention: cf.subject, searchMeta: $root.mention.all, nohtml: 1"
                                                     contentEditable="true" class="mention"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="content" colspan="2">
                                                <div data-bind="mention: cf.body, searchMeta: $root.mention.all"
                                                     contentEditable="true" class="mention notifi-content"
                                                     placeholder="Nội dung thông báo"></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <!-- /ko-->

                                <!-- ko if: ActionTypeID == window.ActionTypeID.assetState-->
                                <td class="action">
                                    <table class="table-form action-conf"
                                           data-bind="init: $root.action.editContextInit">
                                        <tr>
                                            <td onclick="focusNext(this)">Tài sản:</td>
                                            <td>
                                                <div data-bind="mention: cf.list, searchMeta: $root.mention.asset"
                                                     contentEditable="true" class="mention"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td onclick="focusNext(this)">Trạng thái mới:</td>
                                            <td>
                                                <div class="search-box col-sm12"
                                                     data-bind="search:contextState, searchAPI: $root.action.searchAssetState, searchConfig: {displayName: 'name', valueName: 'value'}">
                                                    <input class="search-component input-lable">
                                                    <button class="search-component" style="display: none"></button>
                                                    <ul class="search-component"></ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr data-bind="if: koSelectOwner()">
                                            <td onclick="focusNext(this)">Người sở hữu:</td>
                                            <td>
                                                <select class="input-lable" data-bind="value:cf.Owner, options: $root.field.userFields, optionsValue: 'Name',
                                            optionsText:'DisplayName'"> </select>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <!-- /ko-->

                                <!-- ko if: ActionTypeID == window.ActionTypeID.associateAsset-->
                               <td class="action">
                                  <table class="table-form action-conf"
                                         data-bind="init: $root.action.editContextInit">
                                     <tr>
                                        <td onclick="focusNext(this)">Tài sản:</td>
                                        <td>
                                           <div data-bind="mention: cf.list, searchMeta: $root.mention.asset"
                                                contentEditable="true" class="mention"></div>
                                        </td>
                                     </tr>
                                  </table>
                               </td>
                                <!-- /ko-->

                                <!--  ko if: ActionTypeID == window.ActionTypeID.subOrder -->
                                <td>
                                    <table class="table-form action-conf sub-order">
                                        <tr>
                                            <td>Quy trình con:</td>
                                            <td>
                                                <select class="input-lable" data-bind="value: $root.subWF.wfID,
                                       optionsCaption: '--',
                                       options: $root.listWorkFlow, optionsText:'DisplayName', optionsValue: 'ID'
                                    "></select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Toán tử:</td>
                                            <td>
                                                <select class="input-lable" data-bind="value: cf.condition.operator">
                                                    <option>--</option>
                                                    <option value="1">Ít nhất một biểu thức đúng</option>
                                                    <option value="2">Tất cả các biểu thức đúng</option>
                                                    <option value="3">Ít nhất một biểu thức sai</option>
                                                    <option value="4">Tất cả các biểu thức sai</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Biểu thức:</td>
                                            <td data-bind="if: $root.subWF.editStarted">
                                                <table class="exp">
                                                    <tbody data-bind="foreach: $root.subWF.con.exps">
                                                    <tr data-bind="init: $root.subWF.con.eachExpConextInit">
                                                        <td>
                                                            <select data-bind=" value: obPro,
                                             optionsCaption: '--', enable: $root.subWF.editStarted,
                                                options: $root.subWF.con.pros, optionsText: 'displayName'"> </select>
                                                        </td>
                                                        <td class="mid">
                                                            <select data-bind="value: obCheckType,options: $root.subWF.checkTypes,
                                                optionsCaption: '--', enable: $root.subWF.editStarted,
                                                optionsText: 'name', optionsValue: 'code'"> </select>
                                                        </td>
                                                        <td class="right">
                                                            <select data-bind="value: obCheckValue,options: $context.obCheckValues,
                                                optionsCaption: '--', enable: $root.subWF.editStarted,
                                                optionsText: 'name', optionsValue: 'id'"> </select>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- ko foreach: [{name: 'Mapping trường:', map: $root.subWF.fields},
                                           {name: 'Mapping bước:', map: $root.subWF.steps}]-->
                                        <tr>
                                            <td data-bind="text: name"></td>
                                            <td data-bind="if: $root.subWF.editStarted">
                                                <table class="mapping">
                                                    <tbody data-bind="foreach: map">
                                                    <tr>
                                                        <td data-bind="text: displayName" class=""></td>
                                                        <td>
                                                            <select class="input-lable" data-bind="value: srcItem,
                                                options: options, optionsText:'displayName'
                                             "></select>
                                                        </td>
                                                        <td>
                                                            <!-- ko if: !srcItem().src && !$data.childs-->
                                                            <input data-bind="value: custom" class="input-lable"
                                                                   placeholder="nhập giá trị">
                                                            <!-- /ko-->
                                                            <!-- ko if: srcItem().src || $data.childs-->
                                                            <input disabled class="input-lable">
                                                            <!-- /ko-->
                                                        </td>
                                                    </tr>
                                                    <!-- ko foreach:$data.childs -->
                                                    <tr>
                                                        <td data-bind="text: displayName"></td>
                                                        <td style="width: 50%">
                                                            <select class="input-lable" data-bind="value: srcItem,
                                                options: options, optionsText:'displayName'
                                             "></select>
                                                        </td>
                                                        <td>
                                                            <!-- ko if: !srcItem().src && !$data.childs-->
                                                            <input data-bind="value: custom" class="input-lable"
                                                                   placeholder="nhập giá trị">
                                                            <!-- /ko-->
                                                            <!-- ko if: srcItem().src || $data.childs-->
                                                            <input disabled class="input-lable">
                                                            <!-- /ko-->
                                                        </td>
                                                    </tr>
                                                    <!-- /ko -->
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- /ko-->
                                    </table>

                                </td>
                                <!-- /ko-->
                            </tr>
                            </tbody>
                            <tbody>
                            <tr>
                                <td class="add-action">
                                    <div class="dropdown">
                                        <i class="fa fa-plus"></i>
                                        <ul class="dropdown-menu" data-bind="foreach:$root.action.actions"
                                            aria-labelledby="menu">
                                            <li data-bind="click: function(){$root.action.add(code)}"
                                                role="presentation">
                                                <a data-bind="text: name"></a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div data-bind="if: step.fieldCan.description() === null" style="display: none">
                <table class="datatable no-control field-can" id="selected-field-editor">
                    <thead>
                    <tr>
                        <th>Tên trường</th>
                        <th>Được để trống</th>
                        <th>Được hiển thị</th>
                        <th>Được cập nhật</th>
                        <th>Được chỉnh sửa</th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: field.list">
                    <tr data-bind="init: $root.step.fieldCan.fieldCanInit, can: {}">
                        <td data-bind="text: DisplayName"></td>
                        <td>
                            <input data-bind="enable: $data.RequestDataTypeID != DataTypeID.intendTime, checked: $root.step.fieldCan.can[$data.Name].empty"
                                   type="checkbox"></td>
                        <td><input data-bind="checked: $root.step.fieldCan.can[$data.Name].view" type="checkbox"></td>
                        <td>
                            <input data-bind="enable: $data.RequestDataTypeID != DataTypeID.intendTime, checked: $root.step.fieldCan.can[$data.Name].update"
                                   type="checkbox"></td>
                        <td>
                            <input data-bind="enable: $data.RequestDataTypeID != DataTypeID.intendTime, checked: $root.step.fieldCan.can[$data.Name].edit"
                                   type="checkbox"></td>
                    </tr>
                    <!-- ko foreach: $data.listChild-->
                    <tr>
                        <td data-bind="text: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + DisplayName"></td>
                        <td><input type="checkbox" data-bind="checked: $root.step.fieldCan.can[$data.Name].empty"></td>
                        <td><input type="checkbox" data-bind="checked: $root.step.fieldCan.can[$data.Name].view"></td>
                        <td><input type="checkbox" data-bind="checked: $root.step.fieldCan.can[$data.Name].update"></td>
                        <td><input type="checkbox" data-bind="checked: $root.step.fieldCan.can[$data.Name].edit"></td>
                    </tr>
                    <!-- /ko-->
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="save-panel">
            <button data-bind="click:save"><i class="fa fa-floppy-o"> Lưu quy trình</i></button>
        </div>
    </div>
<?php }); ?>
<?php
importJs('workflow', 'theme.js.framework');
importJs('workflow-field', 'theme.js.framework');
importStyle('workflow', 'theme.css.frontend');
import('theme.pages.master-page-admin');
?>