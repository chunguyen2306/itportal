<?php add_action('content-holder', function () { ?>
   <div class="wrap">
      <h1>Danh sách nhóm model</h1><br/>
      <table class="datatable">
         <thead>
         <tr>
            <th>
               <span>Tên nhóm</span>
            </th>
            <th>
               <a data-bind="attr: {href: $root.editLink}" href="/admin-page/workflow/definition">
                  <button class="green"><i class="fa fa-plus"></i></button>
               </a>
            </th>
         </tr>
         </thead>
         <tbody data-bind="foreach: list">
         <tr>
            <td>
               <a data-bind="attr: {href: $root.editLink + '&ID=' + ID}"> <span><strong data-bind="text: DisplayName"></strong></span> </a>
            </td>
            <td>
               <a data-bind="attr: {href: $root.editLink + '&amp;ID=' + ID}"> <i class="fa fa-pencil"></i> </a>
               <a data-bind="click: $root.remove" type="submit" class="btn btn-flat danger">
                  <i class="fa fa-trash"></i> </a>
            </td>
         </tr>
         </tbody>
      </table>
   </div>

<?php }); ?>
<?php
add_action('am_footer', function () {
    importJs('workflow', 'theme.js.Workflow');
});
import('theme.pages.master-page-admin');
importJs('workflow', 'theme.js.framework');
importCSS('workflow', 'theme.css.frontend');
?>
