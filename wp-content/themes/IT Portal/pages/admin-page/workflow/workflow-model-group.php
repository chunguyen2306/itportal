
<?php
?>
<?php add_action('content-holder', function () { ?>
   <h2>
      <span class="main-title">Nhóm model </span>
   </h2>
    <div data-bind="if: group">
       <input class="group-name" placeholder="Tên nhóm" data-bind="value: group().DisplayName">
       <input class="filter" id="asset-model-filter" placeholder="tìm kiếm với tên tài model">
       <table class="datatable no-control">
          <thead>
          <tr>
             <th>Chọn</th>
             <th>
                Tên model
             </th>
          </tr>
          </thead>
          <tbody data-bind="foreach: assetModelsToSelect">
          <tr>
             <td><input class="selected" data-bind="checked: $data.selected" type="checkbox"></td>
             <td data-bind="text: Name"></td>
          </tr>
          </tbody>
       </table>

       <button class="save" data-bind="click: save">Lưu</button>
    </div>
<?php }); ?>
<?php
import('theme.pages.master-page-admin');
?>