<?php
am_add_script_file(includes_url('js/bootstrap.min.js'));
wp_enqueue_style("lBootStrap", includes_url('css/bootstrap.min.css'));
?>
<?php add_action('content-holder', function(){ ?>

    <h1 class="main-title">Các yêu cầu</h1>

    <table class="datatable" width="100%">
        <thead>
            <tr>
                <th colspan="5">
                    <span>Xem yêu cầu: </span>
                    <select data-bind="options: listRequestStatusID, value: requestStatusIDSelected, optionsValue: 'ID', optionsText: 'DisplayName'"></select>
                </th>
            </tr>
            <tr>
                <th class="am-title">#</th>
                <th class="am-title">Yêu cầu</th>
                <th class="am-title">Tiến độ</th>
                <th class="am-title">Tình trạng</th>
                <th class="am-title">Lần cập nhật cuối</th>
                <th></th>
            </tr>
        </thead>
        <tbody data-bind="foreach: listRequest">
        <tr>
            <td><a data-bind="html: name, attr: {href: $root.requestLink + '&ID=' + ID}"></a></td>
            <td><a data-bind="text: des.string, attr: {href: $root.requestLink + '&ID=' + ID}"></a></td>
            <td> <strong data-bind="text: des.progress"></strong></td>
            <td data-bind="text: des.status"></td>
            <td data-bind="text: des.lastUpdate"> </td>
            <td></td>
        </tr>
        </tbody>
    </table>

<?php }); ?>

<?php
import('theme.pages.master-page-user');
?>