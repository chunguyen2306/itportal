/**
 * Created by PhpStorm.
 * User: AnLac
 * Date: 25/9/2018
 * Time: 3:20 PM
 */
<?php add_action('content-holder', function(){
    $current_user = wp_get_current_user();
    $userRole = get_user_permission($current_user->user_login);
    $view_FeatureName_role = '';
    ?>
    <table class="datatable" width="300px">
        <thead>
        <tr>
            <?php if (check_permisson($view_FeatureName_role)) { ?>
            <th width="20%">FeatureName</th>
            <?php } ?>
            <th width="20%">Tên tính năng</th>
            <th>Mô tả</th>
            <th></th>
        </tr>
        </thead>
        <tbody data-bind="foreach: FeaturesList" >
        <tr>
            <?php if (check_permisson($view_FeatureName_role)) { ?>
            <td>
                <span data-bind="text: FeatureName"></span>
                <input style="display: none" type="text" data-bind="value: FeatureName"/>
            </td>
            <?php } ?>
            <td>
                <span data-bind="text: DisplayName"></span>
                <input style="display: none" type="text" data-bind="value: DisplayName"/>
            </td>
            <td>
                <span data-bind="text: Description"></span>
                <input style="display: none" type="text" data-bind="value: Description"/>
            </td>
            <td>
                <span name="view_control">
                    <a data-bind="click: $root.do_enableEdit">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a data-bind="click: $root.do_delete">
                        <i class="fa fa-trash"></i>
                    </a>
                </span>
                <span style="display: none" name="edit_control">
                    <a data-bind="click: $root.do_saveEdit">
                        <i class="fa fa-check"></i>
                    </a>
                    <a data-bind="click: $root.do_cancelEdit">
                        <i class="fa fa-ban"></i>
                    </a>
                </span>
            </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <?php if (check_permisson($view_FeatureName_role)) { ?>
            <td>
                <input type="text" data-bind="value: Edit().FeatureName"/>
            </td>
            <td>
                <input type="text" data-bind="value: Edit().DisplayName"/>
            </td>
            <td>
                <input type="text" data-bind="value: Edit().Description"/>
            </td>
            <td>
                <button data-bind="click: do_createNew">
                    <i class="fa fa-plus"></i>
                </button>
            </td>
            <?php } ?>
        </tr>
        </tfoot>
    </table>

<?php }); ?>

<?php import('theme.pages.master-page-admin'); ?>