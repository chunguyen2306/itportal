<?php add_action('content-holder', function () { ?>

    <table class="datatable" width="300px">
        <thead>
        <tr>
            <td colspan="3" style="text-align: right">
                <input id="txbSearchDomain" type="search" placeholder="Nhập domain để tìm kiếm">
                <a component="popup-trigger" popupname="ImportCSV" popupstatus="show" class="import csv">Import CSV</a>
            </td>
        </tr>
        </thead>
        <thead>
        <tr>
            <th width="20%">Domain</th>
            <th width="20%">Phòng ban</th>
            <th>Quyền</th>
            <th>
                <button data-bind="click: btnAddNew" component="popup-trigger" popupname="GrantPermissionPopup"
                        popupstatus="show">
                    <i class="fa fa-plus"></i>
                </button>
            </th>
        </tr>
        </thead>
        <tbody data-bind="foreach: UserRoleList">
        <tr>
            <td>
                <span data-bind="text: Domain"></span>
            </td>
            <td>
                <span data-bind="text: Department"></span>
            </td>
            <td>
                <span data-bind="text: PermissionName"></span>
            </td>
            <td>
                <span name="view_control">
                    <a data-bind="click: $root.btnUpdate">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a data-bind="click: $root.btnDelete">
                        <i class="fa fa-trash"></i>
                    </a>
                </span>
            </td>
        </tr>
        </tbody>

        <tfoot>
        <td colspan="4">
            <div component="paging" pageItemLimit="20" name="grantPaging"></div>
        </td>
        </tfoot>
    </table>
    <!-- ko if: UserRoleList().length < 1 -->
    <div class="not-have">Không tìm thấy kết quả theo điều kiện tìm kiếm</div>
    <!-- /ko -->

    <div component="popup" name="ImportCSV" width="50%" height="55%">
        <h2>Import thông tin Deparment Head
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <div component="form" align="float" name="ImportCSVForm">
                <div class="form-content">
                    <item name="File" type="file" text="" multiple="false" accept=".csv"
                          placeholder="File *.csv"></item>
                    <item name="Review" type="custom" text="">
                        <table class="datatable no-control">
                            <thead>
                            <tr>
                                <th style="width: 20%">Domain</th>
                                <th>Department</th>
                            </tr>
                            </thead>
                            <tbody data-bind="foreach: csvData">
                            <tr>
                                <td data-bind="text: Domain"></td>
                                <td data-bind="text: Department"></td>
                            </tr>
                            </tbody>
                        </table>
                    </item>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
            <button data-bind="click: btnImport">Import</button>
            <button component="popup-trigger" popupname="ImportCSV" popupstatus="hide" class="red">Đóng</button>
        </div>
    </div>

    <div component="popup" name="GrantPermissionPopup" width="50%" height="55%">
        <h2>Thông tin quyền hạn
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <div component="form" align="float" name="GrantPermission">
                <div class="form-content">
                    <item text=""
                          name="ID"
                          type="hidden">
                    </item>
                    <item width="100%" text="Tên Domain" name="Domain" type="text"></item>
                    <item width="100%" text="Tên Phòng Ban" name="Department" type="text"></item>
                    <item width="100%" text="Danh sách quyền" data="foreach: RolesList" name="Permission" type="select"
                          multiple="">
                        <option data-bind="text: Name, value: ID"></option>
                    </item>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
            <button data-bind="click: btnSubmit">Đồng ý</button>
            <button data-bind="click: btnCancel" component="popup-trigger" popupname="GrantPermissionPopup"
                    popupstatus="hide" class="red">
                Hủy
            </button>
        </div>
    </div>

<?php }); ?>

<?php import('theme.pages.master-page-admin'); ?>