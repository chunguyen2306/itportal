<?php add_action('content-holder', function(){ ?>

    <table class="datatable" width="300px">
        <thead>
        <tr>
            <th colspan="4" style="text-align: right">
                <input type="search" data-bind="value: Keyword, event: { keyup: Keyword_OnKeyUp }" placeholder="từ khóa">
            </th>
        </tr>
        <tr>
            <th width="20%">Domain</th>
            <th width="20%">Tên đầy đủ</th>
            <th>Tên Quyền</th>
            <th width="20%">Mã nhân viên</th>
            <th>
                <button data-bind="click: btnAddNew" component="popup-trigger" popupname="GrantPermissionPopup" popupstatus="show">
                    <i class="fa fa-plus"></i>
                </button>
            </th>
        </tr>
        </thead>
        <tbody data-bind="foreach: UserRoleList" >
        <tr>
            <td>
                <span data-bind="text: Domain"></span>
            </td>
            <td>
                <span data-bind="text: Name"></span>
            </td>
            <td>
                <span data-bind="text: RoleName"></span>
            </td>
            <td>
                <span data-bind="text: empCode"></span>
            </td>
            <td>
                <span name="view_control">
                    <a data-bind="click: $root.btnUpdate">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a data-bind="click: $root.btnDelete">
                        <i class="fa fa-trash"></i>
                    </a>
                </span>
            </td>
        </tr>
        </tbody>
    </table>

    <div component="popup" name="GrantPermissionPopup" width="50%" height="55%">
        <h2>Thông tin quyền hạn
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <div component="form" align="float" name="GrantPermission">
                <div class="form-content">
                    <item text=""
                          name="ID"
                          type="hidden">
                    </item>
                    <item width="100%" text="Tên domain" name="Domain" type="text"></item>
                    <item width="100%" text="Tên đầy đủ" name="Name" type="text"></item>
                    <item width="100%"
                          text="Quyền"
                          name="RoleID"
                          type="combobox"
                          data="options: RolesList,
                              optionsValue: 'ID',
                              optionsText: 'Name',
                              optionsCaption: '- Chọn một -'">
                    </item>
                    <item width="100%" text="Mã nhân viên" name="empCode" type="text"></item>
                </div>
                <div class="form-control-panel">
                    <button data-bind="click: btnSubmit">Đồng ý</button>
                    <button data-bind="click: btnCancel" component="popup-trigger" popupname="GrantPermissionPopup" popupstatus="hide" class="red">Hủy</button>
                </div>
            </div>
        </div>
    </div>
<?php }); ?>

<?php import('theme.pages.master-page-admin'); ?>