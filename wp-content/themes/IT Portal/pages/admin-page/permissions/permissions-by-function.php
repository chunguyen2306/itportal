/**
 * Created by PhpStorm.
 * User: AnLac
 * Date: 25/9/2018
 * Time: 2:02 PM
 */
<?php add_action('content-holder', function(){ ?>

    <table class="datatable" width="300px">
        <thead>
        <tr>
            <th width="15%">Quyền</th>
            <th width="80%">Tính năng</th>
            <th>
                <button data-bind="click: btnAddNew" component="popup-trigger" popupname="RoleFeaturePopup" popupstatus="show">
                    <i class="fa fa-plus"></i>
                </button>
            </th>
        </tr>
        </thead>
        <tbody data-bind="foreach: FeatureRoleList" >
        <tr>
            <td>
                <span data-bind="text: RoleName"></span>
            </td>
            <td>
                <span data-bind="text: FeatureName"></span>
            </td>
            <td>
                <span name="view_control">
                    <a data-bind="click: $root.btnUpdate">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a data-bind="click: $root.btnDelete">
                        <i class="fa fa-trash"></i>
                    </a>
                </span>
            </td>
        </tr>
        </tbody>
    </table>
    <div component="popup" name="RoleFeaturePopup" width="80%" height="90%">
        <h2>Thông tin quyền hạn
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <div component="form" align="float" name="RoleFeature">

                <div class="form-content">
                    <item text=""
                          name="ID"
                          type="hidden">
                    </item>
                    <item width="100%" text="Quyền" name="Permission" type="combobox" data-text="Name"
                          data-value="Name" data="foreach: RolesList"></item>
                    <item width="100%" text="Danh sách tính năng" data="foreach: OrgFeaturesList, attr: { size: OrgFeaturesListLength}" name="FeatureName" type="select" multiple="" >
                        <optgroup data-bind="attr: { label: i18n(Name)}, foreach: Features">
                            <option data-bind="text: DisplayName, value: ID"></option>
                        </optgroup>
                    </item>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
            <button data-bind="click: btnSubmit">Đồng ý</button>
            <button data-bind="click: btnCancel" component="popup-trigger" popupname="RoleFeaturePopup" popupstatus="hide" class="red">Hủy</button>
        </div>
    </div>

<?php }); ?>

<?php import('theme.pages.master-page-admin'); ?>