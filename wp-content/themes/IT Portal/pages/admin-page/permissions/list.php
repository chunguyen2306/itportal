<?php add_action('content-holder', function(){ ?>

    <table class="datatable" width="300px">
        <thead>
        <tr>
            <th width="20%">Tên</th>
            <th>Ghi chú</th>
            <th></th>
        </tr>
        </thead>
        <tbody data-bind="foreach: RolesList" >
        <tr>
            <td>
                <span data-bind="text: Name"></span>
                <input style="display: none" type="text" data-bind="value: Name"/>
            </td>
            <td>
                <span data-bind="text: Description"></span>
                <input style="display: none" type="text" data-bind="value: Description"/>
            </td>
            <td>
                <span name="view_control">
                    <a data-bind="click: $root.do_enableEdit">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a data-bind="click: $root.do_delete">
                        <i class="fa fa-trash"></i>
                    </a>
                </span>
                <span style="display: none" name="edit_control">
                    <a data-bind="click: $root.do_saveEdit">
                        <i class="fa fa-check"></i>
                    </a>
                    <a data-bind="click: $root.do_cancelEdit">
                        <i class="fa fa-ban"></i>
                    </a>
                </span>
            </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td>
                <input type="text" data-bind="value: Edit().Name"/>
            </td>
            <td>
                <input type="text" data-bind="value: Edit().Description"/>
            </td>
            <td>
                <button data-bind="click: do_createNew">
                    <i class="fa fa-plus"></i>
                </button>
            </td>
        </tr>
        </tfoot>
    </table>

<?php }); ?>

<?php import('theme.pages.master-page-admin'); ?>