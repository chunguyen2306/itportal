<?php add_action('content-holder', function () { ?>
    <h2>
        <span class="main-title">Chi phí khấu hao sản phẩm</span>
    </h2>


    <table class="datatable">
        <thead>
        <tr>
            <th width="32%">
                <span>Nhóm sản phẩm</span>
            </th>
            <th width="32%">
                <span>Số tháng khấu hao tối đa</span>
            </th>
            <th width="32%">
                <span>Mức khấu hao theo năm</span>
            </th>
            <th>
                    <button component="popup-trigger" popupname="edit-group" popupstatus="show" class="green" data-bind="click: do_btnAddNew">
                        <i class="fa fa-plus"></i>
                    </button>
            </th>
        </tr>
        </thead>
        <tbody data-bind="foreach: displaytablediscount">
        <tr>
            <td>
                <span data-bind="text: groupname"></span>
            </td>
            <td>
                <span data-bind="text: maxmonth"></span>
            </td>
            <td>
                <span data-bind="text: $root.getSimpleDiscountPerYear(PercentList)"></span>
            </td>
            <td><a component="popup-trigger" popupname="edit-group" popupstatus="show" data-bind=" click: $root.do_btnUpdate ">
                    <i class="fa fa-pencil"></i>
                </a>
                <a data-bind="click: $root.do_btnDelete" type="submit" class="btn btn-danger">
                    <i class="fa fa-trash"></i> </a>
            </td>
        </tr>
        </tbody>
    </table>


    <div component="popup" name="edit-group" width="90%" height="80%">
        <h2>Thông tin khấu hao theo nhóm sản phẩm
            <button class="red close" data-bind="click: do_CancelSubmit">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">

            <div component="form" align="float" name="DiscountPercentDetail">
                <div class="form-content">
                    <item text="ID" name="id" type="hidden"></item>
                    <item text="Tên nhóm:" name="groupname" type="text"></item>
                    <item text="Số tháng khấu hao tối đa" name="maxmonth" type="text" data="event:{change: getMaxYear}" ></item>

                    <item text="Mức khấu hao theo từ năm" name="PercentList" type="custom">
                        <table class="datatable no-control" name="ArrayPercent">
                            <thead>
                            <tr>
                                <th style="width:50%; text-align: center">
                                    <span>Năm </span>
                                </th>
                                <th  style="width:50%; text-align: center">
                                    <span>Mức phần trăm khấu hao theo năm  </span>
                                </th>
                            </tr>
                            </thead>
                            <tbody data-bind="foreach: arrayPercent">
                            <tr>
                                <th>
                                    <span data-bind="text:Year" Name="Year"></span>
                                </th>
                                <th>
                                    <input data-bind="value:Percent, event: {change: $root.On_DiscountPerYearChange}" name="Percent">
                                </th>
                            </tr>
                            </tbody>
                        </table>
                    </item>
                    <item text="Danh sách loại sản phẩm:" name="assettype" type="custom">
                        <ul style="list-style-type: none;">
                            <li>
                                <label>
                                    <input component="checkbox" class="nolabel" type="checkbox" data-bind="event: { change: do_SelectAll }" name="SelectAll">
                                    <span>Select All</span>
                                </label>
                                <ul class="mapping-item-child" data-bind="foreach: ComponentTypes">
                                    <li>
                                        <label><input component="checkbox" class="nolabel" type="checkbox"
                                                      data-bind="checked: $root.checkedComponentType, value: ID">
                                            <span data-bind="text: Name"></span></label>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </item>
                </div>
            </div>

        </div>
        <div class="popup-bottom">
            <button data-bind="click: do_Submit, text: 'Lưu'"></button>
            <button class="red" data-bind="click: do_CancelSubmit,text: 'Hủy'"></button>
        </div>
    </div>

<?php }) ?>

<?php import('theme.pages.master-page-admin'); ?>