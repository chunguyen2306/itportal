<?php add_action('content-holder', function(){ ?>
    <span data-bind="visible: mainmenus().length > 0">
        <div class="menu-container" data-bind="foreach: $root.sortByIndex(mainmenus())">
            <div class="menu-item" data-bind="template: {name: 'menuItem', data: $data }">
            </div>
            <div data-bind="foreach: $root.sortByIndex($data.Sub)">
                <div class="menu-item" sub="true" data-bind="template: {name: 'subMenuItem', data: $data }">
                </div>
            </div>
        </div>
        <span class="add-new" data-bind="click: $root.do_AddItem, text: i18n('admin-page:gui:personal-menu:add-new-menu:btn:text')"></span>
    </span>

    <div class="no-item" data-bind="visible: mainmenus().length === 0">
        <span class="add-new" data-bind="click: $root.do_AddItem, text: i18n('admin-page:gui:personal-menu:add-new-menu:btn:text')"></span>
    </div>

    <script type="text/html" id="menuItem">
        <label type="text" data-bind="text: Name"></label>
        <button class="" data-bind="click: $root.do_Edit">
            <i class="fa fa-pencil"></i>
        </button>
        <button class="red" data-bind="click: $root.do_Remove">
            <i class="fa fa-trash"></i>
        </button>
        <button class="green" data-bind="click: $root.do_AddItem">
            <i class="fa fa-plus"></i>
        </button>
    </script>

    <script type="text/html" id="subMenuItem">
        <label type="text" data-bind="text: Name"></label>
        <button class="" data-bind="click: $root.do_Edit">
            <i class="fa fa-pencil"></i>
        </button>
        <button class="red" data-bind="click: $root.do_Remove">
            <i class="fa fa-trash"></i>
        </button>
    </script>

    <div component="popup" name="infos" style="top: 5%">
        <h2>Thông tin menu
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
            <div class="popup-content">
            <div component="form" align="float" name="MenuInfoForm">
                <div class="form-content">
                    <item width="100%" text="Tên menu:" name="Name" type="text"></item>
                    <item width="100%" text="Đường dẫn:" name="Link" type="text"></item>
                    <item width="100%" text="Phân Quyền:" name="Role" type="text"></item>
                    <item width="100%" text="Index:" name="Index" type="number"></item>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
            <button data-bind="click: do_SubmitInfo, text: i18n('all-page:ok:btn:text')"></button>
            <button class="red"
                    data-bind="click: do_CancelInfo, text: i18n('all-page:cancel:btn:text')"></button>
        </div>
    </div>

<?php }) ?>
<?php import('theme.pages.master-page-admin'); ?>