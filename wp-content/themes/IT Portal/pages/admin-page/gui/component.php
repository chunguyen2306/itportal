<?php add_action('am_footer', function () {
    importStyle('skin.min', 'theme.js.tinymce.skins.lightgray');
    importJs('tinymce.min', 'theme.js.tinymce');
    ?>
<?php }) ?>
<?php add_action('content-holder', function () { ?>
    <table class="datatable" component="tabledata" name="tableDetails" width="100%">
        <thead>
        <tr>
            <th colspan="3">
                <input id="txbKeyword" type="search" placeholder="Từ khóa tìm kiếm">
            </th>
        </tr>
        <tr>
            <th width="1%"></th>
            <th data-bind="text: i18n('gui:component:tbData:lblName')" width="50%"></th>
            <th data-bind="text: i18n('gui:component:tbData:lblComponentType')" width="20%"></th>
            <th width="7%">
                <!--<button data-bind="click: do_createNew">
                        <i class="fa fa-plus"></i>
                    </button>-->
            </th>
        </tr>
        </thead>
        <tbody data-bind="foreach: Components">
        <tr>
            <td>
                <img width="120px" data-bind="attr: { src: $root.getComponentImage(COMPONENTID) }"/>
            </td>
            <td>
                <span data-bind="text: COMPONENTNAME"></span>
            </td>
            <td>
                <span data-bind="text: COMPONENTTYPENAME"></span>
            </td>
            <td>
                <span name="view_control">
                    <a data-bind="click: $root.do_enableEdit">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a data-bind="click: $root.do_delete">
                        <i class="fa fa-trash"></i>
                    </a>
                </span>
            </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="3">
                <div component="paging" pageItemLimit="5" name="componentPaging"></div>
            </td>
        </tr>
        </tfoot>
    </table>
    <div component="popup" name="details" width="90%" height="90%" style="top: 5%">
        <h2>Thông tin model sản phẩm
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <div component="form" align="float" name="ComponentDetails">
                <div class="form-content">
                    <item text=""
                          name="ModelID"
                          type="hidden">
                    </item>
                    <item text=""
                          name="TypeID"
                          type="hidden">
                    </item>
                    <item text=""
                          name="ID"
                          type="hidden">
                    </item>
                    <item width="100%" text="Tên model:" name="ComponentName" type="text"></item>
                    <div class="left-side" style="width:70%">
                        <item text="Mô tả trang sản phẩm" name="ProductPageDescription" type="editor"
                              height="150px"></item>
                        <item text="Mô tả ngắn" name="ShortDescription" type="editor" height="200px"></item>
                        <item text="Mô tả chi tiết" name="Description" type="editor" height="600px"></item>
                        <item text="Hướng dẫn" name="ManualGuide" type="editor" height="600px"></item>
                    </div>
                    <div class="right-side" style="width:30%">
                        <item style="width:96%" text="Nhà cung cấp" name="ManufacturerName" type="hidden"></item>
                        <item style="width:96%" text="Giá" name="Cost" type="text"></item>
                        <item style="width:96%" text="Trạng thái" name="IsDisplay" type="checkbox"
                              class="cb-cus-supply"></item>
                        <item style="width:96%" text="Thời gian mua hàng(ngày)" name="BuyDays" type="text"></item>
                        <item style="width:96%" text="Thời gian giao hàng(ngày)" name="TransferDays" type="text"></item>
                        <item style="width:96%" text="Cần phê duyệt" name="NeedApproval" type="checkbox"
                              class="cb-cus-vn"></item>
                        <item style="width:96%" text="Năm thay thế" name="ReNewYear" type="text"></item>
                        <item multiple="" text="Ảnh sản phẩm" name="Gallery" accept="image/*"
                              placeholder="Ảnh sản phẩm" imagereview="true" type="file"></item>
                        <item text="Ảnh đại diện" name="Thubmails" type="imagepicker" field="Gallery"></item>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
            <button data-bind="click: do_SubmitDetail, text: i18n('gui:resource-type:detailForm:btnSubmit')"></button>
            <button class="red"
                    data-bind="click: do_CancelDetail, text: i18n('gui:resource-type:detailForm:btnCancel')"></button>
        </div>
    </div>

    <!--<div component="popup" name="details" >
        <h2>Thông tin model tài sản
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <div component="form" name="ComponentDetails">
                <div class="form-content">
                    <item text=""
                          name="COMPONENTID"
                          type="hidden">
                    </item>
                    <item text="gui:component:tbData:lblName"
                          placeholder="gui:component:detailForm:plhName"
                          name="COMPONENTNAME"
                          type="text"></item>
                    <item placeholder="gui:component:detailForm:plhCompobox"
                          text="gui:resource-type:tbData:lblComponentType"
                          name="COMPONENTTYPEID"
                          type="combobox"
                          data-text="COMPONENTTYPENAME"
                          data-value="COMPONENTTYPEID"
                          data="foreach: ComponentTypes"></item>
                    <item text="gui:component:tbData:lblManufactureName"
                          placeholder="gui:component:detailForm:plhManufactureName"
                          name="MANUFACTURERNAME"
                          type="text"></item>
                    <item text="gui:component:tbData:lblComment"
                          placeholder="gui:component:detailForm:plhComment"
                          name="COMMENTS"
                          type="ricktext"></item>
                    <item text="gui:component:tbData:lblImage"
                          multiple=""
                          placeholder="gui:component:detailForm:plhImage"
                          name="IMAGE"
                          type="file"
                          accept="image/*"></item>
                </div>
                <div class="form-control-panel">
                    <button data-bind="click: do_SubmitDetail, text: i18n('gui:resource-type:detailForm:btnSubmit')"></button>
                    <button class="red" data-bind="click: do_CancelDetail, text: i18n('gui:resource-type:detailForm:btnCancel')"></button>
                </div>
            </div>
        </div>
    </div>-->

<?php }) ?>
<?php import('theme.pages.master-page-admin'); ?>