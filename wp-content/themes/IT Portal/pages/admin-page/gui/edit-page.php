<?php add_action('content-holder', function () {
    $APIAsset = using('plugin.itportal.API.Options.EditPage');
    $file = file_get_contents('wp-content/themes/IT Portal/pages/home.php', true);

    //$files = array_filter(glob('wp-content/themes/IT Portal/pages/*'),'is_file');
    $dir = 'wp-content/themes/IT Portal/pages/';
    $files = array_slice(scandir($dir), 2);
    ?>
    <span>CHỌN TRANG CHỈNH SỬA</span>
    <div class="form">

        <form method="POST">
            <select id="combobox_page" component="combobox" class="Theme_GP Theme_GP_Modified SelectUI1">
                <option value="master-page-home.php">master-page-home.php</option>
                <option value="home.php">home.php</option>
                <option value="detail.php">detail.php</option>
            </select>
        </form>
    </div>

    <div class="menu-container" data-bind="foreach: pagecontent">
        <div class="menu-item" data-bind="template: {name: 'pagecontentItem', data: $data }"></div>
    </div>

    <script type="text/html" id="pagecontentItem">
        <span data-bind="text: name"></span>
        <input type="text" data-bind="value: value">
        <button data-bind="click: $root.do_SaveEdit">Lưu</button>
        <button class="red" data-bind="click: $root.do_CancelEditNew">Hủy</button>
    </script>
    <?php

}) ?>
<?php
/*
 * <form method="POST">
        <select id="combobox_page" >
            <?php foreach ($files as $key => $value) {
                if (preg_match('*\.*', $value)) { ?>
                    <option value="<?php echo($key); ?>"> <?php echo($value); }?> </option>
            <?php }
            ?>
        </select>
    </form>
 *
 * */
?>
<?php import('theme.pages.master-page-admin'); ?>

