<?php add_action('content-holder', function(){ ?>
    <table class="datatable" component="tabledata" name="tableDetails" width="100%">
        <thead>
            <tr>
                <th colspan="5" style="text-align: right">
                    <input id="txbKeyword" type="search" placeholder="Từ khóa tìm kiếm">
                </th>
            </tr>
            <tr>
                <th width="100px"></th>
                <th data-bind="text: i18n('gui:resource-type:tbData:lblName')"></th>
                <th data-bind="text: i18n('gui:resource-type:tbData:lblProductType')"></th>
                <th data-bind="text: i18n('gui:resource-type:tbData:lblProductCategory')"></th>
                <th data-bind="text: i18n('gui:resource-type:tbData:lblCode')"></th>
                <th>
                    <!--<button data-bind="click: do_createNew">
                        <i class="fa fa-plus"></i>
                    </button>-->
                </th>
            </tr>
        </thead>
        <tbody data-bind="foreach: ComponentTypes" >
            <tr>
                <td><img width="30px" data-bind="attr: { src: $root.getPathOfDescription(DESCRIPTION, 'Icon') }" /></td>
                <td>
                    <span data-bind="text: COMPONENTTYPENAME"></span>
                </td>
                <td>
                    <span data-bind="text: TYPE"></span>
                </td>
                <td>
                    <span data-bind="text: CATEGORY"></span>
                </td>
                <td>
                    <span data-bind="text: $root.getPathOfDescription(DESCRIPTION, 'Code')"></span>
                </td>
                <td>
                    <span name="view_control">
                        <a data-bind="click: $root.do_enableEdit">
                            <i class="fa fa-pencil"></i>
                        </a>
                    </span>
                </td>
            </tr>
        </tbody>
    </table>
    <div component="popup" name="details" >
        <h2>Thông tin loại tài sản
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <div component="form" name="ResourceTypeDetails">
                <div class="form-content">
                    <item text=""
                          name="COMPONENTTYPEID"
                          type="hidden">
                    </item>
                    <item text="gui:resource-type:tbData:lblName"
                          placeholder="gui:resource-type:detailForm:plhName"
                          name="COMPONENTTYPENAME"
                          type="text"></item>
                    <item placeholder="gui:resource-type:detailForm:plhCompobox"
                          text="gui:resource-type:tbData:lblProductType"
                          name="RESOURCETYPEID"
                          type="combobox"
                          data="options: ResourceTypes,
                                optionsText: 'TYPE',
                                optionsValue: 'RESOURCETYPEID',
                                optionsCaption: '- Chọn một -'"></item>
                    <item placeholder="gui:resource-type:detailForm:plhCompobox"
                          text="gui:resource-type:tbData:lblProductCategory"
                          type="combobox"
                          name="RESOURCECATEGORYID"
                          data="options: ResourceCategories,
                                optionsText: 'CATEGORY',
                                optionsValue: 'RESOURCECATEGORYID',
                                optionsCaption: '- Chọn một -'"></item>
                    <item text="gui:resource-type:tbData:lblCode"
                          placeholder="gui:resource-type:detailForm:plhCode"
                          name="CODE"
                          type="text"></item>
                    <item text="gui:resource-type:tbData:lblNote"
                          placeholder="gui:resource-type:detailForm:plhNote"
                          name="NOTE"
                          type="ricktext"></item>
                    <item text="gui:resource-type:tbData:lblIcon"
                          placeholder="gui:resource-type:detailForm:plhIcon"
                          name="ICON"
                          type="file"
                          accept="image/*"></item>
                </div>
            </div>



        </div>
        <div class="popup-bottom">
            <button data-bind="click: do_SubmitDetail, text: i18n('gui:resource-type:detailForm:btnSubmit')"></button>
            <button class="red" data-bind="click: do_CancelDetail, text: i18n('gui:resource-type:detailForm:btnCancel')"></button>
        </div>

    </div>

<?php }) ?>
<?php import('theme.pages.master-page-admin'); ?>