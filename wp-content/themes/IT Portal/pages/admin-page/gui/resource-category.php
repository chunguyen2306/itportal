<?php add_action('content-holder', function(){ ?>
    <table class="datatable" width="300px">
        <thead>
        <tr>
            <th data-bind="text: i18n('gui:product-category:tbData:lblName')"></th>
            <th></th>
        </tr>
        </thead>
        <tbody data-bind="foreach: ResourceCategories" >
        <tr>
            <td>
                <span data-bind="text: CATEGORY"></span>
                <input style="display: none" type="text" data-bind="value: CATEGORY"/>
            </td>
            <td>
                <span name="view_control">
                    <a data-bind="click: $root.do_enableEdit">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a class="disabled">
                        <i class="fa fa-trash"></i>
                    </a>
                </span>
                <span style="display: none" name="edit_control">
                    <a data-bind="click: $root.do_saveEdit">
                        <i class="fa fa-check"></i>
                    </a>
                    <a data-bind="click: $root.do_cancelEdit">
                        <i class="fa fa-ban"></i>
                    </a>
                </span>
            </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td>
                <input type="text" data-bind="value: Edit().CATEGORY"/>
            </td>
            <td>
                <button data-bind="click: do_createNew">
                    <i class="fa fa-plus"></i>
                </button>
            </td>
        </tr>
        </tfoot>
    </table>
<?php }) ?>
<?php import('theme.pages.master-page-admin'); ?>