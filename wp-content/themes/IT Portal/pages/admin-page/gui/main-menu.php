<?php add_action('content-holder', function(){ ?>
    <span data-bind="visible: mainmenus().length > 0">
        <div class="menu-container" data-bind="foreach: mainmenus">
            <div class="menu-item" data-bind="template: {name: 'menuItem', data: $data }">
            </div>
            <div data-bind="foreach: $data.Childs">
                <div class="menu-item" sub="true" data-bind="template: {name: 'menuItem', data: $data }">
                </div>
            </div>
        </div>
        <span class="add-new" data-bind="click: $root.do_AddItem">Thêm mới menu</span>
    </span>

    <div class="no-item" data-bind="visible: mainmenus().length === 0">
        <span class="add-new" data-bind="click: $root.do_AddItem">Thêm mới menu</span>
    </div>

    <div component="popup" name="popMappingResourceType" >
        <h2>Tham chiếu CMDB
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <ul class="mapping-item" data-bind="foreach: ResourceTypes">
                <li>
                <label><input data-bind="event: { change: $root.do_SelectAll }, attr: {name: TYPE}" type="checkbox" component="checkbox" class="nolabel"> <span data-bind="text: TYPE"></span></label>
                    <ul class="mapping-item-child" data-bind="foreach: ComponentTypes">
                        <li>
                            <label>
                                <input data-bind="checked: $root.MappingSelected,
                                value: COMPONENTTYPEID,
                                attr: { parent: $parent.TYPE}" name="MappingSelected" type="checkbox" component="checkbox" class="nolabel"> <span data-bind="text: COMPONENTTYPENAME"></span></label>
                        </li>
                    </ul>
                </li>
            </ul>
            <button data-bind="click: do_UpdateMapping">Đồng ý</button>
        </div>
    </div>

    <div component="popup" name="popMappingPostCategory" >
        <h2>Tham chiếu loại bài viết
            <button class="red close">
                <i class="fa fa-close"></i>
            </button>
        </h2>
        <div class="popup-content">
            <ul class="mapping-item" data-bind="foreach: PostCategory">
                <li>
                    <label>
                        <input data-bind="checked: $root.MappingSelected, value: cat_ID, attr:{ name: category_nicename}"
                               type="checkbox" component="checkbox" class="nolabel"> <span data-bind="text: name"></span>
                    </label>
                </li>
            </ul>
            <button data-bind="click: do_UpdateMapping">Đồng ý</button>
        </div>
    </div>

    <label id="fileIconSelected" name="fileIconSelected" type="input-file" style="display: none" placeholder="a">
        <input data-bind="event: { change: do_UpdateIcon }" cbType="file" type="file" />
    </label>
    <script type="text/html" id="menuItem">
        <img data-bind="click: $root.do_SelectIcon, attr: { src: $root.getIconPath(Icon) }" title="Thay đổi menu Icon" />
        <!-- Add New -->
        <!-- ko if: ID === 0 -->
        <input type="text" data-bind="value: Name">
        <select placeholder="- Chọn loại -" component="combobox"
                data-bind="value: TypeID,
                            options: $root.MenuTypes,
                            optionsText: 'Name',
                            optionsValue: 'ID',
                            optionsCaption: '- Chọn loại-'" >
        </select>
        <button data-bind="click: $root.do_AddNew">Lưu</button>
        <button class="red" data-bind="click: $root.do_CancelAddNew">Hủy</button>
        <!-- /ko -->
        <!-- Edit -->
        <!-- ko if: ID < 0 -->
        <input type="text" data-bind="value: Name">
        <select placeholder="- Chọn loại -" component="combobox"
                data-bind="value: TypeID, options: $root.MenuTypes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: '- Chọn loại-'" >
        </select>
        <button data-bind="click: $root.do_SaveEdit">Lưu</button>
        <button class="red" data-bind="click: $root.do_CancelEditNew">Hủy</button>
        <!-- /ko -->
        <!-- View -->
        <!-- ko if: ID > 0 -->
        <span data-bind="text: Name"></span>
        <button class="" data-bind="click: $root.do_EditItem, attr:{ Parent: ID }" title="Chỉnh sửa">
            <i class="fa fa-pencil"></i>
        </button>
        <button class="red" data-bind="click: $root.do_DeleteItem, attr:{ Parent: ID }" title="Xóa">
            <i class="fa fa-trash"></i>
        </button>
        <button data-bind="click: $root.do_MappingItem, attr:{ Parent: ID }">
            <i class="fa fa-exchange"></i>
        </button>
        <!-- ko if: ID === Parent -->
        <button class="green" data-bind="click: $root.do_AddItem, attr:{ Parent: ID }" title="Tham chiếu">
            <i class="fa fa-plus"></i>
        </button>
        <!-- /ko -->
        <!-- /ko -->

    </script>
<?php }) ?>
<?php import('theme.pages.master-page-admin'); ?>