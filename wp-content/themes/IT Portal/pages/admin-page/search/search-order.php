<?php
add_action('content-holder', function () { ?>
    <div class="workflow dim-able">
        <h1 class="main-title">
            <span>Cấu hình tìm kiếm</span>
        </h1>
        <span class="group-label">
            <div>
                <div class="con">
                    <strong>Ngày bắt đầu</strong>
                    <input data-bind="date: condition()._start" type="date">
                </div>
                <div class="con">
                    <strong>Ngày kết thúc</strong>
                    <input data-bind="date: condition()._end" type="date">
                </div>
                <div class="con btn-times">
                    <button>Tuần này</button>
                    <button>Tháng này</button>
                    <button data-bind="click: time.recent7">7 ngày gần đây</button>
                    <button data-bind="click: time.recent30">30 ngày gần đây</button>
                    <button data-bind="click: time.unlimit">Không giới hạn thời gian</button>
                </div>
            </div>
            <div class="conditions" data-bind="if: 0">
                <div class="con active">
                    <strong id="k" class="name"></strong>
                    <input data-bind="value: DisplayName">
                </div>
            </div>

                <div style="float: right;" class="con">
                    <button data-bind="click: getResult">Xem kết quả</button>
                </div>
        </span>
        <table class="datatable no-control">
            <thead>
            <tr data-bind="foreach: columns">
                <!-- ko if: !$data.hiden-->
                <th class="edit-able">
                    <strong data-bind="text: DisplayName" class="name"></strong>
                </th>
                <!-- /ko-->
            </tr>
            </thead>
            <tbody data-bind="foreach: {data: results, as: 'row'}">
            <tr data-bind="foreach: $root.columns">
                <!-- ko if: !$data.hiden-->
                <!-- ko if: Array.isArray(row[Name])-->
                <td data-bind="foreach: row[Name]">
                    <div data-bind="text: $data"></div>
                </td>
                <!-- /ko-->
                <!-- ko if: Array.isArray(row[Name]) === false -->
                <td data-bind="text: row[Name]"></td>
                <!-- /ko-->
                <!-- /ko-->
            </tr>
            </tbody>
        </table>
        <div class="dim-background" data-bind="visible: 0"></div>
    </div>
<?php }); ?>

<?php
importJs('workflow', 'theme.js.framework');
import('theme.pages.master-page-admin');
?>