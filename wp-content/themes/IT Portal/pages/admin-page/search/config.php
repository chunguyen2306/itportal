<?php
add_action('content-holder', function () { ?>
    <div class="workflow dim-able">

       <h1 class="main-title">
          <span>Các quy trình được áp dụng</span>
       </h1>
       <table class="datatable no-control workflows">
          <tbody data-bind="foreach: $data.workflows">
             <tr>
                <td><input type="checkbox" data-bind="checked: $data.selected"></td>
                <td data-bind="text: DisplayName"></td>
             </tr>
          </tbody>
       </table>

        <h1 class="main-title">
            <span>Cấu hình lọc yêu cầu</span>
        </h1>
        <input class="search-name" data-bind="value: data().DisplayName" placeholder="Nhập tên mẫu lọc"/>
        <span class="group-label">
            <div>
                <div class="con">
                    <strong>Ngày bắt đầu</strong>
                    <input type="datetime-local" disabled>
                </div>
                <div class="con">
                    <strong>Ngày kết thúc</strong>
                    <input type="datetime-local" disabled>
                </div>
                <div class="con btn-times">
                    <button>Tuần này</button>
                    <button>Tháng này</button>
                    <button>7 ngày gần đây</button>
                    <button>30 ngày gần đây</button>
                    <button>Không giới hạn thời gian</button>
                </div>
            </div>
            <div class="filters" data-bind="foreach: filter.list">
                <div class="con edit-able">
                    <strong data-bind="text: DisplayName, event:{input: $root.onDisplayNameInput, blur: $root.onDisplayNameBlur}" class="name"
                            placeholder="Tên điều kiện lọc" contenteditable="true"></strong>
                    <input readonly>
                    <div class="task">
                        <i data-bind="click: function(){$root.filter.add(0, $data)}" class="fa fa-plus on-hover"></i>
                        <i data-bind="click: function(){$root.filter.add($data)}" class="fa fa-plus on-hover"></i>
                        <i data-bind="click: $root.filter.remove" class="fa fa-times on-hover on-active"></i>
                        <i data-bind="click: $root.filter.doneEdit" class="fa fa-check on-active"></i>
                        <i data-bind="click: $root.filter.edit" class="fa fa-pencil on-hover"></i>
                    </div>
                    <div data-bind="if: $root.filter.editing() == $data" class="attr">
                        <table class="table-form">
                            <tbody>
                            <tr>
                                <td>
                                    <strong>Cột: </strong>
                                </td>
                                <td>
                                    <select data-bind="value: $data.ColumnName, options: $root.column.list, optionsText: 'DisplayName', optionsValue: 'Name'"></select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- ko if:filter.list().length === 0-->
                <div class="con edit-able">
                    <button data-bind="click: filter.add">Thêm điều kiện lọc</button>
                </div>
            <!-- /ko -->
        </span>
        <table class="datatable no-control">
            <thead>
            <tr data-bind="foreach: column.list">
                <!-- ko if: !$data.hiden-->
                <th class="edit-able" data-bind="style:{width: style.width}">
                    <strong data-bind="text: DisplayName, event:{input: $root.onDisplayNameInput, blur: $root.onDisplayNameBlur}" class="name"
                            placeholder="Tên cột" contenteditable="true"></strong>
                    <div class="task">
                        <i data-bind="click: function(){$root.column.add(0, $data)}" class="fa fa-plus on-hover"></i>
                        <i data-bind="click: function(){$root.column.add($data)}" class="fa fa-plus on-hover"></i>
                        <i data-bind="click: $root.column.remove" class="fa fa-times on-hover on-active"></i>
                        <i data-bind="click: $root.column.doneEdit" class="fa fa-check on-active"></i>
                        <i data-bind="click: $root.column.edit" class="fa fa-pencil on-hover"></i>
                    </div>
                </th>
                <!-- /ko-->
            </tr>
            </thead>
            <tbody data-bind="foreach: new Array(5)">
            <tr data-bind="foreach: $root.column.list">
                <!-- ko if: !$data.hiden-->
                <td>&nbsp;</td>
                <!-- /ko-->
            </tr>
            </tbody>
        </table>
        <div class="dim-background" data-bind="click: filter.doneEdit, style:{display: filter.editing() ? 'unset' : ''} "></div>
        <div data-bind="if: column.editing">
            <div id="column-editor" data-bind="if: column.editing">
                <div data-bind="text: column.editing().DisplayName" class="title"></div>
                <table class="table-form cf">
                    <tbody>
                    <tr>
                        <td>
                            <strong>Độ rộng: </strong>
                        </td>
                        <td>
                            <input data-bind="value: column.editing().style.width">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Nguồn dữ liệu: </strong>
                        </td>
                        <td>
                            <select data-bind="value: $root.source.editing.typeID, options: $root.column.types, optionsText: 'Name', optionsValue: 'ID'"></select>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Thuộc tính: </strong></td>
                        <td>
                           <!-- ko if: source.editing.renderType.isSelect -->
                           <select data-bind="value: column.editing().src, options: source.list, optionsText: 'name', optionsValue: 'attr', optionsCaption: ''"></select>
                           <!-- /ko -->
                           <!-- ko if: source.editing.renderType.isCustom -->
                           <div data-bind="mention: column.editing().src, searchMeta: $root.mention.user" contentEditable="true" class="mention">
                           </div>
                           <!-- /ko -->
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table data-bind="if: source.editing.renderType.isTable" class="table-form src">
                    <tbody data-bind="foreach: source.list">
                    <tr data-bind="css:{'wf-unselected': !wf.selected}">
                        <td>
                            <span data-bind="text: $data.wf.DisplayName"></span>
                        </td>
                        <td>
                            <select data-bind="value: selected, options: options, optionsText: 'DisplayName', optionsCaption: '', disable: !wf.selected">
                            </select>
                        </td>
                        <td><button data-bind="click: $root.source.autoAnother, disable: !wf.selected" class="auto-another" title="Tự động thiết lập cho các quy trình khác giống như quy trình này">Chuẩn</button></td>
                    </tr>
                    </tbody>
                </table>
             </div>
        </div>
       <div style="text-align: right; margin-top: 30px;">
          <button data-bind="click: save"><i class="fa fa-floppy-o">Lưu cấu hình</i></button>
       </div>
    </div>
<?php }); ?>

<?php
importJs('workflow', 'theme.js.framework');
import('theme.pages.master-page-admin');
?>