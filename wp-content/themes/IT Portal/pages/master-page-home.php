<?php add_action('am_footer', function () {
    importStyle('home-page', 'theme.css.frontend');
    //importStyle('screen', 'theme.themes.dndTheme.css-full');
    importStyle('swiper.min', 'theme.js.ExternalLibs.Swiper.css');
    importJs('swiper.min', 'theme.js.ExternalLibs.Swiper.js');
    importResource('theme.Module.master-page-home');
}) ?>

<?php
$searchType = isset($_GET['st']) ? $_GET['st'] : 'product';
$searchKeyword = isset($_GET['sk']) ? $_GET['sk'] : '';
$searchCustomType = isset($_GET['sk']) ? $_GET['sct'] : '';
?>

<div section="home">
    <div class="header">
        <div class="header-wrapper">
            <a class="logo" href="/">
            </a>
            <div class="site-search" id="site-search">
                <form component="homeSearch" class="form" action="/search" accept-charset="UTF-8"
                      enctype="application/x-www-form-urlencoded">
                    <fieldset class="select">
                        <label class="rankingLabel">
                            <select value="<?php echo isset($_GET['st']) ? $_GET['st'] : ''; ?>"
                                    component="combobox" id="st" name="st" rel="#"
                                    class="Theme_GP Theme_GP_Modified SelectUI1" name="select">
                                <option value="product" <?php echo ($_GET['st'] === 'product') ? 'selected="selected"' : ''; ?>
                                        data-bind="text: i18n('master-page-home:header:search-option:option-1:cbx:text'),
                                                    attr: {tips: i18n('master-page-home:header:search-option:option-1:cbx:tips')}">
                                </option>
                                <option value="request" <?php echo ($_GET['st'] === 'request') ? 'selected="selected"' : ''; ?>
                                        data-bind="text: i18n('master-page-home:header:search-option:option-2:cbx:text'),
                                                    attr: {tips: i18n('master-page-home:header:search-option:option-2:cbx:tips')}">
                                </option>
                                <option value="guild" <?php echo ($_GET['st'] === 'guild') ? 'selected="selected"' : ''; ?>
                                        data-bind="text: i18n('master-page-home:header:search-option:option-3:cbx:text'),
                                                    attr: {tips: i18n('master-page-home:header:search-option:option-3:cbx:tips')}">
                                </option>
                            </select>
                        </label>
                    </fieldset>
                    <fieldset class="search search-box">
                        <input value="<?php echo isset($_GET['sk']) ? $_GET['sk'] : ''; ?>"
                               class="site-search__field search-component" id="sk" name="sk" required
                               data-bind="attr: {placeholder: i18n('master-page-home:header:search-option:option-1:cbx:tips')}"/>
                        <button class="search-component" class="site-search__button  search-component">
                            <i class="fa fa-search"></i>
                        </button>
                    </fieldset>
                    <input value="<?php echo isset($_GET['sct']) ? $_GET['sct'] : ''; ?>" type="hidden" id="sct"
                           value="model">
                </form>
            </div>

            <div class="app-function">
                <ul>
                    <li class="my-request">
                        <a id="tracking_orders"
                            data-bind="text: i18n('master-page-home:header:track-orders:btn:text'),
                                        attr: {title: i18n('master-page-home:header:track-orders:btn:title')}">
                        </a>
                    </li>
                    <li class="noti" data-bind="click: Notification.showChange" clicktoshow>
                        <a data-bind="text: i18n('master-page-home:header:notifications:btn:text'),
                                        attr: {title: i18n('master-page-home:header:notifications:btn:title')}"
                           clicktoshow>
                        </a>
                        <!-- ko if: Notification.unSeenNumber-->
                        <span data-bind="html: Notification.unSeenNumber" class="item-number" clicktoshow></span>
                        <!-- /ko -->
                        <div class="notis" data-bind="if: Notification.isShowing" id="notis" style="display: none">
                            <!-- ko if: Notification.isLoading-->
                            <div class="item noti-loading"></div>
                            <!-- /ko -->
                            <!-- ko foreach: Notification.list -->
                            <a class="item" data-bind="click: Notification.toNoti, css:{read: IsRead == 1}, attr: {href: Notification.orderLink +
                                                    'ID=' + Content}"
                               target="_blank">
                                <div class="img"></div>
                                <div data-bind="text: Title" class="title"></div>
                                <div data-bind="text: Time" class="time"></div>
                            </a>
                            <!-- /ko -->
                            <!-- ko if: (Notification.isLoading() == false && Notification.list().length == 0) -->
                            <div class="item no-item">
                                <span data-bind="text: i18n('master-page-home:header:notifications-box:no-notification:lbl:text')"></span>
                            </div>
                            <!-- /ko -->
                            <!-- ko if: Notification.isLoading() || Notification.list().length -->
                            <a class="item to-all"
                               data-bind="click: Notification.toAllNoti, attr: {href: Notification.allNotiLink},
                                            text: i18n('master-page-home:header:notifications-box:see-all-notification:lbl:text')">
                            </a>
                            <!-- /ko -->
                        </div>
                    </li>
                    <li class="cart">
                        <a href="/cart" title="Giỏ hàng"
                           data-bind="text: i18n('master-page-home:header:cart:btn:text'),
                                        attr: {title: i18n('master-page-home:header:cart:btn:title')}">
                        </a>
                        <span component="shopcart-noti" class="item-number">0</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="nav-wrapper">
        <nav id="main-nav">
            <div class="main-nav--inner">
                <div class="main-nav-toggle">
                    <div class="blank">

                    </div>

                    <?php generate_home_menu(); ?>
                </div>
                <div class="group-right">
                    <?php if (is_user_logged_in()) { ?>
                        <div class="user-info" id="user-info">
                            <?php
                            $current_user = wp_get_current_user();
                            $userInfo = get_user_info($current_user->user_login);
                            $userIco = get_user_default_icon($userInfo);
                            ?>
                            <a class="user-icon has-sub" href="javascript:;"
                               style="background-color: <?php echo $userIco['color']; ?>">
                                <?php echo $userIco['name'] ?>
                            </a>
                            <p class="user-name"><?php echo $userInfo->fullName; ?></p>
                            <?php

                            $userMenu = using('theme.package.Includes.User.MainMenu');

                            am_add_menu_admin($userMenu, 'user-info__list');

                            unset($userMenu);

                            ?>
                        </div>
                    <?php } else {
                        $redirect_to = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>
                        <a class="btn-login" href="/wp-login.php?redirect_to=<?php echo $redirect_to; ?>&external=cas"
                            data-bind="text: i18n('master-page-home:menu:login:lbl:text')">
                        </a>
                    <?php } ?>
                </div>

            </div>
        </nav>
    </div>
    <div class="content">
        <?php do_action('place_holder1'); ?>
    </div>
    <div class="footer">
        <div class="wapper">
            <div class="left-side">
                <div class="span-column">
                    <!--<h3 data-bind="text: 'IT Portal'"></h3>
                    <h3 pagecontent="Tieudecot3" data-bind="text: Footer.pagecontentMPH().Tieudecot3"></h3>-->
                    <ul>
                        <li>
                            <a target="_blank" pagecontent="Vitri1cot3Text" pagecontent="Vitri1cot3Url"
                               data-bind="text: Footer.pagecontentMPH().Vitri1cot3Text, attr: { href: Footer.pagecontentMPH().Vitri1cot3Url }">
                            </a>
                        </li>
                        <li>
                            <a target="_blank" pagecontent="Vitri2cot3Text" pagecontent="Vitri2cot3Url"
                               data-bind="text: Footer.pagecontentMPH().Vitri2cot3Text, attr: { href: Footer.pagecontentMPH().Vitri2cot3Url }">
                            </a>
                        </li>
                        <li>
                            <a target="_blank" pagecontent="Vitri3cot3Text" pagecontent="Vitri3cot3Url"
                               data-bind="text: Footer.pagecontentMPH().Vitri3cot3Text, attr: { href: Footer.pagecontentMPH().Vitri3cot3Url }">
                            </a>
                        </li>
                        <li>
                            <a target="_blank" pagecontent="Vitri4cot3Text" pagecontent="Vitri4cot3Url"
                               data-bind="text: Footer.pagecontentMPH().Vitri4cot3Text, attr: { href: Footer.pagecontentMPH().Vitri4cot3Url }">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="span-column icon-column">
                    <a href="mailto:helpdesk@vng.com.vn">
                        <img src="<?php echo getFullPath('theme', '', 'url') ?>/img/icon/envelope 128 grey.png">
                        <span>MAIL</span>
                        <span>Helpdesk@vng.com.vn</span>
                    </a>
                </div>
                <div class="span-column icon-column">
                    <a target="_blank" href="https://www.yammer.com/vng.com.vn/#/threads/inGroup?type=in_group&feedId=11177278">
                        <img src="<?php echo getFullPath('theme', '', 'url') ?>/img/icon/yammer.png">
                        <span>YAMMER</span>
                        <span>Kết nối với chúng tôi</span>
                    </a>
                </div>
                <div class="span-column icon-column">
                    <a target="_blank" href="https://chat.zalo.me/?c=6604210483285179274">
                        <img src="<?php echo getFullPath('theme', '', 'url') ?>/img/icon/smartphone-call 128 grey.png">
                        <span>HOTLINE</span>
                        <span>+84 93 4088 588</span>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>
