<?php add_action('am_footer', function() {
    importStyle('sub-page', 'theme.css.frontend');
});?>

<?php add_action('place_holder1', function(){
    $pathInfo = split('/', urldecode($_SERVER['REQUEST_URI']), 6);
    $menuName = $pathInfo[2];
    $cateName = $pathInfo[3];
    $postName = $pathInfo[4];
    ?>
    <div section="document">
        <div class="left-side">
            <?php
                generate_document_list($menuName, $cateName, $postName);
             ?>
        </div>
        <div class="right-side">
            <?php get_post_content($menuName, $cateName, $postName); ?>
        </div>
    </div>
<?php }); ?>
<?php import('theme.pages.master-page-home'); ?>