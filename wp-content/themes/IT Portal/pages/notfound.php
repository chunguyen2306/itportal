<?php add_action('am_footer', function() {
    importStyle('sub-page', 'theme.css.frontend');
});?>

<?php add_action('place_holder1', function(){ ?>

    <div section="notfound">
        <img src="<?php echo get_template_directory_uri(); ?>/img/notfound/notfound_07.png">
        <img src="<?php echo get_template_directory_uri(); ?>/img/notfound/notfound_01.png">
        <img src="<?php echo get_template_directory_uri(); ?>/img/notfound/notfound_03.png">
        <img src="<?php echo get_template_directory_uri(); ?>/img/notfound/notfound_01.png">
        <img src="<?php echo get_template_directory_uri(); ?>/img/notfound/notfound_05.png">
        <p>Not Found Page You Requested</p>
    </div>

<?php });?>

<?php import('theme.pages.master-page-home'); ?>