<?php add_action('am_footer', function () {
    importStyle('sub-page', 'theme.css.frontend');
}); ?>
<?php add_action('place_holder1', function () { ?>
<div section="cart" class='cart'>
    <div>
        <span class="main-title" data-bind="text: i18n('cart:left-side:title:lbl:text')"></span>
        <!-- <a data-bind="click: do_GoBack, text: i18n('cart:left-side:title:back-home:btn:text')"></a> -->
    </div>
    <div class="block-step">
        <div data-bind="attr: { class: step() >= 0 ? 'one-step active':'one-step' }">
            <div class="title-step">Sản phẩm</div>
            <div class="content-step">
                <div class="horizontal"></div>
                <div class="number-step" data-bind='click: ChangeToFirstPage'><i class="fa fa-plus"></i></div>

            </div>
        </div>
        <div data-bind="attr: { class: step() >= 1 ? 'one-step active':'one-step' }">
            <div class="title-step">Thông tin đơn hàng</div>
            <div class="content-step">
                <div class="horizontal"></div>
                <div class="number-step" data-bind='click: ChangeToSecondPage'><i class="fa fa-undo"></i></div>
            </div>
        </div>
        <div data-bind="attr: { class: step() >= 2 ? 'one-step active':'one-step' }">
            <div class="title-step">Xác nhận đơn hàng</div>
            <div class="content-step">
                <div class="horizontal"></div>
                <div class="number-step" data-bind='click: ChangeToThirdPage'><i class="fa fa-check"></i></div>
            </div>
        </div>
    </div>

    <div class="left-side">
        <?php
            generateProductStep()?>
        <?php 
            generateInformationStep()?>
        <?php 
        generateLeftStep3()?>
    </div>
    <div class="right-side">
        <div data-bind='if: step() == 0'>
            <div class="item-line">
                <div class="header" data-bind="text: i18n('cart:right-side:title-input:user-domain:lbl:text')"></div>
                <div>
                    <div class="addon-control">
                        <input id="search_user_text_box" type="text" data-bind="value: full_name">
                        <button id="search_user_bt"><i class="fa fa-search"></i>
                        </button>
                    </div>
                    <?php if (check_permisson('workflow.request.create_for_new_staff')) { ?>
                    <br>
                    <input id="is_new_staff_cbx" type="checkbox" component="checkbox" class="nolabel onlycheck">
                    <span>Nhân viên mới</span>
                    <br>
                    <?php } ?>
                </div>
            </div>

        </div>
        <div data-bind='if: step() == 1'>
            <div class="item-line">
                <div class="header">Địa chỉ nhận hàng</div>
                <div class="form-line" style="width: 100%;">
                    <input id="p_seat" cbtype="rickText" require="" data-bind="value: seat" />
                </div>

            </div>
        </div>
        <div data-bind='if: step() == 2'>
            <div class="item-line">
                <div class="header">Thông tin đơn hàng</div>
                <div class="block-item">
                    <div class='color-edit'>Số lượng (tạm tính)</div>
                    <div class='color-content'><span data-bind='text: countItem'></span> sản phẩm</div>
                </div>
                <div class="block-item" data-bind='if: dayOfPurchase() > 0'>
                    <div class='color-edit'>Thời gian giao hàng</div>
                    <div class='color-content'>khoảng <span data-bind='text: dayOfPurchase'></span> ngày</div>
                </div>
            </div>
            <div class="item-line">
                <div class="header">Thông tin vận chuyển</div>
                <div class="edit-tranform">
                    <div class='color-edit'>Giao hàng đến</div>
                    <div class='edit' data-bind='click: ChangeToSecondPage'>Chỉnh sửa</div>
                </div>
                <div>
                    <div class='title block-right' data-bind="text: full_name"></div>
                    <div class='note-new-item' data-bind="text: seat"></div>
                </div>

            </div>
            <div class="item-line">
                <!-- <div class="header">Thông tin khác</div> -->
                <div class="edit-tranform">
                    <div class='color-edit'>Ghi chú</div>
                    <div class='edit' data-bind='click: ChangeToSecondPage'>Chỉnh sửa</div>
                </div>
                <div class="block-item" data-bind='if: checkItemInCart()'>
                    <div class='color-edit'>Đơn đặt mới</div>
                    <div class='color-content' data-bind='text: noteNewItem'></div>
                </div>
                <div class="block-item" data-bind='if: checkEditItem()'>
                    <div class='color-edit'>Đơn đổi trả/ báo mất</div>
                    <div class='color-content' data-bind='text: noteEditItem'></div>
                </div>
            </div>
        </div>
        <button class="button-next-cart" data-bind="click: changeNextStep">
            <!-- ko ifnot: step() == 2 -->
            <span data-bind="text: i18n('cart:right-side:next-step:btn:text')"></span>
            <!-- /ko -->
            <!-- ko if: step() == 2 -->
            <span data-bind="text: i18n('cart:right-side:submit-order:btn:text')"></span>
            <!-- /ko -->
        </button>

    </div>
    <!-- cpnguyen -->
    <?php 
        generateAssetPopUp()?>

</div>
<?php }) ?>
<?php import('theme.pages.master-page-home'); ?>