<?php add_action('am_footer', function () {
    importStyle('sub-page', 'theme.css.frontend');
    importJs('drag_to_scroll', 'theme.js.ExternalLibs');
    importJs('tinymce.min', 'theme.js.tinymce');
}); ?>

<?php add_action('place_holder1', function () {
    import('plugin.itportal.Factory.AMAssetModelDetailFactory');
    import('plugin.itportal.Factory.AMAssetModelReviewFactory');
    import('plugin.itportal.API.CMDB.Component');

    $cmdbAPI = new Component();
    $detailFac = new AMAssetModelDetailFactory();
    $reviewFac = new AMAssetModelReviewFactory();

    $pathInfo = split('\/', $_SERVER['REQUEST_URI'], 3);
    $modelID = $pathInfo[2];
    $componentJson = $cmdbAPI->GetByID(array(
        id => (double)$modelID
    ));
    $quantityInHand = json_decode($cmdbAPI->GetQuantityInHand(array(
        modelID => (double)$modelID
    )))->Result;
    $current_user = wp_get_current_user();
    $userRole = get_user_permission($current_user->user_login);
    $component = json_decode($componentJson)->Result;

    $data = $detailFac->getByModelID((double)$modelID);

    $reviewData = $reviewFac->getByModelID((double)$modelID);
    $reviewRateAverage = 0;

    foreach ($reviewData as $review) {
        $reviewRateAverage += $review->Rate;
    }
    $reviewRateAverage = $reviewRateAverage / (sizeof($reviewData) > 0 ? sizeof($reviewData) : 1);

    if ($data == null) {
        $data = (object)array(
            Thubmails => '',
            Gallery => array(),
            ShortDescription => '',
            Description => '',
            Cost => 0,
            IsDisplay => 0,
            TransferDays=>2,
            BuyDays=>15
        );
    } else {
        $data->Gallery = json_decode($data->Gallery);
    }

    if($quantityInHand === 0){
        $data->TransferDays = $data->BuyDays + $data->TransferDays;
    }

    $relationAsset = json_decode($cmdbAPI->GetProductSlideByType([
        'type' => json_encode(array($component->COMPONENTTYPENAME))
    ]))->Result;

    if(sizeof($relationAsset) > 0){
        $relationAsset = $relationAsset[0]->Components;
    }

    // Lay so luong TON KHO, DETAIL cho cac SAN PHAM TUONG TU
    foreach ($relationAsset as $value) {
        $stock = json_decode($cmdbAPI->GetQuantityInHand([
            'modelID' => $value->COMPONENTID
        ]));
        $value->Stock = $stock->Result;
        $detailProduct = $detailFac->getByModelID((double)$value->COMPONENTID);
        $value->Detail = $detailProduct;
    }
    // LOAI BO san pham dang xem khoi list SAN PHAM TUONG TU
    foreach ($relationAsset as $key => $value) {
        if ($value->COMPONENTID == $modelID) {
            unset($relationAsset[$key]);
        }
        if ($value->Detail->IsDisplay === 0) {
            unset($relationAsset[$key]);
        }
    }

    $mockSlideProduct = [
        (object)[
            COMPONENTTYPENAME => 'Sản phẩm tương tự',
            Components => $relationAsset
        ]
    ]


    ?>
    <?php if (check_permisson('content.edit')) { ?>
        <div class="admin-control">
            <a target="_blank" href="/admin-page/gui/component/?quickedit=<?php echo $component->COMPONENTID; ?>">
                <i class="fa fa-pencil"></i> Edit
            </a>
        </div>
    <?php } ?>
    <div section="detail">
        <div class="product-gallery">
            <span class="needApproval" title="Cần được phê duyệt"
                  needapproval="<?php echo $data->NeedApproval ?>">Cần <b>phê duyệt</b></span>
            <span class="transferDays"
                  title="Giao hàng trong <?php echo $data->TransferDays ?> Ngày">
                <span><?php echo $data->TransferDays ?>D</span>
            </span>
            <div class="selected-photo">
                <span class="preview" thubmails="<?php echo $data->Thubmails; ?>"></span>
            </div>
            <?php if($quantityInHand == 0 || $data->IsDisplay == 0) {
                if($data->IsDisplay == 0) {?>
                    <span class="outStock">Ngừng cung cấp</span>
                <?php } else {?>
                    <span class="outStock">Hàng không có sẵn</span>
                <?php } ?>
            <?php } ?>
            <div class="all-photo drags_to_croll">
                <?php if (sizeof($data->Gallery) > 0) {
                    foreach ($data->Gallery as $item) { ?>
                        <img src="<?php echo getFullPath('upload.component.' . $modelID, '', 'url') . $item ?>"
                             alt="<?php echo $item; ?>"/>
                    <?php }
                } ?>
            </div>
        </div>
        <div class="product-short-description">
            <h1>
                <?php echo $component->COMPONENTNAME; ?>
                <span class="small disable" component="rate" max="5" symbol="star"
                      value="<?php echo $reviewRateAverage; ?>"></span>
            </h1>
            <!--<div class="cost">
                <?php //echo number_format($data->Cost) . ' VND'; ?>
            </div>-->
            <div class="quantityInHand">
                <?php echo 'Tồn kho: ' . $quantityInHand; ?>
            </div>
            <div class="information">
                <?php echo $data->ShortDescription; ?>
            </div>
            <div class="other-info">
                <?php if ($data->ReNewYear != 0) { ?>
                    <label class="label-checkbox" checked>
                        <?php echo 'Dự kiến thay thế năm ' . $data->ReNewYear; ?>
                    </label>
                <?php } ?>
            </div>

            <div class="other-info">
                <!--
                <b>Giá trị từ:</b>
                <span class="price"> -->
                    <?php

                        //$minModelValue = getMinValueOfModel($component->COMPONENTID);
                        //echo number_format($minModelValue,2,",",".");
                    ?> <!-- VND
                </span> -->
            </div>
            <?php if($data->IsDisplay == 1) {?>
                <div class="cart-control">
                    <input id="quantity" type="number" component="number" value="1" min="1" max="10"/>
                    <button id="btnAddToCart" class="">Thêm vào giỏ hàng</button>
                </div>
            <?php }?>
        </div>
        <div class="clear"></div>
        <div class="product-information" component="tab">
            <ul class="tab-item">
                <li class="active" target="productDescription">Thông tin sản phẩm</li>
                <li target="productGuild">Hướng dẫn liên quan</li>
                <li target="productReview">Đánh giá <b><?php echo round($reviewRateAverage, 1); ?></b>
                    (<?php echo sizeof($reviewData); ?>)
                </li>
            </ul>
            <div class="active tabContent" component="readmore-panel" id="productDescription">
                <?php echo $data->Description; ?>
            </div>
            <div class="tabContent" component="readmore-panel" id="productGuild">
                <?php echo $data->ManualGuide; ?>
            </div>
            <div class="tabContent" id="productReview">
                <div class="review-panel">
                    <div component="form" action="AssetModelReview" method="AddNew" align="float" name="ReviewForm">
                        <div class="form-content">
                            <item text=""
                                  name="ModelID"
                                  default="<?php echo (double)$modelID; ?>"
                                  type="hidden">
                            </item>
                            <item text=""
                                  name="Assetname"
                                  default=""
                                  type="hidden">
                            </item>
                            <item text=""
                                  name="UserName"
                                  default="<?php echo $current_user->user_login; ?>"
                                  type="hidden">
                            </item>
                            <item text=""
                                  name="Date"
                                  default="<?php echo (new DateTime())->getTimestamp(); ?>"
                                  type="hidden">
                            </item>
                            <item text="" name="Rate" max="5" type="rate" symbol="star"></item>
                            <item width="100%" text="Đánh giá" name="Comment" type="editor" height="150px"></item>
                        </div>
                        <div class="form-control-panel">
                            <button type="ajax-submit">Gửi</button>
                        </div>
                    </div>
                </div>
                <?php if (sizeof($reviewData) > 0) { ?>
                    <div class="review-list">
                        <h1 class="main-title">Danh sách đánh giá</h1>
                        <?php
                        if (sizeof($reviewData) > 0) {
                            foreach ($reviewData as $review) {

                                $commentDate = new DateTime();
                                $commentDate->setTimestamp($review->Date);

                                ?>
                                <div class="review">
                                    <div class="owner">
                                        <span class="name"><?php echo $review->UserName; ?></span>
                                        <span class="date"><?php echo $commentDate->format('d-m-Y H:i:s'); ?></span>
                                        <span component="rate" class="small disable" max="5" symbol="star"
                                              value="<?php echo $review->Rate; ?>"></span>
                                    </div>
                                    <div class="comment">
                                        <?php echo $review->Comment; ?>
                                    </div>
                                </div>
                                <?php
                            }
                        } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="clear"></div>
        <?php if(sizeof($relationAsset) > 0) { ?>
        <div class="product-relation">
            <section class="product-slide head-slide">
                <h2 class="main-title" pagecontent="Session3Text" data-bind="text: pagecontent().Session3Text"></h2>
                <article class="slide-tab-product">
                    <?php
                    generate_slide_product($mockSlideProduct);
                    ?>
                </article>
            </section>
        </div>
        <?php }?>
    </div>
    <script>
        $('[section="detail"]')[0].Model = {};
        $('[section="detail"]')[0].Model.Component = '<?php echo $componentJson; ?>';
        $('[section="detail"]')[0].Model.Quantity = '<?php echo $quantityInHand; ?>';
    </script>
<?php }); ?>

<?php import('theme.pages.master-page-home'); ?>


