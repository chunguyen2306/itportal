<div section="notsupport">
    <p>IT Portal chưa hỗ trợ tốt cho Edge và IE, vui lòng sử dụng Chrome hoặc Firefox để truy cập</p>
    <div class="icon">
        <span class="ie" disabled=""></span>
        <span class="edge" disabled=""></span>
        <span class="chrome"></span>
        <span class="firefox"></span>
        <span class="safari"></span>
        <span class="opera"></span>
    </div>
</div>