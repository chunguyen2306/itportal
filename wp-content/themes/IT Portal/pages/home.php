<?php add_action('place_holder1', function(){
    $APIAsset = using('plugin.itportal.API.Asset.APIAsset');
    //$mostRequest = $APIAsset->GetProductByMostUse();

    $allProduct = $APIAsset->GetProductSlideByType(array(
        'type' => json_encode(array('Other laptop', 'Other desktop', 'Monitor', 'Macbook', 'Mac mini', 'iMac', 'iPhone', 'iPad', 'Android and other phone', 'Android and other tablet'))
    ));

    $mostRequest = array_filter($allProduct, function($product){
        preg_match('/Other laptop|Other desktop|Monitor/m', $product->COMPONENTTYPENAME , $matches);
        return sizeof($matches) > 0;
    });
    $products = array_filter($allProduct, function($product){
        preg_match('/Macbook|Mac mini|iMac|iPhone|iPad/m', $product->COMPONENTTYPENAME , $matches);
        return sizeof($matches) > 0;
    });
    $otherProducts = array_filter($allProduct, function($product){
        preg_match('/Android and other phone|Android and other tablet/m', $product->COMPONENTTYPENAME , $matches);
        return sizeof($matches) > 0;
    });
    ?>
    <section class="product-slide head-slide">
        <article class="slide-tab-product">
            <?php
            generate_slide_product($mostRequest);
            ?>
        </article>
    </section>

    <section class="product-slide suggest-product">
        <h2 class="main-title" pagecontent="Session2Text" data-bind="text: pagecontent().Session2Text"></h2>
        <article class="slide-tab-product">
            <?php
            generate_slide_product($products);
            ?>
        </article>
    </section>

    <section class="product-slide necessary-product">
        <h2 class="main-title" pagecontent="Session3Text" data-bind="text: pagecontent().Session3Text"></h2>
        <article class="slide-tab-product">
            <?php
            generate_slide_product($otherProducts);
            ?>
        </article>
    </section>
<?php }); ?>
<?php import('theme.pages.master-page-home'); ?>


