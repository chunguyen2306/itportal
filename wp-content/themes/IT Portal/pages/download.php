<?php

if (isset($_GET['i']) && isset($_GET['e'])) {

    $file     = getFullPath('upload.' . $_GET['i'], $_GET['e'], 'path');
    $fileName = pathinfo($file, PATHINFO_FILENAME) . '.' . $_GET['e'];

    if (isset($_GET['n'])) {
        $fileName = $_GET['n'];
    }

    if (file_exists($file)) {
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=\"$fileName\"");
        header("Content-Type: " . mime_content_type($fileName));
        header("Content-Transfer-Encoding: binary");

        ob_clean();
        readfile($file);
        exit();
    }else{
        error_log('Download file not exist: ' . $file);
    }
}
else {
    error_log('Download file missing request params');
    exit();
}