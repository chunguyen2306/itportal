<?php add_action('am_footer', function () {
    importStyle('sub-page', 'theme.css.frontend');
}); ?>
<?php add_action('place_holder1', function () { ?>

    <?php
    global $AMUSER_ROLES;
    $searchType = $_GET['st'];
    $searchKeyword = isset($_GET['sk'])?$_GET['sk']:'';
    $searchKeyword = trim($searchKeyword);
    $searchCustomType = $_GET['sct'];

    if ($searchType == 'product' && $searchCustomType == '') {
        $searchCustomType = 'model';
    }
    $isOwned = true;
    $APIOptions = using('plugin.itportal.API.Options.AMOptions');
    $searchOption = $APIOptions->GetByCodeAndName([
        'optionCode' => 'home-search',
        'optionName' => 'owned-role'
    ]);

    if ($searchOption != null) {
        $optionValue = json_decode($searchOption->OptionValue);
        $userRole = json_decode($AMUSER_ROLES->Permission);
        $isOwned = !check_permisson('search.home.full');
    }
    ?>
    <div class="header-control">
        <div class="head-filter"
             data-bind="template: {name: '<?php echo "$searchType-head-filter-template"; ?>' }"></div>
        <div class="result-total">
            <span>Bạn đang xem  <span data-bind="text: (currentPage() * limit())+data().length"></span>/<span
                        data-bind="text: pageTotal"></span> kết quả</span>
        </div>
    </div>
    <div data-bind="template: { name: '<?php echo "$searchType-$searchCustomType"; ?>' }"></div>

    <div component="paging" data-bind="attr: { pageItemLimit: limit, total: pageTotal, pageCount: pageCount }"
         name="searchPaging"></div>

    <script type="template" id="product-head-filter-template">
        <a class="<?php echo $searchCustomType == 'model' ? 'active' : ''; ?>"
           data-bind="attr: { href: '?st='+searchType()+'&sk='+searchkeyword()+'&sct=model' }">Sản phẩm</a>
        <a class="<?php echo $searchCustomType == 'owned' ? 'active' : ''; ?>"
           data-bind="attr: { href: '?st='+searchType()+'&sk='+searchkeyword()+'&sct=owned' }">Sản phẩm đang sử dụng</a>
    </script>
    <script type="template" id="request-head-filter-template">

    </script>
    <script type="template" id="guild-head-filter-template">

    </script>
    <script type="template" id="guild-">
        <ul class="request-list" data-bind="foreach: data">
            <li>
                <a target="_blank" data-bind="attr: { href: '/document/'+CateName+'/post/'+post_name }"
                   class="request-info">
                    <span data-bind="text: CateName" ></span> > <span data-bind="text: post_title"></span>
                    <span class="order-created"><span
                                data-bind="text: new Date(post_date).format('DD/MM/YYYY hh:mm:ss')"></span></span>
                </a>
                <div class="request-item post-content" >
                    <span data-bind="html: $root.hightlineContent(post_content)"></span>
                    <a target="_blank" data-bind="attr: { href: '/document/'+CateName+'/post/'+post_name }"> xem thêm</a>
                </div>
            </li>
        </ul>
    </script>
    <script type="template" id="request-">
        <ul class="request-list" data-bind="foreach: {data: data, as: 'request'}">
            <?php import('theme.layout.request.list-view'); ?>
        </ul>
    </script>
    <script type="template" id="product-model">
        <ul class="product-container" data-bind="foreach: data">
            <li class="">
                <span class="transferDays"><span data-bind="text: TransferDays"></span>D</span>
                <span class="needApproval" title="Cần được phê duyệt"
                      data-bind="attr:{ needapproval: NeedApproval }">Cần <b>phê duyệt</b></span>

                <a target="_blank" draggable="false" data-bind="attr:{ href: ((IsDisplay===1)?'/detail/'+ModelID:false)}" class="fancybox">
                    <span class="outStock" data-bind="visible: (Stock == 0 || IsDisplay === 0), text: ((IsDisplay === 0)?'Ngừng cung cấp':'Không có sẵn')" ></span>
                    <span data-bind="style: { backgroundImage: 'url(\'<?php echo getFullPath('upload', '', 'url') ?>' + Thubmails + '\')' }"
                          class="imgstyle">
                                </span>
                    <span class="info-text">
                                <span data-bind="text: ModelName"></span>
                        <!-- ko if: $data['ProductPageDescription'] !== undefined -->
                                <span class="short-description"
                                      data-bind="text: $root.inlineShortDescription(ProductPageDescription)"></span>
                        <!-- /ko -->
                        <!-- ko if: $data['ProductPageDescription'] === undefined && $data['ShortDescription'] !== undefined -->
                                <span class="short-description"
                                      data-bind="text: $root.inlineShortDescription(ShortDescription)"></span>
                        <!-- /ko -->
                    </span>
                </a>
                <!-- ko if: IsDisplay == 1 -->
                    <button class="full"
                            data-bind="click: $root.do_AddToCart,text: i18n('front:product:productlist:btnOrderNow')"></button>
                <!-- /ko -->

            </li>
        </ul>
    </script>
    <script type="template" id="product-owned">
        <ul class="product-container" data-bind="foreach: data">
            <li class="">
                <a target="_blank" draggable="false" data-bind="attr:{ href: '/user-page/asset-detail?asset='+ResourceName}"
                   class="fancybox">
                    <span data-bind="style: { backgroundImage: 'url(\'<?php echo getFullPath('upload', '', 'url') ?>' + Thubmails + '\')' }"
                          class="imgstyle">
                                </span>
                    <span class="info-text">
                        <span data-bind="text: ResourceName"></span>
                        <span data-bind="text: ModelName"></span>
                    </span>
                </a>
            </li>
        </ul>
    </script>
    <input id="isOwnedSearch" type="hidden" value="<?php echo json_encode($isOwned); ?>"/>
<?php }); ?>

<?php
import('theme.pages.master-page-home');
?>