<?php add_action('am_footer', function(){
    importStyle('workflow', 'theme.css.frontend');
    importStyle('sub-page', 'theme.css.frontend');
    importResource('theme.Module.master-page-user');
})?>
<?php add_action('place_holder1', function(){ ?>
    <?php
    $current_user = wp_get_current_user();
    $userInfo = get_user_info($current_user->user_login);
    $userIco = get_user_default_icon($userInfo);
    ?>
    <div page="user">
        <div class="left-panel">
            <div class="user-info">
                <a class="user-avatar" style="background-color: <?php echo $userIco['color']; ?>">
                    <?php echo $userIco['name'] ?>
                </a>
                <span class="user-name"><?php echo $userInfo->fullName; ?></span>
                <span class="user-title"><?php echo $userInfo->jobTitle; ?></span>
                <!--<div class="control-panel">
                    <a href="#" title="Đổi mật khẩu">Đổi mật khẩu</a>
                </div>-->
            </div>
            <?php

            $userMenu = using('theme.package.Includes.User.MainMenu');

            am_add_menu_admin($userMenu);

            unset($userMenu);

            ?>
        </div>
        <div class="right-panel">
            <?php do_action('content-holder'); ?>
        </div>
    </div>
<?php });?>

<?php import('theme.pages.master-page-home'); ?>