<?php
class AbsDB{

    function esc_filter($filter){
        // prase like
        $pattern = '/([^\s]+)\s+([lL][iI][kK][eE])(\'.+)/';
        preg_match($pattern, $filter, $matches);

        // prase in
        if(sizeof($matches) != 4){
            $pattern = '/([^\s]+)\s+([iI][nN])(\(.+)/';
            preg_match($pattern, $filter, $matches);
        }

        // prase bin operators
        if(sizeof($matches) != 4){
            $pattern = '/([^=>!<]+)([!=><]{1,3})(.+)/';
            preg_match($pattern, $filter, $matches);
        }

        // parse chuẩn
        if(sizeof($matches) != 4){
            $pattern = '/([^\s]+)\s+([^\s]+)\s+(.+)/';
            preg_match($pattern, $filter, $matches);
        }

        if(sizeof($matches) != 4){
            $ex = new Exception('Can not parse filter to ESC');
            AMRespone::exception($filter, $ex);
        }

        $operator = strtolower($matches[2]);

        $value = $matches[3];
        if($operator == 'in'){
            $value = trim($value, " ()\t\n\r\0\x0B");
            $array = explode(',', $value);
            foreach ($array as $i => $iValue)
                $array[$i] = $this->esc_value($iValue);

            $value = '(' . join(',', $array) . ')';
        }else{
            $value = $this->esc_value(trim($value));
        }

        //            error_log($filter . '|' . $matches[1] . ',' . $operator . ',' . $value);
        return $matches[1] . ' ' . $operator . ' ' . $value;
    }

    function esc_value($value){
        if(is_numeric($value) || is_string($value) == false)
            return $value;


        $size = strlen($value);
        $requiredQoute = $size > 1
            && (
                $value[0] === '\'' && $value[$size - 1] === '\''
                || $value[0] === '"' && $value[$size - 1] === '"'
            );
        if($requiredQoute){
            $value = substr($value, 1, $size - 2);
        }

        global $wpdb;
        $value = $wpdb->_escape($value);
        return $requiredQoute ? "'" . $value . "'" : $value;
    }
}


$array = [
 //   input                 output            comment
    ["TenCot = 1",        "TenCot = 1",        "số"],
    ["TenCot = '1'",      "TenCot = '1'",      "số trong nháy"],
    ["TenCot = 'a'",      "TenCot = 'a'",      "chữ"],
    ["TenCot = 'a '",     "TenCot = 'a '",     "chữ với khoảng trắng"],
    ["TenCot = 'I\\'m '", "TenCot = 'I\\\\\\'m '", "chữ với dấu nháy an toàn"],
    ["TenCot = 'I'm '", "TenCot = 'I\\'m '", "chữ với dấu nháy k an toàn"],
    ["TenCot= 1",        "TenCot = 1",        "số"],
    ["TenCot= '1'",      "TenCot = '1'",      "số trong nháy"],
    ["TenCot= 'a'",      "TenCot = 'a'",      "chữ"],
    ["TenCot= 'a '",     "TenCot = 'a '",     "chữ với khoảng trắng"],
    ["TenCot =1",        "TenCot = 1",        "số"],
    ["TenCot ='1'",      "TenCot = '1'",      "số trong nháy"],
    ["TenCot ='a'",      "TenCot = 'a'",      "chữ"],
    ["TenCot ='a '",     "TenCot = 'a '",     "chữ với khoảng trắng"],
    ["TenCot=1",        "TenCot = 1",        "số"],
    ["TenCot='1'",      "TenCot = '1'",      "số trong nháy"],
    ["TenCot='a'",      "TenCot = 'a'",      "chữ"],
    ["TenCot='a '",     "TenCot = 'a '",     "chữ với khoảng trắng"],

    ["TenCot lIke'1'",      "TenCot like '1'",      "không có  %"],
    ["TenCot like 'a%d'",    "TenCot like 'a%d'",      "có  %"],
    ["TenCot like'1'",      "TenCot like '1'",      "không dấu cách phía sau - không có  %"],
    ["TenCot like'a%d'",    "TenCot like 'a%d'",    "không dấu cách phía sau - có  %"],

    ["TenCot in (1)",      "TenCot in (1)",      "không có  %"],
    ["TenCot in (1,'2')",    "TenCot in (1,'2')",      "có  %"],
    ["TenCot in(1)",      "TenCot in (1)",      "không dấu cách phía sau - không có  %"],
    ["TenCot in(1,'2')",    "TenCot in (1,'2')",    "không dấu cách phía sau - có  %"],
    ["TenCot in(1,'2')",    "TenCot in (1,'2')",    "không dấu cách phía sau - có  %"],


    ["TenCot > 1",        "TenCot > 1",        "số"],
    ["TenCot > '1'",      "TenCot > '1'",      "số trong nháy"],
    ["TenCot > 'a'",      "TenCot > 'a'",      "chữ"],
    ["TenCot > 'a '",     "TenCot > 'a '",     "chữ với khoảng trắng"],
    ["TenCot > 'I\\'m '", "TenCot > 'I\\\\\\'m '", "chữ với dấu nháy an toàn"],
    ["TenCot > 'I'm '", "TenCot > 'I\\'m '", "chữ với dấu nháy k an toàn"],
    ["TenCot> 1",        "TenCot > 1",        "số"],
    ["TenCot> '1'",      "TenCot > '1'",      "số trong nháy"],
    ["TenCot> 'a'",      "TenCot > 'a'",      "chữ"],
    ["TenCot> 'a '",     "TenCot > 'a '",     "chữ với khoảng trắng"],
    ["TenCot >1",        "TenCot > 1",        "số"],
    ["TenCot >'1'",      "TenCot > '1'",      "số trong nháy"],
    ["TenCot >'a'",      "TenCot > 'a'",      "chữ"],
    ["TenCot >'a '",     "TenCot > 'a '",     "chữ với khoảng trắng"],
    ["TenCot>1",        "TenCot > 1",        "số"],
    ["TenCot>'1'",      "TenCot > '1'",      "số trong nháy"],
    ["TenCot>'a'",      "TenCot > 'a'",      "chữ"],
    ["TenCot>'a '",     "TenCot > 'a '",     "chữ với khoảng trắng"],


    ["TenCot < 1",        "TenCot < 1",        "số"],
    ["TenCot < '1'",      "TenCot < '1'",      "số trong nháy"],
    ["TenCot < 'a'",      "TenCot < 'a'",      "chữ"],
    ["TenCot < 'a '",     "TenCot < 'a '",     "chữ với khoảng trắng"],
    ["TenCot < 'I\\'m '", "TenCot < 'I\\\\\\'m '", "chữ với dấu nháy an toàn"],
    ["TenCot < 'I'm '", "TenCot < 'I\\'m '", "chữ với dấu nháy k an toàn"],
    ["TenCot< 1",        "TenCot < 1",        "số"],
    ["TenCot< '1'",      "TenCot < '1'",      "số trong nháy"],
    ["TenCot< 'a'",      "TenCot < 'a'",      "chữ"],
    ["TenCot< 'a '",     "TenCot < 'a '",     "chữ với khoảng trắng"],
    ["TenCot <1",        "TenCot < 1",        "số"],
    ["TenCot <'1'",      "TenCot < '1'",      "số trong nháy"],
    ["TenCot <'a'",      "TenCot < 'a'",      "chữ"],
    ["TenCot <'a '",     "TenCot < 'a '",     "chữ với khoảng trắng"],
    ["TenCot<1",        "TenCot < 1",        "số"],
    ["TenCot<'1'",      "TenCot < '1'",      "số trong nháy"],
    ["TenCot<'a'",      "TenCot < 'a'",      "chữ"],
    ["TenCot<'a '",     "TenCot < 'a '",     "chữ với khoảng trắng"],


    ["TenCot => 1",        "TenCot => 1",        "số"],
    ["TenCot => '1'",      "TenCot => '1'",      "số trong nháy"],
    ["TenCot => 'a'",      "TenCot => 'a'",      "chữ"],
    ["TenCot => 'a '",     "TenCot => 'a '",     "chữ với khoảng trắng"],
    ["TenCot => 'I\\'m '", "TenCot => 'I\\\\\\'m '", "chữ với dấu nháy an toàn"],
    ["TenCot => 'I'm '", "TenCot => 'I\\'m '", "chữ với dấu nháy k an toàn"],
    ["TenCot=> 1",        "TenCot => 1",        "số"],
    ["TenCot=> '1'",      "TenCot => '1'",      "số trong nháy"],
    ["TenCot=> 'a'",      "TenCot => 'a'",      "chữ"],
    ["TenCot=> 'a '",     "TenCot => 'a '",     "chữ với khoảng trắng"],
    ["TenCot =>1",        "TenCot => 1",        "số"],
    ["TenCot =>'1'",      "TenCot => '1'",      "số trong nháy"],
    ["TenCot =>'a'",      "TenCot => 'a'",      "chữ"],
    ["TenCot =>'a '",     "TenCot => 'a '",     "chữ với khoảng trắng"],
    ["TenCot=>1",        "TenCot => 1",        "số"],
    ["TenCot=>'1'",      "TenCot => '1'",      "số trong nháy"],
    ["TenCot=>'a'",      "TenCot => 'a'",      "chữ"],
    ["TenCot=>'a '",     "TenCot => 'a '",     "chữ với khoảng trắng"],


    ["TenCot <=> 1",        "TenCot <=> 1",        "số"],
    ["TenCot <=> '1'",      "TenCot <=> '1'",      "số trong nháy"],
    ["TenCot <=> 'a'",      "TenCot <=> 'a'",      "chữ"],
    ["TenCot <=> 'a '",     "TenCot <=> 'a '",     "chữ với khoảng trắng"],
    ["TenCot <=> 'I\\'m '", "TenCot <=> 'I\\\\\\'m '", "chữ với dấu nháy an toàn"],
    ["TenCot <=> 'I'm '", "TenCot <=> 'I\\'m '", "chữ với dấu nháy k an toàn"],
    ["TenCot<=> 1",        "TenCot <=> 1",        "số"],
    ["TenCot<=> '1'",      "TenCot <=> '1'",      "số trong nháy"],
    ["TenCot<=> 'a'",      "TenCot <=> 'a'",      "chữ"],
    ["TenCot<=> 'a '",     "TenCot <=> 'a '",     "chữ với khoảng trắng"],
    ["TenCot <=>1",        "TenCot <=> 1",        "số"],
    ["TenCot <=>'1'",      "TenCot <=> '1'",      "số trong nháy"],
    ["TenCot <=>'a'",      "TenCot <=> 'a'",      "chữ"],
    ["TenCot <=>'a '",     "TenCot <=> 'a '",     "chữ với khoảng trắng"],
    ["TenCot<=>1",        "TenCot <=> 1",        "số"],
    ["TenCot<=>'1'",      "TenCot <=> '1'",      "số trong nháy"],
    ["TenCot<=>'a'",      "TenCot <=> 'a'",      "chữ"],
    ["TenCot<=>'a '",     "TenCot <=> 'a '",     "chữ với khoảng trắng"],


    ["TenCot not like 'asd%'",        "TenCot not like 'asd%'",        "not like"],
    ["TenCot not in(1,2)",        "TenCot not likt 'asd%'",        "not like"],
    ["TenCot BETWEEN 1 AND 2",        "TenCot BETWEEN 1 AND 2",        "toán tử ít dùng"],
];

$db = new AbsDB();

foreach ($array as $case){
   $outPut = $db->esc_filter($case[0]);
    $outPut = str_replace('  ', ' ', $outPut);
   $ok = $outPut == $case[1];

   if($ok)
      echo '<div> Pass: ';
   else
       echo '<div style="color: red">Failed: ';

    echo $case[0];
    echo ' => ';
    echo $outPut;
    echo ' vs ';
    echo $case[1];
    echo ' : ';
    echo $case[2];
    echo '</div>';
    var_dump($case[0], $case[1], $outPut);
}