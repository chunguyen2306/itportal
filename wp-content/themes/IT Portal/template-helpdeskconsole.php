<?php /* Template Name: Helpdesk Console Template */ ?>
<?php
//do_action("amLogin_checklogin", "/helpdesk-console");

$baseURL = get_template_directory_uri();
?>
<link href="<?php echo $baseURL; ?>/css/helpdesk.css?v=<?php echo((new DateTime())->getTimestamp()); ?>" type="text/css" rel="stylesheet"/>
<div class="section-helpdeskconsole">
    <div class="header">
        <div>
            <button popup="popCreateServiceMail">Tạo Service Mail</button>
            <button popup="popGroupManager">Quản Lý Nhóm</button>
        </div>
        <input combobox="domain" data-bind="domain: txbAccountKeyword, event: { keypress: do_getUserInformation }" type="text" id="txtKeyword" placeholder="Nhập account để thao tác" />
    </div>

    <div class="content">
        <div comtype="TabContainer" id="tabAccountInfo">
            <tabs>
                <a default tab="tabcAccountInfo">Thông tin tài khoản</a>
                <a tab="tabcPersonInfo">Thông tin cá nhân</a>
                <a tab="tabcLicenseInfo">Thông tin bản quyền</a>
            </tabs>
            <tabcontents>
                <div id="tabcAccountInfo" class="form">
                    <div class="form-line">
                        <span class="form-label">Enable Sync</span>
                        <div class="form-field">
                            <input disabled="disabled" data-bind="checked: userInfo().onPremisesSyncEnabled" type="checkbox"/>
                        </div>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Account Enable</span>
                        <div class="form-field">
                            <input disabled="disabled" data-bind="checked: userInfo().accountEnabled" type="checkbox"/>
                            <!-- ko if: userInfo().onPremisesSyncEnabled -->
                            <span data-bind="click: do_EnableDiableAccount" class="button">Disable</span>
                            <!-- /ko -->
                            <!-- ko ifnot: userInfo().onPremisesSyncEnabled -->
                            <span data-bind="click: do_EnableDiableAccount" class="button">Enable</span>
                            <!-- /ko -->
                        </div>
                    </div>
                    <div class="form-line">
                        <span class="form-label">&nbsp;</span>
                        <div class="form-field">
                            <span data-bind="click: do_ResetUserPassword" class="button">Reset mật khẩu</span>
                        </div>
                    </div>
                </div>
                <div id="tabcPersonInfo" class="form">
                    <div class="form-line">
                        <span class="form-label">Tên tài khoản</span>
                        <div class="form-field" >
                            <input disabled="disabled" data-bind="value: userInfo().onPremisesSamAccountName" type="text"/>
                        </div>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Tên đầy đủ</span>
                        <div class="form-field" >
                            <input data-bind="value: userInfo().displayName" type="text"/>
                        </div>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Mã nhân viên</span>
                        <div class="form-field" >
                            <input disabled="disabled" data-bind="value: userInfo().extension_7f34f4cfae6544b08a81f10846fcbfcd_employeeID" type="text"/>
                        </div>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Mail</span>
                        <div class="form-field" >
                            <input disabled="disabled" data-bind="value: userInfo().mail" type="text"/>
                        </div>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Chức vụ</span>
                        <div class="form-field" >
                            <input disabled="disabled" data-bind="value: userInfo().jobTitle" type="text"/>
                        </div>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Bộ phận</span>
                        <div class="form-field" >
                            <input disabled="disabled" data-bind="value: userInfo().extension_7f34f4cfae6544b08a81f10846fcbfcd_division" type="text"/>
                        </div>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Phòng ban</span>
                        <div class="form-field" >
                            <input disabled="disabled" data-bind="value: userInfo().department" type="text"/>
                        </div>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Team</span>
                        <div class="form-field" >
                            <input disabled="disabled" data-bind="value: userInfo().extension_7f34f4cfae6544b08a81f10846fcbfcd_team" type="text"/>
                        </div>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Số điện thoại</span>
                        <div class="form-field" >
                            <input data-bind="value: userInfo().mobilePhone" type="text"/>
                        </div>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Số nội bộ</span>
                        <div class="form-field" >
                            <input disabled="disabled" data-bind="value: userInfo().extension_7f34f4cfae6544b08a81f10846fcbfcd_divisionID" type="text"/>
                        </div>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Chổ ngồi</span>
                        <div class="form-field" >
                            <input data-bind="value: userInfo().officeLocation" type="text"/>
                        </div>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Quản lý</span>
                        <div class="form-field field-addon" >
                            <input combobox="domain" data-bind="domain: userInfo().manager, attr: { email: userInfo().managerEMail }" type="text"/>
                            <span data-bind="click: do_viewManager" class="addon button" >Xem</span>
                            <!-- ko if: userInfo().accountType -->
                            <span data-bind="click: do_changeManager" class="addon button" >Thay đổi</span>
                            <!-- /ko -->
                        </div>
                    </div>
                    <div class="form-line">
                        <span class="form-label">Thành viên của</span>
                        <div class="form-field" data-bind="foreach: userInfo().memberOf">
                            <a class="line-item" href="" data-bind="click: $root.do_viewGroupDetails, text: displayName">Thêm</a>
                        </div>
                    </div>
                    <!-- ko if: userInfo().accountType -->
                    <div class="form-line">
                        <span class="form-label">
                            &nbsp;
                        </span>
                        <div class="form-field">
                            <span data-bind="click: do_changeUserInfo" class="button">Lưu Thay Đổi</span>
                        </div>
                    </div>
                    <!-- /ko -->
                </div>
                <div id="tabcLicenseInfo" class="form">
                    <div class="form-line">
                        <span class="form-label">Danh sách gói bản quyền</span>
                    </div>
                    <div class="form-line" data-bind="foreach: licenses">
                        <div class="form-field" >
                            <span class="form-label">&nbsp;</span>
                            <label class="checkbox">
                                <input data-bind="checked: $root.licenseCheckeds, attr: { value: $data }" type="checkbox" name="EPack" />
                                <span data-bind="text: $data" ></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-line">
                        <div class="form-field" >
                            <span class="form-label">&nbsp;</span>
                            <div class="form-field">
                                <span data-bind="click: do_changeUserLicense" class="button">Lưu Thay Đổi</span>
                            </div>
                        </div>
                    </div>
                </div>
            </tabcontents>
        </div>
    </div>

    <div class="popup" comtype="Popup" id="popCreateServiceMail">

        <div class="header">
            <h3 class="title">Tạo Service Mail</h3>
            <a class="close">Đóng</a>
        </div>
        <div class="content">
            <div class="form">
                <div class="form-line">
                    <span class="form-label">Kích hoạt</span>
                    <div class="form-field" >
                        <input data-bind="checked: createMailInfo().accountEnabled" type="checkbox" />
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">Tên hiển thị</span>
                    <div class="form-field" >
                        <input data-bind="value: createMailInfo().displayName"  placeholder="Display name" type="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">Tên gọi tắc</span>
                    <div class="form-field" >
                        <input data-bind="value: createMailInfo().mailNickname"  placeholder="Nick name" type="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">Tên tài khoản</span>
                    <div class="form-field" >
                        <input combobox="domain" data-bind="domain: createMailInfo().userPrincipalName"  placeholder="Mail account" type="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">Người sở hữu</span>
                    <div class="form-field" >
                        <input combobox="domain" data-bind="domain: createMailInfo().owner" placeholder="Owner" type="text"/>
                    </div>
                </div>

                <div class="form-line">
                    <span class="form-label">&nbsp;</span>
                    <div class="form-field" >
                        <span data-bind="click: do_createServiceMail" class="button">Đồng Ý Tạo</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" comtype="Popup" id="popGroupManager">
        <div class="header">
            <h3 class="title">Quản Lý Nhóm</h3>
            <a class="close">Đóng</a>
        </div>
        <div class="content">
            <input type="text" id="txbGroupName" data-bind="value: txbGroupName, event: { keypress: do_searchGroup }" placeholder="Nhập tên nhóm để thao tác" />
            <div class="popGroupManager-list">
                <table width="100%">
                    <thead>
                    <tr>
                        <th>Tên</th>
                        <th>Mail</th>
                        <th>Ghi chú</th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: searchGroups">
                    <tr data-bind="click: $root.do_viewGroupDetails">
                        <td data-bind="text: displayName"></td>
                        <td data-bind="text: mail">Mail</td>
                        <td data-bind="text: description">Ghi chú</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="form popGroupManager-content">
                <div class="form-line">
                    <span class="form-label">Tên nhóm</span>
                    <div class="form-field">
                        <input data-bind="value: groupDetail().displayName" type="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">Ngày tạo</span>
                    <div class="form-field">
                        <input disabled="disabled" data-bind="value: groupDetail().createdDateTime" type="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">Mail</span>
                    <div class="form-field">
                        <input disabled="disabled" combobox="domain" data-bind="value: groupDetail().mail" type="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">Mail Enable</span>
                    <div class="form-field">
                        <!-- ko if: groupDetail().onPremisesSyncEnabled -->
                        <input disabled="disabled" data-bind="checked: groupDetail().mailEnabled" type="checkbox"/>
                        <!-- /ko -->
                        <!-- ko ifnot: groupDetail().onPremisesSyncEnabled -->
                        <input data-bind="checked: groupDetail().mailEnabled" type="checkbox"/>
                        <!-- /ko -->
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">Mail nick name</span>
                    <div class="form-field">
                        <input data-bind="value: groupDetail().mailNickname" type="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">Lần Sync cuối</span>
                    <div class="form-field">
                        <input disabled="disabled" data-bind="value: groupDetail().onPremisesLastSyncDateTime" type="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">Enable Sync</span>
                    <div class="form-field">
                        <input disabled="disabled" data-bind="checked: groupDetail().onPremisesSyncEnabled" type="checkbox"/>
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">Security Enabled</span>
                    <div class="form-field">
                        <!-- ko if: groupDetail().onPremisesSyncEnabled -->
                        <input disabled="disabled" data-bind="checked: groupDetail().securityEnabled" type="checkbox"/>
                        <!-- /ko -->
                        <!-- ko ifnot: groupDetail().onPremisesSyncEnabled -->
                        <input data-bind="checked: groupDetail().securityEnabled" type="checkbox"/>
                        <!-- /ko -->
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">Chế độ</span>
                    <div class="form-field">
                        <input data-bind="value: groupDetail().visibility" type="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">Owner</span>
                    <div class="form-field" data-bind="foreach: groupDetail().owner">
                        <span data-bind="text: onPremisesSamAccountName"></span>
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">&nbsp;</span>
                    <div class="form-field pull-right field-addon">
                        <input combobox="domain" type="text" data-bind="value: txbGroupOwnerName" />
                        <span data-bind="click: do_updateGroupOwner" class="addon button">Đổi Owner</span>
                    </div>
                </div>
                <div class="form-line">
                    <span class="form-label">&nbsp;</span>
                    <div class="form-field">
                        <span data-bind="click: do_updateGroup" class="button">Lưu Thay Đổi</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <ul comboboxitem name="domain" data-bind="foreach:domains">
        <li data-bind="text: id"></li>
    </ul>
    <div class="blank"></div>
    <div class="alert"></div>
    <div class="loadding"></div>
</div>
<script src="<?php echo $baseURL; ?>/js/Data/Utils.js"></script>
<script src="<?php echo $baseURL; ?>/js/Data/helpdesk.js"></script>
<script src="<?php echo $baseURL; ?>/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $baseURL; ?>/js/knockout-3.4.1.js"></script>
<script src="<?php echo $baseURL; ?>/js/helpdesk.js?v=<?php echo((new DateTime())->getTimestamp()); ?>"></script>

