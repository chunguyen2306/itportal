'use strict';

var DnDMoM = {};

jQuery(document).ready(function(e) {
    if (jQuery(".SelectUI1").length > 0) {
        jQuery(".SelectUI1").addSelectUI({
            scrollbarWidth: 10 //default is 10
        });
    }

    // calculate number of star in rating
    $('span.rating').each(function(){
        if($(this).text() > 0) {
            $(this).css('width', $(this).text() * 18 + 'px');
            // console.log($(this).text()* 17 + 'px');
        }
    });


    StaticMain();
    (function($) {

        var $window = $(window);
        var $body = $('body');


       $('.play-video').fancybox({
            openEffect: "elastic",
            padding: [0, 0, 0, 0],
            width: '100%',
            height: '100%',
            fitToView: true,
            autoCenter: !0,
            'autoDimensions': false,
            'width': 800,
            'height': 550,
            beforeShow: function() {
                // if ($('#sound').hasClass('sound-on')) {
                //  // off sound
                //  $('#sound').removeClass('sound-on');
                //  $('#sound').addClass('sound-off');
                // }
                // audio1.pause();
                // audio2.pause();
            },
            afterClose: function() {
                // if ($('#sound').hasClass('sound-off')) {
                //  // off sound
                //  $('#sound').removeClass('sound-off');
                //  $('#sound').addClass('sound-on');               
                // }
                // audio1.play();
            },
            helpers: {
                title: !1,
                // thumbs: {
                //  width: 92,
                //  height: 47
                // },
                media: {}
            },
            nextEffect: "elastic",
            prevEffect: "elastic"
        });
		// fancybox images
		if ($('.fancybox').length > 0) {
			DnDMoM.initPopup('.fancybox');
		}

        //init main navigation behavior
        DnDMoM.initMainNavPart1();

        //init main navigation sub-menu behavior
        DnDMoM.initMainNavPart2();


        //init userinfo navigation sub-menu behavior
        DnDMoM.initSubNavigation('#user-info', '#user-info__list');


        if ($body.hasClass('homepage')) {

            var firstTabGroup1 = DnDMoM.initTabpannel('#post1');
            var firstTabGroup2 = DnDMoM.initTabpannel('#post2');
            var firstTabGroup3 = DnDMoM.initTabpannel('#post3');
            // var firstSlideTab1 = DnDMoM.initImageSlider('#primary-banner1');
            // var firstSlideTab2 = DnDMoM.initImageSlider('#primary-banner2');
            // var firstSlideTab3 = DnDMoM.initImageSlider('#primary-banner3');
            // var suggestSlideTab1 = DnDMoM.initImageSlider('#suggess-banner1');
            // var necessarySlideTab1 = DnDMoM.initImageSlider('#necessary-banner1');





            // DnDMoM.setHeaderView();
        }



        $window.on('orientationchange', function(e) {
            if (window.orientation == 0) { //portrait
            } else { //90(right) or -90(left)
            }
        });
    })(jQuery);
});

//functions
DnDMoM = (function($) {
    var _options = {
        swiper: {
            mode: 'horizontal',
            loop: false,
            slidesPerView: 'auto',
            slideElement: 'li'
        }
    }
    var $window = $(window);



    return {
        initSubNavigation: function(container, navList) {
            var tContainer = $(container);
            var tNavList = $(navList);
            var navTriggerEvent = Modernizr.touch ? 'touchend' : 'click';
            tContainer
                .on(navTriggerEvent, '> li > a', function(e) {
                    e.stopPropagation();
                })
                .on(navTriggerEvent, '> a.has-sub', function(e) {
                    $(this).toggleClass('focus');
                    return false;
                });
            $(document).on(navTriggerEvent, function(e) {
                $(container + ' > a.has-sub.focus').removeClass('focus');
            });

            //init state for reload page
            // if (tNavList.find('a.active').length == 0) {
            //     var section = window.location.href.split('/').pop();
            //     var sectionRegExp = new RegExp('.html', 'g');
            //     if (sectionRegExp.test(section)) {
            //         tNavList.find('a[href*="' + section + '"]').addClass('active');
            //     } else {
            //         tNavList.find('a').eq(0).addClass('active');
            //     }
            // }

            return navList;
        },

        initMainNavPart1: function() {
            var mainNav = $('#main-nav');
            var mainNavList = $('#main-nav__list');
            var mainNavTriggerEvent = Modernizr.touch ? 'touchend' : 'click';
            mainNavList
                .on(mainNavTriggerEvent, '.select-product ul > li a', function(e) {
                    e.stopPropagation();
                })
                .on(mainNavTriggerEvent, 'li > a', function(e) {
                    $('li > a.active').not(this).end().removeClass('active');
                    $(this).toggleClass('active');
                    return false;
                });


            var drawToggle = $('#drawer-toggle');
            var _fn_ = function() {
                if (Modernizr.mq('only screen and (max-width: 666px), (min-width: 667px) and (max-width: 1024px)')) {
                    drawToggle.on('change', function() {
                        if ($(this).is(':checked')) {
                            mainNav.addClass('shown');
                        } else {
                            setTimeout(function() {
                                mainNav.removeClass('shown');
                            }, 250);
                        }
                    });
                }
            }
            $(window).on('resize', _fn_);
            _fn_();

            //swipe main-nav on/off
            if (Modernizr.mq('only screen and (max-width: 666px), (min-width: 667px) and (max-width: 1024px)')) {
                $(window).swipeListener({
                    minX: 25,
                    minY: 0,
                    swipeRight: function(e) {
                        if (e.coords.start.x <= $window.width() * .25 && Math.abs(e.coords.start.y - e.coords.stop.y) <= 20) {
                            drawToggle.prop('checked', 'checked');
                            $('#main-nav').addClass('shown');
                        }
                    },
                    swipeLeft: function(e) {
                        if (Math.abs(e.coords.start.y - e.coords.stop.y) <= 20) {
                            drawToggle.prop('checked', false);
                            setTimeout(function() {
                                mainNav.removeClass('shown');
                            }, 250);
                        }
                    },
                    swipeUp: function(e) {
                        return true;
                    },
                    swipeDown: function(e) {
                        return true;
                    }
                });
            }

            return mainNavList;
        },

        initMainNavPart2: function() {
            var tContainer = $('#main-nav2');
            var tNavList = $('#main-nav2__list');
            var navTriggerEvent = Modernizr.touch ? 'touchend' : 'click';
            tContainer
                .on(navTriggerEvent, 'ul#main-nav2__list > li > a', function(e) {
                    $('li > a.active').not(this).end().removeClass('active');
                    // console.log(this);
                    $(this).toggleClass('active');
                    $('#main-nav2__list').toggleClass('show-sub');
                    $('#main-nav2 > a.has-sub.focus').removeClass('focus');
                    return false;
                })
                .on(navTriggerEvent, '.select-product ul > li a', function(e) {
                    e.stopPropagation();
                })
                .on(navTriggerEvent, '> a.has-sub', function(e) {
                    $(this).toggleClass('focus');
                    if($('#main-nav2__list').hasClass('show-sub')) {
                        $('#main-nav2__list').removeClass('show-sub');
                    }
                    return false;
                });
            $(document).on(navTriggerEvent, function(e) {
                $('#main-nav2 > a.has-sub.focus').removeClass('focus');
            });

            //init state for reload page
            // if (tNavList.find('a.active').length == 0) {
            //     var section = window.location.href.split('/').pop();
            //     var sectionRegExp = new RegExp('.html', 'g');
            //     if (sectionRegExp.test(section)) {
            //         tNavList.find('a[href*="' + section + '"]').addClass('active');
            //     } else {
            //         tNavList.find('a').eq(0).addClass('active');
            //     }
            // }

            return tContainer;
        },

        initImageSlider: function(selector) {

            var _fn_ = function() {

                // if (DnDMoM.keyFeaturesSwiper instanceof Swiper) {
                //     DnDMoM.keyFeaturesSwiper = DnDMoM.destroySlider(DnDMoM.keyFeaturesSwiper);
                // }
                var _disableFancyClick_ = function() {
                    return false;
                }
                var fancylinks = $(selector + ' a.fancybox');
                var next = selector + " " + '.swiper-button-next';
                var prev = selector + " " + '.swiper-button-prev';
                var paging = selector + " " + '.swiper-button-pagination';


                DnDMoM.keyFeaturesSwiper = new Swiper(selector, $.extend(true, {}, _options.swiper, {
                    initialSlide: 0,
                    slidesPerView: 'auto',
                    autoplay: false,
                    spaceBetween: 0,
                    paginationClickable: true,
                    pagination: paging,
                    nextButton: next,
                    prevButton: prev,
                    autoplayDisableOnInteraction: true,
                    // spaceBetween: Modernizr.mq('only screen and (min-width: 667px)') ? 30 : 15,
                    onTouchMove: function(swiper, e, diff) {
                        if (diff !== 0) {
                            if (fancylinks.hasClass('fancybox--disabled')) {
                                return false;
                            }
                            fancylinks.addClass('fancybox--disabled').on('click', _disableFancyClick_);
                        }
                    },
                    onTouchEnd: function(swiper) {
                        setTimeout(function() {
                            fancylinks.off('click', _disableFancyClick_);
                            fancylinks.removeClass('fancybox--disabled')
                        }, 1);
                    }
                }));

            }

            $window.resize(function(e) {
                _fn_();
            })
            _fn_();
        },

        initPopup: function(selector) {
            if ($(selector).length > 0) {
                $(selector).fancybox({
                    openEffect: 'elastic',
                    autoCenter: true,
                    padding: [3, 3, 3, 3],
                    helpers: {
                        title: {
                            type: 'inside'
                        },
                        media: {}
                    },
                    nextEffect: 'elastic',
                    prevEffect: 'elastic'
                });
            }
        },

        initTabpannel: function(selector) {
            var curContainTab, curContainSlide;
            // var selector = '#post1';
            var $ttab = $(selector + ' .posts__tabs > li');
            var $ttabDetail = $(selector + ' .tab-details');
            if ($ttabDetail.length > 0) {
                $ttabDetail.hide();
                curContainTab = selector + " #" + $ttabDetail.first().attr('id');
                $ttabDetail.first().show();
                $ttab.first().addClass("active");
                // init slide show
                curContainSlide = curContainTab + " .swiper-container";
                var thisSlideTab = DnDMoM.initImageSlider(curContainSlide);


                $ttab.children('a').click(function() {
                    $ttabDetail.hide().end();
                    $ttab.removeClass("active");
                    $(this).closest('li').addClass("active");
                    var curId = $(this).attr("href");
                    curContainTab = selector + " " + curId;
                    curContainSlide = curContainTab + " .swiper-container";
                    $(curContainTab).show();
                    DnDMoM.destroySlider(thisSlideTab);
                    thisSlideTab = DnDMoM.initImageSlider(curContainSlide);
                    return false;
                });
            }
        },

        destroySlider: function(object) {
            if (object !== undefined) {
                // destroy and delete swiper object
                var container = $(object.container);
                container.find('*').stop(true, true).removeAttr('style');
                object.destroy();
                return undefined;
            }
        },

        // setHeaderView: function() {
        //     var _fn_ = function() {
        //         var primaryBanner = $('#primary-banner');
        //         if (Modernizr.mq('only screen and (max-width: 667px)')) {
        //             primaryBanner.css({
        //                 height: $(window).height() - $('.app-info').eq(0).height() - primaryBanner.offset().top
        //             });
        //         } else {
        //             primaryBanner.removeAttr('style');
        //         }
        //     }
        //     $(window).on('resize', _fn_);
        //     _fn_();
        // },


    }
})(jQuery);
//pattern
Pattern.Mediator.installTo(DnDMoM);


var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Desktop: function() {
        return navigator.userAgent.match(/Windows NT/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
function StaticMain() {
    

}