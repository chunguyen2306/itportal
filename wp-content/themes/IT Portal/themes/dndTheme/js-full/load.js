(function() {
    function requireScriptsDependingOnJQueryAndRactive() {
        return basket.require({
            url: 'http://img.zing.vn/products/dmhmp1/skin-2015/js-full/pattern.js'
        }, {
            url: 'http://img.zing.vn/products/dmhmp1/skin-2015/js-full/modernizr.js',
        }, {
            url: 'http://img.zing.vn/products/dmhmp1/skin-2015/js-full/signals.min.js',
        }, {
            url: 'http://img.zing.vn/products/dmhmp1/skin-2015/js-full/swiper.jquery.js',
        }, {
            url: 'http://img.zing.vn/products/dmhmp1/skin-2015/js-full/hasher.min.js',
        }, {
            url: 'http://img.zing.vn/products/dmhmp1/skin-2015/js-full/crossroads.min.js',
        }, {
            url: 'http://img.zing.vn/products/dmhmp1/skin-2015/js-full/main.js',
        });
    }

    basket.require({
        url: 'http://img.zing.vn/products/vendor/jquery/jquery.min.js',
        key: 'jquery'
    }).then(requireScriptsDependingOnJQueryAndRactive)
}());