/*! dnd.mom 2015-03-27 */ ! function(a, b) {
    function c(a) {
        var b = a.length,
            c = fb.type(a);
        return fb.isWindow(a) ? !1 : 1 === a.nodeType && b ? !0 : "array" === c || "function" !== c && (0 === b || "number" == typeof b && b > 0 && b - 1 in a)
    }

    function d(a) {
        var b = ob[a] = {};
        return fb.each(a.match(hb) || [], function(a, c) {
            b[c] = !0
        }), b
    }

    function e() {
        Object.defineProperty(this.cache = {}, 0, {
            get: function() {
                return {}
            }
        }), this.expando = fb.expando + Math.random()
    }

    function f(a, c, d) {
        var e;
        if (d === b && 1 === a.nodeType)
            if (e = "data-" + c.replace(sb, "-$1").toLowerCase(), d = a.getAttribute(e), "string" == typeof d) {
                try {
                    d = "true" === d ? !0 : "false" === d ? !1 : "null" === d ? null : +d + "" === d ? +d : rb.test(d) ? JSON.parse(d) : d
                } catch (f) {}
                pb.set(a, c, d)
            } else d = b;
        return d
    }

    function g() {
        return !0
    }

    function h() {
        return !1
    }

    function i() {
        try {
            return T.activeElement
        } catch (a) {}
    }

    function j(a, b) {
        for (;
            (a = a[b]) && 1 !== a.nodeType;);
        return a
    }

    function k(a, b, c) {
        if (fb.isFunction(b)) return fb.grep(a, function(a, d) {
            return !!b.call(a, d, a) !== c
        });
        if (b.nodeType) return fb.grep(a, function(a) {
            return a === b !== c
        });
        if ("string" == typeof b) {
            if (Cb.test(b)) return fb.filter(b, a, c);
            b = fb.filter(b, a)
        }
        return fb.grep(a, function(a) {
            return bb.call(b, a) >= 0 !== c
        })
    }

    function l(a, b) {
        return fb.nodeName(a, "table") && fb.nodeName(1 === b.nodeType ? b : b.firstChild, "tr") ? a.getElementsByTagName("tbody")[0] || a.appendChild(a.ownerDocument.createElement("tbody")) : a
    }

    function m(a) {
        return a.type = (null !== a.getAttribute("type")) + "/" + a.type, a
    }

    function n(a) {
        var b = Mb.exec(a.type);
        return b ? a.type = b[1] : a.removeAttribute("type"), a
    }

    function o(a, b) {
        for (var c = a.length, d = 0; c > d; d++) qb.set(a[d], "globalEval", !b || qb.get(b[d], "globalEval"))
    }

    function p(a, b) {
        var c, d, e, f, g, h, i, j;
        if (1 === b.nodeType) {
            if (qb.hasData(a) && (f = qb.access(a), g = fb.extend({}, f), j = f.events, qb.set(b, g), j)) {
                delete g.handle, g.events = {};
                for (e in j)
                    for (c = 0, d = j[e].length; d > c; c++) fb.event.add(b, e, j[e][c])
            }
            pb.hasData(a) && (h = pb.access(a), i = fb.extend({}, h), pb.set(b, i))
        }
    }

    function q(a, c) {
        var d = a.getElementsByTagName ? a.getElementsByTagName(c || "*") : a.querySelectorAll ? a.querySelectorAll(c || "*") : [];
        return c === b || c && fb.nodeName(a, c) ? fb.merge([a], d) : d
    }

    function r(a, b) {
        var c = b.nodeName.toLowerCase();
        "input" === c && Jb.test(a.type) ? b.checked = a.checked : ("input" === c || "textarea" === c) && (b.defaultValue = a.defaultValue)
    }

    function s(a, b) {
        if (b in a) return b;
        for (var c = b.charAt(0).toUpperCase() + b.slice(1), d = b, e = $b.length; e--;)
            if (b = $b[e] + c, b in a) return b;
        return d
    }

    function t(a, b) {
        return a = b || a, "none" === fb.css(a, "display") || !fb.contains(a.ownerDocument, a)
    }

    function u(b) {
        return a.getComputedStyle(b, null)
    }

    function v(a, b) {
        for (var c, d, e, f = [], g = 0, h = a.length; h > g; g++) d = a[g], d.style && (f[g] = qb.get(d, "olddisplay"), c = d.style.display, b ? (f[g] || "none" !== c || (d.style.display = ""), "" === d.style.display && t(d) && (f[g] = qb.access(d, "olddisplay", z(d.nodeName)))) : f[g] || (e = t(d), (c && "none" !== c || !e) && qb.set(d, "olddisplay", e ? c : fb.css(d, "display"))));
        for (g = 0; h > g; g++) d = a[g], d.style && (b && "none" !== d.style.display && "" !== d.style.display || (d.style.display = b ? f[g] || "" : "none"));
        return a
    }

    function w(a, b, c) {
        var d = Tb.exec(b);
        return d ? Math.max(0, d[1] - (c || 0)) + (d[2] || "px") : b
    }

    function x(a, b, c, d, e) {
        for (var f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0, g = 0; 4 > f; f += 2) "margin" === c && (g += fb.css(a, c + Zb[f], !0, e)), d ? ("content" === c && (g -= fb.css(a, "padding" + Zb[f], !0, e)), "margin" !== c && (g -= fb.css(a, "border" + Zb[f] + "Width", !0, e))) : (g += fb.css(a, "padding" + Zb[f], !0, e), "padding" !== c && (g += fb.css(a, "border" + Zb[f] + "Width", !0, e)));
        return g
    }

    function y(a, b, c) {
        var d = !0,
            e = "width" === b ? a.offsetWidth : a.offsetHeight,
            f = u(a),
            g = fb.support.boxSizing && "border-box" === fb.css(a, "boxSizing", !1, f);
        if (0 >= e || null == e) {
            if (e = Pb(a, b, f), (0 > e || null == e) && (e = a.style[b]), Ub.test(e)) return e;
            d = g && (fb.support.boxSizingReliable || e === a.style[b]), e = parseFloat(e) || 0
        }
        return e + x(a, b, c || (g ? "border" : "content"), d, f) + "px"
    }

    function z(a) {
        var b = T,
            c = Wb[a];
        return c || (c = A(a, b), "none" !== c && c || (Qb = (Qb || fb("<iframe frameborder='0' width='0' height='0'/>").css("cssText", "display:block !important")).appendTo(b.documentElement), b = (Qb[0].contentWindow || Qb[0].contentDocument).document, b.write("<!doctype html><html><body>"), b.close(), c = A(a, b), Qb.detach()), Wb[a] = c), c
    }

    function A(a, b) {
        var c = fb(b.createElement(a)).appendTo(b.body),
            d = fb.css(c[0], "display");
        return c.remove(), d
    }

    function B(a, b, c, d) {
        var e;
        if (fb.isArray(b)) fb.each(b, function(b, e) {
            c || ac.test(a) ? d(a, e) : B(a + "[" + ("object" == typeof e ? b : "") + "]", e, c, d)
        });
        else if (c || "object" !== fb.type(b)) d(a, b);
        else
            for (e in b) B(a + "[" + e + "]", b[e], c, d)
    }

    function C(a) {
        return function(b, c) {
            "string" != typeof b && (c = b, b = "*");
            var d, e = 0,
                f = b.toLowerCase().match(hb) || [];
            if (fb.isFunction(c))
                for (; d = f[e++];) "+" === d[0] ? (d = d.slice(1) || "*", (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c)
        }
    }

    function D(a, c, d, e) {
        function f(i) {
            var j;
            return g[i] = !0, fb.each(a[i] || [], function(a, i) {
                var k = i(c, d, e);
                return "string" != typeof k || h || g[k] ? h ? !(j = k) : b : (c.dataTypes.unshift(k), f(k), !1)
            }), j
        }
        var g = {},
            h = a === rc;
        return f(c.dataTypes[0]) || !g["*"] && f("*")
    }

    function E(a, c) {
        var d, e, f = fb.ajaxSettings.flatOptions || {};
        for (d in c) c[d] !== b && ((f[d] ? a : e || (e = {}))[d] = c[d]);
        return e && fb.extend(!0, a, e), a
    }

    function F(a, c, d) {
        for (var e, f, g, h, i = a.contents, j = a.dataTypes;
            "*" === j[0];) j.shift(), e === b && (e = a.mimeType || c.getResponseHeader("Content-Type"));
        if (e)
            for (f in i)
                if (i[f] && i[f].test(e)) {
                    j.unshift(f);
                    break
                }
        if (j[0] in d) g = j[0];
        else {
            for (f in d) {
                if (!j[0] || a.converters[f + " " + j[0]]) {
                    g = f;
                    break
                }
                h || (h = f)
            }
            g = g || h
        }
        return g ? (g !== j[0] && j.unshift(g), d[g]) : b
    }

    function G(a, b, c, d) {
        var e, f, g, h, i, j = {},
            k = a.dataTypes.slice();
        if (k[1])
            for (g in a.converters) j[g.toLowerCase()] = a.converters[g];
        for (f = k.shift(); f;)
            if (a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), i = f, f = k.shift())
                if ("*" === f) f = i;
                else if ("*" !== i && i !== f) {
            if (g = j[i + " " + f] || j["* " + f], !g)
                for (e in j)
                    if (h = e.split(" "), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]])) {
                        g === !0 ? g = j[e] : j[e] !== !0 && (f = h[0], k.unshift(h[1]));
                        break
                    }
            if (g !== !0)
                if (g && a["throws"]) b = g(b);
                else try {
                    b = g(b)
                } catch (l) {
                    return {
                        state: "parsererror",
                        error: g ? l : "No conversion from " + i + " to " + f
                    }
                }
        }
        return {
            state: "success",
            data: b
        }
    }

    function H() {
        return setTimeout(function() {
            Ac = b
        }), Ac = fb.now()
    }

    function I(a, b) {
        fb.each(b, function(b, c) {
            for (var d = (Gc[b] || []).concat(Gc["*"]), e = 0, f = d.length; f > e; e++)
                if (d[e].call(a, b, c)) return
        })
    }

    function J(a, b, c) {
        var d, e, f = 0,
            g = Fc.length,
            h = fb.Deferred().always(function() {
                delete i.elem
            }),
            i = function() {
                if (e) return !1;
                for (var b = Ac || H(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; i > g; g++) j.tweens[g].run(f);
                return h.notifyWith(a, [j, f, c]), 1 > f && i ? c : (h.resolveWith(a, [j]), !1)
            },
            j = h.promise({
                elem: a,
                props: fb.extend({}, b),
                opts: fb.extend(!0, {
                    specialEasing: {}
                }, c),
                originalProperties: b,
                originalOptions: c,
                startTime: Ac || H(),
                duration: c.duration,
                tweens: [],
                createTween: function(b, c) {
                    var d = fb.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
                    return j.tweens.push(d), d
                },
                stop: function(b) {
                    var c = 0,
                        d = b ? j.tweens.length : 0;
                    if (e) return this;
                    for (e = !0; d > c; c++) j.tweens[c].run(1);
                    return b ? h.resolveWith(a, [j, b]) : h.rejectWith(a, [j, b]), this
                }
            }),
            k = j.props;
        for (K(k, j.opts.specialEasing); g > f; f++)
            if (d = Fc[f].call(j, a, k, j.opts)) return d;
        return I(j, k), fb.isFunction(j.opts.start) && j.opts.start.call(a, j), fb.fx.timer(fb.extend(i, {
            elem: a,
            anim: j,
            queue: j.opts.queue
        })), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always)
    }

    function K(a, b) {
        var c, d, e, f, g;
        for (c in a)
            if (d = fb.camelCase(c), e = b[d], f = a[c], fb.isArray(f) && (e = f[1], f = a[c] = f[0]), c !== d && (a[d] = f, delete a[c]), g = fb.cssHooks[d], g && "expand" in g) {
                f = g.expand(f), delete a[d];
                for (c in f) c in a || (a[c] = f[c], b[c] = e)
            } else b[d] = e
    }

    function L(a, c, d) {
        var e, f, g, h, i, j, k, l, m, n = this,
            o = a.style,
            p = {},
            q = [],
            r = a.nodeType && t(a);
        d.queue || (l = fb._queueHooks(a, "fx"), null == l.unqueued && (l.unqueued = 0, m = l.empty.fire, l.empty.fire = function() {
            l.unqueued || m()
        }), l.unqueued++, n.always(function() {
            n.always(function() {
                l.unqueued--, fb.queue(a, "fx").length || l.empty.fire()
            })
        })), 1 === a.nodeType && ("height" in c || "width" in c) && (d.overflow = [o.overflow, o.overflowX, o.overflowY], "inline" === fb.css(a, "display") && "none" === fb.css(a, "float") && (o.display = "inline-block")), d.overflow && (o.overflow = "hidden", n.always(function() {
            o.overflow = d.overflow[0], o.overflowX = d.overflow[1], o.overflowY = d.overflow[2]
        })), i = qb.get(a, "fxshow");
        for (e in c)
            if (g = c[e], Cc.exec(g)) {
                if (delete c[e], j = j || "toggle" === g, g === (r ? "hide" : "show")) {
                    if ("show" !== g || i === b || i[e] === b) continue;
                    r = !0
                }
                q.push(e)
            }
        if (h = q.length) {
            i = qb.get(a, "fxshow") || qb.access(a, "fxshow", {}), "hidden" in i && (r = i.hidden), j && (i.hidden = !r), r ? fb(a).show() : n.done(function() {
                fb(a).hide()
            }), n.done(function() {
                var b;
                qb.remove(a, "fxshow");
                for (b in p) fb.style(a, b, p[b])
            });
            for (e = 0; h > e; e++) f = q[e], k = n.createTween(f, r ? i[f] : 0), p[f] = i[f] || fb.style(a, f), f in i || (i[f] = k.start, r && (k.end = k.start, k.start = "width" === f || "height" === f ? 1 : 0))
        }
    }

    function M(a, b, c, d, e) {
        return new M.prototype.init(a, b, c, d, e)
    }

    function N(a, b) {
        var c, d = {
                height: a
            },
            e = 0;
        for (b = b ? 1 : 0; 4 > e; e += 2 - b) c = Zb[e], d["margin" + c] = d["padding" + c] = a;
        return b && (d.opacity = d.width = a), d
    }

    function O(a) {
        return fb.isWindow(a) ? a : 9 === a.nodeType && a.defaultView
    }
    var P, Q, R = typeof b,
        S = a.location,
        T = a.document,
        U = T.documentElement,
        V = a.jQuery,
        W = a.$,
        X = {},
        Y = [],
        Z = "2.0.0",
        $ = Y.concat,
        _ = Y.push,
        ab = Y.slice,
        bb = Y.indexOf,
        cb = X.toString,
        db = X.hasOwnProperty,
        eb = Z.trim,
        fb = function(a, b) {
            return new fb.fn.init(a, b, P)
        },
        gb = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        hb = /\S+/g,
        ib = /^(?:(<[\w\W]+>)[^>]*|#([\w-]*))$/,
        jb = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        kb = /^-ms-/,
        lb = /-([\da-z])/gi,
        mb = function(a, b) {
            return b.toUpperCase()
        },
        nb = function() {
            T.removeEventListener("DOMContentLoaded", nb, !1), a.removeEventListener("load", nb, !1), fb.ready()
        };
    fb.fn = fb.prototype = {
            jquery: Z,
            constructor: fb,
            init: function(a, c, d) {
                var e, f;
                if (!a) return this;
                if ("string" == typeof a) {
                    if (e = "<" === a.charAt(0) && ">" === a.charAt(a.length - 1) && a.length >= 3 ? [null, a, null] : ib.exec(a), !e || !e[1] && c) return !c || c.jquery ? (c || d).find(a) : this.constructor(c).find(a);
                    if (e[1]) {
                        if (c = c instanceof fb ? c[0] : c, fb.merge(this, fb.parseHTML(e[1], c && c.nodeType ? c.ownerDocument || c : T, !0)), jb.test(e[1]) && fb.isPlainObject(c))
                            for (e in c) fb.isFunction(this[e]) ? this[e](c[e]) : this.attr(e, c[e]);
                        return this
                    }
                    return f = T.getElementById(e[2]), f && f.parentNode && (this.length = 1, this[0] = f), this.context = T, this.selector = a, this
                }
                return a.nodeType ? (this.context = this[0] = a, this.length = 1, this) : fb.isFunction(a) ? d.ready(a) : (a.selector !== b && (this.selector = a.selector, this.context = a.context), fb.makeArray(a, this))
            },
            selector: "",
            length: 0,
            toArray: function() {
                return ab.call(this)
            },
            get: function(a) {
                return null == a ? this.toArray() : 0 > a ? this[this.length + a] : this[a]
            },
            pushStack: function(a) {
                var b = fb.merge(this.constructor(), a);
                return b.prevObject = this, b.context = this.context, b
            },
            each: function(a, b) {
                return fb.each(this, a, b)
            },
            ready: function(a) {
                return fb.ready.promise().done(a), this
            },
            slice: function() {
                return this.pushStack(ab.apply(this, arguments))
            },
            first: function() {
                return this.eq(0)
            },
            last: function() {
                return this.eq(-1)
            },
            eq: function(a) {
                var b = this.length,
                    c = +a + (0 > a ? b : 0);
                return this.pushStack(c >= 0 && b > c ? [this[c]] : [])
            },
            map: function(a) {
                return this.pushStack(fb.map(this, function(b, c) {
                    return a.call(b, c, b)
                }))
            },
            end: function() {
                return this.prevObject || this.constructor(null)
            },
            push: _,
            sort: [].sort,
            splice: [].splice
        }, fb.fn.init.prototype = fb.fn, fb.extend = fb.fn.extend = function() {
            var a, c, d, e, f, g, h = arguments[0] || {},
                i = 1,
                j = arguments.length,
                k = !1;
            for ("boolean" == typeof h && (k = h, h = arguments[1] || {}, i = 2), "object" == typeof h || fb.isFunction(h) || (h = {}), j === i && (h = this, --i); j > i; i++)
                if (null != (a = arguments[i]))
                    for (c in a) d = h[c], e = a[c], h !== e && (k && e && (fb.isPlainObject(e) || (f = fb.isArray(e))) ? (f ? (f = !1, g = d && fb.isArray(d) ? d : []) : g = d && fb.isPlainObject(d) ? d : {}, h[c] = fb.extend(k, g, e)) : e !== b && (h[c] = e));
            return h
        }, fb.extend({
            expando: "jQuery" + (Z + Math.random()).replace(/\D/g, ""),
            noConflict: function(b) {
                return a.$ === fb && (a.$ = W), b && a.jQuery === fb && (a.jQuery = V), fb
            },
            isReady: !1,
            readyWait: 1,
            holdReady: function(a) {
                a ? fb.readyWait++ : fb.ready(!0)
            },
            ready: function(a) {
                (a === !0 ? --fb.readyWait : fb.isReady) || (fb.isReady = !0, a !== !0 && --fb.readyWait > 0 || (Q.resolveWith(T, [fb]), fb.fn.trigger && fb(T).trigger("ready").off("ready")))
            },
            isFunction: function(a) {
                return "function" === fb.type(a)
            },
            isArray: Array.isArray,
            isWindow: function(a) {
                return null != a && a === a.window
            },
            isNumeric: function(a) {
                return !isNaN(parseFloat(a)) && isFinite(a)
            },
            type: function(a) {
                return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? X[cb.call(a)] || "object" : typeof a
            },
            isPlainObject: function(a) {
                if ("object" !== fb.type(a) || a.nodeType || fb.isWindow(a)) return !1;
                try {
                    if (a.constructor && !db.call(a.constructor.prototype, "isPrototypeOf")) return !1
                } catch (b) {
                    return !1
                }
                return !0
            },
            isEmptyObject: function(a) {
                var b;
                for (b in a) return !1;
                return !0
            },
            error: function(a) {
                throw Error(a)
            },
            parseHTML: function(a, b, c) {
                if (!a || "string" != typeof a) return null;
                "boolean" == typeof b && (c = b, b = !1), b = b || T;
                var d = jb.exec(a),
                    e = !c && [];
                return d ? [b.createElement(d[1])] : (d = fb.buildFragment([a], b, e), e && fb(e).remove(), fb.merge([], d.childNodes))
            },
            parseJSON: JSON.parse,
            parseXML: function(a) {
                var c, d;
                if (!a || "string" != typeof a) return null;
                try {
                    d = new DOMParser, c = d.parseFromString(a, "text/xml")
                } catch (e) {
                    c = b
                }
                return (!c || c.getElementsByTagName("parsererror").length) && fb.error("Invalid XML: " + a), c
            },
            noop: function() {},
            globalEval: function(a) {
                var b, c = eval;
                a = fb.trim(a), a && (1 === a.indexOf("use strict") ? (b = T.createElement("script"), b.text = a, T.head.appendChild(b).parentNode.removeChild(b)) : c(a))
            },
            camelCase: function(a) {
                return a.replace(kb, "ms-").replace(lb, mb)
            },
            nodeName: function(a, b) {
                return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase()
            },
            each: function(a, b, d) {
                var e, f = 0,
                    g = a.length,
                    h = c(a);
                if (d) {
                    if (h)
                        for (; g > f && (e = b.apply(a[f], d), e !== !1); f++);
                    else
                        for (f in a)
                            if (e = b.apply(a[f], d), e === !1) break
                } else if (h)
                    for (; g > f && (e = b.call(a[f], f, a[f]), e !== !1); f++);
                else
                    for (f in a)
                        if (e = b.call(a[f], f, a[f]), e === !1) break; return a
            },
            trim: function(a) {
                return null == a ? "" : eb.call(a)
            },
            makeArray: function(a, b) {
                var d = b || [];
                return null != a && (c(Object(a)) ? fb.merge(d, "string" == typeof a ? [a] : a) : _.call(d, a)), d
            },
            inArray: function(a, b, c) {
                return null == b ? -1 : bb.call(b, a, c)
            },
            merge: function(a, c) {
                var d = c.length,
                    e = a.length,
                    f = 0;
                if ("number" == typeof d)
                    for (; d > f; f++) a[e++] = c[f];
                else
                    for (; c[f] !== b;) a[e++] = c[f++];
                return a.length = e, a
            },
            grep: function(a, b, c) {
                var d, e = [],
                    f = 0,
                    g = a.length;
                for (c = !!c; g > f; f++) d = !!b(a[f], f), c !== d && e.push(a[f]);
                return e
            },
            map: function(a, b, d) {
                var e, f = 0,
                    g = a.length,
                    h = c(a),
                    i = [];
                if (h)
                    for (; g > f; f++) e = b(a[f], f, d), null != e && (i[i.length] = e);
                else
                    for (f in a) e = b(a[f], f, d), null != e && (i[i.length] = e);
                return $.apply([], i)
            },
            guid: 1,
            proxy: function(a, c) {
                var d, e, f;
                return "string" == typeof c && (d = a[c], c = a, a = d), fb.isFunction(a) ? (e = ab.call(arguments, 2), f = function() {
                    return a.apply(c || this, e.concat(ab.call(arguments)))
                }, f.guid = a.guid = a.guid || fb.guid++, f) : b
            },
            access: function(a, c, d, e, f, g, h) {
                var i = 0,
                    j = a.length,
                    k = null == d;
                if ("object" === fb.type(d)) {
                    f = !0;
                    for (i in d) fb.access(a, c, i, d[i], !0, g, h)
                } else if (e !== b && (f = !0, fb.isFunction(e) || (h = !0), k && (h ? (c.call(a, e), c = null) : (k = c, c = function(a, b, c) {
                        return k.call(fb(a), c)
                    })), c))
                    for (; j > i; i++) c(a[i], d, h ? e : e.call(a[i], i, c(a[i], d)));
                return f ? a : k ? c.call(a) : j ? c(a[0], d) : g
            },
            now: Date.now,
            swap: function(a, b, c, d) {
                var e, f, g = {};
                for (f in b) g[f] = a.style[f], a.style[f] = b[f];
                e = c.apply(a, d || []);
                for (f in b) a.style[f] = g[f];
                return e
            }
        }), fb.ready.promise = function(b) {
            return Q || (Q = fb.Deferred(), "complete" === T.readyState ? setTimeout(fb.ready) : (T.addEventListener("DOMContentLoaded", nb, !1), a.addEventListener("load", nb, !1))), Q.promise(b)
        }, fb.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(a, b) {
            X["[object " + b + "]"] = b.toLowerCase()
        }), P = fb(T),
        function(a, b) {
            function c(a) {
                return tb.test(a + "")
            }

            function d() {
                var a, b = [];
                return a = function(c, d) {
                    return b.push(c += " ") > A.cacheLength && delete a[b.shift()], a[c] = d
                }
            }

            function e(a) {
                return a[O] = !0, a
            }

            function f(a) {
                var b = H.createElement("div");
                try {
                    return !!a(b)
                } catch (c) {
                    return !1
                } finally {
                    b.parentNode && b.parentNode.removeChild(b), b = null
                }
            }

            function g(a, b, c, d) {
                var e, f, g, h, i, j, k, l, m, p;
                if ((b ? b.ownerDocument || b : P) !== H && G(b), b = b || H, c = c || [], !a || "string" != typeof a) return c;
                if (1 !== (h = b.nodeType) && 9 !== h) return [];
                if (J && !d) {
                    if (e = ub.exec(a))
                        if (g = e[1]) {
                            if (9 === h) {
                                if (f = b.getElementById(g), !f || !f.parentNode) return c;
                                if (f.id === g) return c.push(f), c
                            } else if (b.ownerDocument && (f = b.ownerDocument.getElementById(g)) && N(b, f) && f.id === g) return c.push(f), c
                        } else {
                            if (e[2]) return bb.apply(c, b.getElementsByTagName(a)), c;
                            if ((g = e[3]) && Q.getElementsByClassName && b.getElementsByClassName) return bb.apply(c, b.getElementsByClassName(g)), c
                        }
                    if (Q.qsa && (!K || !K.test(a))) {
                        if (l = k = O, m = b, p = 9 === h && a, 1 === h && "object" !== b.nodeName.toLowerCase()) {
                            for (j = n(a), (k = b.getAttribute("id")) ? l = k.replace(xb, "\\$&") : b.setAttribute("id", l), l = "[id='" + l + "'] ", i = j.length; i--;) j[i] = l + o(j[i]);
                            m = ob.test(a) && b.parentNode || b, p = j.join(",")
                        }
                        if (p) try {
                            return bb.apply(c, m.querySelectorAll(p)), c
                        } catch (q) {} finally {
                            k || b.removeAttribute("id")
                        }
                    }
                }
                return w(a.replace(lb, "$1"), b, c, d)
            }

            function h(a, b) {
                var c = b && a,
                    d = c && (~b.sourceIndex || Z) - (~a.sourceIndex || Z);
                if (d) return d;
                if (c)
                    for (; c = c.nextSibling;)
                        if (c === b) return -1;
                return a ? 1 : -1
            }

            function i(a, c, d) {
                var e;
                return d ? b : (e = a.getAttributeNode(c)) && e.specified ? e.value : a[c] === !0 ? c.toLowerCase() : null
            }

            function j(a, c, d) {
                var e;
                return d ? b : e = a.getAttribute(c, "type" === c.toLowerCase() ? 1 : 2)
            }

            function k(a) {
                return function(b) {
                    var c = b.nodeName.toLowerCase();
                    return "input" === c && b.type === a
                }
            }

            function l(a) {
                return function(b) {
                    var c = b.nodeName.toLowerCase();
                    return ("input" === c || "button" === c) && b.type === a
                }
            }

            function m(a) {
                return e(function(b) {
                    return b = +b, e(function(c, d) {
                        for (var e, f = a([], c.length, b), g = f.length; g--;) c[e = f[g]] && (c[e] = !(d[e] = c[e]))
                    })
                })
            }

            function n(a, b) {
                var c, d, e, f, h, i, j, k = U[a + " "];
                if (k) return b ? 0 : k.slice(0);
                for (h = a, i = [], j = A.preFilter; h;) {
                    (!c || (d = mb.exec(h))) && (d && (h = h.slice(d[0].length) || h), i.push(e = [])), c = !1, (d = nb.exec(h)) && (c = d.shift(), e.push({
                        value: c,
                        type: d[0].replace(lb, " ")
                    }), h = h.slice(c.length));
                    for (f in A.filter) !(d = sb[f].exec(h)) || j[f] && !(d = j[f](d)) || (c = d.shift(), e.push({
                        value: c,
                        type: f,
                        matches: d
                    }), h = h.slice(c.length));
                    if (!c) break
                }
                return b ? h.length : h ? g.error(a) : U(a, i).slice(0)
            }

            function o(a) {
                for (var b = 0, c = a.length, d = ""; c > b; b++) d += a[b].value;
                return d
            }

            function p(a, b, c) {
                var d = b.dir,
                    e = c && "parentNode" === d,
                    f = S++;
                return b.first ? function(b, c, f) {
                    for (; b = b[d];)
                        if (1 === b.nodeType || e) return a(b, c, f)
                } : function(b, c, g) {
                    var h, i, j, k = R + " " + f;
                    if (g) {
                        for (; b = b[d];)
                            if ((1 === b.nodeType || e) && a(b, c, g)) return !0
                    } else
                        for (; b = b[d];)
                            if (1 === b.nodeType || e)
                                if (j = b[O] || (b[O] = {}), (i = j[d]) && i[0] === k) {
                                    if ((h = i[1]) === !0 || h === z) return h === !0
                                } else if (i = j[d] = [k], i[1] = a(b, c, g) || z, i[1] === !0) return !0
                }
            }

            function q(a) {
                return a.length > 1 ? function(b, c, d) {
                    for (var e = a.length; e--;)
                        if (!a[e](b, c, d)) return !1;
                    return !0
                } : a[0]
            }

            function r(a, b, c, d, e) {
                for (var f, g = [], h = 0, i = a.length, j = null != b; i > h; h++)(f = a[h]) && (!c || c(f, d, e)) && (g.push(f), j && b.push(h));
                return g
            }

            function s(a, b, c, d, f, g) {
                return d && !d[O] && (d = s(d)), f && !f[O] && (f = s(f, g)), e(function(e, g, h, i) {
                    var j, k, l, m = [],
                        n = [],
                        o = g.length,
                        p = e || v(b || "*", h.nodeType ? [h] : h, []),
                        q = !a || !e && b ? p : r(p, m, a, h, i),
                        s = c ? f || (e ? a : o || d) ? [] : g : q;
                    if (c && c(q, s, h, i), d)
                        for (j = r(s, n), d(j, [], h, i), k = j.length; k--;)(l = j[k]) && (s[n[k]] = !(q[n[k]] = l));
                    if (e) {
                        if (f || a) {
                            if (f) {
                                for (j = [], k = s.length; k--;)(l = s[k]) && j.push(q[k] = l);
                                f(null, s = [], j, i)
                            }
                            for (k = s.length; k--;)(l = s[k]) && (j = f ? db.call(e, l) : m[k]) > -1 && (e[j] = !(g[j] = l))
                        }
                    } else s = r(s === g ? s.splice(o, s.length) : s), f ? f(null, g, s, i) : bb.apply(g, s)
                })
            }

            function t(a) {
                for (var b, c, d, e = a.length, f = A.relative[a[0].type], g = f || A.relative[" "], h = f ? 1 : 0, i = p(function(a) {
                        return a === b
                    }, g, !0), j = p(function(a) {
                        return db.call(b, a) > -1
                    }, g, !0), k = [function(a, c, d) {
                        return !f && (d || c !== E) || ((b = c).nodeType ? i(a, c, d) : j(a, c, d))
                    }]; e > h; h++)
                    if (c = A.relative[a[h].type]) k = [p(q(k), c)];
                    else {
                        if (c = A.filter[a[h].type].apply(null, a[h].matches), c[O]) {
                            for (d = ++h; e > d && !A.relative[a[d].type]; d++);
                            return s(h > 1 && q(k), h > 1 && o(a.slice(0, h - 1)).replace(lb, "$1"), c, d > h && t(a.slice(h, d)), e > d && t(a = a.slice(d)), e > d && o(a))
                        }
                        k.push(c)
                    }
                return q(k)
            }

            function u(a, b) {
                var c = 0,
                    d = b.length > 0,
                    f = a.length > 0,
                    h = function(e, h, i, j, k) {
                        var l, m, n, o = [],
                            p = 0,
                            q = "0",
                            s = e && [],
                            t = null != k,
                            u = E,
                            v = e || f && A.find.TAG("*", k && h.parentNode || h),
                            w = R += null == u ? 1 : Math.random() || .1;
                        for (t && (E = h !== H && h, z = c); null != (l = v[q]); q++) {
                            if (f && l) {
                                for (m = 0; n = a[m++];)
                                    if (n(l, h, i)) {
                                        j.push(l);
                                        break
                                    }
                                t && (R = w, z = ++c)
                            }
                            d && ((l = !n && l) && p--, e && s.push(l))
                        }
                        if (p += q, d && q !== p) {
                            for (m = 0; n = b[m++];) n(s, o, h, i);
                            if (e) {
                                if (p > 0)
                                    for (; q--;) s[q] || o[q] || (o[q] = _.call(j));
                                o = r(o)
                            }
                            bb.apply(j, o), t && !e && o.length > 0 && p + b.length > 1 && g.uniqueSort(j)
                        }
                        return t && (R = w, E = u), s
                    };
                return d ? e(h) : h
            }

            function v(a, b, c) {
                for (var d = 0, e = b.length; e > d; d++) g(a, b[d], c);
                return c
            }

            function w(a, b, c, d) {
                var e, f, g, h, i, j = n(a);
                if (!d && 1 === j.length) {
                    if (f = j[0] = j[0].slice(0), f.length > 2 && "ID" === (g = f[0]).type && 9 === b.nodeType && J && A.relative[f[1].type]) {
                        if (b = (A.find.ID(g.matches[0].replace(yb, zb), b) || [])[0], !b) return c;
                        a = a.slice(f.shift().value.length)
                    }
                    for (e = sb.needsContext.test(a) ? 0 : f.length; e-- && (g = f[e], !A.relative[h = g.type]);)
                        if ((i = A.find[h]) && (d = i(g.matches[0].replace(yb, zb), ob.test(f[0].type) && b.parentNode || b))) {
                            if (f.splice(e, 1), a = d.length && o(f), !a) return bb.apply(c, d), c;
                            break
                        }
                }
                return D(a, j)(d, b, !J, c, ob.test(a)), c
            }

            function x() {}
            var y, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O = "sizzle" + -new Date,
                P = a.document,
                Q = {},
                R = 0,
                S = 0,
                T = d(),
                U = d(),
                V = d(),
                W = !1,
                X = function() {
                    return 0
                },
                Y = typeof b,
                Z = 1 << 31,
                $ = [],
                _ = $.pop,
                ab = $.push,
                bb = $.push,
                cb = $.slice,
                db = $.indexOf || function(a) {
                    for (var b = 0, c = this.length; c > b; b++)
                        if (this[b] === a) return b;
                    return -1
                },
                eb = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
                gb = "[\\x20\\t\\r\\n\\f]",
                hb = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
                ib = hb.replace("w", "w#"),
                jb = "\\[" + gb + "*(" + hb + ")" + gb + "*(?:([*^$|!~]?=)" + gb + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + ib + ")|)|)" + gb + "*\\]",
                kb = ":(" + hb + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + jb.replace(3, 8) + ")*)|.*)\\)|)",
                lb = RegExp("^" + gb + "+|((?:^|[^\\\\])(?:\\\\.)*)" + gb + "+$", "g"),
                mb = RegExp("^" + gb + "*," + gb + "*"),
                nb = RegExp("^" + gb + "*([>+~]|" + gb + ")" + gb + "*"),
                ob = RegExp(gb + "*[+~]"),
                pb = RegExp("=" + gb + "*([^\\]'\"]*)" + gb + "*\\]", "g"),
                qb = RegExp(kb),
                rb = RegExp("^" + ib + "$"),
                sb = {
                    ID: RegExp("^#(" + hb + ")"),
                    CLASS: RegExp("^\\.(" + hb + ")"),
                    TAG: RegExp("^(" + hb.replace("w", "w*") + ")"),
                    ATTR: RegExp("^" + jb),
                    PSEUDO: RegExp("^" + kb),
                    CHILD: RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + gb + "*(even|odd|(([+-]|)(\\d*)n|)" + gb + "*(?:([+-]|)" + gb + "*(\\d+)|))" + gb + "*\\)|)", "i"),
                    "boolean": RegExp("^(?:" + eb + ")$", "i"),
                    needsContext: RegExp("^" + gb + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + gb + "*((?:-\\d)?\\d*)" + gb + "*\\)|)(?=[^-]|$)", "i")
                },
                tb = /^[^{]+\{\s*\[native \w/,
                ub = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                vb = /^(?:input|select|textarea|button)$/i,
                wb = /^h\d$/i,
                xb = /'|\\/g,
                yb = /\\([\da-fA-F]{1,6}[\x20\t\r\n\f]?|.)/g,
                zb = function(a, b) {
                    var c = "0x" + b - 65536;
                    return c !== c ? b : 0 > c ? String.fromCharCode(c + 65536) : String.fromCharCode(55296 | c >> 10, 56320 | 1023 & c)
                };
            try {
                bb.apply($ = cb.call(P.childNodes), P.childNodes), $[P.childNodes.length].nodeType
            } catch (Ab) {
                bb = {
                    apply: $.length ? function(a, b) {
                        ab.apply(a, cb.call(b))
                    } : function(a, b) {
                        for (var c = a.length, d = 0; a[c++] = b[d++];);
                        a.length = c - 1
                    }
                }
            }
            C = g.isXML = function(a) {
                var b = a && (a.ownerDocument || a).documentElement;
                return b ? "HTML" !== b.nodeName : !1
            }, G = g.setDocument = function(a) {
                var d = a ? a.ownerDocument || a : P;
                return d !== H && 9 === d.nodeType && d.documentElement ? (H = d, I = d.documentElement, J = !C(d), Q.getElementsByTagName = f(function(a) {
                    return a.appendChild(d.createComment("")), !a.getElementsByTagName("*").length
                }), Q.attributes = f(function(a) {
                    return a.className = "i", !a.getAttribute("className")
                }), Q.getElementsByClassName = f(function(a) {
                    return a.innerHTML = "<div class='a'></div><div class='a i'></div>", a.firstChild.className = "i", 2 === a.getElementsByClassName("i").length
                }), Q.sortDetached = f(function(a) {
                    return 1 & a.compareDocumentPosition(H.createElement("div"))
                }), Q.getById = f(function(a) {
                    return I.appendChild(a).id = O, !d.getElementsByName || !d.getElementsByName(O).length
                }), Q.getById ? (A.find.ID = function(a, b) {
                    if (typeof b.getElementById !== Y && J) {
                        var c = b.getElementById(a);
                        return c && c.parentNode ? [c] : []
                    }
                }, A.filter.ID = function(a) {
                    var b = a.replace(yb, zb);
                    return function(a) {
                        return a.getAttribute("id") === b
                    }
                }) : (A.find.ID = function(a, c) {
                    if (typeof c.getElementById !== Y && J) {
                        var d = c.getElementById(a);
                        return d ? d.id === a || typeof d.getAttributeNode !== Y && d.getAttributeNode("id").value === a ? [d] : b : []
                    }
                }, A.filter.ID = function(a) {
                    var b = a.replace(yb, zb);
                    return function(a) {
                        var c = typeof a.getAttributeNode !== Y && a.getAttributeNode("id");
                        return c && c.value === b
                    }
                }), A.find.TAG = Q.getElementsByTagName ? function(a, c) {
                    return typeof c.getElementsByTagName !== Y ? c.getElementsByTagName(a) : b
                } : function(a, b) {
                    var c, d = [],
                        e = 0,
                        f = b.getElementsByTagName(a);
                    if ("*" === a) {
                        for (; c = f[e++];) 1 === c.nodeType && d.push(c);
                        return d
                    }
                    return f
                }, A.find.CLASS = Q.getElementsByClassName && function(a, c) {
                    return typeof c.getElementsByClassName !== Y && J ? c.getElementsByClassName(a) : b
                }, L = [], K = [], (Q.qsa = c(d.querySelectorAll)) && (f(function(a) {
                    a.innerHTML = "<select><option selected=''></option></select>", a.querySelectorAll("[selected]").length || K.push("\\[" + gb + "*(?:value|" + eb + ")"), a.querySelectorAll(":checked").length || K.push(":checked")
                }), f(function(a) {
                    var b = H.createElement("input");
                    b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("t", ""), a.querySelectorAll("[t^='']").length && K.push("[*^$]=" + gb + "*(?:''|\"\")"), a.querySelectorAll(":enabled").length || K.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), K.push(",.*:")
                })), (Q.matchesSelector = c(M = I.webkitMatchesSelector || I.mozMatchesSelector || I.oMatchesSelector || I.msMatchesSelector)) && f(function(a) {
                    Q.disconnectedMatch = M.call(a, "div"), M.call(a, "[s!='']:x"), L.push("!=", kb)
                }), K = K.length && RegExp(K.join("|")), L = L.length && RegExp(L.join("|")), N = c(I.contains) || I.compareDocumentPosition ? function(a, b) {
                    var c = 9 === a.nodeType ? a.documentElement : a,
                        d = b && b.parentNode;
                    return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)))
                } : function(a, b) {
                    if (b)
                        for (; b = b.parentNode;)
                            if (b === a) return !0;
                    return !1
                }, X = I.compareDocumentPosition ? function(a, b) {
                    if (a === b) return W = !0, 0;
                    var c = b.compareDocumentPosition && a.compareDocumentPosition && a.compareDocumentPosition(b);
                    return c ? 1 & c || !Q.sortDetached && b.compareDocumentPosition(a) === c ? a === d || N(P, a) ? -1 : b === d || N(P, b) ? 1 : F ? db.call(F, a) - db.call(F, b) : 0 : 4 & c ? -1 : 1 : a.compareDocumentPosition ? -1 : 1
                } : function(a, b) {
                    var c, e = 0,
                        f = a.parentNode,
                        g = b.parentNode,
                        i = [a],
                        j = [b];
                    if (a === b) return W = !0, 0;
                    if (!f || !g) return a === d ? -1 : b === d ? 1 : f ? -1 : g ? 1 : F ? db.call(F, a) - db.call(F, b) : 0;
                    if (f === g) return h(a, b);
                    for (c = a; c = c.parentNode;) i.unshift(c);
                    for (c = b; c = c.parentNode;) j.unshift(c);
                    for (; i[e] === j[e];) e++;
                    return e ? h(i[e], j[e]) : i[e] === P ? -1 : j[e] === P ? 1 : 0
                }, H) : H
            }, g.matches = function(a, b) {
                return g(a, null, null, b)
            }, g.matchesSelector = function(a, b) {
                if ((a.ownerDocument || a) !== H && G(a), b = b.replace(pb, "='$1']"), !(!Q.matchesSelector || !J || L && L.test(b) || K && K.test(b))) try {
                    var c = M.call(a, b);
                    if (c || Q.disconnectedMatch || a.document && 11 !== a.document.nodeType) return c
                } catch (d) {}
                return g(b, H, null, [a]).length > 0
            }, g.contains = function(a, b) {
                return (a.ownerDocument || a) !== H && G(a), N(a, b)
            }, g.attr = function(a, c) {
                (a.ownerDocument || a) !== H && G(a);
                var d = A.attrHandle[c.toLowerCase()],
                    e = d && d(a, c, !J);
                return e === b ? Q.attributes || !J ? a.getAttribute(c) : (e = a.getAttributeNode(c)) && e.specified ? e.value : null : e
            }, g.error = function(a) {
                throw Error("Syntax error, unrecognized expression: " + a)
            }, g.uniqueSort = function(a) {
                var b, c = [],
                    d = 0,
                    e = 0;
                if (W = !Q.detectDuplicates, F = !Q.sortStable && a.slice(0), a.sort(X), W) {
                    for (; b = a[e++];) b === a[e] && (d = c.push(e));
                    for (; d--;) a.splice(c[d], 1)
                }
                return a
            }, B = g.getText = function(a) {
                var b, c = "",
                    d = 0,
                    e = a.nodeType;
                if (e) {
                    if (1 === e || 9 === e || 11 === e) {
                        if ("string" == typeof a.textContent) return a.textContent;
                        for (a = a.firstChild; a; a = a.nextSibling) c += B(a)
                    } else if (3 === e || 4 === e) return a.nodeValue
                } else
                    for (; b = a[d]; d++) c += B(b);
                return c
            }, A = g.selectors = {
                cacheLength: 50,
                createPseudo: e,
                match: sb,
                attrHandle: {},
                find: {},
                relative: {
                    ">": {
                        dir: "parentNode",
                        first: !0
                    },
                    " ": {
                        dir: "parentNode"
                    },
                    "+": {
                        dir: "previousSibling",
                        first: !0
                    },
                    "~": {
                        dir: "previousSibling"
                    }
                },
                preFilter: {
                    ATTR: function(a) {
                        return a[1] = a[1].replace(yb, zb), a[3] = (a[4] || a[5] || "").replace(yb, zb), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4)
                    },
                    CHILD: function(a) {
                        return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || g.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && g.error(a[0]), a
                    },
                    PSEUDO: function(a) {
                        var b, c = !a[5] && a[2];
                        return sb.CHILD.test(a[0]) ? null : (a[4] ? a[2] = a[4] : c && qb.test(c) && (b = n(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3))
                    }
                },
                filter: {
                    TAG: function(a) {
                        var b = a.replace(yb, zb).toLowerCase();
                        return "*" === a ? function() {
                            return !0
                        } : function(a) {
                            return a.nodeName && a.nodeName.toLowerCase() === b
                        }
                    },
                    CLASS: function(a) {
                        var b = T[a + " "];
                        return b || (b = RegExp("(^|" + gb + ")" + a + "(" + gb + "|$)")) && T(a, function(a) {
                            return b.test("string" == typeof a.className && a.className || typeof a.getAttribute !== Y && a.getAttribute("class") || "")
                        })
                    },
                    ATTR: function(a, b, c) {
                        return function(d) {
                            var e = g.attr(d, a);
                            return null == e ? "!=" === b : b ? (e += "", "=" === b ? e === c : "!=" === b ? e !== c : "^=" === b ? c && 0 === e.indexOf(c) : "*=" === b ? c && e.indexOf(c) > -1 : "$=" === b ? c && e.slice(-c.length) === c : "~=" === b ? (" " + e + " ").indexOf(c) > -1 : "|=" === b ? e === c || e.slice(0, c.length + 1) === c + "-" : !1) : !0
                        }
                    },
                    CHILD: function(a, b, c, d, e) {
                        var f = "nth" !== a.slice(0, 3),
                            g = "last" !== a.slice(-4),
                            h = "of-type" === b;
                        return 1 === d && 0 === e ? function(a) {
                            return !!a.parentNode
                        } : function(b, c, i) {
                            var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling",
                                q = b.parentNode,
                                r = h && b.nodeName.toLowerCase(),
                                s = !i && !h;
                            if (q) {
                                if (f) {
                                    for (; p;) {
                                        for (l = b; l = l[p];)
                                            if (h ? l.nodeName.toLowerCase() === r : 1 === l.nodeType) return !1;
                                        o = p = "only" === a && !o && "nextSibling"
                                    }
                                    return !0
                                }
                                if (o = [g ? q.firstChild : q.lastChild], g && s) {
                                    for (k = q[O] || (q[O] = {}), j = k[a] || [], n = j[0] === R && j[1], m = j[0] === R && j[2], l = n && q.childNodes[n]; l = ++n && l && l[p] || (m = n = 0) || o.pop();)
                                        if (1 === l.nodeType && ++m && l === b) {
                                            k[a] = [R, n, m];
                                            break
                                        }
                                } else if (s && (j = (b[O] || (b[O] = {}))[a]) && j[0] === R) m = j[1];
                                else
                                    for (;
                                        (l = ++n && l && l[p] || (m = n = 0) || o.pop()) && ((h ? l.nodeName.toLowerCase() !== r : 1 !== l.nodeType) || !++m || (s && ((l[O] || (l[O] = {}))[a] = [R, m]), l !== b)););
                                return m -= e, m === d || 0 === m % d && m / d >= 0
                            }
                        }
                    },
                    PSEUDO: function(a, b) {
                        var c, d = A.pseudos[a] || A.setFilters[a.toLowerCase()] || g.error("unsupported pseudo: " + a);
                        return d[O] ? d(b) : d.length > 1 ? (c = [a, a, "", b], A.setFilters.hasOwnProperty(a.toLowerCase()) ? e(function(a, c) {
                            for (var e, f = d(a, b), g = f.length; g--;) e = db.call(a, f[g]), a[e] = !(c[e] = f[g])
                        }) : function(a) {
                            return d(a, 0, c)
                        }) : d
                    }
                },
                pseudos: {
                    not: e(function(a) {
                        var b = [],
                            c = [],
                            d = D(a.replace(lb, "$1"));
                        return d[O] ? e(function(a, b, c, e) {
                            for (var f, g = d(a, null, e, []), h = a.length; h--;)(f = g[h]) && (a[h] = !(b[h] = f))
                        }) : function(a, e, f) {
                            return b[0] = a, d(b, null, f, c), !c.pop()
                        }
                    }),
                    has: e(function(a) {
                        return function(b) {
                            return g(a, b).length > 0
                        }
                    }),
                    contains: e(function(a) {
                        return function(b) {
                            return (b.textContent || b.innerText || B(b)).indexOf(a) > -1
                        }
                    }),
                    lang: e(function(a) {
                        return rb.test(a || "") || g.error("unsupported lang: " + a), a = a.replace(yb, zb).toLowerCase(),
                            function(b) {
                                var c;
                                do
                                    if (c = J ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) return c = c.toLowerCase(), c === a || 0 === c.indexOf(a + "-");
                                while ((b = b.parentNode) && 1 === b.nodeType);
                                return !1
                            }
                    }),
                    target: function(b) {
                        var c = a.location && a.location.hash;
                        return c && c.slice(1) === b.id
                    },
                    root: function(a) {
                        return a === I
                    },
                    focus: function(a) {
                        return a === H.activeElement && (!H.hasFocus || H.hasFocus()) && !!(a.type || a.href || ~a.tabIndex)
                    },
                    enabled: function(a) {
                        return a.disabled === !1
                    },
                    disabled: function(a) {
                        return a.disabled === !0
                    },
                    checked: function(a) {
                        var b = a.nodeName.toLowerCase();
                        return "input" === b && !!a.checked || "option" === b && !!a.selected
                    },
                    selected: function(a) {
                        return a.parentNode && a.parentNode.selectedIndex, a.selected === !0
                    },
                    empty: function(a) {
                        for (a = a.firstChild; a; a = a.nextSibling)
                            if (a.nodeName > "@" || 3 === a.nodeType || 4 === a.nodeType) return !1;
                        return !0
                    },
                    parent: function(a) {
                        return !A.pseudos.empty(a)
                    },
                    header: function(a) {
                        return wb.test(a.nodeName)
                    },
                    input: function(a) {
                        return vb.test(a.nodeName)
                    },
                    button: function(a) {
                        var b = a.nodeName.toLowerCase();
                        return "input" === b && "button" === a.type || "button" === b
                    },
                    text: function(a) {
                        var b;
                        return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || b.toLowerCase() === a.type)
                    },
                    first: m(function() {
                        return [0]
                    }),
                    last: m(function(a, b) {
                        return [b - 1]
                    }),
                    eq: m(function(a, b, c) {
                        return [0 > c ? c + b : c]
                    }),
                    even: m(function(a, b) {
                        for (var c = 0; b > c; c += 2) a.push(c);
                        return a
                    }),
                    odd: m(function(a, b) {
                        for (var c = 1; b > c; c += 2) a.push(c);
                        return a
                    }),
                    lt: m(function(a, b, c) {
                        for (var d = 0 > c ? c + b : c; --d >= 0;) a.push(d);
                        return a
                    }),
                    gt: m(function(a, b, c) {
                        for (var d = 0 > c ? c + b : c; b > ++d;) a.push(d);
                        return a
                    })
                }
            };
            for (y in {
                    radio: !0,
                    checkbox: !0,
                    file: !0,
                    password: !0,
                    image: !0
                }) A.pseudos[y] = k(y);
            for (y in {
                    submit: !0,
                    reset: !0
                }) A.pseudos[y] = l(y);
            D = g.compile = function(a, b) {
                var c, d = [],
                    e = [],
                    f = V[a + " "];
                if (!f) {
                    for (b || (b = n(a)), c = b.length; c--;) f = t(b[c]), f[O] ? d.push(f) : e.push(f);
                    f = V(a, u(e, d))
                }
                return f
            }, A.pseudos.nth = A.pseudos.eq, x.prototype = A.filters = A.pseudos, A.setFilters = new x, Q.sortStable = O.split("").sort(X).join("") === O, G(), [0, 0].sort(X), Q.detectDuplicates = W, f(function(a) {
                if (a.innerHTML = "<a href='#'></a>", "#" !== a.firstChild.getAttribute("href"))
                    for (var b = "type|href|height|width".split("|"), c = b.length; c--;) A.attrHandle[b[c]] = j
            }), f(function(a) {
                if (null != a.getAttribute("disabled"))
                    for (var b = eb.split("|"), c = b.length; c--;) A.attrHandle[b[c]] = i
            }), fb.find = g, fb.expr = g.selectors, fb.expr[":"] = fb.expr.pseudos, fb.unique = g.uniqueSort, fb.text = g.getText, fb.isXMLDoc = g.isXML, fb.contains = g.contains
        }(a);
    var ob = {};
    fb.Callbacks = function(a) {
        a = "string" == typeof a ? ob[a] || d(a) : fb.extend({}, a);
        var c, e, f, g, h, i, j = [],
            k = !a.once && [],
            l = function(b) {
                for (c = a.memory && b, e = !0, i = g || 0, g = 0, h = j.length, f = !0; j && h > i; i++)
                    if (j[i].apply(b[0], b[1]) === !1 && a.stopOnFalse) {
                        c = !1;
                        break
                    }
                f = !1, j && (k ? k.length && l(k.shift()) : c ? j = [] : m.disable())
            },
            m = {
                add: function() {
                    if (j) {
                        var b = j.length;
                        ! function d(b) {
                            fb.each(b, function(b, c) {
                                var e = fb.type(c);
                                "function" === e ? a.unique && m.has(c) || j.push(c) : c && c.length && "string" !== e && d(c)
                            })
                        }(arguments), f ? h = j.length : c && (g = b, l(c))
                    }
                    return this
                },
                remove: function() {
                    return j && fb.each(arguments, function(a, b) {
                        for (var c;
                            (c = fb.inArray(b, j, c)) > -1;) j.splice(c, 1), f && (h >= c && h--, i >= c && i--)
                    }), this
                },
                has: function(a) {
                    return a ? fb.inArray(a, j) > -1 : !(!j || !j.length)
                },
                empty: function() {
                    return j = [], h = 0, this
                },
                disable: function() {
                    return j = k = c = b, this
                },
                disabled: function() {
                    return !j
                },
                lock: function() {
                    return k = b, c || m.disable(), this
                },
                locked: function() {
                    return !k
                },
                fireWith: function(a, b) {
                    return b = b || [], b = [a, b.slice ? b.slice() : b], !j || e && !k || (f ? k.push(b) : l(b)), this
                },
                fire: function() {
                    return m.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!e
                }
            };
        return m
    }, fb.extend({
        Deferred: function(a) {
            var b = [
                    ["resolve", "done", fb.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", fb.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", fb.Callbacks("memory")]
                ],
                c = "pending",
                d = {
                    state: function() {
                        return c
                    },
                    always: function() {
                        return e.done(arguments).fail(arguments), this
                    },
                    then: function() {
                        var a = arguments;
                        return fb.Deferred(function(c) {
                            fb.each(b, function(b, f) {
                                var g = f[0],
                                    h = fb.isFunction(a[b]) && a[b];
                                e[f[1]](function() {
                                    var a = h && h.apply(this, arguments);
                                    a && fb.isFunction(a.promise) ? a.promise().done(c.resolve).fail(c.reject).progress(c.notify) : c[g + "With"](this === d ? c.promise() : this, h ? [a] : arguments)
                                })
                            }), a = null
                        }).promise()
                    },
                    promise: function(a) {
                        return null != a ? fb.extend(a, d) : d
                    }
                },
                e = {};
            return d.pipe = d.then, fb.each(b, function(a, f) {
                var g = f[2],
                    h = f[3];
                d[f[1]] = g.add, h && g.add(function() {
                    c = h
                }, b[1 ^ a][2].disable, b[2][2].lock), e[f[0]] = function() {
                    return e[f[0] + "With"](this === e ? d : this, arguments), this
                }, e[f[0] + "With"] = g.fireWith
            }), d.promise(e), a && a.call(e, e), e
        },
        when: function(a) {
            var b, c, d, e = 0,
                f = ab.call(arguments),
                g = f.length,
                h = 1 !== g || a && fb.isFunction(a.promise) ? g : 0,
                i = 1 === h ? a : fb.Deferred(),
                j = function(a, c, d) {
                    return function(e) {
                        c[a] = this, d[a] = arguments.length > 1 ? ab.call(arguments) : e, d === b ? i.notifyWith(c, d) : --h || i.resolveWith(c, d)
                    }
                };
            if (g > 1)
                for (b = Array(g), c = Array(g), d = Array(g); g > e; e++) f[e] && fb.isFunction(f[e].promise) ? f[e].promise().done(j(e, d, f)).fail(i.reject).progress(j(e, c, b)) : --h;
            return h || i.resolveWith(d, f), i.promise()
        }
    }), fb.support = function(b) {
        var c = T.createElement("input"),
            d = T.createDocumentFragment(),
            e = T.createElement("div"),
            f = T.createElement("select"),
            g = f.appendChild(T.createElement("option"));
        return c.type ? (c.type = "checkbox", b.checkOn = "" !== c.value, b.optSelected = g.selected, b.reliableMarginRight = !0, b.boxSizingReliable = !0, b.pixelPosition = !1, c.checked = !0, b.noCloneChecked = c.cloneNode(!0).checked, f.disabled = !0, b.optDisabled = !g.disabled, c = T.createElement("input"), c.value = "t", c.type = "radio", b.radioValue = "t" === c.value, c.setAttribute("checked", "t"), c.setAttribute("name", "t"), d.appendChild(c), b.checkClone = d.cloneNode(!0).cloneNode(!0).lastChild.checked, b.focusinBubbles = "onfocusin" in a, e.style.backgroundClip = "content-box", e.cloneNode(!0).style.backgroundClip = "", b.clearCloneStyle = "content-box" === e.style.backgroundClip, fb(function() {
            var c, d, f = "padding:0;margin:0;border:0;display:block;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box",
                g = T.getElementsByTagName("body")[0];
            g && (c = T.createElement("div"), c.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px", g.appendChild(c).appendChild(e), e.innerHTML = "", e.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%", fb.swap(g, null != g.style.zoom ? {
                zoom: 1
            } : {}, function() {
                b.boxSizing = 4 === e.offsetWidth
            }), a.getComputedStyle && (b.pixelPosition = "1%" !== (a.getComputedStyle(e, null) || {}).top, b.boxSizingReliable = "4px" === (a.getComputedStyle(e, null) || {
                width: "4px"
            }).width, d = e.appendChild(T.createElement("div")), d.style.cssText = e.style.cssText = f, d.style.marginRight = d.style.width = "0", e.style.width = "1px", b.reliableMarginRight = !parseFloat((a.getComputedStyle(d, null) || {}).marginRight)), g.removeChild(c))
        }), b) : b
    }({});
    var pb, qb, rb = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/,
        sb = /([A-Z])/g;
    e.uid = 1, e.accepts = function(a) {
        return a.nodeType ? 1 === a.nodeType || 9 === a.nodeType : !0
    }, e.prototype = {
        key: function(a) {
            if (!e.accepts(a)) return 0;
            var b = {},
                c = a[this.expando];
            if (!c) {
                c = e.uid++;
                try {
                    b[this.expando] = {
                        value: c
                    }, Object.defineProperties(a, b)
                } catch (d) {
                    b[this.expando] = c, fb.extend(a, b)
                }
            }
            return this.cache[c] || (this.cache[c] = {}), c
        },
        set: function(a, b, c) {
            var d, e = this.key(a),
                f = this.cache[e];
            if ("string" == typeof b) f[b] = c;
            else if (fb.isEmptyObject(f)) this.cache[e] = b;
            else
                for (d in b) f[d] = b[d]
        },
        get: function(a, c) {
            var d = this.cache[this.key(a)];
            return c === b ? d : d[c]
        },
        access: function(a, c, d) {
            return c === b || c && "string" == typeof c && d === b ? this.get(a, c) : (this.set(a, c, d), d !== b ? d : c)
        },
        remove: function(a, c) {
            var d, e, f = this.key(a),
                g = this.cache[f];
            if (c === b) this.cache[f] = {};
            else {
                fb.isArray(c) ? e = c.concat(c.map(fb.camelCase)) : c in g ? e = [c] : (e = fb.camelCase(c), e = e in g ? [e] : e.match(hb) || []), d = e.length;
                for (; d--;) delete g[e[d]]
            }
        },
        hasData: function(a) {
            return !fb.isEmptyObject(this.cache[a[this.expando]] || {})
        },
        discard: function(a) {
            delete this.cache[this.key(a)]
        }
    }, pb = new e, qb = new e, fb.extend({
        acceptData: e.accepts,
        hasData: function(a) {
            return pb.hasData(a) || qb.hasData(a)
        },
        data: function(a, b, c) {
            return pb.access(a, b, c)
        },
        removeData: function(a, b) {
            pb.remove(a, b)
        },
        _data: function(a, b, c) {
            return qb.access(a, b, c)
        },
        _removeData: function(a, b) {
            qb.remove(a, b)
        }
    }), fb.fn.extend({
        data: function(a, c) {
            var d, e, g = this[0],
                h = 0,
                i = null;
            if (a === b) {
                if (this.length && (i = pb.get(g), 1 === g.nodeType && !qb.get(g, "hasDataAttrs"))) {
                    for (d = g.attributes; d.length > h; h++) e = d[h].name, 0 === e.indexOf("data-") && (e = fb.camelCase(e.substring(5)), f(g, e, i[e]));
                    qb.set(g, "hasDataAttrs", !0)
                }
                return i
            }
            return "object" == typeof a ? this.each(function() {
                pb.set(this, a)
            }) : fb.access(this, function(c) {
                var d, e = fb.camelCase(a);
                if (g && c === b) {
                    if (d = pb.get(g, a), d !== b) return d;
                    if (d = pb.get(g, e), d !== b) return d;
                    if (d = f(g, e, b), d !== b) return d
                } else this.each(function() {
                    var d = pb.get(this, e);
                    pb.set(this, e, c), -1 !== a.indexOf("-") && d !== b && pb.set(this, a, c)
                })
            }, null, c, arguments.length > 1, null, !0)
        },
        removeData: function(a) {
            return this.each(function() {
                pb.remove(this, a)
            })
        }
    }), fb.extend({
        queue: function(a, c, d) {
            var e;
            return a ? (c = (c || "fx") + "queue", e = qb.get(a, c), d && (!e || fb.isArray(d) ? e = qb.access(a, c, fb.makeArray(d)) : e.push(d)), e || []) : b
        },
        dequeue: function(a, b) {
            b = b || "fx";
            var c = fb.queue(a, b),
                d = c.length,
                e = c.shift(),
                f = fb._queueHooks(a, b),
                g = function() {
                    fb.dequeue(a, b)
                };
            "inprogress" === e && (e = c.shift(), d--), f.cur = e, e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire()
        },
        _queueHooks: function(a, b) {
            var c = b + "queueHooks";
            return qb.get(a, c) || qb.access(a, c, {
                empty: fb.Callbacks("once memory").add(function() {
                    qb.remove(a, [b + "queue", c])
                })
            })
        }
    }), fb.fn.extend({
        queue: function(a, c) {
            var d = 2;
            return "string" != typeof a && (c = a, a = "fx", d--), d > arguments.length ? fb.queue(this[0], a) : c === b ? this : this.each(function() {
                var b = fb.queue(this, a, c);
                fb._queueHooks(this, a), "fx" === a && "inprogress" !== b[0] && fb.dequeue(this, a)
            })
        },
        dequeue: function(a) {
            return this.each(function() {
                fb.dequeue(this, a)
            })
        },
        delay: function(a, b) {
            return a = fb.fx ? fb.fx.speeds[a] || a : a, b = b || "fx", this.queue(b, function(b, c) {
                var d = setTimeout(b, a);
                c.stop = function() {
                    clearTimeout(d)
                }
            })
        },
        clearQueue: function(a) {
            return this.queue(a || "fx", [])
        },
        promise: function(a, c) {
            var d, e = 1,
                f = fb.Deferred(),
                g = this,
                h = this.length,
                i = function() {
                    --e || f.resolveWith(g, [g])
                };
            for ("string" != typeof a && (c = a, a = b), a = a || "fx"; h--;) d = qb.get(g[h], a + "queueHooks"), d && d.empty && (e++, d.empty.add(i));
            return i(), f.promise(c)
        }
    });
    var tb, ub, vb = /[\t\r\n]/g,
        wb = /\r/g,
        xb = /^(?:input|select|textarea|button)$/i;
    fb.fn.extend({
        attr: function(a, b) {
            return fb.access(this, fb.attr, a, b, arguments.length > 1)
        },
        removeAttr: function(a) {
            return this.each(function() {
                fb.removeAttr(this, a)
            })
        },
        prop: function(a, b) {
            return fb.access(this, fb.prop, a, b, arguments.length > 1)
        },
        removeProp: function(a) {
            return this.each(function() {
                delete this[fb.propFix[a] || a]
            })
        },
        addClass: function(a) {
            var b, c, d, e, f, g = 0,
                h = this.length,
                i = "string" == typeof a && a;
            if (fb.isFunction(a)) return this.each(function(b) {
                fb(this).addClass(a.call(this, b, this.className))
            });
            if (i)
                for (b = (a || "").match(hb) || []; h > g; g++)
                    if (c = this[g], d = 1 === c.nodeType && (c.className ? (" " + c.className + " ").replace(vb, " ") : " ")) {
                        for (f = 0; e = b[f++];) 0 > d.indexOf(" " + e + " ") && (d += e + " ");
                        c.className = fb.trim(d)
                    }
            return this
        },
        removeClass: function(a) {
            var b, c, d, e, f, g = 0,
                h = this.length,
                i = 0 === arguments.length || "string" == typeof a && a;
            if (fb.isFunction(a)) return this.each(function(b) {
                fb(this).removeClass(a.call(this, b, this.className))
            });
            if (i)
                for (b = (a || "").match(hb) || []; h > g; g++)
                    if (c = this[g], d = 1 === c.nodeType && (c.className ? (" " + c.className + " ").replace(vb, " ") : "")) {
                        for (f = 0; e = b[f++];)
                            for (; d.indexOf(" " + e + " ") >= 0;) d = d.replace(" " + e + " ", " ");
                        c.className = a ? fb.trim(d) : ""
                    }
            return this
        },
        toggleClass: function(a, b) {
            var c = typeof a,
                d = "boolean" == typeof b;
            return this.each(fb.isFunction(a) ? function(c) {
                fb(this).toggleClass(a.call(this, c, this.className, b), b)
            } : function() {
                if ("string" === c)
                    for (var e, f = 0, g = fb(this), h = b, i = a.match(hb) || []; e = i[f++];) h = d ? h : !g.hasClass(e), g[h ? "addClass" : "removeClass"](e);
                else(c === R || "boolean" === c) && (this.className && qb.set(this, "__className__", this.className), this.className = this.className || a === !1 ? "" : qb.get(this, "__className__") || "")
            })
        },
        hasClass: function(a) {
            for (var b = " " + a + " ", c = 0, d = this.length; d > c; c++)
                if (1 === this[c].nodeType && (" " + this[c].className + " ").replace(vb, " ").indexOf(b) >= 0) return !0;
            return !1
        },
        val: function(a) {
            var c, d, e, f = this[0];
            return arguments.length ? (e = fb.isFunction(a), this.each(function(d) {
                var f, g = fb(this);
                1 === this.nodeType && (f = e ? a.call(this, d, g.val()) : a, null == f ? f = "" : "number" == typeof f ? f += "" : fb.isArray(f) && (f = fb.map(f, function(a) {
                    return null == a ? "" : a + ""
                })), c = fb.valHooks[this.type] || fb.valHooks[this.nodeName.toLowerCase()], c && "set" in c && c.set(this, f, "value") !== b || (this.value = f))
            })) : f ? (c = fb.valHooks[f.type] || fb.valHooks[f.nodeName.toLowerCase()], c && "get" in c && (d = c.get(f, "value")) !== b ? d : (d = f.value, "string" == typeof d ? d.replace(wb, "") : null == d ? "" : d)) : void 0
        }
    }), fb.extend({
        valHooks: {
            option: {
                get: function(a) {
                    var b = a.attributes.value;
                    return !b || b.specified ? a.value : a.text
                }
            },
            select: {
                get: function(a) {
                    for (var b, c, d = a.options, e = a.selectedIndex, f = "select-one" === a.type || 0 > e, g = f ? null : [], h = f ? e + 1 : d.length, i = 0 > e ? h : f ? e : 0; h > i; i++)
                        if (c = d[i], !(!c.selected && i !== e || (fb.support.optDisabled ? c.disabled : null !== c.getAttribute("disabled")) || c.parentNode.disabled && fb.nodeName(c.parentNode, "optgroup"))) {
                            if (b = fb(c).val(), f) return b;
                            g.push(b)
                        }
                    return g
                },
                set: function(a, b) {
                    for (var c, d, e = a.options, f = fb.makeArray(b), g = e.length; g--;) d = e[g], (d.selected = fb.inArray(fb(d).val(), f) >= 0) && (c = !0);
                    return c || (a.selectedIndex = -1), f
                }
            }
        },
        attr: function(a, c, d) {
            var e, f, g = a.nodeType;
            return a && 3 !== g && 8 !== g && 2 !== g ? typeof a.getAttribute === R ? fb.prop(a, c, d) : (1 === g && fb.isXMLDoc(a) || (c = c.toLowerCase(), e = fb.attrHooks[c] || (fb.expr.match.boolean.test(c) ? ub : tb)), d === b ? e && "get" in e && null !== (f = e.get(a, c)) ? f : (f = fb.find.attr(a, c), null == f ? b : f) : null !== d ? e && "set" in e && (f = e.set(a, d, c)) !== b ? f : (a.setAttribute(c, d + ""), d) : (fb.removeAttr(a, c), b)) : void 0
        },
        removeAttr: function(a, b) {
            var c, d, e = 0,
                f = b && b.match(hb);
            if (f && 1 === a.nodeType)
                for (; c = f[e++];) d = fb.propFix[c] || c, fb.expr.match.boolean.test(c) && (a[d] = !1), a.removeAttribute(c)
        },
        attrHooks: {
            type: {
                set: function(a, b) {
                    if (!fb.support.radioValue && "radio" === b && fb.nodeName(a, "input")) {
                        var c = a.value;
                        return a.setAttribute("type", b), c && (a.value = c), b
                    }
                }
            }
        },
        propFix: {
            "for": "htmlFor",
            "class": "className"
        },
        prop: function(a, c, d) {
            var e, f, g, h = a.nodeType;
            return a && 3 !== h && 8 !== h && 2 !== h ? (g = 1 !== h || !fb.isXMLDoc(a), g && (c = fb.propFix[c] || c, f = fb.propHooks[c]), d !== b ? f && "set" in f && (e = f.set(a, d, c)) !== b ? e : a[c] = d : f && "get" in f && null !== (e = f.get(a, c)) ? e : a[c]) : void 0
        },
        propHooks: {
            tabIndex: {
                get: function(a) {
                    return a.hasAttribute("tabindex") || xb.test(a.nodeName) || a.href ? a.tabIndex : -1
                }
            }
        }
    }), ub = {
        set: function(a, b, c) {
            return b === !1 ? fb.removeAttr(a, c) : a.setAttribute(c, c), c
        }
    }, fb.each(fb.expr.match.boolean.source.match(/\w+/g), function(a, c) {
        var d = fb.expr.attrHandle[c] || fb.find.attr;
        fb.expr.attrHandle[c] = function(a, c, e) {
            var f = fb.expr.attrHandle[c],
                g = e ? b : (fb.expr.attrHandle[c] = b) != d(a, c, e) ? c.toLowerCase() : null;
            return fb.expr.attrHandle[c] = f, g
        }
    }), fb.support.optSelected || (fb.propHooks.selected = {
        get: function(a) {
            var b = a.parentNode;
            return b && b.parentNode && b.parentNode.selectedIndex, null
        }
    }), fb.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        fb.propFix[this.toLowerCase()] = this
    }), fb.each(["radio", "checkbox"], function() {
        fb.valHooks[this] = {
            set: function(a, c) {
                return fb.isArray(c) ? a.checked = fb.inArray(fb(a).val(), c) >= 0 : b
            }
        }, fb.support.checkOn || (fb.valHooks[this].get = function(a) {
            return null === a.getAttribute("value") ? "on" : a.value
        })
    });
    var yb = /^key/,
        zb = /^(?:mouse|contextmenu)|click/,
        Ab = /^(?:focusinfocus|focusoutblur)$/,
        Bb = /^([^.]*)(?:\.(.+)|)$/;
    fb.event = {
        global: {},
        add: function(a, c, d, e, f) {
            var g, h, i, j, k, l, m, n, o, p, q, r = qb.get(a);
            if (r) {
                for (d.handler && (g = d, d = g.handler, f = g.selector), d.guid || (d.guid = fb.guid++), (j = r.events) || (j = r.events = {}), (h = r.handle) || (h = r.handle = function(a) {
                        return typeof fb === R || a && fb.event.triggered === a.type ? b : fb.event.dispatch.apply(h.elem, arguments)
                    }, h.elem = a), c = (c || "").match(hb) || [""], k = c.length; k--;) i = Bb.exec(c[k]) || [], o = q = i[1], p = (i[2] || "").split(".").sort(), o && (m = fb.event.special[o] || {}, o = (f ? m.delegateType : m.bindType) || o, m = fb.event.special[o] || {}, l = fb.extend({
                    type: o,
                    origType: q,
                    data: e,
                    handler: d,
                    guid: d.guid,
                    selector: f,
                    needsContext: f && fb.expr.match.needsContext.test(f),
                    namespace: p.join(".")
                }, g), (n = j[o]) || (n = j[o] = [], n.delegateCount = 0, m.setup && m.setup.call(a, e, p, h) !== !1 || a.addEventListener && a.addEventListener(o, h, !1)), m.add && (m.add.call(a, l), l.handler.guid || (l.handler.guid = d.guid)), f ? n.splice(n.delegateCount++, 0, l) : n.push(l), fb.event.global[o] = !0);
                a = null
            }
        },
        remove: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, n, o, p, q = qb.hasData(a) && qb.get(a);
            if (q && (i = q.events)) {
                for (b = (b || "").match(hb) || [""], j = b.length; j--;)
                    if (h = Bb.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n) {
                        for (l = fb.event.special[n] || {}, n = (d ? l.delegateType : l.bindType) || n, m = i[n] || [], h = h[2] && RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)"), g = f = m.length; f--;) k = m[f], !e && p !== k.origType || c && c.guid !== k.guid || h && !h.test(k.namespace) || d && d !== k.selector && ("**" !== d || !k.selector) || (m.splice(f, 1), k.selector && m.delegateCount--, l.remove && l.remove.call(a, k));
                        g && !m.length && (l.teardown && l.teardown.call(a, o, q.handle) !== !1 || fb.removeEvent(a, n, q.handle), delete i[n])
                    } else
                        for (n in i) fb.event.remove(a, n + b[j], c, d, !0);
                fb.isEmptyObject(i) && (delete q.handle, qb.remove(a, "events"))
            }
        },
        trigger: function(c, d, e, f) {
            var g, h, i, j, k, l, m, n = [e || T],
                o = db.call(c, "type") ? c.type : c,
                p = db.call(c, "namespace") ? c.namespace.split(".") : [];
            if (h = i = e = e || T, 3 !== e.nodeType && 8 !== e.nodeType && !Ab.test(o + fb.event.triggered) && (o.indexOf(".") >= 0 && (p = o.split("."), o = p.shift(), p.sort()), k = 0 > o.indexOf(":") && "on" + o, c = c[fb.expando] ? c : new fb.Event(o, "object" == typeof c && c), c.isTrigger = f ? 2 : 3, c.namespace = p.join("."), c.namespace_re = c.namespace ? RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, c.result = b, c.target || (c.target = e), d = null == d ? [c] : fb.makeArray(d, [c]), m = fb.event.special[o] || {}, f || !m.trigger || m.trigger.apply(e, d) !== !1)) {
                if (!f && !m.noBubble && !fb.isWindow(e)) {
                    for (j = m.delegateType || o, Ab.test(j + o) || (h = h.parentNode); h; h = h.parentNode) n.push(h), i = h;
                    i === (e.ownerDocument || T) && n.push(i.defaultView || i.parentWindow || a)
                }
                for (g = 0;
                    (h = n[g++]) && !c.isPropagationStopped();) c.type = g > 1 ? j : m.bindType || o, l = (qb.get(h, "events") || {})[c.type] && qb.get(h, "handle"), l && l.apply(h, d), l = k && h[k], l && fb.acceptData(h) && l.apply && l.apply(h, d) === !1 && c.preventDefault();
                return c.type = o, f || c.isDefaultPrevented() || m._default && m._default.apply(n.pop(), d) !== !1 || !fb.acceptData(e) || k && fb.isFunction(e[o]) && !fb.isWindow(e) && (i = e[k], i && (e[k] = null), fb.event.triggered = o, e[o](), fb.event.triggered = b, i && (e[k] = i)), c.result
            }
        },
        dispatch: function(a) {
            a = fb.event.fix(a);
            var c, d, e, f, g, h = [],
                i = ab.call(arguments),
                j = (qb.get(this, "events") || {})[a.type] || [],
                k = fb.event.special[a.type] || {};
            if (i[0] = a, a.delegateTarget = this, !k.preDispatch || k.preDispatch.call(this, a) !== !1) {
                for (h = fb.event.handlers.call(this, a, j), c = 0;
                    (f = h[c++]) && !a.isPropagationStopped();)
                    for (a.currentTarget = f.elem, d = 0;
                        (g = f.handlers[d++]) && !a.isImmediatePropagationStopped();)(!a.namespace_re || a.namespace_re.test(g.namespace)) && (a.handleObj = g, a.data = g.data, e = ((fb.event.special[g.origType] || {}).handle || g.handler).apply(f.elem, i), e !== b && (a.result = e) === !1 && (a.preventDefault(), a.stopPropagation()));
                return k.postDispatch && k.postDispatch.call(this, a), a.result
            }
        },
        handlers: function(a, c) {
            var d, e, f, g, h = [],
                i = c.delegateCount,
                j = a.target;
            if (i && j.nodeType && (!a.button || "click" !== a.type))
                for (; j !== this; j = j.parentNode || this)
                    if (j.disabled !== !0 || "click" !== a.type) {
                        for (e = [], d = 0; i > d; d++) g = c[d], f = g.selector + " ", e[f] === b && (e[f] = g.needsContext ? fb(f, this).index(j) >= 0 : fb.find(f, this, null, [j]).length), e[f] && e.push(g);
                        e.length && h.push({
                            elem: j,
                            handlers: e
                        })
                    }
            return c.length > i && h.push({
                elem: this,
                handlers: c.slice(i)
            }), h
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(a, b) {
                return null == a.which && (a.which = null != b.charCode ? b.charCode : b.keyCode), a
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(a, c) {
                var d, e, f, g = c.button;
                return null == a.pageX && null != c.clientX && (d = a.target.ownerDocument || T, e = d.documentElement, f = d.body, a.pageX = c.clientX + (e && e.scrollLeft || f && f.scrollLeft || 0) - (e && e.clientLeft || f && f.clientLeft || 0), a.pageY = c.clientY + (e && e.scrollTop || f && f.scrollTop || 0) - (e && e.clientTop || f && f.clientTop || 0)), a.which || g === b || (a.which = 1 & g ? 1 : 2 & g ? 3 : 4 & g ? 2 : 0), a
            }
        },
        fix: function(a) {
            if (a[fb.expando]) return a;
            var b, c, d, e = a.type,
                f = a,
                g = this.fixHooks[e];
            for (g || (this.fixHooks[e] = g = zb.test(e) ? this.mouseHooks : yb.test(e) ? this.keyHooks : {}), d = g.props ? this.props.concat(g.props) : this.props, a = new fb.Event(f), b = d.length; b--;) c = d[b], a[c] = f[c];
            return 3 === a.target.nodeType && (a.target = a.target.parentNode), g.filter ? g.filter(a, f) : a
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    return this !== i() && this.focus ? (this.focus(), !1) : b
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    return this === i() && this.blur ? (this.blur(), !1) : b
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    return "checkbox" === this.type && this.click && fb.nodeName(this, "input") ? (this.click(), !1) : b
                },
                _default: function(a) {
                    return fb.nodeName(a.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(a) {
                    a.result !== b && (a.originalEvent.returnValue = a.result)
                }
            }
        },
        simulate: function(a, b, c, d) {
            var e = fb.extend(new fb.Event, c, {
                type: a,
                isSimulated: !0,
                originalEvent: {}
            });
            d ? fb.event.trigger(e, null, b) : fb.event.dispatch.call(b, e), e.isDefaultPrevented() && c.preventDefault()
        }
    }, fb.removeEvent = function(a, b, c) {
        a.removeEventListener && a.removeEventListener(b, c, !1)
    }, fb.Event = function(a, c) {
        return this instanceof fb.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || a.getPreventDefault && a.getPreventDefault() ? g : h) : this.type = a, c && fb.extend(this, c), this.timeStamp = a && a.timeStamp || fb.now(), this[fb.expando] = !0, b) : new fb.Event(a, c)
    }, fb.Event.prototype = {
        isDefaultPrevented: h,
        isPropagationStopped: h,
        isImmediatePropagationStopped: h,
        preventDefault: function() {
            var a = this.originalEvent;
            this.isDefaultPrevented = g, a && a.preventDefault && a.preventDefault()
        },
        stopPropagation: function() {
            var a = this.originalEvent;
            this.isPropagationStopped = g, a && a.stopPropagation && a.stopPropagation()
        },
        stopImmediatePropagation: function() {
            this.isImmediatePropagationStopped = g, this.stopPropagation()
        }
    }, fb.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout"
    }, function(a, b) {
        fb.event.special[a] = {
            delegateType: b,
            bindType: b,
            handle: function(a) {
                var c, d = this,
                    e = a.relatedTarget,
                    f = a.handleObj;
                return (!e || e !== d && !fb.contains(d, e)) && (a.type = f.origType, c = f.handler.apply(this, arguments), a.type = b), c
            }
        }
    }), fb.support.focusinBubbles || fb.each({
        focus: "focusin",
        blur: "focusout"
    }, function(a, b) {
        var c = 0,
            d = function(a) {
                fb.event.simulate(b, a.target, fb.event.fix(a), !0)
            };
        fb.event.special[b] = {
            setup: function() {
                0 === c++ && T.addEventListener(a, d, !0)
            },
            teardown: function() {
                0 === --c && T.removeEventListener(a, d, !0)
            }
        }
    }), fb.fn.extend({
        on: function(a, c, d, e, f) {
            var g, i;
            if ("object" == typeof a) {
                "string" != typeof c && (d = d || c, c = b);
                for (i in a) this.on(i, c, d, a[i], f);
                return this
            }
            if (null == d && null == e ? (e = c, d = c = b) : null == e && ("string" == typeof c ? (e = d, d = b) : (e = d, d = c, c = b)), e === !1) e = h;
            else if (!e) return this;
            return 1 === f && (g = e, e = function(a) {
                return fb().off(a), g.apply(this, arguments)
            }, e.guid = g.guid || (g.guid = fb.guid++)), this.each(function() {
                fb.event.add(this, a, e, d, c)
            })
        },
        one: function(a, b, c, d) {
            return this.on(a, b, c, d, 1)
        },
        off: function(a, c, d) {
            var e, f;
            if (a && a.preventDefault && a.handleObj) return e = a.handleObj, fb(a.delegateTarget).off(e.namespace ? e.origType + "." + e.namespace : e.origType, e.selector, e.handler), this;
            if ("object" == typeof a) {
                for (f in a) this.off(f, c, a[f]);
                return this
            }
            return (c === !1 || "function" == typeof c) && (d = c, c = b), d === !1 && (d = h), this.each(function() {
                fb.event.remove(this, a, d, c)
            })
        },
        trigger: function(a, b) {
            return this.each(function() {
                fb.event.trigger(a, b, this)
            })
        },
        triggerHandler: function(a, c) {
            var d = this[0];
            return d ? fb.event.trigger(a, c, d, !0) : b
        }
    });
    var Cb = /^.[^:#\[\.,]*$/,
        Db = fb.expr.match.needsContext,
        Eb = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    fb.fn.extend({
        find: function(a) {
            var b, c, d, e = this.length;
            if ("string" != typeof a) return b = this, this.pushStack(fb(a).filter(function() {
                for (d = 0; e > d; d++)
                    if (fb.contains(b[d], this)) return !0
            }));
            for (c = [], d = 0; e > d; d++) fb.find(a, this[d], c);
            return c = this.pushStack(e > 1 ? fb.unique(c) : c), c.selector = (this.selector ? this.selector + " " : "") + a, c
        },
        has: function(a) {
            var b = fb(a, this),
                c = b.length;
            return this.filter(function() {
                for (var a = 0; c > a; a++)
                    if (fb.contains(this, b[a])) return !0
            })
        },
        not: function(a) {
            return this.pushStack(k(this, a || [], !0))
        },
        filter: function(a) {
            return this.pushStack(k(this, a || [], !1))
        },
        is: function(a) {
            return !!a && ("string" == typeof a ? Db.test(a) ? fb(a, this.context).index(this[0]) >= 0 : fb.filter(a, this).length > 0 : this.filter(a).length > 0)
        },
        closest: function(a, b) {
            for (var c, d = 0, e = this.length, f = [], g = Db.test(a) || "string" != typeof a ? fb(a, b || this.context) : 0; e > d; d++)
                for (c = this[d]; c && c !== b; c = c.parentNode)
                    if (11 > c.nodeType && (g ? g.index(c) > -1 : 1 === c.nodeType && fb.find.matchesSelector(c, a))) {
                        c = f.push(c);
                        break
                    }
            return this.pushStack(f.length > 1 ? fb.unique(f) : f)
        },
        index: function(a) {
            return a ? "string" == typeof a ? bb.call(fb(a), this[0]) : bb.call(this, a.jquery ? a[0] : a) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(a, b) {
            var c = "string" == typeof a ? fb(a, b) : fb.makeArray(a && a.nodeType ? [a] : a),
                d = fb.merge(this.get(), c);
            return this.pushStack(fb.unique(d))
        },
        addBack: function(a) {
            return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
        }
    }), fb.each({
        parent: function(a) {
            var b = a.parentNode;
            return b && 11 !== b.nodeType ? b : null
        },
        parents: function(a) {
            return fb.dir(a, "parentNode")
        },
        parentsUntil: function(a, b, c) {
            return fb.dir(a, "parentNode", c)
        },
        next: function(a) {
            return j(a, "nextSibling")
        },
        prev: function(a) {
            return j(a, "previousSibling")
        },
        nextAll: function(a) {
            return fb.dir(a, "nextSibling")
        },
        prevAll: function(a) {
            return fb.dir(a, "previousSibling")
        },
        nextUntil: function(a, b, c) {
            return fb.dir(a, "nextSibling", c)
        },
        prevUntil: function(a, b, c) {
            return fb.dir(a, "previousSibling", c)
        },
        siblings: function(a) {
            return fb.sibling((a.parentNode || {}).firstChild, a)
        },
        children: function(a) {
            return fb.sibling(a.firstChild)
        },
        contents: function(a) {
            return fb.nodeName(a, "iframe") ? a.contentDocument || a.contentWindow.document : fb.merge([], a.childNodes)
        }
    }, function(a, b) {
        fb.fn[a] = function(c, d) {
            var e = fb.map(this, b, c);
            return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = fb.filter(d, e)), this.length > 1 && (Eb[a] || fb.unique(e), "p" === a[0] && e.reverse()), this.pushStack(e)
        }
    }), fb.extend({
        filter: function(a, b, c) {
            var d = b[0];
            return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? fb.find.matchesSelector(d, a) ? [d] : [] : fb.find.matches(a, fb.grep(b, function(a) {
                return 1 === a.nodeType
            }))
        },
        dir: function(a, c, d) {
            for (var e = [], f = d !== b;
                (a = a[c]) && 9 !== a.nodeType;)
                if (1 === a.nodeType) {
                    if (f && fb(a).is(d)) break;
                    e.push(a)
                }
            return e
        },
        sibling: function(a, b) {
            for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a);
            return c
        }
    });
    var Fb = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        Gb = /<([\w:]+)/,
        Hb = /<|&#?\w+;/,
        Ib = /<(?:script|style|link)/i,
        Jb = /^(?:checkbox|radio)$/i,
        Kb = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Lb = /^$|\/(?:java|ecma)script/i,
        Mb = /^true\/(.*)/,
        Nb = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        Ob = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };
    Ob.optgroup = Ob.option, Ob.tbody = Ob.tfoot = Ob.colgroup = Ob.caption = Ob.col = Ob.thead, Ob.th = Ob.td, fb.fn.extend({
        text: function(a) {
            return fb.access(this, function(a) {
                return a === b ? fb.text(this) : this.empty().append((this[0] && this[0].ownerDocument || T).createTextNode(a))
            }, null, a, arguments.length)
        },
        append: function() {
            return this.domManip(arguments, function(a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = l(this, a);
                    b.appendChild(a)
                }
            })
        },
        prepend: function() {
            return this.domManip(arguments, function(a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = l(this, a);
                    b.insertBefore(a, b.firstChild)
                }
            })
        },
        before: function() {
            return this.domManip(arguments, function(a) {
                this.parentNode && this.parentNode.insertBefore(a, this)
            })
        },
        after: function() {
            return this.domManip(arguments, function(a) {
                this.parentNode && this.parentNode.insertBefore(a, this.nextSibling)
            })
        },
        remove: function(a, b) {
            for (var c, d = a ? fb.filter(a, this) : this, e = 0; null != (c = d[e]); e++) b || 1 !== c.nodeType || fb.cleanData(q(c)), c.parentNode && (b && fb.contains(c.ownerDocument, c) && o(q(c, "script")), c.parentNode.removeChild(c));
            return this
        },
        empty: function() {
            for (var a, b = 0; null != (a = this[b]); b++) 1 === a.nodeType && (fb.cleanData(q(a, !1)), a.textContent = "");
            return this
        },
        clone: function(a, b) {
            return a = null == a ? !1 : a, b = null == b ? a : b, this.map(function() {
                return fb.clone(this, a, b)
            })
        },
        html: function(a) {
            return fb.access(this, function(a) {
                var c = this[0] || {},
                    d = 0,
                    e = this.length;
                if (a === b && 1 === c.nodeType) return c.innerHTML;
                if ("string" == typeof a && !Ib.test(a) && !Ob[(Gb.exec(a) || ["", ""])[1].toLowerCase()]) {
                    a = a.replace(Fb, "<$1></$2>");
                    try {
                        for (; e > d; d++) c = this[d] || {}, 1 === c.nodeType && (fb.cleanData(q(c, !1)), c.innerHTML = a);
                        c = 0
                    } catch (f) {}
                }
                c && this.empty().append(a)
            }, null, a, arguments.length)
        },
        replaceWith: function() {
            var a = fb.map(this, function(a) {
                    return [a.nextSibling, a.parentNode]
                }),
                b = 0;
            return this.domManip(arguments, function(c) {
                var d = a[b++],
                    e = a[b++];
                e && (fb(this).remove(), e.insertBefore(c, d))
            }, !0), b ? this : this.remove()
        },
        detach: function(a) {
            return this.remove(a, !0)
        },
        domManip: function(a, b, c) {
            a = $.apply([], a);
            var d, e, f, g, h, i, j = 0,
                k = this.length,
                l = this,
                o = k - 1,
                p = a[0],
                r = fb.isFunction(p);
            if (r || !(1 >= k || "string" != typeof p || fb.support.checkClone) && Kb.test(p)) return this.each(function(d) {
                var e = l.eq(d);
                r && (a[0] = p.call(this, d, e.html())), e.domManip(a, b, c)
            });
            if (k && (d = fb.buildFragment(a, this[0].ownerDocument, !1, !c && this), e = d.firstChild, 1 === d.childNodes.length && (d = e), e)) {
                for (f = fb.map(q(d, "script"), m), g = f.length; k > j; j++) h = d, j !== o && (h = fb.clone(h, !0, !0), g && fb.merge(f, q(h, "script"))), b.call(this[j], h, j);
                if (g)
                    for (i = f[f.length - 1].ownerDocument, fb.map(f, n), j = 0; g > j; j++) h = f[j], Lb.test(h.type || "") && !qb.access(h, "globalEval") && fb.contains(i, h) && (h.src ? fb._evalUrl(h.src) : fb.globalEval(h.textContent.replace(Nb, "")))
            }
            return this
        }
    }), fb.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(a, b) {
        fb.fn[a] = function(a) {
            for (var c, d = [], e = fb(a), f = e.length - 1, g = 0; f >= g; g++) c = g === f ? this : this.clone(!0), fb(e[g])[b](c), _.apply(d, c.get());
            return this.pushStack(d)
        }
    }), fb.extend({
        clone: function(a, b, c) {
            var d, e, f, g, h = a.cloneNode(!0),
                i = fb.contains(a.ownerDocument, a);
            if (!(fb.support.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || fb.isXMLDoc(a)))
                for (g = q(h), f = q(a), d = 0, e = f.length; e > d; d++) r(f[d], g[d]);
            if (b)
                if (c)
                    for (f = f || q(a), g = g || q(h), d = 0, e = f.length; e > d; d++) p(f[d], g[d]);
                else p(a, h);
            return g = q(h, "script"), g.length > 0 && o(g, !i && q(a, "script")), h
        },
        buildFragment: function(a, b, c, d) {
            for (var e, f, g, h, i, j, k = 0, l = a.length, m = b.createDocumentFragment(), n = []; l > k; k++)
                if (e = a[k], e || 0 === e)
                    if ("object" === fb.type(e)) fb.merge(n, e.nodeType ? [e] : e);
                    else if (Hb.test(e)) {
                for (f = f || m.appendChild(b.createElement("div")), g = (Gb.exec(e) || ["", ""])[1].toLowerCase(), h = Ob[g] || Ob._default, f.innerHTML = h[1] + e.replace(Fb, "<$1></$2>") + h[2], j = h[0]; j--;) f = f.firstChild;
                fb.merge(n, f.childNodes), f = m.firstChild, f.textContent = ""
            } else n.push(b.createTextNode(e));
            for (m.textContent = "", k = 0; e = n[k++];)
                if ((!d || -1 === fb.inArray(e, d)) && (i = fb.contains(e.ownerDocument, e), f = q(m.appendChild(e), "script"), i && o(f), c))
                    for (j = 0; e = f[j++];) Lb.test(e.type || "") && c.push(e);
            return m
        },
        cleanData: function(a) {
            for (var b, c, d, e = a.length, f = 0, g = fb.event.special; e > f; f++) {
                if (c = a[f], fb.acceptData(c) && (b = qb.access(c)))
                    for (d in b.events) g[d] ? fb.event.remove(c, d) : fb.removeEvent(c, d, b.handle);
                pb.discard(c), qb.discard(c)
            }
        },
        _evalUrl: function(a) {
            return fb.ajax({
                url: a,
                type: "GET",
                dataType: "text",
                async: !1,
                global: !1,
                success: fb.globalEval
            })
        }
    }), fb.fn.extend({
        wrapAll: function(a) {
            var b;
            return fb.isFunction(a) ? this.each(function(b) {
                fb(this).wrapAll(a.call(this, b))
            }) : (this[0] && (b = fb(a, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && b.insertBefore(this[0]), b.map(function() {
                for (var a = this; a.firstElementChild;) a = a.firstElementChild;
                return a
            }).append(this)), this)
        },
        wrapInner: function(a) {
            return this.each(fb.isFunction(a) ? function(b) {
                fb(this).wrapInner(a.call(this, b))
            } : function() {
                var b = fb(this),
                    c = b.contents();
                c.length ? c.wrapAll(a) : b.append(a)
            })
        },
        wrap: function(a) {
            var b = fb.isFunction(a);
            return this.each(function(c) {
                fb(this).wrapAll(b ? a.call(this, c) : a)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                fb.nodeName(this, "body") || fb(this).replaceWith(this.childNodes)
            }).end()
        }
    });
    var Pb, Qb, Rb = /^(none|table(?!-c[ea]).+)/,
        Sb = /^margin/,
        Tb = RegExp("^(" + gb + ")(.*)$", "i"),
        Ub = RegExp("^(" + gb + ")(?!px)[a-z%]+$", "i"),
        Vb = RegExp("^([+-])=(" + gb + ")", "i"),
        Wb = {
            BODY: "block"
        },
        Xb = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        Yb = {
            letterSpacing: 0,
            fontWeight: 400
        },
        Zb = ["Top", "Right", "Bottom", "Left"],
        $b = ["Webkit", "O", "Moz", "ms"];
    fb.fn.extend({
        css: function(a, c) {
            return fb.access(this, function(a, c, d) {
                var e, f, g = {},
                    h = 0;
                if (fb.isArray(c)) {
                    for (e = u(a), f = c.length; f > h; h++) g[c[h]] = fb.css(a, c[h], !1, e);
                    return g
                }
                return d !== b ? fb.style(a, c, d) : fb.css(a, c)
            }, a, c, arguments.length > 1)
        },
        show: function() {
            return v(this, !0)
        },
        hide: function() {
            return v(this)
        },
        toggle: function(a) {
            var b = "boolean" == typeof a;
            return this.each(function() {
                (b ? a : t(this)) ? fb(this).show(): fb(this).hide()
            })
        }
    }), fb.extend({
        cssHooks: {
            opacity: {
                get: function(a, b) {
                    if (b) {
                        var c = Pb(a, "opacity");
                        return "" === c ? "1" : c
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": "cssFloat"
        },
        style: function(a, c, d, e) {
            if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
                var f, g, h, i = fb.camelCase(c),
                    j = a.style;
                return c = fb.cssProps[i] || (fb.cssProps[i] = s(j, i)), h = fb.cssHooks[c] || fb.cssHooks[i], d === b ? h && "get" in h && (f = h.get(a, !1, e)) !== b ? f : j[c] : (g = typeof d, "string" === g && (f = Vb.exec(d)) && (d = (f[1] + 1) * f[2] + parseFloat(fb.css(a, c)), g = "number"), null == d || "number" === g && isNaN(d) || ("number" !== g || fb.cssNumber[i] || (d += "px"), fb.support.clearCloneStyle || "" !== d || 0 !== c.indexOf("background") || (j[c] = "inherit"), h && "set" in h && (d = h.set(a, d, e)) === b || (j[c] = d)), b)
            }
        },
        css: function(a, c, d, e) {
            var f, g, h, i = fb.camelCase(c);
            return c = fb.cssProps[i] || (fb.cssProps[i] = s(a.style, i)), h = fb.cssHooks[c] || fb.cssHooks[i], h && "get" in h && (f = h.get(a, !0, d)), f === b && (f = Pb(a, c, e)), "normal" === f && c in Yb && (f = Yb[c]), "" === d || d ? (g = parseFloat(f), d === !0 || fb.isNumeric(g) ? g || 0 : f) : f
        }
    }), Pb = function(a, c, d) {
        var e, f, g, h = d || u(a),
            i = h ? h.getPropertyValue(c) || h[c] : b,
            j = a.style;
        return h && ("" !== i || fb.contains(a.ownerDocument, a) || (i = fb.style(a, c)), Ub.test(i) && Sb.test(c) && (e = j.width, f = j.minWidth, g = j.maxWidth, j.minWidth = j.maxWidth = j.width = i, i = h.width, j.width = e, j.minWidth = f, j.maxWidth = g)), i
    }, fb.each(["height", "width"], function(a, c) {
        fb.cssHooks[c] = {
            get: function(a, d, e) {
                return d ? 0 === a.offsetWidth && Rb.test(fb.css(a, "display")) ? fb.swap(a, Xb, function() {
                    return y(a, c, e)
                }) : y(a, c, e) : b
            },
            set: function(a, b, d) {
                var e = d && u(a);
                return w(a, b, d ? x(a, c, d, fb.support.boxSizing && "border-box" === fb.css(a, "boxSizing", !1, e), e) : 0)
            }
        }
    }), fb(function() {
        fb.support.reliableMarginRight || (fb.cssHooks.marginRight = {
            get: function(a, c) {
                return c ? fb.swap(a, {
                    display: "inline-block"
                }, Pb, [a, "marginRight"]) : b
            }
        }), !fb.support.pixelPosition && fb.fn.position && fb.each(["top", "left"], function(a, c) {
            fb.cssHooks[c] = {
                get: function(a, d) {
                    return d ? (d = Pb(a, c), Ub.test(d) ? fb(a).position()[c] + "px" : d) : b
                }
            }
        })
    }), fb.expr && fb.expr.filters && (fb.expr.filters.hidden = function(a) {
        return 0 >= a.offsetWidth && 0 >= a.offsetHeight
    }, fb.expr.filters.visible = function(a) {
        return !fb.expr.filters.hidden(a)
    }), fb.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(a, b) {
        fb.cssHooks[a + b] = {
            expand: function(c) {
                for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; 4 > d; d++) e[a + Zb[d] + b] = f[d] || f[d - 2] || f[0];
                return e
            }
        }, Sb.test(a) || (fb.cssHooks[a + b].set = w)
    });
    var _b = /%20/g,
        ac = /\[\]$/,
        bc = /\r?\n/g,
        cc = /^(?:submit|button|image|reset|file)$/i,
        dc = /^(?:input|select|textarea|keygen)/i;
    fb.fn.extend({
        serialize: function() {
            return fb.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var a = fb.prop(this, "elements");
                return a ? fb.makeArray(a) : this
            }).filter(function() {
                var a = this.type;
                return this.name && !fb(this).is(":disabled") && dc.test(this.nodeName) && !cc.test(a) && (this.checked || !Jb.test(a))
            }).map(function(a, b) {
                var c = fb(this).val();
                return null == c ? null : fb.isArray(c) ? fb.map(c, function(a) {
                    return {
                        name: b.name,
                        value: a.replace(bc, "\r\n")
                    }
                }) : {
                    name: b.name,
                    value: c.replace(bc, "\r\n")
                }
            }).get()
        }
    }), fb.param = function(a, c) {
        var d, e = [],
            f = function(a, b) {
                b = fb.isFunction(b) ? b() : null == b ? "" : b, e[e.length] = encodeURIComponent(a) + "=" + encodeURIComponent(b)
            };
        if (c === b && (c = fb.ajaxSettings && fb.ajaxSettings.traditional), fb.isArray(a) || a.jquery && !fb.isPlainObject(a)) fb.each(a, function() {
            f(this.name, this.value)
        });
        else
            for (d in a) B(d, a[d], c, f);
        return e.join("&").replace(_b, "+")
    }, fb.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(a, b) {
        fb.fn[b] = function(a, c) {
            return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b)
        }
    }), fb.fn.extend({
        hover: function(a, b) {
            return this.mouseenter(a).mouseleave(b || a)
        },
        bind: function(a, b, c) {
            return this.on(a, null, b, c)
        },
        unbind: function(a, b) {
            return this.off(a, null, b)
        },
        delegate: function(a, b, c, d) {
            return this.on(b, a, c, d)
        },
        undelegate: function(a, b, c) {
            return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c)
        }
    });
    var ec, fc, gc = fb.now(),
        hc = /\?/,
        ic = /#.*$/,
        jc = /([?&])_=[^&]*/,
        kc = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        lc = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        mc = /^(?:GET|HEAD)$/,
        nc = /^\/\//,
        oc = /^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
        pc = fb.fn.load,
        qc = {},
        rc = {},
        sc = "*/".concat("*");
    try {
        fc = S.href
    } catch (tc) {
        fc = T.createElement("a"), fc.href = "", fc = fc.href
    }
    ec = oc.exec(fc.toLowerCase()) || [], fb.fn.load = function(a, c, d) {
        if ("string" != typeof a && pc) return pc.apply(this, arguments);
        var e, f, g, h = this,
            i = a.indexOf(" ");
        return i >= 0 && (e = a.slice(i), a = a.slice(0, i)), fb.isFunction(c) ? (d = c, c = b) : c && "object" == typeof c && (f = "POST"), h.length > 0 && fb.ajax({
            url: a,
            type: f,
            dataType: "html",
            data: c
        }).done(function(a) {
            g = arguments, h.html(e ? fb("<div>").append(fb.parseHTML(a)).find(e) : a)
        }).complete(d && function(a, b) {
            h.each(d, g || [a.responseText, b, a])
        }), this
    }, fb.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(a, b) {
        fb.fn[b] = function(a) {
            return this.on(b, a)
        }
    }), fb.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: fc,
            type: "GET",
            isLocal: lc.test(ec[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": sc,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": fb.parseJSON,
                "text xml": fb.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(a, b) {
            return b ? E(E(a, fb.ajaxSettings), b) : E(fb.ajaxSettings, a)
        },
        ajaxPrefilter: C(qc),
        ajaxTransport: C(rc),
        ajax: function(a, c) {
            function d(a, c, d, h) {
                var j, l, s, t, v, x = c;
                2 !== u && (u = 2, i && clearTimeout(i), e = b, g = h || "", w.readyState = a > 0 ? 4 : 0, j = a >= 200 && 300 > a || 304 === a, d && (t = F(m, w, d)), t = G(m, t, w, j), j ? (m.ifModified && (v = w.getResponseHeader("Last-Modified"), v && (fb.lastModified[f] = v), v = w.getResponseHeader("etag"), v && (fb.etag[f] = v)), 204 === a ? x = "nocontent" : 304 === a ? x = "notmodified" : (x = t.state, l = t.data, s = t.error, j = !s)) : (s = x, (a || !x) && (x = "error", 0 > a && (a = 0))), w.status = a, w.statusText = (c || x) + "", j ? p.resolveWith(n, [l, x, w]) : p.rejectWith(n, [w, x, s]), w.statusCode(r), r = b, k && o.trigger(j ? "ajaxSuccess" : "ajaxError", [w, m, j ? l : s]), q.fireWith(n, [w, x]), k && (o.trigger("ajaxComplete", [w, m]), --fb.active || fb.event.trigger("ajaxStop")))
            }
            "object" == typeof a && (c = a, a = b), c = c || {};
            var e, f, g, h, i, j, k, l, m = fb.ajaxSetup({}, c),
                n = m.context || m,
                o = m.context && (n.nodeType || n.jquery) ? fb(n) : fb.event,
                p = fb.Deferred(),
                q = fb.Callbacks("once memory"),
                r = m.statusCode || {},
                s = {},
                t = {},
                u = 0,
                v = "canceled",
                w = {
                    readyState: 0,
                    getResponseHeader: function(a) {
                        var b;
                        if (2 === u) {
                            if (!h)
                                for (h = {}; b = kc.exec(g);) h[b[1].toLowerCase()] = b[2];
                            b = h[a.toLowerCase()]
                        }
                        return null == b ? null : b
                    },
                    getAllResponseHeaders: function() {
                        return 2 === u ? g : null
                    },
                    setRequestHeader: function(a, b) {
                        var c = a.toLowerCase();
                        return u || (a = t[c] = t[c] || a, s[a] = b), this
                    },
                    overrideMimeType: function(a) {
                        return u || (m.mimeType = a), this
                    },
                    statusCode: function(a) {
                        var b;
                        if (a)
                            if (2 > u)
                                for (b in a) r[b] = [r[b], a[b]];
                            else w.always(a[w.status]);
                        return this
                    },
                    abort: function(a) {
                        var b = a || v;
                        return e && e.abort(b), d(0, b), this
                    }
                };
            if (p.promise(w).complete = q.add, w.success = w.done, w.error = w.fail, m.url = ((a || m.url || fc) + "").replace(ic, "").replace(nc, ec[1] + "//"), m.type = c.method || c.type || m.method || m.type, m.dataTypes = fb.trim(m.dataType || "*").toLowerCase().match(hb) || [""], null == m.crossDomain && (j = oc.exec(m.url.toLowerCase()), m.crossDomain = !(!j || j[1] === ec[1] && j[2] === ec[2] && (j[3] || ("http:" === j[1] ? "80" : "443")) === (ec[3] || ("http:" === ec[1] ? "80" : "443")))), m.data && m.processData && "string" != typeof m.data && (m.data = fb.param(m.data, m.traditional)), D(qc, m, c, w), 2 === u) return w;
            k = m.global, k && 0 === fb.active++ && fb.event.trigger("ajaxStart"), m.type = m.type.toUpperCase(), m.hasContent = !mc.test(m.type), f = m.url, m.hasContent || (m.data && (f = m.url += (hc.test(f) ? "&" : "?") + m.data, delete m.data), m.cache === !1 && (m.url = jc.test(f) ? f.replace(jc, "$1_=" + gc++) : f + (hc.test(f) ? "&" : "?") + "_=" + gc++)), m.ifModified && (fb.lastModified[f] && w.setRequestHeader("If-Modified-Since", fb.lastModified[f]), fb.etag[f] && w.setRequestHeader("If-None-Match", fb.etag[f])), (m.data && m.hasContent && m.contentType !== !1 || c.contentType) && w.setRequestHeader("Content-Type", m.contentType), w.setRequestHeader("Accept", m.dataTypes[0] && m.accepts[m.dataTypes[0]] ? m.accepts[m.dataTypes[0]] + ("*" !== m.dataTypes[0] ? ", " + sc + "; q=0.01" : "") : m.accepts["*"]);
            for (l in m.headers) w.setRequestHeader(l, m.headers[l]);
            if (m.beforeSend && (m.beforeSend.call(n, w, m) === !1 || 2 === u)) return w.abort();
            v = "abort";
            for (l in {
                    success: 1,
                    error: 1,
                    complete: 1
                }) w[l](m[l]);
            if (e = D(rc, m, c, w)) {
                w.readyState = 1, k && o.trigger("ajaxSend", [w, m]), m.async && m.timeout > 0 && (i = setTimeout(function() {
                    w.abort("timeout")
                }, m.timeout));
                try {
                    u = 1, e.send(s, d)
                } catch (x) {
                    if (!(2 > u)) throw x;
                    d(-1, x)
                }
            } else d(-1, "No Transport");
            return w
        },
        getJSON: function(a, b, c) {
            return fb.get(a, b, c, "json")
        },
        getScript: function(a, c) {
            return fb.get(a, b, c, "script")
        }
    }), fb.each(["get", "post"], function(a, c) {
        fb[c] = function(a, d, e, f) {
            return fb.isFunction(d) && (f = f || e, e = d, d = b), fb.ajax({
                url: a,
                type: c,
                dataType: f,
                data: d,
                success: e
            })
        }
    }), fb.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(a) {
                return fb.globalEval(a), a
            }
        }
    }), fb.ajaxPrefilter("script", function(a) {
        a.cache === b && (a.cache = !1), a.crossDomain && (a.type = "GET")
    }), fb.ajaxTransport("script", function(a) {
        if (a.crossDomain) {
            var b, c;
            return {
                send: function(d, e) {
                    b = fb("<script>").prop({
                        async: !0,
                        charset: a.scriptCharset,
                        src: a.url
                    }).on("load error", c = function(a) {
                        b.remove(), c = null, a && e("error" === a.type ? 404 : 200, a.type)
                    }), T.head.appendChild(b[0])
                },
                abort: function() {
                    c && c()
                }
            }
        }
    });
    var uc = [],
        vc = /(=)\?(?=&|$)|\?\?/;
    fb.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var a = uc.pop() || fb.expando + "_" + gc++;
            return this[a] = !0, a
        }
    }), fb.ajaxPrefilter("json jsonp", function(c, d, e) {
        var f, g, h, i = c.jsonp !== !1 && (vc.test(c.url) ? "url" : "string" == typeof c.data && !(c.contentType || "").indexOf("application/x-www-form-urlencoded") && vc.test(c.data) && "data");
        return i || "jsonp" === c.dataTypes[0] ? (f = c.jsonpCallback = fb.isFunction(c.jsonpCallback) ? c.jsonpCallback() : c.jsonpCallback, i ? c[i] = c[i].replace(vc, "$1" + f) : c.jsonp !== !1 && (c.url += (hc.test(c.url) ? "&" : "?") + c.jsonp + "=" + f), c.converters["script json"] = function() {
            return h || fb.error(f + " was not called"), h[0]
        }, c.dataTypes[0] = "json", g = a[f], a[f] = function() {
            h = arguments
        }, e.always(function() {
            a[f] = g, c[f] && (c.jsonpCallback = d.jsonpCallback, uc.push(f)), h && fb.isFunction(g) && g(h[0]), h = g = b
        }), "script") : b
    }), fb.ajaxSettings.xhr = function() {
        try {
            return new XMLHttpRequest
        } catch (a) {}
    };
    var wc = fb.ajaxSettings.xhr(),
        xc = {
            0: 200,
            1223: 204
        },
        yc = 0,
        zc = {};
    a.ActiveXObject && fb(a).on("unload", function() {
        for (var a in zc) zc[a]();
        zc = b
    }), fb.support.cors = !!wc && "withCredentials" in wc, fb.support.ajax = wc = !!wc, fb.ajaxTransport(function(a) {
        var c;
        return fb.support.cors || wc && !a.crossDomain ? {
            send: function(d, e) {
                var f, g, h = a.xhr();
                if (h.open(a.type, a.url, a.async, a.username, a.password), a.xhrFields)
                    for (f in a.xhrFields) h[f] = a.xhrFields[f];
                a.mimeType && h.overrideMimeType && h.overrideMimeType(a.mimeType), a.crossDomain || d["X-Requested-With"] || (d["X-Requested-With"] = "XMLHttpRequest");
                for (f in d) h.setRequestHeader(f, d[f]);
                c = function(a) {
                    return function() {
                        c && (delete zc[g], c = h.onload = h.onerror = null, "abort" === a ? h.abort() : "error" === a ? e(h.status || 404, h.statusText) : e(xc[h.status] || h.status, h.statusText, "string" == typeof h.responseText ? {
                            text: h.responseText
                        } : b, h.getAllResponseHeaders()))
                    }
                }, h.onload = c(), h.onerror = c("error"), c = zc[g = yc++] = c("abort"), h.send(a.hasContent && a.data || null)
            },
            abort: function() {
                c && c()
            }
        } : b
    });
    var Ac, Bc, Cc = /^(?:toggle|show|hide)$/,
        Dc = RegExp("^(?:([+-])=|)(" + gb + ")([a-z%]*)$", "i"),
        Ec = /queueHooks$/,
        Fc = [L],
        Gc = {
            "*": [function(a, b) {
                var c, d, e = this.createTween(a, b),
                    f = Dc.exec(b),
                    g = e.cur(),
                    h = +g || 0,
                    i = 1,
                    j = 20;
                if (f) {
                    if (c = +f[2], d = f[3] || (fb.cssNumber[a] ? "" : "px"), "px" !== d && h) {
                        h = fb.css(e.elem, a, !0) || c || 1;
                        do i = i || ".5", h /= i, fb.style(e.elem, a, h + d); while (i !== (i = e.cur() / g) && 1 !== i && --j)
                    }
                    e.unit = d, e.start = h, e.end = f[1] ? h + (f[1] + 1) * c : c
                }
                return e
            }]
        };
    fb.Animation = fb.extend(J, {
        tweener: function(a, b) {
            fb.isFunction(a) ? (b = a, a = ["*"]) : a = a.split(" ");
            for (var c, d = 0, e = a.length; e > d; d++) c = a[d], Gc[c] = Gc[c] || [], Gc[c].unshift(b)
        },
        prefilter: function(a, b) {
            b ? Fc.unshift(a) : Fc.push(a)
        }
    }), fb.Tween = M, M.prototype = {
        constructor: M,
        init: function(a, b, c, d, e, f) {
            this.elem = a, this.prop = c, this.easing = e || "swing", this.options = b, this.start = this.now = this.cur(), this.end = d, this.unit = f || (fb.cssNumber[c] ? "" : "px")
        },
        cur: function() {
            var a = M.propHooks[this.prop];
            return a && a.get ? a.get(this) : M.propHooks._default.get(this)
        },
        run: function(a) {
            var b, c = M.propHooks[this.prop];
            return this.pos = b = this.options.duration ? fb.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : M.propHooks._default.set(this), this
        }
    }, M.prototype.init.prototype = M.prototype, M.propHooks = {
        _default: {
            get: function(a) {
                var b;
                return null == a.elem[a.prop] || a.elem.style && null != a.elem.style[a.prop] ? (b = fb.css(a.elem, a.prop, ""), b && "auto" !== b ? b : 0) : a.elem[a.prop]
            },
            set: function(a) {
                fb.fx.step[a.prop] ? fb.fx.step[a.prop](a) : a.elem.style && (null != a.elem.style[fb.cssProps[a.prop]] || fb.cssHooks[a.prop]) ? fb.style(a.elem, a.prop, a.now + a.unit) : a.elem[a.prop] = a.now
            }
        }
    }, M.propHooks.scrollTop = M.propHooks.scrollLeft = {
        set: function(a) {
            a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now)
        }
    }, fb.each(["toggle", "show", "hide"], function(a, b) {
        var c = fb.fn[b];
        fb.fn[b] = function(a, d, e) {
            return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(N(b, !0), a, d, e)
        }
    }), fb.fn.extend({
        fadeTo: function(a, b, c, d) {
            return this.filter(t).css("opacity", 0).show().end().animate({
                opacity: b
            }, a, c, d)
        },
        animate: function(a, b, c, d) {
            var e = fb.isEmptyObject(a),
                f = fb.speed(b, c, d),
                g = function() {
                    var b = J(this, fb.extend({}, a), f);
                    g.finish = function() {
                        b.stop(!0)
                    }, (e || qb.get(this, "finish")) && b.stop(!0)
                };
            return g.finish = g, e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g)
        },
        stop: function(a, c, d) {
            var e = function(a) {
                var b = a.stop;
                delete a.stop, b(d)
            };
            return "string" != typeof a && (d = c, c = a, a = b), c && a !== !1 && this.queue(a || "fx", []), this.each(function() {
                var b = !0,
                    c = null != a && a + "queueHooks",
                    f = fb.timers,
                    g = qb.get(this);
                if (c) g[c] && g[c].stop && e(g[c]);
                else
                    for (c in g) g[c] && g[c].stop && Ec.test(c) && e(g[c]);
                for (c = f.length; c--;) f[c].elem !== this || null != a && f[c].queue !== a || (f[c].anim.stop(d), b = !1, f.splice(c, 1));
                (b || !d) && fb.dequeue(this, a)
            })
        },
        finish: function(a) {
            return a !== !1 && (a = a || "fx"), this.each(function() {
                var b, c = qb.get(this),
                    d = c[a + "queue"],
                    e = c[a + "queueHooks"],
                    f = fb.timers,
                    g = d ? d.length : 0;
                for (c.finish = !0, fb.queue(this, a, []), e && e.cur && e.cur.finish && e.cur.finish.call(this), b = f.length; b--;) f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1));
                for (b = 0; g > b; b++) d[b] && d[b].finish && d[b].finish.call(this);
                delete c.finish
            })
        }
    }), fb.each({
        slideDown: N("show"),
        slideUp: N("hide"),
        slideToggle: N("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(a, b) {
        fb.fn[a] = function(a, c, d) {
            return this.animate(b, a, c, d)
        }
    }), fb.speed = function(a, b, c) {
        var d = a && "object" == typeof a ? fb.extend({}, a) : {
            complete: c || !c && b || fb.isFunction(a) && a,
            duration: a,
            easing: c && b || b && !fb.isFunction(b) && b
        };
        return d.duration = fb.fx.off ? 0 : "number" == typeof d.duration ? d.duration : d.duration in fb.fx.speeds ? fb.fx.speeds[d.duration] : fb.fx.speeds._default, (null == d.queue || d.queue === !0) && (d.queue = "fx"), d.old = d.complete, d.complete = function() {
            fb.isFunction(d.old) && d.old.call(this), d.queue && fb.dequeue(this, d.queue)
        }, d
    }, fb.easing = {
        linear: function(a) {
            return a
        },
        swing: function(a) {
            return .5 - Math.cos(a * Math.PI) / 2
        }
    }, fb.timers = [], fb.fx = M.prototype.init, fb.fx.tick = function() {
        var a, c = fb.timers,
            d = 0;
        for (Ac = fb.now(); c.length > d; d++) a = c[d], a() || c[d] !== a || c.splice(d--, 1);
        c.length || fb.fx.stop(), Ac = b
    }, fb.fx.timer = function(a) {
        a() && fb.timers.push(a) && fb.fx.start()
    }, fb.fx.interval = 13, fb.fx.start = function() {
        Bc || (Bc = setInterval(fb.fx.tick, fb.fx.interval))
    }, fb.fx.stop = function() {
        clearInterval(Bc), Bc = null
    }, fb.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    }, fb.fx.step = {}, fb.expr && fb.expr.filters && (fb.expr.filters.animated = function(a) {
        return fb.grep(fb.timers, function(b) {
            return a === b.elem
        }).length
    }), fb.fn.offset = function(a) {
        if (arguments.length) return a === b ? this : this.each(function(b) {
            fb.offset.setOffset(this, a, b)
        });
        var c, d, e = this[0],
            f = {
                top: 0,
                left: 0
            },
            g = e && e.ownerDocument;
        return g ? (c = g.documentElement, fb.contains(c, e) ? (typeof e.getBoundingClientRect !== R && (f = e.getBoundingClientRect()), d = O(g), {
            top: f.top + d.pageYOffset - c.clientTop,
            left: f.left + d.pageXOffset - c.clientLeft
        }) : f) : void 0
    }, fb.offset = {
        setOffset: function(a, b, c) {
            var d, e, f, g, h, i, j, k = fb.css(a, "position"),
                l = fb(a),
                m = {};
            "static" === k && (a.style.position = "relative"), h = l.offset(), f = fb.css(a, "top"), i = fb.css(a, "left"), j = ("absolute" === k || "fixed" === k) && (f + i).indexOf("auto") > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), fb.isFunction(b) && (b = b.call(a, c, h)), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), "using" in b ? b.using.call(a, m) : l.css(m)
        }
    }, fb.fn.extend({
        position: function() {
            if (this[0]) {
                var a, b, c = this[0],
                    d = {
                        top: 0,
                        left: 0
                    };
                return "fixed" === fb.css(c, "position") ? b = c.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), fb.nodeName(a[0], "html") || (d = a.offset()), d.top += fb.css(a[0], "borderTopWidth", !0), d.left += fb.css(a[0], "borderLeftWidth", !0)), {
                    top: b.top - d.top - fb.css(c, "marginTop", !0),
                    left: b.left - d.left - fb.css(c, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var a = this.offsetParent || U; a && !fb.nodeName(a, "html") && "static" === fb.css(a, "position");) a = a.offsetParent;
                return a || U
            })
        }
    }), fb.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(c, d) {
        var e = "pageYOffset" === d;
        fb.fn[c] = function(f) {
            return fb.access(this, function(c, f, g) {
                var h = O(c);
                return g === b ? h ? h[d] : c[f] : (h ? h.scrollTo(e ? a.pageXOffset : g, e ? g : a.pageYOffset) : c[f] = g, b)
            }, c, f, arguments.length, null)
        }
    }), fb.each({
        Height: "height",
        Width: "width"
    }, function(a, c) {
        fb.each({
            padding: "inner" + a,
            content: c,
            "": "outer" + a
        }, function(d, e) {
            fb.fn[e] = function(e, f) {
                var g = arguments.length && (d || "boolean" != typeof e),
                    h = d || (e === !0 || f === !0 ? "margin" : "border");
                return fb.access(this, function(c, d, e) {
                    var f;
                    return fb.isWindow(c) ? c.document.documentElement["client" + a] : 9 === c.nodeType ? (f = c.documentElement, Math.max(c.body["scroll" + a], f["scroll" + a], c.body["offset" + a], f["offset" + a], f["client" + a])) : e === b ? fb.css(c, d, h) : fb.style(c, d, e, h)
                }, c, g ? e : b, g, null)
            }
        })
    }), fb.fn.size = function() {
        return this.length
    }, fb.fn.andSelf = fb.fn.addBack, "object" == typeof module && "object" == typeof module.exports ? module.exports = fb : "function" == typeof define && define.amd && define("jquery", [], function() {
        return fb
    }), "object" == typeof a && "object" == typeof a.document && (a.jQuery = a.$ = fb)
}(window),
function(a) {
    a.fn.swipeListener = function(b) {
        var c = {
            duration: 1e3,
            minX: 20,
            minY: 20,
            swipeUp: void 0,
            swipeDown: void 0,
            swipeLeft: void 0,
            swipeRight: void 0
        };
        return b && a.extend(c, b), this.each(function() {
            function b(a) {
                var b = i ? a.originalEvent.touches[0] : a;
                f = {
                    x: b.pageX,
                    y: b.pageY,
                    time: (new Date).getTime()
                }, a.stopPropagation(), h.bind(k, d).one(l, e)
            }

            function d(a) {
                if (f) {
                    var b = i ? a.originalEvent.touches[0] : a;
                    g = {
                        x: b.pageX,
                        y: b.pageY,
                        time: (new Date).getTime()
                    }
                }
            }

            function e(a) {
                a.coords = {
                    start: f,
                    stop: g
                }, h.unbind("touchmove mousemove", d), f && g && g.time - f.time < c.duration && (diffX = f.x - g.x, diffY = f.y - g.y, Math.abs(diffX) > c.minX ? diffX > 0 && c.swipeLeft ? c.swipeLeft(a) : c.swipeRight && c.swipeRight(a) : Math.abs(diffY) > c.minY && (diffY > 0 && c.swipeUp ? c.swipeUp(a) : c.swipeDown && c.swipeDown(a))), f = g = void 0
            }
            var f = void 0,
                g = void 0,
                h = a(this),
                i = "undefined" != typeof this.ontouchstart,
                j = i ? "touchstart" : "mousedown",
                k = i ? "touchmove" : "mousemove",
                l = i ? "touchend" : "mouseup";
            h.bind(j, b)
        }), this
    }
}(jQuery);
var Swiper = function(a, b) {
    "use strict";

    function c(a, b) {
        return document.querySelectorAll ? (b || document).querySelectorAll(a) : jQuery(a, b)
    }

    function d(a) {
        return "[object Array]" === Object.prototype.toString.apply(a) ? !0 : !1
    }

    function e() {
        var a = G - J;
        return b.freeMode && (a = G - J), b.slidesPerView > D.slides.length && !b.centeredSlides && (a = 0), 0 > a && (a = 0), a
    }

    function f() {
        function a(a) {
            var c, d, e = function() {
                "undefined" != typeof D && null !== D && (void 0 !== D.imagesLoaded && D.imagesLoaded++, D.imagesLoaded === D.imagesToLoad.length && (D.reInit(), b.onImagesReady && D.fireCallback(b.onImagesReady, D)))
            };
            a.complete ? e() : (d = a.currentSrc || a.getAttribute("src"), d ? (c = new Image, c.onload = e, c.onerror = e, c.src = d) : e())
        }
        var d = D.h.addEventListener,
            e = "wrapper" === b.eventTarget ? D.wrapper : D.container;
        if (D.browser.ie10 || D.browser.ie11 ? (d(e, D.touchEvents.touchStart, p), d(document, D.touchEvents.touchMove, q), d(document, D.touchEvents.touchEnd, r)) : (D.support.touch && (d(e, "touchstart", p), d(e, "touchmove", q), d(e, "touchend", r)), b.simulateTouch && (d(e, "mousedown", p), d(document, "mousemove", q), d(document, "mouseup", r))), b.autoResize && d(window, "resize", D.resizeFix), g(), D._wheelEvent = !1, b.mousewheelControl) {
            if (void 0 !== document.onmousewheel && (D._wheelEvent = "mousewheel"), !D._wheelEvent) try {
                new WheelEvent("wheel"), D._wheelEvent = "wheel"
            } catch (f) {}
            D._wheelEvent || (D._wheelEvent = "DOMMouseScroll"), D._wheelEvent && d(D.container, D._wheelEvent, j)
        }
        if (b.keyboardControl && d(document, "keydown", i), b.updateOnImagesReady) {
            D.imagesToLoad = c("img", D.container);
            for (var h = 0; h < D.imagesToLoad.length; h++) a(D.imagesToLoad[h])
        }
    }

    function g() {
        var a, d = D.h.addEventListener;
        if (b.preventLinks) {
            var e = c("a", D.container);
            for (a = 0; a < e.length; a++) d(e[a], "click", n)
        }
        if (b.releaseFormElements) {
            var f = c("input, textarea, select", D.container);
            for (a = 0; a < f.length; a++) d(f[a], D.touchEvents.touchStart, o, !0), D.support.touch && b.simulateTouch && d(f[a], "mousedown", o, !0)
        }
        if (b.onSlideClick)
            for (a = 0; a < D.slides.length; a++) d(D.slides[a], "click", k);
        if (b.onSlideTouch)
            for (a = 0; a < D.slides.length; a++) d(D.slides[a], D.touchEvents.touchStart, l)
    }

    function h() {
        var a, d = D.h.removeEventListener;
        if (b.onSlideClick)
            for (a = 0; a < D.slides.length; a++) d(D.slides[a], "click", k);
        if (b.onSlideTouch)
            for (a = 0; a < D.slides.length; a++) d(D.slides[a], D.touchEvents.touchStart, l);
        if (b.releaseFormElements) {
            var e = c("input, textarea, select", D.container);
            for (a = 0; a < e.length; a++) d(e[a], D.touchEvents.touchStart, o, !0), D.support.touch && b.simulateTouch && d(e[a], "mousedown", o, !0)
        }
        if (b.preventLinks) {
            var f = c("a", D.container);
            for (a = 0; a < f.length; a++) d(f[a], "click", n)
        }
    }

    function i(a) {
        var b = a.keyCode || a.charCode;
        if (!(a.shiftKey || a.altKey || a.ctrlKey || a.metaKey)) {
            if (37 === b || 39 === b || 38 === b || 40 === b) {
                for (var c = !1, d = D.h.getOffset(D.container), e = D.h.windowScroll().left, f = D.h.windowScroll().top, g = D.h.windowWidth(), h = D.h.windowHeight(), i = [
                        [d.left, d.top],
                        [d.left + D.width, d.top],
                        [d.left, d.top + D.height],
                        [d.left + D.width, d.top + D.height]
                    ], j = 0; j < i.length; j++) {
                    var k = i[j];
                    k[0] >= e && k[0] <= e + g && k[1] >= f && k[1] <= f + h && (c = !0)
                }
                if (!c) return
            }
            N ? ((37 === b || 39 === b) && (a.preventDefault ? a.preventDefault() : a.returnValue = !1), 39 === b && D.swipeNext(), 37 === b && D.swipePrev()) : ((38 === b || 40 === b) && (a.preventDefault ? a.preventDefault() : a.returnValue = !1), 40 === b && D.swipeNext(), 38 === b && D.swipePrev())
        }
    }

    function j(a) {
        var c = D._wheelEvent,
            d = 0;
        if (a.detail) d = -a.detail;
        else if ("mousewheel" === c)
            if (b.mousewheelControlForceToAxis)
                if (N) {
                    if (!(Math.abs(a.wheelDeltaX) > Math.abs(a.wheelDeltaY))) return;
                    d = a.wheelDeltaX
                } else {
                    if (!(Math.abs(a.wheelDeltaY) > Math.abs(a.wheelDeltaX))) return;
                    d = a.wheelDeltaY
                } else d = a.wheelDelta;
        else if ("DOMMouseScroll" === c) d = -a.detail;
        else if ("wheel" === c)
            if (b.mousewheelControlForceToAxis)
                if (N) {
                    if (!(Math.abs(a.deltaX) > Math.abs(a.deltaY))) return;
                    d = -a.deltaX
                } else {
                    if (!(Math.abs(a.deltaY) > Math.abs(a.deltaX))) return;
                    d = -a.deltaY
                } else d = Math.abs(a.deltaX) > Math.abs(a.deltaY) ? -a.deltaX : -a.deltaY;
        if (b.freeMode) {
            var f = D.getWrapperTranslate() + d;
            if (f > 0 && (f = 0), f < -e() && (f = -e()), D.setWrapperTransition(0), D.setWrapperTranslate(f), D.updateActiveSlide(f), 0 === f || f === -e()) return
        } else(new Date).getTime() - V > 60 && (0 > d ? D.swipeNext() : D.swipePrev()), V = (new Date).getTime();
        return b.autoplay && D.stopAutoplay(!0), a.preventDefault ? a.preventDefault() : a.returnValue = !1, !1
    }

    function k(a) {
        D.allowSlideClick && (m(a), D.fireCallback(b.onSlideClick, D, a))
    }

    function l(a) {
        m(a), D.fireCallback(b.onSlideTouch, D, a)
    }

    function m(a) {
        if (a.currentTarget) D.clickedSlide = a.currentTarget;
        else {
            var c = a.srcElement;
            do {
                if (c.className.indexOf(b.slideClass) > -1) break;
                c = c.parentNode
            } while (c);
            D.clickedSlide = c
        }
        D.clickedSlideIndex = D.slides.indexOf(D.clickedSlide), D.clickedSlideLoopIndex = D.clickedSlideIndex - (D.loopedSlides || 0)
    }

    function n(a) {
        return D.allowLinks ? void 0 : (a.preventDefault ? a.preventDefault() : a.returnValue = !1, b.preventLinksPropagation && "stopPropagation" in a && a.stopPropagation(), !1)
    }

    function o(a) {
        return a.stopPropagation ? a.stopPropagation() : a.returnValue = !1, !1
    }

    function p(a) {
        if (b.preventLinks && (D.allowLinks = !0), D.isTouched || b.onlyExternal) return !1;
        var c = a.target || a.srcElement;
        document.activeElement && document.activeElement !== document.body && document.activeElement !== c && document.activeElement.blur();
        var d = "input select textarea".split(" ");
        if (b.noSwiping && c && t(c)) return !1;
        if (_ = !1, D.isTouched = !0, $ = "touchstart" === a.type, !$ && "which" in a && 3 === a.which) return D.isTouched = !1, !1;
        if (!$ || 1 === a.targetTouches.length) {
            D.callPlugins("onTouchStartBegin"), !$ && !D.isAndroid && d.indexOf(c.tagName.toLowerCase()) < 0 && (a.preventDefault ? a.preventDefault() : a.returnValue = !1);
            var e = $ ? a.targetTouches[0].pageX : a.pageX || a.clientX,
                f = $ ? a.targetTouches[0].pageY : a.pageY || a.clientY;
            D.touches.startX = D.touches.currentX = e, D.touches.startY = D.touches.currentY = f, D.touches.start = D.touches.current = N ? e : f, D.setWrapperTransition(0), D.positions.start = D.positions.current = D.getWrapperTranslate(), D.setWrapperTranslate(D.positions.start), D.times.start = (new Date).getTime(), I = void 0, b.moveStartThreshold > 0 && (X = !1), b.onTouchStart && D.fireCallback(b.onTouchStart, D, a), D.callPlugins("onTouchStartEnd")
        }
    }

    function q(a) {
        if (D.isTouched && !b.onlyExternal && (!$ || "mousemove" !== a.type)) {
            var c = $ ? a.targetTouches[0].pageX : a.pageX || a.clientX,
                d = $ ? a.targetTouches[0].pageY : a.pageY || a.clientY;
            if ("undefined" == typeof I && N && (I = !!(I || Math.abs(d - D.touches.startY) > Math.abs(c - D.touches.startX))), "undefined" != typeof I || N || (I = !!(I || Math.abs(d - D.touches.startY) < Math.abs(c - D.touches.startX))), I) return void(D.isTouched = !1);
            if (N) {
                if (!b.swipeToNext && c < D.touches.startX || !b.swipeToPrev && c > D.touches.startX) return
            } else if (!b.swipeToNext && d < D.touches.startY || !b.swipeToPrev && d > D.touches.startY) return;
            if (a.assignedToSwiper) return void(D.isTouched = !1);
            if (a.assignedToSwiper = !0, b.preventLinks && (D.allowLinks = !1), b.onSlideClick && (D.allowSlideClick = !1), b.autoplay && D.stopAutoplay(!0), !$ || 1 === a.touches.length) {
                if (D.isMoved || (D.callPlugins("onTouchMoveStart"), b.loop && (D.fixLoop(), D.positions.start = D.getWrapperTranslate()), b.onTouchMoveStart && D.fireCallback(b.onTouchMoveStart, D)), D.isMoved = !0, a.preventDefault ? a.preventDefault() : a.returnValue = !1, D.touches.current = N ? c : d, D.positions.current = (D.touches.current - D.touches.start) * b.touchRatio + D.positions.start, D.positions.current > 0 && b.onResistanceBefore && D.fireCallback(b.onResistanceBefore, D, D.positions.current), D.positions.current < -e() && b.onResistanceAfter && D.fireCallback(b.onResistanceAfter, D, Math.abs(D.positions.current + e())), b.resistance && "100%" !== b.resistance) {
                    var f;
                    if (D.positions.current > 0 && (f = 1 - D.positions.current / J / 2, D.positions.current = .5 > f ? J / 2 : D.positions.current * f), D.positions.current < -e()) {
                        var g = (D.touches.current - D.touches.start) * b.touchRatio + (e() + D.positions.start);
                        f = (J + g) / J;
                        var h = D.positions.current - g * (1 - f) / 2,
                            i = -e() - J / 2;
                        D.positions.current = i > h || 0 >= f ? i : h
                    }
                }
                b.resistance && "100%" === b.resistance && (D.positions.current > 0 && (!b.freeMode || b.freeModeFluid) && (D.positions.current = 0), D.positions.current < -e() && (!b.freeMode || b.freeModeFluid) && (D.positions.current = -e()));
                var j = Math.abs(D.touches.current - D.touches.start);
                if (!b.followFinger) return;
                if (b.moveStartThreshold)
                    if (Math.abs(D.touches.current - D.touches.start) > b.moveStartThreshold || X) {
                        if (!X) return X = !0, void(D.touches.start = D.touches.current);
                        D.setWrapperTranslate(D.positions.current)
                    } else D.positions.current = D.positions.start;
                else D.setWrapperTranslate(D.positions.current);
                return (b.freeMode || b.watchActiveIndex) && D.updateActiveSlide(D.positions.current), b.grabCursor && (D.container.style.cursor = "move", D.container.style.cursor = "grabbing", D.container.style.cursor = "-moz-grabbin", D.container.style.cursor = "-webkit-grabbing"), Y || (Y = D.touches.current), Z || (Z = (new Date).getTime()), D.velocity = (D.touches.current - Y) / ((new Date).getTime() - Z) / 2, Math.abs(D.touches.current - Y) < 2 && (D.velocity = 0), Y = D.touches.current, Z = (new Date).getTime(), D.callPlugins("onTouchMoveEnd"), b.onTouchMove && D.fireCallback(b.onTouchMove, D, a, j), !1
            }
        }
    }

    function r(a) {
        if (I && D.swipeReset(), !b.onlyExternal && D.isTouched) {
            D.isTouched = !1, b.grabCursor && (D.container.style.cursor = "move", D.container.style.cursor = "grab", D.container.style.cursor = "-moz-grab", D.container.style.cursor = "-webkit-grab"), D.positions.current || 0 === D.positions.current || (D.positions.current = D.positions.start), b.followFinger && D.setWrapperTranslate(D.positions.current), D.times.end = (new Date).getTime(), D.touches.diff = D.touches.current - D.touches.start, D.touches.abs = Math.abs(D.touches.diff), D.positions.diff = D.positions.current - D.positions.start, D.positions.abs = Math.abs(D.positions.diff);
            var c = D.positions.diff,
                d = D.positions.abs,
                f = D.times.end - D.times.start;
            5 > d && 300 > f && D.allowLinks === !1 && (b.freeMode || 0 === d || D.swipeReset(), b.preventLinks && (D.allowLinks = !0), b.onSlideClick && (D.allowSlideClick = !0)), setTimeout(function() {
                "undefined" != typeof D && null !== D && (b.preventLinks && (D.allowLinks = !0), b.onSlideClick && (D.allowSlideClick = !0))
            }, 100);
            var g = e();
            if (!D.isMoved && b.freeMode) return D.isMoved = !1, b.onTouchEnd && D.fireCallback(b.onTouchEnd, D, a), void D.callPlugins("onTouchEnd");
            if (!D.isMoved || D.positions.current > 0 || D.positions.current < -g) return D.swipeReset(), b.onTouchEnd && D.fireCallback(b.onTouchEnd, D, a), void D.callPlugins("onTouchEnd");
            if (D.isMoved = !1, b.freeMode) {
                if (b.freeModeFluid) {
                    var h, i = 1e3 * b.momentumRatio,
                        j = D.velocity * i,
                        k = D.positions.current + j,
                        l = !1,
                        m = 20 * Math.abs(D.velocity) * b.momentumBounceRatio; - g > k && (b.momentumBounce && D.support.transitions ? (-m > k + g && (k = -g - m), h = -g, l = !0, _ = !0) : k = -g), k > 0 && (b.momentumBounce && D.support.transitions ? (k > m && (k = m), h = 0, l = !0, _ = !0) : k = 0), 0 !== D.velocity && (i = Math.abs((k - D.positions.current) / D.velocity)), D.setWrapperTranslate(k), D.setWrapperTransition(i), b.momentumBounce && l && D.wrapperTransitionEnd(function() {
                        _ && (b.onMomentumBounce && D.fireCallback(b.onMomentumBounce, D), D.callPlugins("onMomentumBounce"), D.setWrapperTranslate(h), D.setWrapperTransition(300))
                    }), D.updateActiveSlide(k)
                }
                return (!b.freeModeFluid || f >= 300) && D.updateActiveSlide(D.positions.current), b.onTouchEnd && D.fireCallback(b.onTouchEnd, D, a), void D.callPlugins("onTouchEnd")
            }
            H = 0 > c ? "toNext" : "toPrev", "toNext" === H && 300 >= f && (30 > d || !b.shortSwipes ? D.swipeReset() : D.swipeNext(!0, !0)), "toPrev" === H && 300 >= f && (30 > d || !b.shortSwipes ? D.swipeReset() : D.swipePrev(!0, !0));
            var n = 0;
            if ("auto" === b.slidesPerView) {
                for (var o, p = Math.abs(D.getWrapperTranslate()), q = 0, r = 0; r < D.slides.length; r++)
                    if (o = N ? D.slides[r].getWidth(!0, b.roundLengths) : D.slides[r].getHeight(!0, b.roundLengths), q += o, q > p) {
                        n = o;
                        break
                    }
                n > J && (n = J)
            } else n = F * b.slidesPerView;
            "toNext" === H && f > 300 && (d >= n * b.longSwipesRatio ? D.swipeNext(!0, !0) : D.swipeReset()), "toPrev" === H && f > 300 && (d >= n * b.longSwipesRatio ? D.swipePrev(!0, !0) : D.swipeReset()), b.onTouchEnd && D.fireCallback(b.onTouchEnd, D, a), D.callPlugins("onTouchEnd")
        }
    }

    function s(a, b) {
        return a && a.getAttribute("class") && a.getAttribute("class").indexOf(b) > -1
    }

    function t(a) {
        var c = !1;
        do s(a, b.noSwipingClass) && (c = !0), a = a.parentElement; while (!c && a.parentElement && !s(a, b.wrapperClass));
        return !c && s(a, b.wrapperClass) && s(a, b.noSwipingClass) && (c = !0), c
    }

    function u(a, b) {
        var c, d = document.createElement("div");
        return d.innerHTML = b, c = d.firstChild, c.className += " " + a, c.outerHTML
    }

    function v(a, c, d) {
        function e() {
            var f = +new Date,
                l = f - g;
            h += i * l / (1e3 / 60), k = "toNext" === j ? h > a : a > h, k ? (D.setWrapperTranslate(Math.ceil(h)), D._DOMAnimating = !0, window.setTimeout(function() {
                e()
            }, 1e3 / 60)) : (b.onSlideChangeEnd && ("to" === c ? d.runCallbacks === !0 && D.fireCallback(b.onSlideChangeEnd, D, j) : D.fireCallback(b.onSlideChangeEnd, D, j)), D.setWrapperTranslate(a), D._DOMAnimating = !1)
        }
        var f = "to" === c && d.speed >= 0 ? d.speed : b.speed,
            g = +new Date;
        if (D.support.transitions || !b.DOMAnimation) D.setWrapperTranslate(a), D.setWrapperTransition(f);
        else {
            var h = D.getWrapperTranslate(),
                i = Math.ceil((a - h) / f * (1e3 / 60)),
                j = h > a ? "toNext" : "toPrev",
                k = "toNext" === j ? h > a : a > h;
            if (D._DOMAnimating) return;
            e()
        }
        D.updateActiveSlide(a), b.onSlideNext && "next" === c && d.runCallbacks === !0 && D.fireCallback(b.onSlideNext, D, a), b.onSlidePrev && "prev" === c && d.runCallbacks === !0 && D.fireCallback(b.onSlidePrev, D, a), b.onSlideReset && "reset" === c && d.runCallbacks === !0 && D.fireCallback(b.onSlideReset, D, a), "next" !== c && "prev" !== c && "to" !== c || d.runCallbacks !== !0 || w(c)
    }

    function w(a) {
        if (D.callPlugins("onSlideChangeStart"), b.onSlideChangeStart)
            if (b.queueStartCallbacks && D.support.transitions) {
                if (D._queueStartCallbacks) return;
                D._queueStartCallbacks = !0, D.fireCallback(b.onSlideChangeStart, D, a), D.wrapperTransitionEnd(function() {
                    D._queueStartCallbacks = !1
                })
            } else D.fireCallback(b.onSlideChangeStart, D, a);
        if (b.onSlideChangeEnd)
            if (D.support.transitions)
                if (b.queueEndCallbacks) {
                    if (D._queueEndCallbacks) return;
                    D._queueEndCallbacks = !0, D.wrapperTransitionEnd(function(c) {
                        D.fireCallback(b.onSlideChangeEnd, c, a)
                    })
                } else D.wrapperTransitionEnd(function(c) {
                    D.fireCallback(b.onSlideChangeEnd, c, a)
                });
        else b.DOMAnimation || setTimeout(function() {
            D.fireCallback(b.onSlideChangeEnd, D, a)
        }, 10)
    }

    function x() {
        var a = D.paginationButtons;
        if (a)
            for (var b = 0; b < a.length; b++) D.h.removeEventListener(a[b], "click", z)
    }

    function y() {
        var a = D.paginationButtons;
        if (a)
            for (var b = 0; b < a.length; b++) D.h.addEventListener(a[b], "click", z)
    }

    function z(a) {
        for (var c, d = a.target || a.srcElement, e = D.paginationButtons, f = 0; f < e.length; f++) d === e[f] && (c = f);
        b.autoplay && D.stopAutoplay(!0), D.swipeTo(c)
    }

    function A() {
        ab = setTimeout(function() {
            b.loop ? (D.fixLoop(), D.swipeNext(!0, !0)) : D.swipeNext(!0, !0) || (b.autoplayStopOnLast ? (clearTimeout(ab), ab = void 0) : D.swipeTo(0)), D.wrapperTransitionEnd(function() {
                "undefined" != typeof ab && A()
            })
        }, b.autoplay)
    }

    function B() {
        D.calcSlides(), b.loader.slides.length > 0 && 0 === D.slides.length && D.loadSlides(), b.loop && D.createLoop(), D.init(), f(), b.pagination && D.createPagination(!0), b.loop || b.initialSlide > 0 ? D.swipeTo(b.initialSlide, 0, !1) : D.updateActiveSlide(0), b.autoplay && D.startAutoplay(), D.centerIndex = D.activeIndex, b.onSwiperCreated && D.fireCallback(b.onSwiperCreated, D), D.callPlugins("onSwiperCreated")
    }
    if (!document.body.outerHTML && document.body.__defineGetter__ && HTMLElement) {
        var C = HTMLElement.prototype;
        C.__defineGetter__ && C.__defineGetter__("outerHTML", function() {
            return (new XMLSerializer).serializeToString(this)
        })
    }
    if (window.getComputedStyle || (window.getComputedStyle = function(a) {
            return this.el = a, this.getPropertyValue = function(b) {
                var c = /(\-([a-z]){1})/g;
                return "float" === b && (b = "styleFloat"), c.test(b) && (b = b.replace(c, function() {
                    return arguments[2].toUpperCase()
                })), a.currentStyle[b] ? a.currentStyle[b] : null
            }, this
        }), Array.prototype.indexOf || (Array.prototype.indexOf = function(a, b) {
            for (var c = b || 0, d = this.length; d > c; c++)
                if (this[c] === a) return c;
            return -1
        }), (document.querySelectorAll || window.jQuery) && "undefined" != typeof a && (a.nodeType || 0 !== c(a).length)) {
        var D = this;
        D.touches = {
            start: 0,
            startX: 0,
            startY: 0,
            current: 0,
            currentX: 0,
            currentY: 0,
            diff: 0,
            abs: 0
        }, D.positions = {
            start: 0,
            abs: 0,
            diff: 0,
            current: 0
        }, D.times = {
            start: 0,
            end: 0
        }, D.id = (new Date).getTime(), D.container = a.nodeType ? a : c(a)[0], D.isTouched = !1, D.isMoved = !1, D.activeIndex = 0, D.centerIndex = 0, D.activeLoaderIndex = 0, D.activeLoopIndex = 0, D.previousIndex = null, D.velocity = 0, D.snapGrid = [], D.slidesGrid = [], D.imagesToLoad = [], D.imagesLoaded = 0, D.wrapperLeft = 0, D.wrapperRight = 0, D.wrapperTop = 0, D.wrapperBottom = 0, D.isAndroid = navigator.userAgent.toLowerCase().indexOf("android") >= 0;
        var E, F, G, H, I, J, K = {
            eventTarget: "wrapper",
            mode: "horizontal",
            touchRatio: 1,
            speed: 300,
            freeMode: !1,
            freeModeFluid: !1,
            momentumRatio: 1,
            momentumBounce: !0,
            momentumBounceRatio: 1,
            slidesPerView: 1,
            slidesPerGroup: 1,
            slidesPerViewFit: !0,
            simulateTouch: !0,
            followFinger: !0,
            shortSwipes: !0,
            longSwipesRatio: .5,
            moveStartThreshold: !1,
            onlyExternal: !1,
            createPagination: !0,
            pagination: !1,
            paginationElement: "span",
            paginationClickable: !1,
            paginationAsRange: !0,
            resistance: !0,
            scrollContainer: !1,
            preventLinks: !0,
            preventLinksPropagation: !1,
            noSwiping: !1,
            noSwipingClass: "swiper-no-swiping",
            initialSlide: 0,
            keyboardControl: !1,
            mousewheelControl: !1,
            mousewheelControlForceToAxis: !1,
            useCSS3Transforms: !0,
            autoplay: !1,
            autoplayDisableOnInteraction: !0,
            autoplayStopOnLast: !1,
            loop: !1,
            loopAdditionalSlides: 0,
            roundLengths: !1,
            calculateHeight: !1,
            cssWidthAndHeight: !1,
            updateOnImagesReady: !0,
            releaseFormElements: !0,
            watchActiveIndex: !1,
            visibilityFullFit: !1,
            offsetPxBefore: 0,
            offsetPxAfter: 0,
            offsetSlidesBefore: 0,
            offsetSlidesAfter: 0,
            centeredSlides: !1,
            queueStartCallbacks: !1,
            queueEndCallbacks: !1,
            autoResize: !0,
            resizeReInit: !1,
            DOMAnimation: !0,
            loader: {
                slides: [],
                slidesHTMLType: "inner",
                surroundGroups: 1,
                logic: "reload",
                loadAllSlides: !1
            },
            swipeToPrev: !0,
            swipeToNext: !0,
            slideElement: "div",
            slideClass: "swiper-slide",
            slideActiveClass: "swiper-slide-active",
            slideVisibleClass: "swiper-slide-visible",
            slideDuplicateClass: "swiper-slide-duplicate",
            wrapperClass: "swiper-wrapper",
            paginationElementClass: "swiper-pagination-switch",
            paginationActiveClass: "swiper-active-switch",
            paginationVisibleClass: "swiper-visible-switch"
        };
        b = b || {};
        for (var L in K)
            if (L in b && "object" == typeof b[L])
                for (var M in K[L]) M in b[L] || (b[L][M] = K[L][M]);
            else L in b || (b[L] = K[L]);
        D.params = b, b.scrollContainer && (b.freeMode = !0, b.freeModeFluid = !0), b.loop && (b.resistance = "100%");
        var N = "horizontal" === b.mode,
            O = ["mousedown", "mousemove", "mouseup"];
        D.browser.ie10 && (O = ["MSPointerDown", "MSPointerMove", "MSPointerUp"]), D.browser.ie11 && (O = ["pointerdown", "pointermove", "pointerup"]), D.touchEvents = {
            touchStart: D.support.touch || !b.simulateTouch ? "touchstart" : O[0],
            touchMove: D.support.touch || !b.simulateTouch ? "touchmove" : O[1],
            touchEnd: D.support.touch || !b.simulateTouch ? "touchend" : O[2]
        };
        for (var P = D.container.childNodes.length - 1; P >= 0; P--)
            if (D.container.childNodes[P].className)
                for (var Q = D.container.childNodes[P].className.split(/\s+/), R = 0; R < Q.length; R++) Q[R] === b.wrapperClass && (E = D.container.childNodes[P]);
        D.wrapper = E, D._extendSwiperSlide = function(a) {
            return a.append = function() {
                return b.loop ? a.insertAfter(D.slides.length - D.loopedSlides) : (D.wrapper.appendChild(a), D.reInit()), a
            }, a.prepend = function() {
                return b.loop ? (D.wrapper.insertBefore(a, D.slides[D.loopedSlides]), D.removeLoopedSlides(), D.calcSlides(), D.createLoop()) : D.wrapper.insertBefore(a, D.wrapper.firstChild), D.reInit(), a
            }, a.insertAfter = function(c) {
                if ("undefined" == typeof c) return !1;
                var d;
                return b.loop ? (d = D.slides[c + 1 + D.loopedSlides], d ? D.wrapper.insertBefore(a, d) : D.wrapper.appendChild(a), D.removeLoopedSlides(), D.calcSlides(), D.createLoop()) : (d = D.slides[c + 1], D.wrapper.insertBefore(a, d)), D.reInit(), a
            }, a.clone = function() {
                return D._extendSwiperSlide(a.cloneNode(!0))
            }, a.remove = function() {
                D.wrapper.removeChild(a), D.reInit()
            }, a.html = function(b) {
                return "undefined" == typeof b ? a.innerHTML : (a.innerHTML = b, a)
            }, a.index = function() {
                for (var b, c = D.slides.length - 1; c >= 0; c--) a === D.slides[c] && (b = c);
                return b
            }, a.isActive = function() {
                return a.index() === D.activeIndex ? !0 : !1
            }, a.swiperSlideDataStorage || (a.swiperSlideDataStorage = {}), a.getData = function(b) {
                return a.swiperSlideDataStorage[b]
            }, a.setData = function(b, c) {
                return a.swiperSlideDataStorage[b] = c, a
            }, a.data = function(b, c) {
                return "undefined" == typeof c ? a.getAttribute("data-" + b) : (a.setAttribute("data-" + b, c), a)
            }, a.getWidth = function(b, c) {
                return D.h.getWidth(a, b, c)
            }, a.getHeight = function(b, c) {
                return D.h.getHeight(a, b, c)
            }, a.getOffset = function() {
                return D.h.getOffset(a)
            }, a
        }, D.calcSlides = function(a) {
            var c = D.slides ? D.slides.length : !1;
            D.slides = [], D.displaySlides = [];
            for (var d = 0; d < D.wrapper.childNodes.length; d++)
                if (D.wrapper.childNodes[d].className)
                    for (var e = D.wrapper.childNodes[d].className, f = e.split(/\s+/), i = 0; i < f.length; i++) f[i] === b.slideClass && D.slides.push(D.wrapper.childNodes[d]);
            for (d = D.slides.length - 1; d >= 0; d--) D._extendSwiperSlide(D.slides[d]);
            c !== !1 && (c !== D.slides.length || a) && (h(), g(), D.updateActiveSlide(), D.params.pagination && D.createPagination(), D.callPlugins("numberOfSlidesChanged"))
        }, D.createSlide = function(a, c, d) {
            c = c || D.params.slideClass, d = d || b.slideElement;
            var e = document.createElement(d);
            return e.innerHTML = a || "", e.className = c, D._extendSwiperSlide(e)
        }, D.appendSlide = function(a, b, c) {
            return a ? a.nodeType ? D._extendSwiperSlide(a).append() : D.createSlide(a, b, c).append() : void 0
        }, D.prependSlide = function(a, b, c) {
            return a ? a.nodeType ? D._extendSwiperSlide(a).prepend() : D.createSlide(a, b, c).prepend() : void 0
        }, D.insertSlideAfter = function(a, b, c, d) {
            return "undefined" == typeof a ? !1 : b.nodeType ? D._extendSwiperSlide(b).insertAfter(a) : D.createSlide(b, c, d).insertAfter(a)
        }, D.removeSlide = function(a) {
            if (D.slides[a]) {
                if (b.loop) {
                    if (!D.slides[a + D.loopedSlides]) return !1;
                    D.slides[a + D.loopedSlides].remove(), D.removeLoopedSlides(), D.calcSlides(), D.createLoop()
                } else D.slides[a].remove();
                return !0
            }
            return !1
        }, D.removeLastSlide = function() {
            return D.slides.length > 0 ? (b.loop ? (D.slides[D.slides.length - 1 - D.loopedSlides].remove(), D.removeLoopedSlides(), D.calcSlides(), D.createLoop()) : D.slides[D.slides.length - 1].remove(), !0) : !1
        }, D.removeAllSlides = function() {
            for (var a = D.slides.length, b = D.slides.length - 1; b >= 0; b--) D.slides[b].remove(), b === a - 1 && D.setWrapperTranslate(0)
        }, D.getSlide = function(a) {
            return D.slides[a]
        }, D.getLastSlide = function() {
            return D.slides[D.slides.length - 1]
        }, D.getFirstSlide = function() {
            return D.slides[0]
        }, D.activeSlide = function() {
            return D.slides[D.activeIndex]
        }, D.fireCallback = function() {
            var a = arguments[0];
            if ("[object Array]" === Object.prototype.toString.call(a))
                for (var c = 0; c < a.length; c++) "function" == typeof a[c] && a[c](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
            else "[object String]" === Object.prototype.toString.call(a) ? b["on" + a] && D.fireCallback(b["on" + a], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]) : a(arguments[1], arguments[2], arguments[3], arguments[4], arguments[5])
        }, D.addCallback = function(a, b) {
            var c, e = this;
            return e.params["on" + a] ? d(this.params["on" + a]) ? this.params["on" + a].push(b) : "function" == typeof this.params["on" + a] ? (c = this.params["on" + a], this.params["on" + a] = [], this.params["on" + a].push(c), this.params["on" + a].push(b)) : void 0 : (this.params["on" + a] = [], this.params["on" + a].push(b))
        }, D.removeCallbacks = function(a) {
            D.params["on" + a] && (D.params["on" + a] = null)
        };
        var S = [];
        for (var T in D.plugins)
            if (b[T]) {
                var U = D.plugins[T](D, b[T]);
                U && S.push(U)
            }
        D.callPlugins = function(a, b) {
            b || (b = {});
            for (var c = 0; c < S.length; c++) a in S[c] && S[c][a](b)
        }, !D.browser.ie10 && !D.browser.ie11 || b.onlyExternal || D.wrapper.classList.add("swiper-wp8-" + (N ? "horizontal" : "vertical")), b.freeMode && (D.container.className += " swiper-free-mode"), D.initialized = !1, D.init = function(a, c) {
            var d = D.h.getWidth(D.container, !1, b.roundLengths),
                e = D.h.getHeight(D.container, !1, b.roundLengths);
            if (d !== D.width || e !== D.height || a) {
                D.width = d, D.height = e;
                var f, g, h, i, j, k, l;
                J = N ? d : e;
                var m = D.wrapper;
                if (a && D.calcSlides(c), "auto" === b.slidesPerView) {
                    var n = 0,
                        o = 0;
                    b.slidesOffset > 0 && (m.style.paddingLeft = "", m.style.paddingRight = "", m.style.paddingTop = "", m.style.paddingBottom = ""), m.style.width = "", m.style.height = "", b.offsetPxBefore > 0 && (N ? D.wrapperLeft = b.offsetPxBefore : D.wrapperTop = b.offsetPxBefore), b.offsetPxAfter > 0 && (N ? D.wrapperRight = b.offsetPxAfter : D.wrapperBottom = b.offsetPxAfter), b.centeredSlides && (N ? (D.wrapperLeft = (J - this.slides[0].getWidth(!0, b.roundLengths)) / 2, D.wrapperRight = (J - D.slides[D.slides.length - 1].getWidth(!0, b.roundLengths)) / 2) : (D.wrapperTop = (J - D.slides[0].getHeight(!0, b.roundLengths)) / 2, D.wrapperBottom = (J - D.slides[D.slides.length - 1].getHeight(!0, b.roundLengths)) / 2)), N ? (D.wrapperLeft >= 0 && (m.style.paddingLeft = D.wrapperLeft + "px"), D.wrapperRight >= 0 && (m.style.paddingRight = D.wrapperRight + "px")) : (D.wrapperTop >= 0 && (m.style.paddingTop = D.wrapperTop + "px"), D.wrapperBottom >= 0 && (m.style.paddingBottom = D.wrapperBottom + "px")), k = 0;
                    var p = 0;
                    for (D.snapGrid = [], D.slidesGrid = [], h = 0, l = 0; l < D.slides.length; l++) {
                        f = D.slides[l].getWidth(!0, b.roundLengths), g = D.slides[l].getHeight(!0, b.roundLengths), b.calculateHeight && (h = Math.max(h, g));
                        var q = N ? f : g;
                        if (b.centeredSlides) {
                            var r = l === D.slides.length - 1 ? 0 : D.slides[l + 1].getWidth(!0, b.roundLengths),
                                s = l === D.slides.length - 1 ? 0 : D.slides[l + 1].getHeight(!0, b.roundLengths),
                                t = N ? r : s;
                            if (q > J) {
                                if (b.slidesPerViewFit) D.snapGrid.push(k + D.wrapperLeft), D.snapGrid.push(k + q - J + D.wrapperLeft);
                                else
                                    for (var u = 0; u <= Math.floor(q / (J + D.wrapperLeft)); u++) D.snapGrid.push(0 === u ? k + D.wrapperLeft : k + D.wrapperLeft + J * u);
                                D.slidesGrid.push(k + D.wrapperLeft)
                            } else D.snapGrid.push(p), D.slidesGrid.push(p);
                            p += q / 2 + t / 2
                        } else {
                            if (q > J)
                                if (b.slidesPerViewFit) D.snapGrid.push(k), D.snapGrid.push(k + q - J);
                                else if (0 !== J)
                                for (var v = 0; v <= Math.floor(q / J); v++) D.snapGrid.push(k + J * v);
                            else D.snapGrid.push(k);
                            else D.snapGrid.push(k);
                            D.slidesGrid.push(k)
                        }
                        k += q, n += f, o += g
                    }
                    b.calculateHeight && (D.height = h), N ? (G = n + D.wrapperRight + D.wrapperLeft, b.cssWidthAndHeight && "height" !== b.cssWidthAndHeight || (m.style.width = n + "px"), b.cssWidthAndHeight && "width" !== b.cssWidthAndHeight || (m.style.height = D.height + "px")) : (b.cssWidthAndHeight && "height" !== b.cssWidthAndHeight || (m.style.width = D.width + "px"), b.cssWidthAndHeight && "width" !== b.cssWidthAndHeight || (m.style.height = o + "px"), G = o + D.wrapperTop + D.wrapperBottom)
                } else if (b.scrollContainer) m.style.width = "", m.style.height = "", i = D.slides[0].getWidth(!0, b.roundLengths), j = D.slides[0].getHeight(!0, b.roundLengths), G = N ? i : j, m.style.width = i + "px", m.style.height = j + "px", F = N ? i : j;
                else {
                    if (b.calculateHeight) {
                        for (h = 0, j = 0, N || (D.container.style.height = ""), m.style.height = "", l = 0; l < D.slides.length; l++) D.slides[l].style.height = "", h = Math.max(D.slides[l].getHeight(!0), h), N || (j += D.slides[l].getHeight(!0));
                        g = h, D.height = g, N ? j = g : (J = g, D.container.style.height = J + "px")
                    } else g = N ? D.height : D.height / b.slidesPerView, b.roundLengths && (g = Math.ceil(g)), j = N ? D.height : D.slides.length * g;
                    for (f = N ? D.width / b.slidesPerView : D.width, b.roundLengths && (f = Math.ceil(f)), i = N ? D.slides.length * f : D.width, F = N ? f : g, b.offsetSlidesBefore > 0 && (N ? D.wrapperLeft = F * b.offsetSlidesBefore : D.wrapperTop = F * b.offsetSlidesBefore), b.offsetSlidesAfter > 0 && (N ? D.wrapperRight = F * b.offsetSlidesAfter : D.wrapperBottom = F * b.offsetSlidesAfter), b.offsetPxBefore > 0 && (N ? D.wrapperLeft = b.offsetPxBefore : D.wrapperTop = b.offsetPxBefore), b.offsetPxAfter > 0 && (N ? D.wrapperRight = b.offsetPxAfter : D.wrapperBottom = b.offsetPxAfter), b.centeredSlides && (N ? (D.wrapperLeft = (J - F) / 2, D.wrapperRight = (J - F) / 2) : (D.wrapperTop = (J - F) / 2, D.wrapperBottom = (J - F) / 2)), N ? (D.wrapperLeft > 0 && (m.style.paddingLeft = D.wrapperLeft + "px"), D.wrapperRight > 0 && (m.style.paddingRight = D.wrapperRight + "px")) : (D.wrapperTop > 0 && (m.style.paddingTop = D.wrapperTop + "px"), D.wrapperBottom > 0 && (m.style.paddingBottom = D.wrapperBottom + "px")), G = N ? i + D.wrapperRight + D.wrapperLeft : j + D.wrapperTop + D.wrapperBottom, parseFloat(i) > 0 && (!b.cssWidthAndHeight || "height" === b.cssWidthAndHeight) && (m.style.width = i + "px"), parseFloat(j) > 0 && (!b.cssWidthAndHeight || "width" === b.cssWidthAndHeight) && (m.style.height = j + "px"), k = 0, D.snapGrid = [], D.slidesGrid = [], l = 0; l < D.slides.length; l++) D.snapGrid.push(k), D.slidesGrid.push(k), k += F, parseFloat(f) > 0 && (!b.cssWidthAndHeight || "height" === b.cssWidthAndHeight) && (D.slides[l].style.width = f + "px"), parseFloat(g) > 0 && (!b.cssWidthAndHeight || "width" === b.cssWidthAndHeight) && (D.slides[l].style.height = g + "px")
                }
                D.initialized ? (D.callPlugins("onInit"), b.onInit && D.fireCallback(b.onInit, D)) : (D.callPlugins("onFirstInit"), b.onFirstInit && D.fireCallback(b.onFirstInit, D)), D.initialized = !0
            }
        }, D.reInit = function(a) {
            D.init(!0, a)
        }, D.resizeFix = function(a) {
            D.callPlugins("beforeResizeFix"), D.init(b.resizeReInit || a), b.freeMode ? D.getWrapperTranslate() < -e() && (D.setWrapperTransition(0), D.setWrapperTranslate(-e())) : (D.swipeTo(b.loop ? D.activeLoopIndex : D.activeIndex, 0, !1), b.autoplay && (D.support.transitions && "undefined" != typeof ab ? "undefined" != typeof ab && (clearTimeout(ab), ab = void 0, D.startAutoplay()) : "undefined" != typeof bb && (clearInterval(bb), bb = void 0, D.startAutoplay()))), D.callPlugins("afterResizeFix")
        }, D.destroy = function(a) {
            var c = D.h.removeEventListener,
                d = "wrapper" === b.eventTarget ? D.wrapper : D.container;
            if (D.browser.ie10 || D.browser.ie11 ? (c(d, D.touchEvents.touchStart, p), c(document, D.touchEvents.touchMove, q), c(document, D.touchEvents.touchEnd, r)) : (D.support.touch && (c(d, "touchstart", p), c(d, "touchmove", q), c(d, "touchend", r)), b.simulateTouch && (c(d, "mousedown", p), c(document, "mousemove", q), c(document, "mouseup", r))), b.autoResize && c(window, "resize", D.resizeFix), h(), b.paginationClickable && x(), b.mousewheelControl && D._wheelEvent && c(D.container, D._wheelEvent, j), b.keyboardControl && c(document, "keydown", i), b.autoplay && D.stopAutoplay(), a) {
                D.wrapper.removeAttribute("style");
                for (var e = 0; e < D.slides.length; e++) D.slides[e].removeAttribute("style")
            }
            D.callPlugins("onDestroy"), window.jQuery && window.jQuery(D.container).data("swiper") && window.jQuery(D.container).removeData("swiper"), window.Zepto && window.Zepto(D.container).data("swiper") && window.Zepto(D.container).removeData("swiper"), D = null
        }, D.disableKeyboardControl = function() {
            b.keyboardControl = !1, D.h.removeEventListener(document, "keydown", i)
        }, D.enableKeyboardControl = function() {
            b.keyboardControl = !0, D.h.addEventListener(document, "keydown", i)
        };
        var V = (new Date).getTime();
        if (D.disableMousewheelControl = function() {
                return D._wheelEvent ? (b.mousewheelControl = !1, D.h.removeEventListener(D.container, D._wheelEvent, j), !0) : !1
            }, D.enableMousewheelControl = function() {
                return D._wheelEvent ? (b.mousewheelControl = !0, D.h.addEventListener(D.container, D._wheelEvent, j), !0) : !1
            }, b.grabCursor) {
            var W = D.container.style;
            W.cursor = "move", W.cursor = "grab", W.cursor = "-moz-grab", W.cursor = "-webkit-grab"
        }
        D.allowSlideClick = !0, D.allowLinks = !0;
        var X, Y, Z, $ = !1,
            _ = !0;
        D.swipeNext = function(a, c) {
            "undefined" == typeof a && (a = !0), !c && b.loop && D.fixLoop(), !c && b.autoplay && D.stopAutoplay(!0), D.callPlugins("onSwipeNext");
            var d = D.getWrapperTranslate().toFixed(2),
                f = d;
            if ("auto" === b.slidesPerView) {
                for (var g = 0; g < D.snapGrid.length; g++)
                    if (-d >= D.snapGrid[g].toFixed(2) && -d < D.snapGrid[g + 1].toFixed(2)) {
                        f = -D.snapGrid[g + 1];
                        break
                    }
            } else {
                var h = F * b.slidesPerGroup;
                f = -(Math.floor(Math.abs(d) / Math.floor(h)) * h + h)
            }
            return f < -e() && (f = -e()), f === d ? !1 : (v(f, "next", {
                runCallbacks: a
            }), !0)
        }, D.swipePrev = function(a, c) {
            "undefined" == typeof a && (a = !0), !c && b.loop && D.fixLoop(), !c && b.autoplay && D.stopAutoplay(!0), D.callPlugins("onSwipePrev");
            var d, e = Math.ceil(D.getWrapperTranslate());
            if ("auto" === b.slidesPerView) {
                d = 0;
                for (var f = 1; f < D.snapGrid.length; f++) {
                    if (-e === D.snapGrid[f]) {
                        d = -D.snapGrid[f - 1];
                        break
                    }
                    if (-e > D.snapGrid[f] && -e < D.snapGrid[f + 1]) {
                        d = -D.snapGrid[f];
                        break
                    }
                }
            } else {
                var g = F * b.slidesPerGroup;
                d = -(Math.ceil(-e / g) - 1) * g
            }
            return d > 0 && (d = 0), d === e ? !1 : (v(d, "prev", {
                runCallbacks: a
            }), !0)
        }, D.swipeReset = function(a) {
            "undefined" == typeof a && (a = !0), D.callPlugins("onSwipeReset"); {
                var c, d = D.getWrapperTranslate(),
                    f = F * b.slidesPerGroup; - e()
            }
            if ("auto" === b.slidesPerView) {
                c = 0;
                for (var g = 0; g < D.snapGrid.length; g++) {
                    if (-d === D.snapGrid[g]) return;
                    if (-d >= D.snapGrid[g] && -d < D.snapGrid[g + 1]) {
                        c = D.positions.diff > 0 ? -D.snapGrid[g + 1] : -D.snapGrid[g];
                        break
                    }
                } - d >= D.snapGrid[D.snapGrid.length - 1] && (c = -D.snapGrid[D.snapGrid.length - 1]), d <= -e() && (c = -e())
            } else c = 0 > d ? Math.round(d / f) * f : 0, d <= -e() && (c = -e());
            return b.scrollContainer && (c = 0 > d ? d : 0), c < -e() && (c = -e()), b.scrollContainer && J > F && (c = 0), c === d ? !1 : (v(c, "reset", {
                runCallbacks: a
            }), !0)
        }, D.swipeTo = function(a, c, d) {
            a = parseInt(a, 10), D.callPlugins("onSwipeTo", {
                index: a,
                speed: c
            }), b.loop && (a += D.loopedSlides);
            var f = D.getWrapperTranslate();
            if (!(!isFinite(a) || a > D.slides.length - 1 || 0 > a)) {
                var g;
                return g = "auto" === b.slidesPerView ? -D.slidesGrid[a] : -a * F, g < -e() && (g = -e()), g === f ? !1 : ("undefined" == typeof d && (d = !0), v(g, "to", {
                    index: a,
                    speed: c,
                    runCallbacks: d
                }), !0)
            }
        }, D._queueStartCallbacks = !1, D._queueEndCallbacks = !1, D.updateActiveSlide = function(a) {
            if (D.initialized && 0 !== D.slides.length) {
                D.previousIndex = D.activeIndex, "undefined" == typeof a && (a = D.getWrapperTranslate()), a > 0 && (a = 0);
                var c;
                if ("auto" === b.slidesPerView) {
                    if (D.activeIndex = D.slidesGrid.indexOf(-a), D.activeIndex < 0) {
                        for (c = 0; c < D.slidesGrid.length - 1 && !(-a > D.slidesGrid[c] && -a < D.slidesGrid[c + 1]); c++);
                        var d = Math.abs(D.slidesGrid[c] + a),
                            e = Math.abs(D.slidesGrid[c + 1] + a);
                        D.activeIndex = e >= d ? c : c + 1
                    }
                } else D.activeIndex = Math[b.visibilityFullFit ? "ceil" : "round"](-a / F);
                if (D.activeIndex === D.slides.length && (D.activeIndex = D.slides.length - 1), D.activeIndex < 0 && (D.activeIndex = 0), D.slides[D.activeIndex]) {
                    if (D.calcVisibleSlides(a), D.support.classList) {
                        var f;
                        for (c = 0; c < D.slides.length; c++) f = D.slides[c], f.classList.remove(b.slideActiveClass), D.visibleSlides.indexOf(f) >= 0 ? f.classList.add(b.slideVisibleClass) : f.classList.remove(b.slideVisibleClass);
                        D.slides[D.activeIndex].classList.add(b.slideActiveClass)
                    } else {
                        var g = new RegExp("\\s*" + b.slideActiveClass),
                            h = new RegExp("\\s*" + b.slideVisibleClass);
                        for (c = 0; c < D.slides.length; c++) D.slides[c].className = D.slides[c].className.replace(g, "").replace(h, ""), D.visibleSlides.indexOf(D.slides[c]) >= 0 && (D.slides[c].className += " " + b.slideVisibleClass);
                        D.slides[D.activeIndex].className += " " + b.slideActiveClass
                    }
                    if (b.loop) {
                        var i = D.loopedSlides;
                        D.activeLoopIndex = D.activeIndex - i, D.activeLoopIndex >= D.slides.length - 2 * i && (D.activeLoopIndex = D.slides.length - 2 * i - D.activeLoopIndex), D.activeLoopIndex < 0 && (D.activeLoopIndex = D.slides.length - 2 * i + D.activeLoopIndex), D.activeLoopIndex < 0 && (D.activeLoopIndex = 0)
                    } else D.activeLoopIndex = D.activeIndex;
                    b.pagination && D.updatePagination(a)
                }
            }
        }, D.createPagination = function(a) {
            if (b.paginationClickable && D.paginationButtons && x(), D.paginationContainer = b.pagination.nodeType ? b.pagination : c(b.pagination)[0], b.createPagination) {
                var d = "",
                    e = D.slides.length,
                    f = e;
                b.loop && (f -= 2 * D.loopedSlides);
                for (var g = 0; f > g; g++) d += "<" + b.paginationElement + ' class="' + b.paginationElementClass + '"></' + b.paginationElement + ">";
                D.paginationContainer.innerHTML = d
            }
            D.paginationButtons = c("." + b.paginationElementClass, D.paginationContainer), a || D.updatePagination(), D.callPlugins("onCreatePagination"), b.paginationClickable && y()
        }, D.updatePagination = function(a) {
            if (b.pagination && !(D.slides.length < 1)) {
                var d = c("." + b.paginationActiveClass, D.paginationContainer);
                if (d) {
                    var e = D.paginationButtons;
                    if (0 !== e.length) {
                        for (var f = 0; f < e.length; f++) e[f].className = b.paginationElementClass;
                        var g = b.loop ? D.loopedSlides : 0;
                        if (b.paginationAsRange) {
                            D.visibleSlides || D.calcVisibleSlides(a);
                            var h, i = [];
                            for (h = 0; h < D.visibleSlides.length; h++) {
                                var j = D.slides.indexOf(D.visibleSlides[h]) - g;
                                b.loop && 0 > j && (j = D.slides.length - 2 * D.loopedSlides + j), b.loop && j >= D.slides.length - 2 * D.loopedSlides && (j = D.slides.length - 2 * D.loopedSlides - j, j = Math.abs(j)), i.push(j)
                            }
                            for (h = 0; h < i.length; h++) e[i[h]] && (e[i[h]].className += " " + b.paginationVisibleClass);
                            b.loop ? void 0 !== e[D.activeLoopIndex] && (e[D.activeLoopIndex].className += " " + b.paginationActiveClass) : e[D.activeIndex] && (e[D.activeIndex].className += " " + b.paginationActiveClass)
                        } else b.loop ? e[D.activeLoopIndex] && (e[D.activeLoopIndex].className += " " + b.paginationActiveClass + " " + b.paginationVisibleClass) : e[D.activeIndex] && (e[D.activeIndex].className += " " + b.paginationActiveClass + " " + b.paginationVisibleClass)
                    }
                }
            }
        }, D.calcVisibleSlides = function(a) {
            var c = [],
                d = 0,
                e = 0,
                f = 0;
            N && D.wrapperLeft > 0 && (a += D.wrapperLeft), !N && D.wrapperTop > 0 && (a += D.wrapperTop);
            for (var g = 0; g < D.slides.length; g++) {
                d += e, e = "auto" === b.slidesPerView ? N ? D.h.getWidth(D.slides[g], !0, b.roundLengths) : D.h.getHeight(D.slides[g], !0, b.roundLengths) : F, f = d + e;
                var h = !1;
                b.visibilityFullFit ? (d >= -a && -a + J >= f && (h = !0), -a >= d && f >= -a + J && (h = !0)) : (f > -a && -a + J >= f && (h = !0), d >= -a && -a + J > d && (h = !0), -a > d && f > -a + J && (h = !0)), h && c.push(D.slides[g])
            }
            0 === c.length && (c = [D.slides[D.activeIndex]]), D.visibleSlides = c
        };
        var ab, bb;
        D.startAutoplay = function() {
            if (D.support.transitions) {
                if ("undefined" != typeof ab) return !1;
                if (!b.autoplay) return;
                D.callPlugins("onAutoplayStart"), b.onAutoplayStart && D.fireCallback(b.onAutoplayStart, D), A()
            } else {
                if ("undefined" != typeof bb) return !1;
                if (!b.autoplay) return;
                D.callPlugins("onAutoplayStart"), b.onAutoplayStart && D.fireCallback(b.onAutoplayStart, D), bb = setInterval(function() {
                    b.loop ? (D.fixLoop(), D.swipeNext(!0, !0)) : D.swipeNext(!0, !0) || (b.autoplayStopOnLast ? (clearInterval(bb), bb = void 0) : D.swipeTo(0))
                }, b.autoplay)
            }
        }, D.stopAutoplay = function(a) {
            if (D.support.transitions) {
                if (!ab) return;
                ab && clearTimeout(ab), ab = void 0, a && !b.autoplayDisableOnInteraction && D.wrapperTransitionEnd(function() {
                    A()
                }), D.callPlugins("onAutoplayStop"), b.onAutoplayStop && D.fireCallback(b.onAutoplayStop, D)
            } else bb && clearInterval(bb), bb = void 0, D.callPlugins("onAutoplayStop"), b.onAutoplayStop && D.fireCallback(b.onAutoplayStop, D)
        }, D.loopCreated = !1, D.removeLoopedSlides = function() {
            if (D.loopCreated)
                for (var a = 0; a < D.slides.length; a++) D.slides[a].getData("looped") === !0 && D.wrapper.removeChild(D.slides[a])
        }, D.createLoop = function() {
            if (0 !== D.slides.length) {
                D.loopedSlides = "auto" === b.slidesPerView ? b.loopedSlides || 1 : Math.floor(b.slidesPerView) + b.loopAdditionalSlides, D.loopedSlides > D.slides.length && (D.loopedSlides = D.slides.length);
                var a, c = "",
                    d = "",
                    e = "",
                    f = D.slides.length,
                    g = Math.floor(D.loopedSlides / f),
                    h = D.loopedSlides % f;
                for (a = 0; g * f > a; a++) {
                    var i = a;
                    if (a >= f) {
                        var j = Math.floor(a / f);
                        i = a - f * j
                    }
                    e += D.slides[i].outerHTML
                }
                for (a = 0; h > a; a++) d += u(b.slideDuplicateClass, D.slides[a].outerHTML);
                for (a = f - h; f > a; a++) c += u(b.slideDuplicateClass, D.slides[a].outerHTML);
                var k = c + e + E.innerHTML + e + d;
                for (E.innerHTML = k, D.loopCreated = !0, D.calcSlides(), a = 0; a < D.slides.length; a++)(a < D.loopedSlides || a >= D.slides.length - D.loopedSlides) && D.slides[a].setData("looped", !0);
                D.callPlugins("onCreateLoop")
            }
        }, D.fixLoop = function() {
            var a;
            D.activeIndex < D.loopedSlides ? (a = D.slides.length - 3 * D.loopedSlides + D.activeIndex, D.swipeTo(a, 0, !1)) : ("auto" === b.slidesPerView && D.activeIndex >= 2 * D.loopedSlides || D.activeIndex > D.slides.length - 2 * b.slidesPerView) && (a = -D.slides.length + D.activeIndex + D.loopedSlides, D.swipeTo(a, 0, !1))
        }, D.loadSlides = function() {
            var a = "";
            D.activeLoaderIndex = 0;
            for (var c = b.loader.slides, d = b.loader.loadAllSlides ? c.length : b.slidesPerView * (1 + b.loader.surroundGroups), e = 0; d > e; e++) a += "outer" === b.loader.slidesHTMLType ? c[e] : "<" + b.slideElement + ' class="' + b.slideClass + '" data-swiperindex="' + e + '">' + c[e] + "</" + b.slideElement + ">";
            D.wrapper.innerHTML = a, D.calcSlides(!0), b.loader.loadAllSlides || D.wrapperTransitionEnd(D.reloadSlides, !0)
        }, D.reloadSlides = function() {
            var a = b.loader.slides,
                c = parseInt(D.activeSlide().data("swiperindex"), 10);
            if (!(0 > c || c > a.length - 1)) {
                D.activeLoaderIndex = c;
                var d = Math.max(0, c - b.slidesPerView * b.loader.surroundGroups),
                    e = Math.min(c + b.slidesPerView * (1 + b.loader.surroundGroups) - 1, a.length - 1);
                if (c > 0) {
                    var f = -F * (c - d);
                    D.setWrapperTranslate(f), D.setWrapperTransition(0)
                }
                var g;
                if ("reload" === b.loader.logic) {
                    D.wrapper.innerHTML = "";
                    var h = "";
                    for (g = d; e >= g; g++) h += "outer" === b.loader.slidesHTMLType ? a[g] : "<" + b.slideElement + ' class="' + b.slideClass + '" data-swiperindex="' + g + '">' + a[g] + "</" + b.slideElement + ">";
                    D.wrapper.innerHTML = h
                } else {
                    var i = 1e3,
                        j = 0;
                    for (g = 0; g < D.slides.length; g++) {
                        var k = D.slides[g].data("swiperindex");
                        d > k || k > e ? D.wrapper.removeChild(D.slides[g]) : (i = Math.min(k, i), j = Math.max(k, j))
                    }
                    for (g = d; e >= g; g++) {
                        var l;
                        i > g && (l = document.createElement(b.slideElement), l.className = b.slideClass, l.setAttribute("data-swiperindex", g), l.innerHTML = a[g], D.wrapper.insertBefore(l, D.wrapper.firstChild)), g > j && (l = document.createElement(b.slideElement), l.className = b.slideClass, l.setAttribute("data-swiperindex", g), l.innerHTML = a[g], D.wrapper.appendChild(l))
                    }
                }
                D.reInit(!0)
            }
        }, B()
    }
};
Swiper.prototype = {
        plugins: {},
        wrapperTransitionEnd: function(a, b) {
            "use strict";

            function c(h) {
                if (h.target === f && (a(e), e.params.queueEndCallbacks && (e._queueEndCallbacks = !1), !b))
                    for (d = 0; d < g.length; d++) e.h.removeEventListener(f, g[d], c)
            }
            var d, e = this,
                f = e.wrapper,
                g = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"];
            if (a)
                for (d = 0; d < g.length; d++) e.h.addEventListener(f, g[d], c)
        },
        getWrapperTranslate: function(a) {
            "use strict";
            var b, c, d, e, f = this.wrapper;
            return "undefined" == typeof a && (a = "horizontal" === this.params.mode ? "x" : "y"), this.support.transforms && this.params.useCSS3Transforms ? (d = window.getComputedStyle(f, null), window.WebKitCSSMatrix ? e = new WebKitCSSMatrix("none" === d.webkitTransform ? "" : d.webkitTransform) : (e = d.MozTransform || d.OTransform || d.MsTransform || d.msTransform || d.transform || d.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,"), b = e.toString().split(",")), "x" === a && (c = window.WebKitCSSMatrix ? e.m41 : parseFloat(16 === b.length ? b[12] : b[4])), "y" === a && (c = window.WebKitCSSMatrix ? e.m42 : parseFloat(16 === b.length ? b[13] : b[5]))) : ("x" === a && (c = parseFloat(f.style.left, 10) || 0), "y" === a && (c = parseFloat(f.style.top, 10) || 0)), c || 0
        },
        setWrapperTranslate: function(a, b, c) {
            "use strict";
            var d, e = this.wrapper.style,
                f = {
                    x: 0,
                    y: 0,
                    z: 0
                };
            3 === arguments.length ? (f.x = a, f.y = b, f.z = c) : ("undefined" == typeof b && (b = "horizontal" === this.params.mode ? "x" : "y"), f[b] = a), this.support.transforms && this.params.useCSS3Transforms ? (d = this.support.transforms3d ? "translate3d(" + f.x + "px, " + f.y + "px, " + f.z + "px)" : "translate(" + f.x + "px, " + f.y + "px)", e.webkitTransform = e.MsTransform = e.msTransform = e.MozTransform = e.OTransform = e.transform = d) : (e.left = f.x + "px", e.top = f.y + "px"), this.callPlugins("onSetWrapperTransform", f), this.params.onSetWrapperTransform && this.fireCallback(this.params.onSetWrapperTransform, this, f)
        },
        setWrapperTransition: function(a) {
            "use strict";
            var b = this.wrapper.style;
            b.webkitTransitionDuration = b.MsTransitionDuration = b.msTransitionDuration = b.MozTransitionDuration = b.OTransitionDuration = b.transitionDuration = a / 1e3 + "s", this.callPlugins("onSetWrapperTransition", {
                duration: a
            }), this.params.onSetWrapperTransition && this.fireCallback(this.params.onSetWrapperTransition, this, a)
        },
        h: {
            getWidth: function(a, b, c) {
                "use strict";
                var d = window.getComputedStyle(a, null).getPropertyValue("width"),
                    e = parseFloat(d);
                return (isNaN(e) || d.indexOf("%") > 0 || 0 > e) && (e = a.offsetWidth - parseFloat(window.getComputedStyle(a, null).getPropertyValue("padding-left")) - parseFloat(window.getComputedStyle(a, null).getPropertyValue("padding-right"))), b && (e += parseFloat(window.getComputedStyle(a, null).getPropertyValue("padding-left")) + parseFloat(window.getComputedStyle(a, null).getPropertyValue("padding-right"))), c ? Math.ceil(e) : e
            },
            getHeight: function(a, b, c) {
                "use strict";
                if (b) return a.offsetHeight;
                var d = window.getComputedStyle(a, null).getPropertyValue("height"),
                    e = parseFloat(d);
                return (isNaN(e) || d.indexOf("%") > 0 || 0 > e) && (e = a.offsetHeight - parseFloat(window.getComputedStyle(a, null).getPropertyValue("padding-top")) - parseFloat(window.getComputedStyle(a, null).getPropertyValue("padding-bottom"))), b && (e += parseFloat(window.getComputedStyle(a, null).getPropertyValue("padding-top")) + parseFloat(window.getComputedStyle(a, null).getPropertyValue("padding-bottom"))), c ? Math.ceil(e) : e
            },
            getOffset: function(a) {
                "use strict";
                var b = a.getBoundingClientRect(),
                    c = document.body,
                    d = a.clientTop || c.clientTop || 0,
                    e = a.clientLeft || c.clientLeft || 0,
                    f = window.pageYOffset || a.scrollTop,
                    g = window.pageXOffset || a.scrollLeft;
                return document.documentElement && !window.pageYOffset && (f = document.documentElement.scrollTop, g = document.documentElement.scrollLeft), {
                    top: b.top + f - d,
                    left: b.left + g - e
                }
            },
            windowWidth: function() {
                "use strict";
                return window.innerWidth ? window.innerWidth : document.documentElement && document.documentElement.clientWidth ? document.documentElement.clientWidth : void 0
            },
            windowHeight: function() {
                "use strict";
                return window.innerHeight ? window.innerHeight : document.documentElement && document.documentElement.clientHeight ? document.documentElement.clientHeight : void 0
            },
            windowScroll: function() {
                "use strict";
                return "undefined" != typeof pageYOffset ? {
                    left: window.pageXOffset,
                    top: window.pageYOffset
                } : document.documentElement ? {
                    left: document.documentElement.scrollLeft,
                    top: document.documentElement.scrollTop
                } : void 0
            },
            addEventListener: function(a, b, c, d) {
                "use strict";
                "undefined" == typeof d && (d = !1), a.addEventListener ? a.addEventListener(b, c, d) : a.attachEvent && a.attachEvent("on" + b, c)
            },
            removeEventListener: function(a, b, c, d) {
                "use strict";
                "undefined" == typeof d && (d = !1), a.removeEventListener ? a.removeEventListener(b, c, d) : a.detachEvent && a.detachEvent("on" + b, c)
            }
        },
        setTransform: function(a, b) {
            "use strict";
            var c = a.style;
            c.webkitTransform = c.MsTransform = c.msTransform = c.MozTransform = c.OTransform = c.transform = b
        },
        setTranslate: function(a, b) {
            "use strict";
            var c = a.style,
                d = {
                    x: b.x || 0,
                    y: b.y || 0,
                    z: b.z || 0
                },
                e = this.support.transforms3d ? "translate3d(" + d.x + "px," + d.y + "px," + d.z + "px)" : "translate(" + d.x + "px," + d.y + "px)";
            c.webkitTransform = c.MsTransform = c.msTransform = c.MozTransform = c.OTransform = c.transform = e, this.support.transforms || (c.left = d.x + "px", c.top = d.y + "px")
        },
        setTransition: function(a, b) {
            "use strict";
            var c = a.style;
            c.webkitTransitionDuration = c.MsTransitionDuration = c.msTransitionDuration = c.MozTransitionDuration = c.OTransitionDuration = c.transitionDuration = b + "ms"
        },
        support: {
            touch: window.Modernizr && Modernizr.touch === !0 || function() {
                "use strict";
                return !!("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch)
            }(),
            transforms3d: window.Modernizr && Modernizr.csstransforms3d === !0 || function() {
                "use strict";
                var a = document.createElement("div").style;
                return "webkitPerspective" in a || "MozPerspective" in a || "OPerspective" in a || "MsPerspective" in a || "perspective" in a
            }(),
            transforms: window.Modernizr && Modernizr.csstransforms === !0 || function() {
                "use strict";
                var a = document.createElement("div").style;
                return "transform" in a || "WebkitTransform" in a || "MozTransform" in a || "msTransform" in a || "MsTransform" in a || "OTransform" in a
            }(),
            transitions: window.Modernizr && Modernizr.csstransitions === !0 || function() {
                "use strict";
                var a = document.createElement("div").style;
                return "transition" in a || "WebkitTransition" in a || "MozTransition" in a || "msTransition" in a || "MsTransition" in a || "OTransition" in a
            }(),
            classList: function() {
                "use strict";
                var a = document.createElement("div");
                return "classList" in a
            }()
        },
        browser: {
            ie8: function() {
                "use strict";
                var a = -1;
                if ("Microsoft Internet Explorer" === navigator.appName) {
                    var b = navigator.userAgent,
                        c = new RegExp(/MSIE ([0-9]{1,}[\.0-9]{0,})/);
                    null !== c.exec(b) && (a = parseFloat(RegExp.$1))
                }
                return -1 !== a && 9 > a
            }(),
            ie10: window.navigator.msPointerEnabled,
            ie11: window.navigator.pointerEnabled
        }
    }, (window.jQuery || window.Zepto) && ! function(a) {
        "use strict";
        a.fn.swiper = function(b) {
            var c;
            return this.each(function(d) {
                var e = a(this),
                    f = new Swiper(e[0], b);
                d || (c = f), e.data("swiper", f)
            }), c
        }
    }(window.jQuery || window.Zepto), "undefined" != typeof module ? module.exports = Swiper : "function" == typeof define && define.amd && define([], function() {
        "use strict";
        return Swiper
    }), ! function(a, b) {
        "function" == typeof define && define.amd ? define(b) : "object" == typeof exports ? module.exports = b() : a.PhotoSwipe = b()
    }(this, function() {
        "use strict";
        var a = function(a, b, c, d) {
            var e = {
                features: null,
                bind: function(a, b, c, d) {
                    var e = (d ? "remove" : "add") + "EventListener";
                    b = b.split(" ");
                    for (var f = 0; f < b.length; f++) b[f] && a[e](b[f], c, !1)
                },
                isArray: function(a) {
                    return a instanceof Array
                },
                createEl: function(a, b) {
                    var c = document.createElement(b || "div");
                    return a && (c.className = a), c
                },
                getScrollY: function() {
                    var a = window.pageYOffset;
                    return void 0 !== a ? a : document.documentElement.scrollTop
                },
                unbind: function(a, b, c) {
                    e.bind(a, b, c, !0)
                },
                removeClass: function(a, b) {
                    var c = new RegExp("(\\s|^)" + b + "(\\s|$)");
                    a.className = a.className.replace(c, " ").replace(/^\s\s*/, "").replace(/\s\s*$/, "")
                },
                addClass: function(a, b) {
                    e.hasClass(a, b) || (a.className += (a.className ? " " : "") + b)
                },
                hasClass: function(a, b) {
                    return a.className && new RegExp("(^|\\s)" + b + "(\\s|$)").test(a.className)
                },
                getChildByClass: function(a, b) {
                    for (var c = a.firstChild; c;) {
                        if (e.hasClass(c, b)) return c;
                        c = c.nextSibling
                    }
                },
                arraySearch: function(a, b, c) {
                    for (var d = a.length; d--;)
                        if (a[d][c] === b) return d;
                    return -1
                },
                extend: function(a, b, c) {
                    for (var d in b)
                        if (b.hasOwnProperty(d)) {
                            if (c && a.hasOwnProperty(d)) continue;
                            a[d] = b[d]
                        }
                },
                easing: {
                    sine: {
                        out: function(a) {
                            return Math.sin(a * (Math.PI / 2))
                        },
                        inOut: function(a) {
                            return -(Math.cos(Math.PI * a) - 1) / 2
                        }
                    },
                    cubic: {
                        out: function(a) {
                            return --a * a * a + 1
                        }
                    }
                },
                detectFeatures: function() {
                    if (e.features) return e.features;
                    var a = e.createEl(),
                        b = a.style,
                        c = "",
                        d = {};
                    if (d.oldIE = document.all && !document.addEventListener, d.touch = "ontouchstart" in window, window.requestAnimationFrame && (d.raf = window.requestAnimationFrame, d.caf = window.cancelAnimationFrame), d.pointerEvent = navigator.pointerEnabled || navigator.msPointerEnabled, !d.pointerEvent) {
                        var f = navigator.userAgent;
                        if (/iP(hone|od)/.test(navigator.platform)) {
                            var g = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
                            g && g.length > 0 && (g = parseInt(g[1], 10), g >= 1 && 8 > g && (d.isOldIOSPhone = !0))
                        }
                        var h = f.match(/Android\s([0-9\.]*)/),
                            i = h ? h[1] : 0;
                        i = parseFloat(i), i >= 1 && (4.4 > i && (d.isOldAndroid = !0), d.androidVersion = i), d.isMobileOpera = /opera mini|opera mobi/i.test(f)
                    }
                    for (var j, k, l = ["transform", "perspective", "animationName"], m = ["", "webkit", "Moz", "ms", "O"], n = 0; 4 > n; n++) {
                        c = m[n];
                        for (var o = 0; 3 > o; o++) j = l[o], k = c + (c ? j.charAt(0).toUpperCase() + j.slice(1) : j), !d[j] && k in b && (d[j] = k);
                        c && !d.raf && (c = c.toLowerCase(), d.raf = window[c + "RequestAnimationFrame"], d.raf && (d.caf = window[c + "CancelAnimationFrame"] || window[c + "CancelRequestAnimationFrame"]))
                    }
                    if (!d.raf) {
                        var p = 0;
                        d.raf = function(a) {
                            var b = (new Date).getTime(),
                                c = Math.max(0, 16 - (b - p)),
                                d = window.setTimeout(function() {
                                    a(b + c)
                                }, c);
                            return p = b + c, d
                        }, d.caf = function(a) {
                            clearTimeout(a)
                        }
                    }
                    return d.svg = !!document.createElementNS && !!document.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect, e.features = d, d
                }
            };
            e.detectFeatures(), e.features.oldIE && (e.bind = function(a, b, c, d) {
                b = b.split(" ");
                for (var e, f = (d ? "detach" : "attach") + "Event", g = function() {
                        c.handleEvent.call(c)
                    }, h = 0; h < b.length; h++)
                    if (e = b[h])
                        if ("object" == typeof c && c.handleEvent) {
                            if (d) {
                                if (!c["oldIE" + e]) return !1
                            } else c["oldIE" + e] = g;
                            a[f]("on" + e, c["oldIE" + e])
                        } else a[f]("on" + e, c)
            });
            var f = this,
                g = 25,
                h = 3,
                i = {
                    allowPanToNext: !0,
                    spacing: .12,
                    bgOpacity: 1,
                    mouseUsed: !1,
                    loop: !0,
                    pinchToClose: !0,
                    closeOnScroll: !0,
                    closeOnVerticalDrag: !0,
                    hideAnimationDuration: 333,
                    showAnimationDuration: 333,
                    showHideOpacity: !1,
                    focus: !0,
                    escKey: !0,
                    arrowKeys: !0,
                    mainScrollEndFriction: .35,
                    panEndFriction: .35,
                    isClickableElement: function(a) {
                        return "A" === a.tagName
                    },
                    getDoubleTapZoom: function(a, b) {
                        return a ? 1 : b.initialZoomLevel < .7 ? 1 : 1.5
                    },
                    maxSpreadZoom: 2,
                    scaleMode: "fit",
                    modal: !0,
                    alwaysFadeIn: !1
                };
            e.extend(i, d);
            var j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, $, _, ab, bb, cb, db, eb, fb, gb, hb, ib, jb, kb, lb, mb = function() {
                    return {
                        x: 0,
                        y: 0
                    }
                },
                nb = mb(),
                ob = mb(),
                pb = mb(),
                qb = {},
                rb = 0,
                sb = mb(),
                tb = 0,
                ub = !0,
                vb = [],
                wb = {},
                xb = function(a, b) {
                    e.extend(f, b.publicMethods), vb.push(a)
                },
                yb = function(a) {
                    var b = $c();
                    return a > b - 1 ? a - b : 0 > a ? b + a : a
                },
                zb = {},
                Ab = function(a, b) {
                    return zb[a] || (zb[a] = []), zb[a].push(b)
                },
                Bb = function(a) {
                    var b = zb[a];
                    if (b) {
                        var c = Array.prototype.slice.call(arguments);
                        c.shift();
                        for (var d = 0; d < b.length; d++) b[d].apply(f, c)
                    }
                },
                Cb = function() {
                    return (new Date).getTime()
                },
                Db = function(a) {
                    jb = a, f.bg.style.opacity = a * i.bgOpacity
                },
                Eb = function(a, b, c, d) {
                    a[F] = u + b + "px, " + c + "px" + v + " scale(" + d + ")"
                },
                Fb = function() {
                    eb && Eb(eb, pb.x, pb.y, s)
                },
                Gb = function(a) {
                    a.container && Eb(a.container.style, a.initialPosition.x, a.initialPosition.y, a.initialZoomLevel)
                },
                Hb = function(a, b) {
                    b[F] = u + a + "px, 0px" + v
                },
                Ib = function(a, b) {
                    if (!i.loop && b) {
                        var c = m + (sb.x * rb - a) / sb.x,
                            d = Math.round(a - rc.x);
                        (0 > c && d > 0 || c >= $c() - 1 && 0 > d) && (a = rc.x + d * i.mainScrollEndFriction)
                    }
                    rc.x = a, Hb(a, n)
                },
                Jb = function(a, b) {
                    var c = sc[a] - y[a];
                    return ob[a] + nb[a] + c - c * (b / t)
                },
                Kb = function(a, b) {
                    a.x = b.x, a.y = b.y, b.id && (a.id = b.id)
                },
                Lb = function(a) {
                    a.x = Math.round(a.x), a.y = Math.round(a.y)
                },
                Mb = null,
                Nb = function() {
                    Mb && (e.unbind(document, "mousemove", Nb), e.addClass(a, "pswp--has_mouse"), i.mouseUsed = !0, Bb("mouseUsed")), Mb = setTimeout(function() {
                        Mb = null
                    }, 100)
                },
                Ob = function() {
                    e.bind(document, "keydown", f), O.transform && e.bind(f.scrollWrap, "click", f), i.mouseUsed || e.bind(document, "mousemove", Nb), e.bind(window, "resize scroll", f), Bb("bindEvents")
                },
                Pb = function() {
                    e.unbind(window, "resize", f), e.unbind(window, "scroll", r.scroll), e.unbind(document, "keydown", f), e.unbind(document, "mousemove", Nb), O.transform && e.unbind(f.scrollWrap, "click", f), V && e.unbind(window, p, f), Bb("unbindEvents")
                },
                Qb = function(a, b) {
                    var c = gd(f.currItem, qb, a);
                    return b && (db = c), c
                },
                Rb = function(a) {
                    return a || (a = f.currItem), a.initialZoomLevel
                },
                Sb = function(a) {
                    return a || (a = f.currItem), a.w > 0 ? i.maxSpreadZoom : 1
                },
                Tb = function(a, b, c, d) {
                    return d === f.currItem.initialZoomLevel ? (c[a] = f.currItem.initialPosition[a], !0) : (c[a] = Jb(a, d), c[a] > b.min[a] ? (c[a] = b.min[a], !0) : c[a] < b.max[a] ? (c[a] = b.max[a], !0) : !1)
                },
                Ub = function() {
                    if (F) {
                        var b = O.perspective && !H;
                        return u = "translate" + (b ? "3d(" : "("), void(v = O.perspective ? ", 0px)" : ")")
                    }
                    F = "left", e.addClass(a, "pswp--ie"), Hb = function(a, b) {
                        b.left = a + "px"
                    }, Gb = function(a) {
                        var b = a.fitRatio > 1 ? 1 : a.fitRatio,
                            c = a.container.style,
                            d = b * a.w,
                            e = b * a.h;
                        c.width = d + "px", c.height = e + "px", c.left = a.initialPosition.x + "px", c.top = a.initialPosition.y + "px"
                    }, Fb = function() {
                        if (eb) {
                            var a = eb,
                                b = f.currItem,
                                c = b.fitRatio > 1 ? 1 : b.fitRatio,
                                d = c * b.w,
                                e = c * b.h;
                            a.width = d + "px", a.height = e + "px", a.left = pb.x + "px", a.top = pb.y + "px"
                        }
                    }
                },
                Vb = function(a) {
                    var b = "";
                    i.escKey && 27 === a.keyCode ? b = "close" : i.arrowKeys && (37 === a.keyCode ? b = "prev" : 39 === a.keyCode && (b = "next")), b && (a.ctrlKey || a.altKey || a.shiftKey || a.metaKey || (a.preventDefault ? a.preventDefault() : a.returnValue = !1, f[b]()))
                },
                Wb = function(a) {
                    a && (Y || X || fb || T) && (a.preventDefault(), a.stopPropagation())
                },
                Xb = function() {
                    f.setScrollOffset(0, e.getScrollY())
                },
                Yb = {},
                Zb = 0,
                $b = function(a) {
                    Yb[a] && (Yb[a].raf && J(Yb[a].raf), Zb--, delete Yb[a])
                },
                _b = function(a) {
                    Yb[a] && $b(a), Yb[a] || (Zb++, Yb[a] = {})
                },
                ac = function() {
                    for (var a in Yb) Yb.hasOwnProperty(a) && $b(a)
                },
                bc = function(a, b, c, d, e, f, g) {
                    var h, i = Cb();
                    _b(a);
                    var j = function() {
                        if (Yb[a]) {
                            if (h = Cb() - i, h >= d) return $b(a), f(c), void(g && g());
                            f((c - b) * e(h / d) + b), Yb[a].raf = I(j)
                        }
                    };
                    j()
                },
                cc = {
                    shout: Bb,
                    listen: Ab,
                    viewportSize: qb,
                    options: i,
                    isMainScrollAnimating: function() {
                        return fb
                    },
                    getZoomLevel: function() {
                        return s
                    },
                    getCurrentIndex: function() {
                        return m
                    },
                    isDragging: function() {
                        return V
                    },
                    isZooming: function() {
                        return ab
                    },
                    setScrollOffset: function(a, b) {
                        y.x = a, N = y.y = b
                    },
                    applyZoomPan: function(a, b, c) {
                        pb.x = b, pb.y = c, s = a, Fb()
                    },
                    init: function() {
                        if (!j && !k) {
                            var c;
                            f.framework = e, f.template = a, f.bg = e.getChildByClass(a, "pswp__bg"), K = a.className, j = !0, O = e.detectFeatures(), I = O.raf, J = O.caf, F = O.transform, M = O.oldIE, f.scrollWrap = e.getChildByClass(a, "pswp__scroll-wrap"), f.container = e.getChildByClass(f.scrollWrap, "pswp__container"), n = f.container.style, f.itemHolders = z = [{
                                el: f.container.children[0],
                                wrap: 0,
                                index: -1
                            }, {
                                el: f.container.children[1],
                                wrap: 0,
                                index: -1
                            }, {
                                el: f.container.children[2],
                                wrap: 0,
                                index: -1
                            }], z[0].el.style.display = z[2].el.style.display = "none", Ub(), r = {
                                resize: f.updateSize,
                                scroll: Xb,
                                keydown: Vb,
                                click: Wb
                            };
                            var d = O.isOldIOSPhone || O.isOldAndroid || O.isMobileOpera;
                            for (O.animationName && O.transform && !d || (i.showAnimationDuration = i.hideAnimationDuration = 0), c = 0; c < vb.length; c++) f["init" + vb[c]]();
                            if (b) {
                                var g = f.ui = new b(f, e);
                                g.init()
                            }
                            Bb("firstUpdate"), m = m || i.index || 0, (isNaN(m) || 0 > m || m >= $c()) && (m = 0), f.currItem = Zc(m), (O.isOldIOSPhone || O.isOldAndroid) && (ub = !1), i.modal && (a.setAttribute("aria-hidden", "false"), ub ? a.style.position = "fixed" : (a.style.position = "absolute", a.style.top = e.getScrollY() + "px")), void 0 === N && (Bb("initialLayout"), N = L = e.getScrollY());
                            var l = "pswp--open ";
                            for (i.mainClass && (l += i.mainClass + " "), i.showHideOpacity && (l += "pswp--animate_opacity "), l += H ? "pswp--touch" : "pswp--notouch", l += O.animationName ? " pswp--css_animation" : "", l += O.svg ? " pswp--svg" : "", e.addClass(a, l), f.updateSize(), o = -1, tb = null, c = 0; h > c; c++) Hb((c + o) * sb.x, z[c].el.style);
                            M || e.bind(f.scrollWrap, q, f), Ab("initialZoomInEnd", function() {
                                f.setContent(z[0], m - 1), f.setContent(z[2], m + 1), z[0].el.style.display = z[2].el.style.display = "block", i.focus && a.focus(), Ob()
                            }), f.setContent(z[1], m), f.updateCurrItem(), Bb("afterInit"), ub || (w = setInterval(function() {
                                Zb || V || ab || s !== f.currItem.initialZoomLevel || f.updateSize()
                            }, 1e3)), e.addClass(a, "pswp--visible")
                        }
                    },
                    close: function() {
                        j && (j = !1, k = !0, Bb("close"), Pb(), ad(f.currItem, null, !0, f.destroy))
                    },
                    destroy: function() {
                        Bb("destroy"), Vc && clearTimeout(Vc), i.modal && (a.setAttribute("aria-hidden", "true"), a.className = K), w && clearInterval(w), e.unbind(f.scrollWrap, q, f), e.unbind(window, "scroll", f), xc(), ac(), zb = null
                    },
                    panTo: function(a, b, c) {
                        c || (a > db.min.x ? a = db.min.x : a < db.max.x && (a = db.max.x), b > db.min.y ? b = db.min.y : b < db.max.y && (b = db.max.y)), pb.x = a, pb.y = b, Fb()
                    },
                    handleEvent: function(a) {
                        a = a || window.event, r[a.type] && r[a.type](a)
                    },
                    goTo: function(a) {
                        a = yb(a);
                        var b = a - m;
                        tb = b, m = a, f.currItem = Zc(m), rb -= b, Ib(sb.x * rb), ac(), fb = !1, f.updateCurrItem()
                    },
                    next: function() {
                        f.goTo(m + 1)
                    },
                    prev: function() {
                        f.goTo(m - 1)
                    },
                    updateCurrZoomItem: function(a) {
                        if (a && Bb("beforeChange", 0), z[1].el.children.length) {
                            var b = z[1].el.children[0];
                            eb = e.hasClass(b, "pswp__zoom-wrap") ? b.style : null
                        } else eb = null;
                        db = f.currItem.bounds, t = s = f.currItem.initialZoomLevel, pb.x = db.center.x, pb.y = db.center.y, a && Bb("afterChange")
                    },
                    invalidateCurrItems: function() {
                        x = !0;
                        for (var a = 0; h > a; a++) z[a].item && (z[a].item.needsUpdate = !0)
                    },
                    updateCurrItem: function(a) {
                        if (0 !== tb) {
                            var b, c = Math.abs(tb);
                            if (!(a && 2 > c)) {
                                f.currItem = Zc(m), Bb("beforeChange", tb), c >= h && (o += tb + (tb > 0 ? -h : h), c = h);
                                for (var d = 0; c > d; d++) tb > 0 ? (b = z.shift(), z[h - 1] = b, o++, Hb((o + 2) * sb.x, b.el.style), f.setContent(b, m - c + d + 1 + 1)) : (b = z.pop(), z.unshift(b), o--, Hb(o * sb.x, b.el.style), f.setContent(b, m + c - d - 1 - 1));
                                if (eb && 1 === Math.abs(tb)) {
                                    var e = Zc(A);
                                    e.initialZoomLevel !== s && (gd(e, qb), Gb(e))
                                }
                                tb = 0, f.updateCurrZoomItem(), A = m, Bb("afterChange")
                            }
                        }
                    },
                    updateSize: function(b) {
                        if (!ub) {
                            var c = e.getScrollY();
                            if (N !== c && (a.style.top = c + "px", N = c), !b && wb.x === window.innerWidth && wb.y === window.innerHeight) return;
                            wb.x = window.innerWidth, wb.y = window.innerHeight, a.style.height = wb.y + "px"
                        }
                        if (qb.x = f.scrollWrap.clientWidth, qb.y = f.scrollWrap.clientHeight, y = {
                                x: 0,
                                y: N
                            }, sb.x = qb.x + Math.round(qb.x * i.spacing), sb.y = qb.y, Ib(sb.x * rb), Bb("beforeResize"), void 0 !== o) {
                            for (var d, g, j, k = 0; h > k; k++) d = z[k], Hb((k + o) * sb.x, d.el.style), j = m + k - 1, i.loop && $c() > 2 && (j = yb(j)), g = Zc(j), g && (x || g.needsUpdate || !g.bounds) ? (f.cleanSlide(g), f.setContent(d, j), 1 === k && (f.currItem = g, f.updateCurrZoomItem(!0)), g.needsUpdate = !1) : -1 === d.index && j >= 0 && f.setContent(d, j), g && g.container && (gd(g, qb), Gb(g));
                            x = !1
                        }
                        t = s = f.currItem.initialZoomLevel, db = f.currItem.bounds, db && (pb.x = db.center.x, pb.y = db.center.y, Fb()), Bb("resize")
                    },
                    zoomTo: function(a, b, c, d, f) {
                        b && (t = s, sc.x = Math.abs(b.x) - pb.x, sc.y = Math.abs(b.y) - pb.y, Kb(ob, pb));
                        var g = Qb(a, !1),
                            h = {};
                        Tb("x", g, h, a), Tb("y", g, h, a);
                        var i = s,
                            j = {
                                x: pb.x,
                                y: pb.y
                            };
                        Lb(h);
                        var k = function(b) {
                            1 === b ? (s = a, pb.x = h.x, pb.y = h.y) : (s = (a - i) * b + i, pb.x = (h.x - j.x) * b + j.x, pb.y = (h.y - j.y) * b + j.y), f && f(b), Fb()
                        };
                        c ? bc("customZoomTo", 0, 1, c, d || e.easing.sine.inOut, k) : k(1)
                    }
                },
                dc = 30,
                ec = 10,
                fc = {},
                gc = {},
                hc = {},
                ic = {},
                jc = {},
                kc = [],
                lc = {},
                mc = [],
                nc = {},
                oc = 0,
                pc = mb(),
                qc = 0,
                rc = mb(),
                sc = mb(),
                tc = mb(),
                uc = function(a, b) {
                    return a.x === b.x && a.y === b.y
                },
                vc = function(a, b) {
                    return Math.abs(a.x - b.x) < g && Math.abs(a.y - b.y) < g
                },
                wc = function(a, b) {
                    return nc.x = Math.abs(a.x - b.x), nc.y = Math.abs(a.y - b.y), Math.sqrt(nc.x * nc.x + nc.y * nc.y)
                },
                xc = function() {
                    Z && (J(Z), Z = null)
                },
                yc = function() {
                    V && (Z = I(yc), Oc())
                },
                zc = function() {
                    return !("fit" === i.scaleMode && s === f.currItem.initialZoomLevel)
                },
                Ac = function(a, b) {
                    return a ? a.className && a.className.indexOf("pswp__scroll-wrap") > -1 ? !1 : b(a) ? a : Ac(a.parentNode, b) : !1
                },
                Bc = {},
                Cc = function(a, b) {
                    return Bc.prevent = !Ac(a.target, i.isClickableElement), Bb("preventDragEvent", a, b, Bc), Bc.prevent
                },
                Dc = function(a, b) {
                    return b.x = a.pageX, b.y = a.pageY, b.id = a.identifier, b
                },
                Ec = function(a, b, c) {
                    c.x = .5 * (a.x + b.x), c.y = .5 * (a.y + b.y)
                },
                Fc = function(a, b, c) {
                    if (a - Q > 50) {
                        var d = mc.length > 2 ? mc.shift() : {};
                        d.x = b, d.y = c, mc.push(d), Q = a
                    }
                },
                Gc = function() {
                    var a = pb.y - f.currItem.initialPosition.y;
                    return 1 - Math.abs(a / (qb.y / 2))
                },
                Hc = {},
                Ic = {},
                Jc = [],
                Kc = function(a) {
                    for (; Jc.length > 0;) Jc.pop();
                    return G ? (lb = 0, kc.forEach(function(a) {
                        0 === lb ? Jc[0] = a : 1 === lb && (Jc[1] = a), lb++
                    })) : a.type.indexOf("touch") > -1 ? a.touches && a.touches.length > 0 && (Jc[0] = Dc(a.touches[0], Hc), a.touches.length > 1 && (Jc[1] = Dc(a.touches[1], Ic))) : (Hc.x = a.pageX, Hc.y = a.pageY, Hc.id = "", Jc[0] = Hc), Jc
                },
                Lc = function(a, b) {
                    var c, d, e, g, h = 0,
                        j = pb[a] + b[a],
                        k = b[a] > 0,
                        l = rc.x + b.x,
                        m = rc.x - lc.x;
                    return c = j > db.min[a] || j < db.max[a] ? i.panEndFriction : 1, j = pb[a] + b[a] * c, !i.allowPanToNext && s !== f.currItem.initialZoomLevel || (eb ? "h" !== gb || "x" !== a || X || (k ? (j > db.min[a] && (c = i.panEndFriction, h = db.min[a] - j, d = db.min[a] - ob[a]), (0 >= d || 0 > m) && $c() > 1 ? (g = l, 0 > m && l > lc.x && (g = lc.x)) : db.min.x !== db.max.x && (e = j)) : (j < db.max[a] && (c = i.panEndFriction, h = j - db.max[a], d = ob[a] - db.max[a]), (0 >= d || m > 0) && $c() > 1 ? (g = l, m > 0 && l < lc.x && (g = lc.x)) : db.min.x !== db.max.x && (e = j))) : g = l, "x" !== a) ? void(fb || $ || s > f.currItem.fitRatio && (pb[a] += b[a] * c)) : (void 0 !== g && (Ib(g, !0), $ = g === lc.x ? !1 : !0), db.min.x !== db.max.x && (void 0 !== e ? pb.x = e : $ || (pb.x += b.x * c)), void 0 !== g)
                },
                Mc = function(a) {
                    if (!("mousedown" === a.type && a.button > 0)) {
                        if (Yc) return void a.preventDefault();
                        if (!U || "mousedown" !== a.type) {
                            if (Cc(a, !0) && a.preventDefault(), Bb("pointerDown"), G) {
                                var b = e.arraySearch(kc, a.pointerId, "id");
                                0 > b && (b = kc.length), kc[b] = {
                                    x: a.pageX,
                                    y: a.pageY,
                                    id: a.pointerId
                                }
                            }
                            var c = Kc(a),
                                d = c.length;
                            _ = null, ac(), V && 1 !== d || (V = hb = !0, e.bind(window, p, f), S = kb = ib = T = $ = Y = W = X = !1, gb = null, Bb("firstTouchStart", c), Kb(ob, pb), nb.x = nb.y = 0, Kb(ic, c[0]), Kb(jc, ic), lc.x = sb.x * rb, mc = [{
                                x: ic.x,
                                y: ic.y
                            }], Q = P = Cb(), Qb(s, !0), xc(), yc()), !ab && d > 1 && !fb && !$ && (t = s, X = !1, ab = W = !0, nb.y = nb.x = 0, Kb(ob, pb), Kb(fc, c[0]), Kb(gc, c[1]), Ec(fc, gc, tc), sc.x = Math.abs(tc.x) - pb.x, sc.y = Math.abs(tc.y) - pb.y, bb = cb = wc(fc, gc))
                        }
                    }
                },
                Nc = function(a) {
                    if (a.preventDefault(), G) {
                        var b = e.arraySearch(kc, a.pointerId, "id");
                        if (b > -1) {
                            var c = kc[b];
                            c.x = a.pageX, c.y = a.pageY
                        }
                    }
                    if (V) {
                        var d = Kc(a);
                        if (gb || Y || ab) _ = d;
                        else {
                            var f = Math.abs(d[0].x - ic.x) - Math.abs(d[0].y - ic.y);
                            Math.abs(f) >= ec && (gb = f > 0 ? "h" : "v", _ = d)
                        }
                    }
                },
                Oc = function() {
                    if (_) {
                        var a = _.length;
                        if (0 !== a)
                            if (Kb(fc, _[0]), hc.x = fc.x - ic.x, hc.y = fc.y - ic.y, ab && a > 1) {
                                if (ic.x = fc.x, ic.y = fc.y, !hc.x && !hc.y && uc(_[1], gc)) return;
                                Kb(gc, _[1]), X || (X = !0, Bb("zoomGestureStarted"));
                                var b = wc(fc, gc),
                                    c = Tc(b);
                                c > f.currItem.initialZoomLevel + f.currItem.initialZoomLevel / 15 && (kb = !0);
                                var d = 1,
                                    e = Rb(),
                                    g = Sb();
                                if (e > c)
                                    if (i.pinchToClose && !kb && t <= f.currItem.initialZoomLevel) {
                                        var h = e - c,
                                            j = 1 - h / (e / 1.2);
                                        Db(j), Bb("onPinchClose", j), ib = !0
                                    } else d = (e - c) / e, d > 1 && (d = 1), c = e - d * (e / 3);
                                else c > g && (d = (c - g) / (6 * e), d > 1 && (d = 1), c = g + d * e);
                                0 > d && (d = 0), bb = b, Ec(fc, gc, pc), nb.x += pc.x - tc.x, nb.y += pc.y - tc.y, Kb(tc, pc), pb.x = Jb("x", c), pb.y = Jb("y", c), S = c > s, s = c, Fb()
                            } else {
                                if (!gb) return;
                                if (hb && (hb = !1, Math.abs(hc.x) >= ec && (hc.x -= _[0].x - jc.x), Math.abs(hc.y) >= ec && (hc.y -= _[0].y - jc.y)), ic.x = fc.x, ic.y = fc.y, 0 === hc.x && 0 === hc.y) return;
                                if ("v" === gb && i.closeOnVerticalDrag && !zc()) {
                                    nb.y += hc.y, pb.y += hc.y;
                                    var k = Gc();
                                    return T = !0, Bb("onVerticalDrag", k), Db(k), void Fb()
                                }
                                Fc(Cb(), fc.x, fc.y), Y = !0, db = f.currItem.bounds;
                                var l = Lc("x", hc);
                                l || (Lc("y", hc), Lb(pb), Fb())
                            }
                    }
                },
                Pc = function(a) {
                    if (O.isOldAndroid) {
                        if (U && "mouseup" === a.type) return;
                        a.type.indexOf("touch") > -1 && (clearTimeout(U), U = setTimeout(function() {
                            U = 0
                        }, 600))
                    }
                    Bb("pointerUp"), Cc(a, !1) && a.preventDefault();
                    var b;
                    if (G) {
                        var c = e.arraySearch(kc, a.pointerId, "id");
                        if (c > -1)
                            if (b = kc.splice(c, 1)[0], navigator.pointerEnabled) b.type = a.pointerType || "mouse";
                            else {
                                var d = {
                                    4: "mouse",
                                    2: "touch",
                                    3: "pen"
                                };
                                b.type = d[a.pointerType], b.type || (b.type = a.pointerType || "mouse")
                            }
                    }
                    var g, h = Kc(a),
                        i = h.length;
                    if ("mouseup" === a.type && (i = 0), 2 === i) return _ = null, !0;
                    1 === i && Kb(jc, h[0]), 0 !== i || gb || fb || (b || ("mouseup" === a.type ? b = {
                        x: a.pageX,
                        y: a.pageY,
                        type: "mouse"
                    } : a.changedTouches && a.changedTouches[0] && (b = {
                        x: a.changedTouches[0].pageX,
                        y: a.changedTouches[0].pageY,
                        type: "touch"
                    })), Bb("touchRelease", a, b));
                    var j = -1;
                    if (0 === i && (V = !1, e.unbind(window, p, f), xc(), ab ? j = 0 : -1 !== qc && (j = Cb() - qc)), qc = 1 === i ? Cb() : -1, g = -1 !== j && 150 > j ? "zoom" : "swipe", ab && 2 > i && (ab = !1, 1 === i && (g = "zoomPointerUp"), Bb("zoomGestureEnded")), _ = null, Y || X || fb || T)
                        if (ac(), R || (R = Qc()), R.calculateSwipeSpeed("x"), T) {
                            var k = Gc();
                            if (.6 > k) f.close();
                            else {
                                var l = pb.y,
                                    m = jb;
                                bc("verticalDrag", 0, 1, 300, e.easing.cubic.out, function(a) {
                                    pb.y = (f.currItem.initialPosition.y - l) * a + l, Db((1 - m) * a + m), Fb()
                                }), Bb("onVerticalDrag", 1)
                            }
                        } else {
                            if (($ || fb) && 0 === i) {
                                var n = Sc(g, R);
                                if (n) return;
                                g = "zoomPointerUp"
                            }
                            if (!fb) return "swipe" !== g ? void Uc() : void(!$ && s > f.currItem.fitRatio && Rc(R))
                        }
                },
                Qc = function() {
                    var a, b, c = {
                        lastFlickOffset: {},
                        lastFlickDist: {},
                        lastFlickSpeed: {},
                        slowDownRatio: {},
                        slowDownRatioReverse: {},
                        speedDecelerationRatio: {},
                        speedDecelerationRatioAbs: {},
                        distanceOffset: {},
                        backAnimDestination: {},
                        backAnimStarted: {},
                        calculateSwipeSpeed: function(d) {
                            mc.length > 1 ? (a = Cb() - Q + 50, b = mc[mc.length - 2][d]) : (a = Cb() - P, b = jc[d]), c.lastFlickOffset[d] = ic[d] - b, c.lastFlickDist[d] = Math.abs(c.lastFlickOffset[d]), c.lastFlickSpeed[d] = c.lastFlickDist[d] > 20 ? c.lastFlickOffset[d] / a : 0, Math.abs(c.lastFlickSpeed[d]) < .1 && (c.lastFlickSpeed[d] = 0), c.slowDownRatio[d] = .95, c.slowDownRatioReverse[d] = 1 - c.slowDownRatio[d], c.speedDecelerationRatio[d] = 1
                        },
                        calculateOverBoundsAnimOffset: function(a, b) {
                            c.backAnimStarted[a] || (pb[a] > db.min[a] ? c.backAnimDestination[a] = db.min[a] : pb[a] < db.max[a] && (c.backAnimDestination[a] = db.max[a]), void 0 !== c.backAnimDestination[a] && (c.slowDownRatio[a] = .7, c.slowDownRatioReverse[a] = 1 - c.slowDownRatio[a], c.speedDecelerationRatioAbs[a] < .05 && (c.lastFlickSpeed[a] = 0, c.backAnimStarted[a] = !0, bc("bounceZoomPan" + a, pb[a], c.backAnimDestination[a], b || 300, e.easing.sine.out, function(b) {
                                pb[a] = b, Fb()
                            }))))
                        },
                        calculateAnimOffset: function(a) {
                            c.backAnimStarted[a] || (c.speedDecelerationRatio[a] = c.speedDecelerationRatio[a] * (c.slowDownRatio[a] + c.slowDownRatioReverse[a] - c.slowDownRatioReverse[a] * c.timeDiff / 10), c.speedDecelerationRatioAbs[a] = Math.abs(c.lastFlickSpeed[a] * c.speedDecelerationRatio[a]), c.distanceOffset[a] = c.lastFlickSpeed[a] * c.speedDecelerationRatio[a] * c.timeDiff, pb[a] += c.distanceOffset[a])
                        },
                        panAnimLoop: function() {
                            return Yb.zoomPan && (Yb.zoomPan.raf = I(c.panAnimLoop), c.now = Cb(), c.timeDiff = c.now - c.lastNow, c.lastNow = c.now, c.calculateAnimOffset("x"), c.calculateAnimOffset("y"), Fb(), c.calculateOverBoundsAnimOffset("x"), c.calculateOverBoundsAnimOffset("y"), c.speedDecelerationRatioAbs.x < .05 && c.speedDecelerationRatioAbs.y < .05) ? (pb.x = Math.round(pb.x), pb.y = Math.round(pb.y), Fb(), void $b("zoomPan")) : void 0
                        }
                    };
                    return c
                },
                Rc = function(a) {
                    return a.calculateSwipeSpeed("y"), db = f.currItem.bounds, a.backAnimDestination = {}, a.backAnimStarted = {}, Math.abs(a.lastFlickSpeed.x) <= .05 && Math.abs(a.lastFlickSpeed.y) <= .05 ? (a.speedDecelerationRatioAbs.x = a.speedDecelerationRatioAbs.y = 0, a.calculateOverBoundsAnimOffset("x"), a.calculateOverBoundsAnimOffset("y"), !0) : (_b("zoomPan"), a.lastNow = Cb(), void a.panAnimLoop())
                },
                Sc = function(a, b) {
                    var c;
                    fb || (oc = m);
                    var d;
                    if ("swipe" === a) {
                        var g = ic.x - jc.x,
                            h = b.lastFlickDist.x < 10;
                        g > dc && (h || b.lastFlickOffset.x > 20) ? d = -1 : -dc > g && (h || b.lastFlickOffset.x < -20) && (d = 1)
                    }
                    var j;
                    d && (m += d, 0 > m ? (m = i.loop ? $c() - 1 : 0, j = !0) : m >= $c() && (m = i.loop ? 0 : $c() - 1, j = !0), (!j || i.loop) && (tb += d, rb -= d, c = !0));
                    var k, l = sb.x * rb,
                        n = Math.abs(l - rc.x);
                    return c || l > rc.x == b.lastFlickSpeed.x > 0 ? (k = Math.abs(b.lastFlickSpeed.x) > 0 ? n / Math.abs(b.lastFlickSpeed.x) : 333, k = Math.min(k, 400), k = Math.max(k, 250)) : k = 333, oc === m && (c = !1), fb = !0, Bb("mainScrollAnimStart"), bc("mainScroll", rc.x, l, k, e.easing.cubic.out, Ib, function() {
                        ac(), fb = !1, oc = -1, (c || oc !== m) && f.updateCurrItem(), Bb("mainScrollAnimComplete")
                    }), c && f.updateCurrItem(!0), c
                },
                Tc = function(a) {
                    return 1 / cb * a * t
                },
                Uc = function() {
                    var a = s,
                        b = Rb(),
                        c = Sb();
                    b > s ? a = b : s > c && (a = c);
                    var d, g = 1,
                        h = jb;
                    return ib && !S && !kb && b > s ? (f.close(), !0) : (ib && (d = function(a) {
                        Db((g - h) * a + h)
                    }), f.zoomTo(a, 0, 300, e.easing.cubic.out, d), !0)
                };
            xb("Gestures", {
                publicMethods: {
                    initGestures: function() {
                        var a = function(a, b, c, d, e) {
                            B = a + b, C = a + c, D = a + d, E = e ? a + e : ""
                        };
                        G = O.pointerEvent, G && O.touch && (O.touch = !1), G ? navigator.pointerEnabled ? a("pointer", "down", "move", "up", "cancel") : a("MSPointer", "Down", "Move", "Up", "Cancel") : O.touch ? (a("touch", "start", "move", "end", "cancel"), H = !0) : a("mouse", "down", "move", "up"), p = C + " " + D + " " + E, q = B, G && !H && (H = navigator.maxTouchPoints > 1 || navigator.msMaxTouchPoints > 1), f.likelyTouchDevice = H, r[B] = Mc, r[C] = Nc, r[D] = Pc, E && (r[E] = r[D]), O.touch && (q += " mousedown", p += " mousemove mouseup", r.mousedown = r[B], r.mousemove = r[C], r.mouseup = r[D]), H || (i.allowPanToNext = !1)
                    }
                }
            });
            var Vc, Wc, Xc, Yc, Zc, $c, _c, ad = function(b, c, d, g) {
                    Vc && clearTimeout(Vc), Yc = !0, Xc = !0;
                    var h;
                    b.initialLayout ? (h = b.initialLayout, b.initialLayout = null) : h = i.getThumbBoundsFn && i.getThumbBoundsFn(m);
                    var j = d ? i.hideAnimationDuration : i.showAnimationDuration,
                        k = function() {
                            $b("initialZoom"), d ? (f.template.removeAttribute("style"), f.bg.removeAttribute("style")) : (Db(1), c && (c.style.display = "block"), e.addClass(a, "pswp--animated-in"), Bb("initialZoom" + (d ? "OutEnd" : "InEnd"))), g && g(), Yc = !1
                        };
                    if (!j || !h || void 0 === h.x) {
                        var n = function() {
                            Bb("initialZoom" + (d ? "Out" : "In")), s = b.initialZoomLevel, Kb(pb, b.initialPosition), Fb(), a.style.opacity = d ? 0 : 1, Db(1), k()
                        };
                        return void n()
                    }
                    var o = function() {
                        var c = l,
                            g = !f.currItem.src || f.currItem.loadError || i.showHideOpacity;
                        b.miniImg && (b.miniImg.style.webkitBackfaceVisibility = "hidden"), d || (s = h.w / b.w, pb.x = h.x, pb.y = h.y - L, f[g ? "template" : "bg"].style.opacity = .001, Fb()), _b("initialZoom"), d && !c && e.removeClass(a, "pswp--animated-in"), g && (d ? e[(c ? "remove" : "add") + "Class"](a, "pswp--animate_opacity") : setTimeout(function() {
                            e.addClass(a, "pswp--animate_opacity")
                        }, 30)), Vc = setTimeout(function() {
                            if (Bb("initialZoom" + (d ? "Out" : "In")), d) {
                                var f = h.w / b.w,
                                    i = {
                                        x: pb.x,
                                        y: pb.y
                                    },
                                    l = s,
                                    m = jb,
                                    n = function(b) {
                                        1 === b ? (s = f, pb.x = h.x, pb.y = h.y - N) : (s = (f - l) * b + l, pb.x = (h.x - i.x) * b + i.x, pb.y = (h.y - N - i.y) * b + i.y), Fb(), g ? a.style.opacity = 1 - b : Db(m - b * m)
                                    };
                                c ? bc("initialZoom", 0, 1, j, e.easing.cubic.out, n, k) : (n(1), Vc = setTimeout(k, j + 20))
                            } else s = b.initialZoomLevel, Kb(pb, b.initialPosition), Fb(), Db(1), g ? a.style.opacity = 1 : Db(1), Vc = setTimeout(k, j + 20)
                        }, d ? 25 : 90)
                    };
                    o()
                },
                bd = {},
                cd = [],
                dd = {
                    index: 0,
                    errorMsg: '<div class="pswp__error-msg"><a href="%url%" target="_blank">The image</a> could not be loaded.</div>',
                    forceProgressiveLoading: !1,
                    preload: [1, 1],
                    getNumItemsFn: function() {
                        return Wc.length
                    }
                },
                ed = function() {
                    return {
                        center: {
                            x: 0,
                            y: 0
                        },
                        max: {
                            x: 0,
                            y: 0
                        },
                        min: {
                            x: 0,
                            y: 0
                        }
                    }
                },
                fd = function(a, b, c) {
                    var d = a.bounds;
                    d.center.x = Math.round((bd.x - b) / 2), d.center.y = Math.round((bd.y - c) / 2) + a.vGap.top, d.max.x = b > bd.x ? Math.round(bd.x - b) : d.center.x, d.max.y = c > bd.y ? Math.round(bd.y - c) + a.vGap.top : d.center.y, d.min.x = b > bd.x ? 0 : d.center.x, d.min.y = c > bd.y ? a.vGap.top : d.center.y
                },
                gd = function(a, b, c) {
                    if (a.src && !a.loadError) {
                        var d = !c;
                        if (d && (a.vGap || (a.vGap = {
                                top: 0,
                                bottom: 0
                            }), Bb("parseVerticalMargin", a)), bd.x = b.x, bd.y = b.y - a.vGap.top - a.vGap.bottom, d) {
                            var e = bd.x / a.w,
                                f = bd.y / a.h;
                            a.fitRatio = f > e ? e : f;
                            var g = i.scaleMode;
                            "orig" === g ? c = 1 : "fit" === g && (c = a.fitRatio), c > 1 && (c = 1), a.initialZoomLevel = c, a.bounds || (a.bounds = ed())
                        }
                        if (!c) return;
                        return fd(a, a.w * c, a.h * c), d && c === a.initialZoomLevel && (a.initialPosition = a.bounds.center), a.bounds
                    }
                    return a.w = a.h = 0, a.initialZoomLevel = a.fitRatio = 1, a.bounds = ed(), a.initialPosition = a.bounds.center, a.bounds
                },
                hd = function(a, b, c, d, e, g) {
                    if (!b.loadError) {
                        var h, j = f.isDragging() && !f.isZooming(),
                            k = a === m || f.isMainScrollAnimating() || j;
                        !e && (H || i.alwaysFadeIn) && k && (h = !0), d && (h && (d.style.opacity = 0), b.imageAppended = !0, c.appendChild(d), h && setTimeout(function() {
                            d.style.opacity = 1, g && setTimeout(function() {
                                b && b.loaded && b.placeholder && (b.placeholder.style.display = "none", b.placeholder = null)
                            }, 500)
                        }, 50))
                    }
                },
                id = function(a) {
                    a.loading = !0, a.loaded = !1;
                    var b = a.img = e.createEl("pswp__img", "img"),
                        c = function() {
                            a.loading = !1, a.loaded = !0, a.loadComplete ? a.loadComplete(a) : a.img = null, b.onload = b.onerror = null, b = null
                        };
                    return b.onload = c, b.onerror = function() {
                        a.loadError = !0, c()
                    }, b.src = a.src, b
                },
                jd = function(a, b) {
                    return a.src && a.loadError && a.container ? (b && (a.container.innerHTML = ""), a.container.innerHTML = i.errorMsg.replace("%url%", a.src), !0) : void 0
                },
                kd = function() {
                    if (cd.length) {
                        for (var a, b = 0; b < cd.length; b++) a = cd[b], a.holder.index === a.index && hd(a.index, a.item, a.baseDiv, a.img);
                        cd = []
                    }
                };
            xb("Controller", {
                publicMethods: {
                    lazyLoadItem: function(a) {
                        a = yb(a);
                        var b = Zc(a);
                        b && b.src && !b.loaded && !b.loading && (Bb("gettingData", a, b), id(b))
                    },
                    initController: function() {
                        e.extend(i, dd, !0), f.items = Wc = c, Zc = f.getItemAt, $c = i.getNumItemsFn, _c = i.loop, $c() < 3 && (i.loop = !1), Ab("beforeChange", function(a) {
                            var b, c = i.preload,
                                d = null === a ? !0 : a > 0,
                                e = Math.min(c[0], $c()),
                                g = Math.min(c[1], $c());
                            for (b = 1;
                                (d ? g : e) >= b; b++) f.lazyLoadItem(m + b);
                            for (b = 1;
                                (d ? e : g) >= b; b++) f.lazyLoadItem(m - b)
                        }), Ab("initialLayout", function() {
                            f.currItem.initialLayout = i.getThumbBoundsFn && i.getThumbBoundsFn(m)
                        }), Ab("mainScrollAnimComplete", kd), Ab("initialZoomInEnd", kd), Ab("destroy", function() {
                            for (var a, b = 0; b < Wc.length; b++) a = Wc[b], a.container && (a.container = null), a.placeholder && (a.placeholder = null), a.img && (a.img = null), a.preloader && (a.preloader = null), a.loadError && (a.loaded = a.loadError = !1);
                            cd = null
                        })
                    },
                    getItemAt: function(a) {
                        return a >= 0 && void 0 !== Wc[a] ? Wc[a] : !1
                    },
                    allowProgressiveImg: function() {
                        return i.forceProgressiveLoading || !H || i.mouseUsed || screen.width > 1200
                    },
                    setContent: function(a, b) {
                        i.loop && (b = yb(b));
                        var c = f.getItemAt(a.index);
                        c && (c.container = null);
                        var d, g = f.getItemAt(b);
                        if (!g) return void(a.el.innerHTML = "");
                        Bb("gettingData", b, g), a.index = b, a.item = g;
                        var h = g.container = e.createEl("pswp__zoom-wrap");
                        if (!g.src && g.html && (g.html.tagName ? h.appendChild(g.html) : h.innerHTML = g.html), jd(g), !g.src || g.loadError || g.loaded) g.src && !g.loadError && (d = e.createEl("pswp__img", "img"), d.style.webkitBackfaceVisibility = "hidden", d.style.opacity = 1, d.src = g.src, hd(b, g, h, d, !0));
                        else {
                            if (g.loadComplete = function(c) {
                                    if (j) {
                                        if (c.img && (c.img.style.webkitBackfaceVisibility = "hidden"), a && a.index === b) {
                                            if (jd(c, !0)) return c.loadComplete = c.img = null, gd(c, qb), Gb(c), void(a.index === m && f.updateCurrZoomItem());
                                            c.imageAppended ? !Yc && c.placeholder && (c.placeholder.style.display = "none", c.placeholder = null) : O.transform && (fb || Yc) ? cd.push({
                                                item: c,
                                                baseDiv: h,
                                                img: c.img,
                                                index: b,
                                                holder: a
                                            }) : hd(b, c, h, c.img, fb || Yc)
                                        }
                                        c.loadComplete = null, c.img = null, Bb("imageLoadComplete", b, c)
                                    }
                                }, e.features.transform) {
                                var k = "pswp__img pswp__img--placeholder";
                                k += g.msrc ? "" : " pswp__img--placeholder--blank";
                                var l = e.createEl(k, g.msrc ? "img" : "");
                                g.msrc && (l.src = g.msrc), l.style.width = g.w + "px", l.style.height = g.h + "px", h.appendChild(l), g.placeholder = l
                            }
                            g.loading || id(g), f.allowProgressiveImg() && (!Xc && O.transform ? cd.push({
                                item: g,
                                baseDiv: h,
                                img: g.img,
                                index: b,
                                holder: a
                            }) : hd(b, g, h, g.img, !0, !0))
                        }
                        gd(g, qb), Xc || b !== m ? Gb(g) : (eb = h.style, ad(g, d || g.img)), a.el.innerHTML = "", a.el.appendChild(h)
                    },
                    cleanSlide: function(a) {
                        a.img && (a.img.onload = a.img.onerror = null), a.loaded = a.loading = a.img = a.imageAppended = !1
                    }
                }
            });
            var ld, md = {},
                nd = function(a, b, c) {
                    var d = document.createEvent("CustomEvent"),
                        e = {
                            origEvent: a,
                            target: a.target,
                            releasePoint: b,
                            pointerType: c || "touch"
                        };
                    d.initCustomEvent("pswpTap", !0, !0, e), a.target.dispatchEvent(d)
                };
            xb("Tap", {
                publicMethods: {
                    initTap: function() {
                        Ab("firstTouchStart", f.onTapStart), Ab("touchRelease", f.onTapRelease), Ab("destroy", function() {
                            md = {}, ld = null
                        })
                    },
                    onTapStart: function(a) {
                        a.length > 1 && (clearTimeout(ld), ld = null)
                    },
                    onTapRelease: function(a, b) {
                        if (b && !Y && !W && !Zb) {
                            var c = b;
                            if (ld && (clearTimeout(ld), ld = null, vc(c, md))) return void Bb("doubleTap", c);
                            if ("mouse" === b.type) return void nd(a, b, "mouse");
                            var d = a.target.tagName.toUpperCase();
                            if ("BUTTON" === d || e.hasClass(a.target, "pswp__single-tap")) return void nd(a, b);
                            Kb(md, c), ld = setTimeout(function() {
                                nd(a, b), ld = null
                            }, 300)
                        }
                    }
                }
            });
            var od;
            xb("DesktopZoom", {
                publicMethods: {
                    initDesktopZoom: function() {
                        M || (H ? Ab("mouseUsed", function() {
                            f.setupDesktopZoom()
                        }) : f.setupDesktopZoom(!0))
                    },
                    setupDesktopZoom: function(b) {
                        od = {};
                        var c = "wheel mousewheel DOMMouseScroll";
                        Ab("bindEvents", function() {
                            e.bind(a, c, f.handleMouseWheel)
                        }), Ab("unbindEvents", function() {
                            od && e.unbind(a, c, f.handleMouseWheel)
                        }), f.mouseZoomedIn = !1;
                        var d, g = function() {
                                f.mouseZoomedIn && (e.removeClass(a, "pswp--zoomed-in"), f.mouseZoomedIn = !1), 1 > s ? e.addClass(a, "pswp--zoom-allowed") : e.removeClass(a, "pswp--zoom-allowed"), h()
                            },
                            h = function() {
                                d && (e.removeClass(a, "pswp--dragging"), d = !1)
                            };
                        Ab("resize", g), Ab("afterChange", g), Ab("pointerDown", function() {
                            f.mouseZoomedIn && (d = !0, e.addClass(a, "pswp--dragging"))
                        }), Ab("pointerUp", h), b || g()
                    },
                    handleMouseWheel: function(a) {
                        if (s <= f.currItem.fitRatio) return i.closeOnScroll ? F && Math.abs(a.deltaY) > 2 && (l = !0, f.close()) : a.preventDefault(), !0;
                        if (a.preventDefault(), a.stopPropagation(), od.x = 0, "deltaX" in a) od.x = a.deltaX, od.y = a.deltaY;
                        else if ("wheelDelta" in a) a.wheelDeltaX && (od.x = -.16 * a.wheelDeltaX), od.y = a.wheelDeltaY ? -.16 * a.wheelDeltaY : -.16 * a.wheelDelta;
                        else {
                            if (!("detail" in a)) return;
                            od.y = a.detail
                        }
                        Qb(s, !0), f.panTo(pb.x - od.x, pb.y - od.y)
                    },
                    toggleDesktopZoom: function(b) {
                        b = b || {
                            x: qb.x / 2,
                            y: qb.y / 2 + N
                        };
                        var c = i.getDoubleTapZoom(!0, f.currItem),
                            d = s === c;
                        f.mouseZoomedIn = !d, f.zoomTo(d ? f.currItem.initialZoomLevel : c, b, 333), e[(d ? "remove" : "add") + "Class"](a, "pswp--zoomed-in")
                    }
                }
            });
            var pd, qd, rd, sd, td, ud, vd, wd, xd, yd, zd, Ad, Bd = {
                    history: !0,
                    galleryUID: 1
                },
                Cd = function() {
                    return zd.hash.substring(1)
                },
                Dd = function() {
                    pd && clearTimeout(pd), rd && clearTimeout(rd)
                },
                Ed = function() {
                    var a = Cd(),
                        b = {};
                    if (a.length < 5) return b;
                    for (var c = a.split("&"), d = 0; d < c.length; d++)
                        if (c[d]) {
                            var e = c[d].split("=");
                            e.length < 2 || (b[e[0]] = e[1])
                        }
                    return b.pid = parseInt(b.pid, 10) - 1, b.pid < 0 && (b.pid = 0), b
                },
                Fd = function() {
                    if (rd && clearTimeout(rd), Zb || V) return void(rd = setTimeout(Fd, 500));
                    sd ? clearTimeout(qd) : sd = !0;
                    var a = vd + "&gid=" + i.galleryUID + "&pid=" + (m + 1);
                    wd || -1 === zd.hash.indexOf(a) && (yd = !0);
                    var b = zd.href.split("#")[0] + "#" + a;
                    Ad ? "#" + a !== window.location.hash && history[wd ? "replaceState" : "pushState"]("", document.title, b) : wd ? zd.replace(b) : zd.hash = a, wd = !0, qd = setTimeout(function() {
                        sd = !1
                    }, 60)
                };
            xb("History", {
                publicMethods: {
                    initHistory: function() {
                        if (e.extend(i, Bd, !0), i.history) {
                            zd = window.location, yd = !1, xd = !1, wd = !1, vd = Cd(), Ad = "pushState" in history, vd.indexOf("gid=") > -1 && (vd = vd.split("&gid=")[0], vd = vd.split("?gid=")[0]), Ab("afterChange", f.updateURL), Ab("unbindEvents", function() {
                                e.unbind(window, "hashchange", f.onHashChange)
                            });
                            var a = function() {
                                ud = !0, xd || (yd ? history.back() : vd ? zd.hash = vd : Ad ? history.pushState("", document.title, zd.pathname + zd.search) : zd.hash = ""), Dd()
                            };
                            Ab("unbindEvents", function() {
                                l && a()
                            }), Ab("destroy", function() {
                                ud || a()
                            }), Ab("firstUpdate", function() {
                                m = Ed().pid
                            });
                            var b = vd.indexOf("pid=");
                            b > -1 && (vd = vd.substring(0, b), "&" === vd.slice(-1) && (vd = vd.slice(0, -1))), setTimeout(function() {
                                j && e.bind(window, "hashchange", f.onHashChange)
                            }, 40)
                        }
                    },
                    onHashChange: function() {
                        return Cd() === vd ? (xd = !0, void f.close()) : void(sd || (td = !0, f.goTo(Ed().pid), td = !1))
                    },
                    updateURL: function() {
                        Dd(), td || (wd ? pd = setTimeout(Fd, 800) : Fd())
                    }
                }
            }), e.extend(f, cc)
        };
        return a
    }), ! function(a, b) {
        "function" == typeof define && define.amd ? define(b) : "object" == typeof exports ? module.exports = b() : a.PhotoSwipeUI_Default = b()
    }(this, function() {
        "use strict";
        var a = function(a, b) {
            var c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v = this,
                w = !1,
                x = !0,
                y = !0,
                z = {
                    barsSize: {
                        top: 44,
                        bottom: "auto"
                    },
                    closeElClasses: ["item", "caption", "zoom-wrap", "ui", "top-bar"],
                    timeToIdle: 4e3,
                    timeToIdleOutside: 1e3,
                    loadingIndicatorDelay: 1e3,
                    addCaptionHTMLFn: function(a, b) {
                        return a.title ? (b.children[0].innerHTML = a.title, !0) : (b.children[0].innerHTML = "", !1)
                    },
                    closeEl: !0,
                    captionEl: !0,
                    fullscreenEl: !0,
                    zoomEl: !0,
                    shareEl: !0,
                    counterEl: !0,
                    arrowEl: !0,
                    preloaderEl: !0,
                    tapToClose: !1,
                    tapToToggleControls: !0,
                    clickToCloseNonZoomable: !0,
                    shareButtons: [{
                        id: "facebook",
                        label: "Share on Facebook",
                        url: "https://www.facebook.com/sharer/sharer.php?u={{url}}"
                    }, {
                        id: "twitter",
                        label: "Tweet",
                        url: "https://twitter.com/intent/tweet?text={{text}}&url={{url}}"
                    }, {
                        id: "pinterest",
                        label: "Pin it",
                        url: "http://www.pinterest.com/pin/create/button/?url={{url}}&media={{image_url}}&description={{text}}"
                    }, {
                        id: "download",
                        label: "Download image",
                        url: "{{raw_image_url}}",
                        download: !0
                    }],
                    getImageURLForShare: function() {
                        return a.currItem.src || ""
                    },
                    getPageURLForShare: function() {
                        return window.location.href
                    },
                    getTextForShare: function() {
                        return a.currItem.title || ""
                    },
                    indexIndicatorSep: " / "
                },
                A = function(a) {
                    if (r) return !0;
                    a = a || window.event, q.timeToIdle && q.mouseUsed && !k && K();
                    for (var c, d, e = a.target || a.srcElement, f = e.className, g = 0; g < S.length; g++) c = S[g], c.onTap && f.indexOf("pswp__" + c.name) > -1 && (c.onTap(), d = !0);
                    if (d) {
                        a.stopPropagation && a.stopPropagation(), r = !0;
                        var h = b.features.isOldAndroid ? 600 : 30;
                        s = setTimeout(function() {
                            r = !1
                        }, h)
                    }
                },
                B = function() {
                    return !a.likelyTouchDevice || q.mouseUsed || screen.width > 1200
                },
                C = function(a, c, d) {
                    b[(d ? "add" : "remove") + "Class"](a, "pswp__" + c)
                },
                D = function() {
                    var a = 1 === q.getNumItemsFn();
                    a !== p && (C(d, "ui--one-slide", a), p = a)
                },
                E = function() {
                    C(i, "share-modal--hidden", y)
                },
                F = function() {
                    return y = !y, y ? (b.removeClass(i, "pswp__share-modal--fade-in"), setTimeout(function() {
                        y && E()
                    }, 300)) : (E(), setTimeout(function() {
                        y || b.addClass(i, "pswp__share-modal--fade-in")
                    }, 30)), y || H(), !1
                },
                G = function(b) {
                    b = b || window.event;
                    var c = b.target || b.srcElement;
                    return a.shout("shareLinkClick", b, c), c.href ? c.hasAttribute("download") ? !0 : (window.open(c.href, "pswp_share", "scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,top=100,left=" + (window.screen ? Math.round(screen.width / 2 - 275) : 100)), y || F(), !1) : !1
                },
                H = function() {
                    for (var a, b, c, d, e, f = "", g = 0; g < q.shareButtons.length; g++) a = q.shareButtons[g], c = q.getImageURLForShare(a), d = q.getPageURLForShare(a), e = q.getTextForShare(a), b = a.url.replace("{{url}}", encodeURIComponent(d)).replace("{{image_url}}", encodeURIComponent(c)).replace("{{raw_image_url}}", c).replace("{{text}}", encodeURIComponent(e)), f += '<a href="' + b + '" target="_blank" class="pswp__share--' + a.id + '"' + (a.download ? "download" : "") + ">" + a.label + "</a>", q.parseShareButtonOut && (f = q.parseShareButtonOut(a, f));
                    i.children[0].innerHTML = f, i.children[0].onclick = G
                },
                I = function(a) {
                    for (var c = 0; c < q.closeElClasses.length; c++)
                        if (b.hasClass(a, "pswp__" + q.closeElClasses[c])) return !0
                },
                J = 0,
                K = function() {
                    clearTimeout(u), J = 0, k && v.setIdle(!1)
                },
                L = function(a) {
                    a = a ? a : window.event;
                    var b = a.relatedTarget || a.toElement;
                    b && "HTML" !== b.nodeName || (clearTimeout(u), u = setTimeout(function() {
                        v.setIdle(!0)
                    }, q.timeToIdleOutside))
                },
                M = function() {
                    q.fullscreenEl && (c || (c = v.getFullscreenAPI()), c ? (b.bind(document, c.eventK, v.updateFullscreen), v.updateFullscreen(), b.addClass(a.template, "pswp--supports-fs")) : b.removeClass(a.template, "pswp--supports-fs"))
                },
                N = function() {
                    q.preloaderEl && (O(!0), l("beforeChange", function() {
                        clearTimeout(o), o = setTimeout(function() {
                            a.currItem && a.currItem.loading ? (!a.allowProgressiveImg() || a.currItem.img && !a.currItem.img.naturalWidth) && O(!1) : O(!0)
                        }, q.loadingIndicatorDelay)
                    }), l("imageLoadComplete", function(b, c) {
                        a.currItem === c && O(!0)
                    }))
                },
                O = function(a) {
                    n !== a && (C(m, "preloader--active", !a), n = a)
                },
                P = function(a) {
                    var c = a.vGap;
                    if (B()) {
                        var g = q.barsSize;
                        if (q.captionEl && "auto" === g.bottom)
                            if (f || (f = b.createEl("pswp__caption pswp__caption--fake"), f.appendChild(b.createEl("pswp__caption__center")), d.insertBefore(f, e), b.addClass(d, "pswp__ui--fit")), q.addCaptionHTMLFn(a, f, !0)) {
                                var h = f.clientHeight;
                                c.bottom = parseInt(h, 10) || 44
                            } else c.bottom = g.top;
                        else c.bottom = "auto" === g.bottom ? 0 : g.bottom;
                        c.top = g.top
                    } else c.top = c.bottom = 0
                },
                Q = function() {
                    q.timeToIdle && l("mouseUsed", function() {
                        b.bind(document, "mousemove", K), b.bind(document, "mouseout", L), t = setInterval(function() {
                            J++, 2 === J && v.setIdle(!0)
                        }, q.timeToIdle / 2)
                    })
                },
                R = function() {
                    l("onVerticalDrag", function(a) {
                        x && .95 > a ? v.hideControls() : !x && a >= .95 && v.showControls()
                    });
                    var a;
                    l("onPinchClose", function(b) {
                        x && .9 > b ? (v.hideControls(), a = !0) : a && !x && b > .9 && v.showControls()
                    }), l("zoomGestureEnded", function() {
                        a = !1, a && !x && v.showControls()
                    })
                },
                S = [{
                    name: "caption",
                    option: "captionEl",
                    onInit: function(a) {
                        e = a
                    }
                }, {
                    name: "share-modal",
                    option: "shareEl",
                    onInit: function(a) {
                        i = a
                    },
                    onTap: function() {
                        F()
                    }
                }, {
                    name: "button--share",
                    option: "shareEl",
                    onInit: function(a) {
                        h = a
                    },
                    onTap: function() {
                        F()
                    }
                }, {
                    name: "button--zoom",
                    option: "zoomEl",
                    onTap: a.toggleDesktopZoom
                }, {
                    name: "counter",
                    option: "counterEl",
                    onInit: function(a) {
                        g = a
                    }
                }, {
                    name: "button--close",
                    option: "closeEl",
                    onTap: a.close
                }, {
                    name: "button--arrow--left",
                    option: "arrowEl",
                    onTap: a.prev
                }, {
                    name: "button--arrow--right",
                    option: "arrowEl",
                    onTap: a.next
                }, {
                    name: "button--fs",
                    option: "fullscreenEl",
                    onTap: function() {
                        c.isFullscreen() ? c.exit() : c.enter()
                    }
                }, {
                    name: "preloader",
                    option: "preloaderEl",
                    onInit: function(a) {
                        m = a
                    }
                }],
                T = function() {
                    var a, c, e, f = function(d) {
                        if (d)
                            for (var f = d.length, g = 0; f > g; g++) {
                                a = d[g], c = a.className;
                                for (var h = 0; h < S.length; h++) e = S[h], c.indexOf("pswp__" + e.name) > -1 && (q[e.option] ? (b.removeClass(a, "pswp__element--disabled"), e.onInit && e.onInit(a)) : b.addClass(a, "pswp__element--disabled"))
                            }
                    };
                    f(d.children);
                    var g = b.getChildByClass(d, "pswp__top-bar");
                    g && f(g.children)
                };
            v.init = function() {
                b.extend(a.options, z, !0), q = a.options, d = b.getChildByClass(a.scrollWrap, "pswp__ui"), l = a.listen, R(), l("beforeChange", v.update), l("doubleTap", function(b) {
                    var c = a.currItem.initialZoomLevel;
                    a.getZoomLevel() !== c ? a.zoomTo(c, b, 333) : a.zoomTo(q.getDoubleTapZoom(!1, a.currItem), b, 333)
                }), l("preventDragEvent", function(a, b, c) {
                    var d = a.target || a.srcElement;
                    d && d.className && a.type.indexOf("mouse") > -1 && (d.className.indexOf("__caption") > 0 || /(SMALL|STRONG|EM)/i.test(d.tagName)) && (c.prevent = !1)
                }), l("bindEvents", function() {
                    b.bind(d, "pswpTap click", A), b.bind(a.scrollWrap, "pswpTap", v.onGlobalTap), a.likelyTouchDevice || b.bind(a.scrollWrap, "mouseover", v.onMouseOver)
                }), l("unbindEvents", function() {
                    y || F(), t && clearInterval(t), b.unbind(document, "mouseout", L), b.unbind(document, "mousemove", K), b.unbind(d, "pswpTap click", A), b.unbind(a.scrollWrap, "pswpTap", v.onGlobalTap), b.unbind(a.scrollWrap, "mouseover", v.onMouseOver), c && (b.unbind(document, c.eventK, v.updateFullscreen), c.isFullscreen() && (q.hideAnimationDuration = 0, c.exit()), c = null)
                }), l("destroy", function() {
                    q.captionEl && (f && d.removeChild(f), b.removeClass(e, "pswp__caption--empty")), i && (i.children[0].onclick = null), b.removeClass(d, "pswp__ui--over-close"), b.addClass(d, "pswp__ui--hidden"), v.setIdle(!1)
                }), q.showAnimationDuration || b.removeClass(d, "pswp__ui--hidden"), l("initialZoomIn", function() {
                    q.showAnimationDuration && b.removeClass(d, "pswp__ui--hidden")
                }), l("initialZoomOut", function() {
                    b.addClass(d, "pswp__ui--hidden")
                }), l("parseVerticalMargin", P), T(), q.shareEl && h && i && (y = !0), D(), Q(), M(), N()
            }, v.setIdle = function(a) {
                k = a, C(d, "ui--idle", a)
            }, v.update = function() {
                x && a.currItem ? (v.updateIndexIndicator(), q.captionEl && (q.addCaptionHTMLFn(a.currItem, e), C(e, "caption--empty", !a.currItem.title)), w = !0) : w = !1, y || F(), D()
            }, v.updateFullscreen = function(d) {
                d && setTimeout(function() {
                    a.setScrollOffset(0, b.getScrollY())
                }, 50), b[(c.isFullscreen() ? "add" : "remove") + "Class"](a.template, "pswp--fs")
            }, v.updateIndexIndicator = function() {
                q.counterEl && (g.innerHTML = a.getCurrentIndex() + 1 + q.indexIndicatorSep + q.getNumItemsFn())
            }, v.onGlobalTap = function(c) {
                c = c || window.event;
                var d = c.target || c.srcElement;
                if (!r)
                    if (c.detail && "mouse" === c.detail.pointerType) {
                        if (I(d)) return void a.close();
                        b.hasClass(d, "pswp__img") && (1 === a.getZoomLevel() && a.getZoomLevel() <= a.currItem.fitRatio ? q.clickToCloseNonZoomable && a.close() : a.toggleDesktopZoom(c.detail.releasePoint))
                    } else if (q.tapToToggleControls && (x ? v.hideControls() : v.showControls()), q.tapToClose && (b.hasClass(d, "pswp__img") || I(d))) return void a.close()
            }, v.onMouseOver = function(a) {
                a = a || window.event;
                var b = a.target || a.srcElement;
                C(d, "ui--over-close", I(b))
            }, v.hideControls = function() {
                b.addClass(d, "pswp__ui--hidden"), x = !1
            }, v.showControls = function() {
                x = !0, w || v.update(), b.removeClass(d, "pswp__ui--hidden")
            }, v.supportsFullscreen = function() {
                var a = document;
                return !!(a.exitFullscreen || a.mozCancelFullScreen || a.webkitExitFullscreen || a.msExitFullscreen)
            }, v.getFullscreenAPI = function() {
                var b, c = document.documentElement,
                    d = "fullscreenchange";
                return c.requestFullscreen ? b = {
                    enterK: "requestFullscreen",
                    exitK: "exitFullscreen",
                    elementK: "fullscreenElement",
                    eventK: d
                } : c.mozRequestFullScreen ? b = {
                    enterK: "mozRequestFullScreen",
                    exitK: "mozCancelFullScreen",
                    elementK: "mozFullScreenElement",
                    eventK: "moz" + d
                } : c.webkitRequestFullscreen ? b = {
                    enterK: "webkitRequestFullscreen",
                    exitK: "webkitExitFullscreen",
                    elementK: "webkitFullscreenElement",
                    eventK: "webkit" + d
                } : c.msRequestFullscreen && (b = {
                    enterK: "msRequestFullscreen",
                    exitK: "msExitFullscreen",
                    elementK: "msFullscreenElement",
                    eventK: "MSFullscreenChange"
                }), b && (b.enter = function() {
                    return j = q.closeOnScroll, q.closeOnScroll = !1, "webkitRequestFullscreen" !== this.enterK ? a.template[this.enterK]() : void a.template[this.enterK](Element.ALLOW_KEYBOARD_INPUT)
                }, b.exit = function() {
                    return q.closeOnScroll = j, document[this.exitK]()
                }, b.isFullscreen = function() {
                    return document[this.elementK]
                }), b
            }
        };
        return a
    }), window.Modernizr = function(a, b, c) {
        function d(a) {
            t.cssText = a
        }

        function e(a, b) {
            return d(x.join(a + ";") + (b || ""))
        }

        function f(a, b) {
            return typeof a === b
        }

        function g(a, b) {
            return !!~("" + a).indexOf(b)
        }

        function h(a, b) {
            for (var d in a) {
                var e = a[d];
                if (!g(e, "-") && t[e] !== c) return "pfx" == b ? e : !0
            }
            return !1
        }

        function i(a, b, d) {
            for (var e in a) {
                var g = b[a[e]];
                if (g !== c) return d === !1 ? a[e] : f(g, "function") ? g.bind(d || b) : g
            }
            return !1
        }

        function j(a, b, c) {
            var d = a.charAt(0).toUpperCase() + a.slice(1),
                e = (a + " " + z.join(d + " ") + d).split(" ");
            return f(b, "string") || f(b, "undefined") ? h(e, b) : (e = (a + " " + A.join(d + " ") + d).split(" "), i(e, b, c))
        }

        function k() {
            o.input = function(c) {
                for (var d = 0, e = c.length; e > d; d++) E[c[d]] = c[d] in u;
                return E.list && (E.list = !!b.createElement("datalist") && !!a.HTMLDataListElement), E
            }("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")), o.inputtypes = function(a) {
                for (var d, e, f, g = 0, h = a.length; h > g; g++) u.setAttribute("type", e = a[g]), d = "text" !== u.type, d && (u.value = v, u.style.cssText = "position:absolute;visibility:hidden;", /^range$/.test(e) && u.style.WebkitAppearance !== c ? (q.appendChild(u), f = b.defaultView, d = f.getComputedStyle && "textfield" !== f.getComputedStyle(u, null).WebkitAppearance && 0 !== u.offsetHeight, q.removeChild(u)) : /^(search|tel)$/.test(e) || (d = /^(url|email)$/.test(e) ? u.checkValidity && u.checkValidity() === !1 : u.value != v)), D[a[g]] = !!d;
                return D
            }("search tel url email datetime date month week time datetime-local number range color".split(" "))
        }
        var l, m, n = "2.8.3",
            o = {},
            p = !0,
            q = b.documentElement,
            r = "modernizr",
            s = b.createElement(r),
            t = s.style,
            u = b.createElement("input"),
            v = ":)",
            w = {}.toString,
            x = " -webkit- -moz- -o- -ms- ".split(" "),
            y = "Webkit Moz O ms",
            z = y.split(" "),
            A = y.toLowerCase().split(" "),
            B = {
                svg: "http://www.w3.org/2000/svg"
            },
            C = {},
            D = {},
            E = {},
            F = [],
            G = F.slice,
            H = function(a, c, d, e) {
                var f, g, h, i, j = b.createElement("div"),
                    k = b.body,
                    l = k || b.createElement("body");
                if (parseInt(d, 10))
                    for (; d--;) h = b.createElement("div"), h.id = e ? e[d] : r + (d + 1), j.appendChild(h);
                return f = ["&#173;", '<style id="s', r, '">', a, "</style>"].join(""), j.id = r, (k ? j : l).innerHTML += f, l.appendChild(j), k || (l.style.background = "", l.style.overflow = "hidden", i = q.style.overflow, q.style.overflow = "hidden", q.appendChild(l)), g = c(j, a), k ? j.parentNode.removeChild(j) : (l.parentNode.removeChild(l), q.style.overflow = i), !!g
            },
            I = function(b) {
                var c = a.matchMedia || a.msMatchMedia;
                if (c) return c(b) && c(b).matches || !1;
                var d;
                return H("@media " + b + " { #" + r + " { position: absolute; } }", function(b) {
                    d = "absolute" == (a.getComputedStyle ? getComputedStyle(b, null) : b.currentStyle).position
                }), d
            },
            J = function() {
                function a(a, e) {
                    e = e || b.createElement(d[a] || "div"), a = "on" + a;
                    var g = a in e;
                    return g || (e.setAttribute || (e = b.createElement("div")), e.setAttribute && e.removeAttribute && (e.setAttribute(a, ""), g = f(e[a], "function"), f(e[a], "undefined") || (e[a] = c), e.removeAttribute(a))), e = null, g
                }
                var d = {
                    select: "input",
                    change: "input",
                    submit: "form",
                    reset: "form",
                    error: "img",
                    load: "img",
                    abort: "img"
                };
                return a
            }(),
            K = {}.hasOwnProperty;
        m = f(K, "undefined") || f(K.call, "undefined") ? function(a, b) {
            return b in a && f(a.constructor.prototype[b], "undefined")
        } : function(a, b) {
            return K.call(a, b)
        }, Function.prototype.bind || (Function.prototype.bind = function(a) {
            var b = this;
            if ("function" != typeof b) throw new TypeError;
            var c = G.call(arguments, 1),
                d = function() {
                    if (this instanceof d) {
                        var e = function() {};
                        e.prototype = b.prototype;
                        var f = new e,
                            g = b.apply(f, c.concat(G.call(arguments)));
                        return Object(g) === g ? g : f
                    }
                    return b.apply(a, c.concat(G.call(arguments)))
                };
            return d
        }), C.flexbox = function() {
            return j("flexWrap")
        }, C.flexboxlegacy = function() {
            return j("boxDirection")
        }, C.canvas = function() {
            var a = b.createElement("canvas");
            return !!a.getContext && !!a.getContext("2d")
        }, C.canvastext = function() {
            return !!o.canvas && !!f(b.createElement("canvas").getContext("2d").fillText, "function")
        }, C.webgl = function() {
            return !!a.WebGLRenderingContext
        }, C.touch = function() {
            var c;
            return "ontouchstart" in a || a.DocumentTouch && b instanceof DocumentTouch ? c = !0 : H(["@media (", x.join("touch-enabled),("), r, ")", "{#modernizr{top:9px;position:absolute}}"].join(""), function(a) {
                c = 9 === a.offsetTop
            }), c
        }, C.geolocation = function() {
            return "geolocation" in navigator
        }, C.postmessage = function() {
            return !!a.postMessage
        }, C.websqldatabase = function() {
            return !!a.openDatabase
        }, C.indexedDB = function() {
            return !!j("indexedDB", a)
        }, C.hashchange = function() {
            return J("hashchange", a) && (b.documentMode === c || b.documentMode > 7)
        }, C.history = function() {
            return !!a.history && !!history.pushState
        }, C.draganddrop = function() {
            var a = b.createElement("div");
            return "draggable" in a || "ondragstart" in a && "ondrop" in a
        }, C.websockets = function() {
            return "WebSocket" in a || "MozWebSocket" in a
        }, C.rgba = function() {
            return d("background-color:rgba(150,255,150,.5)"), g(t.backgroundColor, "rgba")
        }, C.hsla = function() {
            return d("background-color:hsla(120,40%,100%,.5)"), g(t.backgroundColor, "rgba") || g(t.backgroundColor, "hsla")
        }, C.multiplebgs = function() {
            return d("background:url(https://),url(https://),red url(https://)"), /(url\s*\(.*?){3}/.test(t.background)
        }, C.backgroundsize = function() {
            return j("backgroundSize")
        }, C.borderimage = function() {
            return j("borderImage")
        }, C.borderradius = function() {
            return j("borderRadius")
        }, C.boxshadow = function() {
            return j("boxShadow")
        }, C.textshadow = function() {
            return "" === b.createElement("div").style.textShadow
        }, C.opacity = function() {
            return e("opacity:.55"), /^0.55$/.test(t.opacity)
        }, C.cssanimations = function() {
            return j("animationName")
        }, C.csscolumns = function() {
            return j("columnCount")
        }, C.cssgradients = function() {
            var a = "background-image:",
                b = "gradient(linear,left top,right bottom,from(#9f9),to(white));",
                c = "linear-gradient(left top,#9f9, white);";
            return d((a + "-webkit- ".split(" ").join(b + a) + x.join(c + a)).slice(0, -a.length)), g(t.backgroundImage, "gradient")
        }, C.cssreflections = function() {
            return j("boxReflect")
        }, C.csstransforms = function() {
            return !!j("transform")
        }, C.csstransforms3d = function() {
            var a = !!j("perspective");
            return a && "webkitPerspective" in q.style && H("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function(b) {
                a = 9 === b.offsetLeft && 3 === b.offsetHeight
            }), a
        }, C.csstransitions = function() {
            return j("transition")
        }, C.fontface = function() {
            var a;
            return H('@font-face {font-family:"font";src:url("https://")}', function(c, d) {
                var e = b.getElementById("smodernizr"),
                    f = e.sheet || e.styleSheet,
                    g = f ? f.cssRules && f.cssRules[0] ? f.cssRules[0].cssText : f.cssText || "" : "";
                a = /src/i.test(g) && 0 === g.indexOf(d.split(" ")[0])
            }), a
        }, C.generatedcontent = function() {
            var a;
            return H(["#", r, "{font:0/0 a}#", r, ':after{content:"', v, '";visibility:hidden;font:3px/1 a}'].join(""), function(b) {
                a = b.offsetHeight >= 3
            }), a
        }, C.video = function() {
            var a = b.createElement("video"),
                c = !1;
            try {
                (c = !!a.canPlayType) && (c = new Boolean(c), c.ogg = a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, ""), c.h264 = a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, ""), c.webm = a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, ""))
            } catch (d) {}
            return c
        }, C.audio = function() {
            var a = b.createElement("audio"),
                c = !1;
            try {
                (c = !!a.canPlayType) && (c = new Boolean(c), c.ogg = a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""), c.mp3 = a.canPlayType("audio/mpeg;").replace(/^no$/, ""), c.wav = a.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""), c.m4a = (a.canPlayType("audio/x-m4a;") || a.canPlayType("audio/aac;")).replace(/^no$/, ""))
            } catch (d) {}
            return c
        }, C.localstorage = function() {
            try {
                return localStorage.setItem(r, r), localStorage.removeItem(r), !0
            } catch (a) {
                return !1
            }
        }, C.sessionstorage = function() {
            try {
                return sessionStorage.setItem(r, r), sessionStorage.removeItem(r), !0
            } catch (a) {
                return !1
            }
        }, C.webworkers = function() {
            return !!a.Worker
        }, C.applicationcache = function() {
            return !!a.applicationCache
        }, C.svg = function() {
            return !!b.createElementNS && !!b.createElementNS(B.svg, "svg").createSVGRect
        }, C.inlinesvg = function() {
            var a = b.createElement("div");
            return a.innerHTML = "<svg/>", (a.firstChild && a.firstChild.namespaceURI) == B.svg
        }, C.smil = function() {
            return !!b.createElementNS && /SVGAnimate/.test(w.call(b.createElementNS(B.svg, "animate")))
        }, C.svgclippaths = function() {
            return !!b.createElementNS && /SVGClipPath/.test(w.call(b.createElementNS(B.svg, "clipPath")))
        };
        for (var L in C) m(C, L) && (l = L.toLowerCase(), o[l] = C[L](), F.push((o[l] ? "" : "no-") + l));
        return o.input || k(), o.addTest = function(a, b) {
                if ("object" == typeof a)
                    for (var d in a) m(a, d) && o.addTest(d, a[d]);
                else {
                    if (a = a.toLowerCase(), o[a] !== c) return o;
                    b = "function" == typeof b ? b() : b, "undefined" != typeof p && p && (q.className += " " + (b ? "" : "no-") + a), o[a] = b
                }
                return o
            }, d(""), s = u = null,
            function(a, b) {
                function c(a, b) {
                    var c = a.createElement("p"),
                        d = a.getElementsByTagName("head")[0] || a.documentElement;
                    return c.innerHTML = "x<style>" + b + "</style>", d.insertBefore(c.lastChild, d.firstChild)
                }

                function d() {
                    var a = s.elements;
                    return "string" == typeof a ? a.split(" ") : a
                }

                function e(a) {
                    var b = r[a[p]];
                    return b || (b = {}, q++, a[p] = q, r[q] = b), b
                }

                function f(a, c, d) {
                    if (c || (c = b), k) return c.createElement(a);
                    d || (d = e(c));
                    var f;
                    return f = d.cache[a] ? d.cache[a].cloneNode() : o.test(a) ? (d.cache[a] = d.createElem(a)).cloneNode() : d.createElem(a), !f.canHaveChildren || n.test(a) || f.tagUrn ? f : d.frag.appendChild(f)
                }

                function g(a, c) {
                    if (a || (a = b), k) return a.createDocumentFragment();
                    c = c || e(a);
                    for (var f = c.frag.cloneNode(), g = 0, h = d(), i = h.length; i > g; g++) f.createElement(h[g]);
                    return f
                }

                function h(a, b) {
                    b.cache || (b.cache = {}, b.createElem = a.createElement, b.createFrag = a.createDocumentFragment, b.frag = b.createFrag()), a.createElement = function(c) {
                        return s.shivMethods ? f(c, a, b) : b.createElem(c)
                    }, a.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + d().join().replace(/[\w\-]+/g, function(a) {
                        return b.createElem(a), b.frag.createElement(a), 'c("' + a + '")'
                    }) + ");return n}")(s, b.frag)
                }

                function i(a) {
                    a || (a = b);
                    var d = e(a);
                    return s.shivCSS && !j && !d.hasCSS && (d.hasCSS = !!c(a, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")), k || h(a, d), a
                }
                var j, k, l = "3.7.0",
                    m = a.html5 || {},
                    n = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
                    o = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
                    p = "_html5shiv",
                    q = 0,
                    r = {};
                ! function() {
                    try {
                        var a = b.createElement("a");
                        a.innerHTML = "<xyz></xyz>", j = "hidden" in a, k = 1 == a.childNodes.length || function() {
                            b.createElement("a");
                            var a = b.createDocumentFragment();
                            return "undefined" == typeof a.cloneNode || "undefined" == typeof a.createDocumentFragment || "undefined" == typeof a.createElement
                        }()
                    } catch (c) {
                        j = !0, k = !0
                    }
                }();
                var s = {
                    elements: m.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
                    version: l,
                    shivCSS: m.shivCSS !== !1,
                    supportsUnknownElements: k,
                    shivMethods: m.shivMethods !== !1,
                    type: "default",
                    shivDocument: i,
                    createElement: f,
                    createDocumentFragment: g
                };
                a.html5 = s, i(b)
            }(this, b), o._version = n, o._prefixes = x, o._domPrefixes = A, o._cssomPrefixes = z, o.mq = I, o.hasEvent = J, o.testProp = function(a) {
                return h([a])
            }, o.testAllProps = j, o.testStyles = H, q.className = q.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (p ? " js " + F.join(" ") : ""), o
    }(this, this.document),
    function(a, b, c) {
        function d(a) {
            return "[object Function]" == q.call(a)
        }

        function e(a) {
            return "string" == typeof a
        }

        function f() {}

        function g(a) {
            return !a || "loaded" == a || "complete" == a || "uninitialized" == a
        }

        function h() {
            var a = r.shift();
            s = 1, a ? a.t ? o(function() {
                ("c" == a.t ? m.injectCss : m.injectJs)(a.s, 0, a.a, a.x, a.e, 1)
            }, 0) : (a(), h()) : s = 0
        }

        function i(a, c, d, e, f, i, j) {
            function k(b) {
                if (!n && g(l.readyState) && (t.r = n = 1, !s && h(), l.onload = l.onreadystatechange = null, b)) {
                    "img" != a && o(function() {
                        v.removeChild(l)
                    }, 50);
                    for (var d in A[c]) A[c].hasOwnProperty(d) && A[c][d].onload()
                }
            }
            var j = j || m.errorTimeout,
                l = b.createElement(a),
                n = 0,
                q = 0,
                t = {
                    t: d,
                    s: c,
                    e: f,
                    a: i,
                    x: j
                };
            1 === A[c] && (q = 1, A[c] = []), "object" == a ? l.data = c : (l.src = c, l.type = a), l.width = l.height = "0", l.onerror = l.onload = l.onreadystatechange = function() {
                k.call(this, q)
            }, r.splice(e, 0, t), "img" != a && (q || 2 === A[c] ? (v.insertBefore(l, u ? null : p), o(k, j)) : A[c].push(l))
        }

        function j(a, b, c, d, f) {
            return s = 0, b = b || "j", e(a) ? i("c" == b ? x : w, a, b, this.i++, c, d, f) : (r.splice(this.i++, 0, a), 1 == r.length && h()), this
        }

        function k() {
            var a = m;
            return a.loader = {
                load: j,
                i: 0
            }, a
        }
        var l, m, n = b.documentElement,
            o = a.setTimeout,
            p = b.getElementsByTagName("script")[0],
            q = {}.toString,
            r = [],
            s = 0,
            t = "MozAppearance" in n.style,
            u = t && !!b.createRange().compareNode,
            v = u ? n : p.parentNode,
            n = a.opera && "[object Opera]" == q.call(a.opera),
            n = !!b.attachEvent && !n,
            w = t ? "object" : n ? "script" : "img",
            x = n ? "script" : w,
            y = Array.isArray || function(a) {
                return "[object Array]" == q.call(a)
            },
            z = [],
            A = {},
            B = {
                timeout: function(a, b) {
                    return b.length && (a.timeout = b[0]), a
                }
            };
        m = function(a) {
            function b(a) {
                var b, c, d, a = a.split("!"),
                    e = z.length,
                    f = a.pop(),
                    g = a.length,
                    f = {
                        url: f,
                        origUrl: f,
                        prefixes: a
                    };
                for (c = 0; g > c; c++) d = a[c].split("="), (b = B[d.shift()]) && (f = b(f, d));
                for (c = 0; e > c; c++) f = z[c](f);
                return f
            }

            function g(a, e, f, g, h) {
                var i = b(a),
                    j = i.autoCallback;
                i.url.split(".").pop().split("?").shift(), i.bypass || (e && (e = d(e) ? e : e[a] || e[g] || e[a.split("/").pop().split("?")[0]]), i.instead ? i.instead(a, e, f, g, h) : (A[i.url] ? i.noexec = !0 : A[i.url] = 1, f.load(i.url, i.forceCSS || !i.forceJS && "css" == i.url.split(".").pop().split("?").shift() ? "c" : c, i.noexec, i.attrs, i.timeout), (d(e) || d(j)) && f.load(function() {
                    k(), e && e(i.origUrl, h, g), j && j(i.origUrl, h, g), A[i.url] = 2
                })))
            }

            function h(a, b) {
                function c(a, c) {
                    if (a) {
                        if (e(a)) c || (l = function() {
                            var a = [].slice.call(arguments);
                            m.apply(this, a), n()
                        }), g(a, l, b, 0, j);
                        else if (Object(a) === a)
                            for (i in h = function() {
                                    var b, c = 0;
                                    for (b in a) a.hasOwnProperty(b) && c++;
                                    return c
                                }(), a) a.hasOwnProperty(i) && (!c && !--h && (d(l) ? l = function() {
                                var a = [].slice.call(arguments);
                                m.apply(this, a), n()
                            } : l[i] = function(a) {
                                return function() {
                                    var b = [].slice.call(arguments);
                                    a && a.apply(this, b), n()
                                }
                            }(m[i])), g(a[i], l, b, i, j))
                    } else !c && n()
                }
                var h, i, j = !!a.test,
                    k = a.load || a.both,
                    l = a.callback || f,
                    m = l,
                    n = a.complete || f;
                c(j ? a.yep : a.nope, !!k), k && c(k)
            }
            var i, j, l = this.yepnope.loader;
            if (e(a)) g(a, 0, l, 0);
            else if (y(a))
                for (i = 0; i < a.length; i++) j = a[i], e(j) ? g(j, 0, l, 0) : y(j) ? m(j) : Object(j) === j && h(j, l);
            else Object(a) === a && h(a, l)
        }, m.addPrefix = function(a, b) {
            B[a] = b
        }, m.addFilter = function(a) {
            z.push(a)
        }, m.errorTimeout = 1e4, null == b.readyState && b.addEventListener && (b.readyState = "loading", b.addEventListener("DOMContentLoaded", l = function() {
            b.removeEventListener("DOMContentLoaded", l, 0), b.readyState = "complete"
        }, 0)), a.yepnope = k(), a.yepnope.executeStack = h, a.yepnope.injectJs = function(a, c, d, e, i, j) {
            var k, l, n = b.createElement("script"),
                e = e || m.errorTimeout;
            n.src = a;
            for (l in d) n.setAttribute(l, d[l]);
            c = j ? h : c || f, n.onreadystatechange = n.onload = function() {
                !k && g(n.readyState) && (k = 1, c(), n.onload = n.onreadystatechange = null)
            }, o(function() {
                k || (k = 1, c(1))
            }, e), i ? n.onload() : p.parentNode.insertBefore(n, p)
        }, a.yepnope.injectCss = function(a, c, d, e, g, i) {
            var j, e = b.createElement("link"),
                c = i ? h : c || f;
            e.href = a, e.rel = "stylesheet", e.type = "text/css";
            for (j in d) e.setAttribute(j, d[j]);
            g || (p.parentNode.insertBefore(e, p), o(c, 0))
        }
    }(this, document), Modernizr.load = function() {
        yepnope.apply(window, [].slice.call(arguments, 0))
    };
var Pattern = {};
Pattern.Mediator = function() {
    var a = {},
        b = function(b, c) {
            return a[b] || (a[b] = []), a[b].push({
                context: this,
                callback: c
            }), this
        },
        c = function(b) {
            if (!a[b]) return !1;
            for (var c = Array.prototype.slice.call(arguments, 1), d = 0, e = a[b].length; e > d; d++) {
                var f = a[b][d];
                f.callback.apply(f.context, c)
            }
            return this
        };
    return {
        pub: c,
        sub: b,
        installTo: function(a) {
            a.pub = c, a.sub = b
        }
    }
}();
var DnDMoM = {};
jQuery(document).ready(function() {
    ! function(a) {
        var b = a(window),
            c = a("body");
        if (DnDMoM.initMainNav(), c.hasClass("homepage") && (DnDMoM.initImageSlider("#key-features"), DnDMoM.initHotEventSlider()), DnDMoM.initTopButton(), c.hasClass("homepage")) {
            DnDMoM.setHeaderView();
            var d = a("#primary-banner__video");
            d.on("click", function() {
                var a = d.parent().parent();
                this.played.length > 0 && !this.paused ? (this.pause(), a.addClass("video--paused")) : (this.play(), a.removeClass("video--preloading").removeClass("video--paused"))
            }), d.parent().prev(".primary-banner__overlays").on("click", function() {
                return d.trigger("click"), !1
            })
        }
        c.hasClass("homepage") && DnDMoM.initFilterPosts("#posts__tabs");
        var e = "http://img.zing.vn/products/devmobile/config/config.json?callback=?";
        a.ajax({
            type: "GET",
            url: e,
            jsonpCallback: "jsonCallback",
            contentType: "application/json",
            dataType: "jsonp",
            success: function(b) {
                DnDMoM.config = b, c.hasClass("subpage") && (DnDMoM.initRouter(), DnDMoM.initSubpageHasher(), a("#posts__list").hasClass("gallery__list") && DnDMoM.initPopup(".fancybox"))
            },
            error: function() {}
        }), b.on("orientationchange", function() {
            0 == window.orientation
        })
    }(jQuery)
}), DnDMoM = function(a) {
    var b = {
            swiper: {
                mode: "horizontal",
                loop: !1,
                slidesPerView: "auto",
                slideElement: "li"
            }
        },
        c = a(window),
        d = function(b, c) {
            return a.each(c, function(a, c) {
                b = b.replace(new RegExp("{ " + a + " }", "g"), c)
            }), b
        },
        e = function(b, d, e, f) {
            var g = (new Date).valueOf(),
                h = a("#posts__list"),
                i = '<p class="posts__loading"><span>Äang táº£i dá»¯ liá»‡u...</span></p>',
                j = h.prev(".posts__loading");
            return 0 == j.length && (j = a(i), h.before(j)), h.find("> li:not(.posts__loading)").addClass("inactive").end().addClass("posts__list--inactive"), j.removeClass("posts__loading--hidden"), h.offset().top < c.scrollTop() && a("body, html").animate({
                scrollTop: h.offset().top * c.height() / a(document).height()
            }), a.ajax({
                type: "POST",
                url: b,
                dataType: "json",
                contentType: "json",
                data: JSON.stringify({}),
                success: function(a, b, c) {
                    setTimeout(function() {
                        h.html(a).removeClass("posts__list--inactive"), j.addClass("posts__loading--hidden"), void 0 !== d && d(a, b, c)
                    }, (new Date).valueOf() - g >= 1e3 ? 0 : 1e3)
                },
                error: function() {
                    void 0 !== e && e()
                },
                completed: function() {
                    void 0 !== f && f()
                }
            })
        },
        f = {
            blogroll: function(b, c) {
                a("#drawer-toggle").removeAttr("checked");
                var f = '<li><a href="" class="pagination__index" data-index={ page } data-href="#!{ cate }?p={ page }" title="{ page }">{ page }</a></li>';
                a("#posts__tabs").find("> li.active").removeClass("active").end().find("a[data-cate=" + b + "]").parent().addClass("active"), a(".pagination__list").on("click", ".pagination__nav--disabled", function() {
                    return !1
                });
                var g = DnDMoM.config.allPostsService;
                switch (b) {
                    case "all":
                        g = DnDMoM.config.allPostsService;
                        break;
                    case "news":
                        g = DnDMoM.config.newsService;
                        break;
                    case "events":
                        g = DnDMoM.config.eventsService;
                        break;
                    case "gallery":
                        g = DnDMoM.config.galleryService
                }
                return g = d(g, {
                    page: c
                }), e(g, function() {
                    var e = a("#itemTotal").val(),
                        g = a("#itemPerPage").val(),
                        h = 5,
                        i = Math.ceil(e / g),
                        j = h > i ? i : h,
                        k = "";
                    a(".pagination__list a.pagination__index").parent().remove();
                    for (var l = 1; j >= l; l++) k += d(f, {
                        cate: b,
                        page: l
                    });
                    if (a(".pagination__list li:first-child").after(k), k = "", i > h) {
                        if (c >= j)
                            for (var l = c - 2; c + j - 2 > l; l++) k += d(f, {
                                cate: b,
                                page: l
                            });
                        else
                            for (var l = 0; j > l; l++) k += d(f, {
                                cate: b,
                                page: l + 1
                            });
                        a(".pagination__list a.pagination__index").parent().remove(), a(".pagination__list li:first-child").after(k)
                    }
                    a(".pagination__list a.pagination__index").removeClass("pagination__index--active").filter("[data-index=" + c + "]").addClass("pagination__index--active"), a(".pagination__list a.pagination__index").each(function() {
                        var c = a(this),
                            e = c.data("href");
                        c.attr("href", d(e, {
                            cate: b
                        }))
                    }), a(".pagination__list a.pagination__nav").each(function() {
                        var e = a(this),
                            f = e.data("href");
                        e.attr("href", d(f, {
                            cate: b,
                            page: e.hasClass("pagination__nav-prev") ? c - 1 : c + 1
                        })), e.hasClass("pagination__nav-prev") && e.toggleClass("pagination__nav--disabled", 1 == c), e.hasClass("pagination__nav-next") && e.toggleClass("pagination__nav--disabled", c == i)
                    })
                }, function() {}, function() {})
            }
        };
    return {
        initMainNav: function() {
            var b = a("#main-nav"),
                d = a("#main-nav__list"),
                e = Modernizr.touch ? "touchend" : "click";

            if (d.on(e, " > li > ul a", function(a) {
                    a.stopPropagation()
                }).on(e, " > li > a.main-nav__has-sub", function() {
                    return a("#main-nav__list > li > a.main-nav__has-sub.focus").not(this).removeClass("focus"), a(this).toggleClass("focus"), !1
                }), a(document).on(e, function() {
                    a("#main-nav__list > li > a.main-nav__has-sub.focus").removeClass("focus")
                }), DnDMoM.sub("mainNav:changed", function(a, b) {
                    d.find("a.active").removeClass("active"), d.find('a[href$="' + a + ".html#!" + b + '?p=1"]').addClass("active")
                }), 0 == d.find("a.active").length) {
                var f = window.location.href.split("/").pop(),
                    g = new RegExp(".html", "g");
                g.test(f) ? d.find('a[href*="' + f + '"]').addClass("active") : d.find("a").eq(0).addClass("active")
            }
            var h = a("#drawer-toggle"),
                i = function() {
                    Modernizr.mq("only screen and (max-width: 666px), (min-width: 667px) and (max-width: 1024px)") && h.on("change", function() {
                        a(this).is(":checked") ? b.addClass("shown") : setTimeout(function() {
                            b.removeClass("shown")
                        }, 250)
                    })
                };
            return a(window).on("resize", i), i(), Modernizr.mq("only screen and (max-width: 666px), (min-width: 667px) and (max-width: 1024px)") && a(window).swipeListener({
                minX: 25,
                minY: -1, 
                swipeRight: function(b) {
                    //b.coords.start.x <= .25 * c.width() && Math.abs(b.coords.start.y - b.coords.stop.y) <= 20 && (h.prop("checked", "checked"), a("#main-nav").addClass("shown"))
                },
                swipeLeft: function(a) {
                    // Math.abs(a.coords.start.y - a.coords.stop.y) <= 20 && (h.prop("checked", !1), setTimeout(function() {
                    //     b.removeClass("shown")
                    // }, 250))
                },
                swipeUp: function() {
                    return !0
                },
                swipeDown: function() {
                    return !0
                }
            }), d
        },
        initImageSlider: function(d) {
            var e = function() {
                DnDMoM.keyFeaturesSwiper instanceof Swiper && (DnDMoM.keyFeaturesSwiper = DnDMoM.destroySlider(DnDMoM.keyFeaturesSwiper));
                var c = function() {
                        return !1
                    },
                    e = a(d + " a.key-features__fancybox");
                DnDMoM.initMobileGalleryViewer("#key-features .swiper-wrapper"), DnDMoM.keyFeaturesSwiper = new Swiper(d, a.extend(!0, {}, b.swiper, {
                    autoResize: !1,
                    resizeReInit: !0,
                    slidesPerViewFit: !0,
                    grabCursor: !0,
                    slidesPerView: "auto",
                    offsetPxBefore: Modernizr.mq("only screen and (min-width: 667px)") ? 0 : 15,
                    onTouchMove: function(a, b, d) {
                        if (0 !== d) {
                            if (e.hasClass("key-features__fancybox--disabled")) return !1;
                            e.addClass("key-features__fancybox--disabled").on("click", c)
                        }
                    },
                    onTouchEnd: function() {
                        setTimeout(function() {
                            e.off("click", c), e.removeClass("key-features__fancybox--disabled")
                        }, 1)
                    }
                })), a(".key-features__control").on("click", function() {
                    return !1
                }), a(".key-features__control--prev").on("click", function() {
                    DnDMoM.keyFeaturesSwiper.swipePrev()
                }), a(".key-features__control--next").on("click", function() {
                    DnDMoM.keyFeaturesSwiper.swipeNext()
                })
            };
            c.resize(function() {
                e()
            }), e()
        },
        initHotEventSlider: function() {
            var d = function() {
                DnDMoM.hotEvent instanceof Swiper && (DnDMoM.hotEvent = DnDMoM.destroySlider(DnDMoM.hotEvent)), Modernizr.mq("only screen and (max-width: 666px)") ? void 0 === DnDMoM.hotEvent && (DnDMoM.hotEvent = function() {
                    return new Swiper("#hot-events__list", a.extend(!0, {}, b.swiper, {
                        slidesPerView: "auto",
                        slideActiveClass: "swiper-slide--active",
                        offsetPxBefore: Modernizr.mq("only screen and (min-width: 667px)") ? 0 : 15
                    }))
                }()) : void 0 === DnDMoM.hotEvent && (DnDMoM.hotEvent = function() {
                    var b = a("#hot-events__list > ul"),
                        c = b.height(),
                        d = a(".swiper-slide > a > div");
                    d.css({
                        height: c / d.length
                    }), b.on("mouseenter", ".swiper-slide", function() {
                        return b.find(".swiper-slide--active").removeClass("swiper-slide--active"), a(this).addClass("swiper-slide--active"), !1
                    })
                }())
            };
            c.resize(function() {
                d()
            }), d()
        },
        initTopButton: function() {
            var b = a("#top-button"),
                d = .5 * a(document).height(),
                e = function() {
                    c.scrollTop() >= d ? b.hasClass("flyout-content__top-button--shown") || b.addClass("flyout-content__top-button--shown") : b.removeClass("flyout-content__top-button--shown")
                };
            c.on("scroll", function() {
                e()
            }), e();
            var f = Modernizr.touch ? "touchstart" : "click";
            b.on(f, function() {
                return a("body, html").stop(!0, !0).animate({
                    scrollTop: 0
                }), !1
            })
        },
        initFilterPosts: function(b) {
            var c, f = a(b);
            return 0 == f.length ? !1 : (f.on("click", "a", function() {
                var b = a(this);
                f.find("> li.active").removeClass("active"), b.parent().addClass("active"), void 0 !== c && c.abort();
                var g = b.attr("href");
                f.find("> li.active").removeClass("active").end().find('a[href="' + g + '"]').parent().addClass("active");
                var h = a("#posts__view-all");
                return h.attr("href", d(h.data("href"), {
                    cate: b.data("cate")
                })), c = e(g, function() {}, function() {}, function() {}), !1
            }), void f.find("a:eq(0)").trigger("click"))
        },
        initSubpageHasher: function() {
            function a(a, b) {
                setTimeout(function() {
                    a !== b && crossroads.parse(window.location.href.split("/").pop())
                }, 1)
            }
            return "undefined" == typeof hasher ? !1 : (hasher.prependHash = "!", hasher.changed.add(a), hasher.initialized.add(a), void hasher.init())
        },
        initRouter: function() {
            if ("undefined" == typeof crossroads) return !1;
            var b;
            crossroads.addRoute("/posts.html:?query:", function(b) {
                if (void 0 !== b) {
                    var c = window.location.href.indexOf("?");
                    window.location = window.location.href.substr(0, c) + "#!all?" + a.param(b)
                } else hasher.setHash("all")
            }), crossroads.addRoute("/posts.html#!{cate}:?query:", function(a, c) {
                void 0 === c || void 0 === c.p ? hasher.setHash(a + "?p=1") : (void 0 !== b && b.abort(), b = f.blogroll(a, parseInt(c.p)), DnDMoM.pub("mainNav:changed", "posts", a))
            }), crossroads.addRoute("/media.html:?query:", function(b) {
                if (void 0 !== b) {
                    var c = window.location.href.indexOf("?");
                    window.location = window.location.href.substr(0, c) + "#!gallery?" + a.param(b)
                } else hasher.setHash("gallery")
            }), crossroads.addRoute("/media.html#!{cate}:?query:", function(a, c) {
                void 0 === c || void 0 === c.p ? hasher.setHash(a + "?p=1") : (void 0 !== b && b.abort(), b = f.blogroll(a, parseInt(c.p)), b.success(function() {
                    setTimeout(function() {
                        DnDMoM.initMobileGalleryViewer("#posts__list.gallery__list")
                    }, 1e3)
                }), DnDMoM.pub("mainNav:changed", "media", a))
            })
        },
        initPopup: function(b) {
            a(b).length > 0 && a(b).fancybox({
                openEffect: "elastic",
                autoCenter: !0,
                padding: [7, 7, 7, 7],
                helpers: {
                    title: {
                        type: "inside"
                    },
                    media: {}
                },
                nextEffect: "elastic",
                prevEffect: "elastic"
            })
        },
        initMobileGalleryViewer: function(b) {
            var c = {
                    history: !1,
                    timeToIdle: 0,
                    timeToIdleOutside: 500,
                    loadingIndicatorDelay: 0,
                    hideAnimationDuration: 0,
                    showAnimationDuration: 0,
                    preloaderEl: !1
                },
                d = function(b) {
                    for (var d = function(b) {
                            for (var c, d, e, f = a(b).find(".dndmom__photoswipe").toArray(), g = f.length, h = [], i = 0; g > i; i++) c = f[i], 1 === c.nodeType && (d = c.children[0], e = {
                                src: d.getAttribute("href")
                            }, c.children.length > 1 && (e.title = c.children[1].innerHTML), d.children.length > 0 && (e.msrc = d.children[0].getAttribute("src")), e.el = c, h.push(e));
                            return h
                        }, e = function() {
                            var a = window.location.hash.substring(1),
                                b = {};
                            if (a.length < 5) return b;
                            for (var c = a.split("&"), d = 0; d < c.length; d++)
                                if (c[d]) {
                                    var e = c[d].split("=");
                                    e.length < 2 || (b[e[0]] = e[1])
                                }
                            return b.gid && (b.gid = parseInt(b.gid, 10)), b.hasOwnProperty("pid") ? (b.pid = parseInt(b.pid, 10), b) : b
                        }, f = function(b, e) {
                            var f, g, h, i = document.querySelectorAll(".pswp")[0];
                            h = d(e), g = a.extend(!0, c, {
                                index: b,
                                galleryUID: e.getAttribute("data-pswp-uid"),
                                getThumbBoundsFn: function(a) {
                                    var b = h[a].el.getElementsByTagName("img")[0],
                                        c = window.pageYOffset || document.documentElement.scrollTop,
                                        d = b.getBoundingClientRect();
                                    return {
                                        x: d.left,
                                        y: d.top + c,
                                        w: d.width
                                    }
                                }
                            });
                            var j = 0;
                            a.each(h, function() {
                                var a = this,
                                    b = new Image;
                                b.onload = function() {
                                    a.w = b.width, a.h = b.height, j++
                                }, b.src = a.src
                            });
                            var k = setInterval(function() {
                                j == h.length && (clearInterval(k), f = new PhotoSwipe(i, PhotoSwipeUI_Default, h, g), f.listen("imageLoadComplete", function() {
                                    a(".dndmom__photoswipe--loading").removeClass("dndmom__photoswipe--loading")
                                }), f.listen("close unbindEvents", function() {
                                    a(i).removeAttr("class").addClass("pswp")
                                }), f.init())
                            }, 500)
                        }, g = document.querySelectorAll(b), h = 0, i = g.length; i > h; h++) g[h].setAttribute("data-pswp-uid", h + 1), a(g[h]).on("click", ".dndmom__photoswipe", function() {
                        var b = a(this).addClass("dndmom__photoswipe--loading");
                        return f(b.prevAll(".dndmom__photoswipe").length, b.parent().get(0)), !1
                    });
                    var j = e();
                    j.pid > 0 && j.gid > 0 && f(j.pid - 1, g[j.gid - 1])
                };
            d(b), a(".fancybox").on("click", function() {
                var b, c = a(this);
                if (c.hasClass("fancybox.iframe") && !c.hasClass("key-features__fancybox--disabled")) {
                    b = a('<div class="dndmom__modal"><a href="#" title="Close">Close</a></div>');
                    var d = a('<iframe src="' + c.attr("href") + '"></iframe>');
                    return a("body").append(b.append(d)), a(".dndmom__modal > a").on("click", function(a) {
                        a.preventDefault(), void 0 !== b && b.fadeOut().remove()
                    }), !1
                }
            })
        },
        destroySlider: function(b) {
            if (void 0 !== b) {
                var c = a(b.container);
                return c.find("*").stop(!0, !0).removeAttr("style"), void b.destroy()
            }
        },
        setHeaderView: function() {
            var b = function() {
                var b = a("#primary-banner");
                Modernizr.mq("only screen and (max-width: 667px)") ? b.css({
                    height: a(window).height() - a(".app-info").eq(0).height() - b.offset().top
                }) : b.removeAttr("style")
            };
            a(window).on("resize", b), b()
        }
    }
}(jQuery), Pattern.Mediator.installTo(DnDMoM);