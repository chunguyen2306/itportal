<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage IT Portal
 * @since IT Portal 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="shortcut icon" href="<?php echo get_site_icon_url(16); ?>" />
    <title><?php echo get_bloginfo('name'); ?></title>
    <script>
        <?php
            $currentUser = wp_get_current_user();
            $GLOBALS['AMUSER_ROLES'] = get_user_permission($currentUser->user_login);

        ?>
        UploadUrl = '<?php echo getFullPath('upload', '', 'url'); ?>';
        wp_current_user = '<?php echo wp_get_current_user()->display_name; ?>';
        // dev_upgrade  chờ fix lỗi signalr
        wp_current_user_domain = '<?php echo wp_get_current_user()->user_login; ?>';
    </script>
    <?php do_action('am_header')?>
    <script>
        window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
        ga('create', 'UA-126046044-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script async src='https://www.google-analytics.com/analytics.js'></script>
</head>
<body>

    <div class="pre-load-panel">
        <div class="process">
            In Progress ...
        </div>
    </div>
    <div class="loading">
        <div>
            <i class="fa fa-mobile"></i>
            <i class="fa fa-laptop"></i>
            <i class="fa fa-desktop"></i>
            <i class="fa fa-camera-retro"></i>
        </div>
    </div>