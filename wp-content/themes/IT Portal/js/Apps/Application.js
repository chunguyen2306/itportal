/**
 * Created by Administrator on 2/27/2017.
 */
function Application(){
  var me = this;

  var modules = [];
  var view = null;
  var model = null;

  this.reinstallModule = function(){
    modules = [];
  };

  this.registerModule = function(arr){
    if(typeof arr != 'Array') {
      modules.push(arr);
    } else {
      for(var i = 0; i < arr.length; i++){
        modules.push(arr[i]);
      }
    }
  };

  this.reBinding = function () {

    ko.clearNode();
    ko.applyBindings(model);

    $('*[data-bind]').each(function () {
      var bindingStr = $(this).attr('data-bind');
      var patt1 = /foreach: ?([^,\n\r]*)/;
      var patt2 = /foreach: ?{ ?data: ?([^,\n\r]*)/;

      var dataStr = '';

      if(patt2.exec(bindingStr) !== null){
          dataStr = patt2.exec(bindingStr)[1];
      } else {
          dataStr = patt1.exec(bindingStr)[1];
      }

      var data = eval('me.model'+dataStr);
      if(typeof(data) === 'function'){
        var temp = data();
        data([]);
        data(temp);
      } else{
        var pattParrent = /([^\.: {]*)\(\)/;
        var parentObj = pattParrent.exec(dataStr);
        if(parentObj !== null){
            parentObj = parentObj[parentObj.length-1];
            dataStr = dataStr.substr(0, dataStr.indexOf(parentObj) + parentObj.length );
        }
        data = eval('me.model'+dataStr);
        data(data());
      }
    });
  };

  this.binding = function () {
    ko.applyBindings(model);
  };

  this.do_Start = function () {
    model = new Module.Model();
    view = new Module.View(model);
    view.onDone = function () {
      me.binding();
    };
  };

  this.Start = function(){
    $(document).ready(function () {
      me.do_Start();
    });
  }

}