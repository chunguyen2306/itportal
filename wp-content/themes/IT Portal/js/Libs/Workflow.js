Libs.Workflow = new (function Workflow() {
    var me = this;
    this.ActionType = {
        Lost: {
            RequestName: 'Báo mất thiết bị',
            Mapping: [
                {
                    RequestName: 'nyc',
                    DataName: 'CurrentUser'
                },
                {
                    RequestName: 'nyc',
                    DataName: 'CurrentUser'
                }
            ]
        },
        Return: {
            RequestName: 'Trả thiết bị'
        },
        Damage: {
            RequestName: 'Hư hỏng thiết bị'
        },
        Replace: {
            RequestName: 'Thay thế thiết bị'
        },
        Order: {
            RequestName: 'Yêu cầu tài sản'
        },
        OrderOtherAsset: {
            RequestName: 'Mua thiết bị'
        }
    };
    this.requestList = new (function List() {
        var list = this;
        var data = [];
        var currentIndex = -1;
        this.Add = function (args) {
            if (Array.isArray(args)) {
                data = data.concat(args);
            } else {
                data.push(args);
            }
        };
        this.Next = function () {
            currentIndex++;
            if (currentIndex > (data.length - 1)) {
                return null;
            } else {
                return data[currentIndex];
            }
        };
        this.Has = function () {
            return data.length > 0;
        };
        this.HasNext = function () {
            return (currentIndex + 1) <= (data.length - 1);
        };
        _prop('length', function () {
            return data.length;
        });
    })();
    this.mess = [];
    var onDone = null;

    this.getTableData = function (request, isFull) {
        var tableData = request.TableData;
        tableData = tableData.groupBy(function (e) {
            return {
                AssetType: e[me.getTableFieldName(request, 'assetType', 0)],
                AssetModel: e[me.getTableFieldName(request, 'assetModel', 1)],
                AssetName: e[me.getTableFieldName(request, 'asset', 2)]
            };
        }, function (e) {
            if (e.Count) {
                e.Count = e.Count + 1;
            } else {
                e.Count = 1;
            }
            return e;
        });
        request.TableCount = tableData.length;
        if (isFull) {
            return tableData;
        } else {
            return tableData.slice(0, 3);
        }
    };

    this.getTableFieldName = function (request, type, fieldIndex) {
        var field = request.FieldDefinitions.find(function (e) {
            return e.RequestDataTypeID === APIData.Workflow.RequestDataTypeID[type];
        });
        if(field === undefined){
            return 'Field'+fieldIndex;
        }

        return field.MappingName;
    };

    this.getTableField = function (request, model, type) {
        var field = request.FieldDefinitions.find(function (e) {
            return e.RequestDataTypeID === APIData.Workflow.RequestDataTypeID[type];
        });

        if (field === undefined || field === null || field.length === 0) {
            return '';
        }

        return model[field.MappingName];
    };

    this.getPendingDate = function (date) {
        var actionDate = new Date(date);
        var currentDate = new Date();
        var prefix = '{0}';
        var dateDiff = actionDate.diff(currentDate);
        if (dateDiff.hours == 0 && dateDiff.minutes == 0) {
            prefix = 'Vừa mới đấy';
        } else if (dateDiff.hours == 0 && dateDiff.minutes != 0) {
            prefix = 'Khoảng {0} phút trước';
            dateDiff.days = dateDiff.minutes;
        } else if (dateDiff.hours < 24) {
            prefix = 'Khoảng {0} giờ trước';
            dateDiff.days = dateDiff.hours;
        } else {
            dateDiff.days = Math.ceil(dateDiff.hours / 24);
            prefix = 'Khoảng {0} ngày trước';
        }
        return prefix.format(dateDiff.days);
    };

    this.getOwnedField = function (request) {

        var fieldName = request.FieldDefinitions.find(function (e) {
            return e.Name === request.Definition.OwnerField;
        });

        return request.Fields[fieldName.MappingName];
    };

    var createRequestListItem = function (listChild) {
        var item = {};
        for (var i = 0; i < listChild.length; i++) {
            item[listChild[i].Name] = null;
        }

        return item;
    };

    var createOneWithMappingData = function (_reuqestList) { //actionType, data, _cb) {
        if (_reuqestList.HasNext()) {
            var item = _reuqestList.Next();
            var actionType = item.actionType;
            var data = item.data;
            var requestData = new APIData.Request.RequestData();
            var wfID = 0;
            var mappingRes = [];
            APIData.RequestFeatureMapping.GetByName(actionType, function (res) {
                res = JSON.parse(res);
                wfID = res.RequestID;
                mappingRes = res;
                requestData.loadFieldDefinition(wfID, function () {
                    if (mappingRes !== null && mappingRes != 'null') {
                        var mapping = JSON.parse(mappingRes.Mapping);
                        for (var i = 0; i < mapping.length; i++) {
                            var field = null;
                            if (Array.isArray(requestData.listFieldDefinition)) {
                                field = requestData.listFieldDefinition.find(function (e) {
                                    return e.Name === mapping[i].FieldName;
                                });
                            } else {
                                field = requestData.listFieldDefinition[mapping[i].FieldName];
                            }

                            if (field.listChild !== undefined) {
                                field.value = [];
                                var mappingData = data[mapping[i].MappingName];
                                console.log(data);
                                for (var j = 0; j < mappingData.length; j++) {
                                    var fieldItem = createRequestListItem(field.listChild);
                                    for (var name in fieldItem) {
                                        var mappingChild = mapping[i].Detail.find(function (e) {
                                            return e.FieldName === name;
                                        });
                                        if (mappingChild !== null && mappingChild !== undefined) {
                                            fieldItem[name] = data[mapping[i].MappingName][j][mappingChild.MappingName];
                                        }
                                    }

                                    field.value.push(fieldItem);
                                }

                            } else {
                                field.value = data[mapping[i].MappingName];
                            }
                        }
                        //console.log(requestData.listFieldDefinition);
                        //return;
                        requestData.createRequest(function (res) {
                            if (res.code === AMResponseCode.ok) {
                                me.mess.push({
                                    mess: "Yêu cầu của bạn đã được tạo thành công",
                                    response: res.data,
                                    result: true
                                });

                            } else {
                                me.mess.push({
                                    mess: "Yêu cầu của bạn chưa được tạo, vui lòng liên hệ quản trị viên",
                                    response: requestData,
                                    result: false
                                });
                            }
                            if (me.mess.length === me.requestList.length) {
                                onDone(me.mess);
                            }
                            createOneWithMappingData(_reuqestList);
                        });
                    }
                });
            });
        } else {
            return;
        }
    };

    this.createWithMappingData = function (requests, callback) {
        me.requestList.Add(requests);
        onDone = callback;
        createOneWithMappingData(me.requestList);
    }
})();