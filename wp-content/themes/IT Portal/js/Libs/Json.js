/**
 * Created by Administrator on 2/28/2017.
 * Lib: Json
 */
Libs.Json = new (function(){
  this.toJson = function(obj){
    JSON.stringify(obj);
  };
  this.toObject = function(str) {
    JSON.parse(str);
  };
})();