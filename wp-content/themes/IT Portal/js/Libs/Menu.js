/**
 * Created by tientm2 on 9/12/2017.
 */
var Menu = Menu || {};
Menu.AdminMenu = new (function AdminMenu(){
    var me = this;
    this.isActive = function (link) {
        var path = location.pathname + location.search;

        var result = (path.replace(/^\//,'').replace(/\/$/,'') === link.replace(/^\//,'').replace(/\/$/,''))?'active':'';
        return result;
    }
})();