/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//TODO Chuyển các định nghĩa prototype vào folder riêng và rip trong phần rip js
if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}
if (!String.prototype.formatMoney) {
    String.prototype.formatMoney = function () {
        return this.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };
}
if (!Number.prototype.formatMoney) {
    Number.prototype.formatMoney = function () {
        return this.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };
}
if (!Date.prototype.diff) {
    Date.prototype.diff = function (date) {
        var diff = date - this;

        var hours = Math.floor(diff / 3.6e6);
        var minutes = Math.floor((diff % 3.6e6) / 6e4);
        var seconds = Math.floor((diff % 6e4) / 1000);
        return {
            hours: hours,
            minutes: minutes,
            seconds: seconds,
        }
    }
}
if (!Date.prototype.format) {
    Date.prototype.format = function (pattern) {
        pattern = pattern.replace(/MM/g, (this.getMonth()+1).toString().padStart(2, '0'));
        pattern = pattern.replace(/DD/g, this.getDate().toString().padStart(2, '0'));
        pattern = pattern.replace(/YYYY/g, this.getFullYear().toString().padStart(2, '0'));
        pattern = pattern.replace(/hh/g, this.getHours().toString().padStart(2, '0'));
        pattern = pattern.replace(/mm/g, this.getMinutes().toString().padStart(2, '0'));
        pattern = pattern.replace(/ss/g, this.getSeconds().toString().padStart(2, '0'));
        return pattern;
    }
}

Object.defineProperty(Object.prototype, 'merge', {
    enumerable: false,
    configurable: false,
    writable: false,
    value: function(obj2, ignore) {
        for (var attrname in obj2) {
            if((ignore && ignore.test(attrname)) || typeof obj2[attrname] === 'function'){
                continue;
            }
            this[attrname] = obj2[attrname];
        }
    }
});
if (!Array.prototype.groupBy) {
    Array.prototype.groupBy = function(fkey, fgroup) {
        if(Array.isArray(this)) {
            return Object.values(this.reduce(function (result, value) {
                result = result || {};

                var key = fkey(value);
                var keyStr = JSON.stringify(key);
                var reVal = (result[keyStr] !== undefined) ? result[keyStr] : key;
                reVal = fgroup(reVal);
                result[keyStr] = reVal;
                return result;
            }, {}));
        } else {
            return null;
        }
    };
}

Object.defineProperty(Array.prototype, 'unique', {
    enumerable: false,
    configurable: false,
    writable: false,
    value: function() {
        var a = this.concat();
        for(var i=0; i<a.length; ++i) {
            for(var j=i+1; j<a.length; ++j) {
                if(a[i] === a[j])
                    a.splice(j--, 1);
            }
        }

        return a;
    }
});
//TODO Review và xóa các hàm không cần dùng
Common = window.Common || {};
Common = {
    clone: function (obj) {
        var result = {};
        for (var name in obj) {
            result[name] = obj[name];
        }
        return result;
    },
    BaseUrl: "/am_ajax/",
    getAllAttr: function (eml) {
        var attributes = {};
        $.each(eml.attributes, function( index, attr ) {
            attributes[ attr.name ] = attr.value;
        } );
        return attributes;
    },
    build_param: function (_vars) {
        var params = Common.getUrlParameters();
        for (var key in _vars) {
            if (_vars[key] === '#ignore') {
                delete params[key];
            } else {
                params[key] = _vars[key];
            }
        }
        var queryString = '';
        for (var key in params) {
            if(typeof(params[key]) === 'function'){
                continue;
            }
            queryString += key + '=' + params[key] + '&';
        }
        return '?' + queryString.substring(0, queryString.length - 1);
    },
    reBinding: function (container, model) {
        ko.cleanNode(container[0]);
        ko.applyBindings(model, container[0]);
    },
    loadPage: function (container, url, callBack) {
        $(container).load(url, callBack);
    },
    callBackDynamic: function (func) {
        if (func !== null && func !== undefined && func !== "" && typeof func === 'function') {
            var args = [];
            func.apply(this, arguments[1]);
        }
    },
    runSynchronize: function (process, params) {
        $.ajaxSetup({async: false});
        self.callBack(process(params));
        $.ajaxSetup({async: true});
    },
    getRealWidth: function (element) {
        return $(element).width() + Common.getTotalPadding(element);
    },
    getTotalPadding: function (element) {
        return $(element).css('pading-left') + $(element).css('pading-right');
    },
    generateUUID: function () {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    },
    getRandomName: function (prefix, nameLength) {
        var keys = 'zaq12wsxcde34rfvbgt56yhnmju78iklo90pZAQWSXCDERFVBGTYHNMJUIKLOP';
        var result = '' + prefix;
        for (var i = 0; i < nameLength; i++) {
            result += keys[Common.random(0, keys.length)];
        }
        return result;
    },
    random: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    getDateToString: function (date, pattern) {
        if (Common.isNull(date)) {
            return "-";
        }
        var processDate = Common.getValueOfObject(date);
        if (Common.isNull(pattern)) {
            pattern = "MM/DD/YYYY";
        }
        var output = "";
        if (processDate) {
            output = moment(processDate).format(pattern);
        }
        return output;
    },
    convertToByte: function (str) {
        var bytes = [];
        for (var i = 0; i < str.length; ++i) {
            bytes.push(str.charCodeAt(i));
        }
        return bytes;
    },
    log: function (obj) {
        if ($.isFunction(obj)) {
            console.log('Object is function');
            console.log(obj());
        } else {
            console.log('Object is plan JS');
            console.log(obj);
        }
    },
    removeItemFromArray: function (item, arr) {

        for (var i = arr.length; i--;) {
            if (arr[i] === item) {
                arr.splice(i, 1);
                break;
            }
        }
    },
    pushItemToArray: function (item, arr) {
        Common.getValueOfObject(arr).push(item);
    },
    GetIndexOnArr: function (prop, value, array) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][prop] === value) {
                return i;
            }
        }
        return -1;
    },
    getIndexOfObject: function (item, arr) {
        arr = Common.getValueOfObject(arr);
        for (var i = 0; i < arr.length; i++) {
            if (Common.compareObject(item, arr[i])) {
                return i;
            }
        }
        return -1;
    },
    getValueOfObject: function (obj) {
        if ($.isFunction(obj)) {
            return Common.getValueOfObject(obj());
        } else {
            return obj;
        }
    },
    setValueOfObject: function (obj, value) {
        if ($.isFunction(obj)) {
            if ($.isFunction(obj())) {
                Common.setValueOfObject(obj(), value);
            } else {
                obj(value);
            }
        } else {
            obj = value;
        }
    },
    checkValueExistedInObject: function (value, obj) {
        var result = false;
        try {
            var props = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < props.length; i++) {
                var propName = props[i];
                var objectValue = Common.getValueOfObject(obj[propName]).toString();
                //console.log(propName+" : "+objectValue);               
                if (objectValue.indexOf(value) !== -1) {
                    result = true;
                    break;
                }
            }

        } catch (e) {

        }
        //console.log("Result: "+result);
        return result;

    },
    compareObject: function (a, b) {
        var aProps = Object.getOwnPropertyNames(a);
        var bProps = Object.getOwnPropertyNames(b);
        if (aProps.length !== bProps.length) {
            return false;
        }
        for (var i = 0; i < aProps.length; i++) {
            var propName = aProps[i];

            // If values of same property are not equal,
            // objects are not equivalent
            if (a[propName] !== b[propName]) {
                return false;
            }
        }
        // If we made it this far, objects
        // are considered equivalent
        return true;
    },
    clonePlainObject: function (obj) {
        var json = Common.toJSON(obj);
        return Utils.jsonToData(json);
    },
    tryToPlainObject: function (obj) {
        if ($.isPlainObject(obj)) {
            return obj;
        } else {
            try {
                return JSON.parse(obj);
            } catch (e) {
                return obj;
            }
        }

    },
    toJSON: function (obj) {
        return ko.toJSON(obj);
    },
    getKoObject: function (json) {
        return ko.mapping.fromJSON(json);
    },
    isNumber: function (obj) {
        obj = Common.getValueOfObject(obj);
        try {
            return $.isNumeric(obj);
        } catch (e) {
            return false;
        }
    },
    iJsonString: function(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    },
    getCurrentUser: function () {
        return Common.getCookie("assetusername");
    },
    callBack: function (func) {
        if (func && !Common.isNull(func) && !Common.isNull(func.name)) {
            //  console.log("Common: Call back function: " + func.name);
        }
        if (func && func !== null && func !== undefined && func !== "" && typeof func === 'function') {
            var args = [];
            for (var i = 1; i < arguments.length; i++) {
                args.push(arguments[i]);
            }
            func.apply(this, args);
        }
    },
    getCallBack: function (func) {
        if (func && !Common.isNull(func) && !Common.isNull(func.name)) {
            // console.log("Common: Call back function: " + func.name);
        }
        if (func && func !== null && func !== undefined && func !== "" && typeof func === 'function') {
            var args = [];
            for (var i = 1; i < arguments.length; i++) {
                args.push(arguments[i]);
            }
            return func.apply(this, args);
        }
        return func;
    },
    checkAdminRole: function (callback, role) {
        if (Common.isNull(role)) {
            role = "admin";
        }
        if (!Common.processCheckRole('default', role)) {
            RedirectPages.AccesDenined();
        } else {
            Common.callBack(callback);
        }
    },
    checkAdmin: function () {
        try {
            var assetroles = Common.getCookie("assetroles");
            var roles = JSON.parse(assetroles);
            for (var j = 0; j < roles.length; j++) {
                if (roles[j].name.toUpperCase() === 'ADMIN') {
                    return true;
                }
            }
            return false;
        } catch (e) {
            return false;
        }


    },
    processCheckRole: function (checkingUserName, role) {
        //console.log("Check user " + checkingUserName + " with role is " + role);
        if (role === 'all') {
            return true;
        } else {
            var userName = Common.getCurrentUser();
            if (checkingUserName === 'default') {
                checkingUserName = Common.getCurrentUser();
            }
            checkingUserName = checkingUserName.trim();
            var check = false;
            if (userName)
                if ((role === 'user' || role === 'User') && userName.toLowerCase() === checkingUserName.toLowerCase()) {
                    check = true;
                } else if (userName === checkingUserName) {
                    if (Common.checkAdmin()) {
                        check = true;
                    } else if (Common.isNull(role)) {
                        check = true;
                    } else {
                        var assetroles = Common.getCookie("assetroles");

                        if (!Common.isNull(assetroles)) {
                            var roles = JSON.parse(assetroles);
                            var checkingRole = Common.getValueOfObject(role);
                            var bindRoles = checkingRole.split(",");

                            for (var i = 0; i < bindRoles.length; i++) {
                                for (var j = 0; j < roles.length; j++) {
                                    if (bindRoles[i].toUpperCase() === roles[j].name.toUpperCase()) {
                                        check = true;
                                        break;
                                    }
                                }
                                if (check) {
                                    break;
                                }
                            }
                        }
                    }
                }
            //console.log("Result: "+check);
            return check;
        }

    },
    signout: function () {
        Common.deleteCookie('assetuser');
        Common.deleteCookie('assetusername');
        Common.deleteCookie('assetroles');
        ApplcationManager.loadMainContent("main/login");
        Utils.loadLoginInfo();
    },
    isNotNull: function (obj) {
        return !Common.isNull(obj);
    },
    isNull: function (obj) {
        //obj = Common.getValueOfObject(obj);
        if (obj === null || obj === undefined || obj === "" || obj === "null" || obj === 'undefined') {
            return true;
        } else {
            return false;
        }
    },
    checkRealtimeKioshTransaction: function () {
        if (!Common.isNull(MODEL)) {
            var isKioshTransaction = Common.getValueOfObject(MODEL.iskiotTransaction);
            if (isKioshTransaction !== null && isKioshTransaction) {
                return true;
            }
        }
        return false;
    },
    parseBoolean: function (obj) {
        obj = Common.getValueOfObject(obj);
        if (!Common.isNull(obj)) {
            if (obj > 0 || obj.toString().toLowerCase() === 'true') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    },
    getUrlParameters: function () {
        var vars = [], hash;
        var fullPath = $(location).attr('search');
        var hashes = fullPath.slice(fullPath.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            if(hash[0] === ''){
                continue;
            }
            vars[hash[0]] = (hash[1] === undefined)?'':hash[1];
        }
        return vars;
    },
    encrypt: function (value) {
        //
        var encrypted = CryptoJS.AES.encrypt(value, encryptKey);

        return encrypted;
    },
    decrypt: function (encrypted) {
        //
        var decrypted = CryptoJS.AES.decrypt(encrypted, encryptKey);
        return decrypted.toString(CryptoJS.enc.Utf8);
    },
    setCookie: function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        var encryptedValue = Common.encrypt(cvalue);
        //
        //console.log("un encrypted: cookie: "+cvalue);
        //console.log("encrypted: cookie: "+encryptedValue);
        document.cookie = cname + "=" + encryptedValue + "; " + expires;
    },
    getCookie: function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ')
                c = c.substring(1);
            if (c.indexOf(name) !== -1) {
                var result = Common.decrypt(c.substring(name.length, c.length));
                result = result.replace(/&quot;/g, '"');
                //
                //console.log(cname+" : "+result);
                return result;
            }

        }
        return "";
    },
    deleteCookie: function (szName) {
        Common.setCookie(szName, '', -1);

    },
    executeApiWithCalbackOnly: function (module, subPath, method, queryString, successCallback) {
        var opt = {
            success: successCallback
        };
        HttpUtils.CallAjaxWithQueryString(method, module, subPath, queryString, opt);
    },
    wait: function(_expr, todo){
        var expr = _expr;
        var letWait = setInterval(function () {
            if(typeof expr === 'function'){
                if(expr() === true) {
                    clearInterval(letWait);
                    todo();
                }
            } else {
                if(expr) {
                    clearInterval(letWait);
                    todo();
                }
            }
        }, 100);
    }
};
var CommonModelFunctions = {
    login: {
        username: "",
        role: {}
    },
    currentRole: {
        id: ko.observable(0),
        description: ko.observable(""),
        isDelete: ko.observable(false),
        name: ko.observable("")
    },
    checkBooleanBinding: function (value) {
        return Common.parseBoolean(value);
    },
    checkArrayBinding: function (arr) {
        var realValue = Common.getValueOfObject(arr);
        if (Common.isNull(realValue)) {
            return false;
        } else if (realValue.length > 0) {
            return true;
        }
        return false;
    },
    checkIsComponentAsset: function (assetItem) {
        var detail = Common.tryToPlainObject(Common.getValueOfObject(assetItem.resDetail()));
        if (detail) {
            var detailComponent = Common.getValueOfObject(detail.IsComponent);
            return detailComponent;
        }
        return false;
    },
    checkObjectBinding: function (obj) {
        obj = Common.getValueOfObject(obj);
        if (Common.isNull(obj)) {
            return false;
        }
        return true;

    },
    getMessageSource: function (key) {
        return i18n[key];
    },
    urlParameters: ko.observable({}),
    getDateTime: function (dateNumber) {
        var date = new Date(dateNumber);
        return date;
    },
    closePopup: function () {
        $(".modal").modal('hide');
        POPUP = null;
        try {
            Webcam.reset();
        } catch (e) {
        }
    },
    closeImageReviewPopup: function () {
        $('#popup-image-container').hide();
    },
    closeConfirm: function () {
        $('#confirm-popup-container').hide();
    },
    processChangeDataTable: function (action, table, timeout) {
        if ($.fn.DataTable.isDataTable(table)) {
            //$(table).DataTable().destroy();

        }
        Common.callBack(action);
        if (Common.isNull(timeout)) {
            timeout = 500;
        }
//        setTimeout(function () {
//            $(table).DataTable();
//        }, timeout);
    }
};
Utils = new function () {

    var t = this;

    var URLParams = {};

    t.getURLValue = function (name) {
        if (URLParams.hasOwnProperty(name))
            return URLParams[name];

        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);

        var re;
        if (!results)
            re = null;
        else if (!results[2])
            re = '';
        else
            re = decodeURIComponent(results[2].replace(/\+/g, " "));

        URLParams[name] = re;
        return re;
    };
    t.getURLValues = function (name) {
        var oGetVars = {};
        if (window.location.search.length > 1) {
            for (var aItKey, nKeyId = 0, aCouples = window.location.search.substr(1).split("&"); nKeyId < aCouples.length; nKeyId++) {
                aItKey = aCouples[nKeyId].split("=");
                oGetVars[decodeURIComponent(aItKey[0])] = aItKey.length > 1 ? decodeURIComponent(aItKey[1]) : "";
            }
        }
        URLParams  = oGetVars;
        return oGetVars;
    };
    t.setURLValue = function (name, value) {
        var values = {};
        values[name] = value;
        t.setURLValues(values);
    }
    t.setURLValues = function (values) {
        var url = window.location.href;
        var hash = location.hash;
        url = url.replace(hash, '');

        for(var name in values){
            var value = values[name];
            var i = url.indexOf("&" + name + "=");
            if(i == -1)
                i = url.indexOf("?" + name + "=");
            if (i != -1) {
                var prefix = url.substring(0, i + 1);
                var iEnd = url.indexOf("&", i + 1) + 1;
                var suffix = iEnd ? url.substring(iEnd) : "";

                if(value == null){
                    if(suffix)
                        url = prefix + suffix;
                    else
                        url = prefix.substr(0, prefix.length - 1);
                }else{
                    url = prefix + name + "=" + value;
                    if(suffix)
                        url = url + "&" + suffix;
                }
            } else {
                if (url.indexOf("?") < 0) {
                    if (value != null)
                        url += "?" + name + "=" + value;
                }
                else {
                    if (value != null)
                        url += "&" + name + "=" + value;
                }
            }

            URLParams[name] = value;
        }

        if (location.href.endsWith(url) == false)
            history.pushState(null, document.title, url);
    }
    t.makeMapForKoArray = function (koArray, mapName, keyName, valueName) {
        var map = {};
        koArray.subscribe(function (data) {
            for(proName in map)
                map[proName] = null;

            koArray().forEach(function (item) {
                map[item[keyName]] = item[valueName];
            });
        });
        koArray[mapName] = map;
    }
    t.isDate = function(d){
        return d!=null && d.getTime != null && (isNaN(d.getTime()) == false);
    }
    t.StringToCodeName = function (str) {
        if (str == null)
            return null;

        str = str.trim().replace(/ +/g, "_").toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        return str;
    }

    t.jsonToData = function (jsonData) {
        try {
            return JSON.parse(jsonData);
        } catch (ex) {
            console.warn(ex);
            console.warn(jsonData);
            return null;
        }
    };
    t.dataToJson = function (obj) {
        return JSON.stringify(obj);
    };
    t.removeFromArray = function (array, element) {
        var i = array.indexOf(element);
        if(i != -1)
            array.splice(i, 1);

        return i;
    }
//---------------------------------------------------




    // kiểm tra chuỗi, nếu dài hơn max ký tự thì trả về thông báo, ngược lại trả về null
    t.ckStrMax = function (name, str, max) {
        if (max == null)
            max = 50;

        if (str != null && str.length > max)
            return "<br>" + name + " không thể dài hơn " + max + " ký tự";

        return "";
    }

    t.ckStrNullMax = function (name, str, max) {
        if (max == null)
            max = 50;

        if (str == null || str == "")
            return "<br>" + name + " không thể bỏ trống";
        else
            return t.ckStrMax(name, str, max);
    }

    t.ckNumNull = function (name, num) {
        if (num == null)
            return "<br>" + name + " không thể bỏ trống";
        else if (isNaN(num))
            return "<br>" + name + " không hợp lệ";
        else
            return "";
    }

    t.ckNull = function (name, num) {
        if (num == null)
            return "<br>" + name + " không thể bỏ trống";
        else
            return "";
    }


//---------------------------------------------------

    t.getRowsOfTable = function (table) {
        return $(table).find('tbody tr');
    };

    t.getAllPermissions = function () {
        var permission = {
            'assetListID.resTypeID': {
                e: ko.observable(true)
            },
            'assetListID.resModelID': {
                e: ko.observable(true)
            },
            'assetListID.resID': {
                e: ko.observable(true)
            },
            'addAsset': {
                e: ko.observable(true)
            },
            'reasonID.name': {
                e: ko.observable(true)
            },
            'userInformationId.username': {
                e: ko.observable(true)
            },
            'userInformationId.department': {
                e: ko.observable(true)
            },
            'userInformationId.phone': {
                e: ko.observable(true)
            },
            'userInformationId.location': {
                e: ko.observable(true)
            },
            'userInformationId.email': {
                e: ko.observable(true)
            }
        };
        return permission;
    };
    t.getEditTableAttributes = function (attribute) {
        var result = [];
        attribute = Common.getValueOfObject(attribute);
        for (var i = 0; i < attribute.User.length; i++) {
            if (Common.parseBoolean(attribute.User[i].editable)) {
                result.push(attribute.User[i].value);
            }
        }
        for (var i = 0; i < attribute.PR.length; i++) {
            if (Common.parseBoolean(attribute.PR[i].editable)) {
                result.push(attribute.PR[i].value);
            }
        }
        for (var i = 0; i < attribute.Asset.length; i++) {
            if (Common.parseBoolean(attribute.Asset[i].editable)) {
                result.push(attribute.Asset[i].value);
            }
        }
        return result;
    };
    t.getChangesOfTwoString = function (oldStr, newStr) {
        if (!Common.isNull(oldStr) && !Common.isNull(newStr)) {
            var splitinput = oldStr.split("\n");
            var splitoutput = newStr.split("\n");
            var inlines = splitinput.length;
            var outlines = splitoutput.length;
            var lines;
            if (outlines > inlines) {
                lines = outlines;
            }
            else {
                lines = inlines;
            }
            var buildoutput = "";
            for (i = 0; i < lines; i++) {
                var testundefined = false;
                if (splitinput[i] == undefined) {
                    buildoutput = buildoutput + diffString("", splitoutput[i]);
                    testundefined = true;
                }

                if (splitoutput[i] == undefined) {
                    buildoutput = buildoutput + diffString(splitinput[i], "");
                    testundefined = true;
                }

                if (testundefined == false) {
                    buildoutput = buildoutput + diffString(splitinput[i], splitoutput[i]);
                }
            }

            return buildoutput;
        }
        return newStr;
    };
    t.getColumsOfARow = function (row) {
        return $(row).find('td');
    };
    t.buildChangedOfTwoObject = function (a, b) {
        if (!Common.isNull(a) && !Common.isNull(b)) {
            var aProps = Object.getOwnPropertyNames(a);
            //var bProps = Object.getOwnPropertyNames(b);
            for (var i = 0; i < aProps.length; i++) {
                var propName = aProps[i];
                var aAttribute = a[propName];
                var bAttribute = b[propName];
                if (!Common.isNull(aAttribute) && !Common.isNull(bAttribute)) {
                    if ($.isPlainObject(aAttribute) && $.isPlainObject(bAttribute)) {
                        Utils.buildChangedOfTwoObject(aAttribute, bAttribute);
                    } else if ($.isArray(aAttribute) || $.isArray(bAttribute)) {
                        //code later if needed
//                    if ($.isArray(bAttribute)) {
//                        var alen = aAttribute.length;
//                        var blen = aAttribute.length;
//                        if (alen === blen) {
//
//                            for (var e = 0; e < alen; e++) {
//                                Utils.buildChangedOfTwoObject(aAttribute[e], bAttribute[e]);
//                            }
//                        } else if (alen < blen) {
//
//                            for (var e = 0; e < alen; e++) {
//                                Utils.buildChangedOfTwoObject(aAttribute[e], bAttribute[e]);
//                            }
//                            for (var f = alen; f < blen; f++) {
//                                bAttribute[f] = Utils.getChangesOfTwoString("", bAttribute[f].toString());
//                            }
//                        } else {
//
//                            for (var e = 0; e < blen; e++) {
//                                Utils.buildChangedOfTwoObject(aAttribute[e], bAttribute[e]);
//                            }
//                        }
//                    }
                    } else {
                        if (!Common.isNull(bAttribute)) {
                            var rootValue = "-";
                            if (!Common.isNull(aAttribute)) {
                                rootValue = aAttribute.toString();
                            }
                            b[propName] = Utils.getChangesOfTwoString(rootValue, bAttribute.toString());
                        }
                    }
                }
            }
        }

    };
    t.detectTableContentChanges = function (oldTable, newTable) {
        var returnNewTable = $(newTable);
        if ($(oldTable).html() !== $(returnNewTable).html()) {
            var newRows = Utils.getRowsOfTable(returnNewTable);
            var oldRows = Utils.getRowsOfTable(oldTable);
            var rowsLength = oldRows.length;
            for (var i = 0; i < rowsLength; i++) {
                var oldCols = Utils.getColumsOfARow(oldRows[i]);
                var newCols = Utils.getColumsOfARow(newRows[i]);
                var totalCols = oldCols.length;
                for (var j = 0; j < totalCols; j++) {
                    var oldValue = $(oldCols[j]).html();
                    var newValue = $(newCols[j]).html();
                    var changedText = Utils.getChangesOfTwoString(oldValue, newValue);
                    $(newCols[j]).html(changedText);
                }
            }
        }
        var container = $('<div></div>').append($(returnNewTable));
        return $(container).html();

    };
    t.builSubFunction = function (options, container) {
        if (!Common.isNull(options)) {
            $('.asset-sub-function').remove();
            var subFunctionDiv = $('<div class="asset-sub-function asset-text-align-right"></div>');
            var listItems = [];
            var item = null;

            if (!Common.isNull(options.view)) {
                item = $('<span class="glyphicon glyphicon-picture asset-color-blue asset-sub-item" aria-hidden="true"></span>');
                $(item).click(function () {
                    Common.callBack(options.view);
                });
                $(item).attr('title', 'View this item');
                listItems.push(item);
            }
            if (!Common.isNull(options.delete)) {
                item = $('<span class="glyphicon glyphicon-remove asset-color-red asset-sub-item" aria-hidden="true"></span>');
                $(item).click(function () {
                    // var content = CommonModelFunctions.getMessageSource('warning_confirm_box_delete_item');
                    //PopupBusiness.viewConfirm(options.delete, content);
//                    var deleteFunction = function () {
//                        Common.callBack(options.delete);
//                    };
                    //PopupBusiness.viewConfirm(options.delete);
                    Common.callBack(options.delete);
                });

                $(item).attr('title', 'Delete this item');
                listItems.push(item);
            }
            for (var i = 0; i < listItems.length; i++) {
                subFunctionDiv.append(listItems[i]);
            }
            $(subFunctionDiv).width($(container).width());
            //$(subFunctionDiv).prependTo(container);
            $(container).prepend(subFunctionDiv);
        }
    };
    t.loading = function () {
        //var spinner= '<div id="loading-container"><div class="spinner asset-center_screen"></div></div>';
        var load = '<div id="loading-container" class="wys_default_tranparent_bg wys_full_screen">'

            + '<div class="asset-center_screen" style="width:300px; height:150px">'
            + '<div id="loading-container"><div class="spinner asset-center_screen"></div></div>'

            + '<div class="progress asset_progress">'
            + '<div id="asset_main_progress_bar" class="progress-bar progress-bar-info" role="progressbar" data-transitiongoal="60">'
            + '</div>'
            + '</div>'
            + '</div>'
            + '</div>';
        var loadingId = $('#loading-container').attr('id');
        if (Common.isNull(loadingId)) {
            $('body').append(load);
        }
    };
    t.buildAlert = function (content, targetId) {
        var result = '<div class="asset-vertical-middle asset_center_text">'
            + '<div class="alert alert-warning">' + content + '</div>'

            + '</div>';
        $('#' + targetId).html(result);
    };
    t.checkImageIsBase64 = function (imageData) {
        var realValue = Common.getValueOfObject(imageData);
        if (realValue.indexOf('base64') !== -1) {
            return true;
        }
        return false;
    };
    t.removeLoading = function () {
        $('#loading-container').remove();
        // console.log('Data loaded!');

    };
    t.loadLoginInfo = function () {
        var cookieUser = Common.getCookie('assetuser');
        if (cookieUser !== "") {
            $('#header-login_detail').html(cookieUser);
            $('#header-login_signout').show();
            var urlInfos = Common.getUrlParameters();
            if (urlInfos.view) {
                ApplcationManager.loadMainContent(urlInfos.view);
            } else {
                ApplcationManager.loadMainContent("main/defaultView");
            }
        } else {
            $('#header-login_detail').html('');
            $('#header-login_signout').hide();
            $('#panel-heading').html(LoginInfomation.Message());

        }
    };
    t.displayInformation = function (infos) {
        $('#panel-heading').html(infos);
    };
    t.buildAssetDetail = function (assetObject) {

        var realValue = Common.getValueOfObject(assetObject);
        var totals = realValue.length;
        var details = totals;
        for (var m = 0; m < totals; m++) {
            details += "-";
            var props = Object.getOwnPropertyNames(realValue[m]);
            for (var i = 0; i < props.length; i++) {
                var propName = props[i];
                var value = realValue[m][propName];
                if (i === props.length - 1) {
                    details += value;
                } else {
                    details += value + ":";
                }
            }
        }
        return details;
    };
    t.buildFormStepAttributes = function (formStepAttributes, attributeName) {
        Utils.buildFormStepAttributePart(formStepAttributes.Asset, attributeName);
        Utils.buildFormStepAttributePart(formStepAttributes.PR, attributeName);
        Utils.buildFormStepAttributePart(formStepAttributes.User, attributeName);
    };
    t.buildFormStepAttributePart = function (arrayValues, attributeName) {
        for (var i = 0; i < arrayValues.length; i++) {
            if (arrayValues[i].value === attributeName) {
                arrayValues[i].editable(true);
            }
        }
    };
    t.displayMainProgressBarStatus = function (percent) {
        $('#asset_main_progress_bar').css('width', percent + '%').attr('aria-valuenow', percent).html(percent + '%');

    };

};

function EditAndValidate(data) {
    var t = this;
    var msg = [];

    t.addMsg = function(str){
        if(str)
            msg.push("<br>", str);
    }
    t.getMessage = function () {
        if (msg.length == 0)
            return "";

        msg.shift();
        return msg.join("");
    }
    t.string = function (proName, name, min, max) {
        var val = data[proName];

        // validate type
        if (val == null) {
            min != null && msg.push("<br>", name, " không thể bỏ trống");
            return;
        }

        if (typeof val != "string") {
            min != null && msg.push("<br>", name, ": dữ liệu không hợp lệ");
            data[proName] = null;
            return;
        }

        // validate length
        val = val.trim();
        data[proName] = val;
        var l = val.length;
        if (min != null) {
            if (l < min) {
                msg.push("<br>", name, " không thể ít hơn ", min, " ký tự");
                return;
            } else if (l == 0) {
                msg.push("<br>", name, " không thể bỏ trống");
                return;
            }
        }

        if (max && l > max)
            msg.push("<br>", name, " không thể nhiều hơn ", max, " ký tự");

    }
    t.date = function (proName, name, min, max, minName, maxName) {
        var val = data[proName];

        // validate type
        if (val == null) {
            min != null && msg.push("<br>", name, " không thể bỏ trống");
            return;
        }

        if (typeof val != "object" || val.getTime == null || isNaN(val.getTime())) {
            min != null && msg.push("<br>", name, ": dữ liệu không hợp lệ");
            data[proName] = null;
            return;
        }

        // validate value
        if (min && val < min) {
            var mName = typeof minName == 'function' ? minName(min) : minName;
            msg.push("<br>", name, " không thể trước ", mName);
            return;
        }

        if (max && val > max) {
            var mName = typeof maxName == 'function' ? maxName(max) : maxName;
            msg.push("<br>", name, " không thể sau ", mName);
            return;
        }
    }
    t.int = function (proName, name, min, max, minName, maxName) {
        var val = data[proName];

        // validate type
        if (val == null) {
            min != null && msg.push("<br>", name, " không thể bỏ trống");
            return;
        }

        if (isNaN(val)) {
            if(val!=null && min != null){
                msg.push("<br>", name, ": dữ liệu không hợp lệ");
                return;
            }
        } else if(typeof val == "string"){
            val = parseInt(val);
            data[proName] = val;
        }

        // validate value
        if (min != null && val < min) {
            var mName = typeof minName == 'function' ? minName(min) : minName;
            msg.push("<br>", name, " không thể lớn hơn ", mName);
            return;
        }

        if (max != null && val > max) {
            var mName = typeof maxName == 'function' ? maxName(max) : maxName;
            msg.push("<br>", name, " không thể nhỏ hơn ", mName);
            return;
        }
    }
}

APP = window.APP || new function () {
        var t = this;
        t.currentUser = 'dev';
    };

