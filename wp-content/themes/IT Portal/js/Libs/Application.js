/**
 * Created by Administrator on 2/27/2017.
 */
function Application() {
    var me = this;
    var modules = [];
    var view = null;
    var model = null;
    var isBinding = false;

    this.currentLoginUser = '';
    this.currentLoginUserDomain = '';

    this.getModel = function () {
        return model;
    };

    this.reinstallModule = function () {
        modules = [];
    };

    this.registerModule = function (arr) {
        if (typeof arr != 'Array') {
            modules.push(arr);
        } else {
            for (var i = 0; i < arr.length; i++) {
                modules.push(arr[i]);
            }
        }
    };

    this.binding = function () {
        if (isBinding) {
            ko.cleanNode(document.body);
            $('*[data-bind]').each(function () {
                var bindingStr = $(this).attr('data-bind');

                if (bindingStr.indexOf('foreach') >= 0) {
                    var template = this.children[0].outerHTML;
                    this.innerHTML = template;
                }
            });
        }
        Promise.all([ko.applyBindings(model)]).then(function () {
            window['alreadyBinding'] = true;
            isBinding = true;
        });

    };

    this.do_Start = function () {
        if (Module.Model && Module.View) {
            model = new Module.Model();
            model._parent = me;
            view = new Module.View(model, function () {
                Component.System.preLoadingPanel.hide();
                me.binding();
            });
        } else {
            console.warn('Model and View is not defined');
            Component.System.preLoadingPanel.hide();
            me.binding();
        }
    };

    this.Start = function () {
        me.currentLoginUser = (function () {
            return wp_current_user;
        })();
        me.currentLoginUserDomain = (function () {
            return wp_current_user_domain;
        })();

        var cartDate = new Date(Libs.LocalStorage.val('cartDate'));

        if (cartDate !== null && cartDate.diff(new Date()).hours > 3) {
            Component.ShopCart.ResetCart();
            Component.AssetsCart.ResetCart();
        }

        Component.ShopCart.UpdateCart();

        $(document).ready(function () {
            me.do_Start();
        });
    }
}