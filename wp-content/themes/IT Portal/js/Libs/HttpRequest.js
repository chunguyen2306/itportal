/**
 * Created by Administrator on 11/21/2016.
 */
Libs = window.Libs || {};

Libs.Http = Libs.Http || { };

Libs.Http.exe = function (url, data, optionCallback, isAsync) {
    if(isAsync === undefined){
        isAsync = false;
    }
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        async: isAsync,
        success: function (data) {
            Libs.JS.callBack(optionCallback.success, data);
        },
        error: function (data) {
          Libs.JS.callBack(optionCallback.error, data);
        }
    });
};
Libs.Http.send = function(opt){
    opt.isAsync = opt.isAsync || true;
    $.ajax({
        type: opt.method,
        url: opt.url,
        data: opt.data,
        headers: opt.header,
        async: opt.isAsync,
        success: function (data) {
            if(opt.isDontParseJson){
                Libs.JS.callBack(opt.success, JSON.parse(data));
            } else {
                Libs.JS.callBack(opt.success, data);
            }

        },
        error: function (data) {
            if(opt.isDontParseJson){
                Libs.JS.callBack(opt.error, JSON.parse(data));
            } else {
                Libs.JS.callBack(opt.error, data);
            }
        }
    });
};
Libs.Http.getScript = function (url, optionCallback) {
    $.getScript( url, function( script, textStatus ) {
      Libs.JS.callBack(optionCallback.success, script);
    });
};










////////////////////////////////////////////////////////////////////////////////////
Module = {
    ITAAPService: "ITAPP",
    CMDB: "CMDB",
    LOGIN: "Login",
    Direct: "direct",
    ISOAPI: "ISOAPI",
    AAPWEB: "AAPWEB"
};
assetUrls = {itServiceBus: 'process'}
SubPath = {
    Asset:"asset",
    TransferForm: "transferForm",
    TransactionForm: 'transactionForm',
    requestForm: 'requestForm',
    DelegateForm: 'delegateForm',
    FormAssetListDetail: "formassetdetail",
    FormNote: "formnote",
    FormStepDefinition: "formstepdefinition",
    FormStep: "formstep",
    Form: "form",
    SystemLog: "systemLog",
    RecoveryForm: "recovery",
    UserRole: "userrole",
    Role: "role",
    FormReason: "formreason",
    FormType: "formtype",
    PrDetail: "pr",
    AssetImageDetail: "assetimage",
    FormstepPermission: "formsteppermission",
    AssetDetailHistory: "assetDetailHistory",
    AssetStatusHistory: "assetStatusHistory",
    MailTemplate: "mailTemplate",
    WebConfig: "webConfig",
    EmployeeTitle: "employeetitle",
    DefaultAsset: "defaultasset",
    TechnicianSite: "technician",
    AssetGeneralInfo: "assetGeneralInfo"

};
RedirectPages = {
    Error: "",
    clear: function () {
        MODEL = null;
        loadModel = null;
        objectBusiness = null;
        POPUP = null;
        objectPopup = null;
    },
    Undefined: function () {
        HEADING_TITLE = i18n.title_redirect_undefined_page;
        RedirectPages.clear();
        ApplcationManager.loadMainContent("system/notfound");
    },
    NotFound404: function () {
        HEADING_TITLE = i18n.title_redirect_notfound_page;
        RedirectPages.clear();
        ApplcationManager.loadMainContent("system/notfound");
    },
    AccesDenined: function () {
        HEADING_TITLE = i18n.title_redirect_access_denined;
        RedirectPages.clear();
        ApplcationManager.loadMainContent("system/acccessDenined");
    }
};
HttpUtils = {};
HttpUtils.viewCMDBResult = function (response, preventPopup) {
    var displayResult = "";
    if (Common.isNull(response)) {
        if (!Common.isNull(preventPopup) && !Common.parseBoolean(preventPopup)) {
            Popup.warning(_L.warning_undefined_result);
        }
        displayResult = "Operation isn't performed";

    } else {
        var responseObject = JSON.parse(response);
        if (!Common.isNull(responseObject.API)
            && !Common.isNull(responseObject.API.response)
            && !Common.isNull(responseObject.API.response.operation)
            && !Common.isNull(responseObject.API.response.operation.result)) {
            var message = responseObject.API.response.operation.result.message;
            var status = responseObject.API.response.operation.result.status;
            displayResult = "Message: " + message + " Status: " + status;
            if (!Common.isNull(preventPopup) && !Common.parseBoolean(preventPopup)) {
                Popup.info( message + "<br/>" + status);
            }
        } else if (!Common.isNull(responseObject.Message)) {
            displayResult = responseObject.Message;
            if (!Common.isNull(preventPopup) && !Common.parseBoolean(preventPopup)) {
                Popup.info(displayResult);
            }
        } else {
            displayResult = _L.warning_undefined_result;
            if (!Common.isNull(preventPopup) && !Common.parseBoolean(preventPopup)) {

                if(location.hostname == 'itportal'){
                    Popup.error(displayResult);
                }
            }
        }
    }
    return displayResult;
};
HttpUtils.executeAjax = function (url, data, optionCallback, isLoading, isSpecialCallback, paramsCallBack, isAsync) {
    if (Common.isNull(isAsync)) {
        isAsync = true;
    }
    var urlBuilder = url;
    if (!Common.isNull(SPECIFIC_URL) && url !== "uploadAssetImage" && url !== "deleteAssetImage") {
        if (url.indexOf("?") === -1) {
            urlBuilder = url + "?" + SPECIFIC_URL;
        } else {
            urlBuilder = url + "&" + SPECIFIC_URL;
        }
    }
    $.ajax({
        type: 'POST',
        url: urlBuilder,
        data: data,
        async: isAsync,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (dataCalback) {
            try {
                if (!Common.isNull(optionCallback.success)) {
                    // console.log('Calback to successful function: ' + optionCallback.success.name);
                    if (Common.parseBoolean(isSpecialCallback)) {
                        // console.log('Special callback');
                        // Utils.loading();
                        Common.callBack(optionCallback.success, dataCalback, paramsCallBack);
                        // Utils.removeLoading();
                    } else {
                        //Utils.loading();
                        Common.callBack(optionCallback.success, dataCalback);
                        //Utils.removeLoading();

                    }
                } else {
                    //console.log('Call ajax is successfull without success handler');
                }
            } catch (err) {
                console.warn("Ajax error callback: " + err.message);
                console.info("URL: " + urlBuilder);
                console.log("Parameters: ");
                console.log(data);
                console.error(err.stack);
                Utils.removeLoading();
            }

        },
        error: function (err) {
            console.error("ajax failed: " + err.responseText);
            if (!Common.isNull(optionCallback.error)) {
                //console.log('Calback to error function: ' + optionCallback.error.name);
                Common.callBack(optionCallback.error, err);
            } else {
                // console.log('Call ajax is fail without error handler');
            }
            Utils.displayInformation(err);
        },
        progress: function (e) {
            if (e.lengthComputable) {
                var pct = (e.loaded / e.total) * 100;
                Utils.displayMainProgressBarStatus(pct);
            } else {
                console.warn('Content Length not reported!');
                Utils.displayMainProgressBarStatus(100);
            }
        }
    }).done(function (res) {
        if (!Common.isNull(optionCallback.done)) {
            // console.log('Calback to error function: ' + optionCallback.done.name);
            Common.callBack(optionCallback.done, res);
        } else {
            // console.log('Call ajax is done without done handler');
        }
    }).always(function () {
        // Utils.removeLoading();
    });
};
HttpUtils.CallAjax = function (methodName, module, subPath, model, optionCallback, isLoading, isAsync) {
    if (!Common.isNull(model)) {
        model.m = methodName;
        model.module = module;
        model.subPath = subPath;
        model.forcePostParam = true;
    }
    var url = Common.BaseUrl;
    HttpUtils.executeAjax(url, model, optionCallback, isLoading, null, null, isAsync);
};
HttpUtils.CallAjaxWithQueryString = function (methodName, module, subPath, query, optionCallback, isLoading, isAsync) {
    if (Common.isNull(subPath)) {
        subPath = "?";
    } else {
        subPath = "?subPath=" + subPath + "&";
    }
    var url = Common.BaseUrl + subPath + 'm=' + methodName + '&module=' + module + '&' + query;
    HttpUtils.executeAjax(url, null, optionCallback, isLoading, null, null, isAsync);
};