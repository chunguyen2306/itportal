/**
 * Created by tientm2 on 10/18/2017.
 */
Libs = window.Libs || {};

Libs.Location = new (function () {

    this.__defineGetter__('path_param', function(){
        var pathname = location.pathname;
        var regex = /([^\/]+)/g;
        var match;
        var params = [];
        while (match = regex.exec(pathname)){
            params.push(decodeURI(match[1]));
        }

        return params;
    });

    this.__defineGetter__('query_param', function(){

        var vars = [], hash;
        var fullPath = $(location).attr('search');
        var hashes = fullPath.slice(fullPath.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            if(hash[0] === ''){
                continue;
            }
            //vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    });
    
})();

