//TODO REWORK
/**
 * Cần tạo folder Knockout Component chứa tất cả các component của Knockout và rip trong phần rip js vào file min
 */

ko.utils.createUpdateModelHandle = function (valueAccessor, viewModel, bindingContext) {
    var fn;
    var re = function (newValue) {
        if (fn == null) {
            var model = valueAccessor();
            if (ko.isObservable(model)) {
                fn = function (newValue) {
                    if (re.isUpdating)
                        return;


                    re.isUpdating = 1;
                    try {
                        model(newValue);
                    } catch (ex) {
                        throw ex;
                    } finally {
                        re.isUpdating = 0;
                    }
                }
            } else if (typeof model == "function") {
                fn = model;
            } else {
                var modelPathData = valueAccessor.toString().match(/return[\s]+(.+?)[;\s}]/);
                if (modelPathData == null || modelPathData.length != 2 || modelPathData[1].match(/[^\w\.]\(\)/)) {
                    var errorMsg = "binding parse valueAccessor failed: '" + valueAccessor.toString() + "'";
                    console.error(errorMsg);
                    fn = function () {
                    };
                    return;
                }

                var modelPath = modelPathData[1];

                function validateModelPath(parrent) {
                    try {
                        var temp;
                        eval("temp = parrent." + modelPath + ";");
                        return 1;
                    } catch (ex) {
                        return 0;
                    }
                }

                var obj;
                if (validateModelPath(viewModel)) {
                    obj = viewModel;
                } else if (validateModelPath(bindingContext)) {
                    obj = bindingContext;
                } else if (validateModelPath({$context: bindingContext})) {
                    obj = {$context: bindingContext};
                } else if (validateModelPath(window)) {
                    obj = window;
                } else {
                    var errorMsg = "binding missing model: '" + modelPath + "'";
                    console.error(errorMsg);
                    return fn = function () {
                    };
                }

                eval("fn = function(newValue){obj." + modelPath + " = newValue;}");
            }
        }
        fn(newValue);
    };
    return re;
}

/**

 ====== html ============================================
 <div class="search-box" data-bind=" search: <function<item> || model>, searchAPI: <function<keyword, start, limit>>">
 <input class="search-component">
 <button class="search-component"></button>
 <ul class="search-component"></ul>
 </div>

 ====== knockout binding parameter ============================================
 search: model
 searchAPI: function được gọi để lấy dữ liệu
 searchConfig: {
    displayName: string: Tên thuộc tính hiển thị,
    valueName: string: Tên thuộc tính giá trị,
    passNull: boolean: flag để update model khi user select item không có trong dữ liệu
            true(default: displayName != null): update null.
            false(default: displayName == null): update là giá trị user đang nhập
    passValue: boolean: flag để update model khi user select item
            true(default): update null
            false: update là giá trị user đang nhập

  }


 */
ko.bindingHandlers.search = {
    init: function (element, valueAccessor, bindingsAccessor, viewModel, bindingContext) {
        function getComponent(name, before) {
            var re = joContainer.find(name + ".search-component");
            if (re.length == 0) {
                re = $("<" + name + ' class="search-component"/>');
                before ? before.after(re) : joContainer.prepend(re);
            }
            return re;
        }

        var joContainer = $(element);
        var joInput = getComponent("input");
        var joReuslt = getComponent("ul", joInput);
        var jobtnSelect = getComponent("button", joReuslt);
        var model = valueAccessor();
        var bindings = bindingsAccessor();
        var cf = bindings.searchConfig || {};
        var dName = cf.displayName;
        var vName = cf.valueName;
        var updateModel = ko.utils.createUpdateModelHandle(valueAccessor, viewModel, bindingContext);
        var items = new function () {
            var t = this;
            var selectedClass = "search-item-selected";

            t.selectedData = null;
            t.joSelecting = null;
            t.isShow = false;
            t.preventOnNewData = 0;
            t.list = [];

            t.mouseenter = function (e) {
                if (t.joSelecting != null)
                    t.joSelecting.removeClass(selectedClass);

                t.joSelecting = e.data;
                e.data.addClass(selectedClass);
            };

            t.clearSelecting = function () {
                if (t.joSelecting == null)
                    return;

                t.joSelecting.removeClass(selectedClass);
                t.joSelecting = null;
            };

            t.mousedown = function (e) {
                if (e.button != 0 && e.button != undefined)
                    return;

                var doItem = e.currentTarget;
                joInput.val(doItem.innerText);
                selectItem(doItem.data);
            };

            t.nextItem = function () {
                var next = t.joSelecting == null ? joReuslt.children().first() : t.joSelecting.next();
                if (next.length > 0 && next[0].data)
                    t.mouseenter({data: next});
            };

            t.prevItem = function () {
                var prev = t.joSelecting == null ? joReuslt.children().last() : t.joSelecting.prev();
                if (prev.length > 0 && prev[0].data)
                    t.mouseenter({data: prev});
            };

            t.onNewData = function () {
                if (t.preventOnNewData)
                    return;

                t.joSelecting = null;
                t.show();
            };

            t.selectCurrent = function () {
                if (t.isShow && t.joSelecting != null) {
                    t.joSelecting.mousedown();
                } else if (t.selectedData) {
                    selectItem(t.selectedData);
                } else {
                    items.hide();
                    var prvInputText = joInput.val();
                    cf.searchAPI(prvInputText, options.start, options.limit, function (list) {
                        // hủy bỏ nếu user đã update giá trị
                        if (prvInputText != joInput.val())
                            return;

                        if (list.length != 1) {
                            selectItem(null, prvInputText);
                            return;
                        }

                        var data = list[0];
                        if (dName ? (data[dName] == prvInputText) : (data == prvInputText))
                            selectItem(data);
                        else
                            selectItem(null, prvInputText);
                    });
                }
            };

            t.hide = function () {
                if (t.isShow == false)
                    return;

                t.isShow = false;
                joReuslt.css("display", "");
                $(window).unbind("click", t.winHide);
            };

            t.winHide = function () {
                joContainer.thisClick || t.hide();
                joContainer.thisClick = 0;
            }

            t.show = function () {
                if (t.joSelecting)
                    t.mouseenter({data: t.joSelecting});
                else
                    t.nextItem();

                if (t.isShow || joReuslt.children().length < 1)
                    return;

                t.isShow = true;
                joReuslt.css("display", joReuslt.display);
                $(window).bind("click", t.winHide);
            };

        };
        var color = new function () {
            var c = this;
            var orgColor = joInput.css("color");
            var warnColor = "#ffa200";
            var isWarn = 0;

            c.update = function () {
                if (items.selectedData == null) {
                    if (isWarn == 0) {
                        joInput.css("color", warnColor);
                        isWarn = 1;
                    }
                } else {
                    c.off();
                }
            }
            c.off = function () {
                if (isWarn) {
                    joInput.css("color", orgColor);
                    isWarn = 0;
                }
            }
        };
        var resCtrl = new function () {
            var lastID = 1;
            var lastcheckID = 0;

            this.newID = function () {
                return lastID++;
            }

            this.checkID = function(id){
                var re = id > lastcheckID;
                lastcheckID = id;
                return re;
            }
        };

        joReuslt.display = joReuslt[0].style.display || 'inherit';

        // preparse
        {
            if (jobtnSelect.css("display") == "")
                jobtnSelect.css("display", "none");

            var endSearch, options;
            cf.searchAPI = bindings.searchAPI;

            if (cf.hasOwnProperty("passNull") == false) {
                cf.passNull = (dName != null);
            }
            if (jobtnSelect.text() == "")
                jobtnSelect.html("<i class=\"fa fa-search\"></i>");
            if (dName) {
                endSearch = function (res) {
                    var array;
                    if (Array.isArray(res))
                        array = res;
                    else if (Array.isArray(res.data))
                        array = res.data;
                    else
                        return;

                    items.list = array;
                    joReuslt.empty();
                    for (var i = 0; i < array.length; i++) {
                        var item = array[i];
                        var joLi = $("<li>" + item[dName] + "</li>");
                        joLi[0].data = item;
                        joLi.mouseenter(joLi, items.mouseenter);
                        joLi.on('mousedown', items.mousedown);
                        joReuslt.append(joLi);
                    }
                    // if (joReuslt.children().length == 0)
                    //     joReuslt.append("<li>Chưa tìm thấy kết quả</li>");
                    items.onNewData();
                }
            } else {
                endSearch = function (res) {
                    var array;
                    if (Array.isArray(res))
                        array = res;
                    else if (Array.isArray(res.data))
                        array = res.data;
                    else
                        return;

                    items.list = array;
                    joReuslt.empty();
                    for (var i = 0; i < array.length; i++) {
                        var item = array[i];
                        var joLi = $("<li>" + item + "</li>");
                        joLi[0].data = item;
                        joLi.mouseenter(joLi, items.mouseenter);
                        joLi.on('mousedown', items.mousedown);
                        joReuslt.append(joLi);
                    }
                    // if (joReuslt.children().length == 0)
                    //     joReuslt.append("<li>Chưa tìm thấy kết quả</li>");
                    items.onNewData();
                }
            }

            options = {
                start: cf.start || "",
                limit: cf.limit || "",
                endSearch: endSearch
            };
            if (cf.default) {
                var df = cf.default;
                df = typeof df == 'function' ? df() : df;
            }

            joReuslt.css("display", "");
            var btnWidth = jobtnSelect[0].offsetWidth;
            btnWidth > 0 && joInput.css("padding-right", btnWidth + 5);
        }

        function beginSearch(val) {
            items.clearSelecting();
            options.keyword = val;

            var searchID = resCtrl.newID();
            cf.searchAPI(options.keyword, options.start, options.limit, function(res){
                if(resCtrl.checkID(searchID))
                    options.endSearch(res);
            });
        };

        function selectItem(data, str) {
            var d = data;
            var sl = data;
            if (data) {
                if (str && (vName || dName)) {
                    d = {};
                    if (dName)
                        d[dName] = str;

                    if (vName)
                        d[vName] = data;
                } else if (vName) {
                    sl = data[vName];
                }
            } else {
                if (cf.passNull == false) {
                    if (dName) {
                        d = {};
                        d[dName] = str;
                    }

                    if (vName) {
                        d = d || {};
                        d[vName] = null;
                    }

                    (vName == null) && (sl = d);
                    color.update();
                } else {
                    d = {};
                }
            }

            items.selectedData = data;
            updateModel(vName ? d[vName] : d);
            items.hide();
        }

        function setData(data) {
            if (data == null) {
                joInput.val("");
                selectItem(null);
                return;
            }

            var dataIsObject = typeof data == 'object';
            var text = null;
            var searchValue = null;
            if (dataIsObject) {
                text = dName ? data[dName] : '';
                joInput.val(text);
                items.selectedData = data;
                return;
            }

            items.selectedData = null;
            text = searchValue = data;
            var ph = joInput.attr("placeholder") || "";
            var phl = "loading";
            var prvInputText = searchValue;

            var isFocus = 0;

            function onFocus() {
                isFocus = 1;
                //joInput.attr("placeholder", ph);
            }

            function onBlur() {
                isFocus = 0;
                //joInput.attr("placeholder", phl);
                phl = "loading...";
            }

            function end() {
                joInput.unbind("focus", onFocus);
                joInput.unbind("blur", onBlur);
                //joInput.attr("placeholder", ph);
            }

            joInput.val(text);
            joInput.bind("focus", onFocus);
            joInput.bind("blur", onBlur);

            function loadData(isFirst) {
                if (!isFirst)
                    color.update();

                if (isFocus == 0) {
                    phl += ".";
                    //joInput.attr("placeholder", phl);
                }
                cf.searchAPI(searchValue, options.start, options.limit, function (res) {
                    var list = Array.isArray(res) ? res : res.data;

                    // hủy bỏ nếu user đã hoặc đang update giá trị
                    if (joInput.is(":focus") || prvInputText != joInput.val()) {
                        end();
                        return;
                    }

                    // search lại nếu chưa tìm thấy kết quả
                    //if (list == null || list.length == 0) {
                    //    setTimeout(loadData, 500 + Math.random() * 500);
                    //    return;
                    //}

                    end();
                    // update giá trị, nếu ok, return
                    for (var i = 0; list && i < list.length; i++) {
                        var data = list[i];
                        if (
                            vName && (data[vName] == searchValue)
                            || dName && (data[dName] == searchValue)
                            || (data == searchValue)
                        ) {
                            selectItem(data);
                            var dValue = dName ? data[dName] : data;
                            if (joInput.val() != dValue)
                                joInput.val(dValue);
                            return;
                        }
                    }

                    // nếu chưa update được giá trị thì báo lỗi
                    var oColor = joInput.css("color");
                    joInput.css("color", "red");

                    function removeMissingDataStyle() {
                        if (items.selectedData) {
                            joInput.css("color", oColor);
                            joInput.unbind("input", removeMissingDataStyle);
                        }
                    }

                    joInput.bind("input", removeMissingDataStyle);
                });
            }

            setTimeout(loadData, 30, 1);
        }

        // event
        {
            joInput.bind("click", function () {
                //if(items.list && items.list.length){
                //    var selectedVal = null;
                //    if(items.selectedData){
                //        selectedVal = vName ? items.selectedData[vName] : items.selectedData;
                //    }
//
                //    var modelData = typeof model == "function" ? model() : model;
                //    if(modelData == selectedVal)
                //        return;
                //}

                beginSearch(joInput.val());
            });
            joInput.bind("input", function (e) {
                items.selectedData = null;
                beginSearch(joInput.val());
                color.off();
            });
            joInput.bind("keydown", function (e) {
                if (!e)
                    return;

                switch (e.which) {
                    case 38:
                        items.prevItem();  // up
                        break;
                    case 40:
                        items.nextItem();  // down
                        break;
                    case 13:    // enter
                        items.selectCurrent();
                        break;
                }
            });
            joInput.bind("focus", function () {
                items.show();

                if (items.selectedData == null)
                    joInput.css("color", "");
            });
            if (cf.selectByBlur) {
                joInput.bind("blur", function () {
                    var d = items.selectedData;
                    var value = d && (dName ? d[dName] : d);
                    var newValue = joInput.val();
                    if (value != newValue)
                        setData(newValue);
                });
            }
            jobtnSelect.bind("click", items.selectCurrent);
            joContainer.click(function (e) {
                items.isShow && (joContainer.thisClick = 1);

            });
            ko.isObservable(model) && model.subscribe(function (newValue) {
                if (newValue && vName && typeof newValue == 'object') {

                    model(newValue[vName]);
                    return;
                }

                if (updateModel.isUpdating)
                    return;

                setData(newValue);
            });
        }

        if (cf.setHandle) {
            var h = cf;
            h.rebinding = function (searchAPI, model, valueName, displayName) {
                h.searchAPI = searchAPI;
                h.valueName = vName = valueName;
                h.displayName = dName = displayName;

                if (typeof model == "function")
                    updateModel = model;
                else
                    updateModel = ko.utils.createUpdateModelHandle(function () {
                        return model;
                    }, viewModel, bindingContext);
            };
            h.dom = element;
            h.onkeydown = function (e) {
                switch (e.which) {
                    case 38:
                        items.prevItem();  // up
                        break;
                    case 40:
                        items.nextItem();  // down
                        break;
                    case 13:    // enter
                        items.selectCurrent();
                        break;
                }
            }
            h.setHandle(h);
            h.search = beginSearch;
        }

        //set data
        if (model != undefined) {
            if (ko.isObservable(model)) {
                var dfData = ko.unwrap(model);
                if (dfData)
                    setTimeout(setData, 1, dfData);
            } else if (typeof model != "function") {
                setTimeout(setData, 1, model);
            }
        }
    },
};

ko.bindingHandlers.date = {
    init: function (element, valueAccessor, bindingsAccessor, viewModel, bindingContext) {
        var fnUpdate = ko.utils.createUpdateModelHandle(valueAccessor, viewModel, bindingContext);

        element.onchange = function () {
            var date = valueAccessor();
            if (typeof date == "function")
                date = date();

            var valInString = element.value;

            function getNumber(index, length) {
                return parseInt(valInString.substr(index, length));
            }

            var origiTime = date.getTime();
            try {
                date.setFullYear(getNumber(0, 4));
                date.setMonth(getNumber(5, 2));
                date.setDate(getNumber(8, 2));

                if ($(element).attr('type') === 'datetime-local') {
                    date.setHours(getNumber(11, 2));
                    date.setSeconds(getNumber(14, 2));
                }
            } catch (ex) {
                date.setTime(origiTime);
            }
        }
    },
    update: function (element, valueAccessor, bindingsAccessor, viewModel, bindingContext) {
        var date = valueAccessor();
        var val = "";
        if (date) {
            val = date.getFullYear();
            val += "-";
            var temp = date.getMonth().toString();
            val += temp.length == 1 ? "0" + temp : temp;
            val += "-";
            temp = date.getDate().toString();
            val += temp.length == 1 ? "0" + temp : temp;


            if ($(element).attr('type') === 'datetime-local') {
                val += 'T';
                temp = date.getHours().toString();
                val += temp.length == 1 ? "0" + temp : temp;
                temp = date.getSeconds().toString();
                val += temp.length == 1 ? "0" + temp : temp;
            }
        }

        element.value = val;
    }
};

ko.bindingHandlers.ifanimation = {
    init: function (element, valueAccessor, bindingsAccessor, viewModel, bindingContext) {
        var jo = $(element);
        var domStyle = element.style;
        var binds = bindingsAccessor();
        var val = ko.unwrap(binds.value);
        var showHeight, hideHeight;
        var insertContent, clearContent;
        var content = jo.html();
        var originalHeight = domStyle.height;
        var isNoClass = (binds.showClass == null && binds.hideClass == null);
        var isShowing = isNoClass;
        var isInTrans = false;

        if (element.nodeType == Node.COMMENT_NODE) {
            clearContent = insertContent = function () {
                console.error("virtual binding is updating");
            };
        } else {
            insertContent = function () {
                if (jo.html().length)
                    return;

                jo.append(content);
                ko.applyBindingsToDescendants(bindingContext, jo[0]);
            };
            clearContent = function () {
                var children = jo.children();
                for (var i = 0; i < children.length; i++)
                    ko.cleanNode(children[i]);

                jo.empty();
            }
        }

        function initRange() {
            showHeight = hideHeight = jo.height();

            if (isNoClass) {
                hideHeight = 0;
                return;
            }

            // backup
            var preClasses = element.className;
            var prevAnimation = domStyle.animation;
            var prevTransition = domStyle.transition;

            // remove animation
            domStyle.animation = "";
            domStyle.transition = "";

            if (isShowing) {
                jo.addClass(binds.hideClass);
                jo.removeClass(binds.showClass);
                hideHeight = jo.height();
            } else {
                jo.addClass(binds.showClass);
                jo.removeClass(binds.hideClass);
                showHeight = jo.height();
            }

            // restore
            element.className = preClasses;
            domStyle.animation = prevAnimation;
            domStyle.transition = prevTransition;
        }

        function backup() {
            if (isInTrans)
                return;

            var data = backup.data || (backup.data = {});

            if (isShowing) {
                data.height = domStyle.height;
                showHeight = jo.height();
            }
            else {
                hideHeight = jo.height();
            }

            // overflow
            if (!isShowing) {
                data.overflow = domStyle.overflow;
                data.overflowY = domStyle.overflowY;
                domStyle.overflowY = "hidden";
            }

            // transition
            data.noTransition = (jo.css("transition") == null && jo.css("animation") == null);
            if (data.isNoTransition)
                jo.css("transition", '.3s');

            // opacity
            data.noOpacity = (jo.css("opacity") != null);
        }

        function restore() {
            var data = backup.data;
            if (data == null)
                return;

            // overflow
            if (isShowing) {
                domStyle.overflow = data.overflow;
                domStyle.overflowY = data.overflowY;
            }

            // height
            domStyle.height = data.height;

            // transition
            if (data.noTransition)
                jo.css("transition", '');

            // opacity
            if (data.noOpacity)
                jo.css("opacity", '');
        }

        function initTransition() {
            domStyle.transition = binds.transition || '0.3s';
        }

        function show() {
            if (isShowing)
                return;
            showHeight || initRange();

            var height = jo.height();
            if (isInTrans == false) {
                backup();
                hideHeight = height;
            }

            insertContent();

            if (domStyle.height == "")
                jo.height(hideHeight);

            if (isNoClass) {
                initTransition();
            } else {
                jo.addClass(binds.showClass);
                jo.removeClass(binds.hideClass);
            }

            jo.height(showHeight);
            if (backup.data.noOpacity)
                domStyle.opacity = '1';
            isShowing = true;
            isInTrans = true;
        }

        function hide() {
            if (isShowing == false)
                return;

            showHeight || initRange();

            backup();

            if (domStyle.height == "")
                jo.height(showHeight);

            if (isNoClass) {
                initTransition();
            } else {
                jo.addClass(binds.hideClass);
                jo.removeClass(binds.showClass);
            }

            jo.height(hideHeight);
            if (backup.data.noOpacity)
                domStyle.opacity = '.3';
            isShowing = false;
            isInTrans = true;
        }

        jo.bind("transitionend animationend", function () {
            isInTrans = false;
            if (isShowing == false)
                clearContent();
            restore();
        });

        if (isNoClass == false) {
            if (binds.showClass)
                isShowing = jo.hasClass(binds.showClass);
            else if (binds.hideClass)
                isShowing = !jo.hasClass(binds.hideClass);
        }

        element._koShow = show;
        element._koHide = hide;

        if (ko.unwrap(binds))
            binds.hideClass && jo.hasClass(binds.hideClass) && show();
        else
            binds.showClass && jo.hasClass(binds.showClass) && hide();

    },

    update: function (element, valueAccessor, bindingsAccessor, viewModel, bindingContext) {
        ko.unwrap(valueAccessor()) ? element._koShow() : element._koHide();
    }
};

ko.bindingHandlers.numeric = new function () {
    var me = this;
    var key = {
        back: 8,
        del: 46,
        dot: 190,
        A: 65,
        X: 88,
        C: 67,
        V: 86,
    };
    var allowKeys = [
        35, // home
        36, // end
        37, // left
        39, // right
    ];
    var processKey = [
        key.back,
        key.del,
        key.dot,
    ];

    function spliceInput(element, index, count, insert) {
        var jo = element instanceof jQuery ? element : $(element);

        var val = jo.val();
        count = count || 0;

        if (index + count > val.length)
            throw 'Remove input value out of range';

        var left = index ? val.substr(0, index) : '';
        var mid = insert ? insert : '';
        var right = val.substr(index + count);

        jo.val(left + mid + right);

        var carretIndex = left.length + mid.length;
        jo[0].setSelectionRange(carretIndex, carretIndex);
    }

    function format(valBegin, valEnd, numericData) {
        var fnUpdate = numericData.fnUpdate;
        if (fnUpdate && fnUpdate.isUpdating)
            return;

        var binds = numericData.binds;
        var jo = numericData.jo;

        if (!valBegin)
            valBegin = '';
        if (!valEnd)
            valEnd = '';

        var newVal = valBegin + valEnd;
        if (newVal.length == 0) {
            jo.val(newVal);
            return true;
        }

        var carretStart = valBegin.length;

        // format
        var re = [];
        var separate = binds.numericSeparate;
        var index = 0;
        var newCarretIndex = 0;
        var continueSetNewCarretIndex = true;
        var continueSeparate = separate;
        var separateLength = null;
        for (var i = 0; i < newVal.length; i++) {
            // push num
            var ch = newVal[i];

            if (isNaN(ch) && ch != '.')
                continue;

            if (ch == '.') {
                continueSeparate = false;
                separateLength = index;
            }

            if (continueSeparate) {
                if (index && index % 4 == 3) {
                    re.push(separate);
                    index++;
                }
            }

            re.push(ch);
            index++;

            // carret index
            if (continueSetNewCarretIndex) {
                if (i >= carretStart)
                    continueSetNewCarretIndex = false;
                else
                    newCarretIndex = index;
            }
        }

        if (separateLength === null)
            separateLength = re.length;

        var moveSize = (separateLength % 4);
        if (moveSize == 1 || moveSize == 2) {
            moveSize = 3 - moveSize;
            for (var i = 3; i < separateLength; i += 4) {
                for (var j = 0; j < moveSize; j++) {
                    var index = i - j;
                    re[index] = re[index - 1];
                    if (index == newCarretIndex)
                        newCarretIndex++;
                }
                re[i - moveSize] = binds.numericSeparate;
            }
        }


        var valInStr = re.join('');
        jo.val(valInStr);
        jo[0].setSelectionRange(newCarretIndex, newCarretIndex);

        if (fnUpdate) {
            var valInNumber = strToNumber(valInStr);
            fnUpdate(valInNumber);
        }
    };

    function strToNumber(str) {
        if (!str)
            return null;

        var re = [];
        for (var i = 0; i < str.length; i++) {
            var ch = str[i];
            if (isNaN(ch) == false || ch == '.')
                re.push(ch);
        }

        try {
            return parseFloat(re.join(''));
        } catch (ex) {
            return null;
        }
    }

    me.init = function (element, valueAccessor, bindingsAccessor, viewModel, bindingContext) {
        var jo = $(element);
        var binds = bindingsAccessor();
        var fnUpdate = ko.utils.createUpdateModelHandle(valueAccessor, viewModel, bindingContext);

        if (binds.numericSeparate) {
            if (binds.numericSeparate.length > 1)
                throw 'numericSeparate can not be multi charater';

            if (binds.numericSeparate == '.')
                throw 'numericSeparate can not be \'.\'';
        }

        var allowCheckInput = true;
        var numericData = {
            jo: jo,
            binds: binds,
            fnUpdate: fnUpdate
        };
        element.__numneric = numericData;

        jo.on('keydown', function (event) {
            var keyCode = event.keyCode;
            if (isNaN(event.key) && processKey.indexOf(keyCode) == -1) {
                if (keyCode < 32) // control keys
                    return true;

                if (111 < keyCode && keyCode < 124) // function key (F1-12)
                    return true;


                if (allowKeys.indexOf(keyCode) != -1)
                    return true;

                if (event.ctrlKey) {
                    if (keyCode == key.V) {
                        allowCheckInput = true;
                        return true;
                    } else if (keyCode == key.A || keyCode == key.X || keyCode == key.C) {
                        return true;
                    }
                }
                return false;
            }

            var newVal = jo.val();
            var indexOfDot = newVal.indexOf('.');

            var carretStart = this.selectionStart;
            var carretEnd = this.selectionEnd;
            if (carretStart > carretEnd) {
                carretStart = this.selectionEnd;
                carretEnd = this.selectionStart;
            }

            var isDelSelect = carretStart != carretEnd;
            var valBegin = newVal.substr(0, carretStart);
            var valEnd = newVal.substr(carretEnd);

            switch (keyCode) {
                case key.back:
                    if (!isDelSelect && valBegin.length) {
                        var index = valBegin.length - 1;
                        if (index > -1) {
                            var num = 1;
                            if (valBegin[index - 1] == binds.numericSeparate || valBegin[index] == binds.numericSeparate)
                                num++;

                            this.setSelectionRange(carretEnd - num, carretEnd);
                        }

                        //valBegin = valBegin.substr(0, num);
                    }
                    break;
                case key.del:
                    if (!isDelSelect && valEnd.length) {
                        var index = 1;
                        if (index < valEnd.length) {
                            if (valEnd[index - 1] == binds.numericSeparate || valEnd[index] == binds.numericSeparate)
                                index += 1;
                            this.setSelectionRange(carretStart, carretStart + index);
                        }
                        //valEnd = valEnd.substr(index);
                    }

                    break;
                case key.dot:
                    if (binds.numericAllowDot) {
                        if (valBegin.length == 0 || indexOfDot != -1)
                            return false;
                    } else {
                        return false;
                    }

                default:
                    valBegin += event.key;
            }

            return true;
        });

        jo.on('input', function (event) {
            if (allowCheckInput == false) {
                allowCheckInput = true;
                return;
            }

            var val = jo.val();
            var end = this.selectionStart < val.length ? val.substr(this.selectionStart) : '';
            var start = val.substr(0, this.selectionStart);
            format(start, end, numericData);
        });
    }

    me.update = function (element, valueAccessor, bindingsAccessor, viewModel, bindingContext) {
        var val = valueAccessor();
        if (ko.isObservable(val))
            val = val();

        var data = element.__numneric;
        format(val, '', data);
    }
};
