/**
 * Created by Administrator on 11/21/2016.
 */
Libs.JS = {};
Libs.JS.isNull = function(obj){
    if(obj === null || obj === undefined || obj === ""){
        return true;
    } else {
        return false;
    }
};
Libs.JS.callBack = function (func) {
    if (func !== null && func !== undefined && func !== "" && typeof func === 'function') {
        var args = [];
        for (var i = 1; i < arguments.length; i++)
            args.push(arguments[i]);
        func.apply(this, args)
    }
};
Libs.JS.wait = function(exp, callback){
  var thread = setInterval(function () {
    console.log(exp);
    if(exp){
      clearInterval(thread);
      JS.callBack(callback);
    }
  }, 100);
};