/**
 * Created by Administrator on 11/21/2016.
 */
Libs.LocalStorage = new (function(){
    var encodeMap = '012345wertyuio67m~!@#$%WERTYDFGHlzxc^&JKLZXCỦy quyền duyệt đơn hàngUIOPASk*_89Qvbn=+?VBNMqp<>-(';
    var speraMap = '|{}/\'[]';
    var random = function (max) {
        return Math.floor((Math.random() * max) + 1);
    };

    var encode = function(cookie_value) {
        // This variable holds the encoded cookie characters
        var coded_string = "";
        var codeArr = [];
        // Run through each character in the cookie value
        for (var counter = 0; counter < cookie_value.length; counter++) {
            codeArr.push(cookie_value.charCodeAt(counter));
        }
        var encrytStr = "";
        for (var i = 0; i < codeArr.length; i++) {
            var item = codeArr[i] + "";
            var code;

            var codeLength = item.length / 2;
            for (var j = 0; j < codeLength; j++) {
                code = item.substr(j * 2, 2);
                if (parseInt(code) > encodeMap.length) {
                    encrytStr += code;
                } else {
                    encrytStr += encodeMap[parseInt(code)];
                }
            }
            if (i < codeArr.length - 1) {
                encrytStr += speraMap[random(4) - 1];
            }
        }
        return encrytStr
    };
    var decode = function(coded_string) {
        coded_string = unescape(coded_string);
        var decrytStr = "";
        var codeStr = "";
        var enCodeArr = coded_string.replace(/[\|\{\}\/\'\[\]]/g, '*SEPARATION*').split('*SEPARATION*');
        var deCodeArr = [];
        for (var i = 0; i < enCodeArr.length; i++) {
            var item = enCodeArr[i];
            var code = "";
            for (var j = 0; j < item.length; j++) {
                var charItem = item[j];
                if (j < (item.length - 1) && parseInt(charItem) > 7) {
                    code += item[j] + item[j + 1];
                    j++;
                } else {
                    code += encodeMap.indexOf(charItem);
                }
            }
            var intCode = parseInt(code);
            decrytStr += String.fromCharCode(intCode);
        }
        return decrytStr;
    };

    this.removeLocalStorageItem = function (key) {
        localStorage.removeItem(key);
    }

    this.val = function(key, val){
        if(val === undefined){
            if(localStorage.getItem(key) === null || localStorage.getItem(key) === undefined || localStorage.getItem(key) === ''){
                return null;
            } else {
                return localStorage.getItem(key);
            }
        } else {
            localStorage.setItem(key, val);
        }
    }
})();