/**
 * Created by tientm2 on 12/18/2014.
 */
/************* DEMO ***********
 *
 * =============================================
 * ================== Syntax ==================
 * =============================================
 *
 * data-bind="VNGSearch: String-Value property,
 *            SearchLimit: Int-Limit result,
 *            SearchQuery: Function-Search function,
 *            SearchQueryParams: Object-Params list,
 *            SeacrhTemplate: String-Display template,
 *            DefaultData: Array-Default data,
 *            DisplayText: String-Text return,
 *            SeacrhValue: String-Value return "
 *
 * =============================================
 * ================== Example ==================
 * =============================================
 * =============== Search with none filter ================
 *    <input type="text" class="VNGSearch form-control"
 *                       data-bind="VNGSearch: assetType,
 *                       SearchLimit: 10,
 *                       SearchQuery: AAPData.CMDB.GetListComponentType,
 *                       SeacrhTemplate: '{Name}',
 *                       DefaultData: $root.requestAsset.AssetTypes,
 *                       DisplayText: 'Name',
 *                       SeacrhValue: 'Name' "/>
 *
 * =============== Search with filter ================
 *  <input type="text" class="VNGSearch form-control"
 *                      data-bind="VNGSearch: assetModel,
 *                      SearchLimit: 10,
 *                      SearchQuery: AAPData.CMDB.GetListComponentModel,
 *                      SearchQueryParams: { type: function(){ return $root.requestAsset.AssetData()[$index()].assetType }, startindex: 0, limit: 10 },
 *                      SeacrhTemplate: '{Name}',
 *                      DefaultData: $root.requestAsset.AssetModels,
 *                      DisplayText: 'Name',
 *                      SeacrhValue: 'Name' "/>
 *
 ************ END DEMO *********/

ko.bindingHandlers.VNGSearch = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var value = valueAccessor();
        var objectStr = valueAccessor.toString().replace(/^function \(\)\{return /g,'').replace(/ \}/g,'');
        var parentObject = objectStr.substring(0, objectStr.lastIndexOf(')'));

        var eml = $(element)[0];
        $(element)[0].VNGSearch = new VNGSearch({
            Input: $(element),
            limit: allBindings.get('SearchLimit') || '-1',
            query: allBindings.get('SearchQuery') || null,
            queryParams: allBindings.get('SearchQueryParams') || null,
            template: allBindings.get('SeacrhTemplate') || null,
            returnValue: allBindings.get('SeacrhValue') || '',
            defaultData: allBindings.get('DefaultData') || null,
            displayText: allBindings.get('DisplayText') || '',
            displayField: allBindings.get('DisplayField') || '',
            onChange: allBindings.get('ChangeEvent') || null
        });
        if (!Common.isNull(allBindings.get('DefaultData'))) {
            $(element).on('click', function () {
                eml.VNGSearch.ShowDefaultData();
            });
        }
        eml.VNGSearch.OnSelected = function () {
            //$(element).val(eml.VNGSearch.SelectedValue.text);
            if ((typeof value !== 'function')) {
                if (Common.isNull(bindingContext.$index)) {
                    eval('viewModel.' + objectStr + ' = "' + eml.VNGSearch.SelectedValue.value + '"');
                    eval('viewModel.' + parentObject + 'viewModel.' + parentObject + '))');
                } else {
                    eval('bindingContext.$data.' + objectStr + ' = "' + eml.VNGSearch.SelectedValue.value + '"');
                }
            } else {
                value(eml.VNGSearch.SelectedValue.value);
                eml.value = eml.VNGSearch.SelectedValue.text;
            }
            if (!Common.isNull(eml.VNGSearch.option.displayField)) {
                var display = eml.VNGSearch.option.displayField;
                var displayObjectStr = display.toString().replace(/^function \(\)\{return /g, '').replace(/ \}/g, '');
                var displayParentObject = displayObjectStr.substring(0, displayObjectStr.lastIndexOf(')'));
                if ((typeof value !== 'function')) {
                    if (Common.isNull(bindingContext.$index)) {
                        console.log(bindingContext.$index);
                        eval('viewModel.' + displayObjectStr + ' = "' + eml.VNGSearch.SelectedValue.text + '"');
                        eval('viewModel.' + displayParentObject + 'viewModel.' + parentObject + '))');
                    } else {
                        eval('bindingContext.$data.' + displayObjectStr + ' = "' + eml.VNGSearch.SelectedValue.text + '"');
                    }
                } else {
                    display(eml.VNGSearch.SelectedValue.text);
                }
            }

            Common.callBack(eml.VNGSearch.option.onChange, bindingContext.item, $(element));
        };
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

    }
};
var VNGSearch = function (option) {
    var me = this;
    this.option = option;
    var ul = $('<ul class="VNGSearchPopup"></ul>');
    if ($('#vng_search_popup').length !== 0) {
        ul = $('#vng_search_popup');
    } else {
        ul.attr('id', 'vng_search_popup');
        $('body').append(ul);
    }

    var changeHandler = [];

    this.addChangeHandler = function (func) {
        changeHandler.push(func);
    };

    var doChangeHandler = function () {
      for(var i = 0; i < changeHandler.length; i++){
          changeHandler[i]();
      }
    };
    this.OnSelected = function () { };
    this.SelectedValue = {};
    var initResize = false;

    this.Resize = function (ul, input) {
        ul.css('top', input.offset().top + input.outerHeight());
        ul.css('left', input.offset().left);
        ul.width(input.outerWidth());

        if (!initResize) {
            $(window).on('resize', function () {
                me.Resize(ul, input);
            });
            initResize = true;
        }
    };
    this.initData = function(data){
        ul.html('');

        for (var i = 0; i < data.length; i++) {
            var itemText = me.option.template;
            var dataItem = data[i];
            if(itemText !== 'null') {
                var keyWord = itemText.match(/\{[^\}]*\}/g);
                for (var j = 0; j < keyWord.length; j++) {
                    var keyWordItem = keyWord[j].replace(/\{/g, '').replace(/\}/g, '');
                    itemText = itemText.replace(keyWord[j], dataItem[keyWordItem]);
                }
            } else {
                itemText = dataItem;
            }

            var li = $('<li></li>');
            li.append(itemText);
            li[0].data = dataItem;
            li.on('click', function () {
                me.SelectedValue = {};
                me.SelectedValue.value = me.option.returnValue !== 'null'?$(this)[0].data[me.option.returnValue]:$(this)[0].data;
                me.SelectedValue.text = me.option.displayText !== 'null'?$(this)[0].data[me.option.displayText]:$(this)[0].data;
                $(me.option.Input).val(me.SelectedValue.text);
                Common.callBack(me.OnSelected);
                if (!Common.isNull(me.option.defaultData)) {
                    if (typeof me.option.defaultData === 'function') {
                        me.initData(me.option.defaultData());
                    } else {
                        me.initData(me.option.defaultData);
                    }
                }
                ul.hide();
                $(me.option.Input).trigger('itemselected');
                $(me.option.Input).trigger('change');
            });
            ul.append(li);

        }
        if (data.length > 0) {
            ul.show();
        } else {
            ul.hide();
        }
    };

    this.Search = function (onseleced) {
        /*if (Common.isNull(me.option.Input.val())) {
            ul.hide();
            return;
        }*/

        if (me.option.queryParams !== null) {
            var args = [];
            args.push(me.option.Input.val());
            for (var name in me.option.queryParams) {
                if (typeof me.option.queryParams[name] === 'function') {
                    args.push(me.option.queryParams[name]());
                } else {
                    args.push(me.option.queryParams[name]);
                }
            }
            args.push(function (data) {
                if(typeof(data) === 'string') {
                    data = Libs.Json.toObject(data);
                }
                me.initData(data);
                me.Resize(ul, me.option.Input);
            });
            Common.callBackDynamic(me.option.query, args);
        } else {
            Common.callBack(me.option.query, me.option.Input.val(), me.option.startIndex, me.option.limit, function (data) {
                if(typeof(data) === 'string') {
                    data = Libs.Json.toObject(data);
                }
                me.initData(data);
                me.Resize(ul, me.option.Input);
            });
        }
    };

    this.ShowDefaultData = function () {
        if (typeof me.option.defaultData === 'function') {
            me.initData(me.option.defaultData());
        } else {
            me.initData(me.option.defaultData);
        }

        me.Resize(ul, me.option.Input);
        ul.show();
    };

//  if (Common.isNull(option.query)) {
//    throw new exception('Null query function');
//  }
    if (Common.isNull(option.startIndex)) {
        option.startIndex = 0;
    }
    if (Common.isNull(option.limit)) {
        option.limit = "";
    }
    if (!Common.isNull(option.defaultData)) {
        option.Input.addClass('DownArrow');
    }
    var isBlur = false;
    var isLiBlur = false;
    ul.on('mouseenter', function () {
        isBlur = false;
    });
    ul.on('mouseleave', function () {
        isBlur = true;
    });

    me.option.Input.on('blur', function () {
        if (isBlur) {
            ul.hide();
        }
    });

    me.option.Input.on('keyup', function () {
        me.Search();
    });

    me.option.Input.on('click', function () {
        if(this.value == ''){
            me.Search();
        }
    });

    Object.defineProperty(me, 'value', { get: function() {
        return me.SelectedValue;
    } });
};

jQuery.fn.extend({
    VNGSeacrh: function (option) {
        return this.each(function () {
            option.Input = $(this);
            this.VNGSearch = new VNGSearch(option);
            $(this).on('keyup', this.VNGSearch.Search);
        });
    }
});
