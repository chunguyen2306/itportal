/**
 * Created by Administrator on 11/21/2016.
 */
Libs.Cookie = new (function(){
    var encodeMap = '012345wertyuio67m~!@#$%WERTYDFGHlzxc^&JKLZXCỦy quyền duyệt đơn hàngUIOPASk*_89Qvbn=+?VBNMqp<>-(';
    var speraMap = '|{}/\'[]';
    var random = function (max) {
        return Math.floor((Math.random() * max) + 1);
    };
    var encode_cookie = function(cookie_value) {
        // This variable holds the encoded cookie characters
        var coded_string = "";
        var codeArr = [];
        // Run through each character in the cookie value
        for (var counter = 0; counter < cookie_value.length; counter++) {
            codeArr.push(cookie_value.charCodeAt(counter));
        }
        var encrytStr = "";
        for (var i = 0; i < codeArr.length; i++) {
            var item = codeArr[i] + "";
            var code;

            var codeLength = item.length / 2;
            for (var j = 0; j < codeLength; j++) {
                code = item.substr(j * 2, 2);
                if (parseInt(code) > encodeMap.length) {
                    encrytStr += code;
                } else {
                    encrytStr += encodeMap[parseInt(code)];
                }
            }
            if (i < codeArr.length - 1) {
                encrytStr += speraMap[random(4) - 1];
            }
        }
        return encrytStr
    };
    var decode_cookie = function(coded_string) {
        coded_string = unescape(coded_string);
        var decrytStr = "";
        var codeStr = "";
        var enCodeArr = coded_string.replace(/[\|\{\}\/\'\[\]]/g, '*SEPARATION*').split('*SEPARATION*');
        var deCodeArr = [];
        for (var i = 0; i < enCodeArr.length; i++) {
            var item = enCodeArr[i];
            var code = "";
            for (var j = 0; j < item.length; j++) {
                var charItem = item[j];
                if (j < (item.length - 1) && parseInt(charItem) > 7) {
                    code += item[j] + item[j + 1];
                    j++;
                } else {
                    code += encodeMap.indexOf(charItem);
                }
            }
            var intCode = parseInt(code);
            decrytStr += String.fromCharCode(intCode);
        }
        return decrytStr
    };
    var create = function (name, value, days) {
        var expires;

        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = escape(name) + "=" + escape(encode_cookie(value)) + expires + "; path=/";
    };
    var read = function (name) {
        var nameEQ = escape(name) + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0) return unescape(decode_cookie(c.substring(nameEQ.length, c.length)));
        }
        return null;
    };
    var remove = function (name) {
        create(name, "", -1);
    };
    var getsetKey = function (key, value) {
        if (value === undefined) {
            return read(key);
        } else if (value === null) {
            remove(key);
        } else {
            create(key, value);
        }
    };
    this.Key = function(key, value){
        return getsetKey(key, value);
    }
})();