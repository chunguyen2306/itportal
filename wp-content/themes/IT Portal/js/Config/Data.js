Config.Data = {
    Module: {
        ITAAPService: "ITAPP",
        CMDB: "CMDB",
        LOGIN: "Login",
        Direct: "direct",
        ISOAPI: "ISOAPI",
        AAPWEB: "AAPWEB"
    },
    AssetUrls: {
        itServiceBus: 'process'
    },
    SubPath: {
        Asset:"asset",
        TransferForm: "transferForm",
        TransactionForm: 'transactionForm',
        requestForm: 'requestForm',
        DelegateForm: 'delegateForm',
        FormAssetListDetail: "formassetdetail",
        FormNote: "formnote",
        FormStepDefinition: "formstepdefinition",
        FormStep: "formstep",
        Form: "form",
        SystemLog: "systemLog",
        RecoveryForm: "recovery",
        UserRole: "userrole",
        Role: "role",
        FormReason: "formreason",
        FormType: "formtype",
        PrDetail: "pr",
        AssetImageDetail: "assetimage",
        FormstepPermission: "formsteppermission",
        AssetDetailHistory: "assetDetailHistory",
        AssetStatusHistory: "assetStatusHistory",
        MailTemplate: "mailTemplate",
        WebConfig: "webConfig",
        EmployeeTitle: "employeetitle",
        DefaultAsset: "defaultasset",
        TechnicianSite: "technician",
        AssetGeneralInfo: "assetGeneralInfo"
    }
};