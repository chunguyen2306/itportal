/**
 * Created by Administrator on 2/27/2017.
 */
Config.URL = new (function(){
  this.MODULE = DIR_URL+'/js/Module/';
  this.DATA = DIR_URL+'/js/Data/';
  this.LIBS = DIR_URL+'/js/Libs/';
  this.MODULE_VIEW_DIR = DIR_URL+'/{ModuleName}/views/';
  this.MODULE_MODEL = DIR_URL+'/{ModuleName}/model.js';
  this.MODULE_STYLE = DIR_URL+'/{ModuleName}/style.css';
  this.MODULE_VIEW = DIR_URL+'/{ModuleName}/view.js';
  this.MODULE_INDEX = DIR_URL+'/{ModuleName}/index.html';
})();