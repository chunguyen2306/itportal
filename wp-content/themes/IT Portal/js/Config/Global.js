/**
 * Created by tientm2 on 10/17/2017.
 */
Config.Global = {
    isInitMenuMouseHover: true,
    isInitMouseWheel: true,
    mouseWheelBaseOn: '.head-slide',
    mouseWheelLimitHeight: true
};