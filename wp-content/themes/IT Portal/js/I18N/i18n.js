/**
 * Created by tientm2 on 9/12/2017.
 */

var lang_broswer = navigator.language || navigator.userLanguage;
if (lang_broswer !== null && lang_broswer !== undefined && lang_broswer !== '') {
    lang_broswer = lang_broswer.split('-')[0];
}
var userSetting = JSON.parse(Libs.LocalStorage.val('userSetting'));
var lang = 'vi';
if (userSetting === null || userSetting === undefined || userSetting === '') {
    lang = lang_broswer;
    Libs.LocalStorage.val('userSetting', JSON.stringify({language: lang}));
} else if (userSetting.language === null || userSetting.language === undefined || userSetting.language === '') {
    lang = lang_broswer;
    Libs.LocalStorage.val('userSetting', JSON.stringify({language: lang}));
} else {
    lang = userSetting.language;
}

function i18n(name, params) {
    lang = 'vi';
    var result = I18N[lang][name];
    if (result) {
        if (params !== null && params !== undefined && params !== '') {
            var value_param = '@value';
            for (var i = 1; i <= params.length; i++) {
                result = result.replace(value_param + i, params[i - 1]);
            }
        }
        return result;
    } else {
        return name;
    }
}