/**
 * Created by tientm2 on 9/12/2017.
 * @language: English
 */
var I18N = I18N || {};
I18N.en = {
    /*Admin Menu*/
    "admin-menu-asset": "Sản phẩm",
    "admin-menu-asset-allasset": "Danh sách sản phẩm",
    "admin-menu-asset-assetmodel": "Model sản phẩm",
    "admin-menu-asset-assettype": "Loại sản phẩm",
    "admin-menu-user": "Người dùng",
    "admin-menu-user-alluser": "Người dùng",
    "admin-menu-role&permission": "Quyền hạn",
    "admin-menu-role&permission-role": "Danh sách quyền",
    "admin-menu-role&permission-permission": "Phân quyền",
    "admin-menu-automation": "Tự động hóa",
    "admin-menu-automation-list": "Danh sách",
    "admin-menu-automation-setting": "Cấu hình",
    "admin-menu-gui": "Giao diện",
    "admin-menu-gui-main-menu": "Menu chính",
    "admin-menu-gui-menu-resource-category": "Nhóm dịch vụ",
    "admin-menu-gui-menu-resource-type": "Loại dịch vụ",
    "admin-menu-gui-menu-component": "Model sản phẩm",
    "admin-menu-gui-menu-component-type": "Loại sản phẩm",






    /* Button general */
    "all-page:close:btn:text": "Đóng",
    "all-page:yes:btn:text": "Có",
    "all-page:no:btn:text": "Không",
    "all-page:ok:btn:text": "Đồng ý",
    "all-page:cancel:btn:text": "Huỷ",

    /* Master page home - Header */
    "master-page-home:header:track-orders:btn:text": "Theo dõi đơn hàng",
    "master-page-home:header:track-orders:btn:title": "Theo dõi đơn hàng",
    "master-page-home:header:notifications:btn:text": "Thông báo của tôi",
    "master-page-home:header:notifications:btn:title": "Thông báo của tôi",
    "master-page-home:header:search-option:option-1:cbx:text": "Sản phẩm",
    "master-page-home:header:search-option:option-1:cbx:tips": "Nhập tên sản phẩm hoặc mã sản phẩm cần tìm",
    "master-page-home:header:search-option:option-2:cbx:text": "Đơn hàng",
    "master-page-home:header:search-option:option-2:cbx:tips": "Nhập mã đơn hàng, ví dụ #111 hoặc tên sản phẩm có trong đơn hàng",
    "master-page-home:header:search-option:option-3:cbx:text": "Hướng dẫn",
    "master-page-home:header:search-option:option-3:cbx:tips": "Tìm kiếm hướng dẫn, quy định",
    "master-page-home:header:cart:btn:text": "Giỏ hàng",
    "master-page-home:header:cart:btn:title": "Giỏ hàng",

    /* Master page home - Notifications box */
    "master-page-home:header:notifications-box:no-notification:lbl:text": "Chưa có thông báo nào",
    "master-page-home:header:notifications-box:see-all-notification:lbl:text": "Xem tất cả thông báo",

    /* Master page home - Menu */
    "master-page-home:menu:login:lbl:text": "Đăng nhập",

    /* Cart page - Left side */
    "cart:left-side:title:lbl:text": "Thông tin giỏ hàng",
    "cart:left-side:title:back-home:btn:text": "Tiếp tục đặt hàng",
    "cart:left-side:table:empty-cart:lbl:text": "Giỏ hàng đang trống",
    "cart:left-side:table:columns:column-1:lbl:text": "Loại sản phẩm",
    "cart:left-side:table:columns:column-2:lbl:text": "Tên sản phẩm",
    "cart:left-side:table:columns:column-3:lbl:text": "Số lượng",
    "cart:left-side:table:product-notification:not-enough:lbl:text": "Chỉ còn @value1 sản phẩm! Nếu đặt, bạn phải chờ khoảng @value2 ngày.",
    "cart:left-side:table:product-notification:out-of-stock:lbl:text": "Hàng trong kho đã hết! Nếu đặt, bạn phải chờ khoảng @value1 ngày.",

    /* Cart page - Right side */
    "cart:right-side:title:lbl:text": "Thông tin khác",
    "cart:right-side:title-input:user-domain:lbl:text": "Người sử dụng ",
    "cart:right-side:input:wrong-domain:input:text": "Không tồn tại",
    "cart:right-side:input:user-domain:no-permission:lbl:title": "Bạn không có quyền thay đổi người sử dụng",
    "cart:right-side:title-input:address:lbl:text": "Vị trí nhận hoặc trả hàng ",
    "cart:right-side:input:wrong-address:input:text": "Không xác định",
    "cart:right-side:title-input:note:lbl:text": "Mục đích sử dụng hoặc ghi chú khác ",
    "cart:right-side:return-asset:btn:text": "Trả sản phẩm đang quản lý",
    "cart:right-side:return-asset:no-asset:lbl:title": "Bạn không sở hữu tài sản nào",
    "cart:right-side:submit-order:btn:text": "Đặt hàng",
    "cart:right-side:submit-return-asset:btn:text": "Trả hàng",

    /* Cart page - Popup Return Asset */
    "cart:popup-return-asset:title:lbl:text": "Danh sách sản phẩm trả",
    "cart:popup-return-asset:left-side:title:lbl:text": "Sản phẩm @value1 đang quản lý",
    "cart:popup-return-asset:left-side:return-all:btn:text": "Trả tất cả",
    "cart:popup-return-asset:left-side:search:input:placeholder": "Từ khoá tìm kiếm",
    "cart:popup-return-asset:left-side:no-asset:lbl:text": "Không có tài sản nào",
    "cart:popup-return-asset:left-side:table:columns:column-1:lbl:text": "Mã sản phẩm",
    "cart:popup-return-asset:left-side:table:columns:column-2:lbl:text": "Loại sản phẩm",
    "cart:popup-return-asset:left-side:table:columns:column-3:lbl:text": "Dòng sản phẩm",
    "cart:popup-return-asset:right-side:title:lbl:text": "Sản phẩm đang chọn",
    "cart:popup-return-asset:right-side:deselect-all:btn:text": "Bỏ chọn tất cả",
    "cart:popup-return-asset:right-side:table:columns:column-1:lbl:text": "Mã sản phẩm",
    "cart:popup-return-asset:right-side:table:columns:column-2:lbl:text": "Dòng sản phẩm",
    "cart:popup-return-asset:right-side:select-no-item:lbl:text": "Chưa chọn sản phẩm nào",
    "cart:popup-return-asset:footer:submit:btn:text": "Đồng ý trả",

    /* Cart page - Messages */
    "cart:message:not-enough-information": "Vui lòng điền đầy đủ thông tin yêu cầu",
    "cart:message:wrong-domain": "Thông tin người sử dụng chưa đúng",
    "cart:message:submit-order": "Bạn có xác nhận tạo đơn hàng với thông tin trong giỏ hàng của bạn?",
    "cart:message:submit-only-return-asset": "Bạn có đồng ý với những đơn hàng đã chọn ?",
    "cart:message:not-select-any-asset": "Bạn chưa chọn tài sản nào",
    "cart:message:not-allow-seeing-those-assets": "Bạn không có quyền xem tài sản của người này",
    "cart:message:confirm-change-user": "Bạn có chắc chắn thay đổi người sử dụng?",
    "cart:message:consequence-change-user": "Nếu đổi, các sản phẩm trả đang chọn sẽ phải chọn lại theo người sử dụng mới.",
    "cart:message:need-to-buy:start-message": "Thông báo",
    "cart:message:need-to-buy:out-of-stock": "@value1: Đã hết hàng",
    "cart:message:need-to-buy:out-of-stock:waiting": "Nếu đặt hàng, bạn phải chờ khoảng @value1 ngày",
    "cart:message:need-to-buy:not-enough": "@value1: Không đủ số lượng (thiếu @value2)",
    "cart:message:need-to-buy:not-enough:waiting": "Nếu đặt hàng, bạn phải chờ khoảng @value1 ngày",
    "cart:message:create-order:fail": "Đơn hàng CHƯA được tạo",
    "cart:message:create-order:success": "Đơn hàng đã được tạo THÀNH CÔNG",
    "cart:message:create-order:success:detail": "- Mã đơn hàng @value1 là @value2",
    "cart:message:create-order:fail:detail": "- Đơn hàng @value1",
    "cart:message:create-order:fail:contact-bonus": "Vui lòng liên hệ helpdesk để được xử lý",
    "cart:message:note:address": "Địa chỉ nhận hàng: @value1.",
    "cart:message:note:other-note": "Ghi chú khác: @value1.",

    /* Cart page - Button */
    "cart:button:submit-order:btn:text": "Đồng ý đặt hàng",
    "cart:button:select-other-product:btn:text": "Chọn sản phẩm khác",
    "cart:button:back-to-cart:btn:text": "Quay lại giỏ hàng",

    /* User page / Asset */

    /* Admin page / Gui / Personal menu */
    "admin-page:gui:personal-menu:add-new-menu:btn:text": "Thêm mới menu",
    "admin-page:gui:personal-menu:menu:1": "Thông báo của tôi",
    "admin-page:gui:personal-menu:menu:2": "Quản lý đơn hàng",
    "admin-page:gui:personal-menu:menu:3": "Khởi tạo đơn hàng",
    "admin-page:gui:personal-menu:menu:4": "Tra cứu sản phẩm",
    "admin-page:gui:personal-menu:menu:5": "Sản phẩm tôi quản lý",
    "admin-page:gui:personal-menu:menu:6": "Trang quản trị",
    "admin-page:gui:personal-menu:menu:7": "Đăng xuất",
    "admin-page:gui:personal-menu:menu:8": "Đơn hàng của tôi",

    /* User - Main menu */






































    /*User Menu*/
    "user-menu-order-order": "Xem yêu cầu",
    "user-menu-order-list-order": "Các yêu cầu",
    "user-menu-dashboard": "Tổng quang",
    "user-menu-order-pending": "Chờ duyệt",
    "user-menu-asset": "Sản phẩm",

    /*GUI > Resource Category*/
    "gui:product-category:lblName": "Tên",
    "gui:product-category:tbData:lblName": 'Tên Nhóm Dịch Vụ',

    /*GUI > Resource Type*/
    "gui:product-type:tbData:lblName": 'Tên Loại Dịch Vụ',

    /*GUI > Component*/
    "gui:component:tbData:lblName": "Tên Model",
    "gui:component:tbData:lblComponentType": "Loại sản phẩm",
    "gui:component:detailForm:plhName": "Ví dụ: T420, T460",
    "gui:component:detailForm:plhCompobox": "- Chọn một -",
    "gui:resource-type:tbData:lblComponentType": "Loại sản phẩm",
    "gui:component:tbData:lblImage": "Hình ảnh",
    "gui:component:detailForm:plhImage": "Ảnh 256x256, 512x512",
    "gui:component:tbData:lblManufactureName": "Nhà sản xuất",
    "gui:component:detailForm:plhManufactureName": "Ví dụ: Trung Quốc, Ấn Độ",
    "gui:component:tbData:lblComment": "Ghi chú",
    "gui:component:detailForm:plhComment": "Ví dụ: sản phẩm ABC",

    /*GUI > Component Type*/
    "gui:resource-type:tbData:lblName": 'Tên Loại Tài Sản',
    "gui:resource-type:tbData:lblProductType": 'Tên Loại Dịch vụ',
    "gui:resource-type:tbData:lblProductCategory": 'Tên Nhóm Dịch vụ',
    "gui:resource-type:tbData:lblCode": 'Mã',
    "gui:resource-type:tbData:lblNote": 'Ghi Chú',
    "gui:resource-type:tbData:lblIcon": 'Icon',
    "gui:resource-type:detailForm:btnSubmit": 'Đồng ý',
    "gui:resource-type:detailForm:btnCancel": 'Hủy',
    "gui:resource-type:detailForm:plhCompobox": '- Chọn một -',
    "gui:resource-type:detailForm:plhName": 'Ví dụ: Laptop, Desktop',
    "gui:resource-type:detailForm:plhCode": 'Ví dụ: LAP, CPU',
    "gui:resource-type:detailForm:plhNote": 'Nội dung ghi chú',
    "gui:resource-type:detailForm:plhIcon": 'Ảnh 32x32, 64x64',
    "gui:resource-type:popup:btnClose": 'Đóng',

    /*GUI > Main Menu*/
    "gui:mainmenu:lblGetMenuBy": "Menu theo",
    "gui:mainmenu:lblResourceType": "Loại dịch vụ",
    "gui:mainmenu:lblResourceCategory": "Nhóm dịch vụ",
    "gui:mainmenu:plhSelectMenuBy": "- Chọn một -",

    /*Front > Product*/
    "front:product:productlist:btnOrderNow": "Đặt ngay",



    /*User page > delegate*/
    'user-page:delegate:header': 'Tự động Ủy quyền duyệt đơn hàng',
    'user-page:delegate:delegatedDomain': 'Người được ủy quyền',
    'user-page:delegate:startTime': 'Thời điểm bắt đầu',
    'user-page:delegate:endTime': 'Thời điểm kết thúc',
    'user-page:delegate:delegateMode': 'Nhận thông báo',
    'user-page:delegate:active': 'Kích hoạt',
    'user-page:delegate:deactive': 'Ngừng hoạt',
    'user-page:delegate:alert:haveToBefore': 'phải trước',
    'user-page:delegate:alert:required': 'Vui lòng nhập đủ thông tin cần thiết cho trường: ',
    'user-page:delegate:alert:taskSave': 'Cài đặt ủy quyền',

    /* Component */
    'alert:load-failed': 'Xảy ra lỗi trong quá trình tải dữ liệu',
    'alert:failed': 'không thành công',
    'alert:success': 'thành công',


    /* Valid */
    'valid:notNull': 'không thể bỏ trống',

};















