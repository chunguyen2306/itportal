var DIR_URL = "http://itportal:8080/wp-content/themes/IT%20Portal";

var Libs = {};
/**
 * Created by Administrator on 2/27/2017.
 */
function Application() {
    var me = this;
    var modules = [];
    var view = null;
    var model = null;
    var isBinding = false;

    this.currentLoginUser = '';
    this.currentLoginUserDomain = '';

    this.getModel = function () {
        return model;
    };

    this.reinstallModule = function () {
        modules = [];
    };

    this.registerModule = function (arr) {
        if (typeof arr != 'Array') {
            modules.push(arr);
        } else {
            for (var i = 0; i < arr.length; i++) {
                modules.push(arr[i]);
            }
        }
    };

    this.binding = function () {
        if (isBinding) {
            ko.cleanNode(document.body);
            $('*[data-bind]').each(function () {
                var bindingStr = $(this).attr('data-bind');

                if (bindingStr.indexOf('foreach') >= 0) {
                    var template = this.children[0].outerHTML;
                    this.innerHTML = template;
                }
            });
        }
        Promise.all([ko.applyBindings(model)]).then(function () {
            window['alreadyBinding'] = true;
            isBinding = true;
        });

    };

    this.do_Start = function () {
        if (Module.Model && Module.View) {
            model = new Module.Model();
            model._parent = me;
            view = new Module.View(model, function () {
                Component.System.preLoadingPanel.hide();
                me.binding();
            });
        } else {
            console.warn('Model and View is not defined');
            Component.System.preLoadingPanel.hide();
            me.binding();
        }
    };

    this.Start = function () {
        me.currentLoginUser = (function () {
            return wp_current_user;
        })();
        me.currentLoginUserDomain = (function () {
            return wp_current_user_domain;
        })();

        var cartDate = new Date(Libs.LocalStorage.val('cartDate'));

        if (cartDate !== null && cartDate.diff(new Date()).hours > 3) {
            Component.ShopCart.ResetCart();
            Component.AssetsCart.ResetCart();
        }

        Component.ShopCart.UpdateCart();

        $(document).ready(function () {
            me.do_Start();
        });
    }
}/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//TODO Chuyển các định nghĩa prototype vào folder riêng và rip trong phần rip js
if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}
if (!String.prototype.formatMoney) {
    String.prototype.formatMoney = function () {
        return this.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };
}
if (!Number.prototype.formatMoney) {
    Number.prototype.formatMoney = function () {
        return this.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };
}
if (!Date.prototype.diff) {
    Date.prototype.diff = function (date) {
        var diff = date - this;

        var hours = Math.floor(diff / 3.6e6);
        var minutes = Math.floor((diff % 3.6e6) / 6e4);
        var seconds = Math.floor((diff % 6e4) / 1000);
        return {
            hours: hours,
            minutes: minutes,
            seconds: seconds,
        }
    }
}
if (!Date.prototype.format) {
    Date.prototype.format = function (pattern) {
        pattern = pattern.replace(/MM/g, (this.getMonth()+1).toString().padStart(2, '0'));
        pattern = pattern.replace(/DD/g, this.getDate().toString().padStart(2, '0'));
        pattern = pattern.replace(/YYYY/g, this.getFullYear().toString().padStart(2, '0'));
        pattern = pattern.replace(/hh/g, this.getHours().toString().padStart(2, '0'));
        pattern = pattern.replace(/mm/g, this.getMinutes().toString().padStart(2, '0'));
        pattern = pattern.replace(/ss/g, this.getSeconds().toString().padStart(2, '0'));
        return pattern;
    }
}

Object.defineProperty(Object.prototype, 'merge', {
    enumerable: false,
    configurable: false,
    writable: false,
    value: function(obj2, ignore) {
        for (var attrname in obj2) {
            if((ignore && ignore.test(attrname)) || typeof obj2[attrname] === 'function'){
                continue;
            }
            this[attrname] = obj2[attrname];
        }
    }
});
if (!Array.prototype.groupBy) {
    Array.prototype.groupBy = function(fkey, fgroup) {
        if(Array.isArray(this)) {
            return Object.values(this.reduce(function (result, value) {
                result = result || {};

                var key = fkey(value);
                var keyStr = JSON.stringify(key);
                var reVal = (result[keyStr] !== undefined) ? result[keyStr] : key;
                reVal = fgroup(reVal);
                result[keyStr] = reVal;
                return result;
            }, {}));
        } else {
            return null;
        }
    };
}

Object.defineProperty(Array.prototype, 'unique', {
    enumerable: false,
    configurable: false,
    writable: false,
    value: function() {
        var a = this.concat();
        for(var i=0; i<a.length; ++i) {
            for(var j=i+1; j<a.length; ++j) {
                if(a[i] === a[j])
                    a.splice(j--, 1);
            }
        }

        return a;
    }
});
//TODO Review và xóa các hàm không cần dùng
Common = window.Common || {};
Common = {
    clone: function (obj) {
        var result = {};
        for (var name in obj) {
            result[name] = obj[name];
        }
        return result;
    },
    BaseUrl: "/am_ajax/",
    getAllAttr: function (eml) {
        var attributes = {};
        $.each(eml.attributes, function( index, attr ) {
            attributes[ attr.name ] = attr.value;
        } );
        return attributes;
    },
    build_param: function (_vars) {
        var params = Common.getUrlParameters();
        for (var key in _vars) {
            if (_vars[key] === '#ignore') {
                delete params[key];
            } else {
                params[key] = _vars[key];
            }
        }
        var queryString = '';
        for (var key in params) {
            if(typeof(params[key]) === 'function'){
                continue;
            }
            queryString += key + '=' + params[key] + '&';
        }
        return '?' + queryString.substring(0, queryString.length - 1);
    },
    reBinding: function (container, model) {
        ko.cleanNode(container[0]);
        ko.applyBindings(model, container[0]);
    },
    loadPage: function (container, url, callBack) {
        $(container).load(url, callBack);
    },
    callBackDynamic: function (func) {
        if (func !== null && func !== undefined && func !== "" && typeof func === 'function') {
            var args = [];
            func.apply(this, arguments[1]);
        }
    },
    runSynchronize: function (process, params) {
        $.ajaxSetup({async: false});
        self.callBack(process(params));
        $.ajaxSetup({async: true});
    },
    getRealWidth: function (element) {
        return $(element).width() + Common.getTotalPadding(element);
    },
    getTotalPadding: function (element) {
        return $(element).css('pading-left') + $(element).css('pading-right');
    },
    generateUUID: function () {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    },
    getRandomName: function (prefix, nameLength) {
        var keys = 'zaq12wsxcde34rfvbgt56yhnmju78iklo90pZAQWSXCDERFVBGTYHNMJUIKLOP';
        var result = '' + prefix;
        for (var i = 0; i < nameLength; i++) {
            result += keys[Common.random(0, keys.length)];
        }
        return result;
    },
    random: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    getDateToString: function (date, pattern) {
        if (Common.isNull(date)) {
            return "-";
        }
        var processDate = Common.getValueOfObject(date);
        if (Common.isNull(pattern)) {
            pattern = "MM/DD/YYYY";
        }
        var output = "";
        if (processDate) {
            output = moment(processDate).format(pattern);
        }
        return output;
    },
    convertToByte: function (str) {
        var bytes = [];
        for (var i = 0; i < str.length; ++i) {
            bytes.push(str.charCodeAt(i));
        }
        return bytes;
    },
    log: function (obj) {
        if ($.isFunction(obj)) {
            console.log('Object is function');
            console.log(obj());
        } else {
            console.log('Object is plan JS');
            console.log(obj);
        }
    },
    removeItemFromArray: function (item, arr) {

        for (var i = arr.length; i--;) {
            if (arr[i] === item) {
                arr.splice(i, 1);
                break;
            }
        }
    },
    pushItemToArray: function (item, arr) {
        Common.getValueOfObject(arr).push(item);
    },
    GetIndexOnArr: function (prop, value, array) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][prop] === value) {
                return i;
            }
        }
        return -1;
    },
    getIndexOfObject: function (item, arr) {
        arr = Common.getValueOfObject(arr);
        for (var i = 0; i < arr.length; i++) {
            if (Common.compareObject(item, arr[i])) {
                return i;
            }
        }
        return -1;
    },
    getValueOfObject: function (obj) {
        if ($.isFunction(obj)) {
            return Common.getValueOfObject(obj());
        } else {
            return obj;
        }
    },
    setValueOfObject: function (obj, value) {
        if ($.isFunction(obj)) {
            if ($.isFunction(obj())) {
                Common.setValueOfObject(obj(), value);
            } else {
                obj(value);
            }
        } else {
            obj = value;
        }
    },
    checkValueExistedInObject: function (value, obj) {
        var result = false;
        try {
            var props = Object.getOwnPropertyNames(obj);
            for (var i = 0; i < props.length; i++) {
                var propName = props[i];
                var objectValue = Common.getValueOfObject(obj[propName]).toString();
                //console.log(propName+" : "+objectValue);               
                if (objectValue.indexOf(value) !== -1) {
                    result = true;
                    break;
                }
            }

        } catch (e) {

        }
        //console.log("Result: "+result);
        return result;

    },
    compareObject: function (a, b) {
        var aProps = Object.getOwnPropertyNames(a);
        var bProps = Object.getOwnPropertyNames(b);
        if (aProps.length !== bProps.length) {
            return false;
        }
        for (var i = 0; i < aProps.length; i++) {
            var propName = aProps[i];

            // If values of same property are not equal,
            // objects are not equivalent
            if (a[propName] !== b[propName]) {
                return false;
            }
        }
        // If we made it this far, objects
        // are considered equivalent
        return true;
    },
    clonePlainObject: function (obj) {
        var json = Common.toJSON(obj);
        return Utils.jsonToData(json);
    },
    tryToPlainObject: function (obj) {
        if ($.isPlainObject(obj)) {
            return obj;
        } else {
            try {
                return JSON.parse(obj);
            } catch (e) {
                return obj;
            }
        }

    },
    toJSON: function (obj) {
        return ko.toJSON(obj);
    },
    getKoObject: function (json) {
        return ko.mapping.fromJSON(json);
    },
    isNumber: function (obj) {
        obj = Common.getValueOfObject(obj);
        try {
            return $.isNumeric(obj);
        } catch (e) {
            return false;
        }
    },
    iJsonString: function(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    },
    getCurrentUser: function () {
        return Common.getCookie("assetusername");
    },
    callBack: function (func) {
        if (func && !Common.isNull(func) && !Common.isNull(func.name)) {
            //  console.log("Common: Call back function: " + func.name);
        }
        if (func && func !== null && func !== undefined && func !== "" && typeof func === 'function') {
            var args = [];
            for (var i = 1; i < arguments.length; i++) {
                args.push(arguments[i]);
            }
            func.apply(this, args);
        }
    },
    getCallBack: function (func) {
        if (func && !Common.isNull(func) && !Common.isNull(func.name)) {
            // console.log("Common: Call back function: " + func.name);
        }
        if (func && func !== null && func !== undefined && func !== "" && typeof func === 'function') {
            var args = [];
            for (var i = 1; i < arguments.length; i++) {
                args.push(arguments[i]);
            }
            return func.apply(this, args);
        }
        return func;
    },
    checkAdminRole: function (callback, role) {
        if (Common.isNull(role)) {
            role = "admin";
        }
        if (!Common.processCheckRole('default', role)) {
            RedirectPages.AccesDenined();
        } else {
            Common.callBack(callback);
        }
    },
    checkAdmin: function () {
        try {
            var assetroles = Common.getCookie("assetroles");
            var roles = JSON.parse(assetroles);
            for (var j = 0; j < roles.length; j++) {
                if (roles[j].name.toUpperCase() === 'ADMIN') {
                    return true;
                }
            }
            return false;
        } catch (e) {
            return false;
        }


    },
    processCheckRole: function (checkingUserName, role) {
        //console.log("Check user " + checkingUserName + " with role is " + role);
        if (role === 'all') {
            return true;
        } else {
            var userName = Common.getCurrentUser();
            if (checkingUserName === 'default') {
                checkingUserName = Common.getCurrentUser();
            }
            checkingUserName = checkingUserName.trim();
            var check = false;
            if (userName)
                if ((role === 'user' || role === 'User') && userName.toLowerCase() === checkingUserName.toLowerCase()) {
                    check = true;
                } else if (userName === checkingUserName) {
                    if (Common.checkAdmin()) {
                        check = true;
                    } else if (Common.isNull(role)) {
                        check = true;
                    } else {
                        var assetroles = Common.getCookie("assetroles");

                        if (!Common.isNull(assetroles)) {
                            var roles = JSON.parse(assetroles);
                            var checkingRole = Common.getValueOfObject(role);
                            var bindRoles = checkingRole.split(",");

                            for (var i = 0; i < bindRoles.length; i++) {
                                for (var j = 0; j < roles.length; j++) {
                                    if (bindRoles[i].toUpperCase() === roles[j].name.toUpperCase()) {
                                        check = true;
                                        break;
                                    }
                                }
                                if (check) {
                                    break;
                                }
                            }
                        }
                    }
                }
            //console.log("Result: "+check);
            return check;
        }

    },
    signout: function () {
        Common.deleteCookie('assetuser');
        Common.deleteCookie('assetusername');
        Common.deleteCookie('assetroles');
        ApplcationManager.loadMainContent("main/login");
        Utils.loadLoginInfo();
    },
    isNotNull: function (obj) {
        return !Common.isNull(obj);
    },
    isNull: function (obj) {
        //obj = Common.getValueOfObject(obj);
        if (obj === null || obj === undefined || obj === "" || obj === "null" || obj === 'undefined') {
            return true;
        } else {
            return false;
        }
    },
    checkRealtimeKioshTransaction: function () {
        if (!Common.isNull(MODEL)) {
            var isKioshTransaction = Common.getValueOfObject(MODEL.iskiotTransaction);
            if (isKioshTransaction !== null && isKioshTransaction) {
                return true;
            }
        }
        return false;
    },
    parseBoolean: function (obj) {
        obj = Common.getValueOfObject(obj);
        if (!Common.isNull(obj)) {
            if (obj > 0 || obj.toString().toLowerCase() === 'true') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    },
    getUrlParameters: function () {
        var vars = [], hash;
        var fullPath = $(location).attr('search');
        var hashes = fullPath.slice(fullPath.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            if(hash[0] === ''){
                continue;
            }
            vars[hash[0]] = (hash[1] === undefined)?'':hash[1];
        }
        return vars;
    },
    encrypt: function (value) {
        //
        var encrypted = CryptoJS.AES.encrypt(value, encryptKey);

        return encrypted;
    },
    decrypt: function (encrypted) {
        //
        var decrypted = CryptoJS.AES.decrypt(encrypted, encryptKey);
        return decrypted.toString(CryptoJS.enc.Utf8);
    },
    setCookie: function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        var encryptedValue = Common.encrypt(cvalue);
        //
        //console.log("un encrypted: cookie: "+cvalue);
        //console.log("encrypted: cookie: "+encryptedValue);
        document.cookie = cname + "=" + encryptedValue + "; " + expires;
    },
    getCookie: function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ')
                c = c.substring(1);
            if (c.indexOf(name) !== -1) {
                var result = Common.decrypt(c.substring(name.length, c.length));
                result = result.replace(/&quot;/g, '"');
                //
                //console.log(cname+" : "+result);
                return result;
            }

        }
        return "";
    },
    deleteCookie: function (szName) {
        Common.setCookie(szName, '', -1);

    },
    executeApiWithCalbackOnly: function (module, subPath, method, queryString, successCallback) {
        var opt = {
            success: successCallback
        };
        HttpUtils.CallAjaxWithQueryString(method, module, subPath, queryString, opt);
    },
    wait: function(_expr, todo){
        var expr = _expr;
        var letWait = setInterval(function () {
            if(typeof expr === 'function'){
                if(expr() === true) {
                    clearInterval(letWait);
                    todo();
                }
            } else {
                if(expr) {
                    clearInterval(letWait);
                    todo();
                }
            }
        }, 100);
    }
};
var CommonModelFunctions = {
    login: {
        username: "",
        role: {}
    },
    currentRole: {
        id: ko.observable(0),
        description: ko.observable(""),
        isDelete: ko.observable(false),
        name: ko.observable("")
    },
    checkBooleanBinding: function (value) {
        return Common.parseBoolean(value);
    },
    checkArrayBinding: function (arr) {
        var realValue = Common.getValueOfObject(arr);
        if (Common.isNull(realValue)) {
            return false;
        } else if (realValue.length > 0) {
            return true;
        }
        return false;
    },
    checkIsComponentAsset: function (assetItem) {
        var detail = Common.tryToPlainObject(Common.getValueOfObject(assetItem.resDetail()));
        if (detail) {
            var detailComponent = Common.getValueOfObject(detail.IsComponent);
            return detailComponent;
        }
        return false;
    },
    checkObjectBinding: function (obj) {
        obj = Common.getValueOfObject(obj);
        if (Common.isNull(obj)) {
            return false;
        }
        return true;

    },
    getMessageSource: function (key) {
        return i18n[key];
    },
    urlParameters: ko.observable({}),
    getDateTime: function (dateNumber) {
        var date = new Date(dateNumber);
        return date;
    },
    closePopup: function () {
        $(".modal").modal('hide');
        POPUP = null;
        try {
            Webcam.reset();
        } catch (e) {
        }
    },
    closeImageReviewPopup: function () {
        $('#popup-image-container').hide();
    },
    closeConfirm: function () {
        $('#confirm-popup-container').hide();
    },
    processChangeDataTable: function (action, table, timeout) {
        if ($.fn.DataTable.isDataTable(table)) {
            //$(table).DataTable().destroy();

        }
        Common.callBack(action);
        if (Common.isNull(timeout)) {
            timeout = 500;
        }
//        setTimeout(function () {
//            $(table).DataTable();
//        }, timeout);
    }
};
Utils = new function () {

    var t = this;

    var URLParams = {};

    t.getURLValue = function (name) {
        if (URLParams.hasOwnProperty(name))
            return URLParams[name];

        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);

        var re;
        if (!results)
            re = null;
        else if (!results[2])
            re = '';
        else
            re = decodeURIComponent(results[2].replace(/\+/g, " "));

        URLParams[name] = re;
        return re;
    };
    t.getURLValues = function (name) {
        var oGetVars = {};
        if (window.location.search.length > 1) {
            for (var aItKey, nKeyId = 0, aCouples = window.location.search.substr(1).split("&"); nKeyId < aCouples.length; nKeyId++) {
                aItKey = aCouples[nKeyId].split("=");
                oGetVars[decodeURIComponent(aItKey[0])] = aItKey.length > 1 ? decodeURIComponent(aItKey[1]) : "";
            }
        }
        URLParams  = oGetVars;
        return oGetVars;
    };
    t.setURLValue = function (name, value) {
        var values = {};
        values[name] = value;
        t.setURLValues(values);
    }
    t.setURLValues = function (values) {
        var url = window.location.href;
        var hash = location.hash;
        url = url.replace(hash, '');

        for(var name in values){
            var value = values[name];
            var i = url.indexOf("&" + name + "=");
            if(i == -1)
                i = url.indexOf("?" + name + "=");
            if (i != -1) {
                var prefix = url.substring(0, i + 1);
                var iEnd = url.indexOf("&", i + 1) + 1;
                var suffix = iEnd ? url.substring(iEnd) : "";

                if(value == null){
                    if(suffix)
                        url = prefix + suffix;
                    else
                        url = prefix.substr(0, prefix.length - 1);
                }else{
                    url = prefix + name + "=" + value;
                    if(suffix)
                        url = url + "&" + suffix;
                }
            } else {
                if (url.indexOf("?") < 0) {
                    if (value != null)
                        url += "?" + name + "=" + value;
                }
                else {
                    if (value != null)
                        url += "&" + name + "=" + value;
                }
            }

            URLParams[name] = value;
        }

        if (location.href.endsWith(url) == false)
            history.pushState(null, document.title, url);
    }
    t.makeMapForKoArray = function (koArray, mapName, keyName, valueName) {
        var map = {};
        koArray.subscribe(function (data) {
            for(proName in map)
                map[proName] = null;

            koArray().forEach(function (item) {
                map[item[keyName]] = item[valueName];
            });
        });
        koArray[mapName] = map;
    }
    t.isDate = function(d){
        return d!=null && d.getTime != null && (isNaN(d.getTime()) == false);
    }
    t.StringToCodeName = function (str) {
        if (str == null)
            return null;

        str = str.trim().replace(/ +/g, "_").toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        return str;
    }

    t.jsonToData = function (jsonData) {
        try {
            return JSON.parse(jsonData);
        } catch (ex) {
            console.warn(ex);
            console.warn(jsonData);
            return null;
        }
    };
    t.dataToJson = function (obj) {
        return JSON.stringify(obj);
    };
    t.removeFromArray = function (array, element) {
        var i = array.indexOf(element);
        if(i != -1)
            array.splice(i, 1);

        return i;
    }
//---------------------------------------------------




    // kiểm tra chuỗi, nếu dài hơn max ký tự thì trả về thông báo, ngược lại trả về null
    t.ckStrMax = function (name, str, max) {
        if (max == null)
            max = 50;

        if (str != null && str.length > max)
            return "<br>" + name + " không thể dài hơn " + max + " ký tự";

        return "";
    }

    t.ckStrNullMax = function (name, str, max) {
        if (max == null)
            max = 50;

        if (str == null || str == "")
            return "<br>" + name + " không thể bỏ trống";
        else
            return t.ckStrMax(name, str, max);
    }

    t.ckNumNull = function (name, num) {
        if (num == null)
            return "<br>" + name + " không thể bỏ trống";
        else if (isNaN(num))
            return "<br>" + name + " không hợp lệ";
        else
            return "";
    }

    t.ckNull = function (name, num) {
        if (num == null)
            return "<br>" + name + " không thể bỏ trống";
        else
            return "";
    }


//---------------------------------------------------

    t.getRowsOfTable = function (table) {
        return $(table).find('tbody tr');
    };

    t.getAllPermissions = function () {
        var permission = {
            'assetListID.resTypeID': {
                e: ko.observable(true)
            },
            'assetListID.resModelID': {
                e: ko.observable(true)
            },
            'assetListID.resID': {
                e: ko.observable(true)
            },
            'addAsset': {
                e: ko.observable(true)
            },
            'reasonID.name': {
                e: ko.observable(true)
            },
            'userInformationId.username': {
                e: ko.observable(true)
            },
            'userInformationId.department': {
                e: ko.observable(true)
            },
            'userInformationId.phone': {
                e: ko.observable(true)
            },
            'userInformationId.location': {
                e: ko.observable(true)
            },
            'userInformationId.email': {
                e: ko.observable(true)
            }
        };
        return permission;
    };
    t.getEditTableAttributes = function (attribute) {
        var result = [];
        attribute = Common.getValueOfObject(attribute);
        for (var i = 0; i < attribute.User.length; i++) {
            if (Common.parseBoolean(attribute.User[i].editable)) {
                result.push(attribute.User[i].value);
            }
        }
        for (var i = 0; i < attribute.PR.length; i++) {
            if (Common.parseBoolean(attribute.PR[i].editable)) {
                result.push(attribute.PR[i].value);
            }
        }
        for (var i = 0; i < attribute.Asset.length; i++) {
            if (Common.parseBoolean(attribute.Asset[i].editable)) {
                result.push(attribute.Asset[i].value);
            }
        }
        return result;
    };
    t.getChangesOfTwoString = function (oldStr, newStr) {
        if (!Common.isNull(oldStr) && !Common.isNull(newStr)) {
            var splitinput = oldStr.split("\n");
            var splitoutput = newStr.split("\n");
            var inlines = splitinput.length;
            var outlines = splitoutput.length;
            var lines;
            if (outlines > inlines) {
                lines = outlines;
            }
            else {
                lines = inlines;
            }
            var buildoutput = "";
            for (i = 0; i < lines; i++) {
                var testundefined = false;
                if (splitinput[i] == undefined) {
                    buildoutput = buildoutput + diffString("", splitoutput[i]);
                    testundefined = true;
                }

                if (splitoutput[i] == undefined) {
                    buildoutput = buildoutput + diffString(splitinput[i], "");
                    testundefined = true;
                }

                if (testundefined == false) {
                    buildoutput = buildoutput + diffString(splitinput[i], splitoutput[i]);
                }
            }

            return buildoutput;
        }
        return newStr;
    };
    t.getColumsOfARow = function (row) {
        return $(row).find('td');
    };
    t.buildChangedOfTwoObject = function (a, b) {
        if (!Common.isNull(a) && !Common.isNull(b)) {
            var aProps = Object.getOwnPropertyNames(a);
            //var bProps = Object.getOwnPropertyNames(b);
            for (var i = 0; i < aProps.length; i++) {
                var propName = aProps[i];
                var aAttribute = a[propName];
                var bAttribute = b[propName];
                if (!Common.isNull(aAttribute) && !Common.isNull(bAttribute)) {
                    if ($.isPlainObject(aAttribute) && $.isPlainObject(bAttribute)) {
                        Utils.buildChangedOfTwoObject(aAttribute, bAttribute);
                    } else if ($.isArray(aAttribute) || $.isArray(bAttribute)) {
                        //code later if needed
//                    if ($.isArray(bAttribute)) {
//                        var alen = aAttribute.length;
//                        var blen = aAttribute.length;
//                        if (alen === blen) {
//
//                            for (var e = 0; e < alen; e++) {
//                                Utils.buildChangedOfTwoObject(aAttribute[e], bAttribute[e]);
//                            }
//                        } else if (alen < blen) {
//
//                            for (var e = 0; e < alen; e++) {
//                                Utils.buildChangedOfTwoObject(aAttribute[e], bAttribute[e]);
//                            }
//                            for (var f = alen; f < blen; f++) {
//                                bAttribute[f] = Utils.getChangesOfTwoString("", bAttribute[f].toString());
//                            }
//                        } else {
//
//                            for (var e = 0; e < blen; e++) {
//                                Utils.buildChangedOfTwoObject(aAttribute[e], bAttribute[e]);
//                            }
//                        }
//                    }
                    } else {
                        if (!Common.isNull(bAttribute)) {
                            var rootValue = "-";
                            if (!Common.isNull(aAttribute)) {
                                rootValue = aAttribute.toString();
                            }
                            b[propName] = Utils.getChangesOfTwoString(rootValue, bAttribute.toString());
                        }
                    }
                }
            }
        }

    };
    t.detectTableContentChanges = function (oldTable, newTable) {
        var returnNewTable = $(newTable);
        if ($(oldTable).html() !== $(returnNewTable).html()) {
            var newRows = Utils.getRowsOfTable(returnNewTable);
            var oldRows = Utils.getRowsOfTable(oldTable);
            var rowsLength = oldRows.length;
            for (var i = 0; i < rowsLength; i++) {
                var oldCols = Utils.getColumsOfARow(oldRows[i]);
                var newCols = Utils.getColumsOfARow(newRows[i]);
                var totalCols = oldCols.length;
                for (var j = 0; j < totalCols; j++) {
                    var oldValue = $(oldCols[j]).html();
                    var newValue = $(newCols[j]).html();
                    var changedText = Utils.getChangesOfTwoString(oldValue, newValue);
                    $(newCols[j]).html(changedText);
                }
            }
        }
        var container = $('<div></div>').append($(returnNewTable));
        return $(container).html();

    };
    t.builSubFunction = function (options, container) {
        if (!Common.isNull(options)) {
            $('.asset-sub-function').remove();
            var subFunctionDiv = $('<div class="asset-sub-function asset-text-align-right"></div>');
            var listItems = [];
            var item = null;

            if (!Common.isNull(options.view)) {
                item = $('<span class="glyphicon glyphicon-picture asset-color-blue asset-sub-item" aria-hidden="true"></span>');
                $(item).click(function () {
                    Common.callBack(options.view);
                });
                $(item).attr('title', 'View this item');
                listItems.push(item);
            }
            if (!Common.isNull(options.delete)) {
                item = $('<span class="glyphicon glyphicon-remove asset-color-red asset-sub-item" aria-hidden="true"></span>');
                $(item).click(function () {
                    // var content = CommonModelFunctions.getMessageSource('warning_confirm_box_delete_item');
                    //PopupBusiness.viewConfirm(options.delete, content);
//                    var deleteFunction = function () {
//                        Common.callBack(options.delete);
//                    };
                    //PopupBusiness.viewConfirm(options.delete);
                    Common.callBack(options.delete);
                });

                $(item).attr('title', 'Delete this item');
                listItems.push(item);
            }
            for (var i = 0; i < listItems.length; i++) {
                subFunctionDiv.append(listItems[i]);
            }
            $(subFunctionDiv).width($(container).width());
            //$(subFunctionDiv).prependTo(container);
            $(container).prepend(subFunctionDiv);
        }
    };
    t.loading = function () {
        //var spinner= '<div id="loading-container"><div class="spinner asset-center_screen"></div></div>';
        var load = '<div id="loading-container" class="wys_default_tranparent_bg wys_full_screen">'

            + '<div class="asset-center_screen" style="width:300px; height:150px">'
            + '<div id="loading-container"><div class="spinner asset-center_screen"></div></div>'

            + '<div class="progress asset_progress">'
            + '<div id="asset_main_progress_bar" class="progress-bar progress-bar-info" role="progressbar" data-transitiongoal="60">'
            + '</div>'
            + '</div>'
            + '</div>'
            + '</div>';
        var loadingId = $('#loading-container').attr('id');
        if (Common.isNull(loadingId)) {
            $('body').append(load);
        }
    };
    t.buildAlert = function (content, targetId) {
        var result = '<div class="asset-vertical-middle asset_center_text">'
            + '<div class="alert alert-warning">' + content + '</div>'

            + '</div>';
        $('#' + targetId).html(result);
    };
    t.checkImageIsBase64 = function (imageData) {
        var realValue = Common.getValueOfObject(imageData);
        if (realValue.indexOf('base64') !== -1) {
            return true;
        }
        return false;
    };
    t.removeLoading = function () {
        $('#loading-container').remove();
        // console.log('Data loaded!');

    };
    t.loadLoginInfo = function () {
        var cookieUser = Common.getCookie('assetuser');
        if (cookieUser !== "") {
            $('#header-login_detail').html(cookieUser);
            $('#header-login_signout').show();
            var urlInfos = Common.getUrlParameters();
            if (urlInfos.view) {
                ApplcationManager.loadMainContent(urlInfos.view);
            } else {
                ApplcationManager.loadMainContent("main/defaultView");
            }
        } else {
            $('#header-login_detail').html('');
            $('#header-login_signout').hide();
            $('#panel-heading').html(LoginInfomation.Message());

        }
    };
    t.displayInformation = function (infos) {
        $('#panel-heading').html(infos);
    };
    t.buildAssetDetail = function (assetObject) {

        var realValue = Common.getValueOfObject(assetObject);
        var totals = realValue.length;
        var details = totals;
        for (var m = 0; m < totals; m++) {
            details += "-";
            var props = Object.getOwnPropertyNames(realValue[m]);
            for (var i = 0; i < props.length; i++) {
                var propName = props[i];
                var value = realValue[m][propName];
                if (i === props.length - 1) {
                    details += value;
                } else {
                    details += value + ":";
                }
            }
        }
        return details;
    };
    t.buildFormStepAttributes = function (formStepAttributes, attributeName) {
        Utils.buildFormStepAttributePart(formStepAttributes.Asset, attributeName);
        Utils.buildFormStepAttributePart(formStepAttributes.PR, attributeName);
        Utils.buildFormStepAttributePart(formStepAttributes.User, attributeName);
    };
    t.buildFormStepAttributePart = function (arrayValues, attributeName) {
        for (var i = 0; i < arrayValues.length; i++) {
            if (arrayValues[i].value === attributeName) {
                arrayValues[i].editable(true);
            }
        }
    };
    t.displayMainProgressBarStatus = function (percent) {
        $('#asset_main_progress_bar').css('width', percent + '%').attr('aria-valuenow', percent).html(percent + '%');

    };

};

function EditAndValidate(data) {
    var t = this;
    var msg = [];

    t.addMsg = function(str){
        if(str)
            msg.push("<br>", str);
    }
    t.getMessage = function () {
        if (msg.length == 0)
            return "";

        msg.shift();
        return msg.join("");
    }
    t.string = function (proName, name, min, max) {
        var val = data[proName];

        // validate type
        if (val == null) {
            min != null && msg.push("<br>", name, " không thể bỏ trống");
            return;
        }

        if (typeof val != "string") {
            min != null && msg.push("<br>", name, ": dữ liệu không hợp lệ");
            data[proName] = null;
            return;
        }

        // validate length
        val = val.trim();
        data[proName] = val;
        var l = val.length;
        if (min != null) {
            if (l < min) {
                msg.push("<br>", name, " không thể ít hơn ", min, " ký tự");
                return;
            } else if (l == 0) {
                msg.push("<br>", name, " không thể bỏ trống");
                return;
            }
        }

        if (max && l > max)
            msg.push("<br>", name, " không thể nhiều hơn ", max, " ký tự");

    }
    t.date = function (proName, name, min, max, minName, maxName) {
        var val = data[proName];

        // validate type
        if (val == null) {
            min != null && msg.push("<br>", name, " không thể bỏ trống");
            return;
        }

        if (typeof val != "object" || val.getTime == null || isNaN(val.getTime())) {
            min != null && msg.push("<br>", name, ": dữ liệu không hợp lệ");
            data[proName] = null;
            return;
        }

        // validate value
        if (min && val < min) {
            var mName = typeof minName == 'function' ? minName(min) : minName;
            msg.push("<br>", name, " không thể trước ", mName);
            return;
        }

        if (max && val > max) {
            var mName = typeof maxName == 'function' ? maxName(max) : maxName;
            msg.push("<br>", name, " không thể sau ", mName);
            return;
        }
    }
    t.int = function (proName, name, min, max, minName, maxName) {
        var val = data[proName];

        // validate type
        if (val == null) {
            min != null && msg.push("<br>", name, " không thể bỏ trống");
            return;
        }

        if (isNaN(val)) {
            if(val!=null && min != null){
                msg.push("<br>", name, ": dữ liệu không hợp lệ");
                return;
            }
        } else if(typeof val == "string"){
            val = parseInt(val);
            data[proName] = val;
        }

        // validate value
        if (min != null && val < min) {
            var mName = typeof minName == 'function' ? minName(min) : minName;
            msg.push("<br>", name, " không thể lớn hơn ", mName);
            return;
        }

        if (max != null && val > max) {
            var mName = typeof maxName == 'function' ? maxName(max) : maxName;
            msg.push("<br>", name, " không thể nhỏ hơn ", mName);
            return;
        }
    }
}

APP = window.APP || new function () {
        var t = this;
        t.currentUser = 'dev';
    };

/**
 * Created by Administrator on 11/21/2016.
 */
Libs.Cookie = new (function(){
    var encodeMap = '012345wertyuio67m~!@#$%WERTYDFGHlzxc^&JKLZXCỦy quyền duyệt đơn hàngUIOPASk*_89Qvbn=+?VBNMqp<>-(';
    var speraMap = '|{}/\'[]';
    var random = function (max) {
        return Math.floor((Math.random() * max) + 1);
    };
    var encode_cookie = function(cookie_value) {
        // This variable holds the encoded cookie characters
        var coded_string = "";
        var codeArr = [];
        // Run through each character in the cookie value
        for (var counter = 0; counter < cookie_value.length; counter++) {
            codeArr.push(cookie_value.charCodeAt(counter));
        }
        var encrytStr = "";
        for (var i = 0; i < codeArr.length; i++) {
            var item = codeArr[i] + "";
            var code;

            var codeLength = item.length / 2;
            for (var j = 0; j < codeLength; j++) {
                code = item.substr(j * 2, 2);
                if (parseInt(code) > encodeMap.length) {
                    encrytStr += code;
                } else {
                    encrytStr += encodeMap[parseInt(code)];
                }
            }
            if (i < codeArr.length - 1) {
                encrytStr += speraMap[random(4) - 1];
            }
        }
        return encrytStr
    };
    var decode_cookie = function(coded_string) {
        coded_string = unescape(coded_string);
        var decrytStr = "";
        var codeStr = "";
        var enCodeArr = coded_string.replace(/[\|\{\}\/\'\[\]]/g, '*SEPARATION*').split('*SEPARATION*');
        var deCodeArr = [];
        for (var i = 0; i < enCodeArr.length; i++) {
            var item = enCodeArr[i];
            var code = "";
            for (var j = 0; j < item.length; j++) {
                var charItem = item[j];
                if (j < (item.length - 1) && parseInt(charItem) > 7) {
                    code += item[j] + item[j + 1];
                    j++;
                } else {
                    code += encodeMap.indexOf(charItem);
                }
            }
            var intCode = parseInt(code);
            decrytStr += String.fromCharCode(intCode);
        }
        return decrytStr
    };
    var create = function (name, value, days) {
        var expires;

        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = escape(name) + "=" + escape(encode_cookie(value)) + expires + "; path=/";
    };
    var read = function (name) {
        var nameEQ = escape(name) + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0) return unescape(decode_cookie(c.substring(nameEQ.length, c.length)));
        }
        return null;
    };
    var remove = function (name) {
        create(name, "", -1);
    };
    var getsetKey = function (key, value) {
        if (value === undefined) {
            return read(key);
        } else if (value === null) {
            remove(key);
        } else {
            create(key, value);
        }
    };
    this.Key = function(key, value){
        return getsetKey(key, value);
    }
})();
window._prop = function (propName, func) {
    Object.defineProperty(arguments.callee.caller.prototype, propName, { get: func });
};
/**
 * Created by Administrator on 11/21/2016.
 */
Libs = window.Libs || {};

Libs.Http = Libs.Http || { };

Libs.Http.exe = function (url, data, optionCallback, isAsync) {
    if(isAsync === undefined){
        isAsync = false;
    }
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        async: isAsync,
        success: function (data) {
            Libs.JS.callBack(optionCallback.success, data);
        },
        error: function (data) {
          Libs.JS.callBack(optionCallback.error, data);
        }
    });
};
Libs.Http.send = function(opt){
    opt.isAsync = opt.isAsync || true;
    $.ajax({
        type: opt.method,
        url: opt.url,
        data: opt.data,
        headers: opt.header,
        async: opt.isAsync,
        success: function (data) {
            if(opt.isDontParseJson){
                Libs.JS.callBack(opt.success, JSON.parse(data));
            } else {
                Libs.JS.callBack(opt.success, data);
            }

        },
        error: function (data) {
            if(opt.isDontParseJson){
                Libs.JS.callBack(opt.error, JSON.parse(data));
            } else {
                Libs.JS.callBack(opt.error, data);
            }
        }
    });
};
Libs.Http.getScript = function (url, optionCallback) {
    $.getScript( url, function( script, textStatus ) {
      Libs.JS.callBack(optionCallback.success, script);
    });
};










////////////////////////////////////////////////////////////////////////////////////
Module = {
    ITAAPService: "ITAPP",
    CMDB: "CMDB",
    LOGIN: "Login",
    Direct: "direct",
    ISOAPI: "ISOAPI",
    AAPWEB: "AAPWEB"
};
assetUrls = {itServiceBus: 'process'}
SubPath = {
    Asset:"asset",
    TransferForm: "transferForm",
    TransactionForm: 'transactionForm',
    requestForm: 'requestForm',
    DelegateForm: 'delegateForm',
    FormAssetListDetail: "formassetdetail",
    FormNote: "formnote",
    FormStepDefinition: "formstepdefinition",
    FormStep: "formstep",
    Form: "form",
    SystemLog: "systemLog",
    RecoveryForm: "recovery",
    UserRole: "userrole",
    Role: "role",
    FormReason: "formreason",
    FormType: "formtype",
    PrDetail: "pr",
    AssetImageDetail: "assetimage",
    FormstepPermission: "formsteppermission",
    AssetDetailHistory: "assetDetailHistory",
    AssetStatusHistory: "assetStatusHistory",
    MailTemplate: "mailTemplate",
    WebConfig: "webConfig",
    EmployeeTitle: "employeetitle",
    DefaultAsset: "defaultasset",
    TechnicianSite: "technician",
    AssetGeneralInfo: "assetGeneralInfo"

};
RedirectPages = {
    Error: "",
    clear: function () {
        MODEL = null;
        loadModel = null;
        objectBusiness = null;
        POPUP = null;
        objectPopup = null;
    },
    Undefined: function () {
        HEADING_TITLE = i18n.title_redirect_undefined_page;
        RedirectPages.clear();
        ApplcationManager.loadMainContent("system/notfound");
    },
    NotFound404: function () {
        HEADING_TITLE = i18n.title_redirect_notfound_page;
        RedirectPages.clear();
        ApplcationManager.loadMainContent("system/notfound");
    },
    AccesDenined: function () {
        HEADING_TITLE = i18n.title_redirect_access_denined;
        RedirectPages.clear();
        ApplcationManager.loadMainContent("system/acccessDenined");
    }
};
HttpUtils = {};
HttpUtils.viewCMDBResult = function (response, preventPopup) {
    var displayResult = "";
    if (Common.isNull(response)) {
        if (!Common.isNull(preventPopup) && !Common.parseBoolean(preventPopup)) {
            Popup.warning(_L.warning_undefined_result);
        }
        displayResult = "Operation isn't performed";

    } else {
        var responseObject = JSON.parse(response);
        if (!Common.isNull(responseObject.API)
            && !Common.isNull(responseObject.API.response)
            && !Common.isNull(responseObject.API.response.operation)
            && !Common.isNull(responseObject.API.response.operation.result)) {
            var message = responseObject.API.response.operation.result.message;
            var status = responseObject.API.response.operation.result.status;
            displayResult = "Message: " + message + " Status: " + status;
            if (!Common.isNull(preventPopup) && !Common.parseBoolean(preventPopup)) {
                Popup.info( message + "<br/>" + status);
            }
        } else if (!Common.isNull(responseObject.Message)) {
            displayResult = responseObject.Message;
            if (!Common.isNull(preventPopup) && !Common.parseBoolean(preventPopup)) {
                Popup.info(displayResult);
            }
        } else {
            displayResult = _L.warning_undefined_result;
            if (!Common.isNull(preventPopup) && !Common.parseBoolean(preventPopup)) {

                if(location.hostname == 'itportal'){
                    Popup.error(displayResult);
                }
            }
        }
    }
    return displayResult;
};
HttpUtils.executeAjax = function (url, data, optionCallback, isLoading, isSpecialCallback, paramsCallBack, isAsync) {
    if (Common.isNull(isAsync)) {
        isAsync = true;
    }
    var urlBuilder = url;
    if (!Common.isNull(SPECIFIC_URL) && url !== "uploadAssetImage" && url !== "deleteAssetImage") {
        if (url.indexOf("?") === -1) {
            urlBuilder = url + "?" + SPECIFIC_URL;
        } else {
            urlBuilder = url + "&" + SPECIFIC_URL;
        }
    }
    $.ajax({
        type: 'POST',
        url: urlBuilder,
        data: data,
        async: isAsync,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (dataCalback) {
            try {
                if (!Common.isNull(optionCallback.success)) {
                    // console.log('Calback to successful function: ' + optionCallback.success.name);
                    if (Common.parseBoolean(isSpecialCallback)) {
                        // console.log('Special callback');
                        // Utils.loading();
                        Common.callBack(optionCallback.success, dataCalback, paramsCallBack);
                        // Utils.removeLoading();
                    } else {
                        //Utils.loading();
                        Common.callBack(optionCallback.success, dataCalback);
                        //Utils.removeLoading();

                    }
                } else {
                    //console.log('Call ajax is successfull without success handler');
                }
            } catch (err) {
                console.warn("Ajax error callback: " + err.message);
                console.info("URL: " + urlBuilder);
                console.log("Parameters: ");
                console.log(data);
                console.error(err.stack);
                Utils.removeLoading();
            }

        },
        error: function (err) {
            console.error("ajax failed: " + err.responseText);
            if (!Common.isNull(optionCallback.error)) {
                //console.log('Calback to error function: ' + optionCallback.error.name);
                Common.callBack(optionCallback.error, err);
            } else {
                // console.log('Call ajax is fail without error handler');
            }
            Utils.displayInformation(err);
        },
        progress: function (e) {
            if (e.lengthComputable) {
                var pct = (e.loaded / e.total) * 100;
                Utils.displayMainProgressBarStatus(pct);
            } else {
                console.warn('Content Length not reported!');
                Utils.displayMainProgressBarStatus(100);
            }
        }
    }).done(function (res) {
        if (!Common.isNull(optionCallback.done)) {
            // console.log('Calback to error function: ' + optionCallback.done.name);
            Common.callBack(optionCallback.done, res);
        } else {
            // console.log('Call ajax is done without done handler');
        }
    }).always(function () {
        // Utils.removeLoading();
    });
};
HttpUtils.CallAjax = function (methodName, module, subPath, model, optionCallback, isLoading, isAsync) {
    if (!Common.isNull(model)) {
        model.m = methodName;
        model.module = module;
        model.subPath = subPath;
        model.forcePostParam = true;
    }
    var url = Common.BaseUrl;
    HttpUtils.executeAjax(url, model, optionCallback, isLoading, null, null, isAsync);
};
HttpUtils.CallAjaxWithQueryString = function (methodName, module, subPath, query, optionCallback, isLoading, isAsync) {
    if (Common.isNull(subPath)) {
        subPath = "?";
    } else {
        subPath = "?subPath=" + subPath + "&";
    }
    var url = Common.BaseUrl + subPath + 'm=' + methodName + '&module=' + module + '&' + query;
    HttpUtils.executeAjax(url, null, optionCallback, isLoading, null, null, isAsync);
};/**
 * Created by Administrator on 11/21/2016.
 */
Libs.JS = {};
Libs.JS.isNull = function(obj){
    if(obj === null || obj === undefined || obj === ""){
        return true;
    } else {
        return false;
    }
};
Libs.JS.callBack = function (func) {
    if (func !== null && func !== undefined && func !== "" && typeof func === 'function') {
        var args = [];
        for (var i = 1; i < arguments.length; i++)
            args.push(arguments[i]);
        func.apply(this, args)
    }
};
Libs.JS.wait = function(exp, callback){
  var thread = setInterval(function () {
    console.log(exp);
    if(exp){
      clearInterval(thread);
      JS.callBack(callback);
    }
  }, 100);
};/**
 * Created by Administrator on 2/28/2017.
 * Lib: Json
 */
Libs.Json = new (function(){
  this.toJson = function(obj){
    JSON.stringify(obj);
  };
  this.toObject = function(str) {
    JSON.parse(str);
  };
})();/**
 * Created by Administrator on 11/21/2016.
 */
Libs.LocalStorage = new (function(){
    var encodeMap = '012345wertyuio67m~!@#$%WERTYDFGHlzxc^&JKLZXCỦy quyền duyệt đơn hàngUIOPASk*_89Qvbn=+?VBNMqp<>-(';
    var speraMap = '|{}/\'[]';
    var random = function (max) {
        return Math.floor((Math.random() * max) + 1);
    };

    var encode = function(cookie_value) {
        // This variable holds the encoded cookie characters
        var coded_string = "";
        var codeArr = [];
        // Run through each character in the cookie value
        for (var counter = 0; counter < cookie_value.length; counter++) {
            codeArr.push(cookie_value.charCodeAt(counter));
        }
        var encrytStr = "";
        for (var i = 0; i < codeArr.length; i++) {
            var item = codeArr[i] + "";
            var code;

            var codeLength = item.length / 2;
            for (var j = 0; j < codeLength; j++) {
                code = item.substr(j * 2, 2);
                if (parseInt(code) > encodeMap.length) {
                    encrytStr += code;
                } else {
                    encrytStr += encodeMap[parseInt(code)];
                }
            }
            if (i < codeArr.length - 1) {
                encrytStr += speraMap[random(4) - 1];
            }
        }
        return encrytStr
    };
    var decode = function(coded_string) {
        coded_string = unescape(coded_string);
        var decrytStr = "";
        var codeStr = "";
        var enCodeArr = coded_string.replace(/[\|\{\}\/\'\[\]]/g, '*SEPARATION*').split('*SEPARATION*');
        var deCodeArr = [];
        for (var i = 0; i < enCodeArr.length; i++) {
            var item = enCodeArr[i];
            var code = "";
            for (var j = 0; j < item.length; j++) {
                var charItem = item[j];
                if (j < (item.length - 1) && parseInt(charItem) > 7) {
                    code += item[j] + item[j + 1];
                    j++;
                } else {
                    code += encodeMap.indexOf(charItem);
                }
            }
            var intCode = parseInt(code);
            decrytStr += String.fromCharCode(intCode);
        }
        return decrytStr;
    };

    this.removeLocalStorageItem = function (key) {
        localStorage.removeItem(key);
    }

    this.val = function(key, val){
        if(val === undefined){
            if(localStorage.getItem(key) === null || localStorage.getItem(key) === undefined || localStorage.getItem(key) === ''){
                return null;
            } else {
                return localStorage.getItem(key);
            }
        } else {
            localStorage.setItem(key, val);
        }
    }
})();/**
 * Created by tientm2 on 10/18/2017.
 */
Libs = window.Libs || {};

Libs.Location = new (function () {

    this.__defineGetter__('path_param', function(){
        var pathname = location.pathname;
        var regex = /([^\/]+)/g;
        var match;
        var params = [];
        while (match = regex.exec(pathname)){
            params.push(decodeURI(match[1]));
        }

        return params;
    });

    this.__defineGetter__('query_param', function(){

        var vars = [], hash;
        var fullPath = $(location).attr('search');
        var hashes = fullPath.slice(fullPath.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            if(hash[0] === ''){
                continue;
            }
            //vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    });
    
})();

/**
 * Created by tientm2 on 9/12/2017.
 */
var Menu = Menu || {};
Menu.AdminMenu = new (function AdminMenu(){
    var me = this;
    this.isActive = function (link) {
        var path = location.pathname + location.search;

        var result = (path.replace(/^\//,'').replace(/\/$/,'') === link.replace(/^\//,'').replace(/\/$/,''))?'active':'';
        return result;
    }
})();PopupType = {
    info: "popup-base-type-info",
    warning: "popup-base-type-warning",
    error: "popup-base-type-error",

};
PopupResult = PopupButton = {
    OK: 2,
    Retry: 1,
    Cancel: 0,
};
PopupButtons = {
    OK: [0, 0, 1],
    OKCancel: [1, 0, 1],
    OKRetryCancel: [1, 1, 1],
}

function PopupInfo(msg, func, buttons, type) {
    var t = this;

    type = type || PopupType.info;
    var title = func ? "Xác nhận" : "Thông báo";
    var buttonText = ["Hoàn tác", "Thử lại", "Đồng ý"];
    var joContainer;
    var joTitle;
    var joContent;
    var listJOButton;
    var buttonFocus = PopupButton.OK;

    function hotKey(e){
        switch (e.keyCode){
            case 27:
                t.remove();
                break; //  ESC
        }
    }

    t.remove = function(){
        $(document).unbind("keydown", hotKey);
        joContainer.remove();
    }

    t.show = function (joTemplate) {
        joContainer = joTemplate.clone();
        joTitle = joContainer.find(".popup-base-title");
        joContent = joContainer.find(".popup-base-content");
        listJOButton = [];

        { // preparse data
            var btns = joContainer.find(".popup-base-button");
            var n = btns.length;
            for(var i = 0; i < n; i++)
                listJOButton[i] = $(btns[n - 1 - i]);

            buttons = buttons ? buttons : (func ? PopupButtons.OKCancel : PopupButtons.OK);
            func = func || function () { };
        }

        function initButton(button){
            t.setButtonText(button, buttonText[button]);
            var jo = listJOButton[button];
            jo.click(function(){
                t.remove();
                func(button);
            });
            jo.show();
        };
        for (var i = 0; i < buttons.length; i++) {
            if(buttons[i] == false)
                continue;

            initButton(i);
        }

        t.setTitle(title);
        t.setType(type);
        t.setContent(msg);
        t.setButtonFocus(buttonFocus);
        $('body').append(joContainer);
        listJOButton[buttons.length - 1].focus();
        $(window).bind("keydown", hotKey);
        joContainer.click(function(e){
            if(e.target == joContainer[0]){
                t.remove();
                func && func();
            }
        });

        return t;
    };
    t.setTitle = function(val){
        title = val;
        joTitle && joTitle.text(val);
        return t;
    };
    t.setType = function(val){
        joContainer && type && joContainer.removeClass(type);
        type = val;
        joContainer && joContainer.addClass(type);
        return t;
    };
    t.setContent = function(content){
        if(joContent){
            joContent.empty();
            joContent.append(content);
        }
        return t;
    };
    /*
     popupButton:
        example:
            popupButton = PopupButton.OK
            text = 'Yes'
     */
    t.setButtonText = function(popupButton, text){
        buttonText[popupButton] = text;
        listJOButton && listJOButton[popupButton].text(text);
        return t;
    };
    t.setButtonFocus = function(popupButton){
        buttonFocus = popupButton;
        listJOButton && listJOButton[popupButton].focus();
        return t;
    };
};

Popup = new function Popup() {
    var t = this;

    // var baseUrl = "/wp-content/themes/IT%20Portal/commonComponents/";
    var joInput;
    var joInfo;
    function showGetInput(lable, value, onOk, onCancel, onValidate) {
        var jo = joInput.clone();
        var joIP = jo.find("input");

        function cancel(e) {
            onCancel && onCancel(joIP.val());
            jo.remove();
        }

        function ok() {
            var re = joIP.val();
            if (onValidate && onValidate(re) == false)
                return;

            onOk(re);
            jo.remove();
        }

        joIP.val(value);
        jo.find("strong").text(lable);
        jo.find("#popup-input-ok").click(ok);
        jo.find("#popup-input-cancel").click(cancel);
        jo.click(cancel);
        jo.children().click(function (e) {
            e.stopPropagation();
        })
        $("body").append(jo);
    }
    function showInfo(msg, func, buttons) {
        var re = new PopupInfo(msg, func, buttons);
        re = new PopupInfo(msg, func, buttons);
    };
    function loadAndShowPopupInfo(popup) {
        if (joInfo == null) {
            joInfo = $('<div class="popup-wrap popup-base"><div class="popup-box popup-box-sl"><div class="panel-heading' +
                ' popup-base-title"></div><div class="panel-body popup-base-content"></div><div class="panel-heading popup-base-bottom"><button class="btn btn-default popup-base-button" style="display: none;"></button><button class="btn btn-default popup-base-button" style="display: none;"></button><button class="btn btn-default popup-base-button" style="display: none;"></button></div></div></div>');
            popup.show(joInfo);
            // var temp = $("<div>").load(baseUrl + "popup.html", function () {
            //     joInfo = temp.children();
            //     popup.show(joInfo);
            // });
        } else {
            popup.show(joInfo);
        }
        return popup;
    };


    t.info = function (msg, func, buttons) {
        return loadAndShowPopupInfo(new PopupInfo(msg, func, buttons, PopupType.info));
    };
    t.warning = function (msg, func, buttons) {
        return loadAndShowPopupInfo(new PopupInfo(msg, func, buttons, PopupType.warning));
    };
    t.error = function (msg, func, buttons) {
        return loadAndShowPopupInfo(new PopupInfo(msg, func, buttons, PopupType.error));
    };

    t.success = function(){
        t.info("Thành công");
    };
    t.failed = function(){
        t.error("Gặp sự cố khi tải dữ liệu");
    };
    t.errorLoad = function(){
        if(t.errorLoad.isShowing == 1)
            return;

        t.errorLoad.isShowing = 1;
        t.error("Gặp sự cố khi tải dữ liệu", function(){
            t.errorLoad.isShowing = 0;
        });
    };

    t.cleanPopupNode = function () {
        ko.cleanNode($('#mainPopup')[0]);
        ko.applyBindings(POPUP, $('#mainPopup')[0]);
    };

    t.viewConfirm = function (targetAction, content, isPrevented) {
        PopupBusiness.targetAction = targetAction;

        var mainPopup = $('#confirm-popup-container');
        if (mainPopup.attr('id') === undefined) {
            mainPopup = $('<div id="confirm-popup-container" class="modal fade in"></div>');
            $('body').append('<div id="backdrop" class="modal-backdrop fade in"></div>');
            $('body').append(mainPopup);

            if (isPrevented) {
                CommonModelFunctions.loadPage(mainPopup, 'components/confirm.html', loadImageCallack);
            } else {
                CommonModelFunctions.loadPage(mainPopup, 'components/confirm.html', loadImageCallack);
            }
        } else {

            loadConfirmContent(targetAction);
        }

        function loadConfirmContent(targetAction) {
            if (isPrevented) {
                //$('#asset-prevented-sign-out').html();
                $('#confirm-reject').hide();
            } else {
                $('#confirm-reject').show();
            }
            if (Common.isNull(content)) {
                content = "Thao tác này có thể sẽ làm mất dữ liệu và không phục hồi được! <br><b>Bạn chắc chắn muốn thực hiện?</b></br>";
            }
            $('#confirmContent').html(content);
            if (Common.isNull(targetAction)) {
                $('#confirmTitle').html('Thông báo');
            }
            else {
                $('#confirmTitle').html('Cảnh báo');
            }

            if (Common.isNull(targetAction)) {
                $('#confirm-ok').hide();
            } else {
                $('#confirm-ok').show();
            }
            $('#confirm-popup-container').fadeIn('fast');
            $('#confirm-popup-container').show();
            $('#backdrop').fadeIn('fast');
            $('#backdrop').show();


            //debugger;
            $('#confirm-ok')[0].onclick = function () {
                CommonModelFunctions.callBack(PopupBusiness.targetAction);
                CommonModelFunctions.closeConfirm();
                $('#backdrop').hide();
            };

            // hot key
            //$(document)[0].popupHotkey.on();
        }

        function loadImageCallack() {

            // hotkey
            var joDoc = $(document);

            function PopupHotkey() {
                me = this;
                me.btnOK = $('#confirm-ok');
                me.btnReject = $('#confirm-reject');

                me.handler = function (e) {
                    e.preventDefault();

                    switch (e.which) {
                        case 9: //tab
                            if (me.btnOK.is(":focus"))
                                me.btnReject.focus();
                            else
                                me.btnOK.focus();
                            break;

                        case 13: //enter
                            if (me.btnOK.is(":focus"))
                                me.btnOK.click();
                            else
                                me.btnOK.click();

                        case 27: //esc
                            me.btnReject.click();
                            break;
                    }
                };

                me.on = function () {
                    me.btnOK.focus();
                    joDoc.bind("keydown", me.handler);
                };

                me.off = function () {
                    joDoc.unbind("keydown", me.handler);
                };

                me.btnOK.click(me.off);
                me.btnReject.click(me.off);
            };

            joDoc[0].popupHotkey = new PopupHotkey();

            loadConfirmContent(targetAction);

        }
    };

    t.getInput = function (lable, value, onOk, onCancel, onValidate) {
        if (joInput == null) {
            joInput = $("<div>").load("components/input.html", function () {
                joInput = $(joInput.find("div:first-child")[0]);
                showGetInput(lable, value, onOk, onCancel, onValidate);
            });
        } else {
            showGetInput(lable, value, onOk, onCancel, onValidate);
        }
    }

    t.byRequired = function (targetAction) {
        PopupBusiness.viewConfirm(targetAction, 'Bạn chưa điền đủ thông tin');
    };

    t.bySuccess = function (targetAction) {
        PopupBusiness.viewConfirm(targetAction, 'Thành công');
    };

    t.byFailed = function (targetAction) {
        PopupBusiness.viewConfirm(targetAction, 'THẤT BẠI\nXin vui lòng thử lại');
    };

    /*
     input
     info
     confirm
     */
}







var initTableDataElment = function () {
    var emls = $('form-engine');
    emls.each(function () {
        this.tabledata = new TableData(this);
    });
};
function TableData(_eml){
    var me = this;
    var eml = _eml;

    var initTable = function (cols, actions) {
        var table = document.createElement('table');
        var tbHead = document.createElement('thead');
        var tbBody = document.createElement('tbody');
        var headRow = document.createElement('tr');
        var firstRow = document.createElement(tr);
        tbHead.appendChild(headRow);

        cols.find('item').each(function () {
            var headCol = document.createElement('th');
            headCol.innerHTML = i18n(this.getAttribute('header'));
            headRow.appendChild(headCol);
        });

        table.appendChild(tbHead);
        return table;
    };
    this.init = function () {

    }
};
/**
 * Created by tientm2 on 12/18/2014.
 */
/************* DEMO ***********
 *
 * =============================================
 * ================== Syntax ==================
 * =============================================
 *
 * data-bind="VNGSearch: String-Value property,
 *            SearchLimit: Int-Limit result,
 *            SearchQuery: Function-Search function,
 *            SearchQueryParams: Object-Params list,
 *            SeacrhTemplate: String-Display template,
 *            DefaultData: Array-Default data,
 *            DisplayText: String-Text return,
 *            SeacrhValue: String-Value return "
 *
 * =============================================
 * ================== Example ==================
 * =============================================
 * =============== Search with none filter ================
 *    <input type="text" class="VNGSearch form-control"
 *                       data-bind="VNGSearch: assetType,
 *                       SearchLimit: 10,
 *                       SearchQuery: AAPData.CMDB.GetListComponentType,
 *                       SeacrhTemplate: '{Name}',
 *                       DefaultData: $root.requestAsset.AssetTypes,
 *                       DisplayText: 'Name',
 *                       SeacrhValue: 'Name' "/>
 *
 * =============== Search with filter ================
 *  <input type="text" class="VNGSearch form-control"
 *                      data-bind="VNGSearch: assetModel,
 *                      SearchLimit: 10,
 *                      SearchQuery: AAPData.CMDB.GetListComponentModel,
 *                      SearchQueryParams: { type: function(){ return $root.requestAsset.AssetData()[$index()].assetType }, startindex: 0, limit: 10 },
 *                      SeacrhTemplate: '{Name}',
 *                      DefaultData: $root.requestAsset.AssetModels,
 *                      DisplayText: 'Name',
 *                      SeacrhValue: 'Name' "/>
 *
 ************ END DEMO *********/

ko.bindingHandlers.VNGSearch = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var value = valueAccessor();
        var objectStr = valueAccessor.toString().replace(/^function \(\)\{return /g,'').replace(/ \}/g,'');
        var parentObject = objectStr.substring(0, objectStr.lastIndexOf(')'));

        var eml = $(element)[0];
        $(element)[0].VNGSearch = new VNGSearch({
            Input: $(element),
            limit: allBindings.get('SearchLimit') || '-1',
            query: allBindings.get('SearchQuery') || null,
            queryParams: allBindings.get('SearchQueryParams') || null,
            template: allBindings.get('SeacrhTemplate') || null,
            returnValue: allBindings.get('SeacrhValue') || '',
            defaultData: allBindings.get('DefaultData') || null,
            displayText: allBindings.get('DisplayText') || '',
            displayField: allBindings.get('DisplayField') || '',
            onChange: allBindings.get('ChangeEvent') || null
        });
        if (!Common.isNull(allBindings.get('DefaultData'))) {
            $(element).on('click', function () {
                eml.VNGSearch.ShowDefaultData();
            });
        }
        eml.VNGSearch.OnSelected = function () {
            //$(element).val(eml.VNGSearch.SelectedValue.text);
            if ((typeof value !== 'function')) {
                if (Common.isNull(bindingContext.$index)) {
                    eval('viewModel.' + objectStr + ' = "' + eml.VNGSearch.SelectedValue.value + '"');
                    eval('viewModel.' + parentObject + 'viewModel.' + parentObject + '))');
                } else {
                    eval('bindingContext.$data.' + objectStr + ' = "' + eml.VNGSearch.SelectedValue.value + '"');
                }
            } else {
                value(eml.VNGSearch.SelectedValue.value);
                eml.value = eml.VNGSearch.SelectedValue.text;
            }
            if (!Common.isNull(eml.VNGSearch.option.displayField)) {
                var display = eml.VNGSearch.option.displayField;
                var displayObjectStr = display.toString().replace(/^function \(\)\{return /g, '').replace(/ \}/g, '');
                var displayParentObject = displayObjectStr.substring(0, displayObjectStr.lastIndexOf(')'));
                if ((typeof value !== 'function')) {
                    if (Common.isNull(bindingContext.$index)) {
                        console.log(bindingContext.$index);
                        eval('viewModel.' + displayObjectStr + ' = "' + eml.VNGSearch.SelectedValue.text + '"');
                        eval('viewModel.' + displayParentObject + 'viewModel.' + parentObject + '))');
                    } else {
                        eval('bindingContext.$data.' + displayObjectStr + ' = "' + eml.VNGSearch.SelectedValue.text + '"');
                    }
                } else {
                    display(eml.VNGSearch.SelectedValue.text);
                }
            }

            Common.callBack(eml.VNGSearch.option.onChange, bindingContext.item, $(element));
        };
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

    }
};
var VNGSearch = function (option) {
    var me = this;
    this.option = option;
    var ul = $('<ul class="VNGSearchPopup"></ul>');
    if ($('#vng_search_popup').length !== 0) {
        ul = $('#vng_search_popup');
    } else {
        ul.attr('id', 'vng_search_popup');
        $('body').append(ul);
    }

    var changeHandler = [];

    this.addChangeHandler = function (func) {
        changeHandler.push(func);
    };

    var doChangeHandler = function () {
      for(var i = 0; i < changeHandler.length; i++){
          changeHandler[i]();
      }
    };
    this.OnSelected = function () { };
    this.SelectedValue = {};
    var initResize = false;

    this.Resize = function (ul, input) {
        ul.css('top', input.offset().top + input.outerHeight());
        ul.css('left', input.offset().left);
        ul.width(input.outerWidth());

        if (!initResize) {
            $(window).on('resize', function () {
                me.Resize(ul, input);
            });
            initResize = true;
        }
    };
    this.initData = function(data){
        ul.html('');

        for (var i = 0; i < data.length; i++) {
            var itemText = me.option.template;
            var dataItem = data[i];
            if(itemText !== 'null') {
                var keyWord = itemText.match(/\{[^\}]*\}/g);
                for (var j = 0; j < keyWord.length; j++) {
                    var keyWordItem = keyWord[j].replace(/\{/g, '').replace(/\}/g, '');
                    itemText = itemText.replace(keyWord[j], dataItem[keyWordItem]);
                }
            } else {
                itemText = dataItem;
            }

            var li = $('<li></li>');
            li.append(itemText);
            li[0].data = dataItem;
            li.on('click', function () {
                me.SelectedValue = {};
                me.SelectedValue.value = me.option.returnValue !== 'null'?$(this)[0].data[me.option.returnValue]:$(this)[0].data;
                me.SelectedValue.text = me.option.displayText !== 'null'?$(this)[0].data[me.option.displayText]:$(this)[0].data;
                $(me.option.Input).val(me.SelectedValue.text);
                Common.callBack(me.OnSelected);
                if (!Common.isNull(me.option.defaultData)) {
                    if (typeof me.option.defaultData === 'function') {
                        me.initData(me.option.defaultData());
                    } else {
                        me.initData(me.option.defaultData);
                    }
                }
                ul.hide();
                $(me.option.Input).trigger('itemselected');
                $(me.option.Input).trigger('change');
            });
            ul.append(li);

        }
        if (data.length > 0) {
            ul.show();
        } else {
            ul.hide();
        }
    };

    this.Search = function (onseleced) {
        /*if (Common.isNull(me.option.Input.val())) {
            ul.hide();
            return;
        }*/

        if (me.option.queryParams !== null) {
            var args = [];
            args.push(me.option.Input.val());
            for (var name in me.option.queryParams) {
                if (typeof me.option.queryParams[name] === 'function') {
                    args.push(me.option.queryParams[name]());
                } else {
                    args.push(me.option.queryParams[name]);
                }
            }
            args.push(function (data) {
                if(typeof(data) === 'string') {
                    data = Libs.Json.toObject(data);
                }
                me.initData(data);
                me.Resize(ul, me.option.Input);
            });
            Common.callBackDynamic(me.option.query, args);
        } else {
            Common.callBack(me.option.query, me.option.Input.val(), me.option.startIndex, me.option.limit, function (data) {
                if(typeof(data) === 'string') {
                    data = Libs.Json.toObject(data);
                }
                me.initData(data);
                me.Resize(ul, me.option.Input);
            });
        }
    };

    this.ShowDefaultData = function () {
        if (typeof me.option.defaultData === 'function') {
            me.initData(me.option.defaultData());
        } else {
            me.initData(me.option.defaultData);
        }

        me.Resize(ul, me.option.Input);
        ul.show();
    };

//  if (Common.isNull(option.query)) {
//    throw new exception('Null query function');
//  }
    if (Common.isNull(option.startIndex)) {
        option.startIndex = 0;
    }
    if (Common.isNull(option.limit)) {
        option.limit = "";
    }
    if (!Common.isNull(option.defaultData)) {
        option.Input.addClass('DownArrow');
    }
    var isBlur = false;
    var isLiBlur = false;
    ul.on('mouseenter', function () {
        isBlur = false;
    });
    ul.on('mouseleave', function () {
        isBlur = true;
    });

    me.option.Input.on('blur', function () {
        if (isBlur) {
            ul.hide();
        }
    });

    me.option.Input.on('keyup', function () {
        me.Search();
    });

    me.option.Input.on('click', function () {
        if(this.value == ''){
            me.Search();
        }
    });

    Object.defineProperty(me, 'value', { get: function() {
        return me.SelectedValue;
    } });
};

jQuery.fn.extend({
    VNGSeacrh: function (option) {
        return this.each(function () {
            option.Input = $(this);
            this.VNGSearch = new VNGSearch(option);
            $(this).on('keyup', this.VNGSearch.Search);
        });
    }
});
Libs.Workflow = new (function Workflow() {
    var me = this;
    this.ActionType = {
        Lost: {
            RequestName: 'Báo mất thiết bị',
            Mapping: [
                {
                    RequestName: 'nyc',
                    DataName: 'CurrentUser'
                },
                {
                    RequestName: 'nyc',
                    DataName: 'CurrentUser'
                }
            ]
        },
        Return: {
            RequestName: 'Trả thiết bị'
        },
        Damage: {
            RequestName: 'Hư hỏng thiết bị'
        },
        Replace: {
            RequestName: 'Thay thế thiết bị'
        },
        Order: {
            RequestName: 'Yêu cầu tài sản'
        },
        OrderOtherAsset: {
            RequestName: 'Mua thiết bị'
        }
    };
    this.requestList = new (function List() {
        var list = this;
        var data = [];
        var currentIndex = -1;
        this.Add = function (args) {
            if (Array.isArray(args)) {
                data = data.concat(args);
            } else {
                data.push(args);
            }
        };
        this.Next = function () {
            currentIndex++;
            if (currentIndex > (data.length - 1)) {
                return null;
            } else {
                return data[currentIndex];
            }
        };
        this.Has = function () {
            return data.length > 0;
        };
        this.HasNext = function () {
            return (currentIndex + 1) <= (data.length - 1);
        };
        _prop('length', function () {
            return data.length;
        });
    })();
    this.mess = [];
    var onDone = null;

    this.getTableData = function (request, isFull) {
        var tableData = request.TableData;
        tableData = tableData.groupBy(function (e) {
            return {
                AssetType: e[me.getTableFieldName(request, 'assetType', 0)],
                AssetModel: e[me.getTableFieldName(request, 'assetModel', 1)],
                AssetName: e[me.getTableFieldName(request, 'asset', 2)]
            };
        }, function (e) {
            if (e.Count) {
                e.Count = e.Count + 1;
            } else {
                e.Count = 1;
            }
            return e;
        });
        request.TableCount = tableData.length;
        if (isFull) {
            return tableData;
        } else {
            return tableData.slice(0, 3);
        }
    };

    this.getTableFieldName = function (request, type, fieldIndex) {
        var field = request.FieldDefinitions.find(function (e) {
            return e.RequestDataTypeID === APIData.Workflow.RequestDataTypeID[type];
        });
        if(field === undefined){
            return 'Field'+fieldIndex;
        }

        return field.MappingName;
    };

    this.getTableField = function (request, model, type) {
        var field = request.FieldDefinitions.find(function (e) {
            return e.RequestDataTypeID === APIData.Workflow.RequestDataTypeID[type];
        });

        if (field === undefined || field === null || field.length === 0) {
            return '';
        }

        return model[field.MappingName];
    };

    this.getPendingDate = function (date) {
        var actionDate = new Date(date);
        var currentDate = new Date();
        var prefix = '{0}';
        var dateDiff = actionDate.diff(currentDate);
        if (dateDiff.hours == 0 && dateDiff.minutes == 0) {
            prefix = 'Vừa mới đấy';
        } else if (dateDiff.hours == 0 && dateDiff.minutes != 0) {
            prefix = 'Khoảng {0} phút trước';
            dateDiff.days = dateDiff.minutes;
        } else if (dateDiff.hours < 24) {
            prefix = 'Khoảng {0} giờ trước';
            dateDiff.days = dateDiff.hours;
        } else {
            dateDiff.days = Math.ceil(dateDiff.hours / 24);
            prefix = 'Khoảng {0} ngày trước';
        }
        return prefix.format(dateDiff.days);
    };

    this.getOwnedField = function (request) {

        var fieldName = request.FieldDefinitions.find(function (e) {
            return e.Name === request.Definition.OwnerField;
        });

        return request.Fields[fieldName.MappingName];
    };

    var createRequestListItem = function (listChild) {
        var item = {};
        for (var i = 0; i < listChild.length; i++) {
            item[listChild[i].Name] = null;
        }

        return item;
    };

    var createOneWithMappingData = function (_reuqestList) { //actionType, data, _cb) {
        if (_reuqestList.HasNext()) {
            var item = _reuqestList.Next();
            var actionType = item.actionType;
            var data = item.data;
            var requestData = new APIData.Request.RequestData();
            var wfID = 0;
            var mappingRes = [];
            APIData.RequestFeatureMapping.GetByName(actionType, function (res) {
                res = JSON.parse(res);
                wfID = res.RequestID;
                mappingRes = res;
                requestData.loadFieldDefinition(wfID, function () {
                    if (mappingRes !== null && mappingRes != 'null') {
                        var mapping = JSON.parse(mappingRes.Mapping);
                        for (var i = 0; i < mapping.length; i++) {
                            var field = null;
                            if (Array.isArray(requestData.listFieldDefinition)) {
                                field = requestData.listFieldDefinition.find(function (e) {
                                    return e.Name === mapping[i].FieldName;
                                });
                            } else {
                                field = requestData.listFieldDefinition[mapping[i].FieldName];
                            }

                            if (field.listChild !== undefined) {
                                field.value = [];
                                var mappingData = data[mapping[i].MappingName];
                                console.log(data);
                                for (var j = 0; j < mappingData.length; j++) {
                                    var fieldItem = createRequestListItem(field.listChild);
                                    for (var name in fieldItem) {
                                        var mappingChild = mapping[i].Detail.find(function (e) {
                                            return e.FieldName === name;
                                        });
                                        if (mappingChild !== null && mappingChild !== undefined) {
                                            fieldItem[name] = data[mapping[i].MappingName][j][mappingChild.MappingName];
                                        }
                                    }

                                    field.value.push(fieldItem);
                                }

                            } else {
                                field.value = data[mapping[i].MappingName];
                            }
                        }
                        //console.log(requestData.listFieldDefinition);
                        //return;
                        requestData.createRequest(function (res) {
                            if (res.code === AMResponseCode.ok) {
                                me.mess.push({
                                    mess: "Yêu cầu của bạn đã được tạo thành công",
                                    response: res.data,
                                    result: true
                                });

                            } else {
                                me.mess.push({
                                    mess: "Yêu cầu của bạn chưa được tạo, vui lòng liên hệ quản trị viên",
                                    response: requestData,
                                    result: false
                                });
                            }
                            if (me.mess.length === me.requestList.length) {
                                onDone(me.mess);
                            }
                            createOneWithMappingData(_reuqestList);
                        });
                    }
                });
            });
        } else {
            return;
        }
    };

    this.createWithMappingData = function (requests, callback) {
        me.requestList.Add(requests);
        onDone = callback;
        createOneWithMappingData(me.requestList);
    }
})();//TODO REWORK
/**
 * Cần tạo folder Knockout Component chứa tất cả các component của Knockout và rip trong phần rip js vào file min
 */

ko.utils.createUpdateModelHandle = function (valueAccessor, viewModel, bindingContext) {
    var fn;
    var re = function (newValue) {
        if (fn == null) {
            var model = valueAccessor();
            if (ko.isObservable(model)) {
                fn = function (newValue) {
                    if (re.isUpdating)
                        return;


                    re.isUpdating = 1;
                    try {
                        model(newValue);
                    } catch (ex) {
                        throw ex;
                    } finally {
                        re.isUpdating = 0;
                    }
                }
            } else if (typeof model == "function") {
                fn = model;
            } else {
                var modelPathData = valueAccessor.toString().match(/return[\s]+(.+?)[;\s}]/);
                if (modelPathData == null || modelPathData.length != 2 || modelPathData[1].match(/[^\w\.]\(\)/)) {
                    var errorMsg = "binding parse valueAccessor failed: '" + valueAccessor.toString() + "'";
                    console.error(errorMsg);
                    fn = function () {
                    };
                    return;
                }

                var modelPath = modelPathData[1];

                function validateModelPath(parrent) {
                    try {
                        var temp;
                        eval("temp = parrent." + modelPath + ";");
                        return 1;
                    } catch (ex) {
                        return 0;
                    }
                }

                var obj;
                if (validateModelPath(viewModel)) {
                    obj = viewModel;
                } else if (validateModelPath(bindingContext)) {
                    obj = bindingContext;
                } else if (validateModelPath({$context: bindingContext})) {
                    obj = {$context: bindingContext};
                } else if (validateModelPath(window)) {
                    obj = window;
                } else {
                    var errorMsg = "binding missing model: '" + modelPath + "'";
                    console.error(errorMsg);
                    return fn = function () {
                    };
                }

                eval("fn = function(newValue){obj." + modelPath + " = newValue;}");
            }
        }
        fn(newValue);
    };
    return re;
}

/**

 ====== html ============================================
 <div class="search-box" data-bind=" search: <function<item> || model>, searchAPI: <function<keyword, start, limit>>">
 <input class="search-component">
 <button class="search-component"></button>
 <ul class="search-component"></ul>
 </div>

 ====== knockout binding parameter ============================================
 search: model
 searchAPI: function được gọi để lấy dữ liệu
 searchConfig: {
    displayName: string: Tên thuộc tính hiển thị,
    valueName: string: Tên thuộc tính giá trị,
    passNull: boolean: flag để update model khi user select item không có trong dữ liệu
            true(default: displayName != null): update null.
            false(default: displayName == null): update là giá trị user đang nhập
    passValue: boolean: flag để update model khi user select item
            true(default): update null
            false: update là giá trị user đang nhập

  }


 */
ko.bindingHandlers.search = {
    init: function (element, valueAccessor, bindingsAccessor, viewModel, bindingContext) {
        function getComponent(name, before) {
            var re = joContainer.find(name + ".search-component");
            if (re.length == 0) {
                re = $("<" + name + ' class="search-component"/>');
                before ? before.after(re) : joContainer.prepend(re);
            }
            return re;
        }

        var joContainer = $(element);
        var joInput = getComponent("input");
        var joReuslt = getComponent("ul", joInput);
        var jobtnSelect = getComponent("button", joReuslt);
        var model = valueAccessor();
        var bindings = bindingsAccessor();
        var cf = bindings.searchConfig || {};
        var dName = cf.displayName;
        var vName = cf.valueName;
        var updateModel = ko.utils.createUpdateModelHandle(valueAccessor, viewModel, bindingContext);
        var items = new function () {
            var t = this;
            var selectedClass = "search-item-selected";

            t.selectedData = null;
            t.joSelecting = null;
            t.isShow = false;
            t.preventOnNewData = 0;
            t.list = [];

            t.mouseenter = function (e) {
                if (t.joSelecting != null)
                    t.joSelecting.removeClass(selectedClass);

                t.joSelecting = e.data;
                e.data.addClass(selectedClass);
            };

            t.clearSelecting = function () {
                if (t.joSelecting == null)
                    return;

                t.joSelecting.removeClass(selectedClass);
                t.joSelecting = null;
            };

            t.mousedown = function (e) {
                if (e.button != 0 && e.button != undefined)
                    return;

                var doItem = e.currentTarget;
                joInput.val(doItem.innerText);
                selectItem(doItem.data);
            };

            t.nextItem = function () {
                var next = t.joSelecting == null ? joReuslt.children().first() : t.joSelecting.next();
                if (next.length > 0 && next[0].data)
                    t.mouseenter({data: next});
            };

            t.prevItem = function () {
                var prev = t.joSelecting == null ? joReuslt.children().last() : t.joSelecting.prev();
                if (prev.length > 0 && prev[0].data)
                    t.mouseenter({data: prev});
            };

            t.onNewData = function () {
                if (t.preventOnNewData)
                    return;

                t.joSelecting = null;
                t.show();
            };

            t.selectCurrent = function () {
                if (t.isShow && t.joSelecting != null) {
                    t.joSelecting.mousedown();
                } else if (t.selectedData) {
                    selectItem(t.selectedData);
                } else {
                    items.hide();
                    var prvInputText = joInput.val();
                    cf.searchAPI(prvInputText, options.start, options.limit, function (list) {
                        // hủy bỏ nếu user đã update giá trị
                        if (prvInputText != joInput.val())
                            return;

                        if (list.length != 1) {
                            selectItem(null, prvInputText);
                            return;
                        }

                        var data = list[0];
                        if (dName ? (data[dName] == prvInputText) : (data == prvInputText))
                            selectItem(data);
                        else
                            selectItem(null, prvInputText);
                    });
                }
            };

            t.hide = function () {
                if (t.isShow == false)
                    return;

                t.isShow = false;
                joReuslt.css("display", "");
                $(window).unbind("click", t.winHide);
            };

            t.winHide = function () {
                joContainer.thisClick || t.hide();
                joContainer.thisClick = 0;
            }

            t.show = function () {
                if (t.joSelecting)
                    t.mouseenter({data: t.joSelecting});
                else
                    t.nextItem();

                if (t.isShow || joReuslt.children().length < 1)
                    return;

                t.isShow = true;
                joReuslt.css("display", joReuslt.display);
                $(window).bind("click", t.winHide);
            };

        };
        var color = new function () {
            var c = this;
            var orgColor = joInput.css("color");
            var warnColor = "#ffa200";
            var isWarn = 0;

            c.update = function () {
                if (items.selectedData == null) {
                    if (isWarn == 0) {
                        joInput.css("color", warnColor);
                        isWarn = 1;
                    }
                } else {
                    c.off();
                }
            }
            c.off = function () {
                if (isWarn) {
                    joInput.css("color", orgColor);
                    isWarn = 0;
                }
            }
        };
        var resCtrl = new function () {
            var lastID = 1;
            var lastcheckID = 0;

            this.newID = function () {
                return lastID++;
            }

            this.checkID = function(id){
                var re = id > lastcheckID;
                lastcheckID = id;
                return re;
            }
        };

        joReuslt.display = joReuslt[0].style.display || 'inherit';

        // preparse
        {
            if (jobtnSelect.css("display") == "")
                jobtnSelect.css("display", "none");

            var endSearch, options;
            cf.searchAPI = bindings.searchAPI;

            if (cf.hasOwnProperty("passNull") == false) {
                cf.passNull = (dName != null);
            }
            if (jobtnSelect.text() == "")
                jobtnSelect.html("<i class=\"fa fa-search\"></i>");
            if (dName) {
                endSearch = function (res) {
                    var array;
                    if (Array.isArray(res))
                        array = res;
                    else if (Array.isArray(res.data))
                        array = res.data;
                    else
                        return;

                    items.list = array;
                    joReuslt.empty();
                    for (var i = 0; i < array.length; i++) {
                        var item = array[i];
                        var joLi = $("<li>" + item[dName] + "</li>");
                        joLi[0].data = item;
                        joLi.mouseenter(joLi, items.mouseenter);
                        joLi.on('mousedown', items.mousedown);
                        joReuslt.append(joLi);
                    }
                    // if (joReuslt.children().length == 0)
                    //     joReuslt.append("<li>Chưa tìm thấy kết quả</li>");
                    items.onNewData();
                }
            } else {
                endSearch = function (res) {
                    var array;
                    if (Array.isArray(res))
                        array = res;
                    else if (Array.isArray(res.data))
                        array = res.data;
                    else
                        return;

                    items.list = array;
                    joReuslt.empty();
                    for (var i = 0; i < array.length; i++) {
                        var item = array[i];
                        var joLi = $("<li>" + item + "</li>");
                        joLi[0].data = item;
                        joLi.mouseenter(joLi, items.mouseenter);
                        joLi.on('mousedown', items.mousedown);
                        joReuslt.append(joLi);
                    }
                    // if (joReuslt.children().length == 0)
                    //     joReuslt.append("<li>Chưa tìm thấy kết quả</li>");
                    items.onNewData();
                }
            }

            options = {
                start: cf.start || "",
                limit: cf.limit || "",
                endSearch: endSearch
            };
            if (cf.default) {
                var df = cf.default;
                df = typeof df == 'function' ? df() : df;
            }

            joReuslt.css("display", "");
            var btnWidth = jobtnSelect[0].offsetWidth;
            btnWidth > 0 && joInput.css("padding-right", btnWidth + 5);
        }

        function beginSearch(val) {
            items.clearSelecting();
            options.keyword = val;

            var searchID = resCtrl.newID();
            cf.searchAPI(options.keyword, options.start, options.limit, function(res){
                if(resCtrl.checkID(searchID))
                    options.endSearch(res);
            });
        };

        function selectItem(data, str) {
            var d = data;
            var sl = data;
            if (data) {
                if (str && (vName || dName)) {
                    d = {};
                    if (dName)
                        d[dName] = str;

                    if (vName)
                        d[vName] = data;
                } else if (vName) {
                    sl = data[vName];
                }
            } else {
                if (cf.passNull == false) {
                    if (dName) {
                        d = {};
                        d[dName] = str;
                    }

                    if (vName) {
                        d = d || {};
                        d[vName] = null;
                    }

                    (vName == null) && (sl = d);
                    color.update();
                } else {
                    d = {};
                }
            }

            items.selectedData = data;
            updateModel(vName ? d[vName] : d);
            items.hide();
        }

        function setData(data) {
            if (data == null) {
                joInput.val("");
                selectItem(null);
                return;
            }

            var dataIsObject = typeof data == 'object';
            var text = null;
            var searchValue = null;
            if (dataIsObject) {
                text = dName ? data[dName] : '';
                joInput.val(text);
                items.selectedData = data;
                return;
            }

            items.selectedData = null;
            text = searchValue = data;
            var ph = joInput.attr("placeholder") || "";
            var phl = "loading";
            var prvInputText = searchValue;

            var isFocus = 0;

            function onFocus() {
                isFocus = 1;
                //joInput.attr("placeholder", ph);
            }

            function onBlur() {
                isFocus = 0;
                //joInput.attr("placeholder", phl);
                phl = "loading...";
            }

            function end() {
                joInput.unbind("focus", onFocus);
                joInput.unbind("blur", onBlur);
                //joInput.attr("placeholder", ph);
            }

            joInput.val(text);
            joInput.bind("focus", onFocus);
            joInput.bind("blur", onBlur);

            function loadData(isFirst) {
                if (!isFirst)
                    color.update();

                if (isFocus == 0) {
                    phl += ".";
                    //joInput.attr("placeholder", phl);
                }
                cf.searchAPI(searchValue, options.start, options.limit, function (res) {
                    var list = Array.isArray(res) ? res : res.data;

                    // hủy bỏ nếu user đã hoặc đang update giá trị
                    if (joInput.is(":focus") || prvInputText != joInput.val()) {
                        end();
                        return;
                    }

                    // search lại nếu chưa tìm thấy kết quả
                    //if (list == null || list.length == 0) {
                    //    setTimeout(loadData, 500 + Math.random() * 500);
                    //    return;
                    //}

                    end();
                    // update giá trị, nếu ok, return
                    for (var i = 0; list && i < list.length; i++) {
                        var data = list[i];
                        if (
                            vName && (data[vName] == searchValue)
                            || dName && (data[dName] == searchValue)
                            || (data == searchValue)
                        ) {
                            selectItem(data);
                            var dValue = dName ? data[dName] : data;
                            if (joInput.val() != dValue)
                                joInput.val(dValue);
                            return;
                        }
                    }

                    // nếu chưa update được giá trị thì báo lỗi
                    var oColor = joInput.css("color");
                    joInput.css("color", "red");

                    function removeMissingDataStyle() {
                        if (items.selectedData) {
                            joInput.css("color", oColor);
                            joInput.unbind("input", removeMissingDataStyle);
                        }
                    }

                    joInput.bind("input", removeMissingDataStyle);
                });
            }

            setTimeout(loadData, 30, 1);
        }

        // event
        {
            joInput.bind("click", function () {
                //if(items.list && items.list.length){
                //    var selectedVal = null;
                //    if(items.selectedData){
                //        selectedVal = vName ? items.selectedData[vName] : items.selectedData;
                //    }
//
                //    var modelData = typeof model == "function" ? model() : model;
                //    if(modelData == selectedVal)
                //        return;
                //}

                beginSearch(joInput.val());
            });
            joInput.bind("input", function (e) {
                items.selectedData = null;
                beginSearch(joInput.val());
                color.off();
            });
            joInput.bind("keydown", function (e) {
                if (!e)
                    return;

                switch (e.which) {
                    case 38:
                        items.prevItem();  // up
                        break;
                    case 40:
                        items.nextItem();  // down
                        break;
                    case 13:    // enter
                        items.selectCurrent();
                        break;
                }
            });
            joInput.bind("focus", function () {
                items.show();

                if (items.selectedData == null)
                    joInput.css("color", "");
            });
            if (cf.selectByBlur) {
                joInput.bind("blur", function () {
                    var d = items.selectedData;
                    var value = d && (dName ? d[dName] : d);
                    var newValue = joInput.val();
                    if (value != newValue)
                        setData(newValue);
                });
            }
            jobtnSelect.bind("click", items.selectCurrent);
            joContainer.click(function (e) {
                items.isShow && (joContainer.thisClick = 1);

            });
            ko.isObservable(model) && model.subscribe(function (newValue) {
                if (newValue && vName && typeof newValue == 'object') {

                    model(newValue[vName]);
                    return;
                }

                if (updateModel.isUpdating)
                    return;

                setData(newValue);
            });
        }

        if (cf.setHandle) {
            var h = cf;
            h.rebinding = function (searchAPI, model, valueName, displayName) {
                h.searchAPI = searchAPI;
                h.valueName = vName = valueName;
                h.displayName = dName = displayName;

                if (typeof model == "function")
                    updateModel = model;
                else
                    updateModel = ko.utils.createUpdateModelHandle(function () {
                        return model;
                    }, viewModel, bindingContext);
            };
            h.dom = element;
            h.onkeydown = function (e) {
                switch (e.which) {
                    case 38:
                        items.prevItem();  // up
                        break;
                    case 40:
                        items.nextItem();  // down
                        break;
                    case 13:    // enter
                        items.selectCurrent();
                        break;
                }
            }
            h.setHandle(h);
            h.search = beginSearch;
        }

        //set data
        if (model != undefined) {
            if (ko.isObservable(model)) {
                var dfData = ko.unwrap(model);
                if (dfData)
                    setTimeout(setData, 1, dfData);
            } else if (typeof model != "function") {
                setTimeout(setData, 1, model);
            }
        }
    },
};

ko.bindingHandlers.date = {
    init: function (element, valueAccessor, bindingsAccessor, viewModel, bindingContext) {
        var fnUpdate = ko.utils.createUpdateModelHandle(valueAccessor, viewModel, bindingContext);

        element.onchange = function () {
            var date = valueAccessor();
            if (typeof date == "function")
                date = date();

            var valInString = element.value;

            function getNumber(index, length) {
                return parseInt(valInString.substr(index, length));
            }

            var origiTime = date.getTime();
            try {
                date.setFullYear(getNumber(0, 4));
                date.setMonth(getNumber(5, 2));
                date.setDate(getNumber(8, 2));

                if ($(element).attr('type') === 'datetime-local') {
                    date.setHours(getNumber(11, 2));
                    date.setSeconds(getNumber(14, 2));
                }
            } catch (ex) {
                date.setTime(origiTime);
            }
        }
    },
    update: function (element, valueAccessor, bindingsAccessor, viewModel, bindingContext) {
        var date = valueAccessor();
        var val = "";
        if (date) {
            val = date.getFullYear();
            val += "-";
            var temp = date.getMonth().toString();
            val += temp.length == 1 ? "0" + temp : temp;
            val += "-";
            temp = date.getDate().toString();
            val += temp.length == 1 ? "0" + temp : temp;


            if ($(element).attr('type') === 'datetime-local') {
                val += 'T';
                temp = date.getHours().toString();
                val += temp.length == 1 ? "0" + temp : temp;
                temp = date.getSeconds().toString();
                val += temp.length == 1 ? "0" + temp : temp;
            }
        }

        element.value = val;
    }
};

ko.bindingHandlers.ifanimation = {
    init: function (element, valueAccessor, bindingsAccessor, viewModel, bindingContext) {
        var jo = $(element);
        var domStyle = element.style;
        var binds = bindingsAccessor();
        var val = ko.unwrap(binds.value);
        var showHeight, hideHeight;
        var insertContent, clearContent;
        var content = jo.html();
        var originalHeight = domStyle.height;
        var isNoClass = (binds.showClass == null && binds.hideClass == null);
        var isShowing = isNoClass;
        var isInTrans = false;

        if (element.nodeType == Node.COMMENT_NODE) {
            clearContent = insertContent = function () {
                console.error("virtual binding is updating");
            };
        } else {
            insertContent = function () {
                if (jo.html().length)
                    return;

                jo.append(content);
                ko.applyBindingsToDescendants(bindingContext, jo[0]);
            };
            clearContent = function () {
                var children = jo.children();
                for (var i = 0; i < children.length; i++)
                    ko.cleanNode(children[i]);

                jo.empty();
            }
        }

        function initRange() {
            showHeight = hideHeight = jo.height();

            if (isNoClass) {
                hideHeight = 0;
                return;
            }

            // backup
            var preClasses = element.className;
            var prevAnimation = domStyle.animation;
            var prevTransition = domStyle.transition;

            // remove animation
            domStyle.animation = "";
            domStyle.transition = "";

            if (isShowing) {
                jo.addClass(binds.hideClass);
                jo.removeClass(binds.showClass);
                hideHeight = jo.height();
            } else {
                jo.addClass(binds.showClass);
                jo.removeClass(binds.hideClass);
                showHeight = jo.height();
            }

            // restore
            element.className = preClasses;
            domStyle.animation = prevAnimation;
            domStyle.transition = prevTransition;
        }

        function backup() {
            if (isInTrans)
                return;

            var data = backup.data || (backup.data = {});

            if (isShowing) {
                data.height = domStyle.height;
                showHeight = jo.height();
            }
            else {
                hideHeight = jo.height();
            }

            // overflow
            if (!isShowing) {
                data.overflow = domStyle.overflow;
                data.overflowY = domStyle.overflowY;
                domStyle.overflowY = "hidden";
            }

            // transition
            data.noTransition = (jo.css("transition") == null && jo.css("animation") == null);
            if (data.isNoTransition)
                jo.css("transition", '.3s');

            // opacity
            data.noOpacity = (jo.css("opacity") != null);
        }

        function restore() {
            var data = backup.data;
            if (data == null)
                return;

            // overflow
            if (isShowing) {
                domStyle.overflow = data.overflow;
                domStyle.overflowY = data.overflowY;
            }

            // height
            domStyle.height = data.height;

            // transition
            if (data.noTransition)
                jo.css("transition", '');

            // opacity
            if (data.noOpacity)
                jo.css("opacity", '');
        }

        function initTransition() {
            domStyle.transition = binds.transition || '0.3s';
        }

        function show() {
            if (isShowing)
                return;
            showHeight || initRange();

            var height = jo.height();
            if (isInTrans == false) {
                backup();
                hideHeight = height;
            }

            insertContent();

            if (domStyle.height == "")
                jo.height(hideHeight);

            if (isNoClass) {
                initTransition();
            } else {
                jo.addClass(binds.showClass);
                jo.removeClass(binds.hideClass);
            }

            jo.height(showHeight);
            if (backup.data.noOpacity)
                domStyle.opacity = '1';
            isShowing = true;
            isInTrans = true;
        }

        function hide() {
            if (isShowing == false)
                return;

            showHeight || initRange();

            backup();

            if (domStyle.height == "")
                jo.height(showHeight);

            if (isNoClass) {
                initTransition();
            } else {
                jo.addClass(binds.hideClass);
                jo.removeClass(binds.showClass);
            }

            jo.height(hideHeight);
            if (backup.data.noOpacity)
                domStyle.opacity = '.3';
            isShowing = false;
            isInTrans = true;
        }

        jo.bind("transitionend animationend", function () {
            isInTrans = false;
            if (isShowing == false)
                clearContent();
            restore();
        });

        if (isNoClass == false) {
            if (binds.showClass)
                isShowing = jo.hasClass(binds.showClass);
            else if (binds.hideClass)
                isShowing = !jo.hasClass(binds.hideClass);
        }

        element._koShow = show;
        element._koHide = hide;

        if (ko.unwrap(binds))
            binds.hideClass && jo.hasClass(binds.hideClass) && show();
        else
            binds.showClass && jo.hasClass(binds.showClass) && hide();

    },

    update: function (element, valueAccessor, bindingsAccessor, viewModel, bindingContext) {
        ko.unwrap(valueAccessor()) ? element._koShow() : element._koHide();
    }
};

ko.bindingHandlers.numeric = new function () {
    var me = this;
    var key = {
        back: 8,
        del: 46,
        dot: 190,
        A: 65,
        X: 88,
        C: 67,
        V: 86,
    };
    var allowKeys = [
        35, // home
        36, // end
        37, // left
        39, // right
    ];
    var processKey = [
        key.back,
        key.del,
        key.dot,
    ];

    function spliceInput(element, index, count, insert) {
        var jo = element instanceof jQuery ? element : $(element);

        var val = jo.val();
        count = count || 0;

        if (index + count > val.length)
            throw 'Remove input value out of range';

        var left = index ? val.substr(0, index) : '';
        var mid = insert ? insert : '';
        var right = val.substr(index + count);

        jo.val(left + mid + right);

        var carretIndex = left.length + mid.length;
        jo[0].setSelectionRange(carretIndex, carretIndex);
    }

    function format(valBegin, valEnd, numericData) {
        var fnUpdate = numericData.fnUpdate;
        if (fnUpdate && fnUpdate.isUpdating)
            return;

        var binds = numericData.binds;
        var jo = numericData.jo;

        if (!valBegin)
            valBegin = '';
        if (!valEnd)
            valEnd = '';

        var newVal = valBegin + valEnd;
        if (newVal.length == 0) {
            jo.val(newVal);
            return true;
        }

        var carretStart = valBegin.length;

        // format
        var re = [];
        var separate = binds.numericSeparate;
        var index = 0;
        var newCarretIndex = 0;
        var continueSetNewCarretIndex = true;
        var continueSeparate = separate;
        var separateLength = null;
        for (var i = 0; i < newVal.length; i++) {
            // push num
            var ch = newVal[i];

            if (isNaN(ch) && ch != '.')
                continue;

            if (ch == '.') {
                continueSeparate = false;
                separateLength = index;
            }

            if (continueSeparate) {
                if (index && index % 4 == 3) {
                    re.push(separate);
                    index++;
                }
            }

            re.push(ch);
            index++;

            // carret index
            if (continueSetNewCarretIndex) {
                if (i >= carretStart)
                    continueSetNewCarretIndex = false;
                else
                    newCarretIndex = index;
            }
        }

        if (separateLength === null)
            separateLength = re.length;

        var moveSize = (separateLength % 4);
        if (moveSize == 1 || moveSize == 2) {
            moveSize = 3 - moveSize;
            for (var i = 3; i < separateLength; i += 4) {
                for (var j = 0; j < moveSize; j++) {
                    var index = i - j;
                    re[index] = re[index - 1];
                    if (index == newCarretIndex)
                        newCarretIndex++;
                }
                re[i - moveSize] = binds.numericSeparate;
            }
        }


        var valInStr = re.join('');
        jo.val(valInStr);
        jo[0].setSelectionRange(newCarretIndex, newCarretIndex);

        if (fnUpdate) {
            var valInNumber = strToNumber(valInStr);
            fnUpdate(valInNumber);
        }
    };

    function strToNumber(str) {
        if (!str)
            return null;

        var re = [];
        for (var i = 0; i < str.length; i++) {
            var ch = str[i];
            if (isNaN(ch) == false || ch == '.')
                re.push(ch);
        }

        try {
            return parseFloat(re.join(''));
        } catch (ex) {
            return null;
        }
    }

    me.init = function (element, valueAccessor, bindingsAccessor, viewModel, bindingContext) {
        var jo = $(element);
        var binds = bindingsAccessor();
        var fnUpdate = ko.utils.createUpdateModelHandle(valueAccessor, viewModel, bindingContext);

        if (binds.numericSeparate) {
            if (binds.numericSeparate.length > 1)
                throw 'numericSeparate can not be multi charater';

            if (binds.numericSeparate == '.')
                throw 'numericSeparate can not be \'.\'';
        }

        var allowCheckInput = true;
        var numericData = {
            jo: jo,
            binds: binds,
            fnUpdate: fnUpdate
        };
        element.__numneric = numericData;

        jo.on('keydown', function (event) {
            var keyCode = event.keyCode;
            if (isNaN(event.key) && processKey.indexOf(keyCode) == -1) {
                if (keyCode < 32) // control keys
                    return true;

                if (111 < keyCode && keyCode < 124) // function key (F1-12)
                    return true;


                if (allowKeys.indexOf(keyCode) != -1)
                    return true;

                if (event.ctrlKey) {
                    if (keyCode == key.V) {
                        allowCheckInput = true;
                        return true;
                    } else if (keyCode == key.A || keyCode == key.X || keyCode == key.C) {
                        return true;
                    }
                }
                return false;
            }

            var newVal = jo.val();
            var indexOfDot = newVal.indexOf('.');

            var carretStart = this.selectionStart;
            var carretEnd = this.selectionEnd;
            if (carretStart > carretEnd) {
                carretStart = this.selectionEnd;
                carretEnd = this.selectionStart;
            }

            var isDelSelect = carretStart != carretEnd;
            var valBegin = newVal.substr(0, carretStart);
            var valEnd = newVal.substr(carretEnd);

            switch (keyCode) {
                case key.back:
                    if (!isDelSelect && valBegin.length) {
                        var index = valBegin.length - 1;
                        if (index > -1) {
                            var num = 1;
                            if (valBegin[index - 1] == binds.numericSeparate || valBegin[index] == binds.numericSeparate)
                                num++;

                            this.setSelectionRange(carretEnd - num, carretEnd);
                        }

                        //valBegin = valBegin.substr(0, num);
                    }
                    break;
                case key.del:
                    if (!isDelSelect && valEnd.length) {
                        var index = 1;
                        if (index < valEnd.length) {
                            if (valEnd[index - 1] == binds.numericSeparate || valEnd[index] == binds.numericSeparate)
                                index += 1;
                            this.setSelectionRange(carretStart, carretStart + index);
                        }
                        //valEnd = valEnd.substr(index);
                    }

                    break;
                case key.dot:
                    if (binds.numericAllowDot) {
                        if (valBegin.length == 0 || indexOfDot != -1)
                            return false;
                    } else {
                        return false;
                    }

                default:
                    valBegin += event.key;
            }

            return true;
        });

        jo.on('input', function (event) {
            if (allowCheckInput == false) {
                allowCheckInput = true;
                return;
            }

            var val = jo.val();
            var end = this.selectionStart < val.length ? val.substr(this.selectionStart) : '';
            var start = val.substr(0, this.selectionStart);
            format(start, end, numericData);
        });
    }

    me.update = function (element, valueAccessor, bindingsAccessor, viewModel, bindingContext) {
        var val = valueAccessor();
        if (ko.isObservable(val))
            val = val();

        var data = element.__numneric;
        format(val, '', data);
    }
};


/*
 create search model

 _________________________________________________
 method
 setDataSource
 dataSource: list
 searchMember: list of object { Name, IsUnicode}
 -- ex:
 var x; // instance of SearchModel
 var dataSource = [{Value: "v", Display: "đa tềnh tự cổ năng di hận"}];
 var searchMember = [{Name: 'Value', IsUnicode: false}, {Name: 'Display', IsUnicode:true}];
 x.setDataSource(dataSource, searchMember);
 */
function SearchModel(dataSource, searchMember){

    var t = this;
    var _dataSource = [];
    var _searchMember = searchMember;
    var listSearchMethod = [];
    var _unicode = "àáâãèéêìíòóôõùúýăđĩũơưạảấầẩẫậắằẳẵặẹẻẽếềểễệỉịọỏốồổỗộớờởỡợụủứừửữựỳỵỷỹ";
    var _ascii   = "aaaaeeeiioooouuyadiuouaaaaaaaaaaaaeeeeeeeeiioooooooooooouuuuuuuyyyy";

    function getUchar(inputC){
        var minIndex = 0;
        var maxIndex = _unicode.length - 1;
        var i;
        var c;
        while (minIndex <= maxIndex) {
            i = (minIndex + maxIndex) / 2 | 0;
            c = _unicode[i];

            if (c < inputC) {
                minIndex = i + 1;
            }
            else if (c > inputC) {
                maxIndex = i - 1;
            }
            else {
                return _ascii[i];
            }
        }

        return inputC;
    };

    //  to lower. Remove double space, trim
    function optimize1(str){
        try{
            return str.replace(/ +(?= )/g,'').trim().toLowerCase();
        }catch (ex){
            return "";
        }
    };

    // to ascii
    function optimize2(strOpt1) {
        var chars = [];
        var n = strOpt1.length;
        for(var i = 0; i < n; i++)
            chars.push(getUchar(strOpt1[i]));

        return chars.join("");
    };

    t.update = function(i){
        for(var index = 0 ; index  < _searchMember.length; index++){
            var item = _searchMember[index];
            var seachMethod = listSearchMethod[index];
            var mName = item.Name;

            if(item.IsUnicode){
                var strOpt1 = optimize1(_dataSource[i][mName]);
                var strOpt2 = optimize2(strOpt1);
                seachMethod.update(i, strOpt1, strOpt2);
            } else {
                var strOpt1 = optimize1(_dataSource[i][mName]);
                seachMethod.update(i, strOpt1);
            }
        }
    }

    t.setDataSource = function(dataSource, searchMember){
        _dataSource = dataSource;
        _searchMember = searchMember || _searchMember;
        listSearchMethod = [];

        for(var index = 0 ; index  < searchMember.length; index++){
            var item = searchMember[index];
            var n = dataSource.length;
            var srcOpt1 = [];
            var searchMethod;
            var mName = item.Name;

            if(item.IsUnicode){
                var srcOpt2 = [];

                for(var i = 0 ; i  < n; i++){
                    var strOpt1 = optimize1(mName ? dataSource[i][mName] : dataSource[i]);
                    var strOpt2 = optimize2(strOpt1);
                    srcOpt1.push(strOpt1);
                    srcOpt2.push(strOpt2);
                }

                searchMethod = new SearchMethodUnicode(srcOpt1, srcOpt2);
            } else {
                for(var i = 0 ; i  < n; i++){
                    var strOpt1 = optimize1(mName ? dataSource[i][mName] : dataSource[i]);
                    srcOpt1.push(strOpt1);
                }

                searchMethod = new SearchMethodAscii(srcOpt1);
            }
            listSearchMethod.push(searchMethod);
        }
    };

    t.search = function(keyword, callBack){
        var strOpt1 = optimize1(keyword);
        var strOpt2 = optimize2(strOpt1);
        var n = _dataSource.length;
        var lSM = listSearchMethod;
        var nSM = lSM.length;
        var re = [];
        for(var i = 0; i < n; i++){
            for(var iSM = 0; iSM < nSM; iSM++){
                if(lSM[iSM].match(i, strOpt1, strOpt2)){
                    re.push(_dataSource[i]);
                    break;
                }
            }
        }

        setTimeout(callBack, 11, re);
    }

    dataSource && t.setDataSource(dataSource, searchMember);
}

function SearchMethodAscii(srcOpt1){
    var t = this;

    t.match = function(index, strOpt1, strOpt2){
        return srcOpt1[index].indexOf(strOpt1) == -1 ? 0 : 1;
    };

    t.update = function(index, strOpt1){
        srcOpt1[index] = strOpt1;
    }
}

function SearchMethodUnicode(srcOpt1, srcOpt2){
    var t = this;

    t.match = function(index, strOpt1, strOpt2){
        if(srcOpt1[index].indexOf(strOpt1) > -1)
            return 1;

        return srcOpt2[index].indexOf(strOpt2) == -1 ? 0 : 1;
    };

    t.update = function(index, strOpt1, strOpt2){
        srcOpt1[index] = strOpt1;
        strOpt2[index] = strOpt2;
    }
}MODEL = (window.MODEL || {});
var Component = {};
/**
 * Created by lacha on 27/06/2018
 */

Component.AssetsCart = new function() {
  var me = this;
  const assets_cart_name = "listAsset";
  var username = JSON.parse(Libs.LocalStorage.val("username"));

  this.RemoveFormCart = function(assetName, usernameInput) {
    username = JSON.parse(Libs.LocalStorage.val("username"));
    if (usernameInput != username) {
      return;
    }
    var popName = "AssetName";
    var carts = JSON.parse(Libs.LocalStorage.val(assets_cart_name));

    var item = carts.find(function(i) {
      return i[popName] == assetName;
    });
    carts.splice(carts.indexOf(item), 1);
    Libs.LocalStorage.val(assets_cart_name, JSON.stringify(carts));
  };

  this.GetUserName = function() {
    return username;
  };

  this.SetUserName = function(username) {
    Libs.LocalStorage.val("username", JSON.stringify(username));
  };

  this.RemoveAll = function() {
    Libs.LocalStorage.val(assets_cart_name, JSON.stringify([]));
  };

  this.AddToCart = function(asset, usernameInput) {
    username = JSON.parse(Libs.LocalStorage.val("username"));
    if (usernameInput != username) {
      this.SetUserName(usernameInput);
      this.ResetCart();
    }
    var carts = JSON.parse(Libs.LocalStorage.val(assets_cart_name));
    if (carts === undefined || carts === null) {
      carts = [];
    }
    carts.push(asset);
    Libs.LocalStorage.val(assets_cart_name, JSON.stringify(carts));
  };

  this.GetCart = function() {
    var carts = JSON.parse(Libs.LocalStorage.val(assets_cart_name));
    if (carts === undefined || carts === null) {
      carts = [];
    }
    return carts;
  };

  this.ResetCart = function() {
    Libs.LocalStorage.val(assets_cart_name, JSON.stringify([]));
  };
  this.ResetEditCart = function() {
    Libs.LocalStorage.val("listFixItem", JSON.stringify([]));
    Libs.LocalStorage.val("listReturnItem", JSON.stringify([]));
    Libs.LocalStorage.val("listLoseItem", JSON.stringify([]));
  };
  this.GetQuantityAssetSelected = function() {
    return me.GetCart().length;
  };
}();
Component.Custom = Component.Custom || {};
Component.Custom.AutoComplete = new (function () {
    var autoComplete = this;

    var autoCompleteEml = $('<div></div>');
    autoCompleteEml.attr('component', 'autocomplete');

    $(document).ready(function () {
        $('body').append(autoCompleteEml);
    });


    var autoCompleteClass = function (_eml, _data) {
        var me = this;
        var eml = $(_eml);
        var data = _data;
        var currentSelect = 0;

        eml.attr('autocomplete', 'off');
        eml.on('keyup', function (e) {
            autoCompleteEml.html('');
            var value = this.value;
            for (var i = 0; i < data.length; i++) {
                if (data[i].substr(0, value.length).toUpperCase() == value.toUpperCase()) {

                    var item = $('<div></div>');
                    item.addClass('autocomplete-item');
                    item.html("<b>{0}</b>{1}".format(data[i].substr(0, value.length), data[i].substr(value.length)));
                    item.on('mouseover', function () {
                        eml.val(this.innerText);
                    });
                    item.on('click', function () {
                        me.close();
                    });
                    autoCompleteEml.append(item);
                }
            }
            me.show();
        });

        eml.on('keydown', function (e) {
            if (e.keyCode == 40) { //KEY DOWN
                currentSelect++;
            } else if (e.keyCode == 38) { //KEY UP
                currentSelect--;
            } else if (e.keyCode == 13) { //KEY ENTER
                $(autoCompleteEml).find("autocomplete-item:nth-child({0})".format(currentSelect)).click();
            }

            if (currentSelect >= data.length) {
                currentSelect--;
            } else if (currentSelect < 0) {
                currentSelect = 0;
            }
        });

        this.close = function () {
            autoCompleteEml.html('');
            autoCompleteEml.hide();
        };

        this.show = function (){
            autoCompleteEml.css({
                top: eml.offset().top + eml.height(),
                left: eml.offset().left,
                width: eml.width(),
            });
            autoCompleteEml.show();
        };
    };

    this.initTo = function (eml, data) {
        $(eml)[0].AutoComplete = new autoCompleteClass(eml, data);
    }
})();/**
 * Created by tientm2 on 9/20/2017.
 */
Component.Custom = Component.Custom || {};
Component.Custom.ComboBox = new(function () {
    var combobox = this;

    var combobxClass = function (_eml) {
        var me = this;
        var eml = _eml;
        var template = '<div class="wrapper">';
        template +=    '<span class="trigger">'+ eml.attr("placeholder") +'</span>';
        template +=    '<div class="options">';
        template +=    '</div>';
        template +=    '</div>';

        eml.wrap('<div class="component-combobox"></div>');
        eml.hide();
        eml.after(template);

        this.val = function (_val) {
            var option = eml.parent().find('.options span[value="'+_val+'"]');
            var options = $(eml.find('option'));
            options.removeAttr('selected');
            option.attr('selected','selected');
            option.selected = true;
            $(eml.parent().find('.trigger')).html(option.html());
            return $(eml).val();
        };

        eml.parent().find('.trigger').on('click', function () {
            if(eml.parent().hasClass('opened')){
                eml.parent().removeClass('opened');
            } else {
                eml.parent().addClass('opened');
            }
            event.stopPropagation();
        });

        window.addEventListener('click', function (e) {
            if(e.toElement === eml[0]){
                return;
            }
            if(e.toElement.offsetParent !== eml.parent().find('.wrapper .options')[0]){
                if(eml.parent().hasClass('opened')) {
                    eml.parent().removeClass('opened');
                }
            }
        });

        this.bindingItem = function(){
            var options =  eml.parent().find('.options');
            options.html('');
            var isDefaulValue = false;
            if(eml.val() !== ''){
                isDefaulValue = true;
            }
            eml.find('option').each(function (index) {
                var item = $('<span class="'+ $(this)[0].className +'" index="'+ index +'" value="'+$(this).attr('value')+'">'+$(this).html()+'</span>');
                if($(this).attr('value') === eml.val().toString()){
                    $(this).attr('selected','selected');
                    this.selected = true;
                    $(eml.parent().find('.trigger')).html($(this).html());
                }
                item.unbind('click');
                item.on('click', function () {
                    var index = $(this).attr('index');

                    var options = $(eml.find('option'));
                    options.removeAttr('selected');
                    $(options[index]).attr('selected','selected');
                    options[index].selected = true;
                    $(eml.parent().find('.trigger')).html($(this).html());
                    eml.parent().removeClass('opened');

                    $(eml).trigger('change');
                    $(eml).val($(options[index]).val());
                    $(eml).change();

                });
                options.append(item);
            });

        };
    };

    this.bindingItem = function () {
        var emls = $('[component="combobox"]');
        emls.each(function () {
            if(this.ComboBox) {
                this.ComboBox.bindingItem();
            }
        });
    };

    this.init = function init() {
        var emls = $('[component="combobox"]');
        emls.each(function () {
            var eml = $(this);
            if(eml[0].ComboBox === undefined){
                eml[0].ComboBox = new combobxClass(eml);
                eml[0].ComboBox.bindingItem();
            }

        });
    };

    var do_init = function () {
        combobox.init();
        combobox.bindingItem();
    };

    Common.wait(function () {
        return window['alreadyBinding'];
    }, function(){
        if($('[component="form"]').length > 0){
            Common.wait(function () {
                return Component.Custom.Form.isDone;
            }, function(){
                do_init();
            });
        } else {
            do_init();
        }
    });



})();Component.Custom = Component.Custom || {};
Component.Custom.Number = new(function () {
    var number = this;

    this.initEvent = function(){
        $('[component="date-time"]').each(function () {
            var eml = $(this);
            eml.on('click', function () {
                eml.trigger('change');
            });
        });
    };

    this.init = function () {

        $('[component="date-time"]').each(function () {
            var eml = $(this);
            if(eml[0].dateTime === undefined) {
                eml[0].dateTime = true;
            }
        });
    };

    $(document).ready(function () {
        number.init();
    });

    /*Common.wait(function () {
        return window['alreadyBinding'];
    }, function(){
        number.init();
    });*/
})();Component.Custom = Component.Custom || {};
Component.Custom.Editor = new(function () {
    var editor = this;

    var do_init = function () {
        $('[cbtype="editor"]').each(function (){
            var name = $(this).attr('name');
            var height = $(this).attr('height');
            var thisEml = $(this);
            tinymce.init({
                selector: '[name="'+name+'"]',
                height: height,
                menubar: true,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code help'
                ],
                toolbar: 'insert | fontselect  fontsizeselect bold italic backcolor forecolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | formatselect | link | undo redo | help ',
                fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 21pt 22pt 25pt 24pt 26pt 28pt 36pt",
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css'],
                init_instance_callback: function (editor) {
                    editor.on('Change', function (e) {
                        thisEml.trigger('change');
                    });
                }
            });
        });

    };

    this.init = function () {

        if($('[component="form"]').length > 0){
            Common.wait(function () {
                return Component.Custom.Form.isDone;
            }, function(){
                do_init();
            });
        } else {
            do_init();
        }
    };

    $(document).ready(function () {
        editor.init();
    });
})();Component.Custom = Component.Custom || {};
Component.Custom.File = new (function () {
    var file = this;

    var fileClass = function (_eml) {
        var me = this;
        var eml = _eml;
        var file = eml.querySelector('input');
        var label = null;
        var placeHolder = eml.getAttribute('placeholder');
        if (placeHolder) {
            label = $('<span></span>');
            label.text(placeHolder);
            eml.insertBefore(label[0], eml.childNodes[0]);
        }
        this.OnChange = function () {

        };
        var uploadContainer = $('<div></div>');
        label.append(uploadContainer);
        file.addEventListener('change', function () {

            var uploadFiles = this.files;
            var fileName = '';
            var errorMess = '';
            var acceptFiles = {};
            if (uploadFiles.length > 0) {
                for (var key = 0; key < uploadFiles.length; key++) {
                    var res = validFile(uploadFiles[key]);
                    if (res.State) {
                        acceptFiles[key] = uploadFiles[key];
                        fileName += uploadFiles[key].name + ', ';
                    } else {
                        fileName += uploadFiles[key].name + ', ';
                        errorMess += res.Mess + '</br>';
                    }
                }
            }
            if (errorMess !== '') {
                Component.System.alert.show(errorMess + 'Vui lòng xóa hoăc chọn file khác.');
            }
            label.text(fileName);
            me.OnChange();
            if (this.getAttribute('imagereview') !== undefined) {
                me.ShowPrevew(uploadFiles);
            }


        });
        this.uploadResult = [];
        var uploadCount = 0;
        this.hasFile = function () {
            return file.files.length > 0;
        };


        this.getFileContent = function (_cb) {
            if (me.hasFile()) {
                var reader = new FileReader();
                reader.onload = (function (_data) {
                    _cb(_data.target.result);
                });
                reader.readAsText(file.files[0]);
            }
        };

        this.Preview_OnClick = function () {

        };

        this.ShowPrevew = function (files) {
            var previewPanel = $(eml).parent().find('.imagepreview');
            if (previewPanel.length === 0) {
                previewPanel = $('<div class="imagepreview"></div>');
                previewPanel.insertAfter(eml);
            }
            previewPanel.html('');
            if (files !== undefined) {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];

                    var reader = new FileReader();
                    reader.fileName = file.name;
                    reader.onload = function (event) {
                        var img = new Image();
                        img.src = event.target.result;
                        img.alt = this.fileName;
                        img.addEventListener('click', function () {
                            me.Preview_OnClick(this.alt, this);
                        });
                        previewPanel.append(img);
                    };
                    reader.readAsDataURL(file);
                }
            }
            if (eml['data'] !== undefined) {
                var imgData = eml['data'];
                for (var i = 0; i < imgData.length; i++) {
                    var item = imgData[i];
                    var img = new Image();
                    img.src = UploadUrl + '/component/' + eml['dataPath'] + '/' + item;
                    img.alt = item;
                    img.addEventListener('click', function () {
                        me.Preview_OnClick(this.alt, this);
                    });
                    previewPanel.append(img);
                }
            }
        };
        this.isUploadDone = function () {
            return uploadCount === file.files.length;
        };

        var cb_upload = function (res) {
            uploadCount++;
            if (res.State) {
                me.uploadResult.push(res.Result);
            } else {

            }
        };

        var do_upload = function (path, prefix, isChangeName) {
            var files = file.files;
            var errorMess = '';
            for (var i = 0; i < files.length; i++) {
                var validateFile = validFile(files[i]);
                if (validateFile.State) {
                    APIData.UploadFile(path, prefix + '_' + i, files[i], isChangeName, cb_upload);
                } else {
                    errorMess += validateFile.Mess + '<br>';

                }

            }
        };
        this.Reset = function () {
            uploadCount = 0;
            me.uploadResult = [];
        };
        this.Clear = function () {
            delete file.files;
        };
        this.Upload = function (path, prefix, isChangeName) {
            me.Reset();
            isChangeName = isChangeName || false;
            do_upload(path, prefix, isChangeName);
        };
        var validFile = function (file) {
            var types = ['png', 'gif', 'jpeg', 'pjeg', 'pdf', 'doc', 'docx', 'xls', 'xlsx','jpg','bmp'];
            var response = {};
            var type = file.name.split('.').pop().toLowerCase();
            if (!types.includes(type)) {
                response.State = false;
                response.Mess = '<b>' + file.name + '</b>' + ' định dạng file không phù hơp.';
                return response;
            }
            if (file.size > 10440000) {
                response.State = false;
                response.Mess = '<b>' + file.name + '</b>' + ' vượt quá kích thước cho phép(10MB).';
                return response;
            }
            response.State = true;
            response.Mess = '';
            return response;
        }


    };


    var do_init = function () {
        var emls = $('label[type="input-file"]');
        emls.each(function () {
            this['File'] = new fileClass(this);
        });
    };

    this.init = function () {
        if (Component.Custom.Form.hasForm) {
            Common.wait(function () {
                return Component.Custom.Form.isDone;
            }, function () {
                do_init();
            });
        } else {
            do_init();
        }
    };

    Object.defineProperty(file, 'items', {
        get: function () {
            var result = {};
            $('label[type="input-file"]').each(function () {
                result[this.getAttribute('name')] = this.File;
            });
            return result;
        }
    });

    $(document).ready(function () {
        setTimeout(file.init, 100);
    });
})
();Component.Custom = Component.Custom || {};
Component.Custom.Form = new (function () {
    var form = this;
    var formClass = function (_eml) {
        var me = this;
        var eml = _eml;
        var content = $(eml).find('.form-content');
        var formData = {};
        var fields = {};
        var defaultData = {};
        var action = $(eml).attr('action') || null;
        var method = $(eml).attr('method') || null;
        var ignoreList = [];

        if(action !== null){
            $(eml).find('button[type="ajax-submit"]').on('click', function () {
                if(APIData[action] === undefined){
                    console.log('%cForm action "'+action+'" is note defined','color: red');
                    return;
                }

                if(APIData[action][method] === undefined){
                    console.log('%cForm method "'+method+'" is note defined','color: red');
                    return;
                }
                var submitData = {};

                for(var name in formData){
                    if(ignoreList.indexOf(name) < 0){
                        submitData[name] = formData[name];
                    }
                }

                APIData[action][method](submitData, function (res) {
                    $(eml).trigger('ajaxsubmit', [res]);
                })
            });
        }

        this.Submit_OnDone = function () {};

        this.resetData = function () {
            me.val(defaultData);
        };

        var binding = function () {
            for (var name in formData) {
                if (fields[name] !== undefined) {
                    fields[name].setValue(formData[name]);
                }
            }
        };
        this.isValid = function () {
            var valid = true;
            for(var name in formData){
                var fieldValid = me.field(name).isValid();
                valid = valid && fieldValid;
            }
            return valid;
        };
        this.val = function (_val) {
            if (_val) {
                formData = _val;
                binding();
            } else {
                return formData;
            }
        };
        var items = $(eml).find('.form-content item');
        var template = {
            custom: function(config){
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<div value="" ></div>');
                field.setValue = function (_val) {
                    field.attr('value', _val);
                    formData[config.name] = _val;
                };
                field.isValid = function () {
                    return true;
                };

                field.html($(config.eml).html());

                return field;
            },
            hidden: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<input cbType="hidden" type="hidden" />');
                field.setValue = function (_val) {
                    field.val(_val);
                    formData[config.name] = $(config.eml).attr('default') || _val;
                };
                field.isValid = function () {
                    return true;
                };

                if($(config.eml).attr('default')){
                    field.val($(config.eml).attr('default'));
                    formData[config.name] = $(config.eml).attr('default');
                }

                return field;
            },
            text: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<input cbType="text" type="text" />');
                field.on('change', function () {
                    formData[config.name] = $(this).val();
                });
                if(config.currency){
                    field.on('keyup', function () {
                        this.value = this.value.replace(/,/g,'').formatMoney();
                        formData[config.name] = this.value.replace(/,/g,'');
                    });
                }
                field.setValue = function (_val) {
                    field.val(_val);
                };
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else if (field.val() !== '') {
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }
                };
                return field;
            },
            number: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<input cbType="text" type="number" />');
                field.on('change', function () {
                    formData[config.name] = this.value.replace(/[^0-9]/g,'');
                });

                /*if(config.currency){
                    field.on('keyup', function () {
                        this.value = this.value.replace(/,/g,'').formatMoney();
                        formData[config.name] = this.value.replace(/,/g,'');
                    });
                }*/

                field.setValue = function (_val) {
                    field.val(_val);
                };
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else if (field.val() !== '') {
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }
                };
                return field;
            },
            file: function (config) {
                formData[config.name] = [];
                defaultData[config.name] = [];
                var field = $('<label type="input-file"><input cbType="file" type="file" /></label>');
                var accept = config.eml.getAttribute('accept');
                var multiple = config.eml.getAttribute('multiple');
                var imagereview = config.eml.getAttribute('imagereview');

                if (accept && accept != '') {
                    field.find('input').attr('accept', accept);
                }
                if (multiple == '') {
                    field.find('input').attr('multiple', '');
                }
                if (imagereview == '') {
                    field.find('input').attr('imagereview', '');
                }
               //
                field.find('input').on('change', function () {
                    formData[config.name] = this.files;
                });
                field.setValue = function (_val) {
                    field[0]['data'] = _val.data;
                    field[0]['dataPath'] = _val.dataPath;
                    field[0].File.ShowPrevew();
                };
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }
                };
                return field;
            },
            ricktext: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<textarea cbType="rickText" ></textarea>');
                field.on('change', function () {
                    formData[config.name] = $(this).val();
                });

                field.setValue = function (_val) {
                    field.val(_val);
                };
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else if (field.val() !== '') {
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }

                };
                return field;
            },
            combobox: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<select cbType="combobox" component="combobox" ></select>');

                if (config.eml.hasAttribute('data')) {
                    var dataText = config.eml.getAttribute('data-text');
                    var dataValue = config.eml.getAttribute('data-value');
                    var option = $('<option data-bind="text: ' + dataText + ', attr:{ value: ' + dataValue + '}" ></option>');

                    field.append(option);
                }
                field.on('change', function () {
                    formData[config.name] = $(this).val();
                });
                field.setValue = function (_val) {
                    field[0].ComboBox.val(_val);
                };
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }
                };
                return field;
            },
            editor: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<textarea cbtype="editor" ></textarea>');
                field.on('change', function () {
                    formData[config.name] = tinymce.get(config.name).getContent();
                });
                field.attr('height', config.height);
                field.setValue = function (_val) {
                    field.val(_val);
                    tinymce.get(config.name).setContent(_val);
                };
                field[0].reset = function () {
                    tinymce.get(config.name).setContent('');
                };
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }
                };
                return field;
            },
            imagepicker: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<img width="120px" height="120px" component="imagepicker" />');
                field.attr('field', config.eml.getAttribute('field'));
                field.setValue = function (_val) {
                    if(_val !== null && _val !== undefined) {
                        field[0]['data'] = _val.data;
                        field[0]['dataPath'] = _val.dataPath;
                        formData[config.name] = _val.data;
                        field.trigger('update');
                    }
                };
                field.on('change', function () {
                    formData[config.name] = this.alt;
                });
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }
                };
                return field;
            },
            checkbox: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<input type="checkbox" component="checkbox" />');

                if(config.class){
                    field.addClass(config.class);
                }

                field.setValue = function (_val) {
                    this.checked = _val;
                    if(_val){
                        $(this).attr('checked', 'checked');
                    } else {
                        $(this).removeAttr('checked');
                    }

                };
                field.on('change', function () {
                    formData[config.name] = this.checked;
                });
                field.isValid = function () {
                    return true;
                };
                return field;
            },
            rate: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<span component="rate"></span>');
                field.attr('max', $(config.eml).attr('max'));
                field.attr('symbol', $(config.eml).attr('symbol'));
                field.setValue = function (_val) {
                    $(this).attr('value',_val);
                };
                field.on('change', function () {
                    formData[config.name] = $(this).attr('value');
                });
                field.isValid = function () {
                    return true;
                };
                return field;
            },
            search: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var name = config.eml.getAttribute('name');
                var searchhandler = config.eml.getAttribute('searchhandler');
                var searchlimit = config.eml.getAttribute('searchlimit');
                var searchtemplate = config.eml.getAttribute('searchtemplate');
                var displaytext = config.eml.getAttribute('displaytext');
                var searchvalue = config.eml.getAttribute('searchvalue');
                var field = $('<input type="text" ' +
                    'class="VNGSearch form-control" ' +
                    'data-bind="VNGSearch: '+name+', ' +
                    'SearchQuery:'+searchhandler+', ' +
                    'SearchLimit:'+searchlimit+', ' +
                    'SeacrhTemplate:\''+searchtemplate+'\', ' +
                    'DisplayText:\''+displaytext+'\', ' +
                    'SeacrhValue:\''+searchvalue+'\'" />');
                field.setValue = function (_val) {
                    this.value = _val;
                };
                field.on('itemselected', function () {
                    formData[config.name] = this.VNGSearch.SelectedValue.value;
                });
                field.on('change', function () {
                    formData[config.name] = $(this).val();
                });
                field.on('focusout',function () {
                    formData[config.name] = $(this).val();
                });
                field.isValid = function () {
                    return true;
                };
                return field;
            },
            select: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var multiple = config.eml.getAttribute('multiple');
                var fieldTemplate = config.eml.innerHTML;
                var field = null;
                if(fieldTemplate !== ''){
                    field = $('<select>'+fieldTemplate+'</select>');
                } else {
                    field = $('<select></select>');
                }
                if(multiple !== null){
                    field.attr('multiple', '');
                }
                field.setValue = function (_val) {
                    $(this).val(_val);
                };
                field.on('change', function () {
                    formData[config.name] = $(this).val();
                });
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }
                };
                return field;
            },
            'date-time': function(config){
                var field = $('<input type="datetime-local">');
                field.on('change', function(){
                    var val = field.val();
                    formData[config.name] = val ? val.replace('T', ' ') : null;
                });

                field.setValue = function (_val) {
                    if(_val){
                        if(_val.getTime){
                            var strVal = _val.getFullYear();
                            strVal +='-';
                            strVal += (_val.getMonth() + 1).toString().padStart(2, '0');
                            strVal +='-';
                            strVal += _val.getDate().toString().padStart(2, '0');
                            strVal +='T';
                            strVal += _val.getHours().toString().padStart(2, '0');
                            strVal +=':';
                            strVal += _val.getMinutes().toString().padStart(2, '0');

                            field.attr('value',strVal);
                            formData[config.name] = strVal ? strVal : null;
                        }else{
                            field.attr('value',_val.replace(' ', 'T'));
                        }
                    }else{
                        field.attr('value','');
                    }
                };

                field.isValid = function () {
                    return true;
                };

                return field;
            }
        };
        this.field = function (name) {
            return fields[name];
        };

        items.each(function (i, item) {
            var name = item.getAttribute('name');
            var text = item.getAttribute('text');
            var type = item.getAttribute('type');
            var placeHolder = item.getAttribute('placeholder');
            var data = item.getAttribute('data');
            var height = item.getAttribute('height');
            var width = item.getAttribute('width');
            var disabled = item.getAttribute('disabled');
            var _class = item.getAttribute('class');
            var require = item.getAttribute('require');

            if(item.getAttribute('ignore')){
                ignoreList.push(name);
            }

            var formItem = $('<div class="form-line"></div>');
            var fieldLabel = $('<span class="field-label"></span>');
            fieldLabel.text(i18n(text));

            if(width === '' || width === undefined || width === null){
                width = '100%';
            }

            var config = {
                eml: item
            };
            config.merge(Common.getAllAttr(item));
            var field = template[type](config);

            if (placeHolder && placeHolder != '') {
                field.attr('placeholder', i18n(placeHolder));
            }

            if (disabled != null || disabled != undefined) {
                field.attr('disabled', 'disabled');
            }

            if (require != undefined) {
                field.attr('require', require);
                fieldLabel.attr('require', require);
            }

            if (data && data != '') {
                field.attr('data-bind', data);
            }
            field.attr('name', name);
            formItem.css({width: width});
            formItem.append(fieldLabel);
            formItem.append(field);

            if(type === 'hidden'){
                formItem.addClass('hidden');
            }

            field.fieldType = type;
            $(item).after(formItem);
            item.remove();
            fields[name] = field;
        });
    };
    this.hasForm = false;
    this.isDone = false;
    this.get = function (name) {
        return $('[component="form"][name="' + name + '"]')[0].Form;
    };

    Object.defineProperty(form, 'items', { get: function() {
            var result = {};
            $('[component="form"]').each(function () {
                result[this.getAttribute('name')] = this;
            });
            return result;
        }});

    this.init = function () {
        var emls = $('[component="form"]');

        if (emls.length > 0) {
            this.hasForm = true;
        }
        emls.each(function () {

            this['Form'] = new formClass(this);
        });
        form.isDone = true;
    };

    $(document).ready(function () {

        form.init();
    });

})();Component.Custom = Component.Custom || {};
Component.Custom.ImagePicker = new(function () {
    var editor = this;

    var ImagePickerClass = function ImagePickerClass(_eml) {
        var me = this;
        var eml = $(_eml);
        eml.on('update', function () {
            eml[0].src = UploadUrl+'/component/'+eml[0]['dataPath']+'/'+eml[0]['data'];
            eml[0].alt = eml[0]['data'];
        });
        eml.on('click', function () {
           if(eml.attr('field') !== undefined){
               var form = eml.parent().parent().parent().parent()[0].Form;
               var imgField = form.field(eml.attr('field'));
               var imgs = form.field(eml.attr('field'))[0].files;

               var pickerPopup = $('[component="pickerpopup"]');

               if(pickerPopup.length === 0){
                   pickerPopup = $('<div component="pickerpopup" ></div>');
                   var btnClose = $('<span><i class="fa fa-close"></i></span>');
                   var imgList = $('<div class="img-list"></div>');
                   btnClose.on('click', function () {
                       pickerPopup.hide();
                   });
                   pickerPopup.append(btnClose);
                   pickerPopup.append(imgList);
                   $('body').append(pickerPopup);

               }
               pickerPopup.find('.img-list').html('');

               if(imgField.parent().find('.imagepreview').length > 0){
                   pickerPopup.find('.img-list').html((function () {
                       return imgField.parent().find('.imagepreview').html();
                   })());
               } else {
                   for (var i = 0; i < imgs.length; i++) {
                       var item = imgs[i];
                       var reader = new FileReader();
                       reader.onload = function (event) {
                           img = new Image();
                           img.onload = function () {
                               pickerPopup.find('.img-list').append(this);
                           };
                           img.src = event.target.result;
                           img.alt = item.name;
                       };
                       reader.readAsDataURL(item);
                   }
               }

               pickerPopup.find('.img-list').find('img').on('click', function () {

                   eml[0].src = this.src;
                   eml[0].alt = this.getAttribute('alt');
                   pickerPopup.hide();
                   eml.trigger('change');
               });

               pickerPopup.show();
           }
        });
    };

    var do_init = function () {
        $('[component="imagepicker"]').each(function () {
            this.ImagePicker = new ImagePickerClass(this);
        });
    };

    this.init = function () {

        if($('[component="form"]').length > 0){
            Common.wait(function () {
                return Component.Custom.Form.isDone;
            }, function(){
                do_init();
            });
        } else {
            do_init();
        }
    };

    $(document).ready(function () {
        editor.init();
    });
})();Component.Custom = Component.Custom || {};
Component.Custom.LocalPaging = new (function () {
  var localPaging = this;

  var localPagingClass = function (_eml) {
    var me = this;
    var eml = _eml;

    var pageCount = 0;
    var currentPage = 1;
    var pageItemLimit = 5;
    this.pageCount = function (_val) {
      if (_val) {
        pageCount = _val;
        on_localPaging();
      } else {
        return pageCount;
      }
    };

    this.currentPage = function (_val) {
      if (_val) {
        currentPage = _val;
      } else {
        return currentPage;
      }
    };

    this.pageItemCount = function (_val) {
      if (_val) {
        pageItemLimit = _val;
      } else {
        return pageItemLimit;
      }
    };

    this.changeActivePageToPrevious = function() {
      let activeChild = $(eml).find('span.active');
      let newActiveChild = $(eml.find('span[id=' + (currentPage - 1) + ']'));
      if(newActiveChild) {
        currentPage--;
        activeChild.removeClass('active');
        newActiveChild.addClass('active');
        do_PageChange(newActiveChild);
        me.on_PageChange(me);
      }
    }

    this.on_PageChange = function () { /* runtime implement */
    };

    var do_PageChange = function (pageItem) {
      pageItem = $(pageItem);
      pageItem.addClass('active');
      if (pageCount <= pageItemLimit) {
        return;
      }
      if (pageItem.hasClass('next')) {
        if (currentPage + 1 <= pageCount) {
          currentPage += 1;
        }
      } else if (pageItem.hasClass('previous')) {
        if (currentPage - 1 >= 1) {
          currentPage -= 1;
        }
      } else {
        currentPage = parseInt(pageItem.text());
      }

      changePage();
    };

    var changePage = function () {
      var start, end, intervalItem;
      rangeIndex = Math.floor(currentPage / pageItemLimit);

      intervalItem = parseInt(pageItemLimit / 2);
      start = currentPage - intervalItem;
      end = currentPage + intervalItem;
      if (start < 1) {
        start = 1;
        end = start + pageItemLimit - 1;
      } else if (end > pageCount) {
        end = pageCount;
        start = end - pageItemLimit + 1;
      }
      me.render_pageItem(start, end);
    };

    var pageItem_onClick = function () {
      eml.find('span.active').removeClass('active');
      currentPage = parseInt($(this).text());
      $(this).addClass("active");
      do_PageChange(this);
      me.on_PageChange(me);
    };

    var directorItem_onClick = function () {
      eml.find('span.active').removeClass('active');
      do_PageChange(this);
      me.on_PageChange(me);
    };

    var create_page_item = function (text, isItem) {
      var pageItem = $('<span id=' + text + ' data-bind="click: localPagingChangeHandler" ></span>');
      pageItem.text(text);

      if (isItem) {
        pageItem.on('click', pageItem_onClick);
      } else {
        pageItem.on('click', directorItem_onClick);
      }
      return pageItem;
    };

    this.render_pageItem = function (start, end) {
      eml.html('');
      if (end > pageCount) {
        end = pageCount;
      }
      if (pageCount > pageItemLimit) {
        var previousPageRange = create_page_item(' ');
        previousPageRange.addClass('previous fa fa-angle-left');
        eml.append(previousPageRange);
      }
      if (pageCount > 1) {
        for (var i = start; i <= end; i++) {
          if (i <= 0) {
            continue;
          }
          var pageItem = create_page_item(i, true);
          if (i === currentPage) {
            pageItem.addClass('active');
          }
          eml.append(pageItem);
        }
      }
      if (pageCount > pageItemLimit) {
        var nextPageRange = create_page_item(' ');
        nextPageRange.addClass('next fa fa-angle-right');
        eml.append(nextPageRange);
      }
    };

    this.render = function (_currentPage, _pageCount, _pageLimit) {
      pageCount = _pageCount;
      currentPage = _currentPage;
      pageItemLimit = _pageLimit;

      if (pageCount >= pageItemLimit) {
        if (currentPage !== 1) {
          changePage();
        } else {
          me.render_pageItem(1, pageItemLimit);
        }
      } else {
        me.render_pageItem(1, pageCount);
      }
    }
  };

  this.__defineGetter__('items', function () {
    var emls = $('[component="local-paging"]');
    var result = {};
    emls.each(function () {
      result[this.getAttribute('name')] = this['LocalPaging'];
    });

    return result;
  });

  this.init = function () {
    var emls = $('[component="local-paging"]');
    emls.each(function () {
      this['LocalPaging'] = new localPagingClass($(this));
    });
  };
})();Component.Module = {};
Component.Module.AbstractModuleView = function (model, _onDone) {
    if(_onDone === undefined){
        this.onDone = function () {
            console.warn('On Done event of View is not definition');
        }
    } else {
        this.onDone = _onDone;
    }
};
Component.Custom = Component.Custom || {};
Component.Custom.Number = new(function () {
    var number = this;

    this.initEvent = function(){
        $('[component="number"]').each(function () {
            var parent = $(this).parent();
            var up = parent.find('.up');
            var down = parent.find('.down');

            down.unbind('click');
            up.unbind('click');

            down.on('click', function () {
                $(this).parent().find('[component="number"]')[0].stepDown();
                $(this).parent().find('[component="number"]').trigger('change');

            });
            up.on('click', function () {
                $(this).parent().find('[component="number"]')[0].stepUp();
                $(this).parent().find('[component="number"]').trigger('change');
            });
        });
    };

    this.init = function () {
        $('[component="number"]').each(function () {
            var eml = $(this);
            if(eml[0].Number === undefined) {
                eml[0].Number = true;
                var wrapper = $('<div class="com-wapper-number"></div>');
                var up = $('<a class="up"><i class="fa fa-chevron-up"></i></a>');
                var down = $('<a class="down"><i class="fa fa-chevron-down"></i></a>');
                eml.wrap(wrapper);

                down.insertBefore(eml);
                up.insertAfter(eml);

                down.unbind('click');
                up.unbind('click');

                down.on('click', function () {
                    $(this).parent().find('[component="number"]')[0].stepDown();
                    $(this).parent().find('[component="number"]').trigger('change');
                });
                up.on('click', function () {
                    $(this).parent().find('[component="number"]')[0].stepUp();
                    $(this).parent().find('[component="number"]').trigger('change');
                });
            }
        });
    };

    $(document).ready(function () {
        number.init();
    });

    /*Common.wait(function () {
        return window['alreadyBinding'];
    }, function(){
        number.init();
    });*/
})();Component.Custom = Component.Custom || {};
Component.Custom.Paging = new (function () {
    var paging = this;

    var pagingClass = function (_eml) {
        var me = this;
        var eml = _eml;

        var total = 0;
        this.total = function (_val) {
            if (_val) {
                total = _val;
            } else {
                return total;
            }
        };

        var pageCount = 0;
        this.pageCount = function (_val) {
            if (_val) {
                pageCount = _val;
                on_paging();
            } else {
                return pageCount;
            }
        };

        var limit = 0;
        this.limit = function (_val) {
            if (_val) {
                limit = _val;
            } else {
                return limit;
            }
        };

        var currentPage = parseInt(Common.getUrlParameters()['page']) || 1;
        this.currentPage = function (_val) {
            if (_val) {
                currentPage = _val;
            } else {
                return currentPage;
            }
        };

        var pageItemLimit = 5;
        this.pageItemCount = function (_val) {
            if (_val) {
                pageItemLimit = _val;
            } else {
                return pageItemLimit;
            }
        };

        var rangeIndex = 0;
        var currentPageRange = 0;

        //this.isInitPage = false;

        this.on_PageChange = function () { /* runtime implement */
        };

        var do_PageChange = function (pageItem) {
            pageItem = $(pageItem);
            pageItem.addClass('active');
            if (pageCount <= pageItemLimit) {
                return;
            }
            if (pageItem.hasClass('next')) {
                if (currentPage + 1 <= pageCount) {
                    currentPage += 1;
                }
            } else if (pageItem.hasClass('previous')) {
                if (currentPage - 1 >= 1) {
                    currentPage -= 1;
                }
            } else {
                currentPage = parseInt(pageItem.text());
            }

            changePage();
        };

        var changePage = function () {
            var start, end, intervalItem;
            rangeIndex = Math.floor(currentPage / pageItemLimit);

            intervalItem = parseInt(pageItemLimit / 2);
            start = currentPage - intervalItem;
            end = currentPage + intervalItem;
            if (start < 1) {
                start = 1;
                end = start + pageItemLimit - 1;
            } else if (end > pageCount) {
                end = pageCount;
                start = end - pageItemLimit + 1;
            }
            render_pageItem(start, end);
        };

        var pageItem_onClick = function () {
            eml.find('span.active').removeClass('active');
            currentPage = $(this).text();
            $(this).addClass("active");
            do_PageChange(this);
            me.on_PageChange(me);
        };

        var directorItem_onClick = function () {
            eml.find('span.active').removeClass('active');
            do_PageChange(this);
            me.on_PageChange(me);
        };

        var create_page_item = function (text, isItem) {
            var pageItem = $('<span></span>');
            pageItem.text(text);

            if (isItem) {
                pageItem.on('click', pageItem_onClick);
            } else {
                pageItem.on('click', directorItem_onClick);
            }
            return pageItem;
        };

        var render_pageItem = function (start, end) {
            if (pageCount <= 1) {
                //return;
            }
            eml.html('');
            if (end > pageCount) {
                end = pageCount;
            }
            if (pageCount > pageItemLimit) {
                var previousPageRange = create_page_item(' ');
                previousPageRange.addClass('previous fa fa-angle-left');
                eml.append(previousPageRange);
            }
            if (pageCount > 1) {
                for (var i = start; i <= end; i++) {
                    if (i <= 0) {
                        continue;
                    }
                    var pageItem = create_page_item(i, true);
                    if (i === currentPage) {
                        pageItem.addClass('active');
                    }
                    eml.append(pageItem);
                }
            }
            if (pageCount > pageItemLimit) {
                var nextPageRange = create_page_item(' ');
                nextPageRange.addClass('next fa fa-angle-right');
                eml.append(nextPageRange);
            }
        };

        this.render = function (_pageCount, _limit, _total, _currentPage) {
            //if(!me.isInitPage) {
            //pageItemLimit = parseInt(eml.attr('pageItemLimit')); chuyen xuong init()
            pageCount = _pageCount || parseInt(eml.attr('pageCount'));
            limit = _limit || parseInt(eml.attr('pageItemLimit'));
            //currentPage = parseInt(eml.attr('currentPage')) || 1;

            if(_currentPage !== undefined){
                _currentPage = parseInt(_currentPage) || parseInt(eml.attr('currentPage'));
                if (_currentPage !== null && _currentPage !== undefined && _currentPage !== NaN) {
                    currentPage = _currentPage;
                }
            }

            currentPage = parseInt(currentPage);

            total = _total || parseInt(eml.attr('total'));
            if (pageCount >= pageItemLimit) {
                if (currentPage !== 1) {
                    changePage();
                } else {
                    render_pageItem(1, pageItemLimit);
                }
            } else {
                render_pageItem(1, pageCount);
            }
            //me.isInitPage = true;
            //}
        }
    };

    this.__defineGetter__('items', function () {
        var emls = $('[component="paging"]');
        var result = {};
        emls.each(function () {
            result[this.getAttribute('name')] = this['Paging'];
        });
        return result;
    });

    this.init = function () {
        var emls = $('[component="paging"]');
        emls.each(function () {
            this['Paging'] = new pagingClass($(this));
        });
    };

    $(document).ready(function () {
        paging.init();
    });
})();/*
 <div component="popup" name="details" width="80%" height="80%" >
    <h2>Thông tin model tài sản
        <button class="red close">
            <i class="fa fa-close"></i>
        </button>
    </h2>
    <div class="popup-content">
         <div component="form" align="float" name="ComponentDetails">
             <div class="form-content">
             <item text=""
                     name="ModelID"
                     type="hidden">
             </item>
             <item text=""
                     name="ID"
                     type="hidden">
             </item>
             <item width="96%" text="Tên model:" name="ComponentName" type="text"></item>
             <item width="56%" text="Mô tả ngắn" name="ShortDescription" type="editor" height="200px"></item>
             <item width="40%" text="Giá" name="Cost" type="text"></item>
             <item width="40%" text="Hiển thị" name="IsDisplay" type="checkbox"></item>
             <item width="40%" text="Ảnh đại diện" name="Thubmails" type="imagepicker" field="Gallery"></item>
             <item width="96%" text="Mô tả chi tiết" name="Description" type="editor" height="400px"></item>

             <item width="96%" multiple="" text="Ảnh sản phẩm" name="Gallery" accept="image/*" placeholder="Ảnh sản phẩm" imagereview="true" type="file"></item>
             <item width="96%" text="Hướng dẫn" name="ManualGuide" type="editor" height="400px"></item>
             </div>
             <div class="form-control-panel">
             <button data-bind="click: do_SubmitDetail, text: i18n('gui:resource-type:detailForm:btnSubmit')"></button>
             <button class="red" data-bind="click: do_CancelDetail, text: i18n('gui:resource-type:detailForm:btnCancel')"></button>
             </div>
         </div>
     </div>
 </div>
 */

Component.Custom = Component.Custom || {};
Component.Custom.Popup = new(function () {
    var popup = this;
    var popupClass = function (_eml) {
        var me = this;
        var eml = _eml;

        $(eml).find('.close').on('click', function () {
            me.on_Close();
            me.hide();
        });
        var btnClose = $(eml).find('h2 .close');
        btnClose.on('click', function () {
            me.on_Close();
            me.hide();
        });

        this.on_Close = function(){
            /* Runtime Definition */
        };

        this.show = function (callback) {
            /*var myHeight = $(eml).height();
            var winHeight = $(window).height();
            var top = '';
            if(myHeight >= winHeight*0.8){
                top = '20px';
            } else {
                top = (winHeight/2 - myHeight/2 - 60) + 'px';
            }
            $(eml).css({
               top: top
            });*/
            var myWidth = $(eml).attr('width') || '50%';
            var myHeight = $(eml).attr('height') || '55%';
            var unit = myWidth.indexOf('%') >= 0?'%':'px';
            $(eml).css({
                width: myWidth,
                height: myHeight,
                marginLeft: -(parseInt(myWidth) / 2) + unit,
                left: '50%',
                top: '50%'

            });
            if(myWidth !== undefined || myHeight !== undefined) {
                $(eml).find('.popup-content').css({
                    height: $(eml).height() - ($(eml).find('>h2').height() + 40 + $(eml).find('>.popup-bottom').height())
                });
            }
            Component.System.blank.show();
            $(eml).addClass('show');
            $(eml).css({
                marginTop: -($(eml)[0].offsetHeight / 2) + 'px'

            });
            Common.callBack(callback);
        };

        this.hide = function () {
            Component.System.blank.hide();
            $(eml).removeClass('show');
        };
    };

    Object.defineProperty(popup, 'items', { get: function() {
        var result = {};
        $('[component="popup"]').each(function () {
            result[this.getAttribute('name')] = this;
        });
        return result;
    } });

    _prop('Active', function () {
        var activeItem = $('[component="popup"].show');
        if(activeItem.length > 0){
            return activeItem[0].Popup;
        } else {
            return null;
        }

    });
    _prop('HasActive', function () {
        var activeItem = $('[component="popup"].show');
        return activeItem.length > 0;
    });

    this.init = function () {
        var emls = $('[component="popup"]');
        emls.each(function(){
            this['Popup'] = new popupClass(this);
        });
        var emlsTrigger = $('[component="popup-trigger"]');
        emlsTrigger.each(function(){
            this.addEventListener('click', function () {
                var popupName = this.getAttribute('popupname');
                var staus = this.getAttribute('popupstatus');
                Component.Custom.Popup.items[popupName]['Popup'][staus]();
            });

        });
    };

    $(document).ready(function () {
        popup.init();
    });
})();/**
 * Created by tientm2 on 11/21/2017.
 */
Component.Custom = Component.Custom || {};
Component.Custom.Rate = new (function () {
    var rate = this;

    $(document).ready(function () {
        $('[component="rate"]').each(function () {
            var eml = $(this);
            var max = eml.attr('max');
            var symbol = eml.attr('symbol');
            var lblDisplay = $('<span class="display">0</span>');
            var displayName = function (rate) {
                switch (rate){
                    case 0.5:
                        return 'Siêu tệ';
                        break;
                    case 1:
                    case 1.5:
                        return 'Tệ';
                        break;
                    case 2:
                    case 2.5:
                        return 'Tạm được';
                        break;
                    case 3:
                    case 3.5:
                        return 'Được';
                        break;
                    case 4:
                    case 4.5:
                        return 'Tốt';
                        break;
                    case 5:
                        return 'Tuyệt vời';
                        break;
                }
            };


            eml[0].reset = function() {
                $('.rate-item').removeClass('checked');
                lblDisplay.text(0);
            };

            for(var i = max*2; i >0 ; i--){

                var rateItem = $('<span></span>');
                rateItem.addClass('rate-item');
                rateItem.addClass(symbol);
                rateItem.attr('value', ((i)/2));
                eml.append(rateItem);

                if(eml.attr('value') !== undefined && eml.attr('value') !== ''){
                    var value = parseFloat(eml.attr('value'));
                    var valueIndex = Math.ceil(value/0.5);
                    if(valueIndex === i){
                        rateItem.addClass('checked');
                        lblDisplay.text(value.toFixed(1)+' '+displayName((i)/2));
                    }
                } else {
                    rateItem.on('click', function () {
                        var val = parseFloat($(this).attr('value'));
                        eml.find('.rate-item').removeClass('checked');
                        $(this).addClass('checked');
                        lblDisplay.text(val+' '+displayName(val));
                        eml.attr('value', $(this).attr('value'));
                        eml.trigger('change');
                    });
                    rateItem.on('mouseenter', function () {
                        var val = parseFloat($(this).attr('value'));
                        lblDisplay.text(val+' '+displayName(val));
                    });
                    rateItem.on('mouseout', function () {
                        if($('.rate-item.checked').length === 0) {
                            lblDisplay.text('0');
                        }
                    });
                }
            }
            eml.append(lblDisplay);
        });
    });
})();Component.Custom = Component.Custom || {};
Component.Custom.ReadMorePanel = new(function () {
    var readmore = this;

    var readmoreClass = function (_eml) {
        var me = this;
        var eml = _eml;
        $(eml).height(300);
        var btnReadMore = $('<span class="btn-read-more active">Xem Thêm</span>');
        $(eml).append(btnReadMore);

        btnReadMore.on('click', function () {
            $(this).removeClass('active');
            $(this).addClass('off');
            $(eml).height('');
        });
    };

    this.init = function () {

        $('[component="readmore-panel"]').each(function () {
            var eml = $(this);
            if(eml[0].ReadMorePanel === undefined) {
                eml[0].ReadMorePanel = new readmoreClass(eml[0]);
            }
        });
    };

    $(document).ready(function () {
        readmore.init();
    });
})();/**
 * Created by tientm2 on 10/20/2017.
 */

Component.ShopCart = new function() {
  var me = this;
  var MAX_QUANTITY_OF_MODEL_IN_CART = 10;
  //cpnguyen
  const ACTION_TYPE = ["Trả", "Báo mất", "Báo hư hỏng"];

  const ACTION_TYPE_KEY = {
    Trả: "listReturnItem",
    "Báo mất": "listLoseItem",
    "Báo hư hỏng": "listFixItem"
  };

  var checkQuantityAdd = function(modelID, newQuantity, cartname) {
    if (cartname === null || cartname === undefined || cartname == "carts") {
      var quantityInCart = getQuantityInCart(modelID);
      var temp = MAX_QUANTITY_OF_MODEL_IN_CART - quantityInCart;
      if (newQuantity > MAX_QUANTITY_OF_MODEL_IN_CART) {
        if (quantityInCart == MAX_QUANTITY_OF_MODEL_IN_CART) {
          Component.System.alert.show(
            "Bạn đang có <b>" +
              quantityInCart +
              "</b> sản phẩm này trong Giỏ hàng </br> Bạn không được đặt thêm sản phẩm này nữa",
            {
              name: "Đóng",
              cb: function() {}
            }
          );
          return false;
        }
        Component.System.alert.show(
          "Bạn đang có <b>" +
            quantityInCart +
            "</b> sản phẩm này trong Giỏ hàng </br> Chỉ được đặt thêm tối đa <b>" +
            temp +
            "</b> sản phẩm này",
          {
            name: "Đóng",
            cb: function() {}
          }
        );
        return false;
      } else {
        return true;
      }
    } else if (cartname == "cartother") {
      if (newQuantity > MAX_QUANTITY_OF_MODEL_IN_CART) {
        Component.System.alert.show("Bạn không được đặt quá 10 sản phẩm", {
          name: "Đóng",
          cb: function() {}
        });
        return false;
      } else {
        return true;
      }
    }
  };

  var checkQuantityAddNew = function(modelID) {};

  //cpnguyen
  this.AddToCart = function(model) {
    const carts = me.GetCart() || [];
    if (me.IsExisted(model.COMPONENTID, "carts")) {
      var newQuantity =
        getQuantityInCart(model.COMPONENTID) + (model.QUANTITY || 1);
      if (checkQuantityAdd(model.COMPONENTID, newQuantity)) {
        me.ChangeQuantityOfModel(model.COMPONENTID, newQuantity);
      }
    } else {
      getQuantityInStockByModelID(model.COMPONENTID, function(stock) {
        var quantity = model.QUANTITY || 1;
        if (quantity > 10) {
          Component.System.alert.show("Bạn không được đặt quá 10 sản phẩm", {
            name: "Đóng",
            cb: function() {}
          });
          return;
        }
        var status = "";
        if (stock == 0) {
          status = "OutOfStock";
        } else if (quantity > stock) {
          status = "NotEnough";
        } else {
          status = "Normal";
        }
        getDayForProduct(model.COMPONENTID, function(days_of_purchase) {
          carts.push({
            COMPONENTID: model.COMPONENTID,
            COMPONENTNAME: model.COMPONENTNAME,
            COMPONENTTYPEID: model.COMPONENTTYPEID,
            COMPONENTTYPENAME: model.COMPONENTTYPENAME,
            COMMENTS: model.COMMENTS,
            QUANTITY: quantity,
            STOCK: stock,
            STATUS: status,
            DAYSOFPURCHASE: days_of_purchase
          });
          Libs.LocalStorage.val("carts", JSON.stringify(carts));
          me.UpdateCart();
        });
      });
    }
  };

  //cpnguyen
  this.getAllItems = function() {
    //Unify all items of type fix, damage, return
    let cart = [];

    ACTION_TYPE.forEach(function(action) {
      cart = cart.concat(me.GetCart(ACTION_TYPE_KEY[action]) || []);
    });

    return cart;
  };

  //cpnguyen
  this.ChangeActionTypeOfModel = function(model, actionType) {
    let carts = me.getAllItems();
    let item = carts.find(function(asset) {
      return asset.RESOURCENAME == model.RESOURCENAME;
    });
    item.ACTIONTYPE = actionType;

    me.SplitToActionTypes(carts);
    me.UpdateCart();
  };

  //cpnguyen
  this.SplitToActionTypes = function(carts) {
    ACTION_TYPE.forEach(function(action) {
      let typeCart = [];
      carts.forEach(function(item) {
        if (item.ACTIONTYPE == action) {
          typeCart.push(item);
        }
      });
      Libs.LocalStorage.val(ACTION_TYPE_KEY[action], JSON.stringify(typeCart));
    });
  };

  //cpnguyen
  this.AddToCartFromMyProduct = function(asset_array) {
    let carts = [];

    asset_array.forEach(function(model) {
      if (me.IsExisted(model.RESOURCENAME)) {
        me.ChangeActionTypeOfModel(model, model.ACTIONTYPE);
      } else {
        getDayForProduct(model.COMPONENTID, function(days_of_purchase) {
          carts.push({
            RESOURCENAME: model.RESOURCENAME,
            COMPONENTID: model.COMPONENTID,
            COMPONENTNAME: model.COMPONENTNAME,
            COMPONENTTYPEID: model.COMPONENTTYPEID,
            COMPONENTTYPENAME: model.COMPONENTTYPENAME,
            COMMENTS: model.COMMENTS,
            QUANTITY: 1,
            ACTIONTYPE: model.ACTIONTYPE,
            DAYSOFPURCHASE: days_of_purchase
          });
          // Libs.LocalStorage.val('carts', JSON.stringify(carts));
          me.SplitToActionTypes(carts);
          me.UpdateCart();
        });
      }
    });
  };

  this.AddToCartOther = function(item) {
    var carts = me.GetCart("cartother");
    if (me.IsExisted(item.ModelName, "cartother")) {
      me.AddQuantity(item.ModelName, item.Quantity || 1, "cartother");
    } else {
      carts.push({
        ID: item.ID,
        ModelType: item.ModelType,
        ModelName: item.ModelName,
        LinkDescription: item.LinkDescription,
        ReferPrice: item.ReferPrice,
        Quantity: item.Quantity || 1,
        Note: item.Note || 1
      });
      Libs.LocalStorage.val("cartother", JSON.stringify(carts));
    }
    me.UpdateCart();
  };

  var getDayForProduct = function(modelID, callback) {
    APIData.AssetModelDetail.GetByModelID(modelID, function(res) {
      res = JSON.parse(res);
      if (res === undefined || res === null) {
        res = {
          BuyDays: 15,
          TransferDays: 2
        };
      }
      var result = parseInt(res.BuyDays) + parseInt(res.TransferDays);
      callback(result);
    });
  };

  var getQuantityInStockByModelID = function(modelID, callback) {
    APIData.Component.GetQuantityInHand(modelID, function(stock) {
      stock = JSON.parse(stock);
      callback(stock.Result);
    });
  };

  var getQuantityInCart = function(modelID) {
    var carts = me.GetCart();
    var item = carts.find(function(i) {
      return i.COMPONENTID == modelID;
    });

    return item.QUANTITY;
  };

  //cpnguyen
  this.IsExisted = function(id, cartname) {
    let carts = [];
    if (cartname == "carts") {
      carts = me.GetCart();
    } else {
      carts = me.getAllItems();
    }

    if (!cartname) {
      let item = carts.find(function(i) {
        return i.RESOURCENAME == id;
      });
      if (item) {
        return true;
      } else {
        return false;
      }
    } else {
      let item = carts.find(function(i) {
        if (cartname == "carts") {
          return i.COMPONENTID == id;
        } else if (cartname == "cartother") {
          return i.ID == id;
        }
      });
      if (item) {
        return true;
      } else {
        return false;
      }
    }
  };

  this.RemoveFormCart = function(id, cartname) {
    var popName = "COMPONENTID";
    if (cartname === undefined || cartname === null) {
      cartname = "carts";
    } else {
      popName = "ID";
    }
    var carts = me.GetCart(cartname);
    var item = carts.find(function(i) {
      return i[popName] == id;
    });
    carts.splice(carts.indexOf(item), 1);
    Libs.LocalStorage.val(cartname, JSON.stringify(carts));
    me.UpdateCart();
  };
  this.RemoveItemListEdit = function(id, cartname) {
    var popName = "COMPONENTID";
    if (cartname === undefined || cartname === null) {
      cartname = "carts";
    } else {
      popName = "RESOURCENAME";
    }
    var carts = me.GetCart(cartname);

    if (carts.length > 0) {
      var item = carts.find(function(i) {
        return i[popName] == id;
      });
      item !== undefined && carts.splice(carts.indexOf(item), 1);

      Libs.LocalStorage.val(cartname, JSON.stringify(carts));
      me.UpdateCart();
    }
  };
  this.AddQuantity = function(id, quantity, cartname) {
    if (cartname === undefined || cartname === null) {
      cartname = "carts";
    }
    if (cartname == "carts" && !checkQuantityAdd(id, quantity)) {
      return;
    }
    var carts = me.GetCart(cartname);
    var item = carts.find(function(i) {
      return i["COMPONENTID"] == id;
    });
    var newQuantity = item["QUANTITY"] + quantity;
    me.ChangeQuantityOfModel(id, newQuantity, cartname);
  };

  this.ChangeQuantityOfModel = function(modelID, newQuantity, cartName) {
    var popName = "COMPONENTID";
    var quantityPropName = "QUANTITY";
    var statusPropName = "STATUS";
    var stockPropName = "STOCK";

    if (cartName == "cartother") {
      popName = "ID";
      quantityPropName = "Quantity";
    } else {
      cartName = "carts";
    }
    if (cartName == "carts") {
      if (!checkQuantityAdd(modelID, newQuantity)) {
        return;
      }
    } else if (cartName == "cartother") {
      if (!checkQuantityAdd(null, newQuantity, cartName)) {
        return;
      }
    }
    var carts = me.GetCart(cartName);
    var item = carts.find(function(i) {
      return i[popName] == modelID;
    });
    item[quantityPropName] = newQuantity;
    if (cartName == "carts") {
      if (item[stockPropName] != 0) {
        if (item[quantityPropName] > item[stockPropName]) {
          item[statusPropName] = "NotEnough";
        } else {
          item[statusPropName] = "Normal";
        }
      }
    }
    Libs.LocalStorage.val(cartName, JSON.stringify(carts));
    me.UpdateCart();
  };

  this.GetCart = function(cartname) {
    if (cartname === undefined || cartname === null) {
      cartname = "carts";
    }
    var carts = JSON.parse(Libs.LocalStorage.val(cartname));
    if (carts === undefined || carts === null) {
      carts = [];
    }
    return carts;
  };

  this.ResetCart = function() {
    Libs.LocalStorage.val("cartDate", new Date().toString());
    Libs.LocalStorage.val("carts", JSON.stringify([]));
    Libs.LocalStorage.val("cartother", JSON.stringify([]));
    this.UpdateCart();
  };

  //cpnguyen
  //this function update cart counting number
  this.UpdateCart = function() {
    var carts = me.GetCart();
    var cartother = me.GetCart("cartother");
    var quantityProducts = 0;
    for (var i = 0; i < carts.length; i++) {
      quantityProducts += carts[i]["QUANTITY"];
    }
    for (var i = 0; i < cartother.length; i++) {
      quantityProducts += parseInt(cartother[i]["Quantity"]);
    }

    //combine assets of 3 types : lost,return,damage
    quantityProducts += me.GetCart("listLoseItem").length;
    quantityProducts += me.GetCart("listFixItem").length;
    quantityProducts += me.GetCart("listReturnItem").length;

    $('[component="shopcart-noti"]').text(quantityProducts);
    return quantityProducts;
  };
}();
/**
 * Created by Administrator on 11/22/2016.
 */
Component.System = {
    loading: {
        show: function () {
            $('.loading').show();
            //Component.System.blank.show();
        },
        hide: function () {
            $('.loading').hide();
            //Component.System.blank.hide();
        }
    },
    blank: {
        show: function () {
            $('.sys-blank').show();
        },
        hide: function () {
            $('.sys-blank').hide();
        }
    },
    preLoadingPanel: {
        show: function () {
            $('.pre-load-panel').show();
        },
        hide: function () {
            $('.pre-load-panel').hide();
        }
    },
    productLoading: {
        show: function () {
            $('.product-loading').show();
        },
        hide: function () {
            $('.product-loading').hide();
        }
    },
    alert: new (function () {
        var me = this;
        var createAlert = function () {
            var eml = $('<div class="sys-alert">' +
                '<div class="alert-content"></div>' +
                '<div class="alert-control"></div>' +
                '</div>');

            $('body').append(eml);
            console.log(eml);
            return eml;
        };

        this.show = function (mess) {
            var eml = $('.sys-alert');
            if (eml.length <= 0) {
                eml = createAlert();
            }
            var content = eml.find('.alert-content');
            content.empty();
            content.append(mess);

            var joAlertControl = eml.find('.alert-control');
            joAlertControl.empty();

            if(arguments.length == 1){
                var jo = $("<button />");
                jo.text("Đóng");
                jo.unbind('click');
                jo.on('click', me.hide);
                joAlertControl.append(jo);
            }else{
                for(var i = 1; i < arguments.length; i++){
                    var btn  = arguments[i];
                    var jo = $("<button />");
                    jo.html(btn.name);
                    jo.unbind('click');
                    jo.on('click', me.hide);
                    jo.on('click', btn.cb);

                    if(i > 1)
                        jo.addClass("red");
                    joAlertControl.append(jo);
                }
            }


            eml.addClass('show');
            Component.System.blank.show();
        };
        this.hide = function () {
            var eml = $('.sys-alert');
            eml.removeClass('show');

            if($('[component="popup"].show').length === 0){
                Component.System.blank.hide();
            }
        };
        this.failed = function () {
            this.show("Thất bại", {
                name: "Đóng",
                cd: function () { }
            });
        };
        this.success = function () {
            this.show("Thành công", {
                name: "Đóng",
                cd: function () { }
            });
        }
    })()
};/**
 * Created by tientm2 on 11/21/2017.
 */
Component.Custom = Component.Custom || {};
Component.Custom.Tab = new(function () {
    var tab = this;

    $(document).ready(function () {

        $('[component="tab"]').each(function () {
           var eml = $(this);
           var tabItems = eml.find('.tab-item li');
            tabItems.on('click', function () {
                eml.find('.tabContent').removeClass('active');
                tabItems.removeClass('active');
                var target = $(this).attr('target');
                $('#'+target).addClass('active');
                $(this).addClass('active');
            });
        });

    });
})();Component.Custom = Component.Custom || {};
Component.Custom.Table = new(function () {
    var table = this;

    var TableClass = function TableClass(_eml) {
        var me = this;
        var eml = _eml;
        var data = [];
        var table = null;
        var editable = false;
        var isaddnew = false;

        var colEmls = $(_eml).find('column');
        var cols = [];
        for(var i = 0; i < colEmls.length; i++){
            var colEml = colEmls[i];
            var col = {
                Name: colEml.getAttribute('name'),
                Width: colEml.getAttribute('width'),
                DataType: colEml.getAttribute('type')
            };
            cols.push(col);
        }

        var getTableHeader = function () {
            var thead = $('<thead></thead>');
            var tr = $('<tr></tr>');
            for(var i = 0; i < cols.length; i++){
                var col = $('<th></th>');
                col.text(cols[i].Name);
                tr.append(col);
            }
            thead.append(tr);
            return thead;
        };

        var insertAddNewRow = function () {

        };

        var renderData = function(){
          if(editable){

          } else {

          }
        };

        this.render = function () {
            table = $('<table class="datatable"></table>');

            editable = table.atrr('editable');
            isaddnew = table.atrr('isaddnew');

            if(isaddnew === 'true'){
                insertAddNewRow();
            }

            table.append(getTableHeader());
            table.insertAfter(eml);
        };

        me.render();
    };

    this.init = function(){
        var emls = $('[component="table"]');
        emls.each(function(){
            this['Table'] = new TableClass($(this));
        });
    };

    _prop('items', function () {
        var result = {};
        $('[component="table"]').each(function () {
            result[this.getAttribute('name')] = this;
        });
        return result;
    });

    $(document).ready(function () {
        table.init();
    });

})();
Component.Custom = Component.Custom || {};
Component.Custom.TableData = new(function () {
    var tableData = this;

    var tableDataClass = function(_eml){
        var me = this;
        var eml = _eml;

        var total = 0;
        this.total = function(_val){
            if(_val){
                total = _val;
            } else {
                return total;
            }
        };

        var pageCount = 0;
        this.pageCount = function(_val){
            if(_val){
                pageCount = _val;
                on_paging();
            } else {
                return pageCount;
            }
        };

        var limit = 0;
        this.limit = function(_val){
            if(_val){
                limit = _val;
            } else {
                return limit;
            }
        };

        var isEditable = false;
        this.isEditable = function (_val) {
            if(_val){
                editable = _val;
            } else {
                return editable;
            }
        };

        var isPaging = false;
        this.isPaging = function (_val) {
            if(_val){
                isPaging = _val;
            } else {
                return isPaging;
            }
        };

        var currentPage = 1;
        this.currentPage = function (_val) {
            if(_val){
                currentPage = _val;
            } else {
                return currentPage;
            }
        };

        var pageItemCount = 10;
        var currentPageRange = 0;

        this.isInitPage = false;

        var init = function(){
            isPaging = Common.parseBoolean(eml.attr('isPaging'));
            limit = parseInt(eml.attr('limit'));
            isEditable = parseInt(eml.attr('isEditable'));

            /* Bo tu dong tao tfoot
            var tfoot = document.createElement('tfoot');
            eml.append(tfoot);

            if(isEditable){

            }

            var colCount = eml.find('thead tr th').length;
            var paging = $('<tr><td colspan="'+colCount+'"></td></tr>');

            $(tfoot).append(paging);
            */
        };

        this.on_PageChange = function () { /* runtime implement */ };

        var do_PageChange = function (eml) {
            if(pageCount <= 10){
                return;
            }
            currentPage = parseInt(eml.innerText);
            var rangeIndex = Math.floor(currentPage/pageItemCount);
            if(rangeIndex !== currentPageRange){
                currentPageRange = rangeIndex;
                var start = rangeIndex*pageItemCount;
                var end = start + pageItemCount;

                render_pageItem(start, end);
            }
        };

        var render_pageItem = function (start, end) {
            if(end > pageCount){
                end = pageCount;
            }
            var pagingContainer = eml.find('tfoot tr td');
            pagingContainer.html('');
            for(var i = start-1; i <= end; i++){
                if(i <= 0){
                    continue;
                }
                var pageItem = $('<span></span>');
                pageItem.text(i);
                pageItem.on('click', function(){
                    pagingContainer.find('span.active').removeClass('active');
                    $(this).addClass('active');
                    do_PageChange(this);
                    me.on_PageChange(this);
                });
                if(i===currentPage){
                    pageItem.addClass('active');
                }
                pagingContainer.append(pageItem);
            }
        };

        var on_paging = function () {
            render_pageItem(1,pageItemCount);
            me.isInitPage = true;
        };
        init();
    };

    this.get = function (name) {
        return $('[component="tabledata"][name="'+name+'"]')[0].TableData;
    };

    this.init = function(){
        var emls = $('[component="tabledata"]');
        emls.each(function(){
            this['TableData'] = new tableDataClass($(this));
        });
    };

    $(document).ready(function () {
        tableData.init();
    });
})();$(document).ready(function () {
    Common.wait(function () {
        return window['alreadyBinding'];
    }, function () {
        if ($('body').height() < window.innerHeight) {
            var height = window.innerHeight - $('.header').height() - $('.footer').height() - $('.nav-wrapper').height();
            $('.content').css({minHeight: height});
        }
    });

});function classDeviceManager(deviceType, rememberDeviceSelected) {

    var me = this;
    var devices = null;
    var currentDevice = null;
    var currentStream = null;

    rememberDeviceSelected = arguments.length > 1 ? rememberDeviceSelected[1] : true;

    if (deviceType != 'videoinput' && deviceType != 'audioinput' && deviceType != 'audiooutput')
        throw 'Unsuport device type: ' + deviceType;

    if (!navigator || !navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
        throw 'Can not access device on this device';
    }

    if (!navigator || !navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
        throw 'Can not access user device list';
    }

    function accessDevice(device, onDone) {
        if (!devices.length) {
            onDone({
                code: AMResponseCode.notFound,
            });
        }

        if (currentDevice)
            throw 'Can not access another device before release current device';

        // create device filter
        var constraints = {
            video: {
                deviceId: {exact: device.deviceID}
            }
        };

        // access device
        navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
            currentDevice = device;
            currentStream = stream;
            onDone({
                code: AMResponseCode.ok,
                data: stream
            });
        }).catch(function (error) {
            onDone({
                code: AMResponseCode.expression,
                message: error
            });
        });
    }

    me.fetchDevice = function (onDone) {
        if (!devices)
            devices = [];

        var promiss = navigator.mediaDevices.enumerateDevices();
        promiss.then(function (list) {
            list.forEach(function (item) {
                if (item.kind != deviceType)
                    return;

                var existed = devices.find(function (exist) {
                    return exist.deviceId == item.deviceId;
                })
                if (existed)
                    return;

                devices.push(item);
            });

            if (devices.length) {
                onDone({
                    code: AMResponseCode.ok,
                    devices: list
                });
            } else {
                onDone({
                    code: AMResponseCode.notFound
                });
            }
        });

        promiss.catch(function (error) {
            onDone({
                code: AMResponseCode.exception,
                message: error,
            });
        });
    }

    /**
     * @param type: videoinput ||
     * @param onDone
     */
    me.accessDevice = function (onDone) {
        var selectAndAccessDevice = function () {
            var userRememberDeviceID = rememberDeviceSelected ? localStorage['deviceManager_userRememberDeviceID_' + deviceType] : null;
            var selectedDevice = null;

            // query user remember device
            if (userRememberDeviceID) {
                for (var i = 0; i < devices.length; i++) {
                    if (devices[i].deviceId == userRememberDeviceID) {
                        selectedDevice = devices[i];
                        break;
                    }
                }
            }

            // no device -> select last device
            if (selectedDevice == null) {
                selectedDevice = devices[devices.length - 1];

                if (rememberDeviceSelected) {
                    localStorage['deviceManager_userRememberDeviceID_' + deviceType] = selectedDevice;
                }
            }

            accessDevice(selectedDevice, onDone);
        };

        if (devices == null) {
            me.fetchDevice(function (res) {
                if (res.code != AMResponseCode.ok)
                    onDone(res);
                else
                    selectAndAccessDevice();
            });
        } else {
            selectAndAccessDevice();
        }
    }

    me.switchDevice = function (onDone) {
        if (currentDevice)
            me.releaseDevice();

        var index = devices.indexOf(currentDevice) + 1;
        if (index >= devices.length)
            index = 0;

        accessDevice(devices[index]);
    }

    me.releaseDevice = function () {
        if (currentDevice == null)
            return;

        currentStream.getTracks().forEach(function (track) {
            track.stop();
        });
        currentDevice = null;
    }
}

function classCameraInput() {
    var me = this;

    var comPopup = $('<div component="popup" name="comCamera"></div>');
    var comPopupTitle = $('<h2>Camera</h2>');
    var comPopupContent = $('<div class="popup-content"></div>');
    var comPopupBottom = $('<div class="popup-bottom"></div>');
    var joCamInput = $('<div class="input-cam">' +
        '<div class="live-stream"></div>' +
        '<div class="right"></div>' +
        '</div>');

    var btnOk = $('<button>Đồng ý</button>');
    var btnCancel = $('<button class="red" >Đóng</button>');
    var btnTakePhoto = $('<button class="green"><i class="fa fa-camera"></i></button>');

    var panelCamPreview = $('<div class="cam-previews"></div>');
    var emlVideo = $('<video autoplay></video>');
    var palLiveStream = joCamInput.find('.live-stream');
    var canvas = $('<canvas style="display: none"></canvas>')[0];
    //var joBody = $('body');
    //var joBlank = $('.sys-blank')
    var deviceManager = new classDeviceManager('videoinput');
    var camResolution = {x: null, y: null};
    var camFiles = null;
    var popup = null;

    btnTakePhoto.on('click', takePhoto);

    emlVideo.on('resize', function () {
        setTimeout(function () {
            camResolution.x = emlVideo.width();
            camResolution.y = camResolution.x * 3 / 5;
            emlVideo.css('width', '');
        }, 1);
    });

    btnOk.on('click', function () {
        hideGUI();
        me.pickCam.onDone && me.pickCam.onDone(camFiles);
    });

    btnCancel.on('click', function () {
        hideGUI();
        me.pickCam.onDone && me.pickCam.onDone(null);
    });

    me.pickCam = function (onDone) {
        showGUI();
        me.pickCam.onDone = onDone;
        camFiles = [];
        deviceManager.accessDevice(onGetCam);
    };

    function showGUI() {
        /*joBlank.backStyle =  joBlank[0].style.display;
        joBlank[0].style.display = 'block';
        joVideo.css('width', 'unset');
        joBody.append(joCamInput);*/

        if (!popup) {
            Component.Custom.Popup.init();
            popup = Component.Custom.Popup.items.comCamera.Popup;
            popup.on_Close = function () {
                btnCancel.click();
            }
        }

        popup.show();
        resizeCamInput();
    }

    function hideGUI() {
        deviceManager.releaseDevice();
        panelCamPreview.empty();
        emlVideo.css('width', '');
        emlVideo[0].src = '';
        popup.hide();
    }

    function onGetCam(res) {
        if (res.code == AMResponseCode.ok) {
            emlVideo[0].srcObject = res.data;
        } else {
            console.log('Không thể truy cập camera');
        }
    }

    function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);
        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], {type: mimeString});
    }

    function takePhoto() {
        $('body').append(canvas);
        canvas.setAttribute('width', camResolution.x);
        canvas.setAttribute('height', camResolution.y);

        var context = canvas.getContext('2d');
        context.drawImage(emlVideo[0], 0, 0, camResolution.x, camResolution.y, 0, 0, camResolution.x, camResolution.y);
        var data = canvas.toDataURL('image/png');
        var bold = dataURItoBlob(data);
        var file = new File([bold], "IMG" + (camFiles.length + 1) + ".png", {type: "image/png"});
        camFiles.push(file);

        var joThumbnail = $('<img/>');
        joThumbnail.attr('src', data);
        panelCamPreview.append(joThumbnail);
        $('body')[0].removeChild(canvas);
    }

    function resizeCamInput() {
        if (!camFiles)
            return;

        var popupContainer = palLiveStream.parent().parent();
        popupContainer[0].style.height = '';
        palLiveStream.height(palLiveStream.width() * 3 / 5);
        popupContainer.height(popupContainer.height());
    }

    function initLayout() {

        comPopupTitle.append('<button class="red close"><i class="fa fa-close"></i></button>');
        palLiveStream.append(emlVideo);
        joCamInput.find('.right').append(panelCamPreview);

        //var joFoot = joCamInput.find('.foot');
        //joFoot.find('.cam-ctrls').append(joTakePhoto);
        comPopupBottom.append(btnTakePhoto);
        comPopupBottom.append(btnOk);
        comPopupBottom.append(btnCancel);

        /*var joCloseCtrls = joFoot.find('.popup-ctrls');
        joCloseCtrls.append(joOk);
        joCloseCtrls.append(joCancel);*/
        comPopupContent.append(joCamInput);
        comPopup.append(comPopupTitle);
        comPopup.append(comPopupContent);
        comPopup.append(comPopupBottom);
        $('body').append(comPopup);
        $(window).on('resize', resizeCamInput);
    }

    initLayout();
}

var Config = {};
Config.Data = {
    Module: {
        ITAAPService: "ITAPP",
        CMDB: "CMDB",
        LOGIN: "Login",
        Direct: "direct",
        ISOAPI: "ISOAPI",
        AAPWEB: "AAPWEB"
    },
    AssetUrls: {
        itServiceBus: 'process'
    },
    SubPath: {
        Asset:"asset",
        TransferForm: "transferForm",
        TransactionForm: 'transactionForm',
        requestForm: 'requestForm',
        DelegateForm: 'delegateForm',
        FormAssetListDetail: "formassetdetail",
        FormNote: "formnote",
        FormStepDefinition: "formstepdefinition",
        FormStep: "formstep",
        Form: "form",
        SystemLog: "systemLog",
        RecoveryForm: "recovery",
        UserRole: "userrole",
        Role: "role",
        FormReason: "formreason",
        FormType: "formtype",
        PrDetail: "pr",
        AssetImageDetail: "assetimage",
        FormstepPermission: "formsteppermission",
        AssetDetailHistory: "assetDetailHistory",
        AssetStatusHistory: "assetStatusHistory",
        MailTemplate: "mailTemplate",
        WebConfig: "webConfig",
        EmployeeTitle: "employeetitle",
        DefaultAsset: "defaultasset",
        TechnicianSite: "technician",
        AssetGeneralInfo: "assetGeneralInfo"
    }
};/**
 * Created by tientm2 on 10/17/2017.
 */
Config.Global = {
    isInitMenuMouseHover: true,
    isInitMouseWheel: true,
    mouseWheelBaseOn: '.head-slide',
    mouseWheelLimitHeight: true
};/**
 * Created by Administrator on 2/27/2017.
 */
/**
 * Created by Administrator on 2/27/2017.
 */
Config.URL = new (function(){
  this.MODULE = DIR_URL+'/js/Module/';
  this.DATA = DIR_URL+'/js/Data/';
  this.LIBS = DIR_URL+'/js/Libs/';
  this.MODULE_VIEW_DIR = DIR_URL+'/{ModuleName}/views/';
  this.MODULE_MODEL = DIR_URL+'/{ModuleName}/model.js';
  this.MODULE_STYLE = DIR_URL+'/{ModuleName}/style.css';
  this.MODULE_VIEW = DIR_URL+'/{ModuleName}/view.js';
  this.MODULE_INDEX = DIR_URL+'/{ModuleName}/index.html';
})();
var APIData = {};
APIData.BEUrl = '/am_ajax';
APIData.OnError = function () {
};

APIData.AbstractAPIData = function (_config) {
    var me = this;
    this.config = _config;

    this.OnRequestEror = function (error) {
        console.error(error);
    };

    this.send = function (data, cb, config) {
        me.config.method = arguments.callee.caller.name;
        var sendConfig = {
            url: APIData.BEUrl,
            data: data,
            header: {
                "U-Class": me.config.class,
                "U-Type": me.config.type,
                "U-Method": me.config.method
            },
            method: 'POST',
            success: cb,
            error: me.OnRequestEror
        };
        sendConfig = $.extend(sendConfig, config);

        Libs.Http.send(sendConfig);
    };

    this.legacyCMDB = function CMDB(url, method, pNames, pValues, callbackMethod, onSuccess, onError, dontSkip) {
        pNames.push('m');
        pValues.push(method);
        var data = {
            u: url,
            p: JSON.stringify(pNames),
            v: JSON.stringify(pValues)
        };

        $.ajax({
            type: 'POST',
            url: APIData.BEUrl,
            data: data,
            headers: {
                "U-Class": me.config.class,
                "U-Type": me.config.type,
                "U-Method": "CallCMDB"
            },
            success: callbackMethod(onSuccess, method, dontSkip),
            error: function (re) {
                onError && onError(re);
                if(location.hostname == 'itportal'){
                    console.error({url: url, method: method, pNames: pNames, pValues: pValues, re: re});
                    Popup.error(re.responseText).setTitle('Không thể kết nối tới máy chủ');
                }
            },
        });
    };

    this.ITAM = function (pluginName, className, method, data, callbackMethod, onSuccess, onError, dontSkip) {
        $.ajax({
            type: 'POST',
            url: APIData.BEUrl,
            data: data,
            headers: {
                "U-Class": pluginName,
                "U-Type": className,
                "U-Method": method
            },
            success: callbackMethod(onSuccess, method, dontSkip),
            error: function (re) {
                onError && onError(re);
                if(location.hostname == 'itportal'){
                    console.error({pluginName: pluginName, className: className, method: method, data: data, re: re});
                    Popup.error('Không thể kết nối tới máy chủ');
                }
            }
        });
    };
};

APIData.Callback = new function () {
    var t = this;

    function log(name, jData, type, ex) {
        if(location.hostname == 'itportal'){Popup.error(jData);
            console.warn("Data is not a \"" + type + "\" at: " + name);
            try {
                console.warn(JSON.parse(jData));
            } catch (ex) {
                console.warn(jData);
            }
            ex && console.error(ex);
        }
    }
    ;
    t.string = function (callback, name, skipIfEror) {
        return function (jData) {
            var obj;
            try {
                obj = JSON.parse(jData);
            } catch (ex) {
                log(name, jData, "string", ex);
            }

            if (obj == null && jData != "null") {
                log(name, jData, "string");
                if (skipIfEror)
                    return;
            }

            callback(obj, jData);
        };
    }
    t.object = function (callback, name, skipIfEror) {
        return function (jData) {
            var obj;
            try {
                obj = JSON.parse(jData);
            } catch (ex) {
                log(name, jData, "object", ex);
            }

            if (obj == null && jData != "null") {
                log(name, jData, "object");
                if (skipIfEror)
                    return;
            }

            callback(obj, jData);
        };
    }
    t.array = function (callback, name, skipIfEror) {
        return function (jData) {
            var res = null;
            try {
                res = JSON.parse(jData);
            } catch (ex) {
                log(name, jData, "array", ex);
            }

            if (res && Array.isArray(res.data) && res.hasOwnProperty("code")) {
                callback(res, jData);
            } else {

                log(name, jData, "array");
                if (skipIfEror == false)
                    callback(null, jData);
                return;
            }

        };
    }
    t.int = function (callback, name, skipIfEror) {
        return function (jData) {
            var res;
            try {
                res = JSON.parse(jData);
            } catch (ex) {
                log(name, jData, "object", ex);
            }

            if (Number.isInteger(res.data) == false || (res == null && jData != "null")) {
                log(name, jData, "int");
                if (skipIfEror)
                    return;
            }

            callback(res, jData);
        };
    }
    t.float = function (callback, name, skipIfEror) {
        return function (jData) {
            if (isNaN(jData)) {
                log(name, jData, "float");
                callback(null, jData);
                return;
            }

            var number = null;
            try {
                number = parseFloat(jData);
            } catch (ex) {
                log(name, jData, "float", ex);
            }

            number || log(name, jData, "float");

            if (number || skipIfEror == false)
                callback(null, jData);
        };
    }
};

APIData.CmdbCallback = new function () {
    var t = this;

    function log(name, jData, type, ex) {

        if(location.hostname == 'itportal'){
            Popup.error(jData);
            console.warn("Data is not a \"" + type + "\" at: " + name);
        }
        try {
            console.warn(JSON.parse(jData));
        } catch (ex) {
            console.warn(jData);
        }
        ex && console.error(ex);
    }
    ;
    t.string = function (callback, name, skipIfEror) {
        return callback;
    }
    t.object = function (callback, name, skipIfEror) {
        return function (jData) {
            var obj = null;
            try {
                obj = JSON.parse(jData);
            } catch (ex) {
                log(name, jData, "object", ex);
            }
            if (obj == null && jData != "null") {
                log(name, jData, "object");
                if (skipIfEror)
                    return;
            }

            callback(obj, jData);
        };
    }
    t.array = function (callback, name, skipIfEror) {
        return function (jData) {
            var res = null;
            try {
                res = JSON.parse(jData);
            } catch (ex) {
                log(name, jData, "array", ex);
            }

            if (Array.isArray(res)) {

                callback(res, jData);
            } else {

                log(name, jData, "array");
                if (skipIfEror == false)
                    callback(null, jData);
                return;
            }

        };
    }
    t.int = function (callback, name, skipIfEror) {
        return function (jData) {
            if (isNaN(jData)) {
                log(name, jData, "int");
                callback(null, jData);
                return;
            }

            var number = null;
            try {
                number = parseInt(jData);
            } catch (ex) {
                log(name, jData, "int", ex);
            }

            number || log(name, jData, "int");

            if (number || skipIfEror == false)
                callback(null, jData);

        };
    }
    t.float = function (callback, name, skipIfEror) {
        return function (jData) {
            if (isNaN(jData)) {
                log(name, jData, "float");
                callback(null, jData);
                return;
            }

            var number = null;
            try {
                number = parseFloat(jData);
            } catch (ex) {
                log(name, jData, "float", ex);
            }

            if (obj == null && jData != "null") {
                log(name, jData, "float");
                if (skipIfEror)
                    return;
            }

            callback(number, jData);
        };
    }
};
APIData = window.APIData || {};

APIData.APIRequestCategory = {};
APIData.APIRequestCategory = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Workflow',
        'type': 'AMRequestCategory'
    });

    this.GetAll = function GetAll(callback) {
        me.send({}, callback);
    };
    this.GetByID = function GetByID(ID, callback) {
        me.send({ID: ID}, callback);
    };
    this.AddNew = function AddNew(data, callback) {
        me.send({data: data}, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({data: data}, callback);
    };
    this.Delete = function Delete(data, callback) {
        me.send({data: data}, callback);
    };

})();APIData = window.APIData || {};

APIData.APIRequestFilter = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Workflow',
        'type': 'AMRequestFilterAjax'
    });

    this.getView = function getView(callback) {
        me.send({}, callback);
    };

    this.filter = function filter(args, callback) {
        me.send(args, callback);
    };

    this.filterAI = function filterAI(keyword, keywordDoc, args, callback) {
        var data = {
            keyword: keyword,
            keywordDoc: keywordDoc,
            args: args
        };
        me.send(data, callback);
    };

})();
APIData = window.APIData || {};

APIData.APIRequestReason = {};
APIData.APIRequestReason = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Workflow',
        'type': 'AMRequestReason'
    });

    this.GetAll = function GetAll(callback) {
        me.send({}, callback);
    };
    this.GetByID = function GetByID(ID, callback) {
        me.send({ID: ID}, callback);
    };
    this.AddNew = function AddNew(data, callback) {
        me.send({data: data}, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({data: data}, callback);
    };
    this.Delete = function Delete(data, callback) {
        me.send({data: data}, callback);
    };

})();APIData = window.APIData || {};

APIData.APIRequestStatus = {};
APIData.APIRequestStatus = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Workflow',
        'type': 'AMRequestStatusAjax'
    });

    this.GetAll = function GetAll(callback) {
        me.send({}, callback);
    };
    this.GetByID = function GetByID(ID, callback) {
        me.send({ID: ID}, callback);
    };
    this.AddNew = function AddNew(data, callback) {
        me.send({data: data}, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({data: data}, callback);
    };
    this.Delete = function Delete(data, callback) {
        me.send({data: data}, callback);
    };

})();APIData = window.APIData || {};

APIData.APIRequestType = {};
APIData.APIRequestType = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Workflow',
        'type': 'AMRequestType'
    });

    this.GetAll = function GetAll(callback) {
        me.send({}, callback);
    };
    this.GetByID = function GetByID(ID, callback) {
        me.send({ID: ID}, callback);
    };
    this.AddNew = function AddNew(data, callback) {
        me.send({data: data}, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({data: data}, callback);
    };
    this.Delete = function Delete(data, callback) {
        me.send({data: data}, callback);
    };

})();APIData = window.APIData || {};

APIData.RoleAndFeature = {};
APIData.RoleAndFeature = new (function () {
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'RoleAndFeature',
        'type': 'APIRoleAndFeature'
    });

    this.GetAllFeatures = function GetAllFeatures(pageindex, limit, callback) {
        me.send({
            pageindex: pageindex,
            limit: limit
        }, callback);
    };

    this.CreateFeature = function CreateFeature(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.DeleteFeature = function DeleteFeature(id, callback) {
        me.send({
            id: id
        }, callback);
    };

    this.UpdateFeature = function UpdateFeature(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.CreateFeaturesRole = function CreateFeaturesRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.UpdateFeaturesRole = function UpdateFeaturesRole(data, callback) {
        me.send({
            data: data
        }, callback);
    }

    this.DeleteFeatureRole = function DeleteFeatureRole(id, callback) {
        me.send({
            id: id
        }, callback);
    }

    this.GetAllFeaturesRole = function GetAllFeaturesRole(pageindex, limit, callback) {
        me.send({
            pageindex: pageindex,
            limit: limit
        }, callback);
    };
})();APIData = window.APIData || {};

APIData.RoleAndPermission = {};
APIData.RoleAndPermission = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'RoleAndPermission',
        'type': 'APIRoleAndPermission'
    });

    this.checkUserPermission = function checkUserPermission(feature, callback) {
        me.send({
            feature: feature
        }, callback);
    };

    this.GetAllRole = function GetAllRole(keyword, pageindex, limit, callback) {
        me.send({
            keyword: keyword,
            pageindex: pageindex,
            limit: limit
        }, callback);
    };

    this.GetAllUserRole = function GetAllUserRole(keyword, pageindex, limit, callback) {
        me.send({
            keyword: keyword,
            pageindex: pageindex,
            limit: limit
        }, callback);
    };

    this.GetUserRoleByDomain = function GetUserRoleByDomain(domain, callback) {
        me.send({
            domain: domain
        }, callback);
    };

    this.GetDeptHeadByDeptName = function GetDeptHeadByDeptName(deptName, callback) {
        me.send({
            deptName: deptName
        }, callback);
    };

    this.CreateRole = function CreateRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.UpdateRole = function UpdateRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.DeleteRole = function DeleteRole(id, callback) {
        me.send({
            id: id
        }, callback);
    };

    this.CreateUserRole = function CreateUserRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.UpdateUserRole = function UpdateUserRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.DeleteUserRole = function DeleteUserRole(id, callback) {
        me.send({
            id: id
        }, callback);
    };

    this.checkUserPermission = function checkUserPermission(feature, callback){
        me.send({
            feature: feature
        }, callback);
    }
})();APIData = window.APIData || {};

APIData.UserRoles = {};
APIData.UserRoles = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'RoleAndPermission',
        'type': 'APIUserRole'
    });

    this.GetAllRole = function GetAllRole(keyword, pageindex, limit, callback) {
        me.send({
            keyword: keyword,
            pageindex: pageindex,
            limit: limit
        }, callback);
    };

    this.GetAllUserRole = function GetAllUserRole(keyword, pageindex, limit, callback) {
        me.send({
            keyword: keyword,
            pageindex: pageindex,
            limit: limit
        }, callback);
    };


    this.CreateRole = function CreateRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.UpdateRole = function UpdateRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.DeleteRole = function DeleteRole(id, callback) {
        me.send({
            id: id
        }, callback);
    };
    this.CreateUserRole = function CreateUserRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.UpdateUserRole = function UpdateUserRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.DeleteUserRole = function DeleteUserRole(id, callback) {
        me.send({
            id: id
        }, callback);
    };
})();/**
 * Created by tientm2 on 6/26/2018.
 */
APIData = window.APIData || {};
APIData.AssetDiscountPercentYear = {};
APIData.AssetDiscountPercentYear=new(function () {
    var me = this;
    APIData.AbstractAPIData.call(this,{
        'class': 'Asset',
        'type': 'APIAssetDiscountPercentYear'
    });
    this.getTotalPercentByAssetGroupID = function getTotalPercentByAssetGroupID(GroupID,roundYear, cb) {
        me.send({
            GroupID:GroupID,
            roundYear:roundYear
        },cb);
    }
    this.getByGroupID = function getByGroupID(GroupID,cb) {
        me.send({
            groupid: GroupID
        },cb);
    }
    this.getYearPercentByAssetGroupID = function getYearPercentByAssetGroupID(GroupID,roundYear, cb) {
        me.send({
            GroupID:GroupID,
            roundYear:roundYear
        },cb);
    }
    this.Update = function update(data,cb) {
        me.send({
            data:data
        },cb);
    }
    this.AddNew = function addNew(data,cb) {
        me.send({
           data:data
        },cb);
    }
    this.deletegroup = function deletegroup(AssetTypeGroupID,cb) {
        me.send({
            AssetTypeGroupID:AssetTypeGroupID
        },cb);
    }
})();APIData = window.APIData || {};

APIData.AssetModelDetail = {};
APIData.AssetModelDetail = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Asset',
        'type': 'APIAssetModelDetail'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.AddNew = function AddNew(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Delete = function Delete(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.GetByID = function GetByID(ID, callback) {
        me.send({
            ID: ID
        }, callback);
    };

    this.DeleteGallery = function DeleteGallery(galleryName, modelID, callback) {
        me.send({
            galleryName: galleryName,
            modelID: modelID
        }, callback);
    };

    this.GetThubmailsPath = function GetThubmailsPath(modelID, callback) {
        me.send({
            modelID: modelID
        }, callback);
    };
    
    this.GetByModelID = function GetByModelID(ModelID, callback) {
        me.send({
            ModelID: ModelID
        }, callback);
    };
})();APIData = window.APIData || {};

APIData.AssetModelReview = {};
APIData.AssetModelReview = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Asset',
        'type': 'APIAssetModelReview'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.AddNew = function AddNew(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Delete = function Delete(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.GetByID = function GetByID(ID, callback) {
        me.send({
            ID: ID
        }, callback);
    };

    this.GetByModelID = function GetByModelID(ModelID, callback) {
        me.send({
            ModelID: ModelID
        }, callback);
    };
})();APIData = window.APIData || {};

APIData.AssetStateHistory = {};
APIData.AssetStateHistory = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Asset',
        'type': 'APIAssetStateHistory'
    });

    this.InsertStateHistory = function InsertStateHistory(data, cb) {
        me.send({
            data: data
        }, cb);
    };

    this.GetAssetStateHistory = function GetAssetStateHistory(assetName, cb) {
        me.send({
            assetName: assetName
        }, cb);
    };
    this.GetAssetCommentHistory = function GetAssetCommentHistory(assetName, cb) {
        me.send({
            assetName: assetName
        }, cb);
    };
    this.UpdateSysComment = function UpdateSysComment(data,cb) {
       me.send({
          data: data
       },cb);
    }
})();APIData = window.APIData || {};

APIData.AssetTypeGroup = {};
APIData.AssetTypeGroup = new (function () {
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Asset',
        'type': 'APIAssetTypeGroup'
    });

    this.getByAssetType = function getByAssetType(assetType, callback) {
        me.send({
            assetType: assetType
        }, callback);
    }

    this.getByGroupName = function getByGroupName(groupName,callback) {

        me.send({
            groupName:groupName
        },callback);
    }
    this.getByDefault = function getByDefault(callback) {
        me.send({},callback);
    }

    this.Update = function update(data, callback) {
        me.send({
            data:data
        }, callback);
    }

    this.AddNew = function AddNew(data, callback) {
        me.send({
           data:data
        }, callback);
    }

    this.getAll = function getAll(data,callback) {
        me.send({
            data:data
        },callback);
    }
    this.getAllandPercent = function getAllandPercent(data,callback) {
        me.send({
            data:data
        },callback);
    }
    this.deletegroup = function deletegroup(id,callback) {
        me.send({
            id:id
        },callback);
    }
})();
APIData = window.APIData || {};

APIData.CMDB = new (function(){
    var me = this;
    me.ServiceURL = "CMDB2";
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'CMDB'
    });
   
///
    me.GetListAsset = function(resourcename, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetListAsset",
            ["resourcename", "startindex", "limit"],
            [resourcename, startindex || 0, limit || 10],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetListUser = function(username, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetListUser",
            ["username", "startindex", "limit"],
            [username, startindex || 0, limit || 10],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.buildAssetDetail = function (assetObject) {
        var realValue = Common.getValueOfObject(assetObject);
        var totals = realValue.length;
        var details = totals;
        for (var m = 0; m < totals; m++) {
            details += "-";
            var props = Object.getOwnPropertyNames(realValue[m]);
            for (var i = 0; i < props.length; i++) {
                var propName = props[i];
                var value = realValue[m][propName];
                if (i === props.length - 1) {
                    details += value;
                } else {
                    details += value + ":";
                }
            }
        }
        return details;
    };
    me.buildAssetStateModel = function (searModel, listAssetState) {
        var list = [];
        for (var i = 0; i < listAssetState.length; i++) {
            var description = listAssetState[i].DisplayDescription;
            description = description ? listAssetState[i].DisplayState + ' - ' + description
                : listAssetState[i].DisplayState;
            var item = {
                name:  description ,
                value: listAssetState[i].DisplayState
            };
            list.push(item);
        }

        var searchMember = [
            {Name: 'value', IsUnicode: false},
            {Name: 'name', IsUnicode: true}
        ];
        searModel.setDataSource(list, searchMember);
    }
    me.GetAssetModel = function(assetName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetModel",
            ["assetName"],
            [assetName],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };

// auto generate
    me.UpdateAssetState = function(componentType, ciName, state, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssetState",
            ["componentType", "ciName", "state"],
            [componentType, ciName, state],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateAssetOwner = function(componentType, ciName, userName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssetOwner",
            ["componentType", "ciName", "userName"],
            [componentType, ciName, userName],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateAssetDetail = function(componentType, ciName, detailstr, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssetDetail",
            ["componentType", "ciName", "detailstr"],
            [componentType, ciName, detailstr],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateAssociateAsset = function(componentType, ciName, parent, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssociateAsset",
            ["componentType", "ciName", "parent"],
            [componentType, ciName, parent],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetListComponentType = function(keyword, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetListComponentType",
            ["keyword", "startindex", "limit"],
            [keyword, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetListComponentModel = function(keyword, type, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetListComponentModel",
            ["keyword", "type", "startindex", "limit"],
            [keyword, type, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetResourceOwnerByName = function(resourceName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetResourceOwnerByName",
            ["resourceName"],
            [resourceName],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetChildrensOfAsset = function(resourceName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetChildrensOfAsset",
            ["resourceName"],
            [resourceName],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.getAssetOfUser = function(LoginName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "getAssetOfUser",
            ["LoginName"],
            [LoginName],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetOfUser = function(username, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetOfUser",
            ["username", "startindex", "limit"],
            [username, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAccountName = function(name, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAccountName",
            ["name"],
            [name],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetUserFullName = function(name, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetUserFullName",
            ["name"],
            [name],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetUserInformation = function(username, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetUserInformation",
            ["username", "startindex", "limit"],
            [username, startindex, limit],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateCustomAssetDetail = function(resourcename, CPU, RAM, VGA, HDD, DVD, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateCustomAssetDetail",
            ["resourcename", "CPU", "RAM", "VGA", "HDD", "DVD"],
            [resourcename, CPU, RAM, VGA, HDD, DVD],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetLastStatusHistory = function(resourcename, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetLastStatusHistory",
            ["resourcename"],
            [resourcename],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetResourceStateById = function(id, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetResourceStateById",
            ["id"],
            [id],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetResourceStateHistoryByResourceName = function(resourcename, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetResourceStateHistoryByResourceName",
            ["resourcename"],
            [resourcename],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.getResourceOwnerHistoryInfo = function(STATEHISTORYID, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "getResourceOwnerHistoryInfo",
            ["STATEHISTORYID"],
            [STATEHISTORYID],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetNumberFromStr = function(str, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetNumberFromStr",
            ["str"],
            [str],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetType = function(ResourceName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetType",
            ["ResourceName"],
            [ResourceName],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };
    me.CheckAndUpdateAssociationAsset = function(resourceName, parentAsset, updateField, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "CheckAndUpdateAssociationAsset",
            ["resourceName", "parentAsset", "updateField"],
            [resourceName, parentAsset, updateField],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateWorkstationTextFieldByName = function(columnName, value, resourceName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateWorkstationTextFieldByName",
            ["columnName", "value", "resourceName"],
            [columnName, value, resourceName],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAllAssetDetail = function(resourcename, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAllAssetDetail",
            ["resourcename"],
            [resourcename],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetResourceStateByAssetName = function(resourcename, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetResourceStateByAssetName",
            ["resourcename"],
            [resourcename],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetListAssetState = function (keyword, startindex, limit, onSuccess, onError, dontSkip) {
        me.legacyCMDB(
            me.ServiceURL,
            "GetListAssetState",
            ["keyword", "startindex", "limit"],
            [keyword, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetStateList = function(state, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetStateList",
            ["state", "startindex", "limit"],
            [state, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };

    me.GetAssetByType = function(typeID, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetByType",
            ["typeID", "startindex", "limit"],
            [typeID, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetByModel = function(modelID, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetByModel",
            ["modelID", "startindex", "limit"],
            [modelID, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetByTypeAndModel = function(typeID, modelID, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetByTypeAndModel",
            ["typeID", "modelID", "startindex", "limit"],
            [typeID, modelID, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetByTypeAndModelName = function(type, modelName, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetByTypeAndModelName",
            ["type", "modelName", "startindex", "limit"],
            [type, modelName, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetInStoreByTypeAndModelAndName = function(type, modelName, name, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetInStoreByTypeAndModelAndName",
            ["type", "modelName", "name", "startindex", "limit"],
            [type, modelName, name, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetInStoreByTypeAndModel = function(typeID, modelID, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetInStoreByTypeAndModel",
            ["typeID", "modelID", "startindex", "limit"],
            [typeID, modelID, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.checkAssetInStore = function(resourceName, type, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "checkAssetInStore",
            ["resourceName", "type"],
            [resourceName, type],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetByTypeAndModelAndName = function(type, modelName, name, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetByTypeAndModelAndName",
            ["type", "modelName", "name", "startindex", "limit"],
            [type, modelName, name, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetCost = function(assetname, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetCost",
            ["assetname"],
            [assetname],
            APIData.Ajax.CmdbCallback.float,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetDefaultUserBySite = function(listUser, site, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetDefaultUserBySite",
            ["listUser", "site"],
            [listUser, site],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetPersonInfoByUsername = function(userName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetPersonInfoByUsername",
            ["userName"],
            [userName],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetListSite = function(siteName, startIndex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetListSite",
            ["siteName", "startIndex", "limit"],
            [siteName, startIndex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.getUserAccountByEmployeeId = function(employeeId, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "getUserAccountByEmployeeId",
            ["employeeId"],
            [employeeId],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetPositionOfUser = function(username, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetPositionOfUser",
            ["username"],
            [username],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.getListEmployeeTitle = function(name, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "getListEmployeeTitle",
            ["name"],
            [name],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.updateGeneralInformation = function(resourceName, detailJson, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "updateGeneralInformation",
            ["resourceName", "detailJson"],
            [resourceName, detailJson],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.getGeneralInfo = function(resourceName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "getGeneralInfo",
            ["resourceName"],
            [resourceName],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.getUserADInformation = function(username, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "getUserADInformation",
            ["username"],
            [username],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAllResourceStateHistoryByResourceName = function(resourceName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAllResourceStateHistoryByResourceName",
            ["resourcename"],
            [resourceName],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
})();

/*
 CMDBEX: ______________________________________________________________________________________
 */
APIData.CMDBEX = new function () {
    var me = this;
    me.ServiceURL = "CMDBEX";
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'CMDB'
    });
    me.UpdateAssetState = function(componentType, ciName, state, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssetState",
            ["componentType", "ciName", "state"],
            [componentType, ciName, state],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateAssetOwner = function(componentType, ciName, userName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssetOwner",
            ["componentType", "ciName", "userName"],
            [componentType, ciName, userName],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateAssetDetail = function(componentType, ciName, detailstr, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssetDetail",
            ["componentType", "ciName", "detailstr"],
            [componentType, ciName, detailstr],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateAssociateAsset = function(componentType, ciName, parent, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssociateAsset",
            ["componentType", "ciName", "parent"],
            [componentType, ciName, parent],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetListAssetDetailHistory = function(resourceName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetListAssetDetailHistory",
            ["resourceName"],
            [resourceName],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetListAssetModel = function(keyword, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetListAssetModel",
            ["keyword", "startindex", "limit"],
            [keyword, startindex, limit || 7],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.SearchProduct = function(keyword, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "SearchProduct",
            ["keyword", "startindex", "limit"],
            [keyword, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetParentsAssets = function(assetNames, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetParentsAssets",
            ["assetNames"],
            [JSON.stringify(assetNames)],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };

};







APIData.Component = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'Component'
    });

    this.paging = function Paging(keyword, pageIndex, limit, callback) {
        me.send({
            keyword: keyword,
            pageIndex: pageIndex,
            limit: limit
        }, callback);
    };

    this.delete = function Delete(id, callback){
        me.send({
            id: id
        }, callback);
    };

    this.create = function CreateNew(data, callback) {
        me.send({
            newData: data
        }, callback);
    };

    this.update = function Update(data, callback) {
        me.send({
            newData: data
        }, callback);
    };

    this.GetQuantityInHand = function GetQuantityInHand(modelID, callback) {
        me.send({
            modelID: modelID
        }, callback);
    };
    this.GetByID = function GetByID(id, callback) {
        me.send({
            id: id
        }, callback);
    };
    
    this.getByTypeAndName = function GetByTypeAndName(typeName, modelName, pageIndex, limit, callback) {
        me.send({
            typeName: typeName,
            modelName: modelName,
            pageIndex: pageIndex,
            limit: limit
        }, callback);
    };

    this.GetProductSlideByType = function GetProductSlideByType(type, callback) {
        me.send({
            type: type
        }, callback);
    };

    this.SearchPaging = function SearchPaging(keyword, pageIndex, limit, isOwned, userDomain, type, callback) {
        me.send({
            keyword: keyword,
            pageIndex: pageIndex,
            limit: limit,
            isOwned: isOwned,
            userDomain: userDomain,
            type: type
        }, callback);
    }
})();APIData.ComponentType = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'ComponentType'
    });

    this.paging = function Paging(keyword, pageIndex, limit, callback) {
        me.send({
            keyword: keyword,
            pageIndex: pageIndex,
            limit: limit
        }, callback);
    };

    this.delete = function Delete(id, callback){
        me.send({
            id: id
        }, callback);
    };

    this.create = function CreateNew(data, callback) {
        me.send({
            newData: data
        }, callback);
    };

    this.update = function Update(data, callback) {
        me.send({
            newData: data
        }, callback);
    };
    this.GetAllWithComponent = function GetAllWithComponent(callback) {
        me.send({

        }, callback);
    }
})();APIData = window.APIData || {};

APIData.RequestDelegate = new function () {
    var t = this;
    t.Service = "Workflow";
    APIData.AbstractAPIData.call(this, {
        'class': 'Workflow',
        'type': 'AMRequestDelegateAjax'
    });

    t.GetDelegate = function (domain, onSuccess, onError, dontSkip) {
        var data = {
            domain: domain
        };
        t.ITAM(
            t.Service,
            "AMRequestDelegateAjax",
            "GetDelegate",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    t.CanSettingForAnotherUser = function (onSuccess, onError, dontSkip) {
        var data = null;
        t.ITAM(
            t.Service,
            "AMRequestDelegateAjax",
            "CanSettingForAnotherUser",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    t.InsertDelegate = function (delegateData, onSuccess, onError, dontSkip) {
        var data = {
            data: delegateData
        };
        t.ITAM(
            t.Service,
            "AMRequestDelegateAjax",
            "InsertDelegate",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    t.UpdateDelegate = function (delegateData, onSuccess, onError, dontSkip) {
        var data = {
            data: delegateData
        };
        t.ITAM(
            t.Service,
            "AMRequestDelegateAjax",
            "UpdateDelegate",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    t.DeleteDelegate = function (domain, onSuccess, onError, dontSkip) {
        var data = {
            domain: domain
        };
        t.ITAM(
            t.Service,
            "AMRequestDelegateAjax",
            "DeleteDelegate",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    t.DelegateRequests = function (stepIDs, delegatedDomain, comment, attachments, onSuccess, onError, dontSkip) {
        var data = {
            stepIDs: stepIDs,
            delegatedDomain: delegatedDomain,
            comment: comment,
            attachments: attachments
        };
        t.ITAM(
            t.Service,
            "AMRequestDelegateAjax",
            "DelegateRequests",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
}

APIData.Document = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Wordpress',
        'type': 'Document'
    });

    this.GetDocumentList = function GetDocumentList(categoryName, callback) {
        me.send({
            categoryName: categoryName
        }, callback);
    };

})();APIData = window.APIData || {};

APIData.EditPage = {};
APIData.EditPage = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Options',
        'type': 'EditPage'
    });

    this.GetPageContent = function GetPageContent(page, callback) {
        me.send({
            page: page
        }, callback);
    };

    this.GetPageContentForBinding = function GetPageContentForBinding(page, callback) {
        me.send({
            page: page
        }, callback);
    };

    this.getValuePagecontent = function getValuePagecontent(name, page, callback){
        me.send({
            name: name,
            page: page
        }, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.isExistPageContent = function isExistPageContent(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.AddNew = function AddNew(data, callback) {
        me.send({
            data: data
        }, callback);
    };

})();APIData = window.APIData || {};

APIData.Asset = {};
APIData.Asset.Service = "amAsset";
APIData.Asset.InsertAssetDetailHistory = function (assetName, newDetail, previousDetail, comment, sysComment, userAction, onSuccess, onError, dontSkip) {
    var data = {
        AssetName: assetName,
        NewDetail: newDetail,
        PreviousDetail: previousDetail,
        UserAction: userAction || Common.getCurrentUser(),
        Comment: comment,
        SysComment: sysComment
    };

    APIData.Ajax.ITAM3(
        APIData.Asset.Service,
        "AMAssetDetailHistory",
        "InsertAssetDetailHistory",
        {data: JSON.stringify(data)},
        APIData.Ajax.Callback.object,
        onSuccess,
        onError,
        dontSkip
    );
};
APIData.Asset.GetAssetDetailHistory = function (assetName, onSuccess, onError, dontSkip) {

    APIData.Ajax.ITAM3(
        APIData.Asset.Service,
        "AMAssetDetailHistory",
        "GetAssetDetailHistory",
        {assetName: assetName },
        APIData.Ajax.Callback.array,
        onSuccess,
        onError,
        dontSkip
    );
};
APIData.Asset.GetAssetStateHistoryByCMDBHistoryID = function (cmdbHistoryID, onSuccess, onError, dontSkip) {
    APIData.Ajax.ITAM3(
        APIData.Asset.Service,
        "AMAssetStateHistory",
        "GetAssetStateHistoryByCMDBHistoryID",
        {CMDBHistoryID: cmdbHistoryID },
        APIData.Ajax.Callback.object,
        onSuccess,
        onError,
        dontSkip
    );
};
APIData.Asset.InsertStateHistory = function (stateId, assetName, newState, previousState, actionDate, userAction, targetUser, comment, sysComment, onSuccess, onError, dontSkip) {

    actionDate = new Date(actionDate);
    actionDate.setHours(actionDate.getHours() - actionDate.getTimezoneOffset() / 60);
    var data = {
        stateId: stateId,
        assetName: assetName,
        newState: newState,
        previousState: previousState,
        actionDate: actionDate,
        userAction: userAction,
        comment: comment,
        sysComment: sysComment
    };

    APIData.Ajax.ITAM3(
        APIData.Asset.Service,
        "AMAssetStateHistory",
        "InsertStateHistory",
        {data: JSON.stringify(data)},
        APIData.Ajax.Callback.object,
        onSuccess,
        onError,
        dontSkip
    );
};

APIData.Asset.GetAssetStateHistory = function (assetName, onSuccess, onError, dontSkip) {

    APIData.Ajax.ITAM3(
        APIData.Asset.Service,
        "AMAssetStateHistory",
        "GetAssetStateHistory",
        {assetName: assetName },
        APIData.Ajax.Callback.array,
        onSuccess,
        onError,
        dontSkip
    );
};

var AssetState = {
    InUse: "In Use",
    InUseAssociate: "In Use(associate)",
    InStoreAssetState: "In Store",
    Expired: "Expired",
    Disposed: "Disposed",
    InWarranty: "In Warranty",
    Lost: "Lost",
    Retired: "Retired",
    Delivered: "Delivered",
    Error: "Error"
};APIData = window.APIData || {};

APIData.MainMenu = {};
APIData.MainMenu = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Options',
        'type': 'MainMenu'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.GetAsMenu = function GetAsMenu(callback) {
        me.send({
        }, callback);
    };

    this.AddNew = function AddNew(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Delete = function Delete(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.GetByID = function GetByID(ID, callback) {
        me.send({
            ID: ID
        }, callback);
    };
    this.UpdateMapping = function UpdateMapping(ID, mapping, callback) {
        me.send({
            ID: ID,
            mapping: mapping
        }, callback);
    };
    this.GetProducts = function GetProducts(parentName, subName, filter, pageIndex, limit, callback) {
        me.send({
            parentName: parentName,
            subName: subName,
            filter: filter,
            pageIndex: pageIndex,
            limit: limit
        }, callback);
    }
})();APIData = window.APIData || {};

APIData.MenuNotes = {};
APIData.MenuNotes = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Options',
        'type': 'MenuNotes'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.GetByName = function GetByName(menuName, callback){
        me.send({
            menuName: menuName
        }, callback);
    };

    this.Update = function Update(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.UpdateByName = function Update(menuName, data, callback) {
        me.send({
            data: data,
            menuName: menuName
        }, callback);
    };
})();APIData.MenuType = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Options',
        'type': 'MenuType'
    });

    this.getAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.create = function AddNew(typeName, callback) {
        me.send({
            data: {
                Name: typeName
            }
        }, callback);
    };

    this.update = function Update(id, typeName, callback) {
        me.send({
            data: {
                ID: id,
                Name: typeName
            }
        }, callback);
    };

    this.delete = function Delete(id, callback) {
        me.send({
            data: {
                ID: id
            }
        }, callback);
    }

})();APIData = window.APIData || {};

APIData.Notification = new (function () {
    var me = this;
    me.Service = "Notification";
    APIData.AbstractAPIData.call(this, {
        'class': 'NotificationAjax',
        'type': 'Notification'
    });

    me.CountUnseen = function (onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "NotificationAjax",
            "CountUnseen",
            {},
            APIData.Callback.int,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.GetListForPreview = function (onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "NotificationAjax",
            "GetListForPreview",
            {},
            APIData.Callback.array,
            function (res) {
                if (res.code == AMResponseCode.ok) {
                    res.data.sort(
                        function (x, y) {
                            return x.ID < y.ID;
                        }
                    );
                }
                onSuccess(res);
            },
            onError,
            dontSkip
        );
    };

    me.GetQuantity = function (typeID, isRead, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "NotificationAjax",
            "GetQuantity",
            {typeID: typeID, isRead: isRead},
            APIData.Callback.int,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.GetList = function (typeID, isRead, start, limit, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "NotificationAjax",
            "GetList",
            {typeID: typeID, start: start, limit: limit, isRead: isRead},
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.MarkAsRead = function (ids, isRead, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "NotificationAjax",
            "MarkAsRead",
            {ids: ids, isRead: isRead},
            APIData.Callback.string,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.DeleteNotification = function (ids, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "NotificationAjax",
            "DeleteNotification",
            {ids: ids},
            APIData.Callback.string,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.ListenNew = function (signalrID, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "NotificationAjax",
            "ListenNew",
            {signalrID: signalrID},
            APIData.Callback.string,
            onSuccess,
            onError,
            dontSkip
        );
    };
})();





APIData = window.APIData || {};

APIData.Options = {};
APIData.Options = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Options',
        'type': 'AMOptions'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };
    this.GetByID = function GetByID(ID, callback) {
        me.send({
            ID: ID
        }, callback);
    };
    this.AddNew = function AddNew(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.GetByCodeAndName = function GetByCodeAndName(optionName, optionCode, callback) {
        me.send({
            optionName: optionName,
            optionCode: optionCode
        }, callback);
    };

    this.Delete = function Delete(data, callback) {
        me.send({
            data: data
        }, callback);
    };
})();APIData = window.APIData || {};

APIData.PersonalMenu = {};
APIData.PersonalMenu = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Options',
        'type': 'PersonalMenu'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.GetByName = function GetByName(menuName, callback){
        me.send({
            menuName: menuName
        }, callback);
    };

    this.Update = function Update(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.UpdateByName = function Update(menuName, data, callback) {
        me.send({
            data: data,
            menuName: menuName
        }, callback);
    };
})();APIData.PostCategory = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Wordpress',
        'type': 'PostCategory'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.FilterPaging = function FilterPaging(data, callback) {
        me.send(data, callback);
    }

})();APIData = window.APIData || {};

function EventManager() {
    var me = this;

    me.on = function (name, evenHandler) {
        var item = me[name];
        if(item == null)
            item = me[name] = new eventItem();

        item.bind(evenHandler);
    };

    me.off = function (name, evenHandler) {
        var item = me[name];
        if(item)
            item.unbind(evenHandler);
    };

    function eventItem() {
        var list = [];

        this.bind = function (eventHandler) {
            list.push(eventHandler);
        }

        this.unbind = function (eventHandler) {
            Utils.removeFromArray(list, eventHandler);
        }

        this.trigger = function () {
            list.forEach(function (fn) {
                fn.apply(this, arguments);
            });
        }
    }
}

APIData.Request = new function () {
    var me = this;
    var wfID;
    var rqID;
    me.Service = "Workflow";
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'CMDB'
    });

    /**
     * Tạo một đối tượng tương ứng với requestData mà service trả về,
     * bổ sung các method util để mapping, get, set data với definition
     */
    me.RequestData = function (requestData) {

        var t = this;
        Object.assign(t, requestData);
        // t.workflow ;
        // t.request ;
        // this.listFieldDefinition;
        // t.listStepDefinition ;
        // t.field ;
        // t.listStep ;

        function fnCheckNull(listFieldDefinition, dataModel) {
            for(var i = 0; i < listFieldDefinition.length; i++){
                var fieldDefinition = listFieldDefinition[i]
                var can = t.currentFieldCan[fieldDefinition.Name];
                var val = null;

                if(fieldDefinition.getEditData){
                    val = fieldDefinition.getEditData();
                }else if(dataModel){
                    val = dataModel[fieldDefinition.Name];
                }else{
                    val = fieldDefinition.value;
                }

                if(can.update && !can.empty){
                    var isEmpty = false;
                    if(typeof val == 'string'){
                        isEmpty = val.trim() == '';
                    }else if(val === undefined || val === null){
                        isEmpty = true;
                    }else if(Array.isArray(val) && !val.length){
                        isEmpty = true;
                    }

                    if(isEmpty)
                        return '<b>' + fieldDefinition.DisplayName + '</b> không thể bỏ trống';
                }

                if(fieldDefinition.listChild && val){
                    for(var j = 0; j < val.length; j++) {
                        var result = fnCheckNull(fieldDefinition.listChild, val[j]);
                        if(result)
                            return result;
                    }
                }
            }

            return null;
        }

        t.events = new EventManager();

        t.setField = function (field) {
            if (field) {
                t.listFieldDefinition.forEach(function (fieldDefinition) {
                    fieldDefinition.value = field[fieldDefinition.Name];
                });
            } else {
                t.listFieldDefinition.forEach(function (fieldDefinition) {
                    fieldDefinition.value = null;
                });
            }
        };

        t.setListStep = function (listStep) {
            if (listStep) {
                listStep.forEach(function (step) {
                    var n = t.listStepDefinition.length;
                    for (var i = 0; i < n; i++) {
                        var stepDefinition = t.listStepDefinition[i];
                        if (stepDefinition.ID === step.DefinitionID) {
                            stepDefinition.step = step;
                            step.definition = stepDefinition;
                        }
                    }
                });
            } else {
                t.listStepDefinition.forEach(function (stepDefinition) {
                    var step = new APIData.Workflow.RequestStepEntity();
                    step.DefinitionID = stepDefinition.ID;
                    stepDefinition.step = step;
                    step.definition = stepDefinition;
                });
            }
        };

        t.getField = function (sortAndCanUpdateOnly) {
            var re = {};
            t.listFieldDefinition.forEach(function (fieldDefinition) {
                re[fieldDefinition.Name] = fieldDefinition.value;
            });

            // filter and sort data
            if(sortAndCanUpdateOnly){
                // dev_upgrade
            }

            return re;
        };

        t.getFieldDefinition = function (name, fields) {
            var l = fields || t.listFieldDefinition;
            var n = l.length;
            for (var i = 0; i < n; i++) {
                if (l[i].Name == name)
                    return l[i];
            }
        };

        t.loadFieldDefinition = function (workflowID, callBack) {
            APIData.Workflow.GetListWorkflowField(workflowID, function (res) {
                if (res.code == AMResponseCode.ok){
                    t.listFieldDefinition = res.data;
                    wfID = workflowID;
                    callBack(res);
                } else {
                    t.listFieldDefinition = [];
                    callBack(res);
                }
            });
        };

        t.createRequest = function (callBack, cancelCallBack) {
            if (t.createRequest.isProcessing)
                return;

            function create(comment) {
                t.createRequest.isProcessing = 1;
                var field = t.getField();
                APIData.Request.CreateRequest(wfID || t.workflow.ID, field, null, function (res) {
                    t.createRequest.isProcessing = 0;
                    if (res && res.code == AMResponseCode.ok) {
                        rqID = res.data.request.ID;
                        t.listFieldDefinition = res.data.listFieldDefinition;
                        callBack(res);
                    } else {
                        callBack(res);
                    }
                }, function (error) {
                    t.createRequest.isProcessing = 0;
                    callBack({});
                });
            }

            if (cancelCallBack) {
                var jo = $('<strong>Bạn chắc chắn muốn tạo đơn hàng ?</strong>');
                Component.System.alert.show(jo,
                    {
                        name: "Tạo đơn hàng", cb: create
                    },
                    {
                        name: "Xem lại",
                        cb: function () {
                            cancelCallBack()
                        }
                    }
                )
            } else {
                create();
            }

        };

        t.getRequestOwner = function () {
            if(t.request == null)
                return null;

            var ownerField = t.getFieldDefinition(t.workflow.OwnerField);
            var domain = ownerField.RequestDataTypeID == APIData.Workflow.RequestDataTypeID.domain ?
                ownerField.value : ownerField.value.Domain;

            return domain;
        };

        t.checkData = function () {
            return fnCheckNull(t.listFieldDefinition);
        };

        /**
         *
         * @param maxLength độ dài mô tả tối đa trên 1 dòng. Giá trị phải là số lớn hơn hoặc bằng 5, nếu không sẽ không giới hạn số ký tự
         * @param maxLine số dòng tối đa. Nếu maxLine = 1, 1 dòng sẽ mô tả cho tất cả các sản phẩm. Mặc định bằng 1
         */
        t.getDescription = function (maxLength, maxLine) {
            if (maxLength < 5 || isNaN(maxLength))
                maxLength = null;

            if (maxLine < 1 || isNaN(maxLength))
                maxLine = 1;

            var tableField, modelField, numberField;
            var fields = this.listFieldDefinition;
            var requestDataTypeID = APIData.Workflow.RequestDataTypeID;
            for (var i = 0; i < fields.length; i++) {
                if (fields[i].RequestDataTypeID == requestDataTypeID.table) {
                    tableField = fields[i];
                    break;
                }
            }

            for (var i = 0; i < fields.length; i++) {
                var field = fields[i];
                if (field.ParentID = tableField.ID) {
                    if (field.RequestDataTypeID == requestDataTypeID.assetModel)
                        modelField = field;
                    else if (field.RequestDataTypeID == requestDataTypeID.number)
                        modelField = field;
                }
            }

            var table = tableField.value;
            var group = [];
            for (var i = 0; i < table.length; i++) {
                var row = table[i];
                var modelName = row[modelField.name];
                var model = group.find(function (item) {
                    return item.name = modelName;
                });

                if(model == null){
                    model = {
                        name: modelName,
                        count: 0,
                    };

                    group.push(model);
                }

                if (numberField == null || isNaN(row[numberField.name]))
                    model.count++;
                else
                    model.count += row[numberField.name];
            }

            var re = '';
            for (var i = 0; i < group.length; i++) {
                var model = group[i];
                if (re) {
                    if (maxLine == 1)
                        re += ', ';
                    else
                        re += '<br>';
                }

                var line = model.name;
                var count = model.count;
                if (count > 1)
                    line += ' (' + count + ')';

                if (maxLine == 1)
                    line = re + line;

                if (maxLength != null && line.length > maxLength){
                    line = line.substr(0, maxLength - 3) + '... ';
                    if (maxLine > 1)
                        line+= '<br>';
                    line+='và'
                }

                if (maxLine == 1)
                    re = line;
                else
                    re+=line;

            }

            return re;
        };

        t.isDisposed = function () {
            var disposedStatus = (me.RequestStatusID.done |me.RequestStatusID.cancel);
            return t.request.RequestStatusID & disposedStatus;
        };
    };

    me.RequestStatusID = {open: 1, done: 2, cancel: 4};
    me.GetListRequestStatus = function (onSuccess, onError, dontSkip) {
        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestAjax",
            "GetListRequestStatus",
            {},
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetRequestEditData = function (ID, onSuccess, onError, dontSkip) {
        var data = {
            ID: ID,
        };

        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestAjax",
            "GetRequestEditData",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetReportingLine = function (domain, onSuccess, onError, dontSkip) {
        var data = {
            dataSearch: {
                SearchDataTypeID: 8,
                SearchValue: '',
                Constrains: [
                    {
                        SearchDataTypeID: 8,
                        SearchValue: domain,
                        ConstrainName: 'reporting-emp',
                        IsCheckOnly: false
                    }
                ]
            }
        };

        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestFieldDataAjax",
            "Search",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetRequestDefinition = function (workFlowID, onSuccess, onError, dontSkip) {
        var data = {
            workFlowID: workFlowID
        };

        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestAjax",
            "GetRequestDefinition",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetRequestDefinitionIDByDisplayName = function (displayName, onSuccess, onError, dontSkip) {
        var data = {
            displayName: displayName
        };

        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestAjax",
            "GetRequestDefinitionIDByDisplayName",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetListRequest = function (displayname, startindex, limit, onSuccess, onError, dontSkip) {
        var data = {
            displayname: displayname,
            startindex: startindex,
            limit: limit
        };

        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestAjax",
            "GetListRequest",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.ApproveRequestStep = function (requestID, stepIndex, isApproved, comment, attachments, field, onSuccess, onError, dontSkip) {

        var data = {
            requestID: requestID,
            stepIndex: stepIndex,
            isApproved: isApproved,
            comment: comment,
            field: field,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "ApproveRequestStep",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.CancelRequest = function (requestID, comment, attachments, onSuccess, onError, dontSkip) {
        var data = {
            ID: requestID,
            comment: comment,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "CancelRequest",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.PickupStepApproval = function (stepID, comment, attachments, onSuccess, onError, dontSkip) {
        var data = {
            stepID: stepID,
            comment: comment,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "PickupStepApproval",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.RaiseRequestStep = function (stepID, comment, attachments, onSuccess, onError, dontSkip) {
        var data = {
            ID: stepID,
            comment: comment,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "RaiseRequestStep",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.CreateRequest = function (workflowID, field, comment, onSuccess, onError, dontSkip) {
        var data = {
            workflowID: workflowID,
            field: JSON.stringify(field),
            comment: comment
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "CreateRequest",
            data,
            APIData.Ajax.Callback.string,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.EntrustRequestStep = function (stepID, domain, comment, attachments, onSuccess, onError, dontSkip) {

        var data = {
            stepID: stepID,
            domain: domain,
            comment: comment,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "EntrustRequestStep",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.GetUserRequestsByStatus = function (statusID, onSuccess, onError, dontSkip) {
        var data = {
            statusID: statusID
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "GetUserRequestsByStatus",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.SearchUserRequestsByStatus = function (statusIDs, onSuccess, onError, dontSkip) {
        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestAjax",
            "SearchUserRequestsByStatus",
            {statusIDs: statusIDs},
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetRequestsForApproval = function (onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "GetRequestsForApproval",
            {},
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.GetUserRecentRequests = function (onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "GetUserRecentRequests",
            {},
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.UpdateRequest = function (requestID, field, onSuccess, onError, dontSkip) {

        var data = {
            requestID: stepID,
            field: field
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "EntrustRequestStep",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.GetListSubStepData = function (requestID, onSuccess, onError, dontSkip) {

        var data = {
            requestID: requestID,
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "GetListSubStepData",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.RequestComment = function (stepID, mentionDomains, questionComment, attachments, onSuccess, onError, dontSkip) {
        var data = {
            stepID: stepID,
            mentionDomains: mentionDomains,
            comment: questionComment,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "RequestComment",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.AnswerComment = function (stepID, mentionDomains, answerComment, attachments, onSuccess, onError, dontSkip) {
        var data = {
            stepID: stepID,
            mentionDomains: mentionDomains,
            comment: answerComment,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "AnswerComment",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.Comment = function (stepID, comment, mentionDomains, attachments, onSuccess, onError, dontSkip) {
        var data = {
            stepID: stepID,
            mentionDomains: mentionDomains,
            comment: comment,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "Comment",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
};
APIData.RequestDataType = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'ResourceType'
    });

    this.getAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.create = function CreateNew(type, callback) {
        me.send({
            type: type
        }, callback);
    };

    this.update = function Update(id, type, callback) {
        me.send({
            id: id,
            type: type
        }, callback);
    }

})();APIData = window.APIData || {};

APIData.RequestFeatureMapping = {};
APIData.RequestFeatureMapping = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Workflow',
        'type': 'APIRequestFeatureMapping'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.Insert = function Insert(data, callback) {
        me.send({
            data:data
        }, callback);
    };

    this.Update = function Update(data, callback) {
        me.send({
            data:data
        }, callback);
    };

    this.GetByName = function GetByName(name, callback) {
        me.send({
            featureName: name
        }, callback);
    };

    this.GetRequestID = function GetRequestID(requestID, callback) {
        me.send({
            requestID: requestID
        }, callback);
    };
    this.GetPaging = function GetPaging(keyword, pageIndex, limit, callback) {
        me.send({
            keyword: keyword,
            pageIndex: pageIndex,
            limit: limit
        }, callback);
    }
})();APIData = window.APIData || {};

APIData.RequestFieldData = new function () {
    var me = this;
    me.Service = "Workflow";
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'CMDB'
    });


    function Search(dataSearch, isCheckOnly ,onSuccess, onError, dontSkip) {
        dataSearch.IsCheckOnly = isCheckOnly;

        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestFieldDataAjax",
            "Search",
            {dataSearch: dataSearch},
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }

    me.Search = function (dataSearch, onSuccess, onError, dontSkip){
        if(dataSearch.SearchDataTypeID == 7)
            console.log(dataSearch); // dev_test

        Search(dataSearch, false ,onSuccess, onError, dontSkip);

    }

    me.Check = function (dataSearch, onSuccess, onError, dontSkip){
        Search(dataSearch, true ,onSuccess, onError, dontSkip);
    }
};
APIData.RequestReason = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'ResourceCategory'
    });

    this.getAll = function getAll(callback) {
        me.send({
        }, callback);
    };
})();APIData.ResourceCategory = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'ResourceCategory'
    });

    this.getAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.create = function CreateNew(category, callback) {
        me.send({
            category: category
        }, callback);
    };

    this.update = function Update(id, category, callback) {
        me.send({
            id: id,
            category: category
        }, callback);
    }

})();APIData.ResourceType = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'ResourceType'
    });

    this.getAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.create = function CreateNew(type, callback) {
        me.send({
            type: type
        }, callback);
    };

    this.update = function Update(id, type, callback) {
        me.send({
            id: id,
            type: type
        }, callback);
    };

    this.getAsMenu = function GetAsMenu(callback) {
        me.send({
        }, callback);
    };

    this.GetAllWithComponentType = function GetAllWithComponentType(callback) {
        me.send({
        }, callback);
    }

})();APIData.Resources = new (function () {
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'Resources'
    });

    this.GetUserAsset = function GetUserAsset(username, asset, keyword, pageIndex, limit, callback, isForHelpDesk) {
        var data = {
            username: username,
            keyword: keyword,
            limit: limit,
            asset: asset,
            pageIndex: pageIndex,
            viewType: 'detail',
            isForHelpDesk: isForHelpDesk === undefined ? false : isForHelpDesk
        }
        me.send(data, callback);
    };
})();APIData = window.APIData || {};

APIData.Search = new (function(){
    var me = this;
    me.Service = "Search";
    APIData.AbstractAPIData.call(this, {
        'class': 'SearchAjax',
        'type': 'Search'
    });

    me.HomeSearch = function (keyword, start, limit, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "SearchAjax",
            "HomeSearch",
            {keyword: keyword, startindex: start, limit: limit},
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    }

    me.PageSearch = function (keyword, start, limit, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "SearchAjax",
            "PageSearch",
            {keyword: keyword, startindex: start, limit: limit},
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    }
})();





APIData = window.APIData || {};

APIData.RequestSearch = new (function(){
    var me = this;
    me.Service = "Search";
    APIData.AbstractAPIData.call(this, {
        'class': 'RequestSearchAjax',
        'type': 'RequestSearch'
    });

    me.GetListRequestSearch = function (onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "GetListRequestSearch",
            null,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.GetRequestSearchEditData = function (id, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "GetRequestSearchEditData",
            {ID: id},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.GetRequestSearchEditData = function (id, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "GetRequestSearchEditData",
            {ID: id},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.DeleteRequestSearch = function (id, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "DeleteRequestSearch",
            {ID: id},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.Search = function (requestSearchID, condition, onSuccess, onError, dontSkip){
        var data = {
            configID: requestSearchID,
            q: condition
        };

        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "Search",
            data,
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };

    // dev_upgrade
    // code tạm để lấy data cho chức năng xuất nhập kho, sau khi hoàn thiện tính năng search request
    // thì có thể bỏ
    me._temp_SearchStoreRequest = function (onSuccess, onError, dontSkip){
        var data = {
            configID: 6,
            wfStepIDs: [404]
        };

        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "Search",
            data,
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.InsertSearchRequest = function (requestSearch, columns, conditions, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "InsertRequestSearchData",
            {data: requestSearch, columns: columns, conditions: conditions},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }

    me.UpdateSearchRequest = function (requestSearch, columns, filters, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "UpdateRequestSearchData",
            {data: requestSearch, columns: columns, filters: filters},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
})();





APIData = window.APIData || {};

APIData.ShopCart = {};
APIData.ShopCart = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'Component'
    });

})();function SignalREngine(url, hub) {
    var me = this;
    var fn = SignalREngine;
    var onGets = [];

    me.url = url;
    me.chat = null;
    me.isConnected = 0;
    me.signalRID = null;

    var getUrl = $.signalR.transports._logic.getUrl;
    $.signalR.transports._logic.getUrl = function(connection, transport, reconnecting, poll, ajaxPost) {
        var url = getUrl(connection, transport, reconnecting, poll, ajaxPost);
        return connection.url + url.substring(url.indexOf(connection.appRelativeUrl) + connection.appRelativeUrl.length);
    };

    var timeIntervalReconnect = 0;
    var connectedCount = 0;

    function onGetMessage(message) {
        me.isConnected = 1;

        var hubMessage = JSON.parse(message);
        var methods = onGets[hubMessage.method];

        console.log('SIGNALR RECEIVE METHOD: ' + hubMessage.method + ", Data: ");
        console.log(hubMessage.data);

        if (methods){
            for (var i = 0; i < methods.length; i++)
                methods[i](hubMessage.message);
        }
    }

    function setupDisconnected() {
        $.connection.hub.disconnected(function () {
            me.isConnected = 0;
            if (me.onDisconnected.list) {
                var list = me.onDisconnected.list;
                for (var i = 0; i < list.length; i++)
                    list[i](connectedCount, $.connection.hub.id);
                me.onDisconnected.list = [];
            }

            if (timeIntervalReconnect != 0)
                return;

            clearTimeout(timeIntervalReconnect);
            timeIntervalReconnect = setInterval(function () {
                me.connect();
            }, SignalREngine.reconnectDuration);
        });
    }

    me.connect = function () {
        if (!$.connection) {
            fn.onScriptLoadDone(me.connect);
            return;
        }

        /// open
        $.connection.hub.url = me.url;
        me.chat = $.connection.NotificationHub;

        // receive message
        me.chat.client.broadcastMessage = onGetMessage;

        // connect
        $.connection.hub.start().done(function () {
            me.isConnected = 1;
            connectedCount++;
            clearTimeout(timeIntervalReconnect);
            timeIntervalReconnect = 0;

            me.signalRID = $.connection.hub.id;
            console.log("Hub connect time: " + connectedCount + ". ID = " + me.signalRID);

            if (me.onConnected.list) {
                var list = me.onConnected.list;
                for (var i = 0; i < list.length; i++)
                    list[i](connectedCount, $.connection.hub.id);
                me.onConnected.list = [];
            }
        });
    };
    me.onGet = function (methodName, methodCallBack) {
        var list = onGets[methodName];
        if (list == null)
            list = onGets[methodName] = [];
        list.push(methodCallBack);
    };
    me.offGet = function (methodName, methodCallBack) {
        var list = onGets[methodName];
        if (list == null)
            return;

        var index = list.indexOf(methodCallBack);
        list.splice(index, 1);
    };
    me.onConnected = function (callback) {
        var list = me.onConnected.list || (me.onConnected.list = []);
        list.push(callback);
    };
    me.onDisconnected = function (callback) {
        var list = me.onDisconnected.list || (me.onDisconnected.list = []);
        list.push(callback);
    };
    me.send = function (method, data, ids, typeOfReciver) {
        if (me.isConnected == false) {
            me.onConnected(function () {
                me.send(method, data, ids, typeOfReciver);
            });
        }

        var temp = {
            method: method,
            message: data,
            reciverIDs: ids,
            typeOfReciverID: typeOfReciver
        };
        me.chat.server.send(JSON.stringify(temp));
    };

    if ($.connection)
        setupDisconnected();
    else
        fn.onScriptLoadDone(setupDisconnected);
}

// global init
$(function () {
    var fn = SignalREngine;
    var connectOnLoads = [];

    fn.reconnectDuration = 60000;
    fn.onScriptLoadDone = function (callback) {
        connectOnLoads.push(callback);
    };
    fn.connectAfter = function () {
        connectOnLoads.forEach(function (callback) {
            callback();
        });
    };
});

// itportal implement
$(function () {

    var fn = SignalREngine;

    // one instanse for one doc
    fn.getDefault = function () {
        return {
            onConnected: function () { },
            connect: function () { },
            onGet: function () { },
        };
    };

    // type of reciver id
    fn.typeOfReciverID = {
        signalRID: 1,
        Domain: 2,
        all: 3
    };

    // type of reciver id
    fn.method = {
        newNoti: 1,
        showRequestQR: 2,
        showRequestReply: 3
    }

    return; // tắt tránh tải trang lâu

    var comHost = location.protocol == "https:"  ?
        "https://it.vng.com.vn/communication/"
        //: "https://api-it.vng.com.vn/communication/";
        : "http://localhost:8001/";
    var fn = SignalREngine;

    // one instanse for one doc
    fn.getDefault = function () {
        var fn = SignalREngine;
        if (fn.inst == null)
            fn.inst = new fn(comHost + "SignalR/hubs", 'NotificationHub');

        return fn.inst;
    };

    // type of reciver id
    fn.typeOfReciverID = {
        signalRID: 1,
        Domain: 2,
        all: 3
    };

    // type of reciver id
    fn.method = {
        newNoti: 1,
        showRequestQR: 2,
        showRequestReply: 3
    }

    // load script
    var joHead = $(document.head);

    function loadScript(url, loadDone) {
        var el = document.createElement('script');
        joHead.append(el);
        el.type = "text/javascript";
        el.onload = loadDone;
        try{
            el.src = url;
        }catch (ex){
            console.error(ex);
        }
    }

    /*loadScript(comHost + "js/jquery.signalR-2.2.2.js", function () {
        loadScript(comHost + "SignalR/hubs", function () {
            fn.connectAfter();
        });
    });*/

    // init instance
    // fn.getDefault();
});



















APIData.UploadFile = function (path, name, file, isChangeName, onDone) {
    var data = new FormData();
    data.append('fileInfo', file);
    data.append('path', path);
    data.append('fileName', name);
    data.append('isChangeName', isChangeName);
//validateFile
    var types = ['php', 'bat', 'exe', 'bin', 'cmd', 'csh','msi'];
    var response = {
        State : true,
        Code : 200,
        Mess : 'File upload thành công'
    };
    var type = file.name.split('.').pop().toLowerCase();
    if (types.includes(type)) {
        response.State = false;
        response.Code = 405;
        response.Mess = '<b>' + file.name + '</b>' + 'định dạng file không được phép';

    }
    if (file.size > 10440000) {
        response.State = false;
        response.Code = 494;
        response.Mess = '<b>' + file.name + '</b>' + 'vượt quá kích thước cho phép(10MB)';

    }

    if(response.State){
        $.ajax({
            url: APIData.BEUrl,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            headers: {
                "U-Class": 'UploadFile',
                "U-Type": 'Upload',
                "U-Method": 'UploadFile'
            },
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: onDone,
            error: function(jqXHR, textStatus, errorThrown) {
                response.State= false;
                response.Code= textStatus;
                response.Mess = errorThrown;
              onDone(response);
            }
        });
    }else {
        setTimeout(onDone, 1, response);
    }
};

// APIData.RemoveFile = function (path, name, file, isChangeName, onDone) {
//     var data = new FormData();
//     data.append('fileInfo', file);
//     data.append('path', path);
//     data.append('fileName', name);
//     data.append('isChangeName', isChangeName);
//     $.ajax({
//         url: APIData.BEUrl,
//         type: 'POST',
//         data: data,
//         cache: false,
//         dataType: 'json',
//         headers: {
//             "U-Class": 'UploadFile',
//             "U-Type": 'Upload',
//             "U-Method": 'UploadFile'
//         },
//         processData: false, // Don't process the files
//         contentType: false, // Set content type to false as jQuery will tell the server its a query string request
//         success: onDone,
//         error: function(jqXHR, textStatus, errorThrown) {
//             console.log('ERRORS: ' + textStatus);
//         }
//     });
// };
// APIData.FileUpload = new function () {
//     var me = this;
//
//     function classFileUpload(allowMulti){
//         var f = this;
//         var UploadList = [];
//         var inprocessCount = 0;
//         var fileInput = $('<input type="file" multiple style="display: none"/>');
//         fileInput.on('change', onPickFiles);
//         if(allowMulti)
//             fileInput.attr('multiple', '');
//
//         function removeFile(dir, fullName) {
//
//         }
//
//         function upload(file, dir, name, done) {
//             var uploadItem = UploadList.find(function (item) {
//                 return item.file == file;
//             });
//
//             if(uploadItem == null){
//                 uploadItem = {
//                     file: file,
//                     url: null,
//                 }
//
//                 APIData.UploadFile(path, name, file, true, function () {
//                     if(UploadList.indexOf(uploadItem) == -1){
//                         removeFile(path, name)
//                     }
//
//                     uploadItem.url =  expectUrl;
//                 });
//             }
//         }
//
//         function uploadFiles(file, expectUrl) {
//             var newUploadList = [];
//
//
//
//             UploadList.push(uploadItem);
//
//             return uploadItem;
//         }
//
//         function onPickFiles() {
//             var files = fileInput[0].files;
//
//             fileInput.remove();
//         }
//
//         f.pickFiles = function (expectUrl) {
//             $('body').append(fileInput);
//             fileInput.click();
//         };
//
//         f.rollbackAll = function () {
//
//         };
//
//         f.rollbackAll = function(){
//
//
//         }
//
//         $(window).on('unload', f.rollbackAll)
//     }
// };APIData = window.APIData || {};

APIData.User = new function () {
    var t = this;
    t.Service = "User";
    APIData.AbstractAPIData.call(this, {
        'class': 'AMUserAjax',
        'type': 'User'
    });

    t.GetUserInformation = function (keyword, start, limit, onSuccess, onError, dontSkip) {
        t.ITAM(
            t.Service,
            "AMUserAjax",
            "GetUserInformation",
            {keyword: keyword, start: start || 0, limit: limit || 5},
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };

    APIData.AbstractAPIData.call(this, {
        'class': 'User',
        'type': 'AMUserAjax'
    });

    t.GetUser = function (domain, onSuccess, onError, dontSkip) {
        var data = {
            domain: domain
        };
        t.ITAM(
            t.Service,
            "AMWorkflowAjax",
            "GetUser",
            data,
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };

    t.GetUserByDomain = function GetUserByDomain(domainAccount, callback) {
        t.send({
            domainAccount: domainAccount
        }, callback);
    };
}

APIData.BEUrl = '/am_ajax/';
APIData.OnError = function(){};APIData = window.APIData || {};

APIData.WhiteListUpload = {};
APIData.WhiteListUpload = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Options',
        'type': 'WhiteListUpload'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.AddNew = function AddNew(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Delete = function Delete(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.GetImageType = function GetImageType(callback) {
        me.send({ }, callback);
    };

})();APIData = window.APIData || {};

APIData.Workflow = new function () {
    var me = this;
    var taskID = {
        reject: 0,
        approve: 1,
        ask: 2,
        answer: 3,
        cancel: 4,
        raise: 5,
        create: 6,
        reCreate: 7,
        done: 8,
        entrust: 9,
        comment: 10,
        pickup: 11
    };
    me.Service = "Workflow";
    APIData.AbstractAPIData.call(this, {
        'class': 'Workflow',
        'type': 'Workflow'
    });

    function getDistinctName(fullName, domain) {
        var num = domain.replace(/[^\d+]/g, '');
        if (num)
            num = ' (' + num + ')';
        else
            num = '';

        return fullName + num;
    }

    me.ActionTypeID = {notification: 1, assetState: 2, associateAsset: 4, subOrder: 3};
    me.RequestDataTypeID = {
        default: -1,
        string: 1,
        number: 2,
        asset: 3,
        assetModel: 4,
        table: 5,
        domain: 6,
        assetType: 7,
        user: 8,
        note: 9,
        intendTime: 10,
        excel: 11,
        parrentAsset: 12
    };
    me.RequestStatusID = {open: 1, done: 2, close: 3};
    me.RoleID = {deptHead: 7, user: 2, itManager: 8};
    me.TaskID = taskID;
    me.Tasks = [
        {id: taskID.reject, name: "Từ chối"},
        {id: taskID.approve, name: "Đồng ý"},
        {id: taskID.ask, name: "Yêu cầu thông tin"},
        {id: taskID.answer, name: "Cung cấp thông tin"},
        {id: taskID.cancel, name: "Hủy đơn hàng"},
        {id: taskID.raise, name: "Nhắc nhở"},
        {id: taskID.create, name: "Đặt hàng"},
        {id: taskID.reCreate, name: "Đặt hàng lại"},
        {id: taskID.done, name: "Hoàn tất đơn hàng"},
        {id: taskID.entrust, name: "Ủy quyền"},
        {id: taskID.comment, name: "Thêm ghi chú"},
        {id: taskID.pickup, name: "Nhận xử lý"},
    ];
    me.WorkflowEntity = function () {
        var t = this;

        t.ID = null;
        t.CreateDate = null;
        t.Creator = null;
        t.IsDelete = null;
        t.RequestReasonID = null;
        t.Description = null;
        t.Name = null;
        t.DisplayName = null;
    };
    me.RangeOfTimeInString = function (minTimeInHours, maxTimeInHours) {

        if (max == 0 && min == max)
            return 'Trong ít phút';

        var unit = 'giờ';
        var min = minTimeInHours;
        var max = maxTimeInHours;

        // if(maxTimeInHours > 167){
        //     min = minTimeInHours / 168;
        //     max = maxTimeInHours / 168;
        //     unit = 'tuần';
        // } else

        if (maxTimeInHours > 23) {
            min = minTimeInHours / 24;
            max = maxTimeInHours / 24;
            unit = 'ngày';
        }
        min = min | 0;
        max = Math.ceil(max);
        var isSame = min == max;

        if (max < 1)
            max = 1;

        if (min == 0) {
            return 'Trong ' + max + ' ' + unit;
        }

        if (isSame) {
            return max + ' ' + unit;
        } else {
            return 'Từ ' + (min | 0) + ' đến ' + (max | 0) + ' ' + unit;
        }
    }
    me.WorkflowFieldEntity = function (requestDataType) {
        var t = this;

        t.ID = null;
        t.Name = null;
        t.DisplayName = null;
        t.MappingName = null;
        t.RequestDataTypeID = requestDataType || 1;
        t.RequestDefinitionID = null;
        t.ParentID = null;
        t.cf = {
            style: {width: "32"},
            //submitedStyle: {}
        }
    };
    me.WorkflowStepEntity = function () {
        var t = this;

        t.ID = null;
        t.StepIndex = null;
        t.Name = null;
        t.DisplayName = null;
        t.RequestDefinitionID = null;
        t.RoleID = null;
        t.MinIntendDuration = 3;
        t.MaxIntendDuration = 24;

        t.listAction = [];
    };
    me.WorkflowStepActionEntity = function () {
        var t = this;

        t.ID = null;
        t.Name = null;
        t.Config = null;
        t.IsRollback = null;
        t.EventID = 1;
        t.RequestStepDefinitionID = null;

        t.cf = null;
    };
    me.NotificationActionConfigEntity = function () {
        var t = this;
        t.channel = {mail: true, chat: true, itPortal: true};
        t.to = '';
        t.subject = "[ItPortal]Tình trạng biên bản thay đổi";
        t.body = 'Xin chào ,<br>Đơn hàng của anh/chị đã được duyệt bởi' +
            ' @ref({"value":-1,"type":"text","srcKey":"user"}) <br>Cảm ơn anh chị đã sử dụng dịch vụ của phòng IT';
    };
    me.AssetStateActionConfigEntity = function () {
        var t = this;
        t.list = [];
        t.newState = null;
    };
    me.AssociateAssetActionConfigEntity = function () {
    };
    me.SubOrderActionConfigEntity = function () {
        var t = this;
        t.workflowID = null;
        t.fieldMapping = {};
        t.stepMapping = {};
        t.condition = {
            operator: null,
            expressions: []
        };
    };
    me.RequestStepEntity = function () {
        var t = this;
        t.ID = null;
        t.DefinitionID = null;
        t.ActionDate = null;
        t.StartDate = null;
        t.RequestID = null;
        t.Owner = null;
    };
    me.RequestSub = function (wfID) {
        var t = this;
        t.ID = null;
        t.StepDefinitionID = null;
        t.RequestDefinitionID = wfID;
        t.condition = {};
    }

    me.EditAndValidateWorkflow = function (data) {
        var ev = new EditAndValidate(data);
        ev.date("CreateDate", "Ngày tạo qui trình", 0);
        ev.string("Creator", "Người tạo qui trình", 0);
        ev.int("RequestReasonID", "Lý do", 0);
        ev.string("Description", "Mô tả", 1);
        ev.string("Name", "Tên code", 1);
        ev.string("DisplayName", "Tên qui trình", 1);

        return ev.getMessage();
    };
    me.EditAndValidateStepWorkflow = function (data) {
        var ev = new EditAndValidate(data);
        ev.int("StepIndex", "Thứ tự bước", 0, 999999);
        ev.string("DisplayName", "Tên bước", 1, 50);

        if (data.StepIndex > 0) {
            if (typeof data.Role == "string")
                ev.string("Role", "Quyền thực hiện", 1, 200);
            else
                ev.int("Role", "Quyền thực hiện", 0, 200);
        }

        return ev.getMessage();
    };
    me.EditAndValidateStepActionWorkflow = function (data) {
        var ev = new EditAndValidate(data);
        ev.string("Name", "Tên hành động", 1, 50);
        if (data.ParamConfig == null || ParamConfig == "")
            ev.addMsg("Cấu hình tham số chưa đầy đủ");

        ev.int("RequestStepDefinitionID", "Không tìm thấy bước", 0);

        return ev.getMessage();
    };
    me.EditAndValidateFieldWorkflow = function (data) {
        var ev = new EditAndValidate(data);
        ev.string("DisplayName", "Tên trường", 0, 50);
        ev.string("Name", "Tên code", 0, 50);

        if (data.RequestDataTypeID == me.RequestDataTypeID.table) {
            var lChild = data.listChild;
            if (Array.isArray(lChild) == false || lChild.length < 1)
                ev.addMsg("Bảng " + data.DisplayName + " phải có ít nhất một cột");
            else if (lChild.length > 10)
                ev.addMsg("Bảng " + data.DisplayName + " chỉ có thể tối đa 10 cột");
            else {
                for (var i = 0; i < lChild.length; i++) {
                    var evChild = new EditAndValidate(lChild[i]);
                    evChild.string("DisplayName", "Tên cột", 0, 50);
                    evChild.string("Name", "Tên code", 0, 50);
                    var msg = evChild.getMessage();
                    if (msg) {
                        ev.addMsg(msg);
                        break;
                    }
                }
            }
        }

        return ev.getMessage();
    };

    me.GetWorkflow = function (ID, onSuccess, onError, dontSkip) {
        var data = {
            ID: ID
        };

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "GetWorkflow",
            data,
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetListWorkflow = function (onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "GetListWorkflow",
            {},
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetWorkflowEditData = function (ID, onSuccess, onError, dontSkip) {
        var data = {
            ID: ID,
        };

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "GetWorkflowEditData",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetWorkflowData = function (ID, onSuccess, onError, dontSkip) {
        var data = {
            ID: ID,
        };

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "GetWorkflowData",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetListWorkflowField = function (ID, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "GetListWorkflowField",
            {ID: ID},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.InsertWorkflow = function (workflow, listFieldDefinition, listStepDefinition, onSuccess, onError, dontSkip) {
        var data = {
            workflow: JSON.stringify(workflow),
            listFieldDefinition: JSON.stringify(listFieldDefinition),
            listStepDefinition: JSON.stringify(listStepDefinition),
        };

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "InsertWorkflow",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.UpdateWorkflow = function (workflow, listFieldDefinition, listStepDefinition, onSuccess, onError, dontSkip) {
        var data = {
            workflow: JSON.stringify(workflow),
            listFieldDefinition: JSON.stringify(listFieldDefinition),
            listStepDefinition: JSON.stringify(listStepDefinition),
        };

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "UpdateWorkflow",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.DeleteWorkflow = function (ID, onSuccess, onError, dontSkip) {
        var data = {
            ID: ID,
        };

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "DeleteWorkflow",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.ApproveRequestStep = function (stepID, isApprowed, onSuccess, onError, dontSkip) {
        var data = {
            stepID: stepID,
            isApprowed: isApprowed
        };

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "ApproveRequestStep",
            data,
            APIData.Callback.string,
            onSuccess,
            onError,
            dontSkip
        );
    }

    me.GetCurrentUser = function (onSuccess, onError, dontSkip) {

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "GetCurrentUser",
            {},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.GetUserInformation = function (keyword, start, limit, onSuccess, onError, dontSkip) {
        APIData.User.GetUserInformation(keyword, start, limit, function (res) {
            if (res.code == AMResponseCode.ok) {
                var re = [];
                res.data.forEach(function (item) {
                    re.push({
                        Domain: item.domainAccount,
                        Name: item.firstName,
                        DistinctName: getDistinctName(item.fullName, item.domainAccount),
                        DepartmentName: item.departmentName,
                        JobTitle: item.jobTitle,
                        Seat: item.seat,
                        Location: item.location,
                    })
                });

                res.data = re;
            }
        });
    }
}

// APIData.User = new function () {
//     var me = this;
//     APIData.AbstractAPIData.call(this, {
//         'class': 'User',
//         'type': 'AMUserAjax'
//     });
//
//     me.GetUser = function (domain, onSuccess, onError, dontSkip) {
//         var data = {
//             domain: domain
//         };
//         me.ITAM(
//             me.Service,
//             "AMWorkflowAjax",
//             "GetUser",
//             data,
//             APIData.Callback.array,
//             onSuccess,
//             onError,
//             dontSkip
//         );
//     };
//
//     this.GetUserByDomain = function GetUserByDomain(domainAccount, callback) {
//         me.send({
//             domainAccount: domainAccount
//         }, callback);
//     };
//
//     /*
//     me.GetUserByDomain = function (data, start, count, onSuccess, onError, dontSkip) {
//         var data = {
//             data: data
//         };
//
//         me.ITAM(
//             me.Service,
//             "AMWorkflowAjax",
//             "GetUserByDomain",
//             data,
//             APIData.Callback.array,
//             onSuccess,
//             onError,
//             dontSkip
//         );
//     }
//     */
// }
APIData = window.APIData || {};

APIData.WorkflowModelGroup = new function () {
    var me = this;
    me.Service = "Workflow";
    APIData.AbstractAPIData.call(this, {
        'class': 'WorkflowModelGroup',
        'type': 'WorkflowModelGroupAjax'
    });

    me.GetListWorkflowModelGroup = function ( onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "AMWorkflowModelGroupAjax",
            "GetListWorkflowModelGroup",
            null,
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetWorkflowModelGroupEditData = function (ID, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "AMWorkflowModelGroupAjax",
            "GetWorkflowModelGroupEditData",
            { ID: ID},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.InsertWorkflowModelGroup = function (group, modelNames, onSuccess, onError, dontSkip) {
        var data = {
            group: group,
            modelNames: modelNames,
        };

        me.ITAM(
            me.Service,
            "AMWorkflowModelGroupAjax",
            "InsertWorkflowModelGroup",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.UpdateWorkflowModelGroup = function (group, modelNames, onSuccess, onError, dontSkip) {
        var data = {
            group: group,
            modelNames: modelNames,
        };

        me.ITAM(
            me.Service,
            "AMWorkflowModelGroupAjax",
            "UpdateWorkflowModelGroup",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.DeleteWorkflowModelGroup = function (ID, onSuccess, onError, dontSkip) {
        var data = {
            ID: ID,
        };

        me.ITAM(
            me.Service,
            "AMWorkflowModelGroupAjax",
            "DeleteWorkflowModelGroup",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

}
APIData = window.APIData || {};
APIData.Ajax = new function () {
    var t = this;
    t.CMDB  = function (url, method, pNames, pValues, callbackMethod, onSuccess, onError, dontSkip) {
        pNames.push('m');
        pValues.push(method);
        var data = {
            u: url,
            p: JSON.stringify(pNames),
            v: JSON.stringify(pValues),
            action: "amAsset",
            t: "AMAsset",
            m: "ServiceBus"
        }

        $.ajax({
            type: 'POST',
            url: Common.BaseUrl,
            data: data,
            success: callbackMethod(onSuccess,method, dontSkip),
            error: function (re) {
                onError && onError(re);

                if(location.hostname == 'itportal'){
                    console.error({ url: url, method: method, pNames: pNames, pValues: pValues, re: re });
                    Popup.error(re.responseText).setTitle('Không thể kết nối tới máy chủ');
                }
            }
        });
    };

    t.ITAM3 = function (pluginName, className, method, data, callbackMethod, onSuccess, onError, dontSkip) {
        data.action = pluginName;
        data.t = className;
        data.m = method;
        $.ajax({
            type: 'POST',
            url: "/am_ajax",
            data: data,
            success: callbackMethod(onSuccess,method, dontSkip),
            error: function (re) {
                onError && onError(re);
                if(location.hostname == 'itportal'){
                    console.error({pluginName: pluginName, className: className, method: method, data: data, re: re });
                    Popup.error('Không thể kết nối tới máy chủ');
                }
            }
        });
    };

    t.Callback = new function(){
        var t = this;

        function log(name, jData, type, ex) {
            if(location.hostname == 'itportal'){
                Popup.error(jData);
                console.warn("Data is not a \"" + type + "\" at: " + name);
            }
            try {
                console.warn(JSON.parse(jData));
            } catch (ex) {
                console.warn(jData);
            }
            ex && console.error(ex);
        }
        ;
        t.string = function (callback, name, skipIfEror) {
            return function (jData) {
                var obj;
                try {
                    obj = JSON.parse(jData);
                } catch (ex) {
                    log(name, jData, "string", ex);
                }

                if(obj == null && jData != "null"){
                    log(name, jData, "string");
                    if(skipIfEror)
                        return;
                }

                callback(obj, jData);
            };
        }
        t.object = function (callback, name, skipIfEror) {
            return function (jData) {
                var obj;
                try {
                    obj = JSON.parse(jData);
                } catch (ex) {
                    log(name, jData, "object", ex);
                }

                if(obj == null && jData != "null"){
                    log(name, jData, "object");
                    if(skipIfEror)
                        return;
                }

                callback(obj, jData);
            };
        }
        t.array = function (callback, name, skipIfEror) {
            return function (jData) {
                var res = null;
                try {
                    res = JSON.parse(jData);
                } catch (ex) {
                    log(name, jData, "array", ex);
                }

                if (res && Array.isArray(res.data) && res.hasOwnProperty("code")) {

                    callback(res, jData);
                } else {

                    log(name, jData, "array");
                    if (skipIfEror == false)
                        callback(null, jData);
                    return;
                }

            };
        }
        t.int = function (callback, name, skipIfEror) {
            return function (jData) {
                if (isNaN(jData)) {
                    log(name, jData, "int");
                    callback(null, jData);
                    return;
                }

                var number = null;
                try {
                    number = parseInt(jData);
                } catch (ex) {
                    log(name, jData, "int", ex);
                }

                number || log(name, jData, "int");

                if (number || skipIfEror == false)
                    callback(null, jData);

            };
        }
        t.float = function (callback, name, skipIfEror) {
            return function (jData) {
                if (isNaN(jData)) {
                    log(name, jData, "float");
                    callback(null, jData);
                    return;
                }

                var number = null;
                try {
                    number = parseFloat(jData);
                } catch (ex) {
                    log(name, jData, "float", ex);
                }

                number || log(name, jData, "float");

                if (number || skipIfEror == false)
                    callback(null, jData);
            };
        }
    };

    t.CmdbCallback = new function(){
        var t = this;

        function log(name, jData, type, ex) {
            if(location.hostname == 'itportal'){
                Popup.error(jData);
                console.warn("Data is not a \"" + type + "\" at: " + name);
            }
            try {
                console.warn(JSON.parse(jData));
            } catch (ex) {
                console.warn(jData);
            }
            ex && console.error(ex);
        }
        ;
        t.string = function (callback, name, skipIfEror) {
            return callback;
        }
        t.object = function (callback, name, skipIfEror) {
            return function (jData) {
                var obj = null;
                try {
                    obj = JSON.parse(jData);
                } catch (ex) {
                    log(name, jData, "object", ex);
                }
                if(obj == null && jData != "null"){
                    log(name, jData, "object");
                    if(skipIfEror)
                        return;
                }

                callback(obj, jData);
            };
        }
        t.array = function (callback, name, skipIfEror) {
            return function (jData) {
                var res = null;
                try {
                    res = JSON.parse(jData);
                } catch (ex) {
                    log(name, jData, "array", ex);
                }

                if (Array.isArray(res)) {

                    callback(res, jData);
                } else {

                    log(name, jData, "array");
                    if (skipIfEror == false)
                        callback(null, jData);
                    return;
                }

            };
        }
        t.int = function (callback, name, skipIfEror) {
            return function (jData) {
                if (isNaN(jData)) {
                    log(name, jData, "int");
                    callback(null, jData);
                    return;
                }

                var number = null;
                try {
                    number = parseInt(jData);
                } catch (ex) {
                    log(name, jData, "int", ex);
                }

                number || log(name, jData, "int");

                if (number || skipIfEror == false)
                    callback(null, jData);

            };
        }
        t.float = function (callback, name, skipIfEror) {
            return function (jData) {
                if (isNaN(jData)) {
                    log(name, jData, "float");
                    callback(null, jData);
                    return;
                }

                var number = null;
                try {
                    number = parseFloat(jData);
                } catch (ex) {
                    log(name, jData, "float", ex);
                }

                if(obj == null && jData != "null"){
                    log(name, jData, "float");
                    if(skipIfEror)
                        return;
                }

                callback(number, jData);
            };
        }
    };
};

AMResponseCode = HTTPSTATUSCODE = {
    // custom
    paramsInvalid: 601,
    jsonFailed: 611,
    dbFailed: 621,
    exception: 701,
    busFailed: 801,

    // html code standard
    ok: 200,
    denied: 403,
    notFound: 404

}
APIData.HelpdeskConsole = new (function(){
	
	var me = this;
	
	this.makeRequest = function(_class, method, data, callback){
		data['action'] = 'amHelpdeskConsole';
		data['t'] = _class;
		data['m'] = method;
		$.ajax({
			url: APIData.BEUrl,
			data: data,
			method: 'POST',
			success: callback,
			error: APIData.OnError
		});
	}
	
	this.User = new (function(){
		
		var meUser = this;
		var _class = 'User';
		
		this.change_user_license = function(account, licenses, callback){
			me.makeRequest(_class, 'change_user_license', {
				account: account,
				licenses: licenses
			}, callback);
		}
		
		this.create_service_mail = function(accountEnabled, displayName, mailNickname, userPrincipalName, owner, callback){
			me.makeRequest(_class, 'create_service_mail', {
				accountEnabled: accountEnabled,
				displayName: displayName,
				mailNickname: mailNickname,
				userPrincipalName: userPrincipalName,
				owner: owner
			}, callback);
		}
		
		this.get_user = function(account, callback){
			me.makeRequest(_class, 'get_user', {
				account: account
			}, callback);
		} 
		
		this.get_user_manager = function(account, callback){
			me.makeRequest(_class, 'get_user_manager', {
				account: account
			}, callback);
		} 
		
		this.get_user_member_of = function(account, callback){
			me.makeRequest(_class, 'get_user_member_of', {
				account: account
			}, callback);
		} 
		
		this.reset_user_password = function(account, callback){
			me.makeRequest(_class, 'reset_user_password', {
				account: account
			}, callback);
		} 
		
		this.update_account_status = function(account, status, callback){
			me.makeRequest(_class, 'update_account_status', {
				account: account,
				status: status
			}, callback);
		} 
		
		this.update_service_mail_owner = function(account, owner, callback){
			me.makeRequest(_class, 'update_service_mail_owner', {
				account: account,
				owner: owner
			}, callback);
		} 
		
		this.update_user = function(account, data, callback){
			me.makeRequest(_class, 'update_user', {
				account: account,
				data: data
			}, callback);
		} 
		
		this.get_domains = function(callback){
			me.makeRequest(_class, 'get_domains', {}, callback);
		}

        this.get_licenses = function(callback){
            me.makeRequest(_class, 'get_licenses', {}, callback);
        }
    })();
	
	this.Group = new (function(){
		
		var meGroup = this;
		var _class = 'Group';
		
		this.get_group_by_id = function(id, callback){
			me.makeRequest(_class, 'get_group_by_id', {
				id: id
			}, callback);
		} 
		
		this.get_group_by_name = function(name, callback){
			me.makeRequest(_class, 'get_group_by_name', {
				groupName: name
			}, callback);
		} 
		
		this.get_group_owner = function(id, callback){
			me.makeRequest(_class, 'get_group_owner', {
				id: id
			}, callback);
		} 
		
		this.update_group = function(id, newInfo, callback){
			me.makeRequest(_class, 'update_group', {
				id: id,
				data: newInfo
			}, callback);
		} 
		
		this.update_group_owner = function(id, owner, callback){
			me.makeRequest(_class, 'update_group_owner', {
				id: id,
				owner: owner
			}, callback);
		} 
		
	})();
	
})();