Mention = new function () {
    var t = this;

    t.parse = function (val, onHitReferData) {
        if (typeof val != "string")
            return [];

        var re = [];
        var keySearch = "@ref({";
        var keyLength = keySearch.length - 1;
        var startIndex = 0;
        var startCut = 0;
        while (1) {
            var endIndex = val.indexOf(keySearch, startIndex);
            if (endIndex == -1) {
                if (val.length > startCut) {
                    var temp = val.substr(startCut, val.length - startCut);
                    re.push(temp);
                }
                break;
            }

            re.push(val.substr(startCut, endIndex - startCut));

            startIndex = endIndex + keyLength;
            endIndex = val.indexOf(")", startIndex);
            var n = endIndex - startIndex;
            if (n < 2) {
                startIndex = endIndex + keyLength;
                continue;
            }

            startCut = startIndex + n + 1;

            var referData = JSON.parse(val.substr(startIndex, n));
            re.push(onHitReferData(referData));
        }

        return re.join('');
    }

    t.update = function (srcKey, oldValue, newValue) {
        var src = mapSource[srcKey];
        listReferData.forEach(function (item) {
            if (item.srcKey == srcKey) {
                if (oldValue == null) {
                    if (item.dom != referEditing.dom) {
                        item.dom.innerText = src.getName(item.value);
                    }
                } else if (oldValue == item.value) {
                    if (item.dom != referEditing.dom)
                        item.dom.innerText = src.getName(item.value);

                    item.value = newValue;
                }
            }
        });
    }
};

ko.bindingHandlers.mention = new function () {
    var t = this;
    var slt = getSelection();
    var rf = new function () {
        var rf = this;
        rf.create = function (value, src, type, text) {
            var html = '<refer class="refer"';
            if (value == null) {
                html += '>';
            } else {
                var textValue = value ? src.getName(value, src.key) : '';
                if (textValue)
                    textValue = textValue.replace(/  /g, " &nbsp;");
                html = html +
                    ' srcKey="' + src.key +
                    '" value="' + value +
                    '" type="' + (type || 'text');

                if (text)
                    html += '" value="' + text;
                html += '">' + textValue;
            }

            html += '</refer>';
            return $(html);
        };
        rf.isReferDom = function (dom) {
            return dom.tagName == 'REFER';
        };
        rf.break = function (domRefer) {
            if (domRefer.parentElement == null)
                return;

            // original carret info
            var anchorNode = slt.anchorNode;
            var anchorOffset = slt.anchorOffset;
            var focusNode = slt.focusNode;
            var focusOffset = slt.focusOffset;
            // var isCollapsed = slt.isCollapsed;
            var range = slt.getRangeAt(0);

            // unwarp refer
            var newNode = document.createTextNode(domRefer.innerText);
            var oldNode = domRefer.children.length ? domRefer.children[0].childNodes[0] : domRefer.childNodes[0];
            domRefer.parentElement.replaceChild(newNode, domRefer);


            // update carret
            if (oldNode == anchorNode) {
                range.setStart(newNode, anchorOffset);
                if (slt.isCollapsed) {// reverse
                    range.setStart(focusNode, focusOffset);
                    range.setEnd(newNode, anchorOffset);
                }
            }

            if (oldNode == focusNode) {
                range.setEnd(newNode, focusOffset);
                if (slt.anchorNode != anchorNode) {// reverse
                    range.setStart(newNode, focusOffset);
                    range.setEnd(anchorNode, anchorOffset);
                }
            }

        };
        rf.getData = function (domRefer) {
            var re = {
                srcKey: domRefer.getAttribute('srcKey'),
                value: domRefer.getAttribute('value'),
                type: domRefer.getAttribute('type')
            };

            var textValue = domRefer.getAttribute('text');
            if (textValue)
                re.text = textValue;

            return re;
        };
        rf.getAttr = function (domRefer, name) {
            return domRefer.getAttribute(name);
        };
        rf.update = function (dom, src, data) {
            dom.innerHTML = data[src.displayName];
            dom.setAttribute('value', data[src.valueName]);
            dom.setAttribute('srcKey', data.srcKey);
            dom.setAttribute('type', data.type || 'text');

            if (data.text)
                dom.setAttribute('text', data.text);
            else
                dom.removeAttribute('text');
        };
        rf.en = function (mentionEl, nohtml) {
            var re = mentionEl.cloneNode(true);
            en(re);
            return nohtml ? re.innerText : re.innerHTML;
        }

        function en(el) {
            var childs = el.children;
            if (childs == null)
                return;


            for (var i = 0; i < childs.length;) {
                var child = childs[i];
                if (rf.isReferDom(child)) {
                    var content;
                    if(child.innerText){
                        var json = JSON.stringify(rf.getData(child));
                        content = '@ref(' + json + ')';
                    }else{
                        content = '';
                    }

                    var newNode = document.createTextNode(content);
                    el.replaceChild(newNode, child);
                } else {
                    en(child);
                    i++;
                }
            }
        }
    };
    t.init = function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var allBinding = allBindingsAccessor();
        var koObser = valueAccessor();
        var jo = $(element);
        var data = allBinding.searchMeta;
        var isKo = typeof koObser == 'function';
        var updateModel = isKo ? koObser : ko.utils.createUpdateModelHandle(valueAccessor, viewModel, bindingContext);
        var referEditing = new function () {
            var t = this;
            var dom;
            var domSearch;
            var curSourceKey;
            var searchHandle;
            var isSearchChanged;

            t.dom = dom;

            function setDom(el) {
                t.dom = dom = el;
                if (el)
                    domSearch = dom.children[0];
            }

            function onSearchSelect(item) {
                if (dom == null || item == null)
                    return;

                t.end(item);
            }

            t.end = function (selected) {
                if (selected) {
                    rf.update(dom, mapSrc.getSrc(selected.srcKey), selected);
                    var slt = getSelection();
                    var range = slt.getRangeAt(0);

                    var nextNode = dom.nextSibling;
                    var temp = document.createTextNode('\u00A0');
                    nextNode.parentElement.insertBefore(temp, nextNode);
                    nextNode = temp;
                    range.setStart(nextNode, 1);
                } else {
                    var searchData = t.getCurrentData();
                    if (dom == null)
                        dom = dom;

                    if (isSearchChanged || searchData == null) {
                        var domS = searchHandle.dom;
                        if (domS) {
                            var p = domS.parentElement;
                            if (p) {
                                p.removeChild(domS);
                            }
                        }

                        rf.break(dom);
                    } else {
                        var slt = getSelection();
                        var range = slt.getRangeAt(0);
                        dom.innerHTML = searchData.name;

                        if (dom.nextSibling == slt.anchorNode)
                            range.setStart(slt.anchorNode, 1);
                    }
                }

                setDom(null);
            }

            t.update = function (el) {
                isSearchChanged = 0;

                var range = slt.getRangeAt(0);
                var pos = range.startOffset;
                var data = rf.getData(el);
                var srcKey = mapSrc.isDefault(data.srcKey) ? "" : (data.srcKey + ":");

                el.innerHTML = '<a>@' + srcKey + el.innerText + '</a>';
                setDom(el);
                var ref = domSearch.childNodes[0];

                t.setSource(data.srcKey);
                if (searchHandle)
                    dom.insertBefore(searchHandle.dom, dom.first);
                try {
                    range.setStart(ref, pos + 1 + srcKey.length);
                } finally {
                }

                t.change();
                isSearchChanged = 0;
            }

            t.change = function () {
                isSearchChanged = 1;
                var searchData = t.getCurrentData();

                if (searchData == null)
                    t.end();

                if (searchData.srcKey != curSourceKey)
                    t.setSource(searchData.srcKey)

                if (searchHandle)
                    searchHandle.search(searchData.name);
            }

            t.setSource = function (srcKey) {
                curSourceKey = srcKey;
                var src = mapSrc.getSrc(srcKey);
                searchHandle = data.searchHandle;
                src.setSearchHandle(searchHandle, onSearchSelect);
            }

            t.keyEvent = function (e) {
                if (searchHandle) {
                    searchHandle.onkeydown(e);
                }
            }

            t.getCurrentData = function () {
                if (domSearch.parentElement == null)
                    return null;

                var val = domSearch.innerText;

                if (val.length < 1)
                    null;

                var key = val[0];
                if (key != '@')
                    return null;

                var iName = val.indexOf(":");
                if (iName == -1)
                    return {key: key, srcKey: null, name: val.substr(1)};
                else
                    return {key: key, srcKey: val.substr(1, iName - 1), name: val.substr(iName + 1)};
            }
        };
        var mapSrc = new function () {
            var m = this;
            var map = [];

            m.getSrc = function (srcKey, returnNull) {
                var src = srcKey && map[srcKey];

                if (src)
                    return src;
                else
                    return returnNull ? null : m.default;
            };
            m.isDefault = function (srcKey) {
                return srcKey == null || m.default.key == srcKey;
            };

            data.listSource.forEach(function (item) {
                map[item.key] = item;
            });
            m.default = data.defaultSource;
        };
        var carret = new function () {
            var t = this;

            t.getBackSpaceNode = function () {
                var slt = getSelection();
                var anchor = slt.anchorNode;
                if (anchor == null)
                    return null;

                var offset = slt.anchorOffset;

                if (anchor == element) {
                    if (offset < 2)
                        return null;

                    return anchor.childNodes[offset - 1];
                }

                if (offset < 2) {
                    while (true) {
                        var re = anchor.previousSibling;
                        if (re)
                            return re;

                        anchor = anchor.parentElement;
                        if (anchor == element || anchor == null)
                            return null;
                    }
                }

                return null;
            };
            t.getDelNode = function () {
                var slt = getSelection();
                var anchor = slt.anchorNode;
                if (anchor == null)
                    return null;

                var offset = slt.anchorOffset;
                if (anchor == element) {
                    return anchor;
                }

                if (anchor.length) {
                    if (offset != anchor.length)
                        return null;
                } else {
                    if (offset != anchor.childNodes.length)
                        return null;
                }
                while (true) {
                    var re = anchor.nextSibling;
                    if (re)
                        return re;

                    anchor = anchor.parentElement;
                    if (anchor == element || anchor == null)
                        return null;
                }
                return null;
            };
        };
        jo.bind("input", function (e) {
            if (slt.isCollapsed == false || slt.anchorNode == null)
                return;

            var node = slt.anchorNode;
            var dom = node.parentElement;
            if (referEditing.dom) {
                if (dom == referEditing.dom || dom.parentElement == referEditing.dom)
                    referEditing.change();
                else if (referEditing.dom)
                    referEditing.end();
            } else {
                var i = slt.anchorOffset - 1;
                var data = node.textContent;
                if (i > -1 && data.length && i < data.length) {
                    if (data[i] == '@') {
                        var range = slt.getRangeAt(0);
                        range.setStart(node, slt.anchorOffset - 1);
                        range.deleteContents();

                        var domRefer = rf.create()[0];
                        range.insertNode(domRefer);
                        range.setStart(domRefer, 0);
                        slt.collapseToStart();
                        carretChange();
                    }
                }
            }
        });

        function carretChange() {
            if (slt.isCollapsed == false)
                return;

            var dom = slt.anchorNode;
            if (dom == null)
                return;


            var domEditing = referEditing.dom;
            while (dom != element && rf.isReferDom(dom) == null) {
                dom = dom.parentElement;
                if (dom == null)
                    return;
            }

            if (domEditing == null) {
                if (rf.isReferDom(dom))
                    referEditing.update(dom);
            } else {
                if (rf.isReferDom(dom) == false)
                    dom = dom.parentElement;

                if (dom != domEditing) {
                    referEditing.end();

                    if (rf.isReferDom(dom))
                        referEditing.update(dom);
                }
            }
        }

        function onDelMulti() {
            if (slt.isCollapsed)
                return;

            var r = slt.getRangeAt(0);
            var anchorNode = slt.anchorNode;
            var focusNode = slt.focusNode;
            var parent = anchorNode.parentElement;
            if (rf.isReferDom(parent))
                rf.break(parent);

            if (focusNode == anchorNode)
                return;

            parent = focusNode.parentElement;
            if (rf.isReferDom(parent))
                rf.break(parent);
        }

        function onChange() {
            var val = isKo ? koObser() : koObser;
            jo.html("");
            var re = Mention.parse(val, function (data) {
                return rf.create(data.value, mapSrc.getSrc(data.srcKey), data.type, data.text)[0].outerHTML;
            });
            jo.append(re);
        }

        function flush() {
            referEditing.dom && referEditing.end();
            var temp = rf.en(element, allBinding.nohtml);
            updateModel(temp);
        }

        jo.bind("click", carretChange);
        jo.bind("keydown", function (e) {
            var kCode = e.keyCode;
            var container = e.target;

            if (kCode < 41) {
                if (kCode > 36) {
                    //chặn di chuyển carret lên xuống khi đang search
                    if (referEditing.dom && (kCode == 38 || kCode == 40)) {
                        referEditing.keyEvent(e);
                        e.preventDefault();
                    } else {
                        setTimeout(carretChange, 1);
                    }
                } else {
                    switch (kCode) {
                        case 8:
                            if (slt.isCollapsed) {
                                if (referEditing.dom) {
                                    if (slt.anchorOffset == 1)
                                        setTimeout(referEditing.end, 1);
                                } else {
                                    var node = carret.getBackSpaceNode();
                                    if (node && rf.isReferDom(node)) {
                                        setTimeout(referEditing.update, 1, node);
                                    }
                                }
                            } else {
                                onDelMulti();
                            }
                            break;
                        case 13:
                            // chặn xuống dòng khi đang search
                            if (referEditing.dom) {
                                referEditing.keyEvent(e);
                            } else if(allBinding.flushOnEnter){
                                flush();
                            } else {
                                var range = slt.getRangeAt(0);
                                if (slt.isCollapsed == false) {
                                    onDelMulti();
                                    range.deleteContents();
                                }

                                var ct = range.startContainer;
                                if (ct.tagName == "BR")
                                    range.setStartAfter(ct);

                                var brEl = document.createElement("br");
                                range.insertNode(brEl);
                                var afterBrEl = brEl.nextSibling;

                                if (afterBrEl == null) {
                                    var parent = brEl.parentNode;
                                    if (parent) {
                                        var l = parent.length;
                                        if (l == null) {
                                            if (parent.childNodes)
                                                l = parent.childNodes.length;
                                        }

                                        if (l)
                                            range.setStart(parent, l);
                                    }
                                } else {
                                    if (afterBrEl.data === "") {
                                        var last = afterBrEl.nextSibling;
                                        while (last && last.data === "")
                                            last = last.nextSibling;

                                        if (last == null) {
                                            afterBrEl.remove();
                                            range.insertNode(document.createElement("br"));
                                            //  range.setStartAfter(brEl);
                                        } else {
                                            // range.setStart(last, 0);
                                        }
                                    } else {
                                        //  range.setStart(afterBrEl, 0);
                                    }
                                    range.setStartAfter(brEl);
                                }
                                slt.collapseToStart();
                            }
                            e.preventDefault();
                            break;
                        case 32:
                            if (referEditing.dom && e.shiftKey == false)
                                setTimeout(referEditing.end, 1);
                            else if (slt.isCollapsed == false)
                                onDelMulti();
                            break;
                    }
                }
            } else {
                switch (kCode) {
                    case 46:
                        if (slt.isCollapsed) {
                            var node = carret.getDelNode();
                            if (node && rf.isReferDom(node))
                                rf.break(node);
                        } else {
                            onDelMulti();
                        }
                        break;
                    default:
                        if (e.ctrlKey != true)
                            onDelMulti();
                }
            }
        });
        jo.bind("blur", flush);

        isKo && koObser.subscribe(onChange);
        onChange();
    }

};

function createEventHanlde(thisObject) {
    var listCallBack = [];
    var re = function () {
        for (var i = 0; i < listCallBack.length; i++)
            listCallBack[i].apply(thisObject, arguments);
    }

    re.bind = function (fn, name) {
        if (name)
            listCallBack[name] = fn;
        else
            listCallBack.push(fn);
    }

    re.unbind = function (fn, name) {
        if (name)
            delete listCallBack[name];
        else
            Utils.removeFromArray(listCallBack, fn);
    }

    return re;
}

function ReferSource(name, icon, key, searchAPI, getName, valueName, displayName) {
    var t = this;
    t.name = name; // tên hiển thị
    t.key = key; // từ phía sau chữ @
    t.icon = icon;

    t.searchAPI = searchAPI;
    t.getName = getName;
    t.valueName = valueName || "value";
    t.displayName = displayName || "name";

    t.setSearchHandle = function (searchHandle, onSelected) {

        function callback(selected) {
            onSelected(Object.assign({srcKey: t.key}, selected));
        }

        searchHandle.rebinding(
            t.searchAPI,
            callback,
            null,
            t.displayName
        );
    };

    t.pushChid = function (childReferSource) {
        if (!t.isComplie) {
            if (t.searchAPI) {
                console.log("Can not compile");
                return;
            }
            t.isComplie = 1;
            var list = t.list = [];

            // overide method
            t.searchAPI = function (keyword, index, count, callBack) {
                var id = m.startSearch();

                t.list.forEach(function (item) {
                    item.searchAPI(keyword, null, null, function (re) {
                        var re = m.searchDone(id, re, item);
                        if (re)
                            callBack(re);
                    });
                });
            };
            t.getName = function (value, srcKey) {
                for (var i = 0; i < list.length; i++) {
                    if (list[i].key == srcKey)
                        return list[i].getName(value);
                }
            };
            t.setSearchHandle = function (searchHandle, onSelected) {
                function callBack(selected) {
                    onSelected(Object.assign({srcKey: selected.srcKey}, selected.value));
                }

                searchHandle.rebinding(
                    t.searchAPI,
                    callBack,
                    null,
                    t.displayName
                );
            };
            var m = new function () {
                var m = this;
                var id = 0;
                var listRe = [];
                var nSearchDone = 0;


                /**
                 * bắt đầu search
                 * trả về idSearch
                 * @returns {number}
                 */
                m.startSearch = function () {
                    id++;
                    nSearchDone = 0;
                    listRe = [];
                    return id;
                };

                m.searchDone = function (idSearch, re, referSource) {
                    if (id != idSearch)
                        return 0;

                    nSearchDone++;

                    var valueName = referSource.valueName;
                    var displayName = referSource.displayName;
                    var key = referSource.key;
                    for (var i = 0; i < re.length; i++) {
                        var item = re[i];
                        listRe.push({value: item, srcKey: key, name: item[displayName]});
                    }

                    return nSearchDone == list.length ? listRe : null;
                }
            };
        }

        t.list.push(childReferSource);
    }
}

/**
 *
 * workflowField: field config data
 * wfRefer: {
            list:           list của field
            isConfigMode:   render cho chế độ config
        }

 * wfHandle: {
            onDisplayNameChange: function create name
            edit:       function edit field
            doneEdit:   function done edit field
            onUpdate:   on update field
        }
 */
ko.bindingHandlers.workflowFields = new function () {

    function clear(bindingContext, jo) {
        if (jo.originHtml) {
            var list = jo[0].children;
            for (var i = 0; i < list.length; i++) {
                ko.cleanNode(list[i]);
            }
            jo.html(jo.originHtml);
        } else {
            jo.originHtml = jo.html();
        }

        var domTemplateContainer = jo.find('.field-item')[0];
        if (domTemplateContainer == null)
            domTemplateContainer = jo[0].children[0];

        if (domTemplateContainer == null)
            console.error('workflowFields binding can not find template');

        ko.applyBindingsToDescendants(bindingContext, jo[0]);
        ko.cleanNode(domTemplateContainer);

        return domTemplateContainer;
    }

    this.init = function init(element, valueAccessor, allBindings, viewModel, bindingContext) {
        var jo = $(element);
        element.__wfJo = jo;

        jo.obList = valueAccessor();
        if (!ko.isObservable(jo.obList) || !Array.isArray(jo.obList())) {
            console.error('workflowFields binding have to observableArray');
            return;
        }

        return {controlsDescendantBindings: true};
    }

    this.update = function init(element, valueAccessor, allBindings, viewModel, bindingContext) {
        var jo = element.__wfJo;
        var domTemplateContainer = clear(bindingContext, jo);
        if (domTemplateContainer == null)
            return;

        var list = valueAccessor()();
        if (list.length == 0)
            return;

        // init
        element.removeChild(domTemplateContainer);
        var anchorEL = element.children.length ? element.children[0] : null;
        var bindings = allBindings();
        var joDimArea = null;
        if (bindings.wfConfigMode) {
            try {
                var parent = $(element);
                while (parent.length) {
                    if (parent.hasClass("dim-able"))
                        break;
                    parent = parent.parent();
                }
                if (parent.length == null)
                    throw null;

                joDimArea = parent;
            } catch (ex) {
                console.error("can't detect dim area to dim processing");
            }
        }
        if (!bindings.wfEvent)
            bindings.wfEvent = {};

        // init child field binding
        var broRefers = [];
        var wfFieldBinds = [];
        list.forEach(function (fieldDefinition) {
            var dom = domTemplateContainer.cloneNode();
            var joChild = $(dom);
            var dataModel = bindings.wfDataModel || fieldDefinition;
            var childContext = bindingContext.createChildContext(dataModel);
            var childBinds = {wfField: dataModel};
            var obValue = fieldDefinition.RequestDataTypeID == APIData.Workflow.RequestDataTypeID.table
                ? ko.observableArray() : ko.observable();

            var can = null;
            if (bindings.wfRequestData) {
                var fieldCan = bindings.wfRequestData.currentFieldCan;
                if (fieldCan)
                    can = fieldCan[fieldDefinition.Name];

                if (!can)
                    console.warn('FieldCan do not be null, fix it please');
            }


            if(fieldDefinition.Name == 'den_bu_neu_co_(vnd)')
                refer = refer;

            if (!can)
                can = {};

            var refer = {
                dataModel: dataModel,
                obValue: obValue,
                field: fieldDefinition,
                fields: jo.obList,
                broRefers: broRefers,
                $context: childContext,
                joContainer: joChild,
                valueAttr: dataModel == fieldDefinition ? "value" : fieldDefinition.Name,
                can: can,
                requestData: bindings.wfRequestData,
                joDimArea: joDimArea,
            };

            var value = dataModel[refer.valueAttr];
            if (!value && value !== 0 && refer.can.update) {
                var defaultData = refer.field.constrain && refer.field.constrain.default;
                if (defaultData && defaultData.expression) {
                    value = defaultData.expression;
                    dataModel[refer.valueAttr] = value;
                }
            }

            obValue(value);
            obValue.subscribe(function (newVal) {
                dataModel[refer.valueAttr] = newVal;
            });
            childContext.refer = refer;
            childContext.obValue = obValue;
            childContext.configMode = bindings.wfConfigMode;

            if (anchorEL)
                element.insertBefore(dom, anchorEL);
            else
                element.append(dom);

            wfFieldBinds.push({
                refer: refer,
                dom: dom,
                binds: childBinds
            });
            broRefers.push(refer);
        });

        // before event
        if (bindings.wfEvent.beforeFieldBinding) {
            wfFieldBinds.forEach(function (item) {
                bindings.wfEvent.beforeFieldBinding(item.binds, item.refer);
            });
        }

        // binding
        wfFieldBinds.forEach(function (item) {

            if (!item.refer.$context.configMode) {
                var can = item.refer.can;

                if (!can.view){
                    var parent = item.dom.parentElement;
                    if(parent)
                        parent.removeChild(item.dom);
                    return;
                }
            }

            // try{
            ko.applyBindingsToNode(item.dom, item.binds, item.refer.$context);
            // }catch(ex){
            //     console.log(item);
            //     console.error(ex);
            // }
        });

        return {controlsDescendantBindings: true};
    }
}

ko.bindingHandlers.wfField = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {


        var bindings = allBindings();
        var refer = bindingContext.refer;
        // var can = refer.can;
        // if (!can.view){
        //    var parent = item.dom.parentElement;
        //    if(parent)
        //       parent.removeChild(item.dom);
        //     return;
        // }

        var controlsDescendantBindings = WorkflowField.render(bindingContext.refer, bindingContext.configMode);
        if (!controlsDescendantBindings)
            ko.applyBindingsToDescendants(bindingContext, element);

        if (!bindingContext.configMode && !refer.can.edit && refer.field.RequestDataTypeID != APIData.Workflow.RequestDataTypeID.table)
            refer.joContainer.find("input, textarea, select").attr("disabled", "true");
        return {controlsDescendantBindings: true};
    },
};

function MentionSearchMeta() {
    var t = this;

    t.listSource;
    t.defaultSource;
    t.searchHandle;
    t.onSourceChange;
    t.setSearchHandle;
}

(function () {

    //
    // private methods
    //
    /**
     * @param stylesArray - the array of string
     *          "{name}:{value};" pairs that are to be broken down
     *
     */
    function createCSSRuleObject(stylesArray) {
        var cssObj = {};
        for (_s in stylesArray) {
            var S = stylesArray[_s].split(":");
            if (S[0].trim() == "" || S[1].trim() == "") continue;
            cssObj[S[0].trim()] = S[1].trim();
        }
        return cssObj;
    }

    /**
     * @param $out - the tmp html content
     *
     */
    function interpretAppendedStylesheet($out) {
        var stylesheet = $out[0].styleSheets[0]; // first stylesheet
        for (r in stylesheet.cssRules) {
            try {
                var rule = stylesheet.cssRules[r];
                if (!isNaN(rule)) break; // make sure the rule exists
                var $destObj = $out.find(rule.selectorText);
                var obj = rule.cssText.replace(rule.selectorText, '');
                obj = obj.replace('{', '').replace('}', ''); // clean up the { and }'s
                var styles = obj.split(";"); // separate each
                $destObj.css(createCSSRuleObject(styles)); // do the inline styling
            } catch (e) {
            }
        }
    };


    function isPatternRelevant(newHTML, pattern, relevantPatterns) {
        if (newHTML.indexOf(pattern) > -1)
            relevantPatterns.push(new RegExp(pattern, "i"));
    };

    /**
     * The main method - inlinify
     *  this utilizes two text areas and a div for final output -
     *      (1) css input textarea for the css to apply
     *      (2) html content for the css to apply TO
     */
    function inlinify(input) {
        var tmpWindow = window.open("", "tmpHtml", "width=0,height=0");
        window.blur(); // re focus on main window
        var tmpDoc = tmpWindow.document; // create a window that we can use
        var $tmpDoc = jQuery(tmpDoc); // jquerify the temp window

        tmpDoc.write(input); // write the HTML out to a new window doc
        interpretAppendedStylesheet($tmpDoc); // apply styles to the document just created
        $tmpDoc.find("style").remove(); // sanitize all style tags present prior to the transformation

        var newHTML = $tmpDoc.find("html").html();
        tmpWindow.close();

        var relevantPatterns = [];
        isPatternRelevant(newHTML, "href=\"", relevantPatterns);
        isPatternRelevant(newHTML, "src=\"", relevantPatterns);
        return sanitize(newHTML, relevantPatterns);
    };

    function sanitize(html, patterns) {
        var ret = html;
        for (var i = 0; i < patterns.length; i++) {
            ret = san(ret, patterns[i])
        }
        return ret;
    };

    /**
     * This method will take HTML and a PATTERN and essentially
     * sanitize the following chars within the HTML with that
     * pattern through a filter:
     *      Currently this only applies to &amp;' -> &
     */
    function san(html, pattern) {

        var ret = "";
        var remainingString;
        var hrefIndex;
        for (var i = 0; i < html.length; i++) {
            remainingString = html.substring(i);
            hrefIndex = remainingString.search(pattern);
            if (hrefIndex === 0) {
                // actually sanitize the pattern, i.e. href="[sanitize-candidate]"
                // must be encapsulated within quotes, "
                (function () {
                    // get the start of what we will sanitize
                    var startIndex = remainingString.indexOf("\"");
                    // and the end
                    var endIndex = remainingString.indexOf("\"", startIndex + 1);
                    // get the data to sanitize
                    var newHREF = html.substring(i + startIndex + 1, i + endIndex + 1);
                    // here we actually perform the replacement
                    newHREF = newHREF.replace(/&amp;/g, '&');
                    // add the pattern + the new data + a closing quote
                    var regExpStartLen = "/".length;
                    var regExpFlagsLen = "/i".length;
                    ret += String(pattern).substring(regExpStartLen, String(pattern).length - regExpFlagsLen)
                        + newHREF;
                    i += endIndex;
                })();
                continue;
            } else {
                // if we have another href, copy everything until that href index
                if (hrefIndex > 0) {
                    ret += html.substring(i, hrefIndex);
                    i = hrefIndex - 1;
                } else {
                    // otherwise just add the remaining chars and stop trying to sanitize
                    ret += html.substring(i);
                    break;
                }
            }
        }
        return ret;

    };

    //
    // public methods
    //
    doInline = function (input) {
        return inlinify(input);
    }

})();
