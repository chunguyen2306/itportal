WorkflowField = new function () {
    var wf = this;
    var dataType = APIData.Workflow.RequestDataTypeID;
    var dimBackground = $('<div class="dim-background"/>')[0];
    var group = new function () {
        var t = this;
        var stepEnables = null;
        var stepEnables = null;

        function loadEnableFromLocalStorage() {
            try {
                return JSON.parse(localStorage.workflow_order_detail_group_enable);
            } catch (ex) {
                return {};
            }
        }

        function saveEnableToLocalStorage() {
            localStorage.workflow_order_detail_group_enable = JSON.stringify(stepEnables);
        }

        function getFieldEnable(refer) {
            var requestData = refer.requestData;
            var steps = requestData.listStepDefinition;
            var curStep = requestData.request ? steps[requestData.request.CurrentIndex] : steps[0];
            var fieldEnable = stepEnables[curStep.ID];
            if (fieldEnable === undefined)
                return stepEnables[curStep.ID] = {};
            else
                return fieldEnable;
        }

        t.saveGroup = function (refer, enable) {
            var fieldEnable = getFieldEnable(refer);
            var tableField = refer.field;
            fieldEnable[tableField.ID] = enable ? true : false;
            saveEnableToLocalStorage();
        }

        t.getGroup = function (refer) {
            if (!stepEnables)
                stepEnables = loadEnableFromLocalStorage();

            var fieldEnable = getFieldEnable(refer);
            var tableField = refer.field;
            var re = {enable: fieldEnable[tableField.ID]};
            if (re.enable === undefined) {
                var steps = refer.requestData.listStepDefinition;
                re.enable = true;
                for (var i = 1; i < steps.length; i++) {
                    var step = steps[i];
                    if (
                        step.Role
                        && step.Role.length < 1 // not depthead
                        && step.Role == APIData.Workflow.RoleID.itManager // not itManager
                    ) {
                        fieldEnable[tableField.ID] = false;
                        break;
                    }
                }

                fieldEnable[tableField.ID] = re.enable;
                saveEnableToLocalStorage();
            }
            if (refer.requestData.request == null)
                re.enable = false;

            var reColumns = [];
            tableField.listChild.forEach(function (col) {
                if (col.RequestDataTypeID == dataType.asset) {
                    reColumns.push({
                        DisplayName: "Số lượng",
                        Name: "__count",
                        ParentID: refer.field.ID,
                        RequestDataTypeID: dataType.number,
                        cf: {style: {width: '100px'}}
                    });
                } else {
                    reColumns.push(col.Name);
                }
            });
            re.columns = reColumns;
            return re;
        };
    };
    var validCode = {
        error: 3001,
        warn: 2001,
    };

    var dependentType = {
        html: 1,
        dataType: 2
    };
    var constrain = new function () {
        var me = this;

        function optimizeSearchStringValue(str) {
            if (typeof str == 'string')
                return str.trim().replace(/\s+/g, ' ');
            else
                return str;
        }

        function getConstrainBroForSearchConstrain(refer) {
            var re = [];
            var fieldCT = refer.field.constrain;
            if (!fieldCT)
                return re;

            for (var name in fieldCT) {
                var ctDoc = constrainDoc.getDocByName(name);
                if (ctDoc == null)
                    continue; // dev_upgrade chuẩn hóa lại data lúc lưu

                var relType = ctDoc.RelationshipType;
                if (relType != '1n' && relType != 'n1' && relType != 'nn')
                    continue;

                var ct = fieldCT[name];
                var broRefer = refer.broRefers.find(function (item) {
                    return item.field.Name == ct.expression;
                });

                if (!broRefer)
                    throw "Missing field '" + ct.expression + "' for field constrain config";

                re.push({
                    constrainName: name,
                    relationshipType: relType,
                    refer: broRefer
                });
            }

            return re;
        }

        function buildFieldValueSearchData(keyword, refer, ctBros) {
            var ctSearchData = [];
            ctBros.forEach(function (ctBro) {
                var val = ctBro.refer.obValue();
                if (val || val === 0) {
                    if (ctBro.refer.field.RequestDataTypeID == dataType.user) {
                        if (val.Domain)
                            val = val.Domain;
                    }

                    val = optimizeSearchStringValue(val);
                    ctSearchData.push({
                        SearchDataTypeID: ctBro.refer.field.RequestDataTypeID,
                        SearchValue: val,
                        ConstrainName: ctBro.constrainName
                    });
                }
            });

            keyword = optimizeSearchStringValue(keyword);
            var re = {
                SearchDataTypeID: refer.field.RequestDataTypeID,
                SearchValue: keyword,
                Constrains: ctSearchData
            };
            return re;
        }

        function setupObservForSeachAble(refer, ctBros) {
            var id = 0;

            var fnCheckAction = null;
            if (ctBros.length && ctBros[0].relationshipType == '1n') {
                fnCheckAction = function (checkID, val) {
                    var searchValue = buildFieldValueSearchData(null, refer, ctBros);
                    if (searchValue.Constrains == null || searchValue.Constrains.length == 0) {
                        refer.obValue(null);
                        return;
                    }

                    APIData.RequestFieldData.Search(searchValue, function (res) {
                        if (id != checkID)
                            return;

                        if (res.code == AMResponseCode.ok) {
                            if (res.data && res.data.length == 1)
                                refer.obValue(res.data[0]);
                            else
                                refer.obValue(null);
                        }
                    });
                };
            } else {

                // var isInChecking = false;
                fnCheckAction = function (checkID, val) {
                    // isInChecking = true;
                    APIData.RequestFieldData.Check(
                        buildFieldValueSearchData(val, refer, ctBros),
                        function (res) {
                            if (id != checkID)
                                return;

                            if (res.code == AMResponseCode.ok) {
                                if (res.data && res.data.length) {
                                    //refer.valid.removeMess(validCode.error)
                                } else {
                                    //refer.valid.addMess(validCode.error, 'Dữ liệu không khớp');
                                    refer.obValue(null);
                                }
                            }

                            // isInChecking = false;
                        }
                    );
                };
                // refer.obValue.subscribe(function (val) {
                //     if(isInChecking)
                //         return;
//
                //     checkID++;
                //     fnCheckAction(checkID, val);
                // });
            }
            ctBros.forEach(function (ctBro) {
                ctBro.refer.obValue.subscribe(function (newVal) {
                    if(newVal && typeof newVal == 'object')
                        return;

                    id++;
                    fnCheckAction(id, refer.obValue());
                });
            });
        }

        function buildSearchAPI(refer, ctBros) {
            return function (keyword, index, count, onSuccess) {
                var searchData = buildFieldValueSearchData(keyword, refer, ctBros);

                APIData.RequestFieldData.Search(searchData, onSuccess);
            }
        }

        me.setUpForSearchAble = function (refer) {
            var ctBros = getConstrainBroForSearchConstrain(refer);
            if (ctBros.length) {
                refer.valid = new GuiValidate(refer);

                setupObservForSeachAble(refer, ctBros);
            }

            var re = {
                searchAPI: buildSearchAPI(refer, ctBros)
            };

            return re;
        };

    };
    var constrainDoc = new function () {
        var me = this;

        var constrainDocs = [
            // {
            //     Name: 'depthead-emp',
            //     DisplayName: 'Trưởng phòng',
            //     dependentType: [dataType.user],
            //     constrainType: '1n'
            // },
            {
                Name: 'default',
                DisplayName: 'Mặc định',
                DependentData: ['<input/>'],
                DependentType: dependentType.html,
                RelationshipName: null,
                RelationshipType: null
            },
            {
                Name: 'count',
                DisplayName: 'Đếm',
                DependentData: [
                    '<input type="checkbox" style="width: 20px; height: auto;"/>',
                    '<input type="number" name="min" placeholder="min" style="width: 41%; margin-left: 5%; margin-right: 5%;"/>',
                    '<input type="number" name="max" placeholder="max" style="width: 41%;"/>'
                ],
                DependentType: dependentType.html,
                RelationshipName: null,
                RelationshipType: null
            },
            {
                Name: 'sum',
                DisplayName: 'Tổng',
                DependentData: [dataType.number],
                DependentType: dependentType.dataType,
                RelationshipName: null,
                RelationshipType: null
            },
            {
                Name: 'reporting-emp',
                DisplayName: 'Cấp trên trực tiếp của',
                DependentData: [dataType.user],
                DependentType: dependentType.dataType,
                RelationshipName: null,
                RelationshipType: '1n'
            },
            {
                Name: 'asset-model',
                DisplayName: 'Asset của Model',
                DependentData: [dataType.assetModel],
                DependentType: dependentType.dataType,
                RelationshipName: null,
                RelationshipType: 'n1'
            },
            {
                Name: 'model-type',
                DisplayName: 'Model của Type',
                DependentData: [dataType.assetType],
                DependentType: dependentType.dataType,
                RelationshipName: null,
                RelationshipType: 'n1'
            },
            {
                Name: 'type-model',
                DisplayName: 'Type của Model',
                DependentData: [dataType.assetModel],
                DependentType: dependentType.dataType,
                RelationshipName: null,
                RelationshipType: '1n'
            },
            {
                Name: 'model-asset',
                DisplayName: 'Model của Asset',
                DependentData: [dataType.asset],
                DependentType: dependentType.dataType,
                RelationshipName: null,
                RelationshipType: '1n'
            },
            {
                Name: 'range',
                DisplayName: 'Giới hạn số',
                DependentData: [
                    '<input type="checkbox" style="width: 20px; height: auto;"/>',
                    '<input type="number" name="min" placeholder="min" style="width: 41%; margin-left: 5%; margin-right: 5%;"/>',
                    '<input type="number" name="max" placeholder="max" style="width: 41%;"/>'
                ],
                DependentType: dependentType.html,
                RelationshipName: null,
                RelationshipType: null
            },
        ];

        var mapByName = (function () {
            var re = {};
            constrainDocs.forEach(function (item) {
                re[item.Name] = item;
            });

            return re;
        })();

        me.getDocByName = function (name) {
            return mapByName[name];
        };
        me.getDocByDataTypeID = function (dataTypeID) {
            var re = [];
            switch (dataTypeID) {
                case dataType.excel:
                    re.push([mapByName['count'], mapByName['sum']]);
                    re.push([mapByName['default']]);
                    break;

                case dataType.user:
                    re.push([mapByName['reporting-emp']]);
                    break;

                case dataType.asset:
                    re.push([
                        mapByName['asset-model'],
                    ]);
                    break;

                case dataType.assetModel:
                    re.push([
                        mapByName['model-type'],
                        mapByName['model-asset'],
                    ]);
                    break;

                case dataType.assetType:
                    re.push([
                        mapByName['type-model'],
                    ]);
                    break;

                case dataType.number:
                    re.push([mapByName['default']]);
                    break;

                case dataType.string:
                    re.push([mapByName['default']]);
                    break;
            }
            return re;
        };

        // user
        // map[dataType.user]= {};
        // map[dataType.user][dataType.user]= {};
        //
        // map[dataType.user] = [
        //     [
        //         {
        //             lable: 'Trưởng phòng',
        //             dataType: [dataType.user],
        //             type: '1'
        //         },
        //         {
        //             lable: 'Cấp trên trực tiếp',
        //             dataType: [dataType.user],
        //             type: '1'
        //         },
        //     ],
        // ];
        // map[dataType.asset] = [
        //     [
        //         {
        //             lable: 'Model',
        //             dataType: [dataType.assetModel],
        //             type: 'n1'
        //         },
        //         {
        //             lable: 'Type',
        //             dataType: [dataType.assetType],
        //             type: 'n1'
        //         },
        //     ],
        //     // [
        //     //     {
        //     //         lable: 'Trạng thái',
        //     //         dataType: [dataType.assetType],
        //     //         type: '1'
        //     //     },
        //     //     {
        //     //         lable: 'Sở hữu',
        //     //         dataType: [dataType.user],
        //     //         type: '1'
        //     //     },
        //     // ]
        // ];
        // map[dataType.assetModel] = [
        //     [
        //         {
        //             lable: 'Asset',
        //             dataType: [dataType.asset],
        //             type: '1n'
        //         },
        //         {
        //             lable: 'Type',
        //             dataType: [dataType.assetType],
        //             type: 'n1'
        //         },
        //     ],
        // ];
        // map[dataType.asset] = [
        //     [
        //         {
        //             Lable: 'field',
        //             allowType: [dataType.assetModel],
        //         },
        //         {
        //             Lable: 'Type',
        //             allowType: [dataType.assetType],
        //         },
        //     ],
        //     [
        //         {
        //             Lable: 'Trạng thái',
        //             allowType: [dataType.assetType]
        //         },
        //         {
        //             Lable: 'Sở hữu',
        //             allowType: [dataType.user]
        //         },
        //     ]
        // ];
    }
    var component = new function () {
        var cp = this;

        function setBind(jo, bind) {
            var list = [];
            for (var name in bind) {
                var value = bind[name];
                if (typeof value == 'object') {
                    var childContent = setBind(null, value);
                    if (childContent)
                        list.push(name + ": {" + childContent + "}");
                } else {
                    list.push(name + ": " + value);
                }
            }

            if (list.length == 0)
                return "";
            else {
                var bindContent = list.join(",");
                if (jo)
                    jo.attr("data-bind", bindContent);

                return bindContent;
            }
        }

        function renderSearch(refer, configMode, tagertObser, searchConfig, fnCustomSearchAPI) {
            var jo = $('<div class="search-box col-sm-12"/>');
            var joInput = $('<input class="search-component form-control">');
            jo.append(joInput);
            jo.append('<button class="search-component" style="display: none"/>');
            jo.append('<ul class="search-component" />');

            if (configMode == null) {

                // search config
                if (searchConfig == null)
                    searchConfig = {};
                searchConfig.selectByBlur = true;

                // constrain
                var re = constrain.setUpForSearchAble(refer);
                var searchAPI = fnCustomSearchAPI ? fnCustomSearchAPI(re.searchAPI) : re.searchAPI;

                ko.applyBindingsToNode(jo[0], {
                    search: tagertObser || refer.obValue,
                    searchAPI: searchAPI,
                    searchConfig: searchConfig
                })
            } else {
                joInput.attr("disabled", "true");
            }

            refer.joContainer.append(jo);
            return jo;
        }

        function renderUserTypeInput(refer, configMode, customBind) {
            var bind = {attr: {}};
            if (configMode)
                bind.attr.disabled = "true";

            var row = refer.field.cf.row;
            var jo;
            if (row && row > 1) {
                if (configMode || refer.can.edit) {
                    jo = $('<textarea class="form-control"/>');
                }
                else {
                    var val = refer.obValue();
                    if (!val && val !== 0)
                        val = '';

                    jo = $('<div/>');
                    val = escXss(val).replace(/\n/g, '<br>');
                    jo.html(val);
                }
            }
            else {
                jo = $('<input type="text" class="search-component form-control">');
            }

            bind.attr.rows = row;
            if (configMode == null) {
                if (customBind)
                    Object.assign(bind, customBind);
                else
                    bind.value = refer.obValue;
            }

            ko.applyBindingsToNode(jo[0], bind, refer.$context);
            refer.joContainer.append(jo);

            if(refer.joContainer[0].tagName == 'TD'){
                addTitle(refer);
            }

            return jo;
        }

        function addTitle(refer) {
            var func = function (val) {
                refer.joContainer.attr('title', val);
            }

            func(refer.obValue());
            refer.obValue.subscribe(func);
        }

        cp[dataType.string] = renderUserTypeInput;
        cp[dataType.table] = function (refer, configMode) {
            var field = refer.field;
            var joTableHeader = $('<table class="datatable no-control"/>');
            var joTableBody = $('<table class="datatable no-control wftable"/>');
            var groupConf = null;
            var obList = ko.observableArray();
            var oneColdeEditable = false;
            var editData = null;

            if (configMode) {
                field.cf.row || (field.cf.row = 4);

                var isEmpty = Array.isArray(field.listChild) == false;
                var l;
                if (isEmpty)
                    l = field.listChild = [];
                else
                    l = field.listChild;
                l.insert = function (name, index) {
                    var col = new APIData.Workflow.WorkflowFieldEntity();
                    col.ParentID = field.ID || -1;
                    col.RequestDataTypeID = dataType.string;
                    col.cf.isCol = 1;
                    col.cf.style.width = null;
                    if (index)
                        l[index] = col;
                    else
                        l.push(col);

                    return col;
                };

                if (isEmpty) {
                    l.insert();
                    l.insert();
                }
            } else {
                editData = refer.obValue();
                if (Array.isArray(editData) == false) {
                    editData = [];
                    refer.obValue(editData);
                }

                refer.field.getEditData = function () {
                    return editData;
                };
            }

            // excel
            (function () {
                if (configMode)
                    return;

                var excelColFields = [];
                var noneExcelViewCols = [];

                var excelCountColField = null;
                var countValueAttr = '_count';

                var colFields = refer.field.listChild;
                colFields.forEach(function (colField) {
                    var can = refer.requestData.currentFieldCan[colField.Name];
                    if (!can.view)
                        return;

                    if (colField.RequestDataTypeID == dataType.excel) {
                        excelColFields.push(colField);

                        if (colField.constrain.count) {
                            if (excelCountColField)
                                console.warn('Multi excel count columns, this cause conflic for use view. Please fix it');

                            excelCountColField = colField;
                            countValueAttr = excelCountColField.Name;
                        }
                    }
                    else {
                        noneExcelViewCols.push(colField);
                    }
                });

                if (!excelColFields.length)
                    return;

                // define data
                var rawData = editData;
                var groupData = [];
                editData = groupData;

                // process data
                var fnIsSameRow = function (row1, row2) {
                    for (var i = 0; i < noneExcelViewCols.length; i++) {
                        var colName = noneExcelViewCols[i].Name;
                        if (row1[colName] != row2[colName])
                            return false;
                    }

                    return true;
                };
                var fnCreateGroupRow = function (rawRow) {
                    // var countVal = re[countValueAttr];
                    // if(excelCountColField ){
                    //     var constrain = excelCountColField && excelCountColField.constrain;
                    //     var ctDefault = constrain && constrain.default;
                    //     if(ctDefault)
                    //         countVal = ctDefault.expression;
                    // }
                    //
                    // if(!countVal)
                    //     countVal = 1;

                    var re = {};
                    re[countValueAttr] = 1;
                    for (var i = 0; i < noneExcelViewCols.length; i++) {
                        var colName = noneExcelViewCols[i].Name;
                        re[colName] = rawRow[colName];
                    }
                    return re;
                };
                var fnSetRawData = function (rawTableData) {
                    rawData = rawTableData;
                    rawData.forEach(function (rawRow) {
                        var groupRow = groupData.find(function (groupRow) {
                            return fnIsSameRow(rawRow, groupRow);
                        });

                        if (groupRow == null) {
                            groupRow = fnCreateGroupRow(rawRow);
                            groupData.push(groupRow);
                        } else {
                            groupRow[countValueAttr] += 1;
                        }
                    });
                };
                var fnGetRawData = function () {
                    rawData.length = 0;
                    groupData.forEach(function (groupRow) {
                        var count = groupRow[countValueAttr];
                        while (count > 0) {
                            count--;
                            var rawRow = {};
                            for (var i = 0; i < noneExcelViewCols.length; i++) {
                                var colName = noneExcelViewCols[i].Name;
                                rawRow[colName] = groupRow[colName];
                            }

                            rawData.push(rawRow);
                        }
                    });
                    return rawData;
                };

                fnSetRawData(rawData);

                // defini seter, geter for data model
                Object.defineProperty(refer.dataModel, refer.valueAttr, {
                    get: fnGetRawData,
                    set: function (rawTableData) {
                        throw 'Not yet suport set raw data for table with execel column in active';
                    }
                });
            })();

            // header
            var joThead = $("<thead />");
            {
                joTableHeader.append(joThead);
                var joHeadTR = $('<tr><th></th></tr>');
                joThead.append(joHeadTR);
                obList(field.listChild);

                refer.joContainer.append(joTableHeader);
                ko.applyBindingsToNode(
                    joHeadTR[0],
                    {
                        workflowFields: obList,
                        wfConfigMode: configMode,
                        wfRequestData: refer.requestData,
                        wfEvent: {
                            beforeFieldBinding: function (childBinds, childRefer) {
                                childRefer.dontRenderBody = true;
                                if (!configMode && childRefer.can.edit)
                                    oneColdeEditable = true;

                                var css = childRefer.field.cf.style;
                                if (css)
                                    childRefer.joContainer.css(css);
                            }
                        }
                    },
                    refer.$context
                );
            }

            // body value
            var joTbody = null;
            {
                var bodyInheritAttr = "_bodyInherit";
                //create value body
                if (configMode) {
                    joTbody = $('<tbody/>');

                    var joTR = $("<tr>");
                    var lChild = field.listChild;
                    lChild.forEach(function (colField) {
                        if (configMode) {
                            var cRefer = $.extend({}, refer, {list: lChild, field: colField, isCell: 1});
                            var cHandle = $.extend({}, configMode);
                            var joTH = cRefer.joContainer = $("<td>");
                            cp[colField.RequestDataTypeID](cRefer, cHandle);
                            joTR.append(joTH);
                        }
                    });

                    var n = field.cf.row;
                    for (var i = 0; i < n; i++)
                        joTbody.append(joTR.clone());

                    joTableBody.append(joTbody);
                    ko.applyBindingsToDescendants(refer.$context, joTbody[0]);
                } else {
                    var valueAttr = refer.valueAttr;
                    joTbody = $('<tbody/>');
                    // if (groupConf && groupConf.enable) {
                    //     valueAttr = '__groupValue';
                    //     var colums = groupConf.columns;
                    //     var groupRows = [];
                    //     var orgValues = field[refer.valueAttr] || refer.$context[refer.valueAttr];
                    //     orgValues && orgValues.forEach(function (row) {
                    //         var existRow = groupRows.find(function (item) {
                    //             for (var i = 0; i < colums.length; i++) {
                    //                 var colName = colums[i];
                    //                 if (typeof colName == 'object')
                    //                     continue;
                    //
                    //                 if (item[colName] != row[colName])
                    //                     return false;
                    //             }
                    //             return true;
                    //         });
                    //         if (existRow)
                    //             existRow.__count++
                    //         else
                    //             groupRows.push(Object.assign({__count: 1}, row));
                    //     });
                    //     refer.$context[valueAttr] = groupRows;
                    // }

                    //data
                    var dataModel = refer.fieldData ? refer.fieldData : refer.field;
                    var rows = dataModel[refer.valueAttr];
                    if (rows == null) {
                        rows = dataModel[refer.valueAttr];
                    }
                    var obRows = ko.observableArray();
                    var rowBindTemplate = {
                        wfFieldCan: refer.requestData ? refer.requestData.currentFieldCan : null,
                        wfRequestData: refer.requestData || {}
                    };

                    var renderRow = function (rowDataModel, beforeFieldBinding) {
                        var domRow = $("<tr><td class=\"field-item\"></td></tr>")[0];
                        var rowContext = refer.$context.createChildContext(rowDataModel);
                        var rowBinds = {
                            workflowFields: obList,
                            wfDataModel: rowDataModel,
                            wfRequestData: refer.requestData,
                            wfEvent: {
                                beforeFieldBinding: function (childBinds, childRefer) {
                                    childRefer.dontRenderHeader = true;
                                    if (beforeFieldBinding)
                                        beforeFieldBinding(childBinds, childRefer.$context);
                                }
                            }
                        };
                        Object.assign(rowBinds, rowBindTemplate);
                        ko.applyBindingsToNode(domRow, rowBinds, rowContext);

                        if (refer.can.edit) {
                            var joLastCol = $(domRow.children[domRow.children.length - 1]);
                            var jobtnRemove = $('<i class="remove fa fa-times"></i>');
                            jobtnRemove.click(function () {
                                var itemIndex = editData.indexOf(rowDataModel);
                                if (itemIndex == -1)
                                    return;

                                editData.splice(itemIndex, 1);
                                $(domRow).remove();
                            });
                            joLastCol.append(jobtnRemove);
                        }

                        return domRow;
                    };
                    var referBody = function (data) {
                        rows = data;
                        rows.forEach(function (rowDataModel) {
                            var domRow = renderRow(rowDataModel);
                            joTbody.append(domRow);
                        });

                        if (refer.can.edit && oneColdeEditable) {
                            var newRowDataModel = null;
                            //var subHandlers = null;
                            var joInputs = null;
                            var fnAddNewRow = function () {
                                if (newRowDataModel) {
                                    //while (subHandlers.length) {
                                    //    var subHandler = subHandlers.pop();
                                    //    subHandler.dispose();
                                    //}
                                    joInputs.off('input', fnAddNewRow);

                                    rows.push(newRowDataModel);
                                } else {
                                    //subHandlers = [];
                                }

                                newRowDataModel = {};
                                var domRow = renderRow(newRowDataModel, function (childBinds, childContext) {
                                    // var handler = childContext.obValue.subscribe(fnAddNewRow);
                                    // subHandlers.push(handler);
                                });
                                joInputs = $(domRow).find('input, textarea, select');
                                joInputs.on('input', fnAddNewRow);
                                joTbody.append(domRow);
                            };
                            fnAddNewRow();
                        }
                    }
                    obRows.subscribe(referBody);

                    referBody(editData);
                    joTableBody.append(joTbody);
                }
            }

            var joDivForScroll = $('<div class="form-control table-scroll-body"/>');
            joDivForScroll.append(joTableBody);
            refer.joContainer.append(joDivForScroll);

            if (configMode) {
                joDivForScroll.height("auto");
                refer.field.cf.height = (joDivForScroll.height() | 0) + 1;
            } else {
                //joDivForScroll.css("max-height", refer.field.cf.height);
            }

            // col width
            var fnUpdateWidth = function () {
                var joTHs = joTableHeader.find('th');
                var joFirstTDs = joTableBody.find('tr:first-child>td');
                for (var i = 0; i < joTHs.length; i++) {
                    var domth = joTHs[i];
                    var domtd = joFirstTDs[i];
                    domth.style.width = $(domtd).width() + 'px';
                }
            };
            fnUpdateWidth();
            $(window).on('resize', fnUpdateWidth);
            return true;
        };
        cp[dataType.number] = function (refer, configMode) {

            var constrain = refer.field.constrain;
            var sum = constrain && constrain.range;
            var joInput = renderUserTypeInput(refer, configMode, {
                numeric: refer.obValue,
                numericSeparate: ',',
                numericAllowDot: true
            });

            joInput.css('text-align', 'right');
            joInput.css('padding-right', '5px');
        };
        cp[dataType.asset] = function (refer, configMode) {
            renderSearch(refer, configMode, null, {
                displayName: "AssetName",
                valueName: "AssetName"
            });
            addTitle(refer);
        };
        cp[dataType.assetModel] = function (refer, configMode) {
            renderSearch(refer, configMode, null, {
                displayName: "AssetModelName",
                valueName: "AssetModelName"
            });
            addTitle(refer);
        };
        cp[dataType.assetType] = function (refer, configMode) {
            renderSearch(refer, configMode, null, {
                displayName: "AssetTypeName",
                valueName: "AssetTypeName"
            });
            addTitle(refer);
        };
        cp[dataType.domain] = function (refer, configMode) {
            renderSearch(
                refer,
                configMode,
                "APIData.CMDB.GetListUser",
                {
                    valueName: "'AccountName'",
                    displayName: "'AccountName'"
                }
            );
        };
        cp[dataType.user] = function (refer, configMode) {
            function buildName(fullName, domain, title) {
                var num = domain && domain.replace(/[^\d+]/g, '');

                num = num ? ' (' + num + ')' : '';
                title = title ? (' - ' + title) : '';
                return fullName + num + title;
            }

            function transformData(rawData) {
                return {
                    Domain: rawData.Domain,
                    Name: (rawData.FullName + ' - ' + rawData.JobTitle),
                    Department: rawData.Department || 'chưa xác định',
                    JobTitle: rawData.JobTitle,
                    Seat: rawData.seat,
                    Site: rawData.Site ? rawData.Site : 'chưa xác định',
                    rawData: rawData
                };
            }

            var val = refer.field.value;
            var joContainer = refer.joContainer;
            if (configMode) {
                val = {
                    Name: "Tên. Họ Và Tên Đệm",
                    Departmen: "Tên Phòng Ban",
                    JobTitle: "Title",
                    Seat: "Số ghế",
                    Site: "Tên văn phòng"
                };
            }

            if (val)
                val = transformData(val);

            var koSelect = ko.observable();
            refer.$context.koSelect = koSelect;
            koSelect.subscribe(function (userInfo) {
                koSelect.__isUpdating = true;
                if (!userInfo) {
                    koSelect({
                        Department: 'chưa xác định',
                        Seat: '',
                        Site: 'chưa xác định',
                        rawData: null
                    });

                    return;
                }

                if (userInfo.rawData)
                    refer.obValue(userInfo);
                else
                    refer.obValue(null);

                koSelect.__isUpdating = false;

            });
            refer.obValue.subscribe(function (userInfo) {
                if (koSelect.__isUpdating == false) {
                    if (userInfo)
                        koSelect(userInfo.rawData ? userInfo : transformData(userInfo));
                    else
                        koSelect(null);
                }
            });

            koSelect(val);
            if (configMode || refer.can.edit) {
                var customSearchAPI = function (searchAPI) {
                    return function (keyword, index, count, onDone) {
                        searchAPI(keyword, index, count, function (res) {
                            if (res.code != AMResponseCode.ok)
                                return res;

                            var re = [];
                            res.data.forEach(function (item) {
                                re.push(transformData(item));
                            });

                            res.data = re;
                            onDone(res);
                        });
                    };
                };
                renderSearch(
                    refer,
                    configMode,
                    koSelect,
                    {
                        displayName: "Name",
                        selectByBlur: true
                    },
                    customSearchAPI
                );
            } else {
                constrain.setUpForSearchAble(refer);
                var joName = $("<div><span data-bind=\"text: koSelect().Name\"></span></div>");
                ko.applyBindingsToDescendants(refer.$context, joName[0]);
                joContainer.append(joName);
            }

            var joDep = $("<div>Phòng ban: <span data-bind='text: koSelect().Department'></span></div>");
            var joAddress = $("<div>Địa chỉ: <span data-bind='text: koSelect().Site'></span></div>");
            ko.applyBindingsToDescendants(refer.$context, joDep[0]);
            ko.applyBindingsToDescendants(refer.$context, joAddress[0]);
            joContainer.append(joDep);
            joContainer.append(joAddress);
            return true;
        };
        cp[dataType.intendTime] = function (refer, configMode) {
            var val = refer.field.value;
            if (!val)
                return;

            var joContainer = refer.joContainer;
            var requestStatus = refer.requestData.request && refer.requestData.request.RequestStatusID;
            val = ' ' + refer.field.value;

            if (requestStatus == APIData.Request.RequestStatusID.cancel) {
                joContainer.append('<div>Yêu cầu đã bị hủy <span style="white-space: nowrap">vào ' + val + "</span></div>");
            } else if (requestStatus == APIData.Request.RequestStatusID.done) {
                joContainer.append('<div>Yêu cầu đã hoàn tất <span style="white-space: nowrap">vào ' + val + "</span></div>");
            } else {
                joContainer.append("<div>Yêu cầu dự kiến sẽ hoàn thành <span style=\"white-space: nowrap\">trước " + val + "</span></div>");
                joContainer.append('<div class="note">Nếu trong kho không có sẵn, chờ mua hàng sẽ hơi lâu</div>');
            }
            return joContainer;
        };
        cp[dataType.excel] = function (refer, configMode) {
            if (configMode) {
                renderUserTypeInput(refer, configMode);
                return;
            }

            var constrain = refer.field.constrain;
            var sum = constrain && constrain.sum;
            var joInput = renderUserTypeInput(refer, configMode, {
                numeric: refer.obValue,
                numericSeparate: ',',
                numericAllowDot: true,
                numericAllowDot: true,
                numericMin: sum && sum.min,
                numericMax: sum && sum.max,
            });
            joInput.css('text-align', 'right');
            joInput.css('padding-right', '5px');
        };
        cp[dataType.parrentAsset] = function (refer, configMode) {

            var tableRenderContext = refer.$context.$parentContext.$parentContext;
            if (!tableRenderContext) {
                console.warn('can not find table');
                return;
            }

            var fn = cp[dataType.parrentAsset];
            var fnContext = fn.contexts || (fn.contexts = {});
            var tableName = tableRenderContext.$data.Name;
            var context = fnContext[tableName] || (fnContext[tableName] = {renders: []});
            var searchModel = context.searchModel;

            // search model
            if (!searchModel) {
                searchModel = new SearchModel();
                context.searchModel = searchModel;

                var assetField = tableRenderContext.$data.listChild.find(function (field) {
                    return field.RequestDataTypeID == dataType.asset;
                });
                if (!assetField) {
                    console.warn('can not find asset field');
                    return;
                }
                context.assetField = assetField;

                var fnRenderAll = function (assetParrentsInfo) {
                    var searchDataSrc = [];
                    for (var asssetName in assetParrentsInfo) {
                        var parent = assetParrentsInfo[asssetName];
                        if(parent === false){
                            searchDataSrc.push({
                                AssetName: asssetName
                            });
                        }
                    }
                    var searchMember = [{IsUnicode: 0}];
                    searchModel.setDataSource(searchDataSrc, searchMember);

                    for (var asssetName in assetParrentsInfo) {
                        var parent = assetParrentsInfo[asssetName];
                        var func = context.renders[asssetName];
                        func(parent);
                    }
                };

                var fnLoadParentInfo = function (items) {
                    var assetNames = [];
                    items.forEach(function (item) {
                        var assetName = item[assetField.Name];
                        assetNames.push(assetName);
                    });

                    APIData.CMDBEX.GetParentsAssets(assetNames, fnRenderAll);
                };
                fnLoadParentInfo(tableRenderContext.obValue());
                tableRenderContext.obValue.subscribe(fnLoadParentInfo);
            }

            var fnRender = function (parent) {

                refer.joContainer.empty();
                if (parent === false) {
                    Object.defineProperty(refer.dataModel, refer.valueAttr, {
                        get: function () {
                            return false;
                        },
                        set: function (rawTableData) {
                        }
                    });
                    renderUserTypeInput(refer, 1);
                } else {
                    refer.obValue(parent);
                    renderSearch(
                        refer,
                        configMode,
                        null,
                        {
                            displayName: "AssetName",
                            valueName: "AssetName"
                        },
                        fnCustomSearchAPI
                    );
                }
                ko.applyBindingsToDescendants(refer.$context, refer.joContainer[0]);
            }
            var fnCustomSearchAPI = function (searchAPI) {
                return function (keyword, start, index, onDone) {
                    searchModel.search(keyword, function (res) {
                        if (res.code && res.code != AMResponseCode.ok)
                            return;

                        var results = res.code ? res.data : res;
                        if (results.length == 0) {
                            var dataSearch = {
                                SearchDataTypeID: dataType.asset,
                                SearchValue: keyword,
                                IsCheckOnly: false
                            };

                            APIData.RequestFieldData.Search(dataSearch, onDone);
                        } else {
                            onDone(res);
                        }
                    });
                }
            };

            var assetValue = refer.dataModel[context.assetField.Name];
            context.renders[assetValue] = fnRender;

            refer.joContainer.append('<input disabled>');
            return true;
        };
    };

    function getBroReferByFieldName(refer, broFieldName) {
        var re = refer.broRefers.find(function (item) {
            return item.field.Name == broFieldName;
        });

        if (re == null)
            throw 'Can not find field with Name = \'' + broFieldName + '\'';

        return re;
    }

    function GuiValidate(refer) {
        var me = this;
        var joInput = refer.joContainer.find("input, select, textarea");
        var messMap = {};

        me.addMess = function (mesCode, mesContent) {
            messMap[mesCode] = mesContent;
            updateGui();
        };

        me.removeMess = function (mesCode) {
            delete messMap[mesCode];
            updateGui();
        };

        function updateGui() {
            var messInArray = [];
            var color = null;
            for (var code in messMap) {
                messInArray.push(messMap[code]);
                if (code > 3000) { // error
                    color = 'red';
                } else if (color > 2000) {  // warn
                    if (color == null)
                        color = 'yellow';
                }
            }

            var mess = messInArray.join(', ');
            joInput.attr('title', mess);
            joInput.css('color', color);
        }
    };

    function getPlaceholder(refer) {
        if (refer.isTableChild)
            return "Nhập tên cột";

        if (refer.field.RequestDataTypeID == dataType.table)
            return "Nhập tên danh sách";
        else
            return "Nhập tên trường";
    }

    function showProperties(refer, configMode, doneEdit) {
        var joProperties = $('<div class="properties" />');
        var joTable = $('<table class="table-form"></table>');
        var joBody = null;

        function add(name, joValue) {
            joValue.addClass("form-control");

            var joRow = $("<tr/>");
            var joTD = $('<td class="attr"></td>');

            if (typeof name == 'string')
                joTD.append('<span>' + name + '</span>');
            else
                joTD.append(name);

            joRow.append(joTD);
            var col = $('<td/>');
            col.append(joValue);
            joRow.append(col);
            joBody.append(joRow);

        }

        function addHead(name) {
            joBody = $('<tbody/>');
            joTable.append(joBody);

            var joRow = $("<tr />");
            joRow.append('<td class="block" colspan="2"><strong>' + name + '</strong></td>');
            joBody.append(joRow);
        }

        function reRender() {
            field.cf = {
                style: field.cf.style,
                //submitedStyle: field.cf.submitedStyle,
            };
            refer.$context._tempIsReshowProperties = 1;
            doneEdit();
        }

        var field = refer.field;

        // style
        (function () {
            addHead('Hiển thị');

            // kieu du lieu
            (function () {
                var jo = $('<select id="k"/>');
                if (field.cf.isCol) {
                    var list = refer.$context.listDataType = [];
                    var fullList = configMode.listDataType;
                    if (typeof  fullList == "function")
                        fullList = fullList();

                    fullList.forEach(function (item) {
                        if (item.ID != dataType.table)
                            list.push(item);
                    });
                    refer.$context.listDataType = list;
                } else {
                    refer.$context.listDataType = configMode.listDataType;
                }


                jo.attr("data-bind", "options: listDataType, value: RequestDataTypeID, optionsValue: 'ID', optionsText: 'DisplayName'");
                var joRow = add("Kiểu dữ liệu:", jo);

                setTimeout(function () {
                    var preDataTypeID = field.RequestDataTypeID;
                    jo.bind("change", function () {
                        if (preDataTypeID != dataType.string && field.Name) {
                            Component.System.alert.show("Bạn có chắc chắn muốn đổi kiểu dữ liệu",
                                {
                                    name: "Có",
                                    cb: reRender
                                }, {
                                    name: "Hoàn tác",
                                    cb: function () {
                                        field.RequestDataTypeID = preDataTypeID;
                                        jo.val(preDataTypeID);
                                    }
                                }
                            );
                        } else {
                            reRender();
                        }
                    });
                }, 1);
            })();

            // width
            add("Độ rộng: ", $('<input data-bind="value: cf.style.width">'));

            // float
            if (!field.cf.isCol) {
                add("Lề: ",
                    $('<select data-bind="value: cf.style.float">' +
                        '<option value="">Trái</option>' +
                        '<option value="right">Phải</option>' +
                        '</select>')
                );
            }

            // line number
            if (field.RequestDataTypeID == dataType.string || field.RequestDataTypeID == dataType.table) {
                add("Số dòng: ", $('<input data-bind="value: cf.row" type="number" min="1">'));
            }

            // col number
            if (field.RequestDataTypeID == dataType.table) {
                var colNumber = field.listChild.length;
                var jo = $('<input type="number" max="15">');
                jo.val(colNumber);
                jo.attr("min", colNumber);
                jo.bind("blur", function () {
                    var val = jo.val();
                    var l = field.listChild;
                    if (val <= l.length)
                        return;

                    var n = 0;
                    for (var i = 0; i < val; i++) {
                        if (l[i] == null)
                            n++;
                    }

                    for (var i = 0; i < n; i++)
                        l.insert();
                });
                add("Số cột: ", jo);
            }

            // vị trí
            (function () {
                // var jo = $('<input type="number">');
                // jo.val(field.Index + 1);
                // jo.bind("change", function () {
                //     var list = refer.fields();
                //     var newIndex = jo.val() - 1;
                //     var currentIndex = list.indexOf(field);
                //     if (newIndex < 0 || newIndex > list.length) {
                //         jo.val(currentIndex);
                //         return;
                //     }
                //
                //     if (newIndex == currentIndex)
                //         return;
                //
                //     list.splice(currentIndex, 1);
                //     list.splice(newIndex, 0, field);
                //
                //     for (var i = 0; i < list.length; i++)
                //         list[i].Index = i;
                // });
                // add("Thứ tự:", jo);
            })();

            ko.applyBindingsToDescendants(refer.$context, joBody[0]);
        })();

        // data constrain
        (function () {
            var fieldDataType = refer.field.RequestDataTypeID;
            var fieldCTDoc = constrainDoc.getDocByDataTypeID(refer.field.RequestDataTypeID);
            addHead('Ràng buộc');
            var constrains = [];
            var isInitDone = false;

            function updateFieldConstrain() {
                if (!isInitDone)
                    return;

                var newFieldConstrain = {};
                constrains.forEach(function (constrain) {
                    if (constrain.expression == null)
                        return;

                    var data = Object.assign({}, constrain);
                    delete data._name;

                    newFieldConstrain[constrain._name] = data;
                });

                refer.field.constrain = newFieldConstrain;

            }

            function buildGUIValueEditor(joValueContainer, ctDoc, constrain) {
                ko.cleanNode(joValueContainer[0]);
                joValueContainer.empty();

                constrain._name = ctDoc.Name;
                if (ctDoc.DependentType == dependentType.html) {
                    ctDoc.DependentData.forEach(function (htmlContent) {
                        var jo = $(htmlContent);
                        var observ = ko.observable();

                        var type = jo.attr("type");
                        var name = jo.attr("name") || 'expression';

                        observ(constrain[name]);
                        observ.subscribe(function (val) {
                            if (!val && val !== 0)
                                delete constrain[name];
                            else
                                constrain[name] = val;

                            updateFieldConstrain();
                        });

                        var bind;
                        switch (type) {
                            case 'checkbox':
                                bind = {
                                    checked: observ
                                };
                                break;
                            case 'number':
                                bind = {
                                    numeric: observ
                                };
                                jo.attr('type', null);
                                break;
                            default:
                                bind = {
                                    value: observ
                                };
                        }

                        if (ctDoc.Name == 'default' && refer.field.RequestDataTypeID == dataType.string && refer.field.cf.row > 1)
                            jo = $('<textarea/>');

                        ko.applyBindingsToNode(jo[0], bind, {});
                        joValueContainer.append(jo);
                    });
                } else {
                    var allowField = [];
                    refer.broRefers.forEach(function (broRefer) {
                        if (ctDoc.DependentData.indexOf(broRefer.field.RequestDataTypeID) == -1)
                            return;
                        if (broRefer == refer)
                            return;
                        allowField.push(broRefer.field);
                    });

                    var observ = ko.observable(constrain.expression);
                    observ.subscribe(function (fieldName) {
                        constrain.expression = fieldName;
                        updateFieldConstrain();
                    });

                    var jo = $('<select/>');
                    ko.applyBindingsToNode(jo[0], {
                        value: observ,
                        options: allowField,
                        optionsText: 'DisplayName',
                        optionsValue: 'Name',
                        optionsCaption: '--',
                    }, refer.$context);
                    joValueContainer.append(jo);
                }
            }

            function buildEntry(groupCTDoc) {
                if (!groupCTDoc || groupCTDoc.length == 0)
                    return;

                var constrain = {};
                var obConstrain = ko.observable();
                var obExpressions = ko.observableArray();
                var joName = null;
                var joValue = $('<span/>');
                var joValueContainer = null;

                constrains.push(constrain);
                obConstrain.subscribe(function (ctDoc) {
                    buildGUIValueEditor(joValueContainer, ctDoc, constrain)
                });
                // name
                var selectedCTDoc = null;
                if (groupCTDoc.length > 1) {
                    joName = $('<select/>');
                } else {
                    selectedCTDoc = groupCTDoc[0];
                    joName = selectedCTDoc.DisplayName;
                }

                add(joName, joValue);
                joValueContainer = joValue.parent();

                var fieldConstrain = refer.field.constrain || {};
                groupCTDoc.forEach(function (ctDoc) {
                    var ct = fieldConstrain[ctDoc.Name];
                    if (ct) {
                        Object.assign(constrain, ct);
                        constrain._name = ctDoc.Name;
                        selectedCTDoc = ctDoc;
                    }
                });


                obConstrain(selectedCTDoc || groupCTDoc[0]);
                if (groupCTDoc.length > 1) {
                    ko.applyBindingsToNode(joName[0], {
                        value: obConstrain,
                        options: groupCTDoc,
                        optionsText: 'DisplayName'
                    }, refer.$context);
                }
            }

            fieldCTDoc.forEach(buildEntry);
            isInitDone = true;
        })();

        joProperties.append(joTable);
        refer.joContainer.append(joProperties);
    }

    function buildGroupTable(refer, configMode) {
        if (refer.field.RequestDataTypeID != dataType.table || refer.requestData.request == null)
            return null;

        var jo = $('<div class="config" />');
        refer.joContainer.append(jo);

        var conf = group.getGroup(refer);
        var showClass = 'fa-angle-double-down';
        var hideClass = 'fa-angle-double-up';

        // Expand button
        var joExpand = $('<i class="fa" style="top: 12px;right: -16px;"></i>');
        joExpand.bind("click", function () {
            // change button
            if (conf.enable) {
                conf.enable = false;
                joExpand.removeClass(showClass);
                joExpand.addClass(hideClass);
            }
            else {
                conf.enable = true;
                joExpand.removeClass(hideClass);
                joExpand.addClass(showClass);
            }

            // save config
            group.saveGroup(refer, conf.enable)

            // make conten
            var eContainer = refer.joContainer[0];
            ko.cleanNode(eContainer);
            refer.joContainer.empty();
            wf.render(refer, configMode);
            ko.applyBindingsToDescendants(refer.$context, eContainer);
            if (!refer.fieldCan[refer.field.Name].edit)
                refer.joContainer.find("input, textarea").attr("disabled", "true");
        });

        if (conf.enable)
            joExpand.addClass(showClass);
        else
            joExpand.addClass(hideClass);

        jo.append(joExpand);
    }

    function setHeader(refer, configMode) {
        if (configMode) {
            function edit() {
                if (refer.field.RequestDataTypeID == dataType.table && setHeader.tableEditing == null)
                    setHeader.tableEditing = doneEdit;
                else if (setHeader.tableEditing) {
                    setHeader.tableEditing(1);
                    setHeader.tableEditing = null;
                }

                refer.joDimArea.append(dimBackground);
                dimBackground.onclick = function () {
                    doneEdit()
                };
                joEdit.hide();
                joRemove.hide();
                joDone.show();
                joName.attr('readonly', false);
                refer.joContainer.addClass("active");
                showProperties(refer, configMode, doneEdit);
            }

            function doneEdit(childEditing) {
                dimBackground.onclick = null;
                $(dimBackground).remove();
                joEdit.css("display", "");
                joRemove.css("display", "");
                joDone.hide();
                joName.attr('readonly', true);

                var joContainer = refer.joContainer;
                joContainer.removeClass("active");

                if (childEditing)
                    return;

                var collection = joContainer.children();
                var n = collection.length;
                for (var i = 0; i < n; i++)
                    ko.cleanNode(collection[i]);

                joContainer.empty();
                refer.fields.update(refer.field);
                //wf.render(refer, configMode);
                //ko.applyBindingsToDescendants(refer.$context, joContainer[0]);
                //configMode.onUpdate && configMode.onUpdate(refer.field);
            }

            function remove() {
                refer.fields.remove(refer.field);
            }

            // config container
            var jo = $('<div class="config" />');
            refer.joContainer.append(jo);

            // edit button
            var joEdit = $('<i class="fa fa-pencil"></i>');
            joEdit.bind("click", edit);
            jo.append(joEdit);

            // remove button
            var joRemove = $('<i class="fa fa-times"></i>');
            joRemove.bind("click", function () {
                remove();
            });
            jo.append(joRemove);

            // done edit button
            var joDone = $('<i class="fa fa-check btn" style="display: none"></i>');
            joDone.bind("click", function () {
                doneEdit();
            });
            jo.append(joDone);

            // input to field name
            var joName = $('<input class="name" readonly>');
            joName.val(refer.field.DisplayName);
            joName.attr("placeholder", getPlaceholder(refer));
            joName.bind("change", function () {
                refer.field.DisplayName = joName.val();
                setTimeout(function () {
                    configMode.onDisplayNameChange(refer.field, refer.fields());
                }, 1);
            });
            refer.joContainer.append(joName);

            if (refer.$context._tempIsReshowProperties) {
                delete refer.$context._tempIsReshowProperties;
                setTimeout(edit, 1);
            }
        } else {
            var title = refer.field.DisplayName;

            if (!refer.can.empty)
                title += '<i style="color: red">*</i>'

            refer.joContainer.append('<strong class="name">' + title + '</strong>');
            //buildGroupTable(refer, configMode);
        }
    }

    function escXss(userInput) {
        if (!userInput)
            return userInput;

        return userInput.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
    }

    wf.render = function (refer, configMode) {
        var containerStlye = refer.joContainer[0].style;
        var cfStyle = refer.field.cf.style;
        //Object.assign(containerStlye, cfStyle);
        //if (refer.requestData && refer.requestData.request)
          //  Object.assign(containerStlye, refer.field.cf.submitedStyle);

        if (cfStyle.width != containerStlye.width)
            containerStlye.width = cfStyle.width + "%";

        if (configMode) {
            containerStlye.display = "";
        }

        var re;
        if (!refer.dontRenderHeader)
            setHeader(refer, configMode);

        if (!refer.dontRenderBody) {
            var renderHandle = component[refer.field.RequestDataTypeID];
            re = renderHandle(refer, configMode);
        }

        if (configMode && configMode.onRenderDone)
            configMode.onRenderDone(refer.joContainer, refer.field);

        return re;
    };
};








