
// ko.bindingHandlers.workflowField = {
    // init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
    //     var value = valueAccessor();
    //     var objectStr = valueAccessor.toString().replace(/^function \(\)\{return /g,'').replace(/ \}/g,'');
    //     var parentObject = objectStr.substring(0, objectStr.lastIndexOf(')'));
    //
    //     var dataType = allBindings.get('dataType') || null;
    //     var config = allBindings.get('config') || null;
    //     if(dataType === null || config === null){
    //         console.warn('[CustomKO] workflowField - DataType or Config undefined, return and not install field');
    //         return;
    //     }
    //     element.Field = _wf.Component.render(dataType, config);
    //     element.appendChild(element.Field.eml);
    //     element.Field.onChange = function () {
    //         if(typeof value !== 'function') {
    //             //is not observable
    //             if (Common.isNull(bindingContext.$index)) {
    //                 //is array object
    //                 eval('viewModel.' + objectStr + ' = "' + eml.Field.value() + '"');
    //                 eval('viewModel.' + parentObject + 'viewModel.' + parentObject + '))');
    //             } else {
    //                 //is not array object
    //                 eval('bindingContext.$data.' + objectStr + ' = "' + eml.Field.value() + '"');
    //             }
    //         } else {
    //             //is observable
    //             value(eml.Field.value());
    //         }
    //     }
    // },
    // update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
    //     var value = valueAccessor();
    //     if(typeof value !== 'function') {
    //         //is not observable
    //         element.Field.value(value);
    //     } else {
    //         //is observable
    //         element.Field.value(value());
    //     }
    // },
//     init: function() {
//         return { 'controlsDescendantBindings': true };
//     },
//     update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
//         element.innerHTML = valueAccessor();
//         ko.applyBindingsToDescendants(bindingContext, element);
//     }
// };

// var Workflow = new (function () {
//     var _wf = this;
//
//     function registryKoBinding() {
//     }
//
//     function registryCustomPrototype() {
//         if (!String.prototype.format) {
//             String.prototype.format = function () {
//                 var args = arguments;
//                 return this.replace(/{(\d+)}/g, function (match, number) {
//                     return typeof args[number] != 'undefined'
//                         ? args[number]
//                         : match
//                         ;
//                 });
//             };
//         }
//     }
//
//     registryKoBinding();
//     registryCustomPrototype();
//
//     this.Component = new (function () {
//         var _cp = this;
//
//         this.render = function (type, config) {
//             var result = _cp[type](config);
//             result.render();
//             return result;
//         };
//
//         this.Abstract = function Abstract(_config, renderFunc){
//             var me = this;
//             this.eml = null;
//             this.cid = 0;
//             this.config = {
//                 width: '30%',
//                 height: '25px'
//             };
//
//             if(_config !== null && _config !== undefined){
//                 me.config = _config;
//                 if(me.config.width === undefined){
//                     me.config.width = "30%";
//                 }
//                 if(me.config.height === undefined){
//                     me.config.height = "25px";
//                 }
//             }
//
//             this.koValueBinding = function (bindingType) {
//                 if(me.config.koBinding !== undefined){
//                     var koBinding = "{0}: {1}".format(bindingType, me.config.koBinding.value);
//                     me.eml.setAttribute('data-bind', koBinding);
//                 }
//             };
//
//             this.value = function value() {
//                 var classInfo = me.__proto__.constructor;
//                 console.warn('Value function is not define on class '+classInfo.name);
//                 return null;
//             };
//
//             this.onChange = function () {
//                 var classInfo = me.__proto__.constructor;
//                 console.warn('OnChange function is not define on class '+classInfo.name);
//                 return null;
//             };
//
//             this.render = function () {
//                 var classInfo = me.__proto__.constructor;
//                 me.cid = _wf.takeComponentID();
//                 if(renderFunc === undefined){
//                     console.warn('Render function is not define on class '+classInfo.name);
//                 } else {
//                     return renderFunc();
//                 }
//             };
//         };
//
//         this.TextArea = function TextArea(_config) {
//             var me = this;
//             _cp.Abstract.call(this, _config, function () {
//                 me.eml = document.createElement('textarea');
//                 me.eml.setAttribute('id', 'c'+me.cid);
//                 me.eml.style.width = me.config.width;
//                 me.eml.style.height = me.config.height;
//                 return me.eml;
//             });
//
//             this.bindingName = 'text';
//
//             this.value = function (_value) {
//                 if(_value !== undefined && _value !== null) {
//                     return me.eml.innerText;
//                 } else {
//                     me.eml.innerText = _value;
//                 }
//             }
//         };
//
//         this.Table = function Table(_config) {
//
//             var me = this;
//             _cp.Abstract.call(this, _config, function () {
//                 me.eml = document.createElement('table');
//             });
//
//             var createRow = function (cols) {
//
//             };
//
//             var createCol = function (content) {
//                 var col = document.createElement('TD');
//                 if(content !== undefined){
//                     col.innerHTML = content;
//                 }
//                 return col;
//             };
//         };
//
//         this.Text = function Text(_config){
//             var me = this;
//             _cp.Abstract.call(this, _config, function () {
//                 me.eml = document.createElement('input');
//                 me.eml.setAttribute('id', 'c'+me.cid);
//                 me.eml.setAttribute('type', 'text');
//                 me.eml.setAttribute('comp', 'wf-com');
//                 me.eml.style.width = me.config.width;
//                 me.eml.style.height = me.config.height;
//                 me.eml.addEventListener('change', me.onChange);
//                 return me.eml;
//             });
//
//             this.bindingName = 'value';
//
//             this.value = function (_value) {
//                 if(_value !== undefined && _value !== null) {
//                     return me.eml.value;
//                 } else {
//                     me.eml.value = _value;
//                 }
//             }
//         };
//
//         this.Image = function Image(_config){
//             var me = this;
//             _cp.Abstract.call(this, _config, function () {
//                 me.eml = document.createElement('div');
//
//                 var inputFile = document.createElement('input');
//                 var inputLabel = document.createElement('label');
//                 var reviewPanel = document.createElement('div');
//
//                 me.eml.setAttribute('id', 'c'+me.cid);
//                 me.eml.setAttribute('comp', 'wf-com');
//                 me.eml.appendChild(inputFile);
//                 me.eml.appendChild(inputLabel);
//                 me.eml.appendChild(reviewPanel);
//
//                 inputFile.setAttribute('type', 'file');
//                 inputFile.setAttribute('accept', 'image/*');
//                 inputFile.setAttribute('id', 'c_0_'+me.cid);
//                 inputFile.setAttribute('comp', 'wf-com');
//
//                 inputLabel.setAttribute('id', 'c_1_'+me.cid);
//                 inputLabel.setAttribute('for', 'c_0_'+me.cid);
//                 inputLabel.setAttribute('comp', 'wf-com');
//                 inputLabel.innerText = 'Chọn Ảnh';
//
//                 reviewPanel.setAttribute('for', 'c_2_'+me.cid);
//                 reviewPanel.setAttribute('comp', 'wf-com');
//
//                 me.eml.style.width = me.config.width;
//                 me.eml.style.maxHeight = me.config.height;
//
//                 return me.eml;
//             });
//         };
//     })();
// })();
