
Component.Custom = Component.Custom || {};
Component.Custom.TableData = new(function () {
    var tableData = this;

    var tableDataClass = function(_eml){
        var me = this;
        var eml = _eml;

        var total = 0;
        this.total = function(_val){
            if(_val){
                total = _val;
            } else {
                return total;
            }
        };

        var pageCount = 0;
        this.pageCount = function(_val){
            if(_val){
                pageCount = _val;
                on_paging();
            } else {
                return pageCount;
            }
        };

        var limit = 0;
        this.limit = function(_val){
            if(_val){
                limit = _val;
            } else {
                return limit;
            }
        };

        var isEditable = false;
        this.isEditable = function (_val) {
            if(_val){
                editable = _val;
            } else {
                return editable;
            }
        };

        var isPaging = false;
        this.isPaging = function (_val) {
            if(_val){
                isPaging = _val;
            } else {
                return isPaging;
            }
        };

        var currentPage = 1;
        this.currentPage = function (_val) {
            if(_val){
                currentPage = _val;
            } else {
                return currentPage;
            }
        };

        var pageItemCount = 10;
        var currentPageRange = 0;

        this.isInitPage = false;

        var init = function(){
            isPaging = Common.parseBoolean(eml.attr('isPaging'));
            limit = parseInt(eml.attr('limit'));
            isEditable = parseInt(eml.attr('isEditable'));

            /* Bo tu dong tao tfoot
            var tfoot = document.createElement('tfoot');
            eml.append(tfoot);

            if(isEditable){

            }

            var colCount = eml.find('thead tr th').length;
            var paging = $('<tr><td colspan="'+colCount+'"></td></tr>');

            $(tfoot).append(paging);
            */
        };

        this.on_PageChange = function () { /* runtime implement */ };

        var do_PageChange = function (eml) {
            if(pageCount <= 10){
                return;
            }
            currentPage = parseInt(eml.innerText);
            var rangeIndex = Math.floor(currentPage/pageItemCount);
            if(rangeIndex !== currentPageRange){
                currentPageRange = rangeIndex;
                var start = rangeIndex*pageItemCount;
                var end = start + pageItemCount;

                render_pageItem(start, end);
            }
        };

        var render_pageItem = function (start, end) {
            if(end > pageCount){
                end = pageCount;
            }
            var pagingContainer = eml.find('tfoot tr td');
            pagingContainer.html('');
            for(var i = start-1; i <= end; i++){
                if(i <= 0){
                    continue;
                }
                var pageItem = $('<span></span>');
                pageItem.text(i);
                pageItem.on('click', function(){
                    pagingContainer.find('span.active').removeClass('active');
                    $(this).addClass('active');
                    do_PageChange(this);
                    me.on_PageChange(this);
                });
                if(i===currentPage){
                    pageItem.addClass('active');
                }
                pagingContainer.append(pageItem);
            }
        };

        var on_paging = function () {
            render_pageItem(1,pageItemCount);
            me.isInitPage = true;
        };
        init();
    };

    this.get = function (name) {
        return $('[component="tabledata"][name="'+name+'"]')[0].TableData;
    };

    this.init = function(){
        var emls = $('[component="tabledata"]');
        emls.each(function(){
            this['TableData'] = new tableDataClass($(this));
        });
    };

    $(document).ready(function () {
        tableData.init();
    });
})();