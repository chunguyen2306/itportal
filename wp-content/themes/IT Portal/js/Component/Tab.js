/**
 * Created by tientm2 on 11/21/2017.
 */
Component.Custom = Component.Custom || {};
Component.Custom.Tab = new(function () {
    var tab = this;

    $(document).ready(function () {

        $('[component="tab"]').each(function () {
           var eml = $(this);
           var tabItems = eml.find('.tab-item li');
            tabItems.on('click', function () {
                eml.find('.tabContent').removeClass('active');
                tabItems.removeClass('active');
                var target = $(this).attr('target');
                $('#'+target).addClass('active');
                $(this).addClass('active');
            });
        });

    });
})();