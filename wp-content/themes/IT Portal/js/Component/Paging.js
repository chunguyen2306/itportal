Component.Custom = Component.Custom || {};
Component.Custom.Paging = new (function () {
    var paging = this;

    var pagingClass = function (_eml) {
        var me = this;
        var eml = _eml;

        var total = 0;
        this.total = function (_val) {
            if (_val) {
                total = _val;
            } else {
                return total;
            }
        };

        var pageCount = 0;
        this.pageCount = function (_val) {
            if (_val) {
                pageCount = _val;
                on_paging();
            } else {
                return pageCount;
            }
        };

        var limit = 0;
        this.limit = function (_val) {
            if (_val) {
                limit = _val;
            } else {
                return limit;
            }
        };

        var currentPage = parseInt(Common.getUrlParameters()['page']) || 1;
        this.currentPage = function (_val) {
            if (_val) {
                currentPage = _val;
            } else {
                return currentPage;
            }
        };

        var pageItemLimit = 5;
        this.pageItemCount = function (_val) {
            if (_val) {
                pageItemLimit = _val;
            } else {
                return pageItemLimit;
            }
        };

        var rangeIndex = 0;
        var currentPageRange = 0;

        //this.isInitPage = false;

        this.on_PageChange = function () { /* runtime implement */
        };

        var do_PageChange = function (pageItem) {
            pageItem = $(pageItem);
            pageItem.addClass('active');
            if (pageCount <= pageItemLimit) {
                return;
            }
            if (pageItem.hasClass('next')) {
                if (currentPage + 1 <= pageCount) {
                    currentPage += 1;
                }
            } else if (pageItem.hasClass('previous')) {
                if (currentPage - 1 >= 1) {
                    currentPage -= 1;
                }
            } else {
                currentPage = parseInt(pageItem.text());
            }

            changePage();
        };

        var changePage = function () {
            var start, end, intervalItem;
            rangeIndex = Math.floor(currentPage / pageItemLimit);

            intervalItem = parseInt(pageItemLimit / 2);
            start = currentPage - intervalItem;
            end = currentPage + intervalItem;
            if (start < 1) {
                start = 1;
                end = start + pageItemLimit - 1;
            } else if (end > pageCount) {
                end = pageCount;
                start = end - pageItemLimit + 1;
            }
            render_pageItem(start, end);
        };

        var pageItem_onClick = function () {
            eml.find('span.active').removeClass('active');
            currentPage = $(this).text();
            $(this).addClass("active");
            do_PageChange(this);
            me.on_PageChange(me);
        };

        var directorItem_onClick = function () {
            eml.find('span.active').removeClass('active');
            do_PageChange(this);
            me.on_PageChange(me);
        };

        var create_page_item = function (text, isItem) {
            var pageItem = $('<span></span>');
            pageItem.text(text);

            if (isItem) {
                pageItem.on('click', pageItem_onClick);
            } else {
                pageItem.on('click', directorItem_onClick);
            }
            return pageItem;
        };

        var render_pageItem = function (start, end) {
            if (pageCount <= 1) {
                //return;
            }
            eml.html('');
            if (end > pageCount) {
                end = pageCount;
            }
            if (pageCount > pageItemLimit) {
                var previousPageRange = create_page_item(' ');
                previousPageRange.addClass('previous fa fa-angle-left');
                eml.append(previousPageRange);
            }
            if (pageCount > 1) {
                for (var i = start; i <= end; i++) {
                    if (i <= 0) {
                        continue;
                    }
                    var pageItem = create_page_item(i, true);
                    if (i === currentPage) {
                        pageItem.addClass('active');
                    }
                    eml.append(pageItem);
                }
            }
            if (pageCount > pageItemLimit) {
                var nextPageRange = create_page_item(' ');
                nextPageRange.addClass('next fa fa-angle-right');
                eml.append(nextPageRange);
            }
        };

        this.render = function (_pageCount, _limit, _total, _currentPage) {
            //if(!me.isInitPage) {
            //pageItemLimit = parseInt(eml.attr('pageItemLimit')); chuyen xuong init()
            pageCount = _pageCount || parseInt(eml.attr('pageCount'));
            limit = _limit || parseInt(eml.attr('pageItemLimit'));
            //currentPage = parseInt(eml.attr('currentPage')) || 1;

            if(_currentPage !== undefined){
                _currentPage = parseInt(_currentPage) || parseInt(eml.attr('currentPage'));
                if (_currentPage !== null && _currentPage !== undefined && _currentPage !== NaN) {
                    currentPage = _currentPage;
                }
            }

            currentPage = parseInt(currentPage);

            total = _total || parseInt(eml.attr('total'));
            if (pageCount >= pageItemLimit) {
                if (currentPage !== 1) {
                    changePage();
                } else {
                    render_pageItem(1, pageItemLimit);
                }
            } else {
                render_pageItem(1, pageCount);
            }
            //me.isInitPage = true;
            //}
        }
    };

    this.__defineGetter__('items', function () {
        var emls = $('[component="paging"]');
        var result = {};
        emls.each(function () {
            result[this.getAttribute('name')] = this['Paging'];
        });
        return result;
    });

    this.init = function () {
        var emls = $('[component="paging"]');
        emls.each(function () {
            this['Paging'] = new pagingClass($(this));
        });
    };

    $(document).ready(function () {
        paging.init();
    });
})();