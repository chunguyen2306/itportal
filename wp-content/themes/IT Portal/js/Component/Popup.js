/*
 <div component="popup" name="details" width="80%" height="80%" >
    <h2>Thông tin model tài sản
        <button class="red close">
            <i class="fa fa-close"></i>
        </button>
    </h2>
    <div class="popup-content">
         <div component="form" align="float" name="ComponentDetails">
             <div class="form-content">
             <item text=""
                     name="ModelID"
                     type="hidden">
             </item>
             <item text=""
                     name="ID"
                     type="hidden">
             </item>
             <item width="96%" text="Tên model:" name="ComponentName" type="text"></item>
             <item width="56%" text="Mô tả ngắn" name="ShortDescription" type="editor" height="200px"></item>
             <item width="40%" text="Giá" name="Cost" type="text"></item>
             <item width="40%" text="Hiển thị" name="IsDisplay" type="checkbox"></item>
             <item width="40%" text="Ảnh đại diện" name="Thubmails" type="imagepicker" field="Gallery"></item>
             <item width="96%" text="Mô tả chi tiết" name="Description" type="editor" height="400px"></item>

             <item width="96%" multiple="" text="Ảnh sản phẩm" name="Gallery" accept="image/*" placeholder="Ảnh sản phẩm" imagereview="true" type="file"></item>
             <item width="96%" text="Hướng dẫn" name="ManualGuide" type="editor" height="400px"></item>
             </div>
             <div class="form-control-panel">
             <button data-bind="click: do_SubmitDetail, text: i18n('gui:resource-type:detailForm:btnSubmit')"></button>
             <button class="red" data-bind="click: do_CancelDetail, text: i18n('gui:resource-type:detailForm:btnCancel')"></button>
             </div>
         </div>
     </div>
 </div>
 */

Component.Custom = Component.Custom || {};
Component.Custom.Popup = new(function () {
    var popup = this;
    var popupClass = function (_eml) {
        var me = this;
        var eml = _eml;

        $(eml).find('.close').on('click', function () {
            me.on_Close();
            me.hide();
        });
        var btnClose = $(eml).find('h2 .close');
        btnClose.on('click', function () {
            me.on_Close();
            me.hide();
        });

        this.on_Close = function(){
            /* Runtime Definition */
        };

        this.show = function (callback) {
            /*var myHeight = $(eml).height();
            var winHeight = $(window).height();
            var top = '';
            if(myHeight >= winHeight*0.8){
                top = '20px';
            } else {
                top = (winHeight/2 - myHeight/2 - 60) + 'px';
            }
            $(eml).css({
               top: top
            });*/
            var myWidth = $(eml).attr('width') || '50%';
            var myHeight = $(eml).attr('height') || '55%';
            var unit = myWidth.indexOf('%') >= 0?'%':'px';
            $(eml).css({
                width: myWidth,
                height: myHeight,
                marginLeft: -(parseInt(myWidth) / 2) + unit,
                left: '50%',
                top: '50%'

            });
            if(myWidth !== undefined || myHeight !== undefined) {
                $(eml).find('.popup-content').css({
                    height: $(eml).height() - ($(eml).find('>h2').height() + 40 + $(eml).find('>.popup-bottom').height())
                });
            }
            Component.System.blank.show();
            $(eml).addClass('show');
            $(eml).css({
                marginTop: -($(eml)[0].offsetHeight / 2) + 'px'

            });
            Common.callBack(callback);
        };

        this.hide = function () {
            Component.System.blank.hide();
            $(eml).removeClass('show');
        };
    };

    Object.defineProperty(popup, 'items', { get: function() {
        var result = {};
        $('[component="popup"]').each(function () {
            result[this.getAttribute('name')] = this;
        });
        return result;
    } });

    _prop('Active', function () {
        var activeItem = $('[component="popup"].show');
        if(activeItem.length > 0){
            return activeItem[0].Popup;
        } else {
            return null;
        }

    });
    _prop('HasActive', function () {
        var activeItem = $('[component="popup"].show');
        return activeItem.length > 0;
    });

    this.init = function () {
        var emls = $('[component="popup"]');
        emls.each(function(){
            this['Popup'] = new popupClass(this);
        });
        var emlsTrigger = $('[component="popup-trigger"]');
        emlsTrigger.each(function(){
            this.addEventListener('click', function () {
                var popupName = this.getAttribute('popupname');
                var staus = this.getAttribute('popupstatus');
                Component.Custom.Popup.items[popupName]['Popup'][staus]();
            });

        });
    };

    $(document).ready(function () {
        popup.init();
    });
})();