/**
 * Created by tientm2 on 10/20/2017.
 */

Component.ShopCart = new function() {
  var me = this;
  var MAX_QUANTITY_OF_MODEL_IN_CART = 10;
  //cpnguyen
  const ACTION_TYPE = ["Trả", "Báo mất", "Báo hư hỏng"];

  const ACTION_TYPE_KEY = {
    Trả: "listReturnItem",
    "Báo mất": "listLoseItem",
    "Báo hư hỏng": "listFixItem"
  };

  var checkQuantityAdd = function(modelID, newQuantity, cartname) {
    if (cartname === null || cartname === undefined || cartname == "carts") {
      var quantityInCart = getQuantityInCart(modelID);
      var temp = MAX_QUANTITY_OF_MODEL_IN_CART - quantityInCart;
      if (newQuantity > MAX_QUANTITY_OF_MODEL_IN_CART) {
        if (quantityInCart == MAX_QUANTITY_OF_MODEL_IN_CART) {
          Component.System.alert.show(
            "Bạn đang có <b>" +
              quantityInCart +
              "</b> sản phẩm này trong Giỏ hàng </br> Bạn không được đặt thêm sản phẩm này nữa",
            {
              name: "Đóng",
              cb: function() {}
            }
          );
          return false;
        }
        Component.System.alert.show(
          "Bạn đang có <b>" +
            quantityInCart +
            "</b> sản phẩm này trong Giỏ hàng </br> Chỉ được đặt thêm tối đa <b>" +
            temp +
            "</b> sản phẩm này",
          {
            name: "Đóng",
            cb: function() {}
          }
        );
        return false;
      } else {
        return true;
      }
    } else if (cartname == "cartother") {
      if (newQuantity > MAX_QUANTITY_OF_MODEL_IN_CART) {
        Component.System.alert.show("Bạn không được đặt quá 10 sản phẩm", {
          name: "Đóng",
          cb: function() {}
        });
        return false;
      } else {
        return true;
      }
    }
  };

  var checkQuantityAddNew = function(modelID) {};

  //cpnguyen
  this.AddToCart = function(model) {
    const carts = me.GetCart() || [];
    if (me.IsExisted(model.COMPONENTID, "carts")) {
      var newQuantity =
        getQuantityInCart(model.COMPONENTID) + (model.QUANTITY || 1);
      if (checkQuantityAdd(model.COMPONENTID, newQuantity)) {
        me.ChangeQuantityOfModel(model.COMPONENTID, newQuantity);
      }
    } else {
      getQuantityInStockByModelID(model.COMPONENTID, function(stock) {
        var quantity = model.QUANTITY || 1;
        if (quantity > 10) {
          Component.System.alert.show("Bạn không được đặt quá 10 sản phẩm", {
            name: "Đóng",
            cb: function() {}
          });
          return;
        }
        var status = "";
        if (stock == 0) {
          status = "OutOfStock";
        } else if (quantity > stock) {
          status = "NotEnough";
        } else {
          status = "Normal";
        }
        getDayForProduct(model.COMPONENTID, function(days_of_purchase) {
          carts.push({
            COMPONENTID: model.COMPONENTID,
            COMPONENTNAME: model.COMPONENTNAME,
            COMPONENTTYPEID: model.COMPONENTTYPEID,
            COMPONENTTYPENAME: model.COMPONENTTYPENAME,
            COMMENTS: model.COMMENTS,
            QUANTITY: quantity,
            STOCK: stock,
            STATUS: status,
            DAYSOFPURCHASE: days_of_purchase
          });
          Libs.LocalStorage.val("carts", JSON.stringify(carts));
          me.UpdateCart();
        });
      });
    }
  };

  //cpnguyen
  this.getAllItems = function() {
    //Unify all items of type fix, damage, return
    let cart = [];

    ACTION_TYPE.forEach(function(action) {
      cart = cart.concat(me.GetCart(ACTION_TYPE_KEY[action]) || []);
    });

    return cart;
  };

  //cpnguyen
  this.ChangeActionTypeOfModel = function(model, actionType) {
    let carts = me.getAllItems();
    let item = carts.find(function(asset) {
      return asset.RESOURCENAME == model.RESOURCENAME;
    });
    item.ACTIONTYPE = actionType;

    me.SplitToActionTypes(carts);
    me.UpdateCart();
  };

  //cpnguyen
  this.SplitToActionTypes = function(carts) {
    ACTION_TYPE.forEach(function(action) {
      let typeCart = [];
      carts.forEach(function(item) {
        if (item.ACTIONTYPE == action) {
          typeCart.push(item);
        }
      });
      Libs.LocalStorage.val(ACTION_TYPE_KEY[action], JSON.stringify(typeCart));
    });
  };

  //cpnguyen
  this.AddToCartFromMyProduct = function(asset_array) {
    let carts = [];

    asset_array.forEach(function(model) {
      if (me.IsExisted(model.RESOURCENAME)) {
        me.ChangeActionTypeOfModel(model, model.ACTIONTYPE);
      } else {
        getDayForProduct(model.COMPONENTID, function(days_of_purchase) {
          carts.push({
            RESOURCENAME: model.RESOURCENAME,
            COMPONENTID: model.COMPONENTID,
            COMPONENTNAME: model.COMPONENTNAME,
            COMPONENTTYPEID: model.COMPONENTTYPEID,
            COMPONENTTYPENAME: model.COMPONENTTYPENAME,
            COMMENTS: model.COMMENTS,
            QUANTITY: 1,
            ACTIONTYPE: model.ACTIONTYPE,
            DAYSOFPURCHASE: days_of_purchase
          });
          // Libs.LocalStorage.val('carts', JSON.stringify(carts));
          me.SplitToActionTypes(carts);
          me.UpdateCart();
        });
      }
    });
  };

  this.AddToCartOther = function(item) {
    var carts = me.GetCart("cartother");
    if (me.IsExisted(item.ModelName, "cartother")) {
      me.AddQuantity(item.ModelName, item.Quantity || 1, "cartother");
    } else {
      carts.push({
        ID: item.ID,
        ModelType: item.ModelType,
        ModelName: item.ModelName,
        LinkDescription: item.LinkDescription,
        ReferPrice: item.ReferPrice,
        Quantity: item.Quantity || 1,
        Note: item.Note || 1
      });
      Libs.LocalStorage.val("cartother", JSON.stringify(carts));
    }
    me.UpdateCart();
  };

  var getDayForProduct = function(modelID, callback) {
    APIData.AssetModelDetail.GetByModelID(modelID, function(res) {
      res = JSON.parse(res);
      if (res === undefined || res === null) {
        res = {
          BuyDays: 15,
          TransferDays: 2
        };
      }
      var result = parseInt(res.BuyDays) + parseInt(res.TransferDays);
      callback(result);
    });
  };

  var getQuantityInStockByModelID = function(modelID, callback) {
    APIData.Component.GetQuantityInHand(modelID, function(stock) {
      stock = JSON.parse(stock);
      callback(stock.Result);
    });
  };

  var getQuantityInCart = function(modelID) {
    var carts = me.GetCart();
    var item = carts.find(function(i) {
      return i.COMPONENTID == modelID;
    });

    return item.QUANTITY;
  };

  //cpnguyen
  this.IsExisted = function(id, cartname) {
    let carts = [];
    if (cartname == "carts") {
      carts = me.GetCart();
    } else {
      carts = me.getAllItems();
    }

    if (!cartname) {
      let item = carts.find(function(i) {
        return i.RESOURCENAME == id;
      });
      if (item) {
        return true;
      } else {
        return false;
      }
    } else {
      let item = carts.find(function(i) {
        if (cartname == "carts") {
          return i.COMPONENTID == id;
        } else if (cartname == "cartother") {
          return i.ID == id;
        }
      });
      if (item) {
        return true;
      } else {
        return false;
      }
    }
  };

  this.RemoveFormCart = function(id, cartname) {
    var popName = "COMPONENTID";
    if (cartname === undefined || cartname === null) {
      cartname = "carts";
    } else {
      popName = "ID";
    }
    var carts = me.GetCart(cartname);
    var item = carts.find(function(i) {
      return i[popName] == id;
    });
    carts.splice(carts.indexOf(item), 1);
    Libs.LocalStorage.val(cartname, JSON.stringify(carts));
    me.UpdateCart();
  };
  this.RemoveItemListEdit = function(id, cartname) {
    var popName = "COMPONENTID";
    if (cartname === undefined || cartname === null) {
      cartname = "carts";
    } else {
      popName = "RESOURCENAME";
    }
    var carts = me.GetCart(cartname);

    if (carts.length > 0) {
      var item = carts.find(function(i) {
        return i[popName] == id;
      });
      item !== undefined && carts.splice(carts.indexOf(item), 1);

      Libs.LocalStorage.val(cartname, JSON.stringify(carts));
      me.UpdateCart();
    }
  };
  this.AddQuantity = function(id, quantity, cartname) {
    if (cartname === undefined || cartname === null) {
      cartname = "carts";
    }
    if (cartname == "carts" && !checkQuantityAdd(id, quantity)) {
      return;
    }
    var carts = me.GetCart(cartname);
    var item = carts.find(function(i) {
      return i["COMPONENTID"] == id;
    });
    var newQuantity = item["QUANTITY"] + quantity;
    me.ChangeQuantityOfModel(id, newQuantity, cartname);
  };

  this.ChangeQuantityOfModel = function(modelID, newQuantity, cartName) {
    var popName = "COMPONENTID";
    var quantityPropName = "QUANTITY";
    var statusPropName = "STATUS";
    var stockPropName = "STOCK";

    if (cartName == "cartother") {
      popName = "ID";
      quantityPropName = "Quantity";
    } else {
      cartName = "carts";
    }
    if (cartName == "carts") {
      if (!checkQuantityAdd(modelID, newQuantity)) {
        return;
      }
    } else if (cartName == "cartother") {
      if (!checkQuantityAdd(null, newQuantity, cartName)) {
        return;
      }
    }
    var carts = me.GetCart(cartName);
    var item = carts.find(function(i) {
      return i[popName] == modelID;
    });
    item[quantityPropName] = newQuantity;
    if (cartName == "carts") {
      if (item[stockPropName] != 0) {
        if (item[quantityPropName] > item[stockPropName]) {
          item[statusPropName] = "NotEnough";
        } else {
          item[statusPropName] = "Normal";
        }
      }
    }
    Libs.LocalStorage.val(cartName, JSON.stringify(carts));
    me.UpdateCart();
  };

  this.GetCart = function(cartname) {
    if (cartname === undefined || cartname === null) {
      cartname = "carts";
    }
    var carts = JSON.parse(Libs.LocalStorage.val(cartname));
    if (carts === undefined || carts === null) {
      carts = [];
    }
    return carts;
  };

  this.ResetCart = function() {
    Libs.LocalStorage.val("cartDate", new Date().toString());
    Libs.LocalStorage.val("carts", JSON.stringify([]));
    Libs.LocalStorage.val("cartother", JSON.stringify([]));
    this.UpdateCart();
  };

  //cpnguyen
  //this function update cart counting number
  this.UpdateCart = function() {
    var carts = me.GetCart();
    var cartother = me.GetCart("cartother");
    var quantityProducts = 0;
    for (var i = 0; i < carts.length; i++) {
      quantityProducts += carts[i]["QUANTITY"];
    }
    for (var i = 0; i < cartother.length; i++) {
      quantityProducts += parseInt(cartother[i]["Quantity"]);
    }

    //combine assets of 3 types : lost,return,damage
    quantityProducts += me.GetCart("listLoseItem").length;
    quantityProducts += me.GetCart("listFixItem").length;
    quantityProducts += me.GetCart("listReturnItem").length;

    $('[component="shopcart-noti"]').text(quantityProducts);
    return quantityProducts;
  };
}();
