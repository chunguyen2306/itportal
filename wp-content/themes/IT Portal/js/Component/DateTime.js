Component.Custom = Component.Custom || {};
Component.Custom.Number = new(function () {
    var number = this;

    this.initEvent = function(){
        $('[component="date-time"]').each(function () {
            var eml = $(this);
            eml.on('click', function () {
                eml.trigger('change');
            });
        });
    };

    this.init = function () {

        $('[component="date-time"]').each(function () {
            var eml = $(this);
            if(eml[0].dateTime === undefined) {
                eml[0].dateTime = true;
            }
        });
    };

    $(document).ready(function () {
        number.init();
    });

    /*Common.wait(function () {
        return window['alreadyBinding'];
    }, function(){
        number.init();
    });*/
})();