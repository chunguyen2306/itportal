Component.Module = {};
Component.Module.AbstractModuleView = function (model, _onDone) {
    if(_onDone === undefined){
        this.onDone = function () {
            console.warn('On Done event of View is not definition');
        }
    } else {
        this.onDone = _onDone;
    }
};
