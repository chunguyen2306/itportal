/**
 * Created by lacha on 27/06/2018
 */

Component.AssetsCart = new function() {
  var me = this;
  const assets_cart_name = "listAsset";
  var username = JSON.parse(Libs.LocalStorage.val("username"));

  this.RemoveFormCart = function(assetName, usernameInput) {
    username = JSON.parse(Libs.LocalStorage.val("username"));
    if (usernameInput != username) {
      return;
    }
    var popName = "AssetName";
    var carts = JSON.parse(Libs.LocalStorage.val(assets_cart_name));

    var item = carts.find(function(i) {
      return i[popName] == assetName;
    });
    carts.splice(carts.indexOf(item), 1);
    Libs.LocalStorage.val(assets_cart_name, JSON.stringify(carts));
  };

  this.GetUserName = function() {
    return username;
  };

  this.SetUserName = function(username) {
    Libs.LocalStorage.val("username", JSON.stringify(username));
  };

  this.RemoveAll = function() {
    Libs.LocalStorage.val(assets_cart_name, JSON.stringify([]));
  };

  this.AddToCart = function(asset, usernameInput) {
    username = JSON.parse(Libs.LocalStorage.val("username"));
    if (usernameInput != username) {
      this.SetUserName(usernameInput);
      this.ResetCart();
    }
    var carts = JSON.parse(Libs.LocalStorage.val(assets_cart_name));
    if (carts === undefined || carts === null) {
      carts = [];
    }
    carts.push(asset);
    Libs.LocalStorage.val(assets_cart_name, JSON.stringify(carts));
  };

  this.GetCart = function() {
    var carts = JSON.parse(Libs.LocalStorage.val(assets_cart_name));
    if (carts === undefined || carts === null) {
      carts = [];
    }
    return carts;
  };

  this.ResetCart = function() {
    Libs.LocalStorage.val(assets_cart_name, JSON.stringify([]));
  };
  this.ResetEditCart = function() {
    Libs.LocalStorage.val("listFixItem", JSON.stringify([]));
    Libs.LocalStorage.val("listReturnItem", JSON.stringify([]));
    Libs.LocalStorage.val("listLoseItem", JSON.stringify([]));
  };
  this.GetQuantityAssetSelected = function() {
    return me.GetCart().length;
  };
}();
