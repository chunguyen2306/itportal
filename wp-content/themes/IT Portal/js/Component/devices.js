function classDeviceManager(deviceType, rememberDeviceSelected) {

    var me = this;
    var devices = null;
    var currentDevice = null;
    var currentStream = null;

    rememberDeviceSelected = arguments.length > 1 ? rememberDeviceSelected[1] : true;

    if (deviceType != 'videoinput' && deviceType != 'audioinput' && deviceType != 'audiooutput')
        throw 'Unsuport device type: ' + deviceType;

    if (!navigator || !navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
        throw 'Can not access device on this device';
    }

    if (!navigator || !navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
        throw 'Can not access user device list';
    }

    function accessDevice(device, onDone) {
        if (!devices.length) {
            onDone({
                code: AMResponseCode.notFound,
            });
        }

        if (currentDevice)
            throw 'Can not access another device before release current device';

        // create device filter
        var constraints = {
            video: {
                deviceId: {exact: device.deviceID}
            }
        };

        // access device
        navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
            currentDevice = device;
            currentStream = stream;
            onDone({
                code: AMResponseCode.ok,
                data: stream
            });
        }).catch(function (error) {
            onDone({
                code: AMResponseCode.expression,
                message: error
            });
        });
    }

    me.fetchDevice = function (onDone) {
        if (!devices)
            devices = [];

        var promiss = navigator.mediaDevices.enumerateDevices();
        promiss.then(function (list) {
            list.forEach(function (item) {
                if (item.kind != deviceType)
                    return;

                var existed = devices.find(function (exist) {
                    return exist.deviceId == item.deviceId;
                })
                if (existed)
                    return;

                devices.push(item);
            });

            if (devices.length) {
                onDone({
                    code: AMResponseCode.ok,
                    devices: list
                });
            } else {
                onDone({
                    code: AMResponseCode.notFound
                });
            }
        });

        promiss.catch(function (error) {
            onDone({
                code: AMResponseCode.exception,
                message: error,
            });
        });
    }

    /**
     * @param type: videoinput ||
     * @param onDone
     */
    me.accessDevice = function (onDone) {
        var selectAndAccessDevice = function () {
            var userRememberDeviceID = rememberDeviceSelected ? localStorage['deviceManager_userRememberDeviceID_' + deviceType] : null;
            var selectedDevice = null;

            // query user remember device
            if (userRememberDeviceID) {
                for (var i = 0; i < devices.length; i++) {
                    if (devices[i].deviceId == userRememberDeviceID) {
                        selectedDevice = devices[i];
                        break;
                    }
                }
            }

            // no device -> select last device
            if (selectedDevice == null) {
                selectedDevice = devices[devices.length - 1];

                if (rememberDeviceSelected) {
                    localStorage['deviceManager_userRememberDeviceID_' + deviceType] = selectedDevice;
                }
            }

            accessDevice(selectedDevice, onDone);
        };

        if (devices == null) {
            me.fetchDevice(function (res) {
                if (res.code != AMResponseCode.ok)
                    onDone(res);
                else
                    selectAndAccessDevice();
            });
        } else {
            selectAndAccessDevice();
        }
    }

    me.switchDevice = function (onDone) {
        if (currentDevice)
            me.releaseDevice();

        var index = devices.indexOf(currentDevice) + 1;
        if (index >= devices.length)
            index = 0;

        accessDevice(devices[index]);
    }

    me.releaseDevice = function () {
        if (currentDevice == null)
            return;

        currentStream.getTracks().forEach(function (track) {
            track.stop();
        });
        currentDevice = null;
    }
}

function classCameraInput() {
    var me = this;

    var comPopup = $('<div component="popup" name="comCamera"></div>');
    var comPopupTitle = $('<h2>Camera</h2>');
    var comPopupContent = $('<div class="popup-content"></div>');
    var comPopupBottom = $('<div class="popup-bottom"></div>');
    var joCamInput = $('<div class="input-cam">' +
        '<div class="live-stream"></div>' +
        '<div class="right"></div>' +
        '</div>');

    var btnOk = $('<button>Đồng ý</button>');
    var btnCancel = $('<button class="red" >Đóng</button>');
    var btnTakePhoto = $('<button class="green"><i class="fa fa-camera"></i></button>');

    var panelCamPreview = $('<div class="cam-previews"></div>');
    var emlVideo = $('<video autoplay></video>');
    var palLiveStream = joCamInput.find('.live-stream');
    var canvas = $('<canvas style="display: none"></canvas>')[0];
    //var joBody = $('body');
    //var joBlank = $('.sys-blank')
    var deviceManager = new classDeviceManager('videoinput');
    var camResolution = {x: null, y: null};
    var camFiles = null;
    var popup = null;

    btnTakePhoto.on('click', takePhoto);

    emlVideo.on('resize', function () {
        setTimeout(function () {
            camResolution.x = emlVideo.width();
            camResolution.y = camResolution.x * 3 / 5;
            emlVideo.css('width', '');
        }, 1);
    });

    btnOk.on('click', function () {
        hideGUI();
        me.pickCam.onDone && me.pickCam.onDone(camFiles);
    });

    btnCancel.on('click', function () {
        hideGUI();
        me.pickCam.onDone && me.pickCam.onDone(null);
    });

    me.pickCam = function (onDone) {
        showGUI();
        me.pickCam.onDone = onDone;
        camFiles = [];
        deviceManager.accessDevice(onGetCam);
    };

    function showGUI() {
        /*joBlank.backStyle =  joBlank[0].style.display;
        joBlank[0].style.display = 'block';
        joVideo.css('width', 'unset');
        joBody.append(joCamInput);*/

        if (!popup) {
            Component.Custom.Popup.init();
            popup = Component.Custom.Popup.items.comCamera.Popup;
            popup.on_Close = function () {
                btnCancel.click();
            }
        }

        popup.show();
        resizeCamInput();
    }

    function hideGUI() {
        deviceManager.releaseDevice();
        panelCamPreview.empty();
        emlVideo.css('width', '');
        emlVideo[0].src = '';
        popup.hide();
    }

    function onGetCam(res) {
        if (res.code == AMResponseCode.ok) {
            emlVideo[0].srcObject = res.data;
        } else {
            console.log('Không thể truy cập camera');
        }
    }

    function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);
        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], {type: mimeString});
    }

    function takePhoto() {
        $('body').append(canvas);
        canvas.setAttribute('width', camResolution.x);
        canvas.setAttribute('height', camResolution.y);

        var context = canvas.getContext('2d');
        context.drawImage(emlVideo[0], 0, 0, camResolution.x, camResolution.y, 0, 0, camResolution.x, camResolution.y);
        var data = canvas.toDataURL('image/png');
        var bold = dataURItoBlob(data);
        var file = new File([bold], "IMG" + (camFiles.length + 1) + ".png", {type: "image/png"});
        camFiles.push(file);

        var joThumbnail = $('<img/>');
        joThumbnail.attr('src', data);
        panelCamPreview.append(joThumbnail);
        $('body')[0].removeChild(canvas);
    }

    function resizeCamInput() {
        if (!camFiles)
            return;

        var popupContainer = palLiveStream.parent().parent();
        popupContainer[0].style.height = '';
        palLiveStream.height(palLiveStream.width() * 3 / 5);
        popupContainer.height(popupContainer.height());
    }

    function initLayout() {

        comPopupTitle.append('<button class="red close"><i class="fa fa-close"></i></button>');
        palLiveStream.append(emlVideo);
        joCamInput.find('.right').append(panelCamPreview);

        //var joFoot = joCamInput.find('.foot');
        //joFoot.find('.cam-ctrls').append(joTakePhoto);
        comPopupBottom.append(btnTakePhoto);
        comPopupBottom.append(btnOk);
        comPopupBottom.append(btnCancel);

        /*var joCloseCtrls = joFoot.find('.popup-ctrls');
        joCloseCtrls.append(joOk);
        joCloseCtrls.append(joCancel);*/
        comPopupContent.append(joCamInput);
        comPopup.append(comPopupTitle);
        comPopup.append(comPopupContent);
        comPopup.append(comPopupBottom);
        $('body').append(comPopup);
        $(window).on('resize', resizeCamInput);
    }

    initLayout();
}
