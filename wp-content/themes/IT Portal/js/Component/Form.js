Component.Custom = Component.Custom || {};
Component.Custom.Form = new (function () {
    var form = this;
    var formClass = function (_eml) {
        var me = this;
        var eml = _eml;
        var content = $(eml).find('.form-content');
        var formData = {};
        var fields = {};
        var defaultData = {};
        var action = $(eml).attr('action') || null;
        var method = $(eml).attr('method') || null;
        var ignoreList = [];

        if(action !== null){
            $(eml).find('button[type="ajax-submit"]').on('click', function () {
                if(APIData[action] === undefined){
                    console.log('%cForm action "'+action+'" is note defined','color: red');
                    return;
                }

                if(APIData[action][method] === undefined){
                    console.log('%cForm method "'+method+'" is note defined','color: red');
                    return;
                }
                var submitData = {};

                for(var name in formData){
                    if(ignoreList.indexOf(name) < 0){
                        submitData[name] = formData[name];
                    }
                }

                APIData[action][method](submitData, function (res) {
                    $(eml).trigger('ajaxsubmit', [res]);
                })
            });
        }

        this.Submit_OnDone = function () {};

        this.resetData = function () {
            me.val(defaultData);
        };

        var binding = function () {
            for (var name in formData) {
                if (fields[name] !== undefined) {
                    fields[name].setValue(formData[name]);
                }
            }
        };
        this.isValid = function () {
            var valid = true;
            for(var name in formData){
                var fieldValid = me.field(name).isValid();
                valid = valid && fieldValid;
            }
            return valid;
        };
        this.val = function (_val) {
            if (_val) {
                formData = _val;
                binding();
            } else {
                return formData;
            }
        };
        var items = $(eml).find('.form-content item');
        var template = {
            custom: function(config){
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<div value="" ></div>');
                field.setValue = function (_val) {
                    field.attr('value', _val);
                    formData[config.name] = _val;
                };
                field.isValid = function () {
                    return true;
                };

                field.html($(config.eml).html());

                return field;
            },
            hidden: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<input cbType="hidden" type="hidden" />');
                field.setValue = function (_val) {
                    field.val(_val);
                    formData[config.name] = $(config.eml).attr('default') || _val;
                };
                field.isValid = function () {
                    return true;
                };

                if($(config.eml).attr('default')){
                    field.val($(config.eml).attr('default'));
                    formData[config.name] = $(config.eml).attr('default');
                }

                return field;
            },
            text: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<input cbType="text" type="text" />');
                field.on('change', function () {
                    formData[config.name] = $(this).val();
                });
                if(config.currency){
                    field.on('keyup', function () {
                        this.value = this.value.replace(/,/g,'').formatMoney();
                        formData[config.name] = this.value.replace(/,/g,'');
                    });
                }
                field.setValue = function (_val) {
                    field.val(_val);
                };
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else if (field.val() !== '') {
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }
                };
                return field;
            },
            number: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<input cbType="text" type="number" />');
                field.on('change', function () {
                    formData[config.name] = this.value.replace(/[^0-9]/g,'');
                });

                /*if(config.currency){
                    field.on('keyup', function () {
                        this.value = this.value.replace(/,/g,'').formatMoney();
                        formData[config.name] = this.value.replace(/,/g,'');
                    });
                }*/

                field.setValue = function (_val) {
                    field.val(_val);
                };
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else if (field.val() !== '') {
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }
                };
                return field;
            },
            file: function (config) {
                formData[config.name] = [];
                defaultData[config.name] = [];
                var field = $('<label type="input-file"><input cbType="file" type="file" /></label>');
                var accept = config.eml.getAttribute('accept');
                var multiple = config.eml.getAttribute('multiple');
                var imagereview = config.eml.getAttribute('imagereview');

                if (accept && accept != '') {
                    field.find('input').attr('accept', accept);
                }
                if (multiple == '') {
                    field.find('input').attr('multiple', '');
                }
                if (imagereview == '') {
                    field.find('input').attr('imagereview', '');
                }
               //
                field.find('input').on('change', function () {
                    formData[config.name] = this.files;
                });
                field.setValue = function (_val) {
                    field[0]['data'] = _val.data;
                    field[0]['dataPath'] = _val.dataPath;
                    field[0].File.ShowPrevew();
                };
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }
                };
                return field;
            },
            ricktext: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<textarea cbType="rickText" ></textarea>');
                field.on('change', function () {
                    formData[config.name] = $(this).val();
                });

                field.setValue = function (_val) {
                    field.val(_val);
                };
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else if (field.val() !== '') {
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }

                };
                return field;
            },
            combobox: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<select cbType="combobox" component="combobox" ></select>');

                if (config.eml.hasAttribute('data')) {
                    var dataText = config.eml.getAttribute('data-text');
                    var dataValue = config.eml.getAttribute('data-value');
                    var option = $('<option data-bind="text: ' + dataText + ', attr:{ value: ' + dataValue + '}" ></option>');

                    field.append(option);
                }
                field.on('change', function () {
                    formData[config.name] = $(this).val();
                });
                field.setValue = function (_val) {
                    field[0].ComboBox.val(_val);
                };
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }
                };
                return field;
            },
            editor: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<textarea cbtype="editor" ></textarea>');
                field.on('change', function () {
                    formData[config.name] = tinymce.get(config.name).getContent();
                });
                field.attr('height', config.height);
                field.setValue = function (_val) {
                    field.val(_val);
                    tinymce.get(config.name).setContent(_val);
                };
                field[0].reset = function () {
                    tinymce.get(config.name).setContent('');
                };
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }
                };
                return field;
            },
            imagepicker: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<img width="120px" height="120px" component="imagepicker" />');
                field.attr('field', config.eml.getAttribute('field'));
                field.setValue = function (_val) {
                    if(_val !== null && _val !== undefined) {
                        field[0]['data'] = _val.data;
                        field[0]['dataPath'] = _val.dataPath;
                        formData[config.name] = _val.data;
                        field.trigger('update');
                    }
                };
                field.on('change', function () {
                    formData[config.name] = this.alt;
                });
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }
                };
                return field;
            },
            checkbox: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<input type="checkbox" component="checkbox" />');

                if(config.class){
                    field.addClass(config.class);
                }

                field.setValue = function (_val) {
                    this.checked = _val;
                    if(_val){
                        $(this).attr('checked', 'checked');
                    } else {
                        $(this).removeAttr('checked');
                    }

                };
                field.on('change', function () {
                    formData[config.name] = this.checked;
                });
                field.isValid = function () {
                    return true;
                };
                return field;
            },
            rate: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var field = $('<span component="rate"></span>');
                field.attr('max', $(config.eml).attr('max'));
                field.attr('symbol', $(config.eml).attr('symbol'));
                field.setValue = function (_val) {
                    $(this).attr('value',_val);
                };
                field.on('change', function () {
                    formData[config.name] = $(this).attr('value');
                });
                field.isValid = function () {
                    return true;
                };
                return field;
            },
            search: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var name = config.eml.getAttribute('name');
                var searchhandler = config.eml.getAttribute('searchhandler');
                var searchlimit = config.eml.getAttribute('searchlimit');
                var searchtemplate = config.eml.getAttribute('searchtemplate');
                var displaytext = config.eml.getAttribute('displaytext');
                var searchvalue = config.eml.getAttribute('searchvalue');
                var field = $('<input type="text" ' +
                    'class="VNGSearch form-control" ' +
                    'data-bind="VNGSearch: '+name+', ' +
                    'SearchQuery:'+searchhandler+', ' +
                    'SearchLimit:'+searchlimit+', ' +
                    'SeacrhTemplate:\''+searchtemplate+'\', ' +
                    'DisplayText:\''+displaytext+'\', ' +
                    'SeacrhValue:\''+searchvalue+'\'" />');
                field.setValue = function (_val) {
                    this.value = _val;
                };
                field.on('itemselected', function () {
                    formData[config.name] = this.VNGSearch.SelectedValue.value;
                });
                field.on('change', function () {
                    formData[config.name] = $(this).val();
                });
                field.on('focusout',function () {
                    formData[config.name] = $(this).val();
                });
                field.isValid = function () {
                    return true;
                };
                return field;
            },
            select: function (config) {
                formData[config.name] = '';
                defaultData[config.name] = '';
                var multiple = config.eml.getAttribute('multiple');
                var fieldTemplate = config.eml.innerHTML;
                var field = null;
                if(fieldTemplate !== ''){
                    field = $('<select>'+fieldTemplate+'</select>');
                } else {
                    field = $('<select></select>');
                }
                if(multiple !== null){
                    field.attr('multiple', '');
                }
                field.setValue = function (_val) {
                    $(this).val(_val);
                };
                field.on('change', function () {
                    formData[config.name] = $(this).val();
                });
                field.isValid = function () {
                    if(($(this).attr('require') === undefined)){
                        field.removeClass('invalid');
                        return true;
                    } else {
                        field.addClass('invalid');
                        return false;
                    }
                };
                return field;
            },
            'date-time': function(config){
                var field = $('<input type="datetime-local">');
                field.on('change', function(){
                    var val = field.val();
                    formData[config.name] = val ? val.replace('T', ' ') : null;
                });

                field.setValue = function (_val) {
                    if(_val){
                        if(_val.getTime){
                            var strVal = _val.getFullYear();
                            strVal +='-';
                            strVal += (_val.getMonth() + 1).toString().padStart(2, '0');
                            strVal +='-';
                            strVal += _val.getDate().toString().padStart(2, '0');
                            strVal +='T';
                            strVal += _val.getHours().toString().padStart(2, '0');
                            strVal +=':';
                            strVal += _val.getMinutes().toString().padStart(2, '0');

                            field.attr('value',strVal);
                            formData[config.name] = strVal ? strVal : null;
                        }else{
                            field.attr('value',_val.replace(' ', 'T'));
                        }
                    }else{
                        field.attr('value','');
                    }
                };

                field.isValid = function () {
                    return true;
                };

                return field;
            }
        };
        this.field = function (name) {
            return fields[name];
        };

        items.each(function (i, item) {
            var name = item.getAttribute('name');
            var text = item.getAttribute('text');
            var type = item.getAttribute('type');
            var placeHolder = item.getAttribute('placeholder');
            var data = item.getAttribute('data');
            var height = item.getAttribute('height');
            var width = item.getAttribute('width');
            var disabled = item.getAttribute('disabled');
            var _class = item.getAttribute('class');
            var require = item.getAttribute('require');

            if(item.getAttribute('ignore')){
                ignoreList.push(name);
            }

            var formItem = $('<div class="form-line"></div>');
            var fieldLabel = $('<span class="field-label"></span>');
            fieldLabel.text(i18n(text));

            if(width === '' || width === undefined || width === null){
                width = '100%';
            }

            var config = {
                eml: item
            };
            config.merge(Common.getAllAttr(item));
            var field = template[type](config);

            if (placeHolder && placeHolder != '') {
                field.attr('placeholder', i18n(placeHolder));
            }

            if (disabled != null || disabled != undefined) {
                field.attr('disabled', 'disabled');
            }

            if (require != undefined) {
                field.attr('require', require);
                fieldLabel.attr('require', require);
            }

            if (data && data != '') {
                field.attr('data-bind', data);
            }
            field.attr('name', name);
            formItem.css({width: width});
            formItem.append(fieldLabel);
            formItem.append(field);

            if(type === 'hidden'){
                formItem.addClass('hidden');
            }

            field.fieldType = type;
            $(item).after(formItem);
            item.remove();
            fields[name] = field;
        });
    };
    this.hasForm = false;
    this.isDone = false;
    this.get = function (name) {
        return $('[component="form"][name="' + name + '"]')[0].Form;
    };

    Object.defineProperty(form, 'items', { get: function() {
            var result = {};
            $('[component="form"]').each(function () {
                result[this.getAttribute('name')] = this;
            });
            return result;
        }});

    this.init = function () {
        var emls = $('[component="form"]');

        if (emls.length > 0) {
            this.hasForm = true;
        }
        emls.each(function () {

            this['Form'] = new formClass(this);
        });
        form.isDone = true;
    };

    $(document).ready(function () {

        form.init();
    });

})();