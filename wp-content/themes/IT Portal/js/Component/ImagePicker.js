Component.Custom = Component.Custom || {};
Component.Custom.ImagePicker = new(function () {
    var editor = this;

    var ImagePickerClass = function ImagePickerClass(_eml) {
        var me = this;
        var eml = $(_eml);
        eml.on('update', function () {
            eml[0].src = UploadUrl+'/component/'+eml[0]['dataPath']+'/'+eml[0]['data'];
            eml[0].alt = eml[0]['data'];
        });
        eml.on('click', function () {
           if(eml.attr('field') !== undefined){
               var form = eml.parent().parent().parent().parent()[0].Form;
               var imgField = form.field(eml.attr('field'));
               var imgs = form.field(eml.attr('field'))[0].files;

               var pickerPopup = $('[component="pickerpopup"]');

               if(pickerPopup.length === 0){
                   pickerPopup = $('<div component="pickerpopup" ></div>');
                   var btnClose = $('<span><i class="fa fa-close"></i></span>');
                   var imgList = $('<div class="img-list"></div>');
                   btnClose.on('click', function () {
                       pickerPopup.hide();
                   });
                   pickerPopup.append(btnClose);
                   pickerPopup.append(imgList);
                   $('body').append(pickerPopup);

               }
               pickerPopup.find('.img-list').html('');

               if(imgField.parent().find('.imagepreview').length > 0){
                   pickerPopup.find('.img-list').html((function () {
                       return imgField.parent().find('.imagepreview').html();
                   })());
               } else {
                   for (var i = 0; i < imgs.length; i++) {
                       var item = imgs[i];
                       var reader = new FileReader();
                       reader.onload = function (event) {
                           img = new Image();
                           img.onload = function () {
                               pickerPopup.find('.img-list').append(this);
                           };
                           img.src = event.target.result;
                           img.alt = item.name;
                       };
                       reader.readAsDataURL(item);
                   }
               }

               pickerPopup.find('.img-list').find('img').on('click', function () {

                   eml[0].src = this.src;
                   eml[0].alt = this.getAttribute('alt');
                   pickerPopup.hide();
                   eml.trigger('change');
               });

               pickerPopup.show();
           }
        });
    };

    var do_init = function () {
        $('[component="imagepicker"]').each(function () {
            this.ImagePicker = new ImagePickerClass(this);
        });
    };

    this.init = function () {

        if($('[component="form"]').length > 0){
            Common.wait(function () {
                return Component.Custom.Form.isDone;
            }, function(){
                do_init();
            });
        } else {
            do_init();
        }
    };

    $(document).ready(function () {
        editor.init();
    });
})();