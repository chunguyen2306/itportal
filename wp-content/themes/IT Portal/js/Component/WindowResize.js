$(document).ready(function () {
    Common.wait(function () {
        return window['alreadyBinding'];
    }, function () {
        if ($('body').height() < window.innerHeight) {
            var height = window.innerHeight - $('.header').height() - $('.footer').height() - $('.nav-wrapper').height();
            $('.content').css({minHeight: height});
        }
    });

});