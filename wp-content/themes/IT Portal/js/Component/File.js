Component.Custom = Component.Custom || {};
Component.Custom.File = new (function () {
    var file = this;

    var fileClass = function (_eml) {
        var me = this;
        var eml = _eml;
        var file = eml.querySelector('input');
        var label = null;
        var placeHolder = eml.getAttribute('placeholder');
        if (placeHolder) {
            label = $('<span></span>');
            label.text(placeHolder);
            eml.insertBefore(label[0], eml.childNodes[0]);
        }
        this.OnChange = function () {

        };
        var uploadContainer = $('<div></div>');
        label.append(uploadContainer);
        file.addEventListener('change', function () {

            var uploadFiles = this.files;
            var fileName = '';
            var errorMess = '';
            var acceptFiles = {};
            if (uploadFiles.length > 0) {
                for (var key = 0; key < uploadFiles.length; key++) {
                    var res = validFile(uploadFiles[key]);
                    if (res.State) {
                        acceptFiles[key] = uploadFiles[key];
                        fileName += uploadFiles[key].name + ', ';
                    } else {
                        fileName += uploadFiles[key].name + ', ';
                        errorMess += res.Mess + '</br>';
                    }
                }
            }
            if (errorMess !== '') {
                Component.System.alert.show(errorMess + 'Vui lòng xóa hoăc chọn file khác.');
            }
            label.text(fileName);
            me.OnChange();
            if (this.getAttribute('imagereview') !== undefined) {
                me.ShowPrevew(uploadFiles);
            }


        });
        this.uploadResult = [];
        var uploadCount = 0;
        this.hasFile = function () {
            return file.files.length > 0;
        };


        this.getFileContent = function (_cb) {
            if (me.hasFile()) {
                var reader = new FileReader();
                reader.onload = (function (_data) {
                    _cb(_data.target.result);
                });
                reader.readAsText(file.files[0]);
            }
        };

        this.Preview_OnClick = function () {

        };

        this.ShowPrevew = function (files) {
            var previewPanel = $(eml).parent().find('.imagepreview');
            if (previewPanel.length === 0) {
                previewPanel = $('<div class="imagepreview"></div>');
                previewPanel.insertAfter(eml);
            }
            previewPanel.html('');
            if (files !== undefined) {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];

                    var reader = new FileReader();
                    reader.fileName = file.name;
                    reader.onload = function (event) {
                        var img = new Image();
                        img.src = event.target.result;
                        img.alt = this.fileName;
                        img.addEventListener('click', function () {
                            me.Preview_OnClick(this.alt, this);
                        });
                        previewPanel.append(img);
                    };
                    reader.readAsDataURL(file);
                }
            }
            if (eml['data'] !== undefined) {
                var imgData = eml['data'];
                for (var i = 0; i < imgData.length; i++) {
                    var item = imgData[i];
                    var img = new Image();
                    img.src = UploadUrl + '/component/' + eml['dataPath'] + '/' + item;
                    img.alt = item;
                    img.addEventListener('click', function () {
                        me.Preview_OnClick(this.alt, this);
                    });
                    previewPanel.append(img);
                }
            }
        };
        this.isUploadDone = function () {
            return uploadCount === file.files.length;
        };

        var cb_upload = function (res) {
            uploadCount++;
            if (res.State) {
                me.uploadResult.push(res.Result);
            } else {

            }
        };

        var do_upload = function (path, prefix, isChangeName) {
            var files = file.files;
            var errorMess = '';
            for (var i = 0; i < files.length; i++) {
                var validateFile = validFile(files[i]);
                if (validateFile.State) {
                    APIData.UploadFile(path, prefix + '_' + i, files[i], isChangeName, cb_upload);
                } else {
                    errorMess += validateFile.Mess + '<br>';

                }

            }
        };
        this.Reset = function () {
            uploadCount = 0;
            me.uploadResult = [];
        };
        this.Clear = function () {
            delete file.files;
        };
        this.Upload = function (path, prefix, isChangeName) {
            me.Reset();
            isChangeName = isChangeName || false;
            do_upload(path, prefix, isChangeName);
        };
        var validFile = function (file) {
            var types = ['png', 'gif', 'jpeg', 'pjeg', 'pdf', 'doc', 'docx', 'xls', 'xlsx','jpg','bmp'];
            var response = {};
            var type = file.name.split('.').pop().toLowerCase();
            if (!types.includes(type)) {
                response.State = false;
                response.Mess = '<b>' + file.name + '</b>' + ' định dạng file không phù hơp.';
                return response;
            }
            if (file.size > 10440000) {
                response.State = false;
                response.Mess = '<b>' + file.name + '</b>' + ' vượt quá kích thước cho phép(10MB).';
                return response;
            }
            response.State = true;
            response.Mess = '';
            return response;
        }


    };


    var do_init = function () {
        var emls = $('label[type="input-file"]');
        emls.each(function () {
            this['File'] = new fileClass(this);
        });
    };

    this.init = function () {
        if (Component.Custom.Form.hasForm) {
            Common.wait(function () {
                return Component.Custom.Form.isDone;
            }, function () {
                do_init();
            });
        } else {
            do_init();
        }
    };

    Object.defineProperty(file, 'items', {
        get: function () {
            var result = {};
            $('label[type="input-file"]').each(function () {
                result[this.getAttribute('name')] = this.File;
            });
            return result;
        }
    });

    $(document).ready(function () {
        setTimeout(file.init, 100);
    });
})
();