Component.Custom = Component.Custom || {};
Component.Custom.Number = new(function () {
    var number = this;

    this.initEvent = function(){
        $('[component="number"]').each(function () {
            var parent = $(this).parent();
            var up = parent.find('.up');
            var down = parent.find('.down');

            down.unbind('click');
            up.unbind('click');

            down.on('click', function () {
                $(this).parent().find('[component="number"]')[0].stepDown();
                $(this).parent().find('[component="number"]').trigger('change');

            });
            up.on('click', function () {
                $(this).parent().find('[component="number"]')[0].stepUp();
                $(this).parent().find('[component="number"]').trigger('change');
            });
        });
    };

    this.init = function () {
        $('[component="number"]').each(function () {
            var eml = $(this);
            if(eml[0].Number === undefined) {
                eml[0].Number = true;
                var wrapper = $('<div class="com-wapper-number"></div>');
                var up = $('<a class="up"><i class="fa fa-chevron-up"></i></a>');
                var down = $('<a class="down"><i class="fa fa-chevron-down"></i></a>');
                eml.wrap(wrapper);

                down.insertBefore(eml);
                up.insertAfter(eml);

                down.unbind('click');
                up.unbind('click');

                down.on('click', function () {
                    $(this).parent().find('[component="number"]')[0].stepDown();
                    $(this).parent().find('[component="number"]').trigger('change');
                });
                up.on('click', function () {
                    $(this).parent().find('[component="number"]')[0].stepUp();
                    $(this).parent().find('[component="number"]').trigger('change');
                });
            }
        });
    };

    $(document).ready(function () {
        number.init();
    });

    /*Common.wait(function () {
        return window['alreadyBinding'];
    }, function(){
        number.init();
    });*/
})();