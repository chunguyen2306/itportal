Component.Custom = Component.Custom || {};
Component.Custom.ReadMorePanel = new(function () {
    var readmore = this;

    var readmoreClass = function (_eml) {
        var me = this;
        var eml = _eml;
        $(eml).height(300);
        var btnReadMore = $('<span class="btn-read-more active">Xem Thêm</span>');
        $(eml).append(btnReadMore);

        btnReadMore.on('click', function () {
            $(this).removeClass('active');
            $(this).addClass('off');
            $(eml).height('');
        });
    };

    this.init = function () {

        $('[component="readmore-panel"]').each(function () {
            var eml = $(this);
            if(eml[0].ReadMorePanel === undefined) {
                eml[0].ReadMorePanel = new readmoreClass(eml[0]);
            }
        });
    };

    $(document).ready(function () {
        readmore.init();
    });
})();