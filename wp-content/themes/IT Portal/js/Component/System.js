/**
 * Created by Administrator on 11/22/2016.
 */
Component.System = {
    loading: {
        show: function () {
            $('.loading').show();
            //Component.System.blank.show();
        },
        hide: function () {
            $('.loading').hide();
            //Component.System.blank.hide();
        }
    },
    blank: {
        show: function () {
            $('.sys-blank').show();
        },
        hide: function () {
            $('.sys-blank').hide();
        }
    },
    preLoadingPanel: {
        show: function () {
            $('.pre-load-panel').show();
        },
        hide: function () {
            $('.pre-load-panel').hide();
        }
    },
    productLoading: {
        show: function () {
            $('.product-loading').show();
        },
        hide: function () {
            $('.product-loading').hide();
        }
    },
    alert: new (function () {
        var me = this;
        var createAlert = function () {
            var eml = $('<div class="sys-alert">' +
                '<div class="alert-content"></div>' +
                '<div class="alert-control"></div>' +
                '</div>');

            $('body').append(eml);
            console.log(eml);
            return eml;
        };

        this.show = function (mess) {
            var eml = $('.sys-alert');
            if (eml.length <= 0) {
                eml = createAlert();
            }
            var content = eml.find('.alert-content');
            content.empty();
            content.append(mess);

            var joAlertControl = eml.find('.alert-control');
            joAlertControl.empty();

            if(arguments.length == 1){
                var jo = $("<button />");
                jo.text("Đóng");
                jo.unbind('click');
                jo.on('click', me.hide);
                joAlertControl.append(jo);
            }else{
                for(var i = 1; i < arguments.length; i++){
                    var btn  = arguments[i];
                    var jo = $("<button />");
                    jo.html(btn.name);
                    jo.unbind('click');
                    jo.on('click', me.hide);
                    jo.on('click', btn.cb);

                    if(i > 1)
                        jo.addClass("red");
                    joAlertControl.append(jo);
                }
            }


            eml.addClass('show');
            Component.System.blank.show();
        };
        this.hide = function () {
            var eml = $('.sys-alert');
            eml.removeClass('show');

            if($('[component="popup"].show').length === 0){
                Component.System.blank.hide();
            }
        };
        this.failed = function () {
            this.show("Thất bại", {
                name: "Đóng",
                cd: function () { }
            });
        };
        this.success = function () {
            this.show("Thành công", {
                name: "Đóng",
                cd: function () { }
            });
        }
    })()
};