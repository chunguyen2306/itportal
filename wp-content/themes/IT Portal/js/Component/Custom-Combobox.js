/**
 * Created by tientm2 on 9/20/2017.
 */
Component.Custom = Component.Custom || {};
Component.Custom.ComboBox = new(function () {
    var combobox = this;

    var combobxClass = function (_eml) {
        var me = this;
        var eml = _eml;
        var template = '<div class="wrapper">';
        template +=    '<span class="trigger">'+ eml.attr("placeholder") +'</span>';
        template +=    '<div class="options">';
        template +=    '</div>';
        template +=    '</div>';

        eml.wrap('<div class="component-combobox"></div>');
        eml.hide();
        eml.after(template);

        this.val = function (_val) {
            var option = eml.parent().find('.options span[value="'+_val+'"]');
            var options = $(eml.find('option'));
            options.removeAttr('selected');
            option.attr('selected','selected');
            option.selected = true;
            $(eml.parent().find('.trigger')).html(option.html());
            return $(eml).val();
        };

        eml.parent().find('.trigger').on('click', function () {
            if(eml.parent().hasClass('opened')){
                eml.parent().removeClass('opened');
            } else {
                eml.parent().addClass('opened');
            }
            event.stopPropagation();
        });

        window.addEventListener('click', function (e) {
            if(e.toElement === eml[0]){
                return;
            }
            if(e.toElement.offsetParent !== eml.parent().find('.wrapper .options')[0]){
                if(eml.parent().hasClass('opened')) {
                    eml.parent().removeClass('opened');
                }
            }
        });

        this.bindingItem = function(){
            var options =  eml.parent().find('.options');
            options.html('');
            var isDefaulValue = false;
            if(eml.val() !== ''){
                isDefaulValue = true;
            }
            eml.find('option').each(function (index) {
                var item = $('<span class="'+ $(this)[0].className +'" index="'+ index +'" value="'+$(this).attr('value')+'">'+$(this).html()+'</span>');
                if($(this).attr('value') === eml.val().toString()){
                    $(this).attr('selected','selected');
                    this.selected = true;
                    $(eml.parent().find('.trigger')).html($(this).html());
                }
                item.unbind('click');
                item.on('click', function () {
                    var index = $(this).attr('index');

                    var options = $(eml.find('option'));
                    options.removeAttr('selected');
                    $(options[index]).attr('selected','selected');
                    options[index].selected = true;
                    $(eml.parent().find('.trigger')).html($(this).html());
                    eml.parent().removeClass('opened');

                    $(eml).trigger('change');
                    $(eml).val($(options[index]).val());
                    $(eml).change();

                });
                options.append(item);
            });

        };
    };

    this.bindingItem = function () {
        var emls = $('[component="combobox"]');
        emls.each(function () {
            if(this.ComboBox) {
                this.ComboBox.bindingItem();
            }
        });
    };

    this.init = function init() {
        var emls = $('[component="combobox"]');
        emls.each(function () {
            var eml = $(this);
            if(eml[0].ComboBox === undefined){
                eml[0].ComboBox = new combobxClass(eml);
                eml[0].ComboBox.bindingItem();
            }

        });
    };

    var do_init = function () {
        combobox.init();
        combobox.bindingItem();
    };

    Common.wait(function () {
        return window['alreadyBinding'];
    }, function(){
        if($('[component="form"]').length > 0){
            Common.wait(function () {
                return Component.Custom.Form.isDone;
            }, function(){
                do_init();
            });
        } else {
            do_init();
        }
    });



})();