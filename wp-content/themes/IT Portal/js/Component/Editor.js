Component.Custom = Component.Custom || {};
Component.Custom.Editor = new(function () {
    var editor = this;

    var do_init = function () {
        $('[cbtype="editor"]').each(function (){
            var name = $(this).attr('name');
            var height = $(this).attr('height');
            var thisEml = $(this);
            tinymce.init({
                selector: '[name="'+name+'"]',
                height: height,
                menubar: true,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code help'
                ],
                toolbar: 'insert | fontselect  fontsizeselect bold italic backcolor forecolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | formatselect | link | undo redo | help ',
                fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 21pt 22pt 25pt 24pt 26pt 28pt 36pt",
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css'],
                init_instance_callback: function (editor) {
                    editor.on('Change', function (e) {
                        thisEml.trigger('change');
                    });
                }
            });
        });

    };

    this.init = function () {

        if($('[component="form"]').length > 0){
            Common.wait(function () {
                return Component.Custom.Form.isDone;
            }, function(){
                do_init();
            });
        } else {
            do_init();
        }
    };

    $(document).ready(function () {
        editor.init();
    });
})();