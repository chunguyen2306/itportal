/**
 * Created by tientm2 on 11/21/2017.
 */
Component.Custom = Component.Custom || {};
Component.Custom.Rate = new (function () {
    var rate = this;

    $(document).ready(function () {
        $('[component="rate"]').each(function () {
            var eml = $(this);
            var max = eml.attr('max');
            var symbol = eml.attr('symbol');
            var lblDisplay = $('<span class="display">0</span>');
            var displayName = function (rate) {
                switch (rate){
                    case 0.5:
                        return 'Siêu tệ';
                        break;
                    case 1:
                    case 1.5:
                        return 'Tệ';
                        break;
                    case 2:
                    case 2.5:
                        return 'Tạm được';
                        break;
                    case 3:
                    case 3.5:
                        return 'Được';
                        break;
                    case 4:
                    case 4.5:
                        return 'Tốt';
                        break;
                    case 5:
                        return 'Tuyệt vời';
                        break;
                }
            };


            eml[0].reset = function() {
                $('.rate-item').removeClass('checked');
                lblDisplay.text(0);
            };

            for(var i = max*2; i >0 ; i--){

                var rateItem = $('<span></span>');
                rateItem.addClass('rate-item');
                rateItem.addClass(symbol);
                rateItem.attr('value', ((i)/2));
                eml.append(rateItem);

                if(eml.attr('value') !== undefined && eml.attr('value') !== ''){
                    var value = parseFloat(eml.attr('value'));
                    var valueIndex = Math.ceil(value/0.5);
                    if(valueIndex === i){
                        rateItem.addClass('checked');
                        lblDisplay.text(value.toFixed(1)+' '+displayName((i)/2));
                    }
                } else {
                    rateItem.on('click', function () {
                        var val = parseFloat($(this).attr('value'));
                        eml.find('.rate-item').removeClass('checked');
                        $(this).addClass('checked');
                        lblDisplay.text(val+' '+displayName(val));
                        eml.attr('value', $(this).attr('value'));
                        eml.trigger('change');
                    });
                    rateItem.on('mouseenter', function () {
                        var val = parseFloat($(this).attr('value'));
                        lblDisplay.text(val+' '+displayName(val));
                    });
                    rateItem.on('mouseout', function () {
                        if($('.rate-item.checked').length === 0) {
                            lblDisplay.text('0');
                        }
                    });
                }
            }
            eml.append(lblDisplay);
        });
    });
})();