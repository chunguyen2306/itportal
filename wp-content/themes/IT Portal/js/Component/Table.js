Component.Custom = Component.Custom || {};
Component.Custom.Table = new(function () {
    var table = this;

    var TableClass = function TableClass(_eml) {
        var me = this;
        var eml = _eml;
        var data = [];
        var table = null;
        var editable = false;
        var isaddnew = false;

        var colEmls = $(_eml).find('column');
        var cols = [];
        for(var i = 0; i < colEmls.length; i++){
            var colEml = colEmls[i];
            var col = {
                Name: colEml.getAttribute('name'),
                Width: colEml.getAttribute('width'),
                DataType: colEml.getAttribute('type')
            };
            cols.push(col);
        }

        var getTableHeader = function () {
            var thead = $('<thead></thead>');
            var tr = $('<tr></tr>');
            for(var i = 0; i < cols.length; i++){
                var col = $('<th></th>');
                col.text(cols[i].Name);
                tr.append(col);
            }
            thead.append(tr);
            return thead;
        };

        var insertAddNewRow = function () {

        };

        var renderData = function(){
          if(editable){

          } else {

          }
        };

        this.render = function () {
            table = $('<table class="datatable"></table>');

            editable = table.atrr('editable');
            isaddnew = table.atrr('isaddnew');

            if(isaddnew === 'true'){
                insertAddNewRow();
            }

            table.append(getTableHeader());
            table.insertAfter(eml);
        };

        me.render();
    };

    this.init = function(){
        var emls = $('[component="table"]');
        emls.each(function(){
            this['Table'] = new TableClass($(this));
        });
    };

    _prop('items', function () {
        var result = {};
        $('[component="table"]').each(function () {
            result[this.getAttribute('name')] = this;
        });
        return result;
    });

    $(document).ready(function () {
        table.init();
    });

})();