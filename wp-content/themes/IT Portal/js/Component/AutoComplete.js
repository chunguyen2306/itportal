Component.Custom = Component.Custom || {};
Component.Custom.AutoComplete = new (function () {
    var autoComplete = this;

    var autoCompleteEml = $('<div></div>');
    autoCompleteEml.attr('component', 'autocomplete');

    $(document).ready(function () {
        $('body').append(autoCompleteEml);
    });


    var autoCompleteClass = function (_eml, _data) {
        var me = this;
        var eml = $(_eml);
        var data = _data;
        var currentSelect = 0;

        eml.attr('autocomplete', 'off');
        eml.on('keyup', function (e) {
            autoCompleteEml.html('');
            var value = this.value;
            for (var i = 0; i < data.length; i++) {
                if (data[i].substr(0, value.length).toUpperCase() == value.toUpperCase()) {

                    var item = $('<div></div>');
                    item.addClass('autocomplete-item');
                    item.html("<b>{0}</b>{1}".format(data[i].substr(0, value.length), data[i].substr(value.length)));
                    item.on('mouseover', function () {
                        eml.val(this.innerText);
                    });
                    item.on('click', function () {
                        me.close();
                    });
                    autoCompleteEml.append(item);
                }
            }
            me.show();
        });

        eml.on('keydown', function (e) {
            if (e.keyCode == 40) { //KEY DOWN
                currentSelect++;
            } else if (e.keyCode == 38) { //KEY UP
                currentSelect--;
            } else if (e.keyCode == 13) { //KEY ENTER
                $(autoCompleteEml).find("autocomplete-item:nth-child({0})".format(currentSelect)).click();
            }

            if (currentSelect >= data.length) {
                currentSelect--;
            } else if (currentSelect < 0) {
                currentSelect = 0;
            }
        });

        this.close = function () {
            autoCompleteEml.html('');
            autoCompleteEml.hide();
        };

        this.show = function (){
            autoCompleteEml.css({
                top: eml.offset().top + eml.height(),
                left: eml.offset().left,
                width: eml.width(),
            });
            autoCompleteEml.show();
        };
    };

    this.initTo = function (eml, data) {
        $(eml)[0].AutoComplete = new autoCompleteClass(eml, data);
    }
})();