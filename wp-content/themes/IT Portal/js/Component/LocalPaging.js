Component.Custom = Component.Custom || {};
Component.Custom.LocalPaging = new (function () {
  var localPaging = this;

  var localPagingClass = function (_eml) {
    var me = this;
    var eml = _eml;

    var pageCount = 0;
    var currentPage = 1;
    var pageItemLimit = 5;
    this.pageCount = function (_val) {
      if (_val) {
        pageCount = _val;
        on_localPaging();
      } else {
        return pageCount;
      }
    };

    this.currentPage = function (_val) {
      if (_val) {
        currentPage = _val;
      } else {
        return currentPage;
      }
    };

    this.pageItemCount = function (_val) {
      if (_val) {
        pageItemLimit = _val;
      } else {
        return pageItemLimit;
      }
    };

    this.changeActivePageToPrevious = function() {
      let activeChild = $(eml).find('span.active');
      let newActiveChild = $(eml.find('span[id=' + (currentPage - 1) + ']'));
      if(newActiveChild) {
        currentPage--;
        activeChild.removeClass('active');
        newActiveChild.addClass('active');
        do_PageChange(newActiveChild);
        me.on_PageChange(me);
      }
    }

    this.on_PageChange = function () { /* runtime implement */
    };

    var do_PageChange = function (pageItem) {
      pageItem = $(pageItem);
      pageItem.addClass('active');
      if (pageCount <= pageItemLimit) {
        return;
      }
      if (pageItem.hasClass('next')) {
        if (currentPage + 1 <= pageCount) {
          currentPage += 1;
        }
      } else if (pageItem.hasClass('previous')) {
        if (currentPage - 1 >= 1) {
          currentPage -= 1;
        }
      } else {
        currentPage = parseInt(pageItem.text());
      }

      changePage();
    };

    var changePage = function () {
      var start, end, intervalItem;
      rangeIndex = Math.floor(currentPage / pageItemLimit);

      intervalItem = parseInt(pageItemLimit / 2);
      start = currentPage - intervalItem;
      end = currentPage + intervalItem;
      if (start < 1) {
        start = 1;
        end = start + pageItemLimit - 1;
      } else if (end > pageCount) {
        end = pageCount;
        start = end - pageItemLimit + 1;
      }
      me.render_pageItem(start, end);
    };

    var pageItem_onClick = function () {
      eml.find('span.active').removeClass('active');
      currentPage = parseInt($(this).text());
      $(this).addClass("active");
      do_PageChange(this);
      me.on_PageChange(me);
    };

    var directorItem_onClick = function () {
      eml.find('span.active').removeClass('active');
      do_PageChange(this);
      me.on_PageChange(me);
    };

    var create_page_item = function (text, isItem) {
      var pageItem = $('<span id=' + text + ' data-bind="click: localPagingChangeHandler" ></span>');
      pageItem.text(text);

      if (isItem) {
        pageItem.on('click', pageItem_onClick);
      } else {
        pageItem.on('click', directorItem_onClick);
      }
      return pageItem;
    };

    this.render_pageItem = function (start, end) {
      eml.html('');
      if (end > pageCount) {
        end = pageCount;
      }
      if (pageCount > pageItemLimit) {
        var previousPageRange = create_page_item(' ');
        previousPageRange.addClass('previous fa fa-angle-left');
        eml.append(previousPageRange);
      }
      if (pageCount > 1) {
        for (var i = start; i <= end; i++) {
          if (i <= 0) {
            continue;
          }
          var pageItem = create_page_item(i, true);
          if (i === currentPage) {
            pageItem.addClass('active');
          }
          eml.append(pageItem);
        }
      }
      if (pageCount > pageItemLimit) {
        var nextPageRange = create_page_item(' ');
        nextPageRange.addClass('next fa fa-angle-right');
        eml.append(nextPageRange);
      }
    };

    this.render = function (_currentPage, _pageCount, _pageLimit) {
      pageCount = _pageCount;
      currentPage = _currentPage;
      pageItemLimit = _pageLimit;

      if (pageCount >= pageItemLimit) {
        if (currentPage !== 1) {
          changePage();
        } else {
          me.render_pageItem(1, pageItemLimit);
        }
      } else {
        me.render_pageItem(1, pageCount);
      }
    }
  };

  this.__defineGetter__('items', function () {
    var emls = $('[component="local-paging"]');
    var result = {};
    emls.each(function () {
      result[this.getAttribute('name')] = this['LocalPaging'];
    });

    return result;
  });

  this.init = function () {
    var emls = $('[component="local-paging"]');
    emls.each(function () {
      this['LocalPaging'] = new localPagingClass($(this));
    });
  };
})();