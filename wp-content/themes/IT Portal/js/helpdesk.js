var txtKeyword = $('#txtKeyword');
var content = $('.content');
var comPopup = $('.popup');

var ViewModel = new (function(){
    var me = this;
    this.txbAccountKeyword = ko.observable('');
    this.txbGroupName = ko.observable('');
    this.userInfo = ko.observable({});
    this.selectedLicense = ko.observableArray([]);
    this.domains = ko.observableArray([]);
    this.memberOf = ko.observableArray([]);
    this.licenseCheckeds = ko.observableArray([]);
    this.searchGroups = ko.observableArray([]);
    this.groupDetail = ko.observable({});
    this.txbGroupOwnerName = ko.observable('');
    this.licenses = ko.observableArray([]);
    this.refresh = function () {
        var self = ViewModel;
    };
    var isLincese = function(name){
        if(me.userInfo().memberOf){
            if(ViewModel.licenseCheckeds().indexOf(name) > 0){
                return true;
            }
            for(var i = 0; i < me.userInfo().memberOf.length; i++){
                var group = me.userInfo().memberOf[i].displayName;
                var displayName = name.replace('License_', '')
                displayName = displayName.replace(/_/g,' ');
                group = group.replace('License - ', '');
                if(displayName === group){
                    me.licenseCheckeds().push(name);
                    return true;
                }
            }
        }
        return false;
    }

    APIData.OnError = function(err){
        Loadding.show(false);
        Alert.error('Request server error!');
    }

    this.createMailInfo = ko.observable({
        accountEnabled: false,
        displayName: '',
        mailNickname: '',
        userPrincipalName: '',
        owner: ''
    });
    var resetMailInfo = function(){
        me.createMailInfo({
            accountEnabled: false,
            displayName: '',
            mailNickname: '',
            userPrincipalName: '',
            owner: ''
        });
    }

    var cb_getUserMemberOf = function(res){
        res = JSON.parse(res);
        me.licenseCheckeds([]);
        if(res.errors || res.error){
            Loadding.show(false);
        } else {
            ViewModel.userInfo().memberOf = res.result;
            ViewModel.userInfo(ViewModel.userInfo());
            showAccountInfo();

            for(var i = 0; i < me.userInfo().memberOf.length; i++){
                var group = me.userInfo().memberOf[i].displayName.replace('License - ', 'License_');
                group = group.replace(/ /g, '_');
                if(me.licenses().indexOf(group) >= 0) {
                    me.licenseCheckeds().push(group);
                }
            }

            me.licenseCheckeds(me.licenseCheckeds());

            Loadding.show(false);
        }
    }

    var cb_getUserManager = function(res){
        res = JSON.parse(res);
        if(res.errors || res.error){
            Alert.error("Không thể lấy thông tin Manager");
            ViewModel.userInfo().manager = '';
            ViewModel.userInfo().managerEMail = '';
        } else {
            ViewModel.userInfo().manager = res.result.onPremisesSamAccountName;
            ViewModel.userInfo().managerEMail = res.result.mail;
        }
        ViewModel.userInfo(ViewModel.userInfo());
        APIData.HelpdeskConsole.User.get_user_member_of(me.txbAccountKeyword(),cb_getUserMemberOf);
    }

    var cb_getUserInfo = function(res){

        res = JSON.parse(res);
        if(res.errors || res.error){
            Loadding.show(false);
        } else {
            ViewModel.userInfo(res.result);
            APIData.HelpdeskConsole.User.get_user_manager(me.txbAccountKeyword(),cb_getUserManager);
        }
    }

    this.do_changeManager = function () {
        Loadding.show(true);
        APIData.HelpdeskConsole.User.update_service_mail_owner(me.userInfo().mail, me.userInfo().manager,cb_changeManager);
    }
    
    var cb_changeManager = function (res) {
        Loadding.show(false);
        res = JSON.parse(res);
        if(res.errors || res.error){
            Alert.error("Thay đổi thất bại");
        } else {
            Alert.succ("Thay đổi thành công");
        }
    }
    
    this.do_changeUserInfo = function(){
        Loadding.show(true);

        var updateData = {};

        if(me.userInfo().city !== '' && me.userInfo().city !== null){ updateData.city=me.userInfo().city; }
        if(me.userInfo().country !== '' && me.userInfo().country !== null){updateData.country=me.userInfo().country; }
        if(me.userInfo().displayName !== '' && me.userInfo().displayName !== null){updateData.displayName=me.userInfo().displayName; }
        if(me.userInfo().givenName !== '' && me.userInfo().givenName !== null){updateData.givenName=me.userInfo().givenName; }
        if(me.userInfo().jobTitle !== '' && me.userInfo().jobTitle !== null){updateData.jobTitle=me.userInfo().jobTitle; }
        if(me.userInfo().mobilePhone !== '' && me.userInfo().mobilePhone !== null){updateData.mobilePhone=me.userInfo().mobilePhone; }
        if(me.userInfo().officeLocation !== '' && me.userInfo().officeLocation !== null){updateData.officeLocation=me.userInfo().officeLocation; }
        if(me.userInfo().streetAddress !== '' && me.userInfo().streetAddress !== null){updateData.streetAddress=me.userInfo().streetAddress; }
        if(me.userInfo().surname !== '' && me.userInfo().surname !== null){updateData.surname=me.userInfo().surname; }


        APIData.HelpdeskConsole.User.update_user(me.userInfo().mail, updateData, cb_changeUserInfo);
    }

    var cb_changeUserInfo = function(res){
        Loadding.show(false);
        res = JSON.parse(res);
        if(res.errors !== null && res.errors !== undefined){
            Alert.error("Thay đổi thất bại");
        } else {
            Alert.succ("Thay đổi thành công");
        }
    }

    var fn_getUserInformation = function(){
        Loadding.show(true);
        APIData.HelpdeskConsole.User.get_user(me.txbAccountKeyword(),cb_getUserInfo);
    }

    this.do_getUserInformation = function(model, e){

        if(e.key === 'Enter'){
            $( e.target ).blur();
            fn_getUserInformation();
        } else {
            return true;
        }
    }

    var cb_createServiceMail = function(res){
        res = JSON.parse(res);
        Loadding.show(false);
        if(res.error){
            Alert.error('Tạo mail service thất bại');
        } else if(res.errors){
            var errorsMess = 'Tạo mail service thất bại<br><br>';
            for(var i =0; i < res.errors.length; i++){
                errorsMess+=res.errors[i]+'<br>';
            }

            Alert.error(errorsMess);
        } else {
            Alert.succ('Tạo mail service thành công');
        }

    }

    this.do_createServiceMail = function(){
        Loadding.show(true);
        var info = me.createMailInfo();

        APIData.HelpdeskConsole.User.create_service_mail((info.accountEnabled)?"1":"0",
            info.displayName,
            info.mailNickname,
            info.userPrincipalName,
            info.owner,
            cb_createServiceMail);
    }

    var cb_EnabeDisableAccount = function(res){
        Loadding.show(false);
        res = JSON.parse(res);
        if(res.error){
            Alert.error('Thao tác thất bại<br><br>'+res.error);
        } else if (res.errors){
            Alert.error('Thao tác thất bại<br><br>'+res.errors.join('<br>'));
        } else {
            Alert.succ('Thao tác thành công');
        }
    }

    this.do_EnableDiableAccount = function(){
        Loadding.show(true);

        APIData.HelpdeskConsole.User.update_account_status(me.userInfo().mail, (me.userInfo().accountEnabled)?'0':'1', cb_EnabeDisableAccount);
    }

    this.do_addToGroup = function(){

    }

    this.do_viewManager = function(){
        me.txbAccountKeyword(me.userInfo().managerEMail);
        fn_getUserInformation();
    }

    var cb_changeUserLicense = function(res){
        res = JSON.parse(res);
        Loadding.show(false);
        res = JSON.parse(res);
        if(res.error){
            Alert.error('Thao tác thất bại<br><br>'+res.error);
        } else if (res.errors){
            Alert.error('Thao tác thất bại<br><br>'+res.errors.join('<br>'));
        } else {
            Alert.succ('Thao tác thành công');
        }
    }

    this.do_changeUserLicense = function(){
        Loadding.show(true);
        APIData.HelpdeskConsole.User.change_user_license(me.txbAccountKeyword, me.licenseCheckeds(), cb_changeUserLicense)
    }

    var cb_ResetUserPassword = function(res){
        res = JSON.parse(res);
        Loadding.show(false);
        if(res.error !== null && res.error !== undefined){
            Alert.error('Reset mật khẩu thất bại.<br>'+res.error);
        } else {
            Alert.succ('Reset mật khẩu thành công.');
        }
    }

    this.do_ResetUserPassword = function(){
        Loadding.show(true);
        APIData.HelpdeskConsole.User.reset_user_password(me.txbAccountKeyword(), cb_ResetUserPassword)
    }

    var cb_searchGroup = function(res){
        Loadding.show(false);
        res = JSON.parse(res);
        me.searchGroups(res.result);
        $('.popGroupManager-list').show();
        $('.popGroupManager-content').hide();
    }

    var fn_searchGroup = function(){
        Loadding.show(true);
        APIData.HelpdeskConsole.Group.get_group_by_name(me.txbGroupName(), cb_searchGroup);
    }

    this.do_searchGroup = function(model, e){
        if(e.key === 'Enter'){
            $( e.target ).blur();
            fn_searchGroup();
        } else {
            return true;
        }
    }

    var cb_getGroupOwner = function(res){
        res = JSON.parse(res);
        me.groupDetail().owner = res.result;
        me.groupDetail(me.groupDetail());

        $('.popGroupManager-list').hide();
        $('.popGroupManager-content').show();
    }

    var cb_viewGroupDetails = function(res){
        Loadding.show(false);
        res = JSON.parse(res);
        me.groupDetail(res.result);

        APIData.HelpdeskConsole.Group.get_group_owner(res.result.id, cb_getGroupOwner);
    }

    var fn_viewGroupDetail = function(id){
        Loadding.show(true);
        $('#popGroupManager').show();
        $('.blank').show();
        APIData.HelpdeskConsole.Group.get_group_by_id(id, cb_viewGroupDetails);
    }

    this.do_viewGroupDetails = function(model, e){
        fn_viewGroupDetail(model.id);
    }

    var cd_updateGroupOwner = function(res){
        res = JSON.parse(res);
        if(res.error){
            Alert.error('Thao tác thất bại<br><br>'+res.error);
        } else if (res.errors){
            Alert.error('Thao tác thất bại<br><br>'+res.errors.join('<br>'));
        } else {
            Alert.succ('Thao tác thành công');
        }
        fn_viewGroupDetail(me.groupDetail().id);
    }

    this.do_updateGroupOwner = function(){
        APIData.HelpdeskConsole.Group.update_group_owner(me.groupDetail().id, me.txbGroupOwnerName(),cd_updateGroupOwner);
    }

    var cb_updateGroup = function(res){
        if(res.error){
            Alert.error('Thao tác thất bại<br><br>'+res.error);
        } else if(res.errors) {
            Alert.error('Thao tác thất bại<br><br>'+res.errors.join('<br>'));
        }else {
            Alert.succ('Thao tác thành công');
        }
    }

    this.do_updateGroup = function(){

        var data = me.groupDetail();
        delete data.createdDateTime;
        delete data.owner;
        delete data.mail;
        delete data.onPremisesLastSyncDateTime;
        delete data.groupTypes;
        delete data.onPremisesSyncEnabled;

        APIData.HelpdeskConsole.Group.update_group(me.groupDetail().id, data,cb_updateGroup);
    }

})();

var showAccountInfo = function(){
    content.show();
}

var tabComponents = $('*[comtype="TabContainer"]');

tabComponents.each(function(tabIndex, tab){
    var tab = $(tab);
    var tabHeaders = tab.find('tabs a');
    var tabContents = $(tab.find('tabcontents>div'));

    tabHeaders.each(function(headerIndex, header){
        header = $(header);
        var contentID = header.attr('tab');
        var isDefault = header.attr('default');

        header.on('click', function(){
            tabContents.hide();
            tabHeaders.removeClass('active');
            header.addClass('active');
            $('#'+contentID).show();
        });

        if(isDefault === undefined){
            $('#'+contentID).hide();
        } else {
            header.addClass('active');
        }
    });
});

var popups = $('button[popup]');
var domains = $('input[domain]');
var comboboxItems = $('ul[comboboxitem]');
$('.popup .header a').on('click', function(){
    comPopup.hide();
    $('.blank').hide();
});

Alert = new (function(){

    var eml = $('.alert');
    var me = this;
    var showAlert = function(mess, _class){
        eml.html(mess);
        eml.attr('class', 'alert '+_class);
        eml.show();

        setTimeout(function(){
            eml.hide();
        }, 4000);
    }

    this.info = function(mess){
        showAlert(mess, 'info');
    }

    this.error = function(mess){
        showAlert(mess, 'error');
    }

    this.succ = function(mess){
        showAlert(mess, 'success');
    }

})();

Loadding = new (function(){

    var eml = $('.loadding');
    var me = this;
    this.show = function(isShow){
        if(isShow){
            eml.show();
        } else {
            eml.hide();
        }
    }

})();

function ComboboxItem(_eml) {
    var eml = _eml;
    var currentIndex = 0;
    var forEml = null;
    var me = this;
    this.selectedValue = '';

    eml.children().each(function(index, li){
        li = $(li);
        li.on('click', function () {
            currentIndex = index;
            select();
            me.close();
            $(forEml).change();
        });
    });

    this.open = function (_forEml) {
        forEml = _forEml;

        var top = forEml.offset().top + forEml[0].offsetHeight;
        var left = forEml.offset().left;
        eml.css({
            top: top,
            left: left,
            width: forEml[0].offsetWidth
        });

        eml.show();
    };

    this.keyPress = function (key) {
        var regex = new RegExp("([@].*)","g");
        var text = $(forEml).val();
        var matchs = regex.exec(text);
        if(matchs !== null) {
            var match = matchs[0].substring(1);
            for (var i = 0; i < eml.children().length; i++) {
                var item = $(eml.children()[i]);
                if (item.text().indexOf(match) < 0) {
                    item.hide();
                } else {
                    item.show();
                }
            }
        } else {
            $(eml.find('li')).show();
        };
    };

    this.OnChange = function(){};

    this.close = function () {
        forEml = null;
        $(eml.find('li')).show();
        eml.hide();
    };

    var select = function(){

        var items = $(eml.children()[currentIndex]);
        var text = $(forEml).val();
        var regex = new RegExp("([@].*)","g");

        text = text.replace(regex, '@'+items.text());
        $(forEml).val(text);
        me.selectedValue = text;
        me.OnChange();
    };
}

ko.bindingHandlers.domain = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {

        var value = valueAccessor();
        var unwrapValue = ko.unwrap(value);
        if(unwrapValue !== undefined && unwrapValue !== null && element.value === ''){
            element.value = unwrapValue;
        }

        var change = function(e) {

            var displayObjectStr = valueAccessor.toString().replace(/^function \(\)\{return /g, '').replace(/ \}/g, '');

            var key = e.key || e.char;
            var combobox = $('ul[comboboxitem][name="' + $(element).attr('combobox') + '"]')[0].combobox;
            if (key === '@') {
                combobox.open($(this));
                combobox.OnChange = function () {

                    if (typeof value !== 'function') {
                        //value = combobox.selectedValue;
                        eval('bindingContext.$data.' + displayObjectStr + ' = "' + combobox.selectedValue + '"');
                    } else {
                        value(combobox.selectedValue);
                    }
                };
            }

            if (typeof value !== 'function') {
                //value = combobox.selectedValue;
                eval('bindingContext.$data.' + displayObjectStr + ' = "' + element.value + '"');
            } else {
                value(element.value);
            }

            combobox.keyPress(key);
        };

        $(element).on('keyup', change);
        //$(element).on('change', change);
    },
    update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        var value = valueAccessor();

        var unwrapValue = ko.unwrap(value);

        element.value = unwrapValue;

        /*if(unwrapValue !== undefined && unwrapValue !== null && (element.value === '' || element.value != unwrapValue)){

        }

        var displayObjectStr = valueAccessor.toString().replace(/^function \(\)\{return /g, '').replace(/ \}/g, '');
        //console.log(valueAccessor);
        if(typeof value !== 'function'){
            eval('bindingContext.$data.' + displayObjectStr + ' = "'+ element.value+'"');
        } else {
            value(element.value);
        }*/

    }
};

function init(){
    ko.options.useOnlyNativeEvents = true;
    ko.applyBindings(ViewModel);

    popups.each(function (index, popup){
        popup = $(popup);
        popup.on('click', function(){

            var popupContent = $('#'+popup.attr('popup'));
            //comPopup.find('.content').html(popupContent.html());
            //comPopup.find('.header h3').text(popup.text());
            //comPopup.show();
            popupContent.show();
            $('.blank').show();
        });
    });

    domains.each(function (index, domain) {
        domain = $(domain);
        domain.on('keypress', function(e){
            var key = e.key||e.char;
            var combobox = $('ul[comboboxitem][name="'+ domain.attr('combobox') +'"]')[0].combobox;
            if(key === '@'){
                combobox.open($(this));
            }
        });
    });

    comboboxItems.each(function (index, item) {
        item = $(item);
        item[0].combobox = new ComboboxItem(item);
    });

    Loadding.show(false);
}

Loadding.show(true);
APIData.HelpdeskConsole.User.get_domains(function (res) {
    res = JSON.parse(res);
    ViewModel.domains(res.result);
    APIData.HelpdeskConsole.User.get_licenses(function (_res) {
        _res = JSON.parse(_res);
        ViewModel.licenses(_res.result);
        init();
    });
});

