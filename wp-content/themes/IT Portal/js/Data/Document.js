APIData.Document = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Wordpress',
        'type': 'Document'
    });

    this.GetDocumentList = function GetDocumentList(categoryName, callback) {
        me.send({
            categoryName: categoryName
        }, callback);
    };

})();