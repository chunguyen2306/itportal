APIData = window.APIData || {};

APIData.MainMenu = {};
APIData.MainMenu = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Options',
        'type': 'MainMenu'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.GetAsMenu = function GetAsMenu(callback) {
        me.send({
        }, callback);
    };

    this.AddNew = function AddNew(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Delete = function Delete(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.GetByID = function GetByID(ID, callback) {
        me.send({
            ID: ID
        }, callback);
    };
    this.UpdateMapping = function UpdateMapping(ID, mapping, callback) {
        me.send({
            ID: ID,
            mapping: mapping
        }, callback);
    };
    this.GetProducts = function GetProducts(parentName, subName, filter, pageIndex, limit, callback) {
        me.send({
            parentName: parentName,
            subName: subName,
            filter: filter,
            pageIndex: pageIndex,
            limit: limit
        }, callback);
    }
})();