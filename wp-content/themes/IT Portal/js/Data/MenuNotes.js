APIData = window.APIData || {};

APIData.MenuNotes = {};
APIData.MenuNotes = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Options',
        'type': 'MenuNotes'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.GetByName = function GetByName(menuName, callback){
        me.send({
            menuName: menuName
        }, callback);
    };

    this.Update = function Update(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.UpdateByName = function Update(menuName, data, callback) {
        me.send({
            data: data,
            menuName: menuName
        }, callback);
    };
})();