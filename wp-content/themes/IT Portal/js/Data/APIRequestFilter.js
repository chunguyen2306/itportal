APIData = window.APIData || {};

APIData.APIRequestFilter = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Workflow',
        'type': 'AMRequestFilterAjax'
    });

    this.getView = function getView(callback) {
        me.send({}, callback);
    };

    this.filter = function filter(args, callback) {
        me.send(args, callback);
    };

    this.filterAI = function filterAI(keyword, keywordDoc, args, callback) {
        var data = {
            keyword: keyword,
            keywordDoc: keywordDoc,
            args: args
        };
        me.send(data, callback);
    };

})();
