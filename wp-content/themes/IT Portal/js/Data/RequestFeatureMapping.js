APIData = window.APIData || {};

APIData.RequestFeatureMapping = {};
APIData.RequestFeatureMapping = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Workflow',
        'type': 'APIRequestFeatureMapping'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.Insert = function Insert(data, callback) {
        me.send({
            data:data
        }, callback);
    };

    this.Update = function Update(data, callback) {
        me.send({
            data:data
        }, callback);
    };

    this.GetByName = function GetByName(name, callback) {
        me.send({
            featureName: name
        }, callback);
    };

    this.GetRequestID = function GetRequestID(requestID, callback) {
        me.send({
            requestID: requestID
        }, callback);
    };
    this.GetPaging = function GetPaging(keyword, pageIndex, limit, callback) {
        me.send({
            keyword: keyword,
            pageIndex: pageIndex,
            limit: limit
        }, callback);
    }
})();