APIData = window.APIData || {};

APIData.APIRequestReason = {};
APIData.APIRequestReason = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Workflow',
        'type': 'AMRequestReason'
    });

    this.GetAll = function GetAll(callback) {
        me.send({}, callback);
    };
    this.GetByID = function GetByID(ID, callback) {
        me.send({ID: ID}, callback);
    };
    this.AddNew = function AddNew(data, callback) {
        me.send({data: data}, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({data: data}, callback);
    };
    this.Delete = function Delete(data, callback) {
        me.send({data: data}, callback);
    };

})();