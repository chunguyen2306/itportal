APIData.MenuType = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Options',
        'type': 'MenuType'
    });

    this.getAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.create = function AddNew(typeName, callback) {
        me.send({
            data: {
                Name: typeName
            }
        }, callback);
    };

    this.update = function Update(id, typeName, callback) {
        me.send({
            data: {
                ID: id,
                Name: typeName
            }
        }, callback);
    };

    this.delete = function Delete(id, callback) {
        me.send({
            data: {
                ID: id
            }
        }, callback);
    }

})();