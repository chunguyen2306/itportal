APIData.ResourceType = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'ResourceType'
    });

    this.getAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.create = function CreateNew(type, callback) {
        me.send({
            type: type
        }, callback);
    };

    this.update = function Update(id, type, callback) {
        me.send({
            id: id,
            type: type
        }, callback);
    };

    this.getAsMenu = function GetAsMenu(callback) {
        me.send({
        }, callback);
    };

    this.GetAllWithComponentType = function GetAllWithComponentType(callback) {
        me.send({
        }, callback);
    }

})();