APIData = window.APIData || {};

APIData.Notification = new (function () {
    var me = this;
    me.Service = "Notification";
    APIData.AbstractAPIData.call(this, {
        'class': 'NotificationAjax',
        'type': 'Notification'
    });

    me.CountUnseen = function (onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "NotificationAjax",
            "CountUnseen",
            {},
            APIData.Callback.int,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.GetListForPreview = function (onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "NotificationAjax",
            "GetListForPreview",
            {},
            APIData.Callback.array,
            function (res) {
                if (res.code == AMResponseCode.ok) {
                    res.data.sort(
                        function (x, y) {
                            return x.ID < y.ID;
                        }
                    );
                }
                onSuccess(res);
            },
            onError,
            dontSkip
        );
    };

    me.GetQuantity = function (typeID, isRead, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "NotificationAjax",
            "GetQuantity",
            {typeID: typeID, isRead: isRead},
            APIData.Callback.int,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.GetList = function (typeID, isRead, start, limit, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "NotificationAjax",
            "GetList",
            {typeID: typeID, start: start, limit: limit, isRead: isRead},
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.MarkAsRead = function (ids, isRead, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "NotificationAjax",
            "MarkAsRead",
            {ids: ids, isRead: isRead},
            APIData.Callback.string,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.DeleteNotification = function (ids, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "NotificationAjax",
            "DeleteNotification",
            {ids: ids},
            APIData.Callback.string,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.ListenNew = function (signalrID, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "NotificationAjax",
            "ListenNew",
            {signalrID: signalrID},
            APIData.Callback.string,
            onSuccess,
            onError,
            dontSkip
        );
    };
})();





