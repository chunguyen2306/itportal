APIData = window.APIData || {};

APIData.AssetStateHistory = {};
APIData.AssetStateHistory = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Asset',
        'type': 'APIAssetStateHistory'
    });

    this.InsertStateHistory = function InsertStateHistory(data, cb) {
        me.send({
            data: data
        }, cb);
    };

    this.GetAssetStateHistory = function GetAssetStateHistory(assetName, cb) {
        me.send({
            assetName: assetName
        }, cb);
    };
    this.GetAssetCommentHistory = function GetAssetCommentHistory(assetName, cb) {
        me.send({
            assetName: assetName
        }, cb);
    };
    this.UpdateSysComment = function UpdateSysComment(data,cb) {
       me.send({
          data: data
       },cb);
    }
})();