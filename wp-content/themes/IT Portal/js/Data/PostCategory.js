APIData.PostCategory = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Wordpress',
        'type': 'PostCategory'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.FilterPaging = function FilterPaging(data, callback) {
        me.send(data, callback);
    }

})();