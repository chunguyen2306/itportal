APIData.ComponentType = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'ComponentType'
    });

    this.paging = function Paging(keyword, pageIndex, limit, callback) {
        me.send({
            keyword: keyword,
            pageIndex: pageIndex,
            limit: limit
        }, callback);
    };

    this.delete = function Delete(id, callback){
        me.send({
            id: id
        }, callback);
    };

    this.create = function CreateNew(data, callback) {
        me.send({
            newData: data
        }, callback);
    };

    this.update = function Update(data, callback) {
        me.send({
            newData: data
        }, callback);
    };
    this.GetAllWithComponent = function GetAllWithComponent(callback) {
        me.send({

        }, callback);
    }
})();