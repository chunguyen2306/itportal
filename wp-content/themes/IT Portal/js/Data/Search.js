APIData = window.APIData || {};

APIData.Search = new (function(){
    var me = this;
    me.Service = "Search";
    APIData.AbstractAPIData.call(this, {
        'class': 'SearchAjax',
        'type': 'Search'
    });

    me.HomeSearch = function (keyword, start, limit, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "SearchAjax",
            "HomeSearch",
            {keyword: keyword, startindex: start, limit: limit},
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    }

    me.PageSearch = function (keyword, start, limit, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "SearchAjax",
            "PageSearch",
            {keyword: keyword, startindex: start, limit: limit},
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    }
})();





