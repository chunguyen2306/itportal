APIData = window.APIData || {};
APIData.AssetDiscountPercentYear = {};
APIData.AssetDiscountPercentYear=new(function () {
    var me = this;
    APIData.AbstractAPIData.call(this,{
        'class': 'Asset',
        'type': 'APIAssetDiscountPercentYear'
    });
    this.getTotalPercentByAssetGroupID = function getTotalPercentByAssetGroupID(GroupID,roundYear, cb) {
        me.send({
            GroupID:GroupID,
            roundYear:roundYear
        },cb);
    }
    this.getByGroupID = function getByGroupID(GroupID,cb) {
        me.send({
            groupid: GroupID
        },cb);
    }
    this.getYearPercentByAssetGroupID = function getYearPercentByAssetGroupID(GroupID,roundYear, cb) {
        me.send({
            GroupID:GroupID,
            roundYear:roundYear
        },cb);
    }
    this.Update = function update(data,cb) {
        me.send({
            data:data
        },cb);
    }
    this.AddNew = function addNew(data,cb) {
        me.send({
           data:data
        },cb);
    }
    this.deletegroup = function deletegroup(AssetTypeGroupID,cb) {
        me.send({
            AssetTypeGroupID:AssetTypeGroupID
        },cb);
    }
})();