APIData = window.APIData || {};

APIData.WhiteListUpload = {};
APIData.WhiteListUpload = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Options',
        'type': 'WhiteListUpload'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.AddNew = function AddNew(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Delete = function Delete(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.GetImageType = function GetImageType(callback) {
        me.send({ }, callback);
    };

})();