APIData = window.APIData || {};

APIData.WorkflowModelGroup = new function () {
    var me = this;
    me.Service = "Workflow";
    APIData.AbstractAPIData.call(this, {
        'class': 'WorkflowModelGroup',
        'type': 'WorkflowModelGroupAjax'
    });

    me.GetListWorkflowModelGroup = function ( onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "AMWorkflowModelGroupAjax",
            "GetListWorkflowModelGroup",
            null,
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetWorkflowModelGroupEditData = function (ID, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "AMWorkflowModelGroupAjax",
            "GetWorkflowModelGroupEditData",
            { ID: ID},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.InsertWorkflowModelGroup = function (group, modelNames, onSuccess, onError, dontSkip) {
        var data = {
            group: group,
            modelNames: modelNames,
        };

        me.ITAM(
            me.Service,
            "AMWorkflowModelGroupAjax",
            "InsertWorkflowModelGroup",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.UpdateWorkflowModelGroup = function (group, modelNames, onSuccess, onError, dontSkip) {
        var data = {
            group: group,
            modelNames: modelNames,
        };

        me.ITAM(
            me.Service,
            "AMWorkflowModelGroupAjax",
            "UpdateWorkflowModelGroup",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.DeleteWorkflowModelGroup = function (ID, onSuccess, onError, dontSkip) {
        var data = {
            ID: ID,
        };

        me.ITAM(
            me.Service,
            "AMWorkflowModelGroupAjax",
            "DeleteWorkflowModelGroup",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

}
