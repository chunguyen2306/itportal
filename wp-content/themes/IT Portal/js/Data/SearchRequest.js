APIData = window.APIData || {};

APIData.RequestSearch = new (function(){
    var me = this;
    me.Service = "Search";
    APIData.AbstractAPIData.call(this, {
        'class': 'RequestSearchAjax',
        'type': 'RequestSearch'
    });

    me.GetListRequestSearch = function (onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "GetListRequestSearch",
            null,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.GetRequestSearchEditData = function (id, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "GetRequestSearchEditData",
            {ID: id},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.GetRequestSearchEditData = function (id, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "GetRequestSearchEditData",
            {ID: id},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.DeleteRequestSearch = function (id, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "DeleteRequestSearch",
            {ID: id},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.Search = function (requestSearchID, condition, onSuccess, onError, dontSkip){
        var data = {
            configID: requestSearchID,
            q: condition
        };

        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "Search",
            data,
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };

    // dev_upgrade
    // code tạm để lấy data cho chức năng xuất nhập kho, sau khi hoàn thiện tính năng search request
    // thì có thể bỏ
    me._temp_SearchStoreRequest = function (onSuccess, onError, dontSkip){
        var data = {
            configID: 6,
            wfStepIDs: [404]
        };

        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "Search",
            data,
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };

    me.InsertSearchRequest = function (requestSearch, columns, conditions, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "InsertRequestSearchData",
            {data: requestSearch, columns: columns, conditions: conditions},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }

    me.UpdateSearchRequest = function (requestSearch, columns, filters, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "RequestSearchAjax",
            "UpdateRequestSearchData",
            {data: requestSearch, columns: columns, filters: filters},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
})();





