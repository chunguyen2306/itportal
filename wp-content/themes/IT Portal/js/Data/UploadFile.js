APIData.UploadFile = function (path, name, file, isChangeName, onDone) {
    var data = new FormData();
    data.append('fileInfo', file);
    data.append('path', path);
    data.append('fileName', name);
    data.append('isChangeName', isChangeName);
//validateFile
    var types = ['php', 'bat', 'exe', 'bin', 'cmd', 'csh','msi'];
    var response = {
        State : true,
        Code : 200,
        Mess : 'File upload thành công'
    };
    var type = file.name.split('.').pop().toLowerCase();
    if (types.includes(type)) {
        response.State = false;
        response.Code = 405;
        response.Mess = '<b>' + file.name + '</b>' + 'định dạng file không được phép';

    }
    if (file.size > 10440000) {
        response.State = false;
        response.Code = 494;
        response.Mess = '<b>' + file.name + '</b>' + 'vượt quá kích thước cho phép(10MB)';

    }

    if(response.State){
        $.ajax({
            url: APIData.BEUrl,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            headers: {
                "U-Class": 'UploadFile',
                "U-Type": 'Upload',
                "U-Method": 'UploadFile'
            },
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: onDone,
            error: function(jqXHR, textStatus, errorThrown) {
                response.State= false;
                response.Code= textStatus;
                response.Mess = errorThrown;
              onDone(response);
            }
        });
    }else {
        setTimeout(onDone, 1, response);
    }
};

// APIData.RemoveFile = function (path, name, file, isChangeName, onDone) {
//     var data = new FormData();
//     data.append('fileInfo', file);
//     data.append('path', path);
//     data.append('fileName', name);
//     data.append('isChangeName', isChangeName);
//     $.ajax({
//         url: APIData.BEUrl,
//         type: 'POST',
//         data: data,
//         cache: false,
//         dataType: 'json',
//         headers: {
//             "U-Class": 'UploadFile',
//             "U-Type": 'Upload',
//             "U-Method": 'UploadFile'
//         },
//         processData: false, // Don't process the files
//         contentType: false, // Set content type to false as jQuery will tell the server its a query string request
//         success: onDone,
//         error: function(jqXHR, textStatus, errorThrown) {
//             console.log('ERRORS: ' + textStatus);
//         }
//     });
// };
// APIData.FileUpload = new function () {
//     var me = this;
//
//     function classFileUpload(allowMulti){
//         var f = this;
//         var UploadList = [];
//         var inprocessCount = 0;
//         var fileInput = $('<input type="file" multiple style="display: none"/>');
//         fileInput.on('change', onPickFiles);
//         if(allowMulti)
//             fileInput.attr('multiple', '');
//
//         function removeFile(dir, fullName) {
//
//         }
//
//         function upload(file, dir, name, done) {
//             var uploadItem = UploadList.find(function (item) {
//                 return item.file == file;
//             });
//
//             if(uploadItem == null){
//                 uploadItem = {
//                     file: file,
//                     url: null,
//                 }
//
//                 APIData.UploadFile(path, name, file, true, function () {
//                     if(UploadList.indexOf(uploadItem) == -1){
//                         removeFile(path, name)
//                     }
//
//                     uploadItem.url =  expectUrl;
//                 });
//             }
//         }
//
//         function uploadFiles(file, expectUrl) {
//             var newUploadList = [];
//
//
//
//             UploadList.push(uploadItem);
//
//             return uploadItem;
//         }
//
//         function onPickFiles() {
//             var files = fileInput[0].files;
//
//             fileInput.remove();
//         }
//
//         f.pickFiles = function (expectUrl) {
//             $('body').append(fileInput);
//             fileInput.click();
//         };
//
//         f.rollbackAll = function () {
//
//         };
//
//         f.rollbackAll = function(){
//
//
//         }
//
//         $(window).on('unload', f.rollbackAll)
//     }
// };