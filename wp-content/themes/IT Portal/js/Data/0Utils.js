APIData.BEUrl = '/am_ajax';
APIData.OnError = function () {
};

APIData.AbstractAPIData = function (_config) {
    var me = this;
    this.config = _config;

    this.OnRequestEror = function (error) {
        console.error(error);
    };

    this.send = function (data, cb, config) {
        me.config.method = arguments.callee.caller.name;
        var sendConfig = {
            url: APIData.BEUrl,
            data: data,
            header: {
                "U-Class": me.config.class,
                "U-Type": me.config.type,
                "U-Method": me.config.method
            },
            method: 'POST',
            success: cb,
            error: me.OnRequestEror
        };
        sendConfig = $.extend(sendConfig, config);

        Libs.Http.send(sendConfig);
    };

    this.legacyCMDB = function CMDB(url, method, pNames, pValues, callbackMethod, onSuccess, onError, dontSkip) {
        pNames.push('m');
        pValues.push(method);
        var data = {
            u: url,
            p: JSON.stringify(pNames),
            v: JSON.stringify(pValues)
        };

        $.ajax({
            type: 'POST',
            url: APIData.BEUrl,
            data: data,
            headers: {
                "U-Class": me.config.class,
                "U-Type": me.config.type,
                "U-Method": "CallCMDB"
            },
            success: callbackMethod(onSuccess, method, dontSkip),
            error: function (re) {
                onError && onError(re);
                if(location.hostname == 'itportal'){
                    console.error({url: url, method: method, pNames: pNames, pValues: pValues, re: re});
                    Popup.error(re.responseText).setTitle('Không thể kết nối tới máy chủ');
                }
            },
        });
    };

    this.ITAM = function (pluginName, className, method, data, callbackMethod, onSuccess, onError, dontSkip) {
        $.ajax({
            type: 'POST',
            url: APIData.BEUrl,
            data: data,
            headers: {
                "U-Class": pluginName,
                "U-Type": className,
                "U-Method": method
            },
            success: callbackMethod(onSuccess, method, dontSkip),
            error: function (re) {
                onError && onError(re);
                if(location.hostname == 'itportal'){
                    console.error({pluginName: pluginName, className: className, method: method, data: data, re: re});
                    Popup.error('Không thể kết nối tới máy chủ');
                }
            }
        });
    };
};

APIData.Callback = new function () {
    var t = this;

    function log(name, jData, type, ex) {
        if(location.hostname == 'itportal'){Popup.error(jData);
            console.warn("Data is not a \"" + type + "\" at: " + name);
            try {
                console.warn(JSON.parse(jData));
            } catch (ex) {
                console.warn(jData);
            }
            ex && console.error(ex);
        }
    }
    ;
    t.string = function (callback, name, skipIfEror) {
        return function (jData) {
            var obj;
            try {
                obj = JSON.parse(jData);
            } catch (ex) {
                log(name, jData, "string", ex);
            }

            if (obj == null && jData != "null") {
                log(name, jData, "string");
                if (skipIfEror)
                    return;
            }

            callback(obj, jData);
        };
    }
    t.object = function (callback, name, skipIfEror) {
        return function (jData) {
            var obj;
            try {
                obj = JSON.parse(jData);
            } catch (ex) {
                log(name, jData, "object", ex);
            }

            if (obj == null && jData != "null") {
                log(name, jData, "object");
                if (skipIfEror)
                    return;
            }

            callback(obj, jData);
        };
    }
    t.array = function (callback, name, skipIfEror) {
        return function (jData) {
            var res = null;
            try {
                res = JSON.parse(jData);
            } catch (ex) {
                log(name, jData, "array", ex);
            }

            if (res && Array.isArray(res.data) && res.hasOwnProperty("code")) {
                callback(res, jData);
            } else {

                log(name, jData, "array");
                if (skipIfEror == false)
                    callback(null, jData);
                return;
            }

        };
    }
    t.int = function (callback, name, skipIfEror) {
        return function (jData) {
            var res;
            try {
                res = JSON.parse(jData);
            } catch (ex) {
                log(name, jData, "object", ex);
            }

            if (Number.isInteger(res.data) == false || (res == null && jData != "null")) {
                log(name, jData, "int");
                if (skipIfEror)
                    return;
            }

            callback(res, jData);
        };
    }
    t.float = function (callback, name, skipIfEror) {
        return function (jData) {
            if (isNaN(jData)) {
                log(name, jData, "float");
                callback(null, jData);
                return;
            }

            var number = null;
            try {
                number = parseFloat(jData);
            } catch (ex) {
                log(name, jData, "float", ex);
            }

            number || log(name, jData, "float");

            if (number || skipIfEror == false)
                callback(null, jData);
        };
    }
};

APIData.CmdbCallback = new function () {
    var t = this;

    function log(name, jData, type, ex) {

        if(location.hostname == 'itportal'){
            Popup.error(jData);
            console.warn("Data is not a \"" + type + "\" at: " + name);
        }
        try {
            console.warn(JSON.parse(jData));
        } catch (ex) {
            console.warn(jData);
        }
        ex && console.error(ex);
    }
    ;
    t.string = function (callback, name, skipIfEror) {
        return callback;
    }
    t.object = function (callback, name, skipIfEror) {
        return function (jData) {
            var obj = null;
            try {
                obj = JSON.parse(jData);
            } catch (ex) {
                log(name, jData, "object", ex);
            }
            if (obj == null && jData != "null") {
                log(name, jData, "object");
                if (skipIfEror)
                    return;
            }

            callback(obj, jData);
        };
    }
    t.array = function (callback, name, skipIfEror) {
        return function (jData) {
            var res = null;
            try {
                res = JSON.parse(jData);
            } catch (ex) {
                log(name, jData, "array", ex);
            }

            if (Array.isArray(res)) {

                callback(res, jData);
            } else {

                log(name, jData, "array");
                if (skipIfEror == false)
                    callback(null, jData);
                return;
            }

        };
    }
    t.int = function (callback, name, skipIfEror) {
        return function (jData) {
            if (isNaN(jData)) {
                log(name, jData, "int");
                callback(null, jData);
                return;
            }

            var number = null;
            try {
                number = parseInt(jData);
            } catch (ex) {
                log(name, jData, "int", ex);
            }

            number || log(name, jData, "int");

            if (number || skipIfEror == false)
                callback(null, jData);

        };
    }
    t.float = function (callback, name, skipIfEror) {
        return function (jData) {
            if (isNaN(jData)) {
                log(name, jData, "float");
                callback(null, jData);
                return;
            }

            var number = null;
            try {
                number = parseFloat(jData);
            } catch (ex) {
                log(name, jData, "float", ex);
            }

            if (obj == null && jData != "null") {
                log(name, jData, "float");
                if (skipIfEror)
                    return;
            }

            callback(number, jData);
        };
    }
};
