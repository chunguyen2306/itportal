function SignalREngine(url, hub) {
    var me = this;
    var fn = SignalREngine;
    var onGets = [];

    me.url = url;
    me.chat = null;
    me.isConnected = 0;
    me.signalRID = null;

    var getUrl = $.signalR.transports._logic.getUrl;
    $.signalR.transports._logic.getUrl = function(connection, transport, reconnecting, poll, ajaxPost) {
        var url = getUrl(connection, transport, reconnecting, poll, ajaxPost);
        return connection.url + url.substring(url.indexOf(connection.appRelativeUrl) + connection.appRelativeUrl.length);
    };

    var timeIntervalReconnect = 0;
    var connectedCount = 0;

    function onGetMessage(message) {
        me.isConnected = 1;

        var hubMessage = JSON.parse(message);
        var methods = onGets[hubMessage.method];

        console.log('SIGNALR RECEIVE METHOD: ' + hubMessage.method + ", Data: ");
        console.log(hubMessage.data);

        if (methods){
            for (var i = 0; i < methods.length; i++)
                methods[i](hubMessage.message);
        }
    }

    function setupDisconnected() {
        $.connection.hub.disconnected(function () {
            me.isConnected = 0;
            if (me.onDisconnected.list) {
                var list = me.onDisconnected.list;
                for (var i = 0; i < list.length; i++)
                    list[i](connectedCount, $.connection.hub.id);
                me.onDisconnected.list = [];
            }

            if (timeIntervalReconnect != 0)
                return;

            clearTimeout(timeIntervalReconnect);
            timeIntervalReconnect = setInterval(function () {
                me.connect();
            }, SignalREngine.reconnectDuration);
        });
    }

    me.connect = function () {
        if (!$.connection) {
            fn.onScriptLoadDone(me.connect);
            return;
        }

        /// open
        $.connection.hub.url = me.url;
        me.chat = $.connection.NotificationHub;

        // receive message
        me.chat.client.broadcastMessage = onGetMessage;

        // connect
        $.connection.hub.start().done(function () {
            me.isConnected = 1;
            connectedCount++;
            clearTimeout(timeIntervalReconnect);
            timeIntervalReconnect = 0;

            me.signalRID = $.connection.hub.id;
            console.log("Hub connect time: " + connectedCount + ". ID = " + me.signalRID);

            if (me.onConnected.list) {
                var list = me.onConnected.list;
                for (var i = 0; i < list.length; i++)
                    list[i](connectedCount, $.connection.hub.id);
                me.onConnected.list = [];
            }
        });
    };
    me.onGet = function (methodName, methodCallBack) {
        var list = onGets[methodName];
        if (list == null)
            list = onGets[methodName] = [];
        list.push(methodCallBack);
    };
    me.offGet = function (methodName, methodCallBack) {
        var list = onGets[methodName];
        if (list == null)
            return;

        var index = list.indexOf(methodCallBack);
        list.splice(index, 1);
    };
    me.onConnected = function (callback) {
        var list = me.onConnected.list || (me.onConnected.list = []);
        list.push(callback);
    };
    me.onDisconnected = function (callback) {
        var list = me.onDisconnected.list || (me.onDisconnected.list = []);
        list.push(callback);
    };
    me.send = function (method, data, ids, typeOfReciver) {
        if (me.isConnected == false) {
            me.onConnected(function () {
                me.send(method, data, ids, typeOfReciver);
            });
        }

        var temp = {
            method: method,
            message: data,
            reciverIDs: ids,
            typeOfReciverID: typeOfReciver
        };
        me.chat.server.send(JSON.stringify(temp));
    };

    if ($.connection)
        setupDisconnected();
    else
        fn.onScriptLoadDone(setupDisconnected);
}

// global init
$(function () {
    var fn = SignalREngine;
    var connectOnLoads = [];

    fn.reconnectDuration = 60000;
    fn.onScriptLoadDone = function (callback) {
        connectOnLoads.push(callback);
    };
    fn.connectAfter = function () {
        connectOnLoads.forEach(function (callback) {
            callback();
        });
    };
});

// itportal implement
$(function () {

    var fn = SignalREngine;

    // one instanse for one doc
    fn.getDefault = function () {
        return {
            onConnected: function () { },
            connect: function () { },
            onGet: function () { },
        };
    };

    // type of reciver id
    fn.typeOfReciverID = {
        signalRID: 1,
        Domain: 2,
        all: 3
    };

    // type of reciver id
    fn.method = {
        newNoti: 1,
        showRequestQR: 2,
        showRequestReply: 3
    }

    return; // tắt tránh tải trang lâu

    var comHost = location.protocol == "https:"  ?
        "https://it.vng.com.vn/communication/"
        //: "https://api-it.vng.com.vn/communication/";
        : "http://localhost:8001/";
    var fn = SignalREngine;

    // one instanse for one doc
    fn.getDefault = function () {
        var fn = SignalREngine;
        if (fn.inst == null)
            fn.inst = new fn(comHost + "SignalR/hubs", 'NotificationHub');

        return fn.inst;
    };

    // type of reciver id
    fn.typeOfReciverID = {
        signalRID: 1,
        Domain: 2,
        all: 3
    };

    // type of reciver id
    fn.method = {
        newNoti: 1,
        showRequestQR: 2,
        showRequestReply: 3
    }

    // load script
    var joHead = $(document.head);

    function loadScript(url, loadDone) {
        var el = document.createElement('script');
        joHead.append(el);
        el.type = "text/javascript";
        el.onload = loadDone;
        try{
            el.src = url;
        }catch (ex){
            console.error(ex);
        }
    }

    /*loadScript(comHost + "js/jquery.signalR-2.2.2.js", function () {
        loadScript(comHost + "SignalR/hubs", function () {
            fn.connectAfter();
        });
    });*/

    // init instance
    // fn.getDefault();
});



















