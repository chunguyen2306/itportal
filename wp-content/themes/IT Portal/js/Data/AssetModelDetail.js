APIData = window.APIData || {};

APIData.AssetModelDetail = {};
APIData.AssetModelDetail = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Asset',
        'type': 'APIAssetModelDetail'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.AddNew = function AddNew(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Delete = function Delete(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.GetByID = function GetByID(ID, callback) {
        me.send({
            ID: ID
        }, callback);
    };

    this.DeleteGallery = function DeleteGallery(galleryName, modelID, callback) {
        me.send({
            galleryName: galleryName,
            modelID: modelID
        }, callback);
    };

    this.GetThubmailsPath = function GetThubmailsPath(modelID, callback) {
        me.send({
            modelID: modelID
        }, callback);
    };
    
    this.GetByModelID = function GetByModelID(ModelID, callback) {
        me.send({
            ModelID: ModelID
        }, callback);
    };
})();