APIData = window.APIData || {};

APIData.CMDB = new (function(){
    var me = this;
    me.ServiceURL = "CMDB2";
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'CMDB'
    });
   
///
    me.GetListAsset = function(resourcename, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetListAsset",
            ["resourcename", "startindex", "limit"],
            [resourcename, startindex || 0, limit || 10],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetListUser = function(username, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetListUser",
            ["username", "startindex", "limit"],
            [username, startindex || 0, limit || 10],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.buildAssetDetail = function (assetObject) {
        var realValue = Common.getValueOfObject(assetObject);
        var totals = realValue.length;
        var details = totals;
        for (var m = 0; m < totals; m++) {
            details += "-";
            var props = Object.getOwnPropertyNames(realValue[m]);
            for (var i = 0; i < props.length; i++) {
                var propName = props[i];
                var value = realValue[m][propName];
                if (i === props.length - 1) {
                    details += value;
                } else {
                    details += value + ":";
                }
            }
        }
        return details;
    };
    me.buildAssetStateModel = function (searModel, listAssetState) {
        var list = [];
        for (var i = 0; i < listAssetState.length; i++) {
            var description = listAssetState[i].DisplayDescription;
            description = description ? listAssetState[i].DisplayState + ' - ' + description
                : listAssetState[i].DisplayState;
            var item = {
                name:  description ,
                value: listAssetState[i].DisplayState
            };
            list.push(item);
        }

        var searchMember = [
            {Name: 'value', IsUnicode: false},
            {Name: 'name', IsUnicode: true}
        ];
        searModel.setDataSource(list, searchMember);
    }
    me.GetAssetModel = function(assetName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetModel",
            ["assetName"],
            [assetName],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };

// auto generate
    me.UpdateAssetState = function(componentType, ciName, state, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssetState",
            ["componentType", "ciName", "state"],
            [componentType, ciName, state],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateAssetOwner = function(componentType, ciName, userName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssetOwner",
            ["componentType", "ciName", "userName"],
            [componentType, ciName, userName],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateAssetDetail = function(componentType, ciName, detailstr, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssetDetail",
            ["componentType", "ciName", "detailstr"],
            [componentType, ciName, detailstr],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateAssociateAsset = function(componentType, ciName, parent, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssociateAsset",
            ["componentType", "ciName", "parent"],
            [componentType, ciName, parent],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetListComponentType = function(keyword, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetListComponentType",
            ["keyword", "startindex", "limit"],
            [keyword, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetListComponentModel = function(keyword, type, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetListComponentModel",
            ["keyword", "type", "startindex", "limit"],
            [keyword, type, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetResourceOwnerByName = function(resourceName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetResourceOwnerByName",
            ["resourceName"],
            [resourceName],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetChildrensOfAsset = function(resourceName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetChildrensOfAsset",
            ["resourceName"],
            [resourceName],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.getAssetOfUser = function(LoginName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "getAssetOfUser",
            ["LoginName"],
            [LoginName],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetOfUser = function(username, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetOfUser",
            ["username", "startindex", "limit"],
            [username, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAccountName = function(name, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAccountName",
            ["name"],
            [name],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetUserFullName = function(name, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetUserFullName",
            ["name"],
            [name],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetUserInformation = function(username, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetUserInformation",
            ["username", "startindex", "limit"],
            [username, startindex, limit],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateCustomAssetDetail = function(resourcename, CPU, RAM, VGA, HDD, DVD, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateCustomAssetDetail",
            ["resourcename", "CPU", "RAM", "VGA", "HDD", "DVD"],
            [resourcename, CPU, RAM, VGA, HDD, DVD],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetLastStatusHistory = function(resourcename, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetLastStatusHistory",
            ["resourcename"],
            [resourcename],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetResourceStateById = function(id, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetResourceStateById",
            ["id"],
            [id],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetResourceStateHistoryByResourceName = function(resourcename, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetResourceStateHistoryByResourceName",
            ["resourcename"],
            [resourcename],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.getResourceOwnerHistoryInfo = function(STATEHISTORYID, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "getResourceOwnerHistoryInfo",
            ["STATEHISTORYID"],
            [STATEHISTORYID],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetNumberFromStr = function(str, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetNumberFromStr",
            ["str"],
            [str],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetType = function(ResourceName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetType",
            ["ResourceName"],
            [ResourceName],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };
    me.CheckAndUpdateAssociationAsset = function(resourceName, parentAsset, updateField, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "CheckAndUpdateAssociationAsset",
            ["resourceName", "parentAsset", "updateField"],
            [resourceName, parentAsset, updateField],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateWorkstationTextFieldByName = function(columnName, value, resourceName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateWorkstationTextFieldByName",
            ["columnName", "value", "resourceName"],
            [columnName, value, resourceName],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAllAssetDetail = function(resourcename, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAllAssetDetail",
            ["resourcename"],
            [resourcename],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetResourceStateByAssetName = function(resourcename, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetResourceStateByAssetName",
            ["resourcename"],
            [resourcename],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetListAssetState = function (keyword, startindex, limit, onSuccess, onError, dontSkip) {
        me.legacyCMDB(
            me.ServiceURL,
            "GetListAssetState",
            ["keyword", "startindex", "limit"],
            [keyword, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetStateList = function(state, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetStateList",
            ["state", "startindex", "limit"],
            [state, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };

    me.GetAssetByType = function(typeID, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetByType",
            ["typeID", "startindex", "limit"],
            [typeID, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetByModel = function(modelID, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetByModel",
            ["modelID", "startindex", "limit"],
            [modelID, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetByTypeAndModel = function(typeID, modelID, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetByTypeAndModel",
            ["typeID", "modelID", "startindex", "limit"],
            [typeID, modelID, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetByTypeAndModelName = function(type, modelName, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetByTypeAndModelName",
            ["type", "modelName", "startindex", "limit"],
            [type, modelName, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetInStoreByTypeAndModelAndName = function(type, modelName, name, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetInStoreByTypeAndModelAndName",
            ["type", "modelName", "name", "startindex", "limit"],
            [type, modelName, name, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetInStoreByTypeAndModel = function(typeID, modelID, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetInStoreByTypeAndModel",
            ["typeID", "modelID", "startindex", "limit"],
            [typeID, modelID, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.checkAssetInStore = function(resourceName, type, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "checkAssetInStore",
            ["resourceName", "type"],
            [resourceName, type],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetByTypeAndModelAndName = function(type, modelName, name, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetByTypeAndModelAndName",
            ["type", "modelName", "name", "startindex", "limit"],
            [type, modelName, name, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAssetCost = function(assetname, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAssetCost",
            ["assetname"],
            [assetname],
            APIData.Ajax.CmdbCallback.float,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetDefaultUserBySite = function(listUser, site, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetDefaultUserBySite",
            ["listUser", "site"],
            [listUser, site],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetPersonInfoByUsername = function(userName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetPersonInfoByUsername",
            ["userName"],
            [userName],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetListSite = function(siteName, startIndex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetListSite",
            ["siteName", "startIndex", "limit"],
            [siteName, startIndex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.getUserAccountByEmployeeId = function(employeeId, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "getUserAccountByEmployeeId",
            ["employeeId"],
            [employeeId],
            APIData.Ajax.CmdbCallback.string,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetPositionOfUser = function(username, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetPositionOfUser",
            ["username"],
            [username],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.getListEmployeeTitle = function(name, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "getListEmployeeTitle",
            ["name"],
            [name],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.updateGeneralInformation = function(resourceName, detailJson, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "updateGeneralInformation",
            ["resourceName", "detailJson"],
            [resourceName, detailJson],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.getGeneralInfo = function(resourceName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "getGeneralInfo",
            ["resourceName"],
            [resourceName],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.getUserADInformation = function(username, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "getUserADInformation",
            ["username"],
            [username],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetAllResourceStateHistoryByResourceName = function(resourceName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetAllResourceStateHistoryByResourceName",
            ["resourcename"],
            [resourceName],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
})();

/*
 CMDBEX: ______________________________________________________________________________________
 */
APIData.CMDBEX = new function () {
    var me = this;
    me.ServiceURL = "CMDBEX";
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'CMDB'
    });
    me.UpdateAssetState = function(componentType, ciName, state, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssetState",
            ["componentType", "ciName", "state"],
            [componentType, ciName, state],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateAssetOwner = function(componentType, ciName, userName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssetOwner",
            ["componentType", "ciName", "userName"],
            [componentType, ciName, userName],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateAssetDetail = function(componentType, ciName, detailstr, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssetDetail",
            ["componentType", "ciName", "detailstr"],
            [componentType, ciName, detailstr],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.UpdateAssociateAsset = function(componentType, ciName, parent, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "UpdateAssociateAsset",
            ["componentType", "ciName", "parent"],
            [componentType, ciName, parent],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetListAssetDetailHistory = function(resourceName, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetListAssetDetailHistory",
            ["resourceName"],
            [resourceName],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetListAssetModel = function(keyword, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetListAssetModel",
            ["keyword", "startindex", "limit"],
            [keyword, startindex, limit || 7],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.SearchProduct = function(keyword, startindex, limit, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "SearchProduct",
            ["keyword", "startindex", "limit"],
            [keyword, startindex, limit],
            APIData.Ajax.CmdbCallback.array,
            onSuccess,
            onError, dontSkip
        );
    };
    me.GetParentsAssets = function(assetNames, onSuccess, onError, dontSkip){
        me.legacyCMDB(
            me.ServiceURL,
            "GetParentsAssets",
            ["assetNames"],
            [JSON.stringify(assetNames)],
            APIData.Ajax.CmdbCallback.object,
            onSuccess,
            onError, dontSkip
        );
    };

};







