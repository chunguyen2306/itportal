APIData.ResourceCategory = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'ResourceCategory'
    });

    this.getAll = function GetAll(callback) {
        me.send({
        }, callback);
    };

    this.create = function CreateNew(category, callback) {
        me.send({
            category: category
        }, callback);
    };

    this.update = function Update(id, category, callback) {
        me.send({
            id: id,
            category: category
        }, callback);
    }

})();