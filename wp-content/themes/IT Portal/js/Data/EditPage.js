APIData = window.APIData || {};

APIData.EditPage = {};
APIData.EditPage = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Options',
        'type': 'EditPage'
    });

    this.GetPageContent = function GetPageContent(page, callback) {
        me.send({
            page: page
        }, callback);
    };

    this.GetPageContentForBinding = function GetPageContentForBinding(page, callback) {
        me.send({
            page: page
        }, callback);
    };

    this.getValuePagecontent = function getValuePagecontent(name, page, callback){
        me.send({
            name: name,
            page: page
        }, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.isExistPageContent = function isExistPageContent(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.AddNew = function AddNew(data, callback) {
        me.send({
            data: data
        }, callback);
    };

})();