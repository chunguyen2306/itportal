APIData = window.APIData || {};

APIData.RequestFieldData = new function () {
    var me = this;
    me.Service = "Workflow";
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'CMDB'
    });


    function Search(dataSearch, isCheckOnly ,onSuccess, onError, dontSkip) {
        dataSearch.IsCheckOnly = isCheckOnly;

        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestFieldDataAjax",
            "Search",
            {dataSearch: dataSearch},
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }

    me.Search = function (dataSearch, onSuccess, onError, dontSkip){
        if(dataSearch.SearchDataTypeID == 7)
            console.log(dataSearch); // dev_test

        Search(dataSearch, false ,onSuccess, onError, dontSkip);

    }

    me.Check = function (dataSearch, onSuccess, onError, dontSkip){
        Search(dataSearch, true ,onSuccess, onError, dontSkip);
    }
};
