APIData = window.APIData || {};

function EventManager() {
    var me = this;

    me.on = function (name, evenHandler) {
        var item = me[name];
        if(item == null)
            item = me[name] = new eventItem();

        item.bind(evenHandler);
    };

    me.off = function (name, evenHandler) {
        var item = me[name];
        if(item)
            item.unbind(evenHandler);
    };

    function eventItem() {
        var list = [];

        this.bind = function (eventHandler) {
            list.push(eventHandler);
        }

        this.unbind = function (eventHandler) {
            Utils.removeFromArray(list, eventHandler);
        }

        this.trigger = function () {
            list.forEach(function (fn) {
                fn.apply(this, arguments);
            });
        }
    }
}

APIData.Request = new function () {
    var me = this;
    var wfID;
    var rqID;
    me.Service = "Workflow";
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'CMDB'
    });

    /**
     * Tạo một đối tượng tương ứng với requestData mà service trả về,
     * bổ sung các method util để mapping, get, set data với definition
     */
    me.RequestData = function (requestData) {

        var t = this;
        Object.assign(t, requestData);
        // t.workflow ;
        // t.request ;
        // this.listFieldDefinition;
        // t.listStepDefinition ;
        // t.field ;
        // t.listStep ;

        function fnCheckNull(listFieldDefinition, dataModel) {
            for(var i = 0; i < listFieldDefinition.length; i++){
                var fieldDefinition = listFieldDefinition[i]
                var can = t.currentFieldCan[fieldDefinition.Name];
                var val = null;

                if(fieldDefinition.getEditData){
                    val = fieldDefinition.getEditData();
                }else if(dataModel){
                    val = dataModel[fieldDefinition.Name];
                }else{
                    val = fieldDefinition.value;
                }

                if(can.update && !can.empty){
                    var isEmpty = false;
                    if(typeof val == 'string'){
                        isEmpty = val.trim() == '';
                    }else if(val === undefined || val === null){
                        isEmpty = true;
                    }else if(Array.isArray(val) && !val.length){
                        isEmpty = true;
                    }

                    if(isEmpty)
                        return '<b>' + fieldDefinition.DisplayName + '</b> không thể bỏ trống';
                }

                if(fieldDefinition.listChild && val){
                    for(var j = 0; j < val.length; j++) {
                        var result = fnCheckNull(fieldDefinition.listChild, val[j]);
                        if(result)
                            return result;
                    }
                }
            }

            return null;
        }

        t.events = new EventManager();

        t.setField = function (field) {
            if (field) {
                t.listFieldDefinition.forEach(function (fieldDefinition) {
                    fieldDefinition.value = field[fieldDefinition.Name];
                });
            } else {
                t.listFieldDefinition.forEach(function (fieldDefinition) {
                    fieldDefinition.value = null;
                });
            }
        };

        t.setListStep = function (listStep) {
            if (listStep) {
                listStep.forEach(function (step) {
                    var n = t.listStepDefinition.length;
                    for (var i = 0; i < n; i++) {
                        var stepDefinition = t.listStepDefinition[i];
                        if (stepDefinition.ID === step.DefinitionID) {
                            stepDefinition.step = step;
                            step.definition = stepDefinition;
                        }
                    }
                });
            } else {
                t.listStepDefinition.forEach(function (stepDefinition) {
                    var step = new APIData.Workflow.RequestStepEntity();
                    step.DefinitionID = stepDefinition.ID;
                    stepDefinition.step = step;
                    step.definition = stepDefinition;
                });
            }
        };

        t.getField = function (sortAndCanUpdateOnly) {
            var re = {};
            t.listFieldDefinition.forEach(function (fieldDefinition) {
                re[fieldDefinition.Name] = fieldDefinition.value;
            });

            // filter and sort data
            if(sortAndCanUpdateOnly){
                // dev_upgrade
            }

            return re;
        };

        t.getFieldDefinition = function (name, fields) {
            var l = fields || t.listFieldDefinition;
            var n = l.length;
            for (var i = 0; i < n; i++) {
                if (l[i].Name == name)
                    return l[i];
            }
        };

        t.loadFieldDefinition = function (workflowID, callBack) {
            APIData.Workflow.GetListWorkflowField(workflowID, function (res) {
                if (res.code == AMResponseCode.ok){
                    t.listFieldDefinition = res.data;
                    wfID = workflowID;
                    callBack(res);
                } else {
                    t.listFieldDefinition = [];
                    callBack(res);
                }
            });
        };

        t.createRequest = function (callBack, cancelCallBack) {
            if (t.createRequest.isProcessing)
                return;

            function create(comment) {
                t.createRequest.isProcessing = 1;
                var field = t.getField();
                APIData.Request.CreateRequest(wfID || t.workflow.ID, field, null, function (res) {
                    t.createRequest.isProcessing = 0;
                    if (res && res.code == AMResponseCode.ok) {
                        rqID = res.data.request.ID;
                        t.listFieldDefinition = res.data.listFieldDefinition;
                        callBack(res);
                    } else {
                        callBack(res);
                    }
                }, function (error) {
                    t.createRequest.isProcessing = 0;
                    callBack({});
                });
            }

            if (cancelCallBack) {
                var jo = $('<strong>Bạn chắc chắn muốn tạo đơn hàng ?</strong>');
                Component.System.alert.show(jo,
                    {
                        name: "Tạo đơn hàng", cb: create
                    },
                    {
                        name: "Xem lại",
                        cb: function () {
                            cancelCallBack()
                        }
                    }
                )
            } else {
                create();
            }

        };

        t.getRequestOwner = function () {
            if(t.request == null)
                return null;

            var ownerField = t.getFieldDefinition(t.workflow.OwnerField);
            var domain = ownerField.RequestDataTypeID == APIData.Workflow.RequestDataTypeID.domain ?
                ownerField.value : ownerField.value.Domain;

            return domain;
        };

        t.checkData = function () {
            return fnCheckNull(t.listFieldDefinition);
        };

        /**
         *
         * @param maxLength độ dài mô tả tối đa trên 1 dòng. Giá trị phải là số lớn hơn hoặc bằng 5, nếu không sẽ không giới hạn số ký tự
         * @param maxLine số dòng tối đa. Nếu maxLine = 1, 1 dòng sẽ mô tả cho tất cả các sản phẩm. Mặc định bằng 1
         */
        t.getDescription = function (maxLength, maxLine) {
            if (maxLength < 5 || isNaN(maxLength))
                maxLength = null;

            if (maxLine < 1 || isNaN(maxLength))
                maxLine = 1;

            var tableField, modelField, numberField;
            var fields = this.listFieldDefinition;
            var requestDataTypeID = APIData.Workflow.RequestDataTypeID;
            for (var i = 0; i < fields.length; i++) {
                if (fields[i].RequestDataTypeID == requestDataTypeID.table) {
                    tableField = fields[i];
                    break;
                }
            }

            for (var i = 0; i < fields.length; i++) {
                var field = fields[i];
                if (field.ParentID = tableField.ID) {
                    if (field.RequestDataTypeID == requestDataTypeID.assetModel)
                        modelField = field;
                    else if (field.RequestDataTypeID == requestDataTypeID.number)
                        modelField = field;
                }
            }

            var table = tableField.value;
            var group = [];
            for (var i = 0; i < table.length; i++) {
                var row = table[i];
                var modelName = row[modelField.name];
                var model = group.find(function (item) {
                    return item.name = modelName;
                });

                if(model == null){
                    model = {
                        name: modelName,
                        count: 0,
                    };

                    group.push(model);
                }

                if (numberField == null || isNaN(row[numberField.name]))
                    model.count++;
                else
                    model.count += row[numberField.name];
            }

            var re = '';
            for (var i = 0; i < group.length; i++) {
                var model = group[i];
                if (re) {
                    if (maxLine == 1)
                        re += ', ';
                    else
                        re += '<br>';
                }

                var line = model.name;
                var count = model.count;
                if (count > 1)
                    line += ' (' + count + ')';

                if (maxLine == 1)
                    line = re + line;

                if (maxLength != null && line.length > maxLength){
                    line = line.substr(0, maxLength - 3) + '... ';
                    if (maxLine > 1)
                        line+= '<br>';
                    line+='và'
                }

                if (maxLine == 1)
                    re = line;
                else
                    re+=line;

            }

            return re;
        };

        t.isDisposed = function () {
            var disposedStatus = (me.RequestStatusID.done |me.RequestStatusID.cancel);
            return t.request.RequestStatusID & disposedStatus;
        };
    };

    me.RequestStatusID = {open: 1, done: 2, cancel: 4};
    me.GetListRequestStatus = function (onSuccess, onError, dontSkip) {
        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestAjax",
            "GetListRequestStatus",
            {},
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetRequestEditData = function (ID, onSuccess, onError, dontSkip) {
        var data = {
            ID: ID,
        };

        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestAjax",
            "GetRequestEditData",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetReportingLine = function (domain, onSuccess, onError, dontSkip) {
        var data = {
            dataSearch: {
                SearchDataTypeID: 8,
                SearchValue: '',
                Constrains: [
                    {
                        SearchDataTypeID: 8,
                        SearchValue: domain,
                        ConstrainName: 'reporting-emp',
                        IsCheckOnly: false
                    }
                ]
            }
        };

        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestFieldDataAjax",
            "Search",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetRequestDefinition = function (workFlowID, onSuccess, onError, dontSkip) {
        var data = {
            workFlowID: workFlowID
        };

        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestAjax",
            "GetRequestDefinition",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetRequestDefinitionIDByDisplayName = function (displayName, onSuccess, onError, dontSkip) {
        var data = {
            displayName: displayName
        };

        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestAjax",
            "GetRequestDefinitionIDByDisplayName",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetListRequest = function (displayname, startindex, limit, onSuccess, onError, dontSkip) {
        var data = {
            displayname: displayname,
            startindex: startindex,
            limit: limit
        };

        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestAjax",
            "GetListRequest",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.ApproveRequestStep = function (requestID, stepIndex, isApproved, comment, attachments, field, onSuccess, onError, dontSkip) {

        var data = {
            requestID: requestID,
            stepIndex: stepIndex,
            isApproved: isApproved,
            comment: comment,
            field: field,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "ApproveRequestStep",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.CancelRequest = function (requestID, comment, attachments, onSuccess, onError, dontSkip) {
        var data = {
            ID: requestID,
            comment: comment,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "CancelRequest",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.PickupStepApproval = function (stepID, comment, attachments, onSuccess, onError, dontSkip) {
        var data = {
            stepID: stepID,
            comment: comment,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "PickupStepApproval",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.RaiseRequestStep = function (stepID, comment, attachments, onSuccess, onError, dontSkip) {
        var data = {
            ID: stepID,
            comment: comment,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "RaiseRequestStep",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.CreateRequest = function (workflowID, field, comment, onSuccess, onError, dontSkip) {
        var data = {
            workflowID: workflowID,
            field: JSON.stringify(field),
            comment: comment
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "CreateRequest",
            data,
            APIData.Ajax.Callback.string,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.EntrustRequestStep = function (stepID, domain, comment, attachments, onSuccess, onError, dontSkip) {

        var data = {
            stepID: stepID,
            domain: domain,
            comment: comment,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "EntrustRequestStep",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.GetUserRequestsByStatus = function (statusID, onSuccess, onError, dontSkip) {
        var data = {
            statusID: statusID
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "GetUserRequestsByStatus",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.SearchUserRequestsByStatus = function (statusIDs, onSuccess, onError, dontSkip) {
        me.ITAM(
            APIData.Workflow.Service,
            "AMRequestAjax",
            "SearchUserRequestsByStatus",
            {statusIDs: statusIDs},
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetRequestsForApproval = function (onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "GetRequestsForApproval",
            {},
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.GetUserRecentRequests = function (onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "GetUserRecentRequests",
            {},
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.UpdateRequest = function (requestID, field, onSuccess, onError, dontSkip) {

        var data = {
            requestID: stepID,
            field: field
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "EntrustRequestStep",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.GetListSubStepData = function (requestID, onSuccess, onError, dontSkip) {

        var data = {
            requestID: requestID,
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "GetListSubStepData",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.RequestComment = function (stepID, mentionDomains, questionComment, attachments, onSuccess, onError, dontSkip) {
        var data = {
            stepID: stepID,
            mentionDomains: mentionDomains,
            comment: questionComment,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "RequestComment",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.AnswerComment = function (stepID, mentionDomains, answerComment, attachments, onSuccess, onError, dontSkip) {
        var data = {
            stepID: stepID,
            mentionDomains: mentionDomains,
            comment: answerComment,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "AnswerComment",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.Comment = function (stepID, comment, mentionDomains, attachments, onSuccess, onError, dontSkip) {
        var data = {
            stepID: stepID,
            mentionDomains: mentionDomains,
            comment: comment,
            attachments: attachments
        };

        me.ITAM(
            me.Service,
            "AMRequestAjax",
            "Comment",
            data,
            APIData.Ajax.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
};
