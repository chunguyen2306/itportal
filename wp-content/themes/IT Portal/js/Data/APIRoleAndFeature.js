APIData = window.APIData || {};

APIData.RoleAndFeature = {};
APIData.RoleAndFeature = new (function () {
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'RoleAndFeature',
        'type': 'APIRoleAndFeature'
    });

    this.GetAllFeatures = function GetAllFeatures(pageindex, limit, callback) {
        me.send({
            pageindex: pageindex,
            limit: limit
        }, callback);
    };

    this.CreateFeature = function CreateFeature(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.DeleteFeature = function DeleteFeature(id, callback) {
        me.send({
            id: id
        }, callback);
    };

    this.UpdateFeature = function UpdateFeature(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.CreateFeaturesRole = function CreateFeaturesRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.UpdateFeaturesRole = function UpdateFeaturesRole(data, callback) {
        me.send({
            data: data
        }, callback);
    }

    this.DeleteFeatureRole = function DeleteFeatureRole(id, callback) {
        me.send({
            id: id
        }, callback);
    }

    this.GetAllFeaturesRole = function GetAllFeaturesRole(pageindex, limit, callback) {
        me.send({
            pageindex: pageindex,
            limit: limit
        }, callback);
    };
})();