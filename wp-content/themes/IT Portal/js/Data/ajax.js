APIData = window.APIData || {};
APIData.Ajax = new function () {
    var t = this;
    t.CMDB  = function (url, method, pNames, pValues, callbackMethod, onSuccess, onError, dontSkip) {
        pNames.push('m');
        pValues.push(method);
        var data = {
            u: url,
            p: JSON.stringify(pNames),
            v: JSON.stringify(pValues),
            action: "amAsset",
            t: "AMAsset",
            m: "ServiceBus"
        }

        $.ajax({
            type: 'POST',
            url: Common.BaseUrl,
            data: data,
            success: callbackMethod(onSuccess,method, dontSkip),
            error: function (re) {
                onError && onError(re);

                if(location.hostname == 'itportal'){
                    console.error({ url: url, method: method, pNames: pNames, pValues: pValues, re: re });
                    Popup.error(re.responseText).setTitle('Không thể kết nối tới máy chủ');
                }
            }
        });
    };

    t.ITAM3 = function (pluginName, className, method, data, callbackMethod, onSuccess, onError, dontSkip) {
        data.action = pluginName;
        data.t = className;
        data.m = method;
        $.ajax({
            type: 'POST',
            url: "/am_ajax",
            data: data,
            success: callbackMethod(onSuccess,method, dontSkip),
            error: function (re) {
                onError && onError(re);
                if(location.hostname == 'itportal'){
                    console.error({pluginName: pluginName, className: className, method: method, data: data, re: re });
                    Popup.error('Không thể kết nối tới máy chủ');
                }
            }
        });
    };

    t.Callback = new function(){
        var t = this;

        function log(name, jData, type, ex) {
            if(location.hostname == 'itportal'){
                Popup.error(jData);
                console.warn("Data is not a \"" + type + "\" at: " + name);
            }
            try {
                console.warn(JSON.parse(jData));
            } catch (ex) {
                console.warn(jData);
            }
            ex && console.error(ex);
        }
        ;
        t.string = function (callback, name, skipIfEror) {
            return function (jData) {
                var obj;
                try {
                    obj = JSON.parse(jData);
                } catch (ex) {
                    log(name, jData, "string", ex);
                }

                if(obj == null && jData != "null"){
                    log(name, jData, "string");
                    if(skipIfEror)
                        return;
                }

                callback(obj, jData);
            };
        }
        t.object = function (callback, name, skipIfEror) {
            return function (jData) {
                var obj;
                try {
                    obj = JSON.parse(jData);
                } catch (ex) {
                    log(name, jData, "object", ex);
                }

                if(obj == null && jData != "null"){
                    log(name, jData, "object");
                    if(skipIfEror)
                        return;
                }

                callback(obj, jData);
            };
        }
        t.array = function (callback, name, skipIfEror) {
            return function (jData) {
                var res = null;
                try {
                    res = JSON.parse(jData);
                } catch (ex) {
                    log(name, jData, "array", ex);
                }

                if (res && Array.isArray(res.data) && res.hasOwnProperty("code")) {

                    callback(res, jData);
                } else {

                    log(name, jData, "array");
                    if (skipIfEror == false)
                        callback(null, jData);
                    return;
                }

            };
        }
        t.int = function (callback, name, skipIfEror) {
            return function (jData) {
                if (isNaN(jData)) {
                    log(name, jData, "int");
                    callback(null, jData);
                    return;
                }

                var number = null;
                try {
                    number = parseInt(jData);
                } catch (ex) {
                    log(name, jData, "int", ex);
                }

                number || log(name, jData, "int");

                if (number || skipIfEror == false)
                    callback(null, jData);

            };
        }
        t.float = function (callback, name, skipIfEror) {
            return function (jData) {
                if (isNaN(jData)) {
                    log(name, jData, "float");
                    callback(null, jData);
                    return;
                }

                var number = null;
                try {
                    number = parseFloat(jData);
                } catch (ex) {
                    log(name, jData, "float", ex);
                }

                number || log(name, jData, "float");

                if (number || skipIfEror == false)
                    callback(null, jData);
            };
        }
    };

    t.CmdbCallback = new function(){
        var t = this;

        function log(name, jData, type, ex) {
            if(location.hostname == 'itportal'){
                Popup.error(jData);
                console.warn("Data is not a \"" + type + "\" at: " + name);
            }
            try {
                console.warn(JSON.parse(jData));
            } catch (ex) {
                console.warn(jData);
            }
            ex && console.error(ex);
        }
        ;
        t.string = function (callback, name, skipIfEror) {
            return callback;
        }
        t.object = function (callback, name, skipIfEror) {
            return function (jData) {
                var obj = null;
                try {
                    obj = JSON.parse(jData);
                } catch (ex) {
                    log(name, jData, "object", ex);
                }
                if(obj == null && jData != "null"){
                    log(name, jData, "object");
                    if(skipIfEror)
                        return;
                }

                callback(obj, jData);
            };
        }
        t.array = function (callback, name, skipIfEror) {
            return function (jData) {
                var res = null;
                try {
                    res = JSON.parse(jData);
                } catch (ex) {
                    log(name, jData, "array", ex);
                }

                if (Array.isArray(res)) {

                    callback(res, jData);
                } else {

                    log(name, jData, "array");
                    if (skipIfEror == false)
                        callback(null, jData);
                    return;
                }

            };
        }
        t.int = function (callback, name, skipIfEror) {
            return function (jData) {
                if (isNaN(jData)) {
                    log(name, jData, "int");
                    callback(null, jData);
                    return;
                }

                var number = null;
                try {
                    number = parseInt(jData);
                } catch (ex) {
                    log(name, jData, "int", ex);
                }

                number || log(name, jData, "int");

                if (number || skipIfEror == false)
                    callback(null, jData);

            };
        }
        t.float = function (callback, name, skipIfEror) {
            return function (jData) {
                if (isNaN(jData)) {
                    log(name, jData, "float");
                    callback(null, jData);
                    return;
                }

                var number = null;
                try {
                    number = parseFloat(jData);
                } catch (ex) {
                    log(name, jData, "float", ex);
                }

                if(obj == null && jData != "null"){
                    log(name, jData, "float");
                    if(skipIfEror)
                        return;
                }

                callback(number, jData);
            };
        }
    };
};

AMResponseCode = HTTPSTATUSCODE = {
    // custom
    paramsInvalid: 601,
    jsonFailed: 611,
    dbFailed: 621,
    exception: 701,
    busFailed: 801,

    // html code standard
    ok: 200,
    denied: 403,
    notFound: 404

}