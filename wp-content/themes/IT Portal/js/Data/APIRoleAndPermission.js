APIData = window.APIData || {};

APIData.RoleAndPermission = {};
APIData.RoleAndPermission = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'RoleAndPermission',
        'type': 'APIRoleAndPermission'
    });

    this.checkUserPermission = function checkUserPermission(feature, callback) {
        me.send({
            feature: feature
        }, callback);
    };

    this.GetAllRole = function GetAllRole(keyword, pageindex, limit, callback) {
        me.send({
            keyword: keyword,
            pageindex: pageindex,
            limit: limit
        }, callback);
    };

    this.GetAllUserRole = function GetAllUserRole(keyword, pageindex, limit, callback) {
        me.send({
            keyword: keyword,
            pageindex: pageindex,
            limit: limit
        }, callback);
    };

    this.GetUserRoleByDomain = function GetUserRoleByDomain(domain, callback) {
        me.send({
            domain: domain
        }, callback);
    };

    this.GetDeptHeadByDeptName = function GetDeptHeadByDeptName(deptName, callback) {
        me.send({
            deptName: deptName
        }, callback);
    };

    this.CreateRole = function CreateRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.UpdateRole = function UpdateRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.DeleteRole = function DeleteRole(id, callback) {
        me.send({
            id: id
        }, callback);
    };

    this.CreateUserRole = function CreateUserRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.UpdateUserRole = function UpdateUserRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.DeleteUserRole = function DeleteUserRole(id, callback) {
        me.send({
            id: id
        }, callback);
    };

    this.checkUserPermission = function checkUserPermission(feature, callback){
        me.send({
            feature: feature
        }, callback);
    }
})();