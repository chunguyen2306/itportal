APIData.Resources = new (function () {
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'Resources'
    });

    this.GetUserAsset = function GetUserAsset(username, asset, keyword, pageIndex, limit, callback, isForHelpDesk) {
        var data = {
            username: username,
            keyword: keyword,
            limit: limit,
            asset: asset,
            pageIndex: pageIndex,
            viewType: 'detail',
            isForHelpDesk: isForHelpDesk === undefined ? false : isForHelpDesk
        }
        me.send(data, callback);
    };
})();