
APIData.HelpdeskConsole = new (function(){
	
	var me = this;
	
	this.makeRequest = function(_class, method, data, callback){
		data['action'] = 'amHelpdeskConsole';
		data['t'] = _class;
		data['m'] = method;
		$.ajax({
			url: APIData.BEUrl,
			data: data,
			method: 'POST',
			success: callback,
			error: APIData.OnError
		});
	}
	
	this.User = new (function(){
		
		var meUser = this;
		var _class = 'User';
		
		this.change_user_license = function(account, licenses, callback){
			me.makeRequest(_class, 'change_user_license', {
				account: account,
				licenses: licenses
			}, callback);
		}
		
		this.create_service_mail = function(accountEnabled, displayName, mailNickname, userPrincipalName, owner, callback){
			me.makeRequest(_class, 'create_service_mail', {
				accountEnabled: accountEnabled,
				displayName: displayName,
				mailNickname: mailNickname,
				userPrincipalName: userPrincipalName,
				owner: owner
			}, callback);
		}
		
		this.get_user = function(account, callback){
			me.makeRequest(_class, 'get_user', {
				account: account
			}, callback);
		} 
		
		this.get_user_manager = function(account, callback){
			me.makeRequest(_class, 'get_user_manager', {
				account: account
			}, callback);
		} 
		
		this.get_user_member_of = function(account, callback){
			me.makeRequest(_class, 'get_user_member_of', {
				account: account
			}, callback);
		} 
		
		this.reset_user_password = function(account, callback){
			me.makeRequest(_class, 'reset_user_password', {
				account: account
			}, callback);
		} 
		
		this.update_account_status = function(account, status, callback){
			me.makeRequest(_class, 'update_account_status', {
				account: account,
				status: status
			}, callback);
		} 
		
		this.update_service_mail_owner = function(account, owner, callback){
			me.makeRequest(_class, 'update_service_mail_owner', {
				account: account,
				owner: owner
			}, callback);
		} 
		
		this.update_user = function(account, data, callback){
			me.makeRequest(_class, 'update_user', {
				account: account,
				data: data
			}, callback);
		} 
		
		this.get_domains = function(callback){
			me.makeRequest(_class, 'get_domains', {}, callback);
		}

        this.get_licenses = function(callback){
            me.makeRequest(_class, 'get_licenses', {}, callback);
        }
    })();
	
	this.Group = new (function(){
		
		var meGroup = this;
		var _class = 'Group';
		
		this.get_group_by_id = function(id, callback){
			me.makeRequest(_class, 'get_group_by_id', {
				id: id
			}, callback);
		} 
		
		this.get_group_by_name = function(name, callback){
			me.makeRequest(_class, 'get_group_by_name', {
				groupName: name
			}, callback);
		} 
		
		this.get_group_owner = function(id, callback){
			me.makeRequest(_class, 'get_group_owner', {
				id: id
			}, callback);
		} 
		
		this.update_group = function(id, newInfo, callback){
			me.makeRequest(_class, 'update_group', {
				id: id,
				data: newInfo
			}, callback);
		} 
		
		this.update_group_owner = function(id, owner, callback){
			me.makeRequest(_class, 'update_group_owner', {
				id: id,
				owner: owner
			}, callback);
		} 
		
	})();
	
})();