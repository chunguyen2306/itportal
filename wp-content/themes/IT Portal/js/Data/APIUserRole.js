APIData = window.APIData || {};

APIData.UserRoles = {};
APIData.UserRoles = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'RoleAndPermission',
        'type': 'APIUserRole'
    });

    this.GetAllRole = function GetAllRole(keyword, pageindex, limit, callback) {
        me.send({
            keyword: keyword,
            pageindex: pageindex,
            limit: limit
        }, callback);
    };

    this.GetAllUserRole = function GetAllUserRole(keyword, pageindex, limit, callback) {
        me.send({
            keyword: keyword,
            pageindex: pageindex,
            limit: limit
        }, callback);
    };


    this.CreateRole = function CreateRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.UpdateRole = function UpdateRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.DeleteRole = function DeleteRole(id, callback) {
        me.send({
            id: id
        }, callback);
    };
    this.CreateUserRole = function CreateUserRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.UpdateUserRole = function UpdateUserRole(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.DeleteUserRole = function DeleteUserRole(id, callback) {
        me.send({
            id: id
        }, callback);
    };
})();/**
 * Created by tientm2 on 6/26/2018.
 */
