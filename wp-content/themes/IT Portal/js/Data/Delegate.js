APIData = window.APIData || {};

APIData.RequestDelegate = new function () {
    var t = this;
    t.Service = "Workflow";
    APIData.AbstractAPIData.call(this, {
        'class': 'Workflow',
        'type': 'AMRequestDelegateAjax'
    });

    t.GetDelegate = function (domain, onSuccess, onError, dontSkip) {
        var data = {
            domain: domain
        };
        t.ITAM(
            t.Service,
            "AMRequestDelegateAjax",
            "GetDelegate",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    t.CanSettingForAnotherUser = function (onSuccess, onError, dontSkip) {
        var data = null;
        t.ITAM(
            t.Service,
            "AMRequestDelegateAjax",
            "CanSettingForAnotherUser",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    t.InsertDelegate = function (delegateData, onSuccess, onError, dontSkip) {
        var data = {
            data: delegateData
        };
        t.ITAM(
            t.Service,
            "AMRequestDelegateAjax",
            "InsertDelegate",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    t.UpdateDelegate = function (delegateData, onSuccess, onError, dontSkip) {
        var data = {
            data: delegateData
        };
        t.ITAM(
            t.Service,
            "AMRequestDelegateAjax",
            "UpdateDelegate",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    t.DeleteDelegate = function (domain, onSuccess, onError, dontSkip) {
        var data = {
            domain: domain
        };
        t.ITAM(
            t.Service,
            "AMRequestDelegateAjax",
            "DeleteDelegate",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };

    t.DelegateRequests = function (stepIDs, delegatedDomain, comment, attachments, onSuccess, onError, dontSkip) {
        var data = {
            stepIDs: stepIDs,
            delegatedDomain: delegatedDomain,
            comment: comment,
            attachments: attachments
        };
        t.ITAM(
            t.Service,
            "AMRequestDelegateAjax",
            "DelegateRequests",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
}

