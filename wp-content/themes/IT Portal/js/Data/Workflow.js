APIData = window.APIData || {};

APIData.Workflow = new function () {
    var me = this;
    var taskID = {
        reject: 0,
        approve: 1,
        ask: 2,
        answer: 3,
        cancel: 4,
        raise: 5,
        create: 6,
        reCreate: 7,
        done: 8,
        entrust: 9,
        comment: 10,
        pickup: 11
    };
    me.Service = "Workflow";
    APIData.AbstractAPIData.call(this, {
        'class': 'Workflow',
        'type': 'Workflow'
    });

    function getDistinctName(fullName, domain) {
        var num = domain.replace(/[^\d+]/g, '');
        if (num)
            num = ' (' + num + ')';
        else
            num = '';

        return fullName + num;
    }

    me.ActionTypeID = {notification: 1, assetState: 2, associateAsset: 4, subOrder: 3};
    me.RequestDataTypeID = {
        default: -1,
        string: 1,
        number: 2,
        asset: 3,
        assetModel: 4,
        table: 5,
        domain: 6,
        assetType: 7,
        user: 8,
        note: 9,
        intendTime: 10,
        excel: 11,
        parrentAsset: 12
    };
    me.RequestStatusID = {open: 1, done: 2, close: 3};
    me.RoleID = {deptHead: 7, user: 2, itManager: 8};
    me.TaskID = taskID;
    me.Tasks = [
        {id: taskID.reject, name: "Từ chối"},
        {id: taskID.approve, name: "Đồng ý"},
        {id: taskID.ask, name: "Yêu cầu thông tin"},
        {id: taskID.answer, name: "Cung cấp thông tin"},
        {id: taskID.cancel, name: "Hủy đơn hàng"},
        {id: taskID.raise, name: "Nhắc nhở"},
        {id: taskID.create, name: "Đặt hàng"},
        {id: taskID.reCreate, name: "Đặt hàng lại"},
        {id: taskID.done, name: "Hoàn tất đơn hàng"},
        {id: taskID.entrust, name: "Ủy quyền"},
        {id: taskID.comment, name: "Thêm ghi chú"},
        {id: taskID.pickup, name: "Nhận xử lý"},
    ];
    me.WorkflowEntity = function () {
        var t = this;

        t.ID = null;
        t.CreateDate = null;
        t.Creator = null;
        t.IsDelete = null;
        t.RequestReasonID = null;
        t.Description = null;
        t.Name = null;
        t.DisplayName = null;
    };
    me.RangeOfTimeInString = function (minTimeInHours, maxTimeInHours) {

        if (max == 0 && min == max)
            return 'Trong ít phút';

        var unit = 'giờ';
        var min = minTimeInHours;
        var max = maxTimeInHours;

        // if(maxTimeInHours > 167){
        //     min = minTimeInHours / 168;
        //     max = maxTimeInHours / 168;
        //     unit = 'tuần';
        // } else

        if (maxTimeInHours > 23) {
            min = minTimeInHours / 24;
            max = maxTimeInHours / 24;
            unit = 'ngày';
        }
        min = min | 0;
        max = Math.ceil(max);
        var isSame = min == max;

        if (max < 1)
            max = 1;

        if (min == 0) {
            return 'Trong ' + max + ' ' + unit;
        }

        if (isSame) {
            return max + ' ' + unit;
        } else {
            return 'Từ ' + (min | 0) + ' đến ' + (max | 0) + ' ' + unit;
        }
    }
    me.WorkflowFieldEntity = function (requestDataType) {
        var t = this;

        t.ID = null;
        t.Name = null;
        t.DisplayName = null;
        t.MappingName = null;
        t.RequestDataTypeID = requestDataType || 1;
        t.RequestDefinitionID = null;
        t.ParentID = null;
        t.cf = {
            style: {width: "32"},
            //submitedStyle: {}
        }
    };
    me.WorkflowStepEntity = function () {
        var t = this;

        t.ID = null;
        t.StepIndex = null;
        t.Name = null;
        t.DisplayName = null;
        t.RequestDefinitionID = null;
        t.RoleID = null;
        t.MinIntendDuration = 3;
        t.MaxIntendDuration = 24;

        t.listAction = [];
    };
    me.WorkflowStepActionEntity = function () {
        var t = this;

        t.ID = null;
        t.Name = null;
        t.Config = null;
        t.IsRollback = null;
        t.EventID = 1;
        t.RequestStepDefinitionID = null;

        t.cf = null;
    };
    me.NotificationActionConfigEntity = function () {
        var t = this;
        t.channel = {mail: true, chat: true, itPortal: true};
        t.to = '';
        t.subject = "[ItPortal]Tình trạng biên bản thay đổi";
        t.body = 'Xin chào ,<br>Đơn hàng của anh/chị đã được duyệt bởi' +
            ' @ref({"value":-1,"type":"text","srcKey":"user"}) <br>Cảm ơn anh chị đã sử dụng dịch vụ của phòng IT';
    };
    me.AssetStateActionConfigEntity = function () {
        var t = this;
        t.list = [];
        t.newState = null;
    };
    me.AssociateAssetActionConfigEntity = function () {
    };
    me.SubOrderActionConfigEntity = function () {
        var t = this;
        t.workflowID = null;
        t.fieldMapping = {};
        t.stepMapping = {};
        t.condition = {
            operator: null,
            expressions: []
        };
    };
    me.RequestStepEntity = function () {
        var t = this;
        t.ID = null;
        t.DefinitionID = null;
        t.ActionDate = null;
        t.StartDate = null;
        t.RequestID = null;
        t.Owner = null;
    };
    me.RequestSub = function (wfID) {
        var t = this;
        t.ID = null;
        t.StepDefinitionID = null;
        t.RequestDefinitionID = wfID;
        t.condition = {};
    }

    me.EditAndValidateWorkflow = function (data) {
        var ev = new EditAndValidate(data);
        ev.date("CreateDate", "Ngày tạo qui trình", 0);
        ev.string("Creator", "Người tạo qui trình", 0);
        ev.int("RequestReasonID", "Lý do", 0);
        ev.string("Description", "Mô tả", 1);
        ev.string("Name", "Tên code", 1);
        ev.string("DisplayName", "Tên qui trình", 1);

        return ev.getMessage();
    };
    me.EditAndValidateStepWorkflow = function (data) {
        var ev = new EditAndValidate(data);
        ev.int("StepIndex", "Thứ tự bước", 0, 999999);
        ev.string("DisplayName", "Tên bước", 1, 50);

        if (data.StepIndex > 0) {
            if (typeof data.Role == "string")
                ev.string("Role", "Quyền thực hiện", 1, 200);
            else
                ev.int("Role", "Quyền thực hiện", 0, 200);
        }

        return ev.getMessage();
    };
    me.EditAndValidateStepActionWorkflow = function (data) {
        var ev = new EditAndValidate(data);
        ev.string("Name", "Tên hành động", 1, 50);
        if (data.ParamConfig == null || ParamConfig == "")
            ev.addMsg("Cấu hình tham số chưa đầy đủ");

        ev.int("RequestStepDefinitionID", "Không tìm thấy bước", 0);

        return ev.getMessage();
    };
    me.EditAndValidateFieldWorkflow = function (data) {
        var ev = new EditAndValidate(data);
        ev.string("DisplayName", "Tên trường", 0, 50);
        ev.string("Name", "Tên code", 0, 50);

        if (data.RequestDataTypeID == me.RequestDataTypeID.table) {
            var lChild = data.listChild;
            if (Array.isArray(lChild) == false || lChild.length < 1)
                ev.addMsg("Bảng " + data.DisplayName + " phải có ít nhất một cột");
            else if (lChild.length > 10)
                ev.addMsg("Bảng " + data.DisplayName + " chỉ có thể tối đa 10 cột");
            else {
                for (var i = 0; i < lChild.length; i++) {
                    var evChild = new EditAndValidate(lChild[i]);
                    evChild.string("DisplayName", "Tên cột", 0, 50);
                    evChild.string("Name", "Tên code", 0, 50);
                    var msg = evChild.getMessage();
                    if (msg) {
                        ev.addMsg(msg);
                        break;
                    }
                }
            }
        }

        return ev.getMessage();
    };

    me.GetWorkflow = function (ID, onSuccess, onError, dontSkip) {
        var data = {
            ID: ID
        };

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "GetWorkflow",
            data,
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetListWorkflow = function (onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "GetListWorkflow",
            {},
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetWorkflowEditData = function (ID, onSuccess, onError, dontSkip) {
        var data = {
            ID: ID,
        };

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "GetWorkflowEditData",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetWorkflowData = function (ID, onSuccess, onError, dontSkip) {
        var data = {
            ID: ID,
        };

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "GetWorkflowData",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.GetListWorkflowField = function (ID, onSuccess, onError, dontSkip) {
        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "GetListWorkflowField",
            {ID: ID},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.InsertWorkflow = function (workflow, listFieldDefinition, listStepDefinition, onSuccess, onError, dontSkip) {
        var data = {
            workflow: JSON.stringify(workflow),
            listFieldDefinition: JSON.stringify(listFieldDefinition),
            listStepDefinition: JSON.stringify(listStepDefinition),
        };

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "InsertWorkflow",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.UpdateWorkflow = function (workflow, listFieldDefinition, listStepDefinition, onSuccess, onError, dontSkip) {
        var data = {
            workflow: JSON.stringify(workflow),
            listFieldDefinition: JSON.stringify(listFieldDefinition),
            listStepDefinition: JSON.stringify(listStepDefinition),
        };

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "UpdateWorkflow",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.DeleteWorkflow = function (ID, onSuccess, onError, dontSkip) {
        var data = {
            ID: ID,
        };

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "DeleteWorkflow",
            data,
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    };
    me.ApproveRequestStep = function (stepID, isApprowed, onSuccess, onError, dontSkip) {
        var data = {
            stepID: stepID,
            isApprowed: isApprowed
        };

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "ApproveRequestStep",
            data,
            APIData.Callback.string,
            onSuccess,
            onError,
            dontSkip
        );
    }

    me.GetCurrentUser = function (onSuccess, onError, dontSkip) {

        me.ITAM(
            me.Service,
            "AMWorkflowAjax",
            "GetCurrentUser",
            {},
            APIData.Callback.object,
            onSuccess,
            onError,
            dontSkip
        );
    }
    me.GetUserInformation = function (keyword, start, limit, onSuccess, onError, dontSkip) {
        APIData.User.GetUserInformation(keyword, start, limit, function (res) {
            if (res.code == AMResponseCode.ok) {
                var re = [];
                res.data.forEach(function (item) {
                    re.push({
                        Domain: item.domainAccount,
                        Name: item.firstName,
                        DistinctName: getDistinctName(item.fullName, item.domainAccount),
                        DepartmentName: item.departmentName,
                        JobTitle: item.jobTitle,
                        Seat: item.seat,
                        Location: item.location,
                    })
                });

                res.data = re;
            }
        });
    }
}

// APIData.User = new function () {
//     var me = this;
//     APIData.AbstractAPIData.call(this, {
//         'class': 'User',
//         'type': 'AMUserAjax'
//     });
//
//     me.GetUser = function (domain, onSuccess, onError, dontSkip) {
//         var data = {
//             domain: domain
//         };
//         me.ITAM(
//             me.Service,
//             "AMWorkflowAjax",
//             "GetUser",
//             data,
//             APIData.Callback.array,
//             onSuccess,
//             onError,
//             dontSkip
//         );
//     };
//
//     this.GetUserByDomain = function GetUserByDomain(domainAccount, callback) {
//         me.send({
//             domainAccount: domainAccount
//         }, callback);
//     };
//
//     /*
//     me.GetUserByDomain = function (data, start, count, onSuccess, onError, dontSkip) {
//         var data = {
//             data: data
//         };
//
//         me.ITAM(
//             me.Service,
//             "AMWorkflowAjax",
//             "GetUserByDomain",
//             data,
//             APIData.Callback.array,
//             onSuccess,
//             onError,
//             dontSkip
//         );
//     }
//     */
// }
