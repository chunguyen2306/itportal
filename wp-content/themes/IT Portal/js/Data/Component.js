APIData.Component = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'CMDB',
        'type': 'Component'
    });

    this.paging = function Paging(keyword, pageIndex, limit, callback) {
        me.send({
            keyword: keyword,
            pageIndex: pageIndex,
            limit: limit
        }, callback);
    };

    this.delete = function Delete(id, callback){
        me.send({
            id: id
        }, callback);
    };

    this.create = function CreateNew(data, callback) {
        me.send({
            newData: data
        }, callback);
    };

    this.update = function Update(data, callback) {
        me.send({
            newData: data
        }, callback);
    };

    this.GetQuantityInHand = function GetQuantityInHand(modelID, callback) {
        me.send({
            modelID: modelID
        }, callback);
    };
    this.GetByID = function GetByID(id, callback) {
        me.send({
            id: id
        }, callback);
    };
    
    this.getByTypeAndName = function GetByTypeAndName(typeName, modelName, pageIndex, limit, callback) {
        me.send({
            typeName: typeName,
            modelName: modelName,
            pageIndex: pageIndex,
            limit: limit
        }, callback);
    };

    this.GetProductSlideByType = function GetProductSlideByType(type, callback) {
        me.send({
            type: type
        }, callback);
    };

    this.SearchPaging = function SearchPaging(keyword, pageIndex, limit, isOwned, userDomain, type, callback) {
        me.send({
            keyword: keyword,
            pageIndex: pageIndex,
            limit: limit,
            isOwned: isOwned,
            userDomain: userDomain,
            type: type
        }, callback);
    }
})();