APIData = window.APIData || {};

APIData.Asset = {};
APIData.Asset.Service = "amAsset";
APIData.Asset.InsertAssetDetailHistory = function (assetName, newDetail, previousDetail, comment, sysComment, userAction, onSuccess, onError, dontSkip) {
    var data = {
        AssetName: assetName,
        NewDetail: newDetail,
        PreviousDetail: previousDetail,
        UserAction: userAction || Common.getCurrentUser(),
        Comment: comment,
        SysComment: sysComment
    };

    APIData.Ajax.ITAM3(
        APIData.Asset.Service,
        "AMAssetDetailHistory",
        "InsertAssetDetailHistory",
        {data: JSON.stringify(data)},
        APIData.Ajax.Callback.object,
        onSuccess,
        onError,
        dontSkip
    );
};
APIData.Asset.GetAssetDetailHistory = function (assetName, onSuccess, onError, dontSkip) {

    APIData.Ajax.ITAM3(
        APIData.Asset.Service,
        "AMAssetDetailHistory",
        "GetAssetDetailHistory",
        {assetName: assetName },
        APIData.Ajax.Callback.array,
        onSuccess,
        onError,
        dontSkip
    );
};
APIData.Asset.GetAssetStateHistoryByCMDBHistoryID = function (cmdbHistoryID, onSuccess, onError, dontSkip) {
    APIData.Ajax.ITAM3(
        APIData.Asset.Service,
        "AMAssetStateHistory",
        "GetAssetStateHistoryByCMDBHistoryID",
        {CMDBHistoryID: cmdbHistoryID },
        APIData.Ajax.Callback.object,
        onSuccess,
        onError,
        dontSkip
    );
};
APIData.Asset.InsertStateHistory = function (stateId, assetName, newState, previousState, actionDate, userAction, targetUser, comment, sysComment, onSuccess, onError, dontSkip) {

    actionDate = new Date(actionDate);
    actionDate.setHours(actionDate.getHours() - actionDate.getTimezoneOffset() / 60);
    var data = {
        stateId: stateId,
        assetName: assetName,
        newState: newState,
        previousState: previousState,
        actionDate: actionDate,
        userAction: userAction,
        comment: comment,
        sysComment: sysComment
    };

    APIData.Ajax.ITAM3(
        APIData.Asset.Service,
        "AMAssetStateHistory",
        "InsertStateHistory",
        {data: JSON.stringify(data)},
        APIData.Ajax.Callback.object,
        onSuccess,
        onError,
        dontSkip
    );
};

APIData.Asset.GetAssetStateHistory = function (assetName, onSuccess, onError, dontSkip) {

    APIData.Ajax.ITAM3(
        APIData.Asset.Service,
        "AMAssetStateHistory",
        "GetAssetStateHistory",
        {assetName: assetName },
        APIData.Ajax.Callback.array,
        onSuccess,
        onError,
        dontSkip
    );
};

var AssetState = {
    InUse: "In Use",
    InUseAssociate: "In Use(associate)",
    InStoreAssetState: "In Store",
    Expired: "Expired",
    Disposed: "Disposed",
    InWarranty: "In Warranty",
    Lost: "Lost",
    Retired: "Retired",
    Delivered: "Delivered",
    Error: "Error"
};