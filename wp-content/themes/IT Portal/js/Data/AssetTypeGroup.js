APIData = window.APIData || {};

APIData.AssetTypeGroup = {};
APIData.AssetTypeGroup = new (function () {
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Asset',
        'type': 'APIAssetTypeGroup'
    });

    this.getByAssetType = function getByAssetType(assetType, callback) {
        me.send({
            assetType: assetType
        }, callback);
    }

    this.getByGroupName = function getByGroupName(groupName,callback) {

        me.send({
            groupName:groupName
        },callback);
    }
    this.getByDefault = function getByDefault(callback) {
        me.send({},callback);
    }

    this.Update = function update(data, callback) {
        me.send({
            data:data
        }, callback);
    }

    this.AddNew = function AddNew(data, callback) {
        me.send({
           data:data
        }, callback);
    }

    this.getAll = function getAll(data,callback) {
        me.send({
            data:data
        },callback);
    }
    this.getAllandPercent = function getAllandPercent(data,callback) {
        me.send({
            data:data
        },callback);
    }
    this.deletegroup = function deletegroup(id,callback) {
        me.send({
            id:id
        },callback);
    }
})();
