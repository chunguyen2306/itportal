APIData = window.APIData || {};

APIData.Options = {};
APIData.Options = new (function(){
    var me = this;
    APIData.AbstractAPIData.call(this, {
        'class': 'Options',
        'type': 'AMOptions'
    });

    this.GetAll = function GetAll(callback) {
        me.send({
        }, callback);
    };
    this.GetByID = function GetByID(ID, callback) {
        me.send({
            ID: ID
        }, callback);
    };
    this.AddNew = function AddNew(data, callback) {
        me.send({
            data: data
        }, callback);
    };
    this.Update = function Update(data, callback) {
        me.send({
            data: data
        }, callback);
    };

    this.GetByCodeAndName = function GetByCodeAndName(optionName, optionCode, callback) {
        me.send({
            optionName: optionName,
            optionCode: optionCode
        }, callback);
    };

    this.Delete = function Delete(data, callback) {
        me.send({
            data: data
        }, callback);
    };
})();