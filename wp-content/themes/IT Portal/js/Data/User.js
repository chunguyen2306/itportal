APIData = window.APIData || {};

APIData.User = new function () {
    var t = this;
    t.Service = "User";
    APIData.AbstractAPIData.call(this, {
        'class': 'AMUserAjax',
        'type': 'User'
    });

    t.GetUserInformation = function (keyword, start, limit, onSuccess, onError, dontSkip) {
        t.ITAM(
            t.Service,
            "AMUserAjax",
            "GetUserInformation",
            {keyword: keyword, start: start || 0, limit: limit || 5},
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };

    APIData.AbstractAPIData.call(this, {
        'class': 'User',
        'type': 'AMUserAjax'
    });

    t.GetUser = function (domain, onSuccess, onError, dontSkip) {
        var data = {
            domain: domain
        };
        t.ITAM(
            t.Service,
            "AMWorkflowAjax",
            "GetUser",
            data,
            APIData.Callback.array,
            onSuccess,
            onError,
            dontSkip
        );
    };

    t.GetUserByDomain = function GetUserByDomain(domainAccount, callback) {
        t.send({
            domainAccount: domainAccount
        }, callback);
    };
}

