<?php
$amPage = get_query_var('am-page');
$amDir = get_query_var('am-page-dir');
$ignorePage = using('theme.package.Includes.User.IgnoreLoginPage');

if ( ( is_single() || is_front_page() || is_page() ) && !is_page('login') && !is_user_logged_in() && !in_array($amPage, $ignorePage)){
    ?>
    <script>
        var query = '?';
        var redirect_to = 'redirect_to=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>';
        query += redirect_to + '&external=cas';
        window.location = '/wp-login.php'+query;
    </script>
    <?php
} else {
    if($amPage == 'am_ajax' || $amPage == 'resource-content'){
        import('theme.pages.'.$amPage);
    } else {
        $_BASEURL = get_template_directory_uri();
        get_header();

        do_action('am_content');

        get_footer();
    }
}
?>
