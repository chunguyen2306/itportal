<?php get_header('common'); ?>
    <link type="text/css" rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/main-shop.css?v=<?php echo date('hhmmss'); ?>" />
	<div class="header">
        <span class="logo"></span>
        <div class="search-header-block">
            <div class="menu-btn">
                <span class="glyphicon glyphicon-th"></span>
                <span>Menu</span>
            </div>
            <div class="search-block">
                <input type="text" placeholder="Enter product name to search"/>
                <span class="fa fa-search"></span>
            </div>
        </div>
        <div class="user-info-block">
            <a href="<?php echo esc_url( site_url( '/my-page' ) ); ?>"><span class="icon icon-btn icon-login">My page</span></a>
        </div>
	</div>
