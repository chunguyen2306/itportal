Module.View = function (_model, _onDone) {
    var me = this;
    var model = _model;
    var ComponentData = {};
    Component.Module.AbstractModuleView.call(this, model, _onDone );
    Config.Global.isInitMenuMouseHover = false;
    Config.Global.isInitMouseWheel = false;

    me.onDone();
};
