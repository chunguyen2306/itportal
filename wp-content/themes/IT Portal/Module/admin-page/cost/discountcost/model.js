Module.Model = function () {
    var me = this;
    this.ComponentTypes = ko.observableArray([]);
    this.arrayPercent = ko.observableArray([]);
    this.checkedComponentType = ko.observable([]);
    this.displaytablediscount = ko.observableArray([]);

    this.getMaxYear = function (model, e) {

        //thông báo nhận sự kiện khi nhập dữ liệu
        var form = Component.Custom.Form.items['DiscountPercentDetail'].Form;
        var maxmonth = form.val().maxmonth;
        var maxyear = Math.ceil(maxmonth / 12);
        me.arrayPercent([]);
        for (var i = 0; i < maxyear; i++) {
            var percentPerYear = {};
            percentPerYear.Year = i + 1;
            percentPerYear.Percent = '';
            me.arrayPercent().push(percentPerYear);
        }
        //rebiinding
        var temp = me.arrayPercent();
        me.arrayPercent([]);
        me.arrayPercent(temp);
    }
    this.do_SelectAll = function (model, e) {
        var checkboxs = $('[component="checkbox"]');
        checkboxs.each(function (i, item) {
            if ($(item).prop('checked') !== $(e.currentTarget).prop('checked')) {
                $(item).trigger('click');
            }
        })
    };
    var praseDiscordPerYear = function (strData) {

        var percentArray = strData.split(',');
        var year = 0;
        for (var i = percentArray.length - 1; i >= 0; i--) {
            var percentPerYear = {};
            year++;
            percentPerYear.Year = year;
            percentPerYear.Percent = percentArray[i];
            me.arrayPercent().push(percentPerYear);
        }
        me.arrayPercent(me.arrayPercent());
    };

    this.do_Submit = function (model, e) {
        Component.System.loading.show();
        var typeGroupActionFunc = null;
        var form = Component.Custom.Form.items['DiscountPercentDetail'].Form;

        var formData = form.val();
        if (formData.assettype == '[]') {
            formData.assettype = 'default';
        }

        if (formData.id !== '' && formData.id !== null && formData.id !== 0 && formData.id !== undefined) {
            typeGroupActionFunc = APIData.AssetTypeGroup.Update;
        }else {
            typeGroupActionFunc = APIData.AssetTypeGroup.AddNew;
        }
        typeGroupActionFunc(formData,function (res) {

            res=JSON.parse(res);
            if(res.State)
            {

                form.resetData();
                me.arrayPercent([]);
                me.checkedComponentType([]);
                if (Component.Custom.Popup.HasActive) {
                    Component.Custom.Popup.Active.hide();
                }
                loadData();
                Component.System.loading.hide();
                Component.System.alert.show('Thêm mới thành công');
            }
        });
    };
    this.do_btnUpdate = function (model, e) {
        me.checkedComponentType([]);
        if (model.assettype !== 'default') {
            var parsedTypes = JSON.parse(model.assettype);
            if (Array.isArray(parsedTypes)) {
                me.checkedComponentType(parsedTypes);
            }
        }

        me.arrayPercent(JSON.parse(model.PercentList));
        me.arrayPercent(me.arrayPercent().sort(function (a, b) {
            return a.Year>b.Year;
        }));
        var form = Component.Custom.Form.items['DiscountPercentDetail'].Form;
        form.val(model);
        Component.Custom.Popup.items['edit-group'].Popup.show();
    };
    this.do_btnDelete = function (model, e) {
        Component.System.alert.show('Bạn có chắc muốn xóa nhóm ', {
            name: 'Đồng ý', cb: function () {
                Component.System.loading.show();
                APIData.AssetTypeGroup.deletegroup(model.id, function (res) {
                    res = JSON.parse(res);
                    if (res.State) {
                        APIData.AssetDiscountPercentYear.deletegroup(model.id, function (rs) {
                            rs = JSON.parse(rs);

                            if (rs.State) {
                                if (Component.Custom.Popup.HasActive) {
                                    Component.Custom.Popup.Active.hide();
                                }
                                loadData();
                                Component.System.loading.hide();
                                Component.System.alert.show('Xóa hoàn tất');
                            } else{
                                Component.System.alert.show('Xóa thất bại');
                                Component.System.loading.hide();
                            }

                        });
                    } else{
                        Component.System.loading.hide();
                        Component.System.alert.show('Xóa group thất bại');
                    }

                });
            }
        }, {
            name: 'Hủy', cb: function () {
                Component.System.alert.hide();
            }
        });

    }
    this.do_btnAddNew = function () {
        me.checkedComponentType([]);
        me.arrayPercent([]);
    };
    this.do_CancelSubmit = function () {
        var form = Component.Custom.Form.items['DiscountPercentDetail'].Form;
        form.resetData();
        me.arrayPercent([]);
        me.checkedComponentType([]);
        if (Component.Custom.Popup.HasActive) {
            Component.Custom.Popup.Active.hide();
        }
    };


    var loadData = function () {

        APIData.AssetTypeGroup.getAllandPercent('', function (res) {
            res = JSON.parse(res);
            me.displaytablediscount(res);
        });
    };

    this.getSimpleDiscountPerYear = function (data) {
        return JSON.parse(data).map(function(elem){
            return elem.Percent;
        }).join(",");
    };

    this.On_DiscountPerYearChange = function(newVal){

        var form = Component.Custom.Form.items['DiscountPercentDetail'].Form;
        form.field('PercentList').setValue(JSON.stringify(me.arrayPercent()));
    };

    var init = function () {
        APIData.CMDB.GetListComponentType('', 0, 200, function (res) {
            me.ComponentTypes(res);
        });

        me.checkedComponentType.subscribe(function(val) {
            var form = Component.Custom.Form.items['DiscountPercentDetail'].Form;
            form.field('assettype').setValue(JSON.stringify(val));
        });

        me.arrayPercent.subscribe(me.On_DiscountPerYearChange);

        loadData();
    };
    init();
};