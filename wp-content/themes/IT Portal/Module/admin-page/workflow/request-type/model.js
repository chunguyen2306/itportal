Module.Model = function () {
    var me = this;
    this.RequestCategorys = ko.observableArray([]);
    this.Data = ko.observableArray([]);

    this.btnSubmit = function () {
        var form = Component.Custom.Form.items.FormData.Form;
        var data = form.val();
        var actionFunc = null;
        if(data.ID === 0 || data.ID === '' || data.ID === null || data.ID === undefined){
            actionFunc = APIData.APIRequestType.AddNew;
        } else {
            actionFunc = APIData.APIRequestType.Update;
        }
        actionFunc(data, function (res) {
            res = JSON.parse(res);
            if(res.State){
                Component.Custom.Popup.items.Info.Popup.hide();
                Component.Custom.Form.items.FormData.Form.resetData();
                loadData();
                Component.System.alert.show('Thao tác thành công');
            } else {
                Component.System.alert.show('Thao tác thất bại');
            }
        })
    };

    this.btnCancel = function () {
        Component.Custom.Form.items.FormData.Form.resetData();
    };

    this.btnUpdate = function (model, e) {
        var form = Component.Custom.Form.items.FormData.Form;
        form.val(model);
        Component.Custom.Popup.items.Info.Popup.show();
    };

    this.btnDelete = function (model, e) {
        APIData.APIRequestType.Delete(model, function (res) {
            res = JSON.parse(res);
            if(res.State){
                Component.Custom.Popup.items.Info.Popup.hide();
                loadData();
                Component.System.alert.show('Xóa thành công');
            } else {
                Component.System.alert.show('Xóa thất bại');
            }
        })
    };

    this.btnAddNew = function () {
        Component.Custom.Form.items.FormData.Form.resetData();
    };

    var loadData = function () {
        APIData.APIRequestType.GetAll(function (res) {
            res = JSON.parse(res);
            if(res) {
                me.Data(res);
            }
        });
    };

    this.GetNameFormCateID = function (ID) {
        var item = me.RequestCategorys().find(function (item) {
            return item.ID === ID;
        });
        if(item){
            return item.Name;
        } else {
            return '';
        }
    };

    var init = function(){
        loadData();
        APIData.APIRequestCategory.GetAll(function (res) {
            res = JSON.parse(res);
            if(res) {
                me.RequestCategorys(res);
                Component.Custom.ComboBox.bindingItem();
            }
        });
    };

    init();
};