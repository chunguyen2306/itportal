function workflowConfig_ActionDefault() {

    /***********  DECLARE ************************/
    /*********************************************/
    var t = this;

    // các action tham khảo trong model
    var eventID = WFEventID;

    /**
     * các action đồng bộ sẽ được chạy lần lượt, xong cái này mới tới cái tiếp theo
     * tất cả các action phải hoàn tất thì việc approval, commen, ... mới được hoàn tất
     * */
    var actionSync = {yes: 1, no: 0};

    var actionTypeID = APIData.Workflow.ActionTypeID;

    /***********  END DECLARE ************************/


    /***********  CONFIG ************************/
    /*********************************************/
    var action_approve_noti_order_created = {
        Name: "Thông báo đơn hàng đã được tạo",
        cf: {
            channel: {mail: true, chat: true, itPortal: true, sms: false},
            to: "@ref({\"srcKey\":\"user\",\"value\":\"2:nguoi_yeu_cau\",\"type\":\"text\"}) ",
            subject: "[ITAM3] Đơn hàng đã được tạo",
            body: "Xin chào @ref({\"srcKey\":\"user\",\"value\":\"2:nguoi_yeu_cau\",\"type\":\"text\"})&nbsp;,<br>Yêu cầu của bạn đã được khởi tạo.<br>Nội dung biên bản:<br>@ref({\"srcKey\":\"info\",\"value\":\"_orderContent\",\"type\":\"text\"})&nbsp;<br><br>"
        },
        //IsRollback: 1,
        EventID: eventID.approve,
        IsSync: actionSync.no,
        ActionTypeID: actionTypeID.notification
    };
    var action_approve_noti_order_request_approval = {
        Name: "Thông báo cho người duyệt tiếp theo",
        cf: {
            channel: {mail: true, chat: true, itPortal: true},
            to: "@ref({\"srcKey\":\"user\",\"value\":\"-2\",\"type\":\"text\"})",
            subject: "[ITAM3] Đơn hàng chờ phê duyệt từ @ref({\"srcKey\":\"user\",\"value\":\"2:nguoi_yeu_cau\",\"type\":\"text\"})",
            body: "Xin chào&nbsp;<b style=\"color: rgb(242, 136, 75);\">@ref({\"srcKey\":\"user\",\"value\":\"-2\",\"type\":\"text\"})&nbsp;</b>,<br>@ref({\"srcKey\":\"user\",\"value\":\"2:nguoi_yeu_cau\",\"type\":\"text\"})&nbsp;có một yêu cầu cần sự phê duyệt của bạn.<br><br><b>Chi tiết yêu cầu:</b><br>@ref({\"srcKey\":\"field\",\"value\":\"chi_tiet_yeu_cau\",\"type\":\"text\"})&nbsp;<br>@ref({\"srcKey\":\"info\",\"value\":\"_approval\",\"type\":\"text\"})&nbsp;<br><br>@ref({\"srcKey\":\"info\",\"value\":\"_link\",\"type\":\"text\"})&nbsp;\n"
        },
        ///IsRollback: 1,
        EventID: eventID.approve,
        IsSync: actionSync.no,
        ActionTypeID: actionTypeID.notification
    }

    var action_reject_noti_order_cancel = {
        Name: "Thông báo đơn hàng đã được hủy",
        cf: {
            channel: {mail: true, chat: true, itPortal: true},
            to: "@ref({\"srcKey\":\"user\",\"value\":\"-1\",\"type\":\"text\"})",
            subject: "[ITAM3] Đơn hàng đã được hủy",
            body: "Xin chào&nbsp;@ref({\"srcKey\":\"user\",\"value\":\"2:nguoi_yeu_cau\",\"type\":\"text\"}),<br>Yêu cầu của bạn đã được hủy bỏ.&nbsp;<br><br><b>Nội dung biên bản:</b><br>@ref({\"srcKey\":\"info\",\"value\":\"_orderContent\",\"type\":\"text\"})&nbsp;<br>"
        },
        //IsRollback: 1,
        EventID: eventID.reject,
        IsSync: actionSync.no,
        ActionTypeID: actionTypeID.notification
    };
}