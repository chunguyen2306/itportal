function kolog(val) {
    console.log(val);
    return val;
}

function slideByMouseDrag(jo) {
    var dom = jo[0];
    var dx = 0;
    var preScroll = 0;
    var joWin = $(window);

    function mouseMove(e) {
        if (dx == 0)
            return;

        var scrollValue = preScroll - (e.screenX - dx);

        if (scrollValue <= 0) {
            if (dom.scrollLeft !== 0)
                dom.scrollLeft = 0;

            dx = e.screenX;
            preScroll = dom.scrollLeft;
        } else if (scrollValue >= dom.scrollWidth) {
            if (dom.scrollLeft !== dom.scrollWidth)
                dom.scrollLeft = dom.scrollWidth;

            dx = e.screenX;
            preScroll = dom.scrollLeft;
        } else {
            dom.scrollLeft = scrollValue;
        }
    }

    function mouseUp(e) {
        if (dx == 0)
            return;

        jo.removeClass('prevent-select-text');
        dx = 0;

        joWin.unbind("mousemove", mouseMove);
        joWin.unbind("mouseup", mouseUp);
    }

    jo.bind("mousedown", function (e) {
        if (e.currentTarget != dom)
            return;

        jo.addClass('prevent-select-text');
        dx = e.screenX;
        preScroll = dom.scrollLeft;


        joWin.bind("mousemove", mouseMove);
        joWin.bind("mouseup", mouseUp);
    });

}

function focusNext(el) {
    el.nextElementSibling.children[0].focus();
}

WFEventID = APIData.Workflow.TaskID
WFEvents = APIData.Workflow.Tasks;
ActionTypeID = APIData.Workflow.ActionTypeID;
DataTypeID = APIData.Workflow.RequestDataTypeID;
RoleID = APIData.Workflow.RoleID;

ko.bindingHandlers.init = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var fn = valueAccessor();
        if (typeof fn != 'function') {
            console.error("data-bind 'int' require be a function");
            return;
        }

        fn(bindingContext, element, allBindings());
    },
};

Module.Model = function () {
    // variable
    var t = this;

    var data = new APIData.Workflow.WorkflowEntity();
    t.data = ko.observable(data);
    t.listRole = ko.observableArray();
    t.listDataType = ko.observableArray();
    t.listReason = ko.observableArray();
    t.listAssetState = ko.observableArray();
    t.listWorkFlow = ko.observableArray();
    t.showTab = {
        info: ko.observable(0),
        field: ko.observable(0),
        step: ko.observable(1),
        infoChange: function () {
            t.showTab.info(!t.showTab.info())
        },
        fieldChange: function () {
            t.showTab.field(!t.showTab.field())
        },
        stepChange: function () {
            t.showTab.step(!t.showTab.step())
        }
    };
    t.animationend = function () {
        console.log(arguments);
    };
    t.infoShowChange = function () {
        t.showTab.info(!t.showTab.info());
    };
    t.listFieldShowChange = function () {
        t.showTab.field(!t.showTab.field());
    };
    t.stepCanOption = [{id: 0, name: "Không được phép"}, {id: 1, name: "Được phép"}];

    t.listStatus = ko.observableArray([]);

    var field = t.field = new function () {
        var f = this;
        f.list = ko.observableArray();
        f.editing = ko.observable().extend({numeric: 1});
        f.userFields = ko.observableArray();
        f.noteFields = ko.observableArray();

        f.add = function () {
            var item = new APIData.Workflow.WorkflowFieldEntity();
            item.RequestDataTypeID = DataTypeID.string;
            f.list.push(item);
        }

        f.edit = function (context) {
            return function () {
                var data = context.$data;
                f.editing(data);

                if (data.ParentID == null) {
                    f.editing.rootData = data;
                } else {
                    var parentContext = context.$parentContext;
                    var rootData = parentContext.$data;
                    while (rootData.ParentID != null) {
                        parentContext = parentContext.$parentContext;
                        rootData = parentContext.$data;
                    }
                    f.editing.rootData = rootData;
                }
            };
        }

        f.closeEdit = function () {
            var data = f.editing.rootData;

            data.cf.lineNumber && (data.cf.lineNumber = parseInt(data.cf.lineNumber));
            data.cf.colNumber && (data.cf.colNumber = parseInt(data.cf.colNumber));

            var l = data.listChild;
            if (l) {
                for (var i = l.length - 1; i > -1; i--) {
                    if (l[i] == null)
                        l.insert("", i);
                    else
                        break;
                }
            }

            f.list.update(data);
            f.editing(null);
        }

        f.remove = function (context) {
            var data = context.$data;
            if (data.ParentID) {
                return function () {
                    function doRemove() {
                        var parent = context.$parent;
                        parent.listChild.remove(data);
                        f.list.update(parent);
                    }

                    if (data.ID || data.DisplayName) {
                        Component.System.alert.show("Bạn có chắc muốn xóa cột <i>" + data.DisplayName + "</i>", {
                            name: "Xóa", cb: doRemove
                        }, {
                            name: "Hoàn tác"
                        });
                    } else {
                        doRemove();
                    }
                };
            } else {
                return function () {
                    if (data.ID || data.DisplayName) {
                        Component.System.alert.show("Bạn có chắc muốn xóa trường <i>" + data.DisplayName + "</i>", {
                            name: "Xóa", cb: function () {
                                f.list.remove(data);
                            }
                        }, {
                            name: "Hoàn tác"
                        });

                    } else {
                        f.list.remove(data);
                    }
                };
            }
        }

        f.onUpdate = function (field) {
            f.list.update();
        }

        f.validate = function () {
            var l = f.list();
            var n = l.length;

            if (n < 1)
                return "Cần phải có ít nhất một trường dữ liệu";

            if (n > 10)
                return "Số trường dữ liệu lớn hơn 10";

            for (var i = 0; i < n; i++) {
                var item = l[i];
                if (item.RequestDataTypeID == DataTypeID.table) {
                    if (Array.isArray(item.listChild) == false)
                        return "Bảng '" + item.DisplayName + "' cần có ít nhất một cột";
                }

                var msg = APIData.Workflow.EditAndValidateFieldWorkflow(item);
                if (msg)
                    return msg;
            }
        }

        f.setData = function (data) {
            data == null && (data = []);

            f.list(data);
        }

        f.getByName = function (name, parrentName) {
            var list = field.list();
            if (parrentName) {
                for (var i = 0; i < list.length; i++) {
                    var item = list[i];
                    if (item.Name == parrentName) {
                        list = item.listChild;
                        break;
                    }
                }
            }

            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                if (item.Name == name) {
                    return item;
                }
            }

        }
    };

    var step = t.step = new function () {
        var s = this;
        var listStepError = [];

        s.list = ko.observableArray();
        s.editing = ko.observable();
        s.editing.durationInString = ko.observable();
        s.editing.canBack = ko.observableArray();
        s.fieldCan = {
            description: ko.observable(),
            can: {},
            fieldCanInit: function (context, element, binds) {
                return; // to do

                // init
                var can = binds.can;
                var koempty = context.koempty = ko.observable(can.empty);
                var koview = context.koview = ko.observable(can.view);
                var koupdate = context.koupdate = ko.observable(can.update);
                var koedit = context.koedit = ko.observable(can.edit);
               // var kochildContext = context.

                // subscrip
                koempty.subscribe(function (val) { can.empty = val; });
                koview.subscribe(function (val) { can.view = val; });
                koupdate.subscribe(function (val) { can.update = val; });
                koedit.subscribe(function (val) { can.edit = val; });
            }
        }


        function reIndex(lStep) {
            for (var i = 0; i < lStep.length; i++)
                lStep[i].StepIndex = i;
        };

        function sortAndReindex(lStep) {
            lStep.sort(function (x, y) {
                if (x.StepIndex > y.StepIndex) {
                    return 1;
                } else if (x.StepIndex < y.StepIndex) {
                    return -1;
                } else {
                    return 1;
                }
            });

            reIndex(lStep);
        };

        function showFieldCanInString(flag) {
            if (!flag) {
                s.fieldCan.description(null);
                return;
            }

            var fieldCan = step.editing().fieldCan;

            function toString(listField) {
                fieldCan || (fieldCan = {});
                var list = [];
                listField.forEach(function (field) {
                    if (field.listChild) {
                        var childStr = toString(field.listChild);
                        if (childStr.length)
                            list.push(field.DisplayName + ' [' + childStr.join(', ') + ']')
                    } else {
                        var can = fieldCan[field.Name];
                        var re = [];
                        for (var pro in can) {
                            if (can[pro])
                                re.push(pro);
                        }

                        if (re.length) {
                            if (re.length == 4)
                                re = ['full'];

                            list.push(field.DisplayName + '(' + re.join(', ') + ')');
                        }
                    }
                });
                return list;
            }

            var re = toString(field.list());
            if (re.length)
                s.fieldCan.description(re.join(', '));
            else
                s.fieldCan.description("");
        }

        function initFieldCan(stepDifinition, listFieldDefinition) {
            var fieldCan = stepDifinition.fieldCan;
            if (fieldCan == null)
                fieldCan = stepDifinition.fieldCan = {};

            listFieldDefinition.forEach(function (fieldDefinition) {
                var can = fieldCan[fieldDefinition.Name];
                if (!can) {
                    can = fieldCan[fieldDefinition.Name] = {
                        empty: true,
                        view: true
                    };
                    if (stepDifinition.StepIndex == 0) {
                        can.empty = true;
                        can.view = true;
                        can.update = true;
                        can.edit = true;
                    } else {
                        if (fieldDefinition.RequestDataTypeID == DataTypeID.intendTime) {
                            can.empty = true;
                            can.update = true;
                            can.edit = true;
                        } else if (fieldDefinition.RequestDataTypeID == DataTypeID.excel) {
                            can.empty = true;
                            can.update = true;
                            can.edit = true;
                        }
                    }
                }
                if (fieldDefinition.listChild) {
                    initFieldCan(stepDifinition, fieldDefinition.listChild);
                }
            });
        }

        s.edit = function (data) {
            var selectedStep = s.editing();
            if (selectedStep) {
                selectedStep.StepIndex = selectedStep.StepOrder - 1;
                delete selectedStep.StepOrder;
                var iSE = listStepError.indexOf(selectedStep);
                if (selectedStep.StepIndex == 0 || APIData.Workflow.EditAndValidateStepWorkflow(selectedStep) == "") {
                    iSE != -1 && listStepError.splice(iSE, 1);
                } else {
                    iSE == -1 && listStepError.push(selectedStep);
                }
            }
            sortAndReindex(s.list());
            data.StepOrder = data.StepIndex + 1;

            s.editing(null);
            s.list.update();

            var l = s.list();
            var lCanBack = [];
            if (data.StepIndex == 0) {
                lCanBack.push({name: null, displayName: 'Hủy đơn hàng'});
            } else {
                for (var i = 0; i < l.length; i++) {
                    var step = l[i];
                    if (step.StepIndex >= data.StepIndex)
                        break;

                    if (step.StepIndex == 0)
                        lCanBack.push({name: null, displayName: 'Quay lại bước đầu tiên'});
                    else
                        lCanBack.push({name: step.Name, displayName: 'Quay lại bước ' + step.DisplayName});
                }
            }
            s.editing.canBack(lCanBack);

            s.editing(data);

            // update field can
            showFieldCanInString(1);

            Component.Custom.ComboBox.init();
            Component.Custom.ComboBox.bindingItem();
            Component.Custom.Number.init();
            //Component.Custom.Number.initEvent();

        };
        s.add = function () {
            var d = new APIData.Workflow.WorkflowStepEntity();
            d.StepIndex = s.list().length;
            if (d.StepIndex == 0) {
                d.Role = RoleID.user;
                d.CanCancel = 1;
                d.DisplayName = "Nhập thông tin";
                var tempName = Utils.StringToCodeName(d.DisplayName);
                var name = tempName;
            }
            d.CanRaise = 1;

            s.list().push(d);
            if (d.StepIndex == 0)
                t.onDisplayNameChange(d, s.list());
            s.edit(d);
        };
        s.remove = function () {
            var editing = s.editing();
            if (editing) {
                Component.System.alert.show("Bạn có chắc muốn xóa bước " + editing.DisplayName, {
                    name: "Xóa", cb: function () {
                        s.editing(null);
                        s.list.remove(editing);
                        Utils.removeFromArray(listStepError, editing);
                        sortAndReindex(s.list());
                        s.list();
                    }
                }, {
                    name: "Hàn tác"
                });
            }
        };
        s.stepStyle = function (data) {
            if (data == t.step.editing())
                return "selected";

            if (listStepError.indexOf(data) != -1)
                return "error";

            return null;
        };
        s.validate = function () {
            listStepError.length = 0;
            var message = '';
            s.list().forEach(function (item) {
                var temp = APIData.Workflow.EditAndValidateStepWorkflow(item);
                if (temp) {
                    message = message + "<br><b>Bước " + (item.StepIndex + 1) + ":</b><br>" + temp;
                    listStepError.push(item);
                }


                initFieldCan(item, field.list());
            });

            return message;
        };
        s.showDurationInString = function () {
            var step = s.editing();
            if (step == null)
                return;

            var msg = "";
            // if (step.MinIntendDuration == step.MaxIntendDuration)
            //     msg = step.MinIntendDuration + " giờ";
            // else
            //     msg = "Từ " + step.MinIntendDuration + " đến " + step.MaxIntendDuration + " giờ";

            msg = APIData.Workflow.RangeOfTimeInString(step.MinIntendDuration, step.MaxIntendDuration);
            s.editing.durationInString(msg);
        };
        s.showDurationEditor = function () {

            console.log(data);
            console.log(event);

            s.editing.durationInString(null);
            $(".step-duration>input")[0].focus();
        };
        s.durationEditorBlur = function (data, event) {
            setTimeout(function () {
                if ($(event.currentTarget).parent().find("input").is(":focus"))
                    return;

                var editing = s.editing();
                if (editing.MaxIntendDuration < editing.MinIntendDuration) {
                    var temp = editing.MaxIntendDuration;
                    editing.MaxIntendDuration = editing.MinIntendDuration;
                    editing.MinIntendDuration = temp;
                }

                s.showDurationInString();
            }, 100);
        };
        s.setData = function (lStep) {
            if (lStep) {
                sortAndReindex(lStep);
                s.list(lStep);
            } else {
                s.list([]);
            }
        };
        s.editing.subscribe(function (val) {
            var fn = s.editing;

            // step order
            if (fn.maxOrder == null)
                fn.maxOrder = ko.observable();
            fn.maxOrder(s.list().length);

            // step duration
            s.showDurationInString();
        });
        s.onFieldRenderDone = function (jo, model) {
            jo.find("*").css("cursor", "pointer");
            jo.find(".config> .fa").remove();
            var stepEditing = s.editing();
            if (!stepEditing)
                return;

            var fieldCan = stepEditing.fieldCan || (stepEditing.fieldCan = {});

            function setCanEdit(jo, canEdit) {
                if (canEdit) {
                    jo.attr("readonly", null);
                    jo.css("background", "");
                    jo.find('*').css("background", "");
                } else {
                    jo.attr("readonly", "true");
                    jo.css("background", "#e3e3e3");
                    jo.find('*').css("background", "#e3e3e3");
                }
            }

            function make(jo, model, canEdit) {

                jo.bind("click", function () {
                    var stepEditing = s.editing();
                    if (stepEditing == null)
                        return;

                    var value = canEdit[model.Name];
                    if (typeof value == "object")
                        return;

                    var newValue = !value;
                    canEdit[model.Name] = newValue;
                    setCanEdit(jo, newValue);
                });

                setCanEdit(jo, canEdit[model.Name]);
            }

            if (model.RequestDataTypeID == DataTypeID.table) {
                setTimeout(function () {
                    jo.find("input").attr("readonly", "true");
                    var colCanEdit = fieldCan[model.Name] || (fieldCan[model.Name] = {});
                    var n = model.listChild.length;
                    for (var i = 0; i < n; i++) {
                        make(jo.find("table tr>*:nth-child(" + (i + 1) + ")"), model.listChild[i], colCanEdit);
                    }
                }, 1);
            } else {
                jo.find("input").attr("readonly", "true");
                make(jo, model, fieldCan);
            }
        };
        s.showSelectFieldEditorClick = function () {
            var curStep = s.editing();
            if (curStep == null)
                return;

            initFieldCan(curStep, field.list());
            s.fieldCan.can = curStep.fieldCan;

            showFieldCanInString(0);
            Component.System.alert.show($("#selected-field-editor"), {
                name: "Xong", cb: function () {
                    showFieldCanInString(1);
                    // var style = popup[0].style;
                    // style.width = "";
                    // style.left = "";
                }
            });
        };
    };

    // action of selected step
    var action = t.action = new function () {
        var a = this;
        a.list = ko.observableArray();
        a.editing = ko.observable();
        a.actions = [
            {code: ActionTypeID.notification, name: 'Thông báo'},
            {code: ActionTypeID.assetState, name: 'Đổi trạng thái sản phẩm'},
            {code: ActionTypeID.associateAsset, name: 'Associate asset'},
        ];

        a.edit = function (data) {
            var oldData = a.editing();
            if (oldData) {
                a.editing(null);
                a.list.update(oldData);
            }
            if (oldData != data) {
                a.editing(data);
                a.list.update(data);
            }
        };
        a.editContextInit = function (context, e) {
            var cf = context.$data.cf;
            var koState = ko.observable();
            var koSelectOwner = ko.observable(false);

            // subscribe if assetState InUse, show asset onwer options
            koState.subscribe(function (val) {
                cf.newState = val;
                koSelectOwner(val == AssetState.InUse);
            });

            context.contextState = koState;
            context.koSelectOwner = koSelectOwner;

            koState(cf.newState);
        }
        a.add = function (actionTypeID) {
            if (step.editing() == null)
                return;

            var entity = new APIData.Workflow.WorkflowStepActionEntity();
            switch (actionTypeID) {
                case ActionTypeID.notification:
                    entity.cf = new APIData.Workflow.NotificationActionConfigEntity();
                    entity.Name = "Gởi thông báo";
                    break;

                case ActionTypeID.assetState:
                    entity.cf = new APIData.Workflow.AssetStateActionConfigEntity();
                    entity.Name = "Đổi trạng thái tài sản";
                    break;

                case ActionTypeID.associateAsset:
                    entity.cf = new APIData.Workflow.AssociateAssetActionConfigEntity();
                    entity.Name = "Associate asset";
                    break;

                case ActionTypeID.subOrder:
                    entity.cf = new APIData.Workflow.SubOrderActionConfigEntity();
                    entity.Name = "Tạo đơn hàng";
                    break;
            }
            entity.IsRollback = 1;
            entity.IsSync = 0;
            entity.ActionTypeID = actionTypeID;
            a.list.push(entity);
        };
        a.remove = function (data) {
            if (a.editing() == data)
                a.editing(null);

            a.list.remove(data);
        };

        a.searchAssetState = function (keyword, index, count, callback) {
            var fn = a.searchAssetState;
            var sm = fn.searchModel || (fn.searchModel = new SearchModel());
            sm.search(keyword, callback);
        };
        a.getDescription = function (cf) {
            if (cf.code == ActionTypeID.notification) {
                return "Gởi mail tới: " + cf.to.replace(/ (^[/s])/g, ', ');
            } else if (cf.code == ActionTypeID.assetState) {
                if (cf.list.length)
                    return "Thay đổi trạng thái tài sản của: " + cf.list.join(', ');
                else
                    return 'Chưa chọn tài sản nào';
            }
        }
        a.getTypeName = function (data) {
            switch (data.ActionTypeID) {
                case ActionTypeID.notification:
                    return 'Tự động thông báo';
                case ActionTypeID.assetState:
                    return 'Thay đổi trạng thái tài sản';
                case ActionTypeID.subOrder:
                    return 'Tạo đơn hàng con';
            }
        }

        step.editing.subscribe(function (step) {
            a.edit(null);
            if (step) {
                if (step.listAction == null)
                    step.listAction = [];

                // dev_upgrade fix cho data cũ (trước Beta), có thể xóa sau beta
                step.listAction.forEach(function (a) {
                    if (a.ActionTypeID == ActionTypeID.notification && a.cf.channel == null)
                        a.cf.channel = {};
                })

                action.list(step.listAction);
            } else {
                action.list(null);
            }

        });

    };

    var subWF = t.subWF = new function () {
        var sw = this;
        var smWF = new SearchModel();
        var listWF = [];
        var srcWFData = null;

        sw.selectedID = ko.observable();
        sw.editStarted = ko.observable(false);
        sw.detailEditing = ko.observable();
        sw.detailEditing.listSource = ko.observableArray();
        sw.getSDN = {
            field: function (src, parrentName) {
                var name = src && src.__parrent ? src.__parrent : src;
                var f = field.getByName(name, parrentName);
                return f ? f.DisplayName : "";
            },
            step: function (name) {
                var l = step.list();
                var n = l.length;
                for (var i = 0; i < n; i++) {
                    var item = l[i];
                    if (item.Name == name)
                        return item.DisplayName;
                }
                return "";
            }

        };
        sw.editing = ko.observable();

        // attr
        sw.checkType = {   // cho biết sẽ xét cái gì của value
            cmdbAssetTpye: 'assetType', // xét loại tài sản (loại dịch vụ)
            itmModelGroup: 'modelGroup' // xét nhóm tài sản (nhóm model quy trình)
        };
        sw.checkTypes = [
            {code: sw.checkType.cmdbAssetTpye, name: 'xét loại dịch vụ'},
            {code: sw.checkType.itmModelGroup, name: 'xét nhóm model'},
        ];
        sw.checkValuesMap = [];

        sw.mappingChange = function (srcField) {
            var des = sw.detailEditing();
            if (des.source == srcField.Name)
                delete des.source;
            else
                des.source = srcField.Name;
            sw.detailEditing.listSource.update();
        };
        sw.sourceClass = function (des, src) {
            return des.source == src.Name;
        };
        sw.mappingNote = function (des, src) {
            if (des.source != src.Name)
                return;

            if (des.RequestDataTypeID) {
                if (des.RequestDataTypeID == src.RequestDataTypeID)
                    return;

                if (src.RequestDataTypeID == DataTypeID.number)
                    return " ('" + des.DisplayName + "' x '" + src.DisplayName + "')";
                else
                    return "lỗi";
            }
        };
        sw.getByName = function (name, parrentName) {
            var list = sw.listField();
            if (parrentName) {
                for (var i = 0; i < list.length; i++) {
                    var item = list[i];
                    if (item.Name == parrentName) {
                        list = item.listChild;
                        break;
                    }
                }
            }

            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                if (item.Name == name) {
                    return item;
                }
            }
        };
        sw.addConExp = function () {
            sw.con.exps.push({
                leftType: 'field',
                leftValue: null,
                rightType: 'const',
                rightValue: null
            });
        };
        sw.removeConExp = function (exp) {
            sw.con.exps.remove(exp);
        };
        sw.updateConExp = function (exp, newValue) {
            exp.leftType = newValue.type;
            exp.leftValue = newValue.name;

            console.log(newValue);
            console.log(exp);
        };
        sw.setReferData = function (referData) {
            sw.checkValuesMap[sw.checkType.cmdbAssetTpye] = [
                {id: 1, name: 'là Asset'},
                {id: 2, name: 'là Component'},
                {id: 4, name: 'là Consumable'},
                {id: 3, name: 'là Software'}
            ];

            var list = [];
            referData.workdlowModelGroups.forEach(function (item) {
                list.push({
                    id: item.ID,
                    name: item.DisplayName
                })
            });
            sw.checkValuesMap[sw.checkType.itmModelGroup] = list;
        }

        sw.wfID = ko.observable();
        sw.steps = ko.observableArray();

        // condition
        sw.con = ko.observable(); // condition
        sw.con.exps = ko.observableArray(); // condition's expressions
        sw.con.pros = ko.observableArray(); // request's attributes
        sw.con.selectAttr = function ($context, $element) { // on con select attr
            console.log(arguments);
        };
        sw.con.eachExpConextInit = function ($context, $element) {
            var exps = sw.con().expressions;
            var data = $context.$data;
            var obPro = $context.obPro = ko.observable();
            var obCheckType = $context.obCheckType = ko.observable();
            var obCheckValues = $context.obCheckValues = ko.observableArray();
            var obCheckValue = $context.obCheckValue = ko.observable();

            function expRebind() {
                if (checkExp(data)) {
                    if (exps.indexOf(data) == -1) {
                        exps.length = 0;
                        sw.con.exps().forEach(function (exp) {
                            if (checkExp(exp))
                                exps.push(exp);
                        })
                    }
                } else {
                    Utils.removeFromArray(exps, data);
                }
            }

            function assignValue(attrName, val) {
                // check rebind. Nếu giá trị mới là null và cũ không null, hoặc ngược lại thì set rebind là true
                var needRebind = data[attrName] ? !val : val;

                data[attrName] = val;

                // rebind
                if (needRebind)
                    expRebind();
            }

            obPro.subscribe(function (val) {
                if (val) {
                    assignValue('pro', val.name);
                    data.type = val.type;
                } else {
                    assignValue('pro', val);
                }

                // thêm một dòng temp mới nếu có thể
                sw.con.exps.addTemp();
            });
            obCheckType.subscribe(function (val) {
                assignValue('checkType', val);
                obCheckValues(sw.checkValuesMap[val]);
            });
            obCheckValue.subscribe(function (val) {
                assignValue('checkValue', val);
            });

            obPro(sw.con.pros().find(function (item) {
                return item.name == data.pro;
            }));
            obCheckType(data.checkType);
            obCheckValue(data.checkValue);
        };
        sw.con.exps.addTemp = function () {
            var exps = sw.con.exps();
            if (exps.length && !exps[exps.length - 1].pro)
                return;

            sw.con.exps.push({
                pro: null,
                type: 'null',
                checkType: null,
                checkValue: null
            });
        };
        sw.con.exps.import = function (list) {
            var exps = [];
            for (var i = 0; i < list.length;) {
                var item = list[i];
                exps.push(item);
                if (checkExp(item))
                    i++;
                else
                    Utils.removeFromArray(list, item);
            }

            sw.con.exps(exps);
        };

        // fields
        sw.fields = ko.observableArray();

        sw.getWorkflowName = function (id) {
            var n = listWF.length;
            for (var i = 0; i < n; i++) {
                var item = listWF[i];
                if (item.ID == id) {
                    return item.DisplayName;
                }
            }

            return "";
        };
        sw.searchWorkflow = function (keyword, index, length, callBack) {
            smWF.search(keyword, callBack);
        };
        sw.edit = function (data) {
            if (data == null) {
                sw.wfID(null);
                return;
            }

            // attr
            updateFieldAttr();

            // condition
            sw.editing(data);

            sw.wfID(data.workflowID);
        };

        function checkExp(exp) {
            return exp.pro && exp.type && exp.checkType && exp.checkValue
        }

        function setMapping(listDes, mapping) {
            if (!mapping)
                return;

            listDes.forEach(function (item) {
                var source = mapping[item.Name];
                if (!source)
                    return;

                if (item.listChild) {
                    item._src = source.__parrent;
                    setMapping(item.listChild, source)
                } else {
                    item._src = source;
                }
            });
        }

        function getMapping(listDes) {
            if (!listDes)
                return null;

            var mapping = {};
            listDes.forEach(function (item) {
                if (item.listChild) {
                    var source = getMapping(item.listChild);
                    source.__parrent = item._src;
                    mapping[item.Name] = source;
                } else {
                    mapping[item.Name] = item._src;
                }
            });

            return $.isEmptyObject(mapping) ? null : mapping;
        }

        function buildValues(preName, type, sources, outputInList, fnCheck) {
            sources.forEach(function (src) {
                var displayName = preName + " '" + src.DisplayName + "'";

                if (src.listChild) {
                    buildValues(displayName + ' ->', type, src.listChild, outputInList, fnCheck);
                } else if (src.ParentID || type != 'field') {
                    if (!fnCheck || fnCheck(src))
                        outputInList.push({
                            name: src.Name,
                            type: type,
                            displayName: displayName
                        });
                }
            });
        }

        function updateFieldAttr() {
            var attrs = [];
            buildValues('Trường', 'field', field.list(), attrs, function (src) {
                return src.RequestDataTypeID != DataTypeID.table
                    && src.RequestDataTypeID != DataTypeID.user
                    && src.RequestDataTypeID != DataTypeID.intendTime;
            });
            sw.con.pros(attrs);
        }

        function buildMapping(mapping, dess, desName, srcs, srcName, fnGetType, startToEndBreakOnFailed) {
            if (srcs == null)
                srcs = srcs;


            var re = [];
            var typeCoutings = [];
            var srcLen = srcs.length;

            dess.forEach(function (des) {

                var type = fnGetType(des);
                var typeCouting = typeCoutings[type] || 0;
                var srcOptions = [];
                var srcInMapping = mapping[des.Name];
                var srcInMappingIsObject = typeof srcInMapping == 'object';
                var srcItemSelect = null;
                var item = {
                    name: des.Name,
                    displayName: desName + " '" + des.DisplayName + "'",
                    srcItem: ko.observable(),
                    custom: ko.observable(),
                    options: srcOptions
                };

                if (srcInMappingIsObject && srcInMapping.__type == DataTypeID.default) {
                    item.custom(srcInMapping.__value);
                } else {
                    for (var i = 0; i < srcLen; i++) {
                        var src = srcs[i];
                        var srcType = fnGetType(src);
                        var srcItem = {
                            displayName: srcName + " '" + src.DisplayName + "'",
                            src: src
                        }

                        if (des.listChild ? src.listChild : !src.listChild)
                            srcOptions.push(srcItem);

                        if (srcInMapping) {
                            if (srcInMapping.__type == DataTypeID.table) {
                                if (srcInMapping.__value == src.Name)
                                    srcItemSelect = srcItem;
                            } else {
                                if (srcInMapping == src.Name)
                                    srcItemSelect = srcItem;
                            }
                        } else if (type == srcType) {  // auto mapping
                            if (typeCouting > 0)
                                typeCouting--;
                            else if (typeCouting == 0 && srcInMapping === undefined)
                                srcItemSelect = srcItem;
                        }
                    }
                }

                var srcItemDefault = {displayName: des.listChild ? '--' : 'từ giá trị nhập', src: null};
                srcOptions.push(srcItemDefault);

                if (srcItemSelect == null)
                    srcItemSelect = srcItemDefault;

                item.srcItem.subscribe(function (srcItem) {
                    var srcIM = mapping[des.Name];

                    // remove mapping
                    if (srcItem.src == null) {
                        if (typeof srcIM != 'object')
                            srcIM = mapping[des.Name] = {};

                        srcIM.__type = DataTypeID.default;
                        srcIM.__value = item.custom();

                        if (item.childs)
                            item.childs([]);
                        return;
                    }

                    if (des.listChild) {
                        if (typeof srcIM != 'object')
                            srcIM = mapping[des.Name] = {};
                        srcIM.__type = DataTypeID.table;
                        srcIM.__value = srcItem.src.Name;

                        var listChild = buildMapping(srcIM,
                            des.listChild, item.displayName + " ->",
                            srcItem.src.listChild, srcItem.displayName + " ->",
                            fnGetType, startToEndBreakOnFailed);
                        item.childs(listChild);
                    } else {
                        mapping[des.Name] = srcItem.src.Name;
                    }
                });
                item.custom.subscribe(function (val) {
                    if (val) {
                        var srcIM = mapping[des.Name];
                        if (typeof srcIM != 'object')
                            srcIM = mapping[des.Name] = {};
                        srcIM.__value = val;
                    } else {
                        mapping[des.Name] = null;
                    }
                });

                if (des.listChild)
                    item.childs = ko.observableArray();
                item.srcItem(srcItemSelect);
                re.push(item);
            });

            return re;
        }

        function buildFieldMapping() {
            if (srcWFData == null)
                return;

            var fields = buildMapping(sw.editing().fieldMapping, srcWFData.listFieldDefinition, 'Copy', field.list(), 'từ', function (field) {
                return field.RequestDataTypeID;
            });
            sw.fields(fields);
        }

        function buildStepMapping() {
            if (srcWFData == null)
                return;

            var steps = buildMapping(sw.editing().stepMapping, srcWFData.listStepDefinition, 'Copy', step.list(), 'từ', function (step) {
                return step.Role;
            });
            sw.steps(steps);
        }

        function loadSourceWF() {
            var wfID = sw.wfID();
            sw.editStarted(false);
            APIData.Workflow.GetWorkflowEditData(wfID, function (res) {
                if (res.code == AMResponseCode.ok) {
                    var cf = sw.editing();
                    cf.workflowID = wfID;
                    srcWFData = res.data;

                    sw.con(cf.condition);
                    sw.con.exps.import(cf.condition.expressions);

                    if (!cf.condition.expressions.length)
                        sw.con.exps.addTemp();

                    buildFieldMapping();
                    buildStepMapping();
                    sw.editStarted(true);

                } else {
                    Component.System.alert.show("Xảy ra lỗi trong quá trình tải dữ liệu");
                }
            });
            sw.wfID.oldValue = wfID;
        }

        sw.wfID.subscribe(function (wfID) {
            if (wfID == null) {
                sw.wfID.oldValue = null;
                sw.editStarted(false);
                sw.con(null);
                sw.con.exps([]);
                return;
            }

            if (wfID == sw.wfID.oldValue)
                return;

            if (sw.con.exps().length > 1) {
                Component.System.alert.show('Bạn có chắc muốn chọn quy trình khác cho đơn hàng con',
                    {
                        name: "Đồng ý",
                        cb: loadSourceWF
                    },
                    {
                        name: "Hủy",
                        cb: function () {
                            if (sw.wfID.oldValue == null)
                                return;

                            sw.wfID(sw.wfID.oldValue);
                        }
                    }
                );
            } else {
                loadSourceWF();
            }
        });
        action.editing.subscribe(function (actionData) {
            if (actionData == null) {
                if (sw.editing())
                    sw.edit(null)
            } else {
                if (actionData.ActionTypeID == ActionTypeID.subOrder)
                    sw.edit(actionData.cf)
            }
        });
        field.list.subscribe(function (list) {
            if (!sw.editing())
                return;

            buildFieldMapping();
            updateFieldAttr();
        });
        step.list.subscribe(function (list) {
            if (!sw.editing())
                return;
            buildFieldMapping();
        });
    };

    var mention = t.mention = new function () {
        var m = this;

        m.user = new MentionSearchMeta();
        m.order = new MentionSearchMeta();
        m.field = new MentionSearchMeta();
        m.task = new MentionSearchMeta();
        m.all = new MentionSearchMeta();
        m.asset = new MentionSearchMeta();

        m.setSearchHandle = function (handle) {
            for (var i = 0; i < m.list.length; i++)
                m.list[i].searchHandle = handle;
        }
        m.list = [m.user, m.field, m.task, m.all, m.asset];
    };

    // function
    function setAllWorkflowData(allData) {
        data = allData.workflow;
        if (Utils.isDate(data.CreateDate) == false) {
            var temp = new Date(data.CreateDate);
            if (Utils.isDate(temp))
                data.CreateDate = temp;
        }

        t.data(data);
        step.setData(allData.listStepDefinition);
        field.setData(allData.listFieldDefinition);
        Utils.setURLValue("ID", data.ID);
    }

    function setReferenceData(referData) {

        var listRole = t.listRole.originalData = [];
        deptHeadRoleID = APIData.Workflow.RoleID.deptHead;
        referData.listRole.forEach(function (item) {
            if (item.ID != deptHeadRoleID)
                listRole.push(item);
        });

        t.listDataType([
            {ID: DataTypeID.string, DisplayName: 'Kiểu chữ'},
            {ID: DataTypeID.number, DisplayName: 'Kiểu số'},
            {ID: DataTypeID.asset, DisplayName: 'Mã tài sản'},
            {ID: DataTypeID.assetModel, DisplayName: 'Model tài sản'},
            {ID: DataTypeID.assetType, DisplayName: 'Loại tài sản'},
            {ID: DataTypeID.domain, DisplayName: 'User - domain'},
            {ID: DataTypeID.user, DisplayName: 'User - chi tiết'},
            {ID: DataTypeID.table, DisplayName: 'Bảng'},
            {ID: DataTypeID.intendTime, DisplayName: 'Thời gian hoàn thành dự kiến'},
            {ID: DataTypeID.excel, DisplayName: 'Excel'},
        ]);
        t.listReason(referData.listReason);
        t.listAssetState(referData.listAssetState);

        subWF.setReferData(referData);
    }

    function buildListRole() {
        var list = t.listRole.originalData.slice();
        for (var i = 0; i < list.length;) {
            if (list[i].ID == RoleID.user || list[i].ID == RoleID.deptHead) {
                list.splice(i, 1);
            } else {
                i++;
            }
        }

        var userFields = [];
        var noteFields = [];
        field.list().forEach(function (item) {
            if (checkUserField(item)) {
                list.push({
                    ID: RoleID.user + ":" + item.Name,
                    Name: item.DisplayName
                });
                list.push({
                    ID: RoleID.deptHead + ":" + item.Name,
                    Name: "Deapt head của '" + item.DisplayName + "'"
                });

                userFields.push({
                    Name: item.Name,
                    DisplayName: item.DisplayName
                });
            } else if (item.RequestDataTypeID == DataTypeID.string) {
                noteFields.push({
                    Name: item.Name,
                    DisplayName: item.DisplayName
                });
            }
        });

        // user field
        if (userFields.length > 1) {
            userFields.splice(0, 0, {Name: null, DisplayName: "Chọn một trường loại user"});
        }

        // note field
        if (noteFields.length > 1) {
            noteFields.splice(0, 0, {Name: null, DisplayName: "Chọn một trường loại string"});
        }

        field.userFields(userFields);
        field.noteFields(noteFields);
        t.listRole(list);
    }

    function updateListByID(l, e) {
        var id = e.ID;
        for (i in l) {
            if (l[i].ID == id) {
                l[i] = e;
                return;
            }
        }
    }

    function checkUserField(f) {
        return f.RequestDataTypeID == DataTypeID.user || f.RequestDataTypeID == DataTypeID.domain;
    }

    t.onDisplayNameChange = createEventHanlde(t);
    t.onDisplayNameChange.bind(function (data) {
        if (data.Name == null && data.DisplayName) {
            /// gen name code
            var tempName = Utils.StringToCodeName(data.DisplayName);
            var name = tempName;

            // check name is exist
            function isNameExist(name, list) {
                for (var i = 0; i < list.length; i++) {
                    var item = list[i];
                    if (item.listChild) {
                        if (isNameExist(name, item.listChild))
                            return true;
                    } else if (list[i].Name == name) {
                        return true;
                    }
                }
            }

            /// tránh name code trùng nhau
            var count = null;
            var maxCount = 1000;
            while (count <= maxCount) {
                if (t.data().Name == name || isNameExist(name, field.list()) || isNameExist(name, step.list())) {
                    count = count ? (count + 1) : 1;
                    name = tempName + count;
                    i = 0;
                } else {
                    break;
                }
            }
            if (count > maxCount) {
                console.warn("gen name code failed");
                return;
            }

            data.Name = name;
        }
    });
    t.onDisplayNameChange.bind(buildListRole);
    field.list.subscribe(buildListRole);

    // event
    t.save = function () {

        var fields = field.list();
        var steps = step.list();

        // init
        var isInsert = data.ID == null || isNaN(data.ID);
        if (isInsert) {
            data.CreateDate = new Date();
            data.Creator = APP.currentUser;
            data.IsDelete = false;
        }

        // validate
        var msg = APIData.Workflow.EditAndValidateWorkflow(data);
        if (msg) {
            Component.System.alert.show("<b>Lỗi thông tin quy trình:</b><br>" + msg);
            return;
        }
        // owner field validate
        if (data.OwnerField == null) {
            Component.System.alert.show('Vui lòng chọn trường cho <b>Trường người nhận hàng</b> của thông tin quy trình:<br>');
            return;
        } else if (data.OwnerField == -1) {
            var userfieldCount = 0;
            fields.forEach(function (f) {
                if (checkUserField(f))
                    userfieldCount++;
            });
            if (userfieldCount != 1) {
                Component.System.alert.show('Vui lòng chọn trường cho <b>Trường người nhận hàng</b> của thông tin quy trình:<br>');
                return;
            }
        }

        msg = field.validate();
        if (msg) {
            Component.System.alert.show("<b>Lỗi thông tin trường của biên bản:</b><br>" + msg);
            return;
        }

        msg = step.validate();
        if (msg) {
            Component.System.alert.show("<b>Lỗi thông tin bước của biên bản:</b><br>" + msg);
            return;
        }

        var editingIndex = {
            field: field.editing() ? field.list().indexOf(field.editing()) : null,
            step: step.editing() ? step.list().indexOf(step.editing()) : null,
            action: action.editing() ? action.list().indexOf(action.editing()) : null
        };

        if (editingIndex.field !== null) field.closeEdit();
        if (editingIndex.action !== null) action.editing(null);
        if (editingIndex.step !== null) step.editing(null);

        // finish save
        function saveDone(res, jsonData) {
            if (res.code == AMResponseCode.ok) {
                setAllWorkflowData(res.data);

                if (editingIndex.field !== null) field.edit(field.list()[editingIndex.field]);
                if (editingIndex.step !== null) step.edit(step.list()[editingIndex.step]);
                if (editingIndex.action !== null) action.edit(action.list()[editingIndex.action]);
                Component.System.alert.success();
            } else {
                Component.System.alert.show("Lưu dữ liệu không thành công");
            }
        }

        // call service
        if (isInsert)
            APIData.Workflow.InsertWorkflow(data, field.list(), step.list(), saveDone);
        else
            APIData.Workflow.UpdateWorkflow(data, field.list(), step.list(), saveDone);
    };

    t.changeTab = function (tab) {
        console.log(tab);
        t.tab(tab);
    };

    t.toUser = function () {

    };

    t.paramEdit = function () {
        console.log(arguments);
    };

    // build mention binding data
    (function () {
        // data
        var userReferSource;
        var fieldReferSource;
        var orderReferSource;
        var taskReferSource;
        var wfReferSource;
        var allReferSource;

        var assetReferSource;

        // search user
        (function () {
            userReferSource = new ReferSource("Quyền duyệt", null, "user", searchRole, getRoleName, 'ID', 'Name');
            var searchModelRole = new SearchModel();
            var mapRole = {};

            function updateRole() {
                var l = [];
                mapRole = {};

                l.add = function (id, name) {
                    var item = {
                        ID: id,
                        Name: name
                    };
                    l.push(item);
                    mapRole[item.ID] = item;
                };
                l.add(-1, "Người duyệt");
                l.add(-2, "Người duyệt tiếp theo");
                l.add(-3, "Người duyệt trước đó");
                l.add(-4, "Người tạo");
                t.listRole().forEach(function (item) {
                    l.add(item.ID, item.Name);
                })

                var searchMember = [{Name: userReferSource.displayName, IsUnicode: 1}];
                searchModelRole.setDataSource(l, searchMember);
            }

            field.list.subscribe(updateRole);
            t.listRole.subscribe(updateRole);
            t.onDisplayNameChange.bind(updateRole);

            function searchRole(keyWord, start, count, callBack) {
                searchModelRole.search(keyWord, function (list) {
                    var re = [];
                    var listStep = step.list();
                    var nStep = listStep.length;

                    if (list.length > 2) {
                        list.forEach(function (item) {
                            if (typeof item.ID == "string") {
                                re.push(item);
                                return;
                            }

                            if (item.ID < 0) {
                                re.push(item);
                                return;
                            }

                            for (var i = 0; i < nStep; i++) {
                                if (listStep[i].Role == item.ID) {
                                    re.push(item);
                                    return;
                                }
                            }
                        });
                    } else {
                        var checkID = [];
                        listStep.forEach(function (item) {
                            checkID[item.Role] = 1;
                        });

                        list.forEach(function (item) {
                            if (typeof item.ID == "string") {
                                re.push(item);
                                return;
                            }

                            if (item.ID < 0) {
                                re.push(item);
                                return;
                            }

                            if (checkID[item.ID])
                                re.push(item);
                        });
                    }

                    callBack(re);
                });
            }

            function getRoleName(ID) {
                var obj = mapRole[ID];
                return obj && obj.Name;
            }
        })();

        // search workflow info
        (function () {

            wfReferSource = new ReferSource("Thông tin Đơn hàng", null, "wf", searchAPI, getName, 'Name', 'DisplayName');
            var searchModel = new SearchModel();
            var listSource = [
                {Name: 'DisplayName', DisplayName: 'Tên quy trình'}
            ];
            var searchMember = [{Name: wfReferSource.displayName, IsUnicode: 1}];
            searchModel.setDataSource(listSource, searchMember);

            function searchAPI(keyWord, start, count, callBack) {
                searchModel.search(keyWord, callBack);
            }

            function getName(value) {
                return value == 'DisplayName' ? 'Tên quy trình' : '';
            }

        })();

        // search workflow order info
        (function () {

            orderReferSource = new ReferSource("Thông tin đơn hàng", null, "info", searchAPI, getName, 'Name', 'DisplayName');
            var searchModel = new SearchModel();
            var listSource = [
                {Name: 'ID', DisplayName: 'Mã đơn hàng'},
                {Name: 'CreateDate', DisplayName: 'Thời gian tạo'},
                {Name: '_orderContent', DisplayName: 'Nội dung đơn hàng'},
                {Name: '_approval', DisplayName: 'Nút duyệt đơn hàng'},
                {Name: '_link', DisplayName: 'Link đơn hàng'},
            ];
            var searchMember = [{Name: orderReferSource.displayName, IsUnicode: 1}];
            searchModel.setDataSource(listSource, searchMember);

            function searchAPI(keyWord, start, count, callBack) {
                searchModel.search(keyWord, callBack);
            }

            function getName(value) {
                for (var i = 0; i < listSource.length; i++) {
                    if (listSource[i].Name == value)
                        return listSource[i].DisplayName;
                }
            }
        })();

        // search workflow order step
        (function () {
            taskReferSource = new ReferSource("Thông tin tác vụ", null, "task", searchAPI, getName, 'Name', 'DisplayName');
            var searchModel = new SearchModel();
            var listSource = [
                {Name: 'comment', DisplayName: 'Ghi chú duyệt'},
            ];
            var searchMember = [{Name: taskReferSource.displayName, IsUnicode: 1}];
            searchModel.setDataSource(listSource, searchMember);

            function searchAPI(keyWord, start, count, callBack) {
                searchModel.search(keyWord, callBack);
            }

            function getName(value) {
                for (var i = 0; i < listSource.length; i++) {
                    if (listSource[i].Name == value)
                        return listSource[i].DisplayName;
                }
            }
        })();

        // search workflow field
        (function () {

            fieldReferSource = new ReferSource("Thông tin yêu cầu", null, "field", searchAPI, getName, 'Name', 'DisplayName');
            var searchModel = new SearchModel();
            var listSource = [];

            field.list.subscribe(function (list) {
                setTimeout(function () {
                    listSource = [];
                    list.forEach(function (item) {
                        listSource.push(item);

                        item.listChild && item.listChild.forEach(function (childItem) {
                            listSource.push({
                                Name: childItem.Name,
                                DisplayName: item.DisplayName + ' -> ' + childItem.DisplayName
                            });
                        });
                    });

                    listSource.push({Name: -1, DisplayName: "Tên qui trình"});
                    var searchMember = [{Name: fieldReferSource.displayName, IsUnicode: 1}];
                    searchModel.setDataSource(listSource, searchMember);
                }, 1000);
            });

            function searchAPI(keyWord, start, count, callBack) {
                searchModel.search(keyWord, callBack);
            }

            function getName(value) {
                var n = listSource.length;
                for (var i = 0; i < n; i++) {
                    if (listSource[i].Name == value)
                        return listSource[i].DisplayName;
                }
            }
        })();

        // search all
        (function () {
            allReferSource = new ReferSource("Tất cả", null, "all");
            allReferSource.pushChid(userReferSource);
            allReferSource.pushChid(wfReferSource);
            allReferSource.pushChid(orderReferSource);
            allReferSource.pushChid(fieldReferSource);
            allReferSource.pushChid(taskReferSource);
        })();

        // search asset field
        (function () {

            assetReferSource = new ReferSource("Thông tin tài sản", null, "field", searchAPI, fieldReferSource.getName, 'Name', 'DisplayName');
            var searchModel = new SearchModel();
            var listSource = [];

            function checkItem(item) {
                return item.RequestDataTypeID == DataTypeID.asset;
            }

            function collectItem(listSrc, listDesc, parentName) {
                parentName = parentName ? parentName + ' -> ' : '';

                listSrc.forEach(function (item) {
                    if (item.listChild)
                        collectItem(item.listChild, listDesc, item.DisplayName);
                    else if (checkItem(item))
                        listDesc.push({
                            Name: item.Name,
                            DisplayName: parentName + item.DisplayName
                        });
                });
            }

            function rebin(list) {
                listSource = [];
                collectItem(list, listSource);
                var searchMember = [{Name: fieldReferSource.displayName, IsUnicode: 1}];
                searchModel.setDataSource(listSource, searchMember);
            }

            t.field.list.subscribe(function (list) {
                setTimeout(rebin, 500, list);
            });

            function searchAPI(keyWord, start, count, callBack) {
                searchModel.search(keyWord, callBack);
            }

            function getName(value) {
                for (var i = 0; i < listSource.length; i++) {
                    if (listSource[i].Name == value)
                        return listSource[i].DisplayName;
                }
            }
        })();

        // new ReferSource("Tất cả thông tin", null, API);
        mention.user.listSource = [userReferSource];
        mention.user.defaultSource = userReferSource;
        mention.field.listSource = [fieldReferSource];
        mention.field.defaultSource = fieldReferSource;
        mention.task.listSource = [taskReferSource];
        mention.task.defaultSource = taskReferSource;
        mention.all.listSource = [userReferSource, fieldReferSource, taskReferSource, wfReferSource, orderReferSource, allReferSource];
        mention.all.defaultSource = allReferSource;
        mention.asset.listSource = [assetReferSource, fieldReferSource];
        mention.asset.defaultSource = assetReferSource;


    })();

    Utils.makeMapForKoArray(t.listRole, "map", "ID", "Name");

    APIData.Request.GetListRequestStatus(function (res) {
        if (res.code == AMResponseCode.ok) {
            var listRequestStatus = res.data.slice();
            for (var i = 0; i < listRequestStatus.length; i++) {
                if (listRequestStatus[i].IsSystem != 1) {
                    t.listStatus.push(listRequestStatus[i]);
                }
            }
        }
    });
    APIData.CMDB.GetAssetStateList('', '', '', function (listAssetState) {
        var fn = action.searchAssetState;
        var sm = fn.searchModel || (fn.searchModel = new SearchModel());
        APIData.CMDB.buildAssetStateModel(sm, listAssetState);
    });
    APIData.Workflow.GetListWorkflow(function (res) {
        if (res.code == AMResponseCode.ok) {
            t.listWorkFlow(res.data);
        } else {
            Component.System.alert.show("Xảy ra lỗi trong quá trình tải dữ liệu");
        }
    });
    APIData.Workflow.GetWorkflowEditData(Utils.getURLValue("ID"), function (res) {
        if (res.code == AMResponseCode.ok) {
            var d = res.data;

            if (d.workflow == null) {
                if (Utils.getURLValue("ID") != null) {
                    Component.System.alert.show("Không tìm thấy dữ liệu qui trình");
                    return;
                }

                d.listStepDefinition = [];
                d.listFieldDefinition = [];
                var wf = d.workflow = new APIData.Workflow.WorkflowEntity();
                wf.CreateDate = new Date();
                wf.Creator = d.currentDomain;

                function createDefaultFieldData(displayName, name, type) {
                    var field = new APIData.Workflow.WorkflowFieldEntity(type);
                    field.DisplayName = displayName;
                    field.Name = name;
                    field.cf.style.display = "none";
                    //field.cf.submitedStyle.display = null;
                    return field;
                }

                var field = createDefaultFieldData("Thông tin người sử dụng", "nyc", DataTypeID.user);
                d.listFieldDefinition.push(field);
                delete field.cf.style.display;
            }

            setReferenceData(res.referData);
            setAllWorkflowData(d);

            // dev_test
            // step.edit(step.list()[0]);
            // action.add(ActionTypeID.assetState);
            // action.edit(action.list()[3]);
            // dev_test end
        } else {
            console.log(res);
            Component.System.alert.show("Xảy ra lỗi khi tải dữ liệu");
        }
    });

};