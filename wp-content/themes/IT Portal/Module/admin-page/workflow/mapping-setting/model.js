Module.Model = function () {
    var me = this;

    this.keyword = ko.observable('');
    this.RequestID = ko.observable('');
    this.Output = ko.observable([]);
    this.RequestField = new APIData.Request.RequestData();
    this.Mapping = ko.observable({});
    this.RequestTypes = ko.observableArray([]);

    var newOutputAttr = function (name) {
        this.FieldName = name;
        this.Detail = [];
        this.MappingName = '';
    };

    this.load_requestField = function (model, e, callBack) {
        var form = Component.Custom.Form.items['CreateNewMapping'].Form;
        me.RequestField.loadFieldDefinition(form.val().RequestID, function () {
            Common.callBack(callBack);
        });
    };

    this.add_OutputAttr = function (model, e) {

        var form = Component.Custom.Form.items['CreateNewMapping'].Form;

        if (form.val().RequestID === '') {
            return;
        } else if (me.RequestField.listFieldDefinition !== undefined) {
            var eml = e.currentTarget;
            var parent = eml.getAttribute('parent');
            var id = Math.floor((Math.random() * 10000) + 1);
            var item = new newOutputAttr('Attr' + id, '-1');
            if (parent !== '' && parent !== null) {
                model.Detail = model.Detail || [];
                model.Detail.push(item);
            } else {
                me.Output().push(item);
            }
            me.on_mappingChange();
        }
    };

    var removeItem = function (item, prop, arr) {
        if (!Array.isArray(arr) || arr === undefined) {
            return
        }
        var index = arr.indexOf(item);
        if (index >= 0) {
            arr.splice(index, 1);
        } else {
            arr.forEach(function (arrItem) {
                removeItem(item, prop, arrItem[prop]);
            })
        }
    };

    this.do_DeleteItem = function (model, e) {
        var form = Component.Custom.Form.items['CreateNewMapping'].Form;

        if (form.val().RequestID === '') {
            return;
        } else if (me.RequestField.listFieldDefinition !== undefined) {
            var temp = me.Output();

            removeItem(model, 'Detail', temp);

            me.Output([]);
            me.Output(temp);
            me.on_mappingChange();
        }

        /*
        console.log(model);
        console.log(e.currentTarget);
        console.log(me.Output());
        */
    };

    var getMappingResult = function (_output) {
        var resultItem = {
            FieldName: _output.Name,
            MappingName: _output.Mapping
        };
        if (_output.Child.length > 0) {
            resultItem.Detail = [];
            for (var i in _output.Child) {
                var item = _output.Child[i];
                resultItem.Detail.push(getMappingResult(item));
            }
        }
        return resultItem;
    };

    this.on_mappingChange = function (model, e) {
        var temp = me.Output();
        me.Output([]);
        me.Output(temp);
        Component.Custom.ComboBox.init();
        var form = Component.Custom.Form.items['CreateNewMapping'].Form;
        form.field('Mapping').setValue(JSON.stringify(me.Output()));
    };

    var init = function () {
        APIData.Request.GetListRequest('', 0, 100, function (res) {
            me.RequestTypes(res);
            Component.Custom.ComboBox.bindingItem();
        });
        var popup = Component.Custom.Popup.items['create-new-mapping'].Popup;
        popup.on_Close = function () {
            var form = Component.Custom.Form.items['CreateNewMapping'].Form;
            form.resetData();
            me.Output([]);
        }
    };

    this.do_enableEdit = function (model, e) {
        var mappingName = e.currentTarget.getAttribute('data-mapping');
        APIData.RequestFeatureMapping.GetByName(mappingName, function (res) {
            res = JSON.parse(res);
            var form = Component.Custom.Form.items['CreateNewMapping'].Form;
            form.val({
                ID: res.ID,
                Name: res.Name,
                RequestID: res.RequestID,
                Mapping: res.Mapping
            });
            me.load_requestField(null, null, function () {
                me.Output(JSON.parse(res.Mapping));
                Component.Custom.ComboBox.init();
            });
        });
    };

    this.do_Submit = function () {
        var form = Component.Custom.Form.items['CreateNewMapping'].Form;
        var formData = form.val();
        if (formData.ID === '') {
            APIData.RequestFeatureMapping.Insert(formData, function (res) {
                res = JSON.parse(res);
                if (res.State) {
                    Component.System.alert.show('Thêm mới thành công');
                    Component.Custom.Popup.Active.hide();
                    window.location.reload();
                } else {
                    Component.System.alert.show('Thêm mới thất bại');
                }
            });
        } else {
            APIData.RequestFeatureMapping.Update(formData, function (res) {
                res = JSON.parse(res);
                if (res.State) {
                    Component.System.alert.show('Cập nhật thành công');
                } else {
                    Component.System.alert.show('Cập nhật thất bại');
                }
            });
        }
    };

    this.do_Cancel = function () {
        var form = Component.Custom.Form.items['CreateNewMapping'].Form;
        form.resetData();
        me.Output([]);
    };

    init();
}
;