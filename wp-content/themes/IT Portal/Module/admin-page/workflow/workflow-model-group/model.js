
function kolog(data) {
    //console.log(data);
    return data;
}

Module.Model = function () {
    var groupID = Utils.getURLValue('ID');
    var mapAassetModel = [];
    var searchModel = null;

    var t = this;
    t.group = ko.observable();
    t.assetModelsToSelect = ko.observableArray();

    t.onSearchKeyDown = function (event) {
        if (event.keyCode != 13) // enter
            return 0;

        searchModel.search(event.currentTarget.value, t.assetModelsToSelect);
    };

    t.save = function () {
        if (!t.group().DisplayName) {
            Component.System.alert.show('Tên nhóm không thể bỏ trống');
            return;
        }

        var modelNames = [];
        mapAassetModel.forEach(function (item) {
            if (item && item.selected)
                modelNames.push(item.ID);
        });

        if (modelNames.length == 0) {
            Component.System.alert.show('Nhóm cần có ít nhất 1 model');
            return;
        }

        var api = groupID ? APIData.WorkflowModelGroup.UpdateWorkflowModelGroup : APIData.WorkflowModelGroup.InsertWorkflowModelGroup;
        api(t.group(), modelNames, function (res) {
            if (res.code != AMResponseCode.ok) {
                Component.System.alert.show('Lưu dữ liệu thất bại');
                return;
            }

            var successMsg = (groupID ? 'Cập nhật' : 'Thêm')
                + ' nhóm model quy trình thành công';
            Component.System.alert.show(successMsg);

            t.group(res.data.group);
        });
    }

    APIData.WorkflowModelGroup.GetWorkflowModelGroupEditData(groupID, function (res) {
        if (res.code == AMResponseCode.ok) {
            // init asset model mapping
            mapAassetModel = [];
            var allAssetModel = res.referData.allAssetModel;

            allAssetModel.forEach(function (item) {
                mapAassetModel[item.ID] = item;
            });

            // init search model
            var searchMember = [{Name: 'Name', IsUnicode: true}];
            searchModel = new SearchModel(allAssetModel, searchMember);

            // check data
            if (groupID == null) {
                t.group({DisplayName: ''});
                t.assetModelsToSelect(allAssetModel);
                $("#asset-model-filter").bind('keydown', t.onSearchKeyDown);
                return;
            }

            // set group data
            t.group(res.data.group);

            // set group detail data
            res.data.modelNames.forEach(function (modelName) {
                var assetModel = mapAassetModel[modelName];
                if (assetModel)
                    assetModel.selected = true;
            });

            // set asset model to select
            t.assetModelsToSelect(allAssetModel);
            $("#asset-model-filter").bind('keydown', t.onSearchKeyDown);

        } else {
            Component.System.alert.show("Xảy ra lỗi trong quá trình tải dữ liệu");
        }
    });
    t.group.subscribe(function (group) {
        if(groupID == group.ID)
            return;

        groupID = group.ID;
        Utils.setURLValue('ID', groupID);
    });
};