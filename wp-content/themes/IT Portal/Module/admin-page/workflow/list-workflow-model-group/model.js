Module.Model = function () {
    var t = this;
    t.list = ko.observableArray();
    t.editLink = "/admin-page/workflow/workflow-model-group?";
    
    t.remove = function (data) {
        Component.System.alert.show("Bạn có chắc chắn muốn xóa nhóm '" + data.DisplayName + "' ?",
            {
                name: 'Xóa',
                cb:function () {
                    APIData.WorkflowModelGroup.DeleteWorkflowModelGroup(data.ID, function (res) {
                        if(res.code == AMResponseCode.ok){
                            Component.System.alert.show("Xóa quy nhóm '" + data.DisplayName + "' thành công");
                            t.list.remove(data);
                        }else{
                            Popup.failed();
                        }
                    });
                }
                },
            {
                name: 'Hủy bỏ',
            }
            );

    }
    APIData.WorkflowModelGroup.GetListWorkflowModelGroup(function (res) {
        if(res.code == AMResponseCode.ok){

            t.list(res.data);
        }else{
            Component.System.alert.show("Xảy ra lỗi trong quá trình tải dữ liệu");
        }
    });

};