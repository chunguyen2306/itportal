Module.Model = function () {
    // variable
    var t = this;

    t.listWorkFlow = ko.observableArray();
    t.editLink = "/admin-page/workflow/definition?";
    t.mapWorkflowReason = [];

    t.remove = function (wk) {
        if (wk.canFullEdit == false)
            return;

        Component.System.alert.show("Bạn có chắc chắn muốn xóa quy trình '" + wk.DisplayName + "' ?",
            {
                name: 'Xóa',
                cb: function () {
                    APIData.Workflow.DeleteWorkflow(wk.ID, function (res) {
                        if (res.code == AMResponseCode.ok) {
                            Popup.info("Xóa quy trình '" + wk.DisplayName + "' thành công");
                            t.listWorkFlow.remove(wk);
                        } else {
                            Popup.failed();
                        }
                    });
                }
            },{
                name: 'Hoàn tác'
            });
    }
    APIData.Workflow.GetListWorkflow(function (res) {
        if (res.code == AMResponseCode.ok) {

            var lr = t.mapWorkflowReason = [];
            res.referData.listWorkflowReason.forEach(function (item) {
                lr[item.ID] = item.Name;
            });

            t.listWorkFlow(res.data);

        } else {
            Component.System.alert.show("Xảy ra lỗi trong quá trình tải dữ liệu");
        }
    });
};