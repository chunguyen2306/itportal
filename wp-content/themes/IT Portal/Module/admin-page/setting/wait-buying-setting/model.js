
Module.Model = function () {
    var me = this;
    this.setting = {
        ID: 0,
        OptionCode: 'wait-buying',
        OptionName: 'request-mapping',
        OptionValue: []
    };

    this.SettingValue = ko.observableArray([]);

    this.requests = ko.observableArray([]);

    function cb_update(res) {
        res = JSON.parse(res);
        if(res.State){
            alert('Success');
        } else {
            alert('Error');
        }
        loaddata();
    }

    var reRender = function () {
        var temp = me.SettingValue();
        me.SettingValue([]);
        me.SettingValue(temp);
        delete temp;
        Component.Custom.ComboBox.init();
    };

    this.getRequestField = function (id) {
        return me.requests().find(function(e){ return e.ID = id; }).Fields.listFieldDefinition;
    };

    this.do_AddItem = function () {
        me.SettingValue().push({
            requestID: 0,
            fieldName: ''
        });
        reRender();
    };

    this.btnSubmit = function () {
        me.setting.OptionValue = JSON.stringify(me.SettingValue());
        //data.Permission = JSON.stringify(data.Permission);
        if(me.setting.ID > 0){
            APIData.Options.Update(me.setting, cb_update);
        } else {
            APIData.Options.AddNew(me.setting, cb_update);
        }
    };

    var loaddata = function () {
        APIData.Options.GetByCodeAndName('request-mapping', 'wait-buying', function (res) {
            res = JSON.parse(res);
            if(res !== null) {
                res.OptionValue = JSON.parse(res.OptionValue);
                me.setting = res;
                me.SettingValue(res.OptionValue);
                Component.Custom.ComboBox.bindingItem();
            }
        });
    };

    var init = function(){
        Promise.all([
            APIData.Request.GetListRequest('', 0, 100, function (res) {
                res.forEach(function (item) {
                    item.Fields = new APIData.Request.RequestData();
                    item.Fields.loadFieldDefinition(item.ID, function () {});
                });
                me.requests(res);

            })
        ]).then(loaddata);
    };

    init();
};