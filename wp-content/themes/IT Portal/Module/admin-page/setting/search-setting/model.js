
Module.Model = function () {
    var me = this;
    this.RolesList = ko.observableArray([]);

    function cb_update(res) {
        res = JSON.parse(res);
        if(res.State){
            alert('Success');
        } else {
            alert('Error');
        }
        loaddata();
    }

    this.btnSubmit = function () {
        var form = Component.Custom.Form.items.searchOptions.Form;
        var data = form.val();
        data.OptionValue = JSON.stringify(data.OptionValue);
        //data.Permission = JSON.stringify(data.Permission);
        if(data.ID > 0){
            APIData.Options.Update(data, cb_update);
        } else {
            APIData.Options.AddNew(data, cb_update);
        }
    };

    var loaddata = function () {
        APIData.RoleAndPermission.GetAllRole('', 0, 100, function (res) {
            res = JSON.parse(res);
            if(res !== null) {
                me.RolesList(res);
            }
        });

        APIData.Options.GetByCodeAndName('owned-role', 'home-search', function (res) {
            res = JSON.parse(res);
            if(res === null) {
                res = {
                    ID: 0,
                    OptionCode: 'home-search',
                    OptionName: 'owned-role',
                    OptionValue: '[]'
                }
            }

            var form = Component.Custom.Form.items.searchOptions.Form;

            res.OptionValue = JSON.parse(res.OptionValue);

            form.val(res);
        });
    };

    var init = function(){
        loaddata();
    };

    init();
};