Module.Model = function () {
    var me = this;
    this.ResourceTypes = ko.observableArray([]);
    this.Edit = ko.observable({
        RESOURCETYPEID: 0,
        TYPE: ''
    });

    var disableAllEditState = function () {
        allFirstCol = $('table tbody tr td:first-child');
        allLastCol = $('table tbody tr td:last-child');

        allFirstCol.find('span').show();
        allFirstCol.find('input').hide();

        allLastCol.find('[name="view_control"]').show();
        allLastCol.find('[name="edit_control"]').hide();
    };

    var enableEdit = function (row) {
        disableAllEditState();

        firstCol = row.find('td:first-child');
        lastCol = row.find('td:last-child');

        firstCol.find('span').hide();
        firstCol.find('input').show();

        lastCol.find('[name="view_control"]').hide();
        lastCol.find('[name="edit_control"]').show();
    };

    this.do_enableEdit = function (model, e) {
        var row = $(e.target).parent().parent().parent().parent();
        enableEdit(row);
        var textbox = row.find('td:first-child input');
        textbox[0]['previouschange'] = model.TYPE;
    };

    this.do_saveEdit = function (model, e) {
        APIData.ResourceType.update(model.RESOURCETYPEID, model.TYPE, function(res){
            res = JSON.parse(res);
            if(res.State){
                alert('success');
                disableAllEditState();
                me._parent.binding();
            } else {
                alert('error');
            }
        });
    };

    this.do_cancelEdit = function (model, e) {
        var row = $(e.target).parent().parent().parent().parent();
        disableAllEditState();
        var textbox = row.find('td:first-child input');
        model.TYPE = textbox[0]['previouschange'];
        me._parent.binding();
    };

    this.do_createNew = function (model, e) {
        APIData.ResourceType.create(me.Edit().TYPE, function (res) {
            res = JSON.parse(res);
            if(res.State){
                me.ResourceTypes().push(res.Result);
                me.ResourceTypes(me.ResourceTypes());
                alert('success');
            } else {
                alert('fail');
            }
        })
    };

    var init = function(){
        APIData.ResourceType.getAll(function (res) {
            res = JSON.parse(res);
            if(res.State === true) {
                res = res.Result;
                me.ResourceTypes(res);
            }
        });
    };

    init();
};