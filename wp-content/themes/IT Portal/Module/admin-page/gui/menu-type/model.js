Module.Model = function () {
    var me = this;
    this.Data = ko.observableArray([]);
    this.Edit = ko.observable({
        ID: 0,
        Name: ''
    });

    var disableAllEditState = function () {
        allFirstCol = $('table tbody tr td:first-child');
        allLastCol = $('table tbody tr td:last-child');

        allFirstCol.find('span').show();
        allFirstCol.find('input').hide();

        allLastCol.find('[name="view_control"]').show();
        allLastCol.find('[name="edit_control"]').hide();
    };

    var enableEdit = function (row) {
        disableAllEditState();

        firstCol = row.find('td:first-child');
        lastCol = row.find('td:last-child');

        firstCol.find('span').hide();
        firstCol.find('input').show();

        lastCol.find('[name="view_control"]').hide();
        lastCol.find('[name="edit_control"]').show();
    };

    this.do_remove = function (model, e) {
        APIData.MenuType.delete(model.ID, function(res){
            res = JSON.parse(res);
            if(res){
                alert('success');
                loadData();
                me._parent.binding();
            } else {
                alert('error');
            }
        });
    };

    this.do_enableEdit = function (model, e) {
        var row = $(e.target).parent().parent().parent().parent();
        enableEdit(row);
        var textbox = row.find('td:first-child input');
        textbox[0]['previouschange'] = model.Name;
    };

    this.do_saveEdit = function (model, e) {
        APIData.MenuType.update(model.ID, model.Name, function(res){
            res = JSON.parse(res);
            if(res){
                alert('success');
                disableAllEditState();
                me._parent.binding();
            } else {
                alert('error');
            }
        });
    };

    this.do_cancelEdit = function (model, e) {
        var row = $(e.target).parent().parent().parent().parent();
        disableAllEditState();
        var textbox = row.find('td:first-child input');
        model.Name = textbox[0]['previouschange'];
        me._parent.binding();
    };
    
    this.do_createNew = function (model, e) {
        APIData.MenuType.create(me.Edit().Name, function (res) {
            res = JSON.parse(res);
            if(res.State){
                me.Data().push(res.Result);
                me.Data(me.Data());
                alert('success');
            } else {
                alert('fail');
            }
        })
    };

    var loadData = function () {
        APIData.MenuType.getAll(function (res) {
            res = JSON.parse(res);
            if(res) {
                me.Data(res);
            }
        });
    };

    var init = function(){
        loadData();
    };

    init();
};