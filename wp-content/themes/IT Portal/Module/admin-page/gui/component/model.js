Module.Model = function () {
    var me = this;
    this.Components = ko.observableArray([]);
    this.ComponentTypes = ko.observableArray([]);
    this.StandardTypes = ko.observableArray([
        {value: 1, name: 'Có sẵn'},
        {value: 2, name: 'Mua trong nước'},
        {value: 3, name: 'Mua nước ngoài'}
    ]);
    var currentPage = 1;
    const limit = 15;
    var total = 0;
    var pageCount = 0;

    this.Keyword_OnKeyUp = function (e) {
        if (e.key === 'Enter') {
            window.location = Common.build_param({
                page: 1,
                keyword: this.value
            });
        }
    };

    this.do_createNew = function () {

    };

    var Preview_OnClick = function (name, img) {
        Component.System.alert.show('Bạn có muốn xóa ảnh này', {
            name: 'Đồng ý',
            cb: function () {
                var ComponentForm = Component.Custom.Form.get('ComponentDetails');
                var currentData = ComponentForm.val();
                var fileField = ComponentForm.field('Gallery')[0];
                var fieldInput = ComponentForm.field('Gallery').find('input')[0];

                var fieldFile = ComponentForm.field('Gallery');
                var fileIndex = fileField['data'].indexOf(name);
                var newFileIndex = -1;

                if (fieldInput.files !== undefined) {
                    for (var i in fieldInput.files) {
                        if (fieldInput.files[i].name === name) {
                            newFileIndex = i;
                        }
                    }
                }

                if (fileIndex !== -1) {
                    fileField['data'].splice(fileIndex, 1);
                    //APIData.AssetModelDetail.DeleteGallery(name, currentData.ModelID, function () { });
                } else if (newFileIndex !== -1) {
                    delete fieldInput.files[newFileIndex.toString()];
                }
                img.remove();
            }
        }, {
            name: 'Không',
            cb: function () {
            }
        })
    };

    this.getComponentImage = function (id) {
        return '/resource-content/Asset/APIAssetModelDetail/GetThubmails/?ModelID=' + id; //UploadUrl+'/component/'+id+'/'+commentData.Images[0];
    };

    this.do_enableEdit = function (model, e) {
        APIData.AssetModelDetail.GetByModelID(model.COMPONENTID, function (res) {
            res = JSON.parse(res);
            if (res === null) {
                res = {
                    ModelID: model.COMPONENTID,
                    ComponentName: model.COMPONENTNAME,
                    ID: 0,
                    ShortDescription: '',
                    ProductPageDescription: '',
                    Cost: 0,
                    IsDisplay: 1,
                    BuyDays: 7,
                    TransferDays: 2,
                    NeedApproval: 0,
                    ReNewYear: 0,
                    Description: '',
                    Gallery: '[]',
                    Thubmails: '',
                    ManualGuide: '',
                    TypeID: 0,
                    ManufacturerName: model.MANUFACTURERNAME
                }
            } else {
                res.ComponentName = model.COMPONENTNAME;
                res.TypeID = model.COMPONENTTYPEID;
                res.ManufacturerName = model.MANUFACTURERNAME;
            }
            var form = Component.Custom.Form.get('ComponentDetails');
            form.val({
                ModelID: res.ModelID,
                ComponentName: res.ComponentName,
                ID: res.ID,
                TypeID: res.TypeID,
                ShortDescription: res.ShortDescription || '',
                ProductPageDescription: res.ProductPageDescription || '',
                Cost: res.Cost,
                IsDisplay: res.IsDisplay == 1 ? true : false,
                BuyDays: res.BuyDays,
                TransferDays: res.TransferDays,
                NeedApproval: res.NeedApproval == 1 ? true : false,
                ReNewYear: res.ReNewYear == null ? 0 : res.ReNewYear,
                Description: res.Description,
                Gallery: {
                    data: (res.Gallery === 'null') ? [] : JSON.parse(res.Gallery),
                    dataPath: res.ModelID
                },
                ManualGuide: res.ManualGuide,
                Thubmails: {
                    data: res.Thubmails,
                    dataPath: res.ModelID
                },
                ManufacturerName: res.ManufacturerName
            });

            var ComponentForm = Component.Custom.Form.get('ComponentDetails');
            ComponentForm.field('Gallery')[0].File.Preview_OnClick = Preview_OnClick;

            $('[component="popup"][name="details"]')[0].Popup.show();
        });
    };

    this.do_delete = function () {

    };

    var cb_SunmitDetail = function (res) {
        res = JSON.parse(res);
        if (res.State) {
            loadComponentType(currentPage);
            $('[component="popup"][name="details"]')[0].Popup.hide();
            Component.Custom.Form.get('ComponentDetails').resetData();
        } else {
            alert('error');
        }
    };

    var _do_SubmitDetail = function (isUpload) {
        var form = Component.Custom.Form.get('ComponentDetails');
        var data = form.val();
        var fieldFile = form.field('Gallery')[0].File;
        var emlFile = form.field('Gallery')[0];

        var submitData = {
            ModelID: data.ModelID,
            ID: data.ID,
            TypeID: data.TypeID,
            ProductPageDescription: (data.ProductPageDescription),
            ShortDescription: (data.ShortDescription),
            Cost: data.Cost,
            IsDisplay: data.IsDisplay ? 1 : 0,
            BuyDays: data.BuyDays,
            TransferDays: data.TransferDays,
            NeedApproval: data.NeedApproval ? 1 : 0,
            ReNewYear: data.ReNewYear,
            Description: (data.Description),
            ManualGuide: (data.ManualGuide),
            Gallery: [],
            Thubmails: data.Thubmails,
            ManufacturerName: data.ManufacturerName
        };

        if (isUpload) {
            submitData.Gallery = fieldFile.uploadResult.concat(emlFile['data']).unique();
        } else {
            submitData.Gallery = emlFile['data'];
        }
        fieldFile.Clear();

        if (data !== undefined && data.ID !== 0) {
            APIData.AssetModelDetail.Update(submitData, cb_SunmitDetail);
        } else {
            APIData.AssetModelDetail.AddNew(submitData, cb_SunmitDetail);
        }
    };

    this.do_SubmitDetail = function () {
        var form = Component.Custom.Form.get('ComponentDetails');
        var data = form.val();
        var fileNamePrefix = data.ModelID;
        var fieldFile = form.field('Gallery')[0].File;

        if (fieldFile.hasFile()) {
            fieldFile.Upload('upload.component.' + fileNamePrefix, fileNamePrefix, false);
            Common.wait(fieldFile.isUploadDone, function () {
                _do_SubmitDetail(true);
                fieldFile.Reset();
            })
        } else {
            _do_SubmitDetail(false);
        }
    };

    this.do_CancelDetail = function () {
        $('[component="popup"][name="details"]')[0].Popup.hide();
        Component.Custom.Form.get('ComponentDetails').resetData();
    };

    var do_PageChange = function (paging) {
        currentPage = paging.currentPage();
        loadComponentType();
    };

    var loadComponentType = function () {
        var keyword = Common.getUrlParameters()['keyword'];
        var quickedit = Common.getUrlParameters()['quickedit'];
        if (keyword === undefined || keyword === null) {
            keyword = '';
        } else {
            keyword = decodeURI(keyword);
        }
        APIData.Component.paging(keyword, currentPage - 1, limit, function (res) {
            res = JSON.parse(res);
            if (res.State === true) {
                var tableData = res.Result.tableData;
                me.Components(tableData);
                total = res.Result.total;
                pageCount = res.Result.pageCount;
                Component.Custom.Paging.items['componentPaging'].render(pageCount, limit, total);
                Component.Custom.Paging.items['componentPaging'].on_PageChange = do_PageChange;

                if (quickedit !== undefined && quickedit !== null) {
                    me.do_enableEdit({
                        COMPONENTID: parseInt(quickedit)
                    }, null);
                }
            }
        });
    };

    var init = function () {
        loadComponentType();

        /*
        APIData.ComponentType.paging('', 0, 0, function (res) {
            res = JSON.parse(res);
            if(res.State === true) {
                var tableData = res.Result.tableData;
                me.ComponentTypes(tableData);
            }
        });
        */

        $('[component="popup"][name="details"]')[0].Popup.on_Close = me.do_CancelDetail;
    };

    init();
};