Module.Model = function () {
    var me = this;
    this.ResourceCategories = ko.observableArray([]);
    this.Edit = ko.observable({
        RESOURCECATEGORYID: 0,
        CATEGORY: ''
    });

    var disableAllEditState = function () {
        allFirstCol = $('table tbody tr td:first-child');
        allLastCol = $('table tbody tr td:last-child');

        allFirstCol.find('span').show();
        allFirstCol.find('input').hide();

        allLastCol.find('[name="view_control"]').show();
        allLastCol.find('[name="edit_control"]').hide();
    };

    var enableEdit = function (row) {
        disableAllEditState();

        firstCol = row.find('td:first-child');
        lastCol = row.find('td:last-child');

        firstCol.find('span').hide();
        firstCol.find('input').show();

        lastCol.find('[name="view_control"]').hide();
        lastCol.find('[name="edit_control"]').show();
    };

    this.do_enableEdit = function (model, e) {
        var row = $(e.target).parent().parent().parent().parent();
        enableEdit(row);
        var textbox = row.find('td:first-child input');
        textbox[0]['previouschange'] = model.CATEGORY;
    };

    this.do_saveEdit = function (model, e) {
        APIData.ResourceCategory.update(model.RESOURCECATEGORYID, model.CATEGORY, function(res){
            res = JSON.parse(res);
            if(res.State){
                alert('success');
                disableAllEditState();
                me._parent.binding();
            } else {
                alert('error');
            }
        });
    };

    this.do_cancelEdit = function (model, e) {
        var row = $(e.target).parent().parent().parent().parent();
        disableAllEditState();
        var textbox = row.find('td:first-child input');
        model.CATEGORY = textbox[0]['previouschange'];
        me._parent.binding();
    };
    
    this.do_createNew = function (model, e) {
        APIData.ResourceCategory.create(me.Edit().CATEGORY, function (res) {
            res = JSON.parse(res);
            if(res.State){
                me.ResourceCategories().push(res.Result);
                me.ResourceCategories(me.ResourceCategories());
                alert('success');
            } else {
                alert('fail');
            }
        })
    };

    var init = function(){
        APIData.ResourceCategory.getAll(function (res) {
            res = JSON.parse(res);
            if(res.State === true) {
                res = res.Result;
                me.ResourceCategories(res);
            }
        });
    };

    init();
};