/**
 * Created by tientm2 on 9/25/2017.
 */
Module.Model = function () {
    var me = this;
    this.mainmenus = ko.observableArray([]);
    var currentSelected = null;
    var currentItem = null;

    this.sortByIndex = function (array) {

        if(array !== undefined && array.length > 0){
            var result = array.sort(function (a, b) {
                return parseInt(a.Index) - parseInt(b.Index);
            });
            console.log('Result Sort', result);
            return result;
        } else {
            return [];
        }
    };

    var loadMainMenu = function () {
        APIData.PersonalMenu.GetAll(function (res) {
            res = JSON.parse(res);
            me.mainmenus(res);
            Component.System.loading.hide();
        });
    };

   var init = function () {
       loadMainMenu();
   };

   var newItem = function () {
     return {
         Name: 'Menu Name(new)',
         Link: '#',
         Role: '',
         Index: 99
     }
   };

   this.do_AddItem = function (model, e) {
       if(model !== me){
           if(model.Sub === undefined){
               model.Sub = [];
           }
           model.Sub.push(new newItem());
       } else {
           me.mainmenus().push(new newItem());
       }
       var temp = me.mainmenus();
       me.mainmenus([]);
       me.mainmenus(temp);
   };

   this.do_SubmitInfo = function(){
       me.mainmenus().forEach(function (item) {
           item.Sub = me.sortByIndex(item.Sub);
       });
       me.mainmenus(me.sortByIndex(me.mainmenus()));



       APIData.PersonalMenu.Update(me.mainmenus(), function (res) {
           res = JSON.parse(res);
           if(res){
               Component.Custom.Popup.items['infos'].Popup.hide();
           } else {
               console.log('error');
           }
       });
   };

   this.do_CancelInfo = function () {
       Component.Custom.Popup.items['infos'].Popup.hide();
   };

   this.do_Edit = function (model, e) {
       model.Index = model.Index || 0;
       var form = Component.Custom.Form.items['MenuInfoForm'].Form;
       form.val(model);
       Component.Custom.Popup.items['infos'].Popup.show();
   };

   var removeItem = function (item, prop, arr) {

        if(!Array.isArray(arr) || arr === undefined){
            return
        }
        var index = arr.indexOf(item);
        if(index >= 0){
            arr.splice(index, 1);
        } else {
            arr.forEach(function (arrItem) {
                removeItem(item, prop, arrItem[prop]);
            })
        }
   };

   this.do_Remove = function(model){
       var temp = me.mainmenus();

       removeItem(model, 'Sub', temp);

       me.mainmenus([]);
       me.mainmenus(temp);
       me.do_SubmitInfo();
   };

   init();
};