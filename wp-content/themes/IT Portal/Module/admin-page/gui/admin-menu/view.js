Module.View = function (model, _onDone) {
    var me = this;
    this.content = ko.observableArray([]);
    Component.Module.AbstractModuleView.call(this, model, _onDone);

    me.onDone();
};