Module.View = function (model, _onDone) {
    var me = this;
    Component.Module.AbstractModuleView.call(this, model,_onDone );

    $('#txbKeyword').on('keyup', model.Keyword_OnKeyUp);
    me.onDone();
};