Module.Model = function () {
    var me = this;
    this.ComponentTypes = ko.observableArray([]);
    this.ResourceTypes = ko.observableArray([]);
    this.ResourceCategories = ko.observableArray([]);
    this.Edit = ko.observable({
        RESOURCETYPEID: 0,
        TYPE: ''
    });
    var currentPage = 1;
    var descriptionFormat = 'Code: {0} Notes: {1} Icon: {2}';

    this.Keyword_OnKeyUp = function (e) {

        if (e.key === 'Enter') {
            window.location = Common.build_param({
                page: 1,
                keyword: this.value
            });
        }
    };

    this.getPathOfDescription = function(des, path){
        try {
            var regex = new RegExp(path + ": (((?!Notes|Icon|Code).)*)");
            var match = regex.exec(des);
            var result = '';

            if (match) {
                result = match[1].trim();
            }

            if (path === 'Icon') {
                result = UploadUrl + 'component_type/' + result;
            }

            return result;
        } catch (e){

        }
    };

    var cb_delete = function (res) {
        res = JSON.parse(res);
        if(res.State){
            loadComponentType(currentPage);
        } else {
            alert('error');
        }
    };

    this.do_delete = function(model, e){
        APIData.ComponentType.delete(model.COMPONENTTYPEID, cb_delete);
    };

    this.do_enableEdit = function (model, e) {
        Component.Custom.ComboBox.bindingItem();
        var form = Component.Custom.Form.get('ResourceTypeDetails');
        form.val({
            COMPONENTTYPEID: model.COMPONENTTYPEID,
            COMPONENTTYPENAME: model.COMPONENTTYPENAME,
            RESOURCETYPEID: model.RESOURCETYPEID,
            RESOURCECATEGORYID: model.RESOURCECATEGORYID,
            CODE: me.getPathOfDescription(model.DESCRIPTION, 'Code'),
            NOTE: me.getPathOfDescription(model.DESCRIPTION, 'Notes'),
            ICON: []
        });
        $('[component="popup"][name="details"]')[0].Popup.show();
    };

    this.do_createNew = function (model, e) {
        var form = Component.Custom.Form.get('ResourceTypeDetails');
        form.resetData();
        $('[component="popup"][name="details"]')[0].Popup.show();
    };

    var cb_SunmitDetail = function(res){
        res = JSON.parse(res);
        if(res.State){
            loadComponentType(currentPage);
            $('[component="popup"][name="details"]')[0].Popup.hide();
            Component.Custom.Form.get('ResourceTypeDetails').resetData();
        } else {
            alert('error');
        }
    };

    var _do_SubmitDetail = function (isUpload) {
        var form = Component.Custom.Form.get('ResourceTypeDetails');
        var data = form.val();
        var fieldFile = form.field('ICON')[0].File;
        var emlFile = form.field('ICON')[0];
        var iconData = '';

        if(isUpload){
            iconData = fieldFile.uploadResult[0];
        }

        if(data.COMPONENTID !== '') {

            APIData.ComponentType.update({
                COMPONENTTYPEID: data.COMPONENTTYPEID,
                COMPONENTTYPENAME: data.COMPONENTTYPENAME,
                RESOURCETYPEID: data.RESOURCETYPEID,
                RESOURCECATEGORYID: data.RESOURCECATEGORYID,
                DESCRIPTION: descriptionFormat.format(data.CODE, data.NOTE, iconData)
            }, cb_SunmitDetail);
        }
    };

    this.do_SubmitDetail = function(model, e){
        var form = Component.Custom.Form.get('ResourceTypeDetails');
        var data = form.val();
        var fileNamePrefix = data.COMPONENTTYPENAME.replace(/\//g, '-');
        var fieldFile = form.field('ICON')[0].File;

        if(fieldFile.hasFile()){
            fieldFile.Upload('upload.component_type', fileNamePrefix, true);
            Common.wait(fieldFile.isUploadDone, function () {
                _do_SubmitDetail(true);
                fieldFile.Reset();
            })
        } else {
            _do_SubmitDetail(false);
        }

        if(data.COMPONENTTYPEID !== ''){

        }
    };
    this.do_CancelDetail = function(model, e){
        $('[component="popup"][name="details"]')[0].Popup.hide();
        Component.Custom.Form.get('ResourceTypeDetails').resetData();
    };

    var do_PageChange = function (eml) {
        currentPage = eml.innerText;
        loadComponentType(eml.innerText);
    };

    var loadComponentType = function (page) {
        var keyword = Common.getUrlParameters()['keyword'];
        if(keyword === undefined || keyword === null){
            keyword='';
        }else {
            keyword = decodeURI(keyword);
        }
        APIData.ComponentType.paging(keyword, page-1, 10, function (res) {
            res = JSON.parse(res);

            if(res.State === true) {
                var tableData = res.Result.tableData;
                var comTableData = Component.Custom.TableData.get('tableDetails');
                if(!comTableData.isInitPage){
                    comTableData.pageCount(res.Result.pageCount);
                    comTableData.on_PageChange = do_PageChange;
                }
                me.ComponentTypes(tableData);
            }
        });
    };

    var init = function(){
        loadComponentType(currentPage);

        APIData.ResourceType.getAll(function (res) {
            res = JSON.parse(res);
            if(res.State === true) {
                res = res.Result;
                me.ResourceTypes(res);
            }
        });

        APIData.ResourceCategory.getAll(function (res) {
            res = JSON.parse(res);
            if(res.State === true) {
                res = res.Result;
                me.ResourceCategories(res);
            }
        });

        $('[component="popup"][name="details"]')[0].Popup.on_Close = me.do_CancelDetail;
    };

    init();
};