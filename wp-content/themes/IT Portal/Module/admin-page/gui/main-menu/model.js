/**
 * Created by tientm2 on 9/25/2017.
 */
Module.Model = function () {
    var me = this;
    this.mainmenus = ko.observableArray([]);
    this.ResourceTypes = ko.observableArray([]);
    this.MappingSelected = ko.observableArray([]);
    this.MenuTypes = ko.observableArray([]);
    this.PostCategory = ko.observableArray([]);
    var currentSelected = null;
    var currentItem = null;


    var loadMainMenu = function () {
        APIData.MainMenu.GetAsMenu(function (res) {
            res = JSON.parse(res);
            res = res.sort(function (a, b) {
                return a.Parent > b.Parent;
            });
            me.mainmenus(res);
            //console.log(res);
            Component.System.loading.hide();
        });
    };

   var init = function () {
       loadMainMenu();
       APIData.MenuType.getAll(function(res){
           res = JSON.parse(res);
           me.MenuTypes(res);
       });

       APIData.ResourceType.GetAllWithComponentType(function(res){
           res = JSON.parse(res);
           if(res.State) {
               me.ResourceTypes(res.Result);
           }
       });

       APIData.PostCategory.GetAll(function(res){
           res = JSON.parse(res);
           me.PostCategory(res);
       });
   };

   var newItem = function () {
     return {
         ID: 0,
         Name: 'Menu Name(new)',
         Parent: 0,
         Mapping: '[]',
         TypeID: 0,
         Icon: 'no-icon.png'
     }
   };

   this.do_AddNew = function (model, e) {
       Component.System.loading.show();
       APIData.MainMenu.AddNew(model, function (res) {
           res = JSON.parse(res);
           if(res.State){
               if(res.Record.Parent === 0) {
                   res.Record.Parent = res.Record.ID;
                   APIData.MainMenu.Update(res.Record, function () {
                       loadMainMenu();
                   });
               } else {
                   loadMainMenu();
               }
           }
       });

   };

   this.do_CancelAddNew = function (model, e) {
       if(model.Parent != 0){
           var parent = me.mainmenus().find(function (i) {
               return  (i.Parent === model.Parent);
           });
           var index = parent.Childs.indexOf(model);
           parent.Childs.splice(index, 1);
       } else {
           var index = me.mainmenus().indexOf(model);
           me.mainmenus().splice(index, 1);
       }
       rebindMainMenu();
   };

   this.do_AddItem = function (model, e) {
       var eml = $(e.currentTarget);
       if(eml.attr('parent') !== undefined){
           var item = newItem();
           item.Parent = parseInt(eml.attr('parent'));
           var parent = me.mainmenus().find(function (i) {
               return i.ID === item.Parent;
           });
           parent.Childs.push(item);
           rebindMainMenu();
       } else {
           me.mainmenus().push(newItem());
           //me.mainmenus(me.mainmenus());
           rebindMainMenu();
       }
   };

   this.do_DeleteItem = function (model, e) {
       Component.System.loading.show();
        APIData.MainMenu.Delete(model, function () {
            loadMainMenu();
        })
   };

   var initSelectedDataResourceType = function () {
       for(var i =0; i < me.ResourceTypes().length; i++){
           var item = me.ResourceTypes()[i];

           var checkboxs = $('[component="checkbox"][parent="'+item.TYPE+'"]');
           if(checkboxs.length > 0) {
               $('[component="checkbox"][name="' + item.TYPE + '"]').prop('checked', true);
               checkboxs.each(function (j, checkItem) {
                   if ($(checkItem).prop('checked') === false) {
                       $('[component="checkbox"][name="' + item.TYPE + '"]').prop('checked', false);
                   }
               });
           } else {
               $('[component="checkbox"][name="' + item.TYPE + '"]').prop('checked', false);
           }
       }
   };

   this.do_MappingItem = function (model, e) {
       currentSelected = model.ID;
       $('[name="popMapping"] [component="checkbox"]').prop('checked', false);
       me.MappingSelected(JSON.parse(model.Mapping));

       var type = me.MenuTypes().find(function (e) {
           return e.ID === model.TypeID;
       });
       if(type !== undefined){

           switch (type.Name){
               case 'Asset':
                   initSelectedDataResourceType();
                   Component.Custom.Popup.items['popMappingResourceType'].Popup.show();
                   break;
               case 'Post':
                   Component.Custom.Popup.items['popMappingPostCategory'].Popup.show();
                   break;
               default:
                   break;
           }
       }
   };

   this.do_SelectAll = function (model, e) {
       var checkboxs = $('[component="checkbox"][parent="'+model.TYPE+'"]');
       checkboxs.each(function (i, item) {
           if($(item).prop('checked') !== $(e.currentTarget).prop('checked')){
               $(item).trigger('click');
           }
       })
   };

   this.do_UpdateMapping = function () {
       APIData.MainMenu.UpdateMapping(currentSelected, JSON.stringify(me.MappingSelected()), function () {
           loadMainMenu();
           Component.Custom.Popup.Active.hide();
       })
   };

   var rebindMainMenu = function(){
       var temp = me.mainmenus();
       me.mainmenus([]);
       me.mainmenus(temp);
       Component.Custom.ComboBox.init();
       Component.Custom.ComboBox.bindingItem();
       delete temp;
    };

   this.do_EditItem = function (model) {
       model.ID = model.ID *-1;
       model.Parent = model.Parent *-1;
       rebindMainMenu();
   };

   this.do_SaveEdit = function (model) {
       Component.System.loading.show();
       model.ID = model.ID *-1;
       model.Parent = model.Parent *-1;
       var updateModel = (function () {
           return model;
       })();
       delete updateModel.Childs;
       APIData.MainMenu.Update(model, function () {
           loadMainMenu();
       });
   };

   this.do_CancelEditNew = function (model) {
       model.ID = model.ID *-1;
       model.Parent = model.Parent *-1;
       rebindMainMenu();
   };

   this.do_SelectIcon = function (model) {
       currentItem = model;
       var iconSelected = Component.Custom.File.items['fileIconSelected'];
       $('#fileIconSelected').click();
   };

   this.do_UpdateIcon = function (model, e) {

       model = currentItem;
       var iconSelected = Component.Custom.File.items['fileIconSelected'];
       iconSelected.Upload('upload.main-menu', Utils.StringToCodeName(model.Name));
       Common.wait(iconSelected.isUploadDone, function () {
           model.Icon = iconSelected.uploadResult.join('');
           model.ID = model.ID *-1;
           model.Parent = model.Parent *-1;
           me.do_SaveEdit(model);
           loadMainMenu();
       })
   };

   this.getIconPath = function (icon) {
       if(icon !== null && icon !== undefined) {
           return UploadUrl + 'main-menu/' + icon + '?v=' + new Date().getTime();
       } else {
           return UploadUrl + 'main-menu/no-icon.png?v=' + new Date().getTime();
       }
   };

   init();
};