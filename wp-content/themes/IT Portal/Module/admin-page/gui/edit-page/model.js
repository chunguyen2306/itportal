/**
 * Created by lacha
 */
Module.Model = function () {
    var me = this;
    this.pagecontent = ko.observable('');

    var loadNamePageContent = function (page) {
        APIData.EditPage.GetPageContent(page, function (res) {
            res = JSON.parse(res);
            Component.System.loading.hide();
            me.pagecontent(res);
        });
    };

    $(document).ready(function(){
        $("#combobox_page").on("change",function(){
            var temp = $("#combobox_page").find(":selected").text();
            console.log(temp);
            me.pagecontent('');
            loadNamePageContent(temp);
        });
    });

    var init = function () {
        loadNamePageContent("master-page-home.php");
    };

    this.do_SaveEdit = function (pagecontent) {
        var temp = $("#combobox_page").find(":selected").text();
        pagecontent['page'] = temp;
        APIData.EditPage.isExistPageContent(pagecontent, function (resIEPC) {
            resIEPC = JSON.parse(resIEPC);
            if (resIEPC.Result) {
                APIData.EditPage.Update(pagecontent, function (res) {
                    res = JSON.parse(res);
                    if (res.State) {
                        alert('Cập nhật thành công');
                    } else {
                        alert('Cập nhật thất bại');
                    }
                });
            } else {
                APIData.EditPage.AddNew(pagecontent, function (res) {
                    res = JSON.parse(res);
                    if (res.State) {
                        alert('Thêm mới thành công');
                    } else {
                        alert('Thêm mới thất bại');
                    }
                });
            }
        });

    };

    init();
};
