Module.Model = function () {
    var me = this;
    this.RolesList = ko.observableArray([]);
    this.csvData = ko.observableArray([]);
    this.UserRoleList = ko.observableArray([]);
    this.NewPermission = ko.observable({
        ID: -1,
        Domain: '',
        Permission: ''
    });
    const LIMIT_IN_PAGE = 20;

    this.OnCSVUpload = function () {
        var form = Component.Custom.Form.items.ImportCSVForm.Form;
        var field = form.field('File')[0].File;

        field.getFileContent(function (data) {
            var parseData = data.split('\r\n');
            for (var i = 1; i < parseData.length; i++) {
                var itemStr = parseData[i];
                if (itemStr.trim() === '') {
                    continue;
                }
                var headerStr = parseData[0];
                var item = {};

                var headerData = headerStr.split(',');
                headerData.forEach(function (headerItem, index) {
                    headerItem = headerItem.trim();
                    item[headerItem] = itemStr.split(',')[index];
                });
                me.csvData().push(item);
            }
            /*var temp = me.csvData();
            me.csvData([]);
            me.csvData(temp);*/
            me.csvData(me.csvData());
        });


    };

    var doImport = function (index) {
        if (index >= me.csvData().length) {
            alert('Import done');
            Component.Custom.Popup.items.GrantPermissionPopup.Popup.hide();
            loaddata();
            return;
        }
        var data = me.csvData()[index];
        var deptHeadRole = me.RolesList().find(function (e) {
            return e.Name === 'Department Heads';
        });
        data.Permission = [];
        data.Permission.push(deptHeadRole.ID.toString());
        var isExisted = me.UserRoleList().find(function (e) {
            return e.Domain === data.Domain;
        });
        var doImportFunc = null;

        if (isExisted !== null && isExisted !== undefined) {
            data.ID = isExisted.ID;
            if (isExisted.Permission.indexOf('"' + deptHeadRole.ID + '"') >= 0) {
                data.Permission = JSON.parse(isExisted.Permission);
            } else {
                data.Permission = JSON.parse(isExisted.Permission).push(deptHeadRole.ID.toString());
            }
            doImportFunc = APIData.RoleAndPermission.UpdateUserRole;
        } else {
            doImportFunc = APIData.RoleAndPermission.CreateUserRole;
        }
        Promise.all([
            doImportFunc(data, function () {
                //res = JSON.parse(res);
                //if(res.State){
                //    alert('Success');
                //} else {
                //    alert('Error');
                //}
            })
        ]).then(function () {
            doImport(index + 1);
        });

        //console.log(data);

    };

    this.btnImport = function () {
        doImport(0);
    };

    this.btnAddNew = function () {
        var form = Component.Custom.Form.items.GrantPermission.Form;
        form.val({
            ID: 0,
            Domain: '',
            Permission: [],
        })
    };

    function cb_update(res) {
        res = JSON.parse(res);
        if (res.State) {
            alert('Success');
            Component.Custom.Popup.items.GrantPermissionPopup.Popup.hide();
        } else {
            alert('Error');
        }
        loaddata();
    }


    this.btnUpdate = function (model) {
        var form = Component.Custom.Form.items.GrantPermission.Form;
        form.val({
            ID: model.ID,
            Domain: model.Domain,
            Department: model.Department,
            Permission: JSON.parse(model.Permission)
        });
        Component.Custom.Popup.items.GrantPermissionPopup.Popup.show();
    };

    this.btnSubmit = function () {
        var form = Component.Custom.Form.items.GrantPermission.Form;
        var data = form.val();
        //data.Permission = JSON.stringify(data.Permission);
        if (data.ID > 0) {
            APIData.RoleAndPermission.UpdateUserRole(data, cb_update);
        } else {
            APIData.RoleAndPermission.CreateUserRole(data, cb_update);
        }
    };

    this.btnDelete = function (model) {
        APIData.RoleAndPermission.DeleteUserRole(model.ID, cb_update);
    };

    this.btnCancel = function () {

    };

    var do_PageChange = function (paging) {
        var currentPage = paging.currentPage();
        loaddata('', currentPage);
        $('html').animate({scrollTop: 0}, 500);
        var home = $('[section="home"]');
        home.removeClass('toggle');
    };

    this.Keyword_OnKeyUp = function (e) {
        if (e.key === 'Enter') {
            loaddata(this.value);
        }
        if (this.value == '') {
            loaddata();
        }
    };

    var loaddata = function (domain, page) {
        if (domain === null || domain === undefined) {
            domain = '';
        }
        if (page === null || page === undefined) {
            page = 1;
        }
        APIData.RoleAndPermission.GetAllRole('', 0, 100, function (res) {
            res = JSON.parse(res);
            if (res !== null) {
                me.RolesList(res);
            }
        });

        APIData.RoleAndPermission.GetAllUserRole(domain, page - 1, LIMIT_IN_PAGE, function (res) {
            res = JSON.parse(res);
            if (res !== null) {
                me.UserRoleList(res.Result);
                var pageCount = Math.floor(res.Total / LIMIT_IN_PAGE);
                if (pageCount * LIMIT_IN_PAGE < res.Total) {
                    pageCount++;
                }
                if (Component.Custom.Paging.items['grantPaging'] !== undefined) {
                    Component.Custom.Paging.items['grantPaging'].render(pageCount, LIMIT_IN_PAGE, res.Total, page);
                    Component.Custom.Paging.items['grantPaging'].on_PageChange = do_PageChange;
                }
            }
        });
    };

    var init = function () {
        loaddata();
    };

    init();
};