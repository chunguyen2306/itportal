Module.View = function (model, _onDone) {
    var me = this;
    Component.Module.AbstractModuleView.call(this, model,_onDone );

    Common.wait(function () {
            return Component.Custom.Form.isDone && Component.Custom.Form.items.ImportCSVForm.Form.field('File')[0].File !== undefined
        }, function () {
        var uploadField = Component.Custom.Form.items.ImportCSVForm.Form.field('File')[0].File;
        uploadField.OnChange = model.OnCSVUpload;
    });

    $('#txbSearchDomain').on('keyup', model.Keyword_OnKeyUp);

    $('#txbSearchDomain').on('change', function () {
        if ($('#txbSearchDomain').val() == '') {
            model.Keyword_OnKeyUp;
        }
    });

    me.onDone();
};