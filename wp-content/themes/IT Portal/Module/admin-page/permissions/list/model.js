Module.Model = function () {
    var me = this;
    this.RolesList = ko.observableArray([]);
    this.Edit = ko.observable({
        ID: 0,
        Name: '',
        Description: ''
    });

    var disableAllEditState = function () {
        allFirstCol = $('table tbody tr td:not(:last-child)');
        allLastCol = $('table tbody tr td:last-child');

        allFirstCol.find('span').show();
        allFirstCol.find('input').hide();

        allLastCol.find('[name="view_control"]').show();
        allLastCol.find('[name="edit_control"]').hide();
    };

    var enableEdit = function (row) {
        disableAllEditState();

        firstCol = row.find('td:not(:last-child)');
        lastCol = row.find('td:last-child');

        firstCol.find('span').hide();
        firstCol.find('input').show();

        lastCol.find('[name="view_control"]').hide();
        lastCol.find('[name="edit_control"]').show();
    };

    this.do_enableEdit = function (model, e) {
        var row = $(e.target).parent().parent().parent().parent();
        enableEdit(row);
        var txbName = row.find('td:nth-child(1) input');
        var txbDescrition = row.find('td:nth-child(2) input');
        txbName[0]['previouschange'] = model.Name;
        txbDescrition[0]['previouschange'] = model.Description;
    };

    this.do_saveEdit = function (model, e) {
        APIData.RoleAndPermission.UpdateRole(model, function(res){
            res = JSON.parse(res);
            if(res.State){
                alert('success');
                disableAllEditState();
                me._parent.binding();
            } else {
                alert('error');
            }
        });
    };

    this.do_cancelEdit = function (model, e) {
        var row = $(e.target).parent().parent().parent().parent();
        disableAllEditState();

        var txbName = row.find('td:nth-child(1) input');
        var txbDescrition = row.find('td:nth-child(2) input');
        model.Name = txbName[0]['previouschange'];
        model.Description = txbDescrition[0]['previouschange'];

        me._parent.binding();
    };

    this.do_createNew = function (model, e) {
        APIData.RoleAndPermission.CreateRole(me.Edit(), function (res) {
            res = JSON.parse(res);
            if(res.State){
                me.RolesList().push(res.Result);
                me.RolesList(me.RolesList());
                alert('success');
            } else {
                alert('fail');
            }
        })
    };

    this.do_delete = function (model, e) {
        APIData.RoleAndPermission.DeleteRole(model.ID, function (res) {
            res = JSON.parse(res);
            if(res.State){
                var index = me.RolesList().indexOf(model);
                me.RolesList().splice(index, 1);
                me.RolesList(me.RolesList());
                alert('success');
            } else {
                alert('fail');
            }
        })
    };

    var init = function(){
        APIData.RoleAndPermission.GetAllRole('', 0, 100, function (res) {
            res = JSON.parse(res);
            if(res !== null) {
                me.RolesList(res);
            }
        });
    };

    init();
};