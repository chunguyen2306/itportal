Module.Model = function () {
    var me = this;
    this.RolesList = ko.observableArray([]);
    this.FeaturesList = ko.observableArray([]);
    this.FeatureRoleList = ko.observableArray([]);
    this.OrgFeaturesList = ko.observableArray([]);
    this.OrgFeaturesListLength = ko.observable(0);
    this.NewPermission = ko.observable({
        ID: -1,
        Domain: '',
        Permission: ''
    });

    this.btnAddNew = function () {
        var form = Component.Custom.Form.items.RoleFeature.Form;
        form.val({
            Permission: me.RolesList()[0].Name,
            FeatureName: ''
        });
    };

    function cb_update(res) {
        res = JSON.parse(res);
        if (res.State) {
            alert('Success');
            Component.Custom.Popup.items.RoleFeaturePopup.Popup.hide();
        } else {
            alert('Error');
        }
        loadData();
    }

    this.btnUpdate = function (model) {
        var form = Component.Custom.Form.items.RoleFeature.Form;
        form.val({
            Permission: model.RoleName,
            FeatureName: JSON.parse(model.FeatureID)
        });
        Component.Custom.Popup.items.RoleFeaturePopup.Popup.show();
    };

    var isExist = function (roleName) {
        for (var i = 0; i < me.FeatureRoleList().length; i++) {
            if (roleName == me.FeatureRoleList()[i].RoleName) {
                return true;
            }
        }
        return false;
    }

    var getRoleID = function (roleName) {
        for (var i = 0; i < me.RolesList().length; i++) {
            if (roleName == me.RolesList()[i].Name) {
                return me.RolesList()[i].ID;
            }
        }
        return false;
    }

    this.btnSubmit = function () {
        var form = Component.Custom.Form.items.RoleFeature.Form;
        var data = form.val();
        //data.FeatureName = JSON.stringify(data.FeatureName);

        var dataSubmit = {};
        dataSubmit.RoleID = getRoleID(data.Permission);
        dataSubmit.FeatureID = data.FeatureName;
        if (isExist(data.Permission)) {
            APIData.RoleAndFeature.UpdateFeaturesRole(dataSubmit, cb_update);
        } else {
            APIData.RoleAndFeature.CreateFeaturesRole(dataSubmit, cb_update);
        }
    };

    this.btnDelete = function (model) {
        APIData.RoleAndFeature.DeleteFeatureRole(model.ID, cb_update);
    };

    this.btnCancel = function () {

    };

    var ReorganizeFeatureList = function (data) {
        var result = [];
        data.forEach(function (item) {
            var FeatureName = item.FeatureName.split('.')[0];
            result[FeatureName] = result[FeatureName] || {};
            result[FeatureName].Name = 'admin-page.permission.permissions-by-function.labelFeatureList-' + FeatureName;
            result[FeatureName].Features = result[FeatureName].Features || [];
            result[FeatureName].Features.push(item);
        });
        for(var name in result){
            if(typeof result[name] !== 'function') {
                result.push(result[name]);
                delete result[name];
            }
        }
        me.OrgFeaturesListLength(result.length*2+3);
        return result;
    };

    var loadData = function () {
        APIData.RoleAndPermission.GetAllRole('', 0, 100, function (res) {
            res = JSON.parse(res);
            if (res !== null) {
                me.RolesList(res);
                Component.Custom.ComboBox.bindingItem();

            }
        });

        APIData.RoleAndFeature.GetAllFeatures(0, 100, function (res) {
            res = JSON.parse(res);
            if (res !== null) {
                me.FeaturesList(res);
                me.OrgFeaturesList(ReorganizeFeatureList(res));
            }
        });

        APIData.RoleAndFeature.GetAllFeaturesRole(0, 100, function (res) {
            res = JSON.parse(res);
            //console.log(res);
            if (res !== []) {
                me.FeatureRoleList(res);
            }
        });
    };

    var init = function () {
        loadData();
    };

    init();
};