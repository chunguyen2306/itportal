Module.Model = function () {
    var me = this;
    this.RolesList = ko.observableArray([]);
    this.UserRoleList = ko.observableArray([]);
    this.Keyword = ko.observable('');

    this.btnAddNew = function () {
        var form = Component.Custom.Form.items.GrantPermission.Form;
        form.val({
            ID: 0,
            Domain: '',
            Name: '',
            RoleID: 0,
            empCode: ''
        })
    };

    function cb_update(res) {
        res = JSON.parse(res);
        if(res.State){
            alert('Success');
            Component.Custom.Popup.items.GrantPermissionPopup.Popup.hide();
        } else {
            alert('Error');
        }
        loaddata();
    }


    this.btnUpdate = function (model) {
        var form = Component.Custom.Form.items.GrantPermission.Form;
        form.val(model);
        Component.Custom.Popup.items.GrantPermissionPopup.Popup.show();
    };

    this.btnSubmit = function () {
        var form = Component.Custom.Form.items.GrantPermission.Form;
        var data = form.val();
        //data.Permission = JSON.stringify(data.Permission);
        if(data.ID > 0){
            APIData.UserRoles.UpdateUserRole(data, cb_update);
        } else {
            APIData.UserRoles.CreateUserRole(data, cb_update);
        }
    };

    this.btnDelete = function (model) {
        APIData.UserRoles.DeleteUserRole(model.ID, cb_update);
    };

    this.btnCancel = function () {

    };

    this.Keyword_OnKeyUp = function (model, e) {
        if (e.key === 'Enter') {
            APIData.UserRoles.GetAllUserRole(me.Keyword, 0, 15, function (res) {
                res = JSON.parse(res);
                if(res !== null) {
                    me.UserRoleList(res);
                }
            });
        }
    };

    var loaddata = function () {
        APIData.UserRoles.GetAllRole('', 0, 100, function (res) {
            res = JSON.parse(res);
            if(res !== null) {
                me.RolesList(res);
                Component.Custom.ComboBox.bindingItem();
            }
        });

        APIData.UserRoles.GetAllUserRole('', 0, 15, function (res) {
            res = JSON.parse(res);
            if(res !== null) {
                me.UserRoleList(res);
            }
        });
    };

    var init = function(){
        loaddata();
    };

    init();
};