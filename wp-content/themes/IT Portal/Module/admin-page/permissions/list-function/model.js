Module.Model = function () {
    var me = this;
    this.FeaturesList = ko.observableArray([]);
    this.Edit = ko.observable({
        DisplayName: '',
        Description: ''
    });

    var disableAllEditState = function () {
        allFirstCol = $('table tbody tr td:not(:last-child)');
        allLastCol = $('table tbody tr td:last-child');

        allFirstCol.find('span').show();
        allFirstCol.find('input').hide();

        allLastCol.find('[name="view_control"]').show();
        allLastCol.find('[name="edit_control"]').hide();
    };

    var enableEdit = function (row) {
        disableAllEditState();

        firstCol = row.find('td:not(:last-child)');
        lastCol = row.find('td:last-child');

        firstCol.find('span').hide();
        firstCol.find('input').show();

        lastCol.find('[name="view_control"]').hide();
        lastCol.find('[name="edit_control"]').show();
    };

    var clearInput = function () {
        me.Edit({
            FeatureName: '',
            DisplayName: '',
            Description: ''
        });
    }

    this.do_enableEdit = function (model, e) {
        var row = $(e.target).parent().parent().parent().parent();
        enableEdit(row);
        var txbFeatureName = row.find('td:nth-child(1) input');
        var txbDisplayName = row.find('td:nth-child(2) input');
        var txbDescription = row.find('td:nth-child(3) input');
        txbFeatureName[0]['previouschange'] = model.FeatureName;
        txbDisplayName[0]['previouschange'] = model.DisplayName;
        txbDescription[0]['previouschange'] = model.Description;
    };

    this.do_saveEdit = function (model, e) {
        console.log(model);
        APIData.RoleAndFeature.UpdateFeature(model, function(res){
            res = JSON.parse(res);
            if(res.State){
                alert('success');
                disableAllEditState();
                me._parent.binding();
            } else {
                alert('error');
            }
        });
    };

    this.do_cancelEdit = function (model, e) {
        var row = $(e.target).parent().parent().parent().parent();
        disableAllEditState();
        var txbFeatureName = row.find('td:nth-child(1) input');
        var txbDisplayName = row.find('td:nth-child(2) input');
        var txbDescription = row.find('td:nth-child(3) input');
        model.FeatureName = txbFeatureName[0]['previouschange'];
        model.DisplayName = txbDisplayName[0]['previouschange'];
        model.Description = txbDescription[0]['previouschange'];

        me._parent.binding();
    };

    this.do_createNew = function (model, e) {
        APIData.RoleAndFeature.CreateFeature(me.Edit(), function (res) {
            res = JSON.parse(res);
            console.log(res);
            if(res.State){
                me.FeaturesList().push(res.Result);
                me.FeaturesList(me.FeaturesList());
                alert('success');
            } else {
                alert('fail');
            }
        });
        location.reload();
    };

    this.do_delete = function (model, e) {
        APIData.RoleAndFeature.DeleteFeature(model.ID, function (res) {
            res = JSON.parse(res);
            if(res.State){
                var index = me.FeaturesList().indexOf(model);
                me.FeaturesList().splice(index, 1);
                me.FeaturesList(me.FeaturesList());
                alert('success');
            } else {
                alert('fail');
            }
        });
    };

    var init = function(){
        APIData.RoleAndFeature.GetAllFeatures(0, 100, function (res) {
            res = JSON.parse(res);
            console.log(res);
            if(res !== null) {
                me.FeaturesList(res);
            }
        });
    };

    init();
};