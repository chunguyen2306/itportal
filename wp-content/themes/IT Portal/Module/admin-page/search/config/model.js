function kolog(val) {
    console.log(val);
    return val;
}

Module.Model = function () {
    var t = this;
    t.data = ko.observable({});
    t.workflows = ko.observableArray();

    function addElement(el, observArray, elBefore, elAfter) {

        var list = observArray();
        var index;
        if (elBefore)
            index = list.indexOf(elBefore) + 1;
        else if (elAfter)
            index = list.indexOf(elAfter);
        else
            index = list.length;

        observArray.splice(index, 0, el);
    }

    function getJoEditable(el) {
        var joContainer = $(el);
        while (joContainer.length && joContainer.hasClass("edit-able") == false) {
            joContainer = joContainer.parent();
        }
        return joContainer;
    }

    var filter = t.filter = new function () {
        var ft = this;
        ft.list = ko.observableArray();
        ft.editing = ko.observable();

        ft.add = function (elBefore, elAfter) {
            var re = {Name: null, DisplayName: "", ColumnName: "", style: {width: ""}};

            addElement(re, ft.list, elBefore, elAfter);
            return re;
        };
        ft.remove = function (data) {
            if (data.Name) {
                Component.System.alert.show("Bạn có chắc muốn điều kiện lọc " + data.DisplayName, {
                    name: "Xóa",
                    cb: function () {
                        ft.list.remove(data);
                    }
                }, {
                    name: "Hoàn tác"
                });
            } else {
                ft.list.remove(data);
            }
        };
        ft.edit = function (data, e) {
            var joContainer = getJoEditable(e.currentTarget);
            joContainer.addClass("active");
            ft.editing(data);
        };
        ft.doneEdit = function (data, e) {
            ft.list.update(ft.editing());
            ft.editing(null);
        };

        ft.getSaveData = function () {
            var list = ft.list();
            var errors = [];
            list.forEach(function (item) {
                if (!item.DisplayName)
                    errors.push("Điều kiện lọc chưa được đặt tên");
            });

            return {
                data: list,
                error: errors.length ?
                    "<br>&nbsp;&nbsp;&nbsp;" + errors.join("<br>&nbsp;&nbsp;&nbsp;")
                    : ""
            }
        }
    };

    var column = t.column = new function () {
        var cl = this;
        cl.list = ko.observableArray();
        cl.editing = ko.observable();
        cl.typeID = {
            baseValue: 1,
            fieldValue: 2,
            curStepValue: 3,
            stepValue: 4,
            combined: 5
        };
        cl.types = [
            {ID: cl.typeID.baseValue, Name: "Thông tin cơ bản"},
            {ID: cl.typeID.fieldValue, Name: "Thông tin mở rộng"},
            {ID: cl.typeID.curStepValue, Name: "Thông tin hiện trạng"},
            {ID: cl.typeID.stepValue, Name: "Thông tin phê duyệt"},
            {ID: cl.typeID.combined, Name: "Thông tin kết hợp"}
        ];

        cl.add = function (elBefore, elAfter) {
            var re = {Name: null, DisplayName: "", Source: "", TypeID: cl.typeID.baseValue, style: {width: ""}};

            addElement(re, cl.list, elBefore, elAfter);

            return re;
        };
        cl.remove = function (data) {
            if (cl.list().length < 2) {
                Component.System.alert.show("Không thể xóa hết tất cả các cột");
                return;
            } else if (data.Name) {
                Component.System.alert.show("Bạn có chắc muốn xóa cột " + data.DisplayName, {
                    name: "Xóa",
                    cb: function () {
                        cl.list.remove(data);
                    }
                }, {
                    name: "Hoàn tác"
                });
            } else {
                cl.list.remove(data);
            }
        };
        cl.edit = function (data) {
            source.edit(data);
            cl.editing(data);

            Component.System.alert.show($("#column-editor"),
                {
                    name: 'Xong',
                    cb: function () {
                        joStyle.removeClass('column-editor-popup');
                        cl.doneEdit();
                    }
                }
            );

            var joStyle = $('.sys-alert.show');
            joStyle.addClass('column-editor-popup');
        };
        cl.doneEdit = function () {

            var editing = cl.editing();
            cl.list.update(editing);
            cl.editing(null);
            source.doneEdit(editing);
        };
        cl.getSaveData = function () {
            var columns = cl.list();
            var warningColumnNames = [];
            var errors = [];
            var selectedWFIDs = [];
            t.workflows().forEach(function (wf) {
                if (wf.selected)
                    selectedWFIDs.push(wf.ID);
            });
            columns.forEach(function (col) {
                if (!col.DisplayName) {
                    errors.push("Cột chưa được đặt tên");
                    return;
                }

                var src = col.src;
                if (Array.isArray(src) == false)
                    return;

                // check src đã đầy đủ cho tất cả selected WF chưa
                // nếu chưa thì đưa vào waring list để thông báo
                var newSrc = [];
                src.forEach(function (srcCof) {
                    if (selectedWFIDs.indexOf(srcCof.wfID) != -1)
                        newSrc.push(srcCof);
                    else if (warningColumnNames.indexOf(col.DisplayName) == -1)
                        warningColumnNames.push(col.DisplayName);
                })
                col.src = newSrc;
            });

            return {
                data: JSON.stringify(columns), // chuyển sang JSON để tránh lỗi trên php, php parse request param kiểu số thành string
                error: errors.length ?
                    "<br>&nbsp;&nbsp;&nbsp;" + errors.join("<br>&nbsp;&nbsp;&nbsp;")
                    : "",
                warning: warningColumnNames.length ?
                    "Cột chưa cấu hình cho tất cả các quy trình được chọn<br> &nbsp; &nbsp;" + warningColumnNames.join("<br> &nbsp; &nbsp;")
                    : ""
            }
        }
    };

    var source = t.source = new function () {
        var sr = this;

        sr.list = ko.observableArray();
        sr.editing = ko.observable();
        sr.editing.typeID = ko.observable();
        sr.editing.renderType = (function () {
            var re = {
                isSelect: ko.observable(),
                isTable: ko.observable(),
                isCombined: ko.observable(),
            };

            re.isNone = function(val){
                if(val){
                    re.isSelect(false);
                    re.isTable(false);
                    re.isCombined(false);
                }
            };
            re.isSelect.subscribe(function(val){
                if(val){
                    re.isTable(false);
                    re.isCombined(false);
                }
            });
            re.isTable.subscribe(function(val){
                if(val){
                    re.isSelect(false);
                    re.isCombined(false);
                }
            });
            re.isCombined.subscribe(function(val){
                if(val){
                    re.isSelect(false);
                    re.isTable(false);
                }
            });

            return re;
        })();

        function fieldIsSame(field1, field2) {
            if (field1 == null || field2 == null)
                return field1 == field2;

            if (field1.RequestDataTypeID != field2.RequestDataTypeID)
                return false;

            if ((field1.parent && !field2.parent) || (!field1.parent && field2.parent))
                return false;

            return true;
        }

        function stepIsSame(step1, step2) {
            if (step1 == null || step2 == null)
                return step1 == step2;

            if (step1.Role == step2.Role)
                return true;

            var role1, role2;
            var index = step1.Role.indexOf(':');
            if (index != -1)
                role1 = step1.Role.substr(0, index);
            else
                return false;

            index = step2.Role.indexOf(':');
            if (index != -1)
                role2 = step2.Role.substr(0, index);
            else
                return false;

            return role1 == role2;
        }

        sr.doneEdit = function (editing) {
            switch (sr.editing.typeID()) {
                case column.typeID.fieldValue:
                case column.typeID.stepValue:
                    var list = sr.list();
                    var src = [];
                    list.forEach(function (item) {
                        if (!item.wf.selected || item.selected == null)
                            return;

                        var sl = item.selected;
                        var temp = {wfID: item.wf.ID, name: sl.Name};
                        if (sl.parent)
                            temp.parent = sl.parent.Name;
                        src.push(temp);
                    });
                    editing.src = src;
                    break;
                case column.typeID.combined:
                    break;
            }
            sr.list(null);
            sr.editing.typeID(null);
        }
        sr.edit = function (col) {
            sr.editing(col);
            sr.editing.typeID(col.TypeID);
        };
        sr.onSourceChange = function (item) {
            sr.list.update(item);
        };
        sr.autoAnother = function (data) {
            var fnIsSame;
            if (sr.editing.typeID() == column.typeID.fieldValue) {
                fnIsSame = fieldIsSame;
            } else {
                fnIsSame = stepIsSame
            }

            var dataSelected = data.selected;
            sr.list().forEach(function (srcCf) {
                // skip if no select wf
                if (srcCf.wf.selected == false || srcCf == data)
                    return;

                // skip if select is same
                var srcSelected = srcCf.selected;
                if (fnIsSame(dataSelected, srcSelected))
                    return;

                // find same select and set it, skip if not any
                var options = srcCf.options;
                var len = options.length;
                for (var i = 0; i < len; i++) {
                    var option = options[i];
                    if (fnIsSame(option, dataSelected) == false)
                        continue;

                    srcCf.selected = option
                }
            });
            sr.list.update();
        };
        sr.editing.typeID.subscribe(function (typeID) {
            if (typeID == null) {
                sr.list(list);
                return;
            }
            sr.editing.renderType.isNone(1);

            var col = sr.editing();
            if (col)
                col.TypeID = typeID;
            var list;
            var isRenderType = null;

            switch (typeID) {
                case column.typeID.baseValue:
                    isRenderType = sr.editing.renderType.isSelect;
                    list = [
                        {attr: 'ID', name: 'Mã yêu cầu'},
                        {attr: 'Creator', name: 'Người tạo'},
                        {attr: 'CreateDate', name: 'Thời gian tạo'},
                        {attr: 'RequestStatusID', name: 'Trạng thái'}
                    ];
                    break;
                case column.typeID.curStepValue:
                    isRenderType = sr.editing.renderType.isSelect;
                    list = [
                        {attr: 'Owner', name: 'Người duyệt'},
                        {attr: 'IsApproved', name: 'Trạng thái duyệt'},
                        {attr: 'StartDate', name: 'Thời gian bắt đầu'},
                        {attr: 'ActionDate', name: 'Thời gian duyệt'},
                        {attr: 'MaxIntendTime', name: 'Thời gian duyệt mong đợi tối đa'},
                        {attr: 'MinIntendTime', name: 'Thời gian duyệt mong đợi tối thiểu'}
                    ];
                    break;
                case column.typeID.fieldValue:
                case column.typeID.stepValue:
                    isRenderType = sr.editing.renderType.isTable;
                    list = [];
                    t.workflows().forEach(function (wf) {
                        var src = col ? col.src : [];
                        if (Array.isArray(src) == false)
                            src = [];
                        var len = src.length;

                        // srcCf
                        var srcCf = null;
                        for (var i = 0; i < len; i++) {
                            if (src[i].wfID == wf.ID) {
                                srcCf = src[i];
                                break;
                            }
                        }
                        if (srcCf == null)
                            srcCf = {};

                        var selected;
                        var options = typeID == column.typeID.fieldValue ? wf.fields : wf.steps;
                        options.forEach(function (item) {
                            if (srcCf.name == item.Name)
                                selected = item;

                            if (wf._source_is_field_processed || item.ParentID == null)
                                return;

                            var len = options.length;
                            for (var i = 0; i < len; i++) {
                                var parent = options[i];
                                if (parent.ID == item.ParentID) {
                                    item.parent = parent;
                                    item.DisplayName = parent.DisplayName + " -> " + item.DisplayName;
                                    break;
                                }
                            }
                        });
                        list.push({selected: selected, options: options, wf: wf});

                        if (typeID == column.typeID.fieldValue)
                            wf._source_is_field_processed = true;
                    });

                    sr.list(list);
                    break;
                case column.typeID.combined:
                    isRenderType = sr.editing.renderType.isCombined;
                    break;
            }

            sr.list(list);
            isRenderType(1);
        });

    };

    t.onDisplayNameInput = function (data, e) {
        var el = e.currentTarget;
        if (el.innerText.indexOf('\n') != -1)
            el.innerText = el.innerText.replace(/\n/g, '');

        el._changed = true;
    };

    t.onDisplayNameBlur = function (data, e) {
        var el = e.currentTarget;
        if (data.Name || el._changed == false || el.innerText == "")
            return;

        delete el._changed;
        data.DisplayName = el.innerText;

        /// gen name code
        var tempName = Utils.StringToCodeName(data.DisplayName);
        var name = tempName;

        // check name is exist
        function isNameExist(name, list) {
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                if (item.listChild) {
                    if (isNameExist(name, item.listChild))
                        return true;
                } else if (list[i].Name == name) {
                    return true;
                }
            }
        }

        /// tránh name code trùng nhau
        var count = null;
        var maxCount = 1000;
        while (count <= maxCount) {
            if (t.data().Name == name || isNameExist(name, column.list())) {
                count = count ? (count + 1) : 1;
                name = tempName + count;
            } else {
                break;
            }
        }
        if (count > maxCount) {
            console.warn("gen name code failed");
            return;
        }

        data.Name = name;
    };

    t.getWidthForBindding = function (width) {
        if (!width)
            return '';

        var re = width.toString();
        if (re.endsWith("px") || re.endsWith("%"))
            return re;

        return re.length ? re + '%' : '';
    };

    t.save = function () {
        var errors = [];
        var warnings = [];

        // check search config data
        var requestSearchData = t.data();
        if (!requestSearchData.DisplayName)
            errors.push("<br>Cấu hình lọc chưa được đặt tên");

        // check filters
        var ftData = filter.getSaveData();
        if (ftData.error) {
            errors.push("Lỗi điều kiện lọc: ");
            errors.push(ftData.error);
        }
        if (ftData.warning) {
            warnings.push("Cảnh báo điều kiện lọc chưa tối ưu: ");
            warnings.push(ftData.warning);
        }

        // check columns
        var clData = column.getSaveData();
        if (clData.error) {
            errors.push("Lỗi cột dữ liệu: ");
            errors.push(clData.error);
        }
        if (clData.warning) {
            warnings.push("Cảnh báo cột dữ liệu chưa tối ưu: ");
            warnings.push(clData.warning);
        }

        // if error any, message and return
        if (errors.length) {
            Component.System.alert.show(
                "Không thể lưu cấu hình"
                + errors.join("<br>")
                + "<br><br>"
                + warnings.join("<br>")
            );

            return;
        }

        function save() {
            if (rsID) {
                APIData.RequestSearch.UpdateSearchRequest(requestSearchData, clData.data, ftData.data, onSaveDone);
            } else {
                APIData.RequestSearch.InsertSearchRequest(requestSearchData, clData.data, ftData.data, onSaveDone);
            }
        }
        function onSaveDone(res) {
            if (res.code == AMResponseCode.ok) {
                var mes = rsID ? 'Cập nhật thành công cấu hình: "' : 'Thêm thành công cấu hình mới: "';
                mes += requestSearchData.DisplayName;
                mes += '"';
                Component.System.alert.show(mes);
                rsID = requestSearchData.ID = res.data.data.ID;
                Utils.setURLValue("ID", rsID);
            } else {
                Popup.show(res);
            }
        }

        // save
        if (warnings.length) {
            Component.System.alert.show("Dường như bạn cấu hình thiếu<br>" + warnings.join("<br>"),
                {name: "Xem lại"},
                {name: "Tôi chắc chắn đã cấu hình đầy đủ và muốn lưu", cb: save}
            );
        } else {
            save();
        }
    }

    // init data
    var rsID = Utils.getURLValue("ID");
    if (rsID == null) {
        t.data({
            DisplayName: ""
        });

        var idCol = t.column.add();
        idCol.Name = "ID";
        idCol.DisplayName = "Mã yêu cầu";
        idCol.src = "ID";
    }
    APIData.RequestSearch.GetRequestSearchEditData(rsID, function (res) {
        if (res.code == AMResponseCode.ok) {
            if (rsID) {
                t.data(res.data.data);
                column.list(res.data.columns);
                filter.list(res.data.filters);

                var columns = res.data.columns;
                if (columns && columns.length) {
                    var len = columns.length;
                    for (var i = 0; i < len; i++) {
                        var src = columns[i].src;
                        if (Array.isArray(src) == false)
                            continue;

                        var lensrc = src.length;
                        res.refer.workflows.forEach(function (wf) {
                            if (wf.selected)
                                return;

                            wf.selected = false;
                            for (var isrc = 0; isrc < lensrc; isrc++) {
                                var srcCf = src[isrc];
                                if (srcCf.wfID == wf.ID) {
                                    wf.selected = true;
                                    break;
                                }
                            }
                        });

                        break;
                    }
                }
            }

            t.workflows(res.refer.workflows);

            // column.edit(res.data.columns[1]);
        } else {
        }
    });
};


// field
t = [
    {wfID: 80, name: 'nyc'},
];


t = [
    {wfID: 81, name: 'model', parent: 'chi_tiet_yeu_cau'},
    {wfID: 82, name: 'model', parent: 'chi_tiet_yeu_cau'},
];


// field with child
t = [
    {wfID: 80, name: 'models', child: 'model'},
];

// step
t = [
    {wfID: 80, name: 'models', child: 'model'},
];












