Module.Model = function () {
    // variable
    var t = this;

    t.editLink = "/admin-page/search/config?";
    t.list = ko.observableArray();

    t.remove = function (data) {
        function doRemove(){
            APIData.RequestSearch.DeleteRequestSearch(data.ID, function (res) {
                if(res.code == AMResponseCode.ok){
                    Component.System.alert.show("Xóa cấu hình '" + data.DisplayName + "' thành công");
                    t.list.remove(data);
                }else{
                    Component.System.alert.failed();
                }
            });
        }

        Component.System.alert.show(
            "Bạn có chắc chắn muốn xóa cấu hình '" + data.DisplayName + "' ?",
            {name: "Xóa", cb: doRemove},
            {name: "Hoàn tác"}
        );

    };

    APIData.RequestSearch.GetListRequestSearch(function (res) {
        if(res.code == AMResponseCode.ok){
            t.list(res.data);
        }else{
            Component.System.alert.show("Xảy ra lỗi trong quá trình tải dữ liệu");
        }
    });
};