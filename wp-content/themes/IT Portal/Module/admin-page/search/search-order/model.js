Module.Model = function () {
    var me = this;
    var rsID = Utils.getURLValue("configID");
    var urlConPrefix = "c-";

    me.requestSearch = ko.observable();
    me.columns = ko.observableArray();
    me.condition = ko.observable({_start: null, _end: null});
    me.results = ko.observableArray();

    function updateCondition() {
        var con = me.condition();
        me.condition({});
        me.condition(con);

        Utils.setURLValues(con);
    }
    function setDayRecent (dayNumber) {
        var end = new Date();
        end.setHours(0);
        end.setMinutes(0);
        end.setSeconds(0);
        end.setMilliseconds(0);

        var start = new Date(end);
        start.setDate(start.getDate() - dayNumber);

        end.setDate(end.getDate() + 1);
        end.setMilliseconds(-1);

        var con = me.condition();
        con._start = start;
        con._end = end;
        updateCondition();
    };
    function dateTimeToPhpFormat(time) {
        return time.getFullYear().toString()
            + '-' + (time.getMonth() + 1).toString().padStart(2, '0')
            + '-' +  time.getDate().toString().padStart(2, '0')
            + ' '
            + time.getHours().toString().padStart(2, '0')
            +  ':' + time.getMinutes().toString().padStart(2, '0')
            +  ':' + time.getSeconds().toString().padStart(2, '0')
    }
    me.time = {
        recent7: function () {
            setDayRecent(7);
        },
        recent30: function () {
            setDayRecent(30);
        },
        unlimit: function () {
            var con = me.condition();
            con._start = null;
            con._end = null;
            me.condition({});
            me.condition(con);
        }
    };
    me.getResult = function () {

        var con = Object.assign({}, me.condition());
        for(var pro in con){
           var val = con[pro];
           if(val.getTime && val.constructor.name === "Date")
               con[pro] = dateTimeToPhpFormat(val);
        }
        APIData.RequestSearch.Search(rsID, con, function (res,json) {

            if(res.code == AMResponseCode.ok){
                me.results(res.data);
            }
        }, null, false);
    };

    // init
    (function () {
        var urlValues = Utils.getURLValues();
        var con = {};
        var needGet = false;
        for(var name in urlValues){
            if(name.startsWith(urlConPrefix)){
                con[name] = urlValues[name];
                needGet = true;
            }
        }
        me.condition(con);
        needGet && me.getResult();
    })();

    APIData.RequestSearch.GetRequestSearchEditData(rsID, function (res) {
       if(res.code == AMResponseCode.ok){
           me.requestSearch(res.data.data);
           me.columns(res.data.columns);
       }else{
           Component.System.alert.show(res);
       }
    });

};


















