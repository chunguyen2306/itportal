Module.View = function (_model, _onDone) {
    var me = this;
    var model = _model;
    Component.Module.AbstractModuleView.call(this, model,_onDone );
    Config.Global.isInitMenuMouseHover = false;
    Config.Global.mouseWheelBaseOn = '[section="document"]';
    Config.Global.mouseWheelLimitHeight = false;

    me.onDone();
};