Module.Model = function () {
    var me = this;
    var DocumentList = ko.observableArray([]);

    var loadDocumentList = function () {
        var category = Libs.Location.path_param[1];
        APIData.Document.GetDocumentList('Chính sách cấp tài sản', function (res) {
            res = JSON.parse(res);
            console.log(res);
        })
    };

    var init = function () {
        loadDocumentList();
    };

    init();
};