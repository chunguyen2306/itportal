Module.Model = function () {
    var me = this;
    this.text = ko.observable('aaaa');
    this.arr = ko.observableArray([
        'test1',
        'test2'
    ]);
    this.obj = ko.observable({
        arr: [
            'test1',
            'test2'
        ]
    });
    this.callParent = function () {
        console.log(me._parent);
    };
    this.callRebinding = function () {
        var newul = $('<ul data-bind="foreach: obj().arr"><li data-bind="text: $data"></li></ul>');
        document.body.appendChild(newul[0]);
        me._parent.reBinding();
    }
};