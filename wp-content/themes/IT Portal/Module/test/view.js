Module.View = function (model, _onDone) {
    var me = this;
    Component.Module.AbstractModuleView.call(this, model,_onDone );

    $('[name="btnAddToCart"]').on('click',function () {
        var parent = $(this).parent();
        var COMPONENTID = parent.attr('COMPONENTID');
        var COMPONENTNAME = parent.find('[name="COMPONENTNAME"]').text();
        var COMPONENTTYPEID = parent.find('[name="COMPONENTTYPEID"]').val();
        var COMPONENTTYPENAME = parent.find('[name="COMPONENTTYPENAME"]').val();

        Component.ShopCart.AddToCart({
            COMPONENTID: COMPONENTID,
            COMPONENTNAME: COMPONENTNAME,
            COMPONENTTYPEID: COMPONENTTYPEID,
            COMPONENTTYPENAME: COMPONENTTYPENAME
        });
    });

    me.onDone();
};