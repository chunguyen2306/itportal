/**
 * Created by tientm2 on 10/17/2018.
 */
const SignalRClient = new (function(){

    let me = this;
    let hub = null;
    let url = '';
    let clientID = 0;
    let actionClass = null;

    this.setting = function(args){
        url = args.url;
        actionClass = args.class;
    };

    this.connect = function(onSuccess){
        $.connection.hub.url = url;
        me.hub = $.connection.ComHub;

        $.connection.hub.start().done(function () {
            clientID = $.connection.hub.id;
            if (onSuccess)
                onSuccess(clientID);
        });

        me.hub.client.broadcastMessage = onReceived;
    };

    this.disconnect = function(){

    };

    this.send = function(mess){
        me.hub.server.send(JSON.stringify(mess));
    };

    let onReceived = function (mess) {
        console.log(`%cMESS RECEIVED %c${mess}`,"color: green","color: red");
        mess = JSON.parse(mess);
        let lazz = mess.Method.split('.')[0];
        let method = mess.Method.split('.')[1];
        actionClass[lazz][method](mess.Message);
    };

    })();