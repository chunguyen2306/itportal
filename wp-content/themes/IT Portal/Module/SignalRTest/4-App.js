/**
 * Created by tientm2 on 10/17/2018.
 */
SignalRAction.App = SignalRAction.App || {

        Chat: function (mess) {
            var data = JSON.parse(mess);

            App.PushChat(data.UserName, data.Mess);
        }

    };