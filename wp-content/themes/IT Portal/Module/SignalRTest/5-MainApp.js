/**
 * Created by tientm2 on 10/17/2018.
 */
var App = new (function(){
    var me = this;

    this.UserName = function () {
        return localStorage.getItem('userName');
    };

    this.RegistryUser = function () {
        let userName = localStorage.getItem('userName');
        if(userName === undefined || userName === '' || userName === null) {
            userName = prompt('Give me your name: ');
            localStorage.setItem('userName', userName);
        }
    };

    this.PushChat = function (user, mess) {
        let userName = localStorage.getItem('userName');
        var chatbox = document.querySelector('.Chatbox');

        var chatitem = document.createElement('div');

        if(user === userName){
            chatitem.className = 'ChatItem Me';
        } else {
            chatitem.className = 'ChatItem';
        }

        var username = document.createElement('span');
        username.clasName = 'UserName';
        username.innerText = user;

        var usermess = document.createElement('span');
        usermess.clasName = 'UserMessage';
        usermess.innerText = mess;

        chatitem.appendChild(username);
        chatitem.appendChild(usermess);

        chatbox.appendChild(chatitem);

    };
})();