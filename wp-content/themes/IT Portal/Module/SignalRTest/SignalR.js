/**
 * Created by tientm2 on 10/17/2018.
 */
SignalRAction.SignalR = SignalRAction.SignalR || {

        Disconnected: function (mess) {
            console.log(mess)
        },
        Connected: function (mess) {
            console.log(`%cUSER CONNECTED %cwith id %c${mess}`,"color: green","color: #000000","color: red");
            App.RegistryUser();
        }

    };

