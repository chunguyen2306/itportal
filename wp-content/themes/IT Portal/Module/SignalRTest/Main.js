/**
 * Created by tientm2 on 10/17/2018.
 */
$(document).ready(function(){
    document.querySelector('#mess').addEventListener('keyup', function (e) {
        if(e.keyCode === 13){
            $('#send').click();
        }
    });

    document.querySelector('#send').addEventListener('click', function (e) {
        SignalRClient.send({
            Destination: [],
            Method: 'App.Chat',
            Message: JSON.stringify({
                UserName: App.UserName(),
                Mess: $('#mess').val()
            }),
            IsBroadcast: true
        });
        $('#mess').val('');
    });

    SignalRClient.setting({
        url: "http://10.199.100.11:9090/testsoc/signalr",
        class: SignalRAction
    });
    SignalRClient.connect();
});
