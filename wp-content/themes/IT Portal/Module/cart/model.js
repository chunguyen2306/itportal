function kolog(data) {
  console.log(data);
  return data;
}

Module.Model = function() {
  var me = this;
  //cpnguyen
  this.listFixItem = ko.observableArray([]);
  this.listLoseItem = ko.observableArray([]);
  this.listReturnItem = ko.observableArray([]);
  this.ProductCart = ko.observableArray([]);
  this.OtherProductCart = ko.observableArray([]);

  this.myAsset = ko.observableArray([]);
  this.isCheckAllPage = ko.observable(false);
  this.retrieveList = ko.observableArray([]);
  this.selectedAsset = ko.observableArray([]);
  this.full_name = ko.observable("");
  this.user_domain = "";
  this.seat = ko.observable("");
  this.noteNewItem = ko.observable("");
  this.noteEditItem = ko.observable("");
  this.step = ko.observableArray(0);
  this.totalAssetLeft = ko.observable(0);
  this.isUser = ko.observable(true);
  this.showTableNewItem = ko.observable(true);
  this.countItem = ko.observable(Component.ShopCart.UpdateCart());
  this.dayOfPurchase = ko.observable(2);
  var currentPage = 1;
  var limitItemSecondStep = 3;
  this.remainEditItem = ko.observable(0);
  var pageCountLeft = 0;
  var step = 1;
  var totalAssetLeft = 0;
  const LIMIT_ASSETS_IN_PAGE = 9;
  //cpnguyen
  const maximumCellPerPage = 5;

  var reportingLine = "";
  var removeStyleActiveTable = function(e) {
    [...e.parentNode.getElementsByTagName("div")].forEach(el => {
      el.classList.remove("active");
    });
  };
  this.showMoreItem = function() {
    console.log(me.getLengthListEditItem());
  };
  this.handleClickTableNewItem = function(model, e) {
    removeStyleActiveTable(e.target);
    e.target.classList.add("active");
    me.showTableNewItem(true);
  };
  this.handleClickTableEditItem = function(model, e) {
    removeStyleActiveTable(e.target);
    e.target.classList.add("active");
    me.showTableNewItem(false);
  };
  var addItem = function(assetItem) {
    Component.AssetsCart.AddToCart(assetItem, me.user_domain);
    revert_asset_selected();
    me.selectedAsset(Component.AssetsCart.GetCart());
    if (me.retrieveList().length == LIMIT_ASSETS_IN_PAGE) {
      me.isCheckAllPage("");
      me.isCheckAllPage(true);
    } else {
      me.isCheckAllPage("");
      me.isCheckAllPage(false);
    }
  };
  var RemoveStyleIcon = function(e) {
    [...e.parentNode.parentNode.getElementsByTagName("i")].forEach(el => {
      el.classList.remove(
        "active-background",
        "btn-red",
        "btn-blue",
        "btn-orange",
        "active-tr-retrieve"
      );
    });
  };
  var RemoveItemInOldLocal = function(model) {
    var temp = [];
    ["listFixItem", "listLoseItem", "listReturnItem"].forEach(function(item) {
      temp = JSON.parse(localStorage.getItem(item));
      var index = temp.findIndex(x => x.RESOURCENAME === model.RESOURCENAME);
      if (index > -1) {
        temp.splice(index, 1);
        localStorage.setItem(item, JSON.stringify(temp));
      }
    });
  };
  var SaveDataToLocalStorage = function(data, name) {
    var temp = [];
    temp = JSON.parse(localStorage.getItem(name));

    //cpnguyen
    if (name == "listFixItem") {
      data.ACTIONTYPE = "Báo hư hỏng";
    } else if (name == "listReturnItem") {
      data.ACTIONTYPE = "Trả";
    } else {
      data.ACTIONTYPE = "Báo mất";
    }

    var found = temp.some(function(el) {
      return el.RESOURCENAME == data.RESOURCENAME;
    });
    if (!found) {
      temp.push(data);
    }
    localStorage.setItem(name, JSON.stringify(temp));
  };
  var selectEditItem = function(model, e, localName, color) {
    let td = e.target.parentNode.parentNode.parentNode;
    if (!e.target.classList.contains("active-tr-retrieve")) {
      e.target.parentNode.parentNode.parentNode.childNodes[1].childNodes[1].checked = true;
      td.classList.add("active-background");
      RemoveStyleIcon(e.target);
      RemoveItemInOldLocal(model);
      e.target.classList.add("active-tr-retrieve", color);
      SaveDataToLocalStorage(model, localName);
    } else {
      e.target.parentNode.parentNode.parentNode.childNodes[1].childNodes[1].checked = false;
      e.target.classList.remove(
        "btn-red",
        "btn-blue",
        "btn-orange",
        "active-tr-retrieve"
      );
      td.classList.remove("active-background");
      RemoveItemInOldLocal(model);
    }
  };
  this.selectReturnItem = function(model, e) {
    selectEditItem(model, e, "listReturnItem", "btn-orange");
  };
  this.selectMissItem = function(model, e) {
    selectEditItem(model, e, "listLoseItem", "btn-red");
  };
  this.selectFixItem = function(model, e) {
    selectEditItem(model, e, "listFixItem", "btn-blue");
  };
  this.AddItemActionList = function() {
    Component.Custom.Popup.items["popReturnAsset"].Popup.hide();
    me.listLoseItem(JSON.parse(Libs.LocalStorage.val("listLoseItem")));
    me.listReturnItem(JSON.parse(Libs.LocalStorage.val("listReturnItem")));
    me.listFixItem(JSON.parse(Libs.LocalStorage.val("listFixItem")));
  };
  this.changeQuantity = function(data) {
    var newQuantity = parseInt(data.QUANTITY);
    Component.ShopCart.ChangeQuantityOfModel(data.COMPONENTID, newQuantity);
    me.ProductCart(Component.ShopCart.GetCart());
  };
  this.changeQuantityCartOrder = function(data) {
    var newQuantity = parseInt(data.Quantity);
    Component.ShopCart.ChangeQuantityOfModel(data.ID, newQuantity, "cartother");
    me.OtherProductCart(Component.ShopCart.GetCart("cartother"));
  };
  this.check_SelectAllInPage = function(model, e) {
    var isChecked = e.currentTarget.checked;
    if (isChecked) {
      me.do_SelectAllRetriveAssetInPage();
    } else {
      me.do_RemoveAllRetriveAssetInPage();
    }
    revert_asset_selected();
  };
  var _createOrder = function() {
    Component.System.loading.show();
    var requestList = [];
    if (me.ProductCart().length > 0 || me.OtherProductCart().length > 0) {
      requestList.push({
        actionType: $("#is_new_staff_cbx").is(":checked")
          ? "ShopCart_NewStaff"
          : "ShopCart",
        data: getRequestAssetData()
      });
    }
    if (me.listReturnItem().length > 0) {
      requestList.push({
        actionType: "ReturnAsset",
        data: getRequestReturnAssetData()
      });
    }
    if (me.listFixItem().length > 0) {
      requestList.push({
        actionType: "BrokenAsset",
        data: getRequestFixAssetData()
      });
    }
    if (me.listLoseItem().length > 0) {
      requestList.push({
        actionType: "LostAsset",
        data: getRequestLoseAssetData()
      });
    }
    Libs.Workflow.createWithMappingData(requestList, function(res) {
      var messFail =
        "<b>" + i18n("cart:message:create-order:fail") + "</b><br>";
      var messSuccess =
        "<b>" + i18n("cart:message:create-order:success") + "</b><br>";
      var isSuccess = false;
      var isFail = false;
      for (var i = 0; i < res.length; i++) {
        if (res[i].result) {
          isSuccess = true;
          messSuccess +=
            "<b>" +
            i18n("cart:message:create-order:success:detail", [
              res[i].response.workflow.DisplayName,
              res[i].response.request.ID
            ]) +
            "</b><br>";
        } else {
          isFail = true;
        }
      }
      messFail =
        messFail + i18n("cart:message:create-order:fail:contact-bonus");
      Component.System.loading.hide();
      if (Component.Custom.Popup.HasActive) {
        Component.Custom.Popup.Active.hide();
      }
      var mess = "";
      if (isSuccess) {
        mess = messSuccess;
        Component.System.alert.show(
          mess,
          {
            name: "Xem đơn hàng",
            cb: function() {
              window.location =
                "/user-page/order/detail/?ID=" + res[0].response.request.ID;
            }
          },
          {
            name: i18n("all-page:close:btn:text"),
            cb: function() {
              location.reload();
            }
          }
        );
        //cpnguyen
        if (mess.includes("Trả")) {
          localStorage.setItem("listReturnItem", JSON.stringify([]));
          me.listReturnItem([]);
        } else if (mess.includes("Sửa")) {
          localStorage.setItem("listFixItem", JSON.stringify([]));
          me.listFixItem([]);
        } else if (mess.includes("mất")) {
          localStorage.setItem("listLoseItem", JSON.stringify([]));
          me.listLoseItem([]);
        }

        me.reset_asset();
      }
      if (isFail) {
        mess = mess + messFail;
        Component.System.alert.show(mess, {
          name: i18n("all-page:close:btn:text"),
          cb: function() {}
        });
      }
    });
  };
  this.do_AddRetriveAsset = function() {
    loadAsset();
    $("#quantity_product").text(
      Component.AssetsCart.GetQuantityAssetSelected()
    );
    Component.Custom.Popup.items["popReturnAsset"].Popup.hide();
  };
  var getEditNote = function() {
    var note = $("#note_edit_text_area").val();
    console.log(note);
    return note;
  };
  var validateStep = function() {
    if (
      step === 2 &&
      !me.checkItemInCart() &&
      !me.checkEditItem() &&
      $("#note_edit_text_area").val() == undefined
    ) {
      Component.System.loading.hide();
      Component.System.alert.show(i18n("cart:message:not-item-in-cart"), {
        name: i18n("all-page:close:btn:text"),
        cb: function() {}
      });
      return false;
    }

    //Neu khong co san pham moi trong gio hang thi khong qua buoc tiep theo
    if (me.checkItemInCart() && $("#note_new_text_area").val() == "") {
      Component.System.loading.hide();
      Component.System.alert.show(i18n("cart:message:not-enough-information"), {
        name: i18n("all-page:close:btn:text"),
        cb: function() {}
      });
      step = 1;
      return false;
    }
    //Neu khong co san pham edit trong gio hang thi khong qua buoc tiep theo
    if (
      me.checkEditItem() &&
      $("#note_edit_text_area").val() == "" &&
      step !== 0
    ) {
      Component.System.loading.hide();
      Component.System.alert.show(i18n("cart:message:not-enough-information"), {
        name: i18n("all-page:close:btn:text"),
        cb: function() {}
      });
      return false;
    }
    return true;
  };
  this.changeNextStep = function() {
    if (validateStep()) {
      if (step === 0) {
        //reInit localPaging
        Component.Custom.LocalPaging.init();
        Component.Custom.LocalPaging['localPaging'].currentPageIndex(2);
      }
      if (step === 1) {
        me.generatePageViewData(1, maximumCellPerPage);
      }
      if (step === 2) {
        me.noteEditItem($("#note_edit_text_area").val());
        me.noteNewItem($("#note_new_text_area").val());
      }
      if (step == 3) {
        me.do_CreateOrder();
        return;
      }
      me.step(step++);
    }
  };
  this.ChangeToFirstPage = function() {
    step = 0;
    me.changeNextStep();
  };
  this.ChangeToSecondPage = function() {
    step = 1;
    me.remainEditItem(me.getLengthListEditItem() - limitItemSecondStep);
    me.changeNextStep();
  };
  this.ChangeToThirdPage = function() {
    step = 2;
    me.countItem(Component.ShopCart.UpdateCart());
    me.changeNextStep();
    me.dayOfPurchase(getMaxDayOfPurchase(Component.ShopCart.GetCart()));
  };
  var getMaxDayOfPurchase = function(cart) {
    return cart.reduce((cur, item, index) => {
      return cur > item.DAYSOFPURCHASE ? cur : item.DAYSOFPURCHASE;
    }, 0);
  };
  this.do_CreateOrder = function() {
    // if (
    //   $("#note_new_text_area").val() == "" ||
    //   $("#p_seat").val() == "" ||
    //   $("#search_user_text_box").val() == ""
    // ) {
    //   Component.System.loading.hide();
    //   Component.System.alert.show(i18n("cart:message:not-enough-information"), {
    //     name: i18n("all-page:close:btn:text"),
    //     cb: function() {}
    //   });
    //   return;
    // }
    if (me.user_domain == null) {
      Component.System.loading.hide();
      Component.System.alert.show(i18n("cart:message:wrong-domain"), {
        name: i18n("all-page:close:btn:text"),
        cb: function() {}
      });
      return;
    }
    if (me.OtherProductCart().length !== 0 || me.ProductCart().length !== 0) {
      // Check STOCK
      var message =
        "<b>" + i18n("cart:message:need-to-buy:start-message") + "</b>";
      var exception = false;
      for (var i = 0; i < me.ProductCart().length; i++) {
        var product = me.ProductCart()[i];
        if (product.STATUS == "NotEnough") {
          var short = parseInt(product.QUANTITY) - parseInt(product.STOCK);
          message +=
            "<br>" +
            i18n("cart:message:need-to-buy:not-enough", [
              product.COMPONENTNAME,
              short
            ]) +
            "<br>" +
            "<b>" +
            i18n("cart:message:need-to-buy:not-enough:waiting", [
              product.DAYSOFPURCHASE
            ]) +
            "</b>";
          exception = true;
        }
      }
      if (exception) {
        Component.System.alert.show(
          message,
          {
            name: i18n("cart:button:submit-order:btn:text"),
            cb: function() {
              _createOrder();
            }
          },
          {
            name: i18n("cart:button:select-other-product:btn:text"),
            cb: function() {}
          }
        );
      } else {
        Component.System.alert.show(
          i18n("cart:message:submit-order"),
          {
            name: i18n("cart:button:submit-order:btn:text"),
            cb: function() {
              _createOrder();
            }
          },
          {
            name: i18n("cart:button:back-to-cart:btn:text"),
            cb: function() {}
          }
        );
      }
    } else if (
      me.listFixItem().length !== 0 ||
      me.listReturnItem().length !== 0 ||
      me.listLoseItem().length !== 0
    ) {
      Component.System.alert.show(
        i18n("cart:message:submit-only-return-asset"),
        {
          name: i18n("all-page:yes:btn:text"),
          cb: function() {
            _createOrder();
          }
        },
        {
          name: i18n("all-page:no:btn:text"),
          cb: function() {}
        }
      );
    } else {
      Component.System.loading.hide();
      Component.System.alert.show(i18n("cart:message:not-select-any-asset"), {
        name: i18n("all-page:close:btn:text"),
        cb: function() {}
      });
    }
  };
  this.do_InitComponent = function(model) {
    Component.Custom.Number.initEvent(model);
  };
  this.do_GoBack = function() {
    if (history.length > 1) {
      history.back();
    } else {
      location = "/";
    }
  };

  this.do_ReturnAsset = function() {
    loadAsset();

    Component.Custom.Popup.items["popReturnAsset"].Popup.show(function() {
      Component.Custom.Paging.items[
        "assetPaging"
      ].on_PageChange = do_PageChange;
    });
  };
  this.handeleChangePage = function() {
    Component.Custom.Paging.items["assetPaging"].on_PageChange = do_PageChange;
  };
  this.do_RemoveAllRetriveAsset = function() {
    me.retrieveList([]);
    me.selectedAsset([]);
    Component.AssetsCart.ResetCart();
    $("#product_checked_all_id").prop("checked", false);
  };
  this.do_RemoveAllRetriveAssetInPage = function() {
    me.isCheckAllPage(false);
    me.retrieveList([]);
    for (var i = 0; i < me.myAsset().length; i++) {
      if (isSelected(me.myAsset()[i].RESOURCENAME)) {
        removeItem(me.myAsset()[i].RESOURCENAME);
      }
    }
    for (var i = 0; i < me.listFixItem().length; i++) {
      if (isSelected(me.listFixItem()[i].RESOURCENAME)) {
        removeItem(me.listFixItem()[i].RESOURCENAME);
      }
    }
    for (var i = 0; i < me.listReturnItem().length; i++) {
      if (isSelected(me.listReturnItem()[i].RESOURCENAME)) {
        removeItem(me.listReturnItem()[i].RESOURCENAME);
      }
    }
    for (var i = 0; i < me.listLoseItem().length; i++) {
      if (isSelected(me.listLoseItem()[i].RESOURCENAME)) {
        removeItem(me.listLoseItem()[i].RESOURCENAME);
      }
    }
  };
  this.do_RemoveFormCart = function(model, e) {
    Component.ShopCart.RemoveFormCart(model.COMPONENTID);
    me.emptyPageHandler();
  };
  this.do_RemoveFormCartOrder = function(model, e) {
    Component.ShopCart.RemoveFormCart(model.ID, "cartother");
    me.emptyPageHandler();
  };
  this.do_SelectAll = function() {
    me.do_RemoveAllRetriveAsset();
    $("#product_checked_all_id").prop("checked", true);
    APIData.Resources.GetUserAsset(me.user_domain, "", "", 0, 0, function(res) {
      res = JSON.parse(res);
      if (res.State) {
        res = res.Result;
        for (var i = 0; i < res.Assets.length; i++) {
          var data = res.Assets[i];
          var productItem = {
            AssetModel: data.COMPONENTNAME,
            AssetModelID: data.COMPONENTID,
            AssetType: data.COMPONENTTYPENAME,
            AssetTypeID: null,
            AssetName: data.RESOURCENAME
          };
          me.selectedAsset.push(productItem);
          Component.AssetsCart.AddToCart(productItem, me.user_domain);
        }
      }
    });
    me.retrieveList([]);
    for (var i = 0; i < LIMIT_ASSETS_IN_PAGE; i++) {
      var index = i.toString();
      me.retrieveList.push(index);
    }
  };
  this.do_SelectAllRetriveAsset = function() {
    //me.do_RemoveAllRetriveAsset();
    $("#product_checked_all_id").prop("checked", true);
    for (var i = 0; i < me.myAsset().length; i++) {
      var data = me.myAsset()[i];
      var productItem = {
        AssetModel: data.COMPONENTNAME,
        AssetModelID: data.COMPONENTID,
        AssetType: data.COMPONENTTYPENAME,
        AssetTypeID: null,
        AssetName: data.RESOURCENAME
      };
      me.selectedAsset.push(productItem);
      Component.AssetsCart.AddToCart(productItem, me.user_domain);
    }
    me.retrieveList([]);
    for (var i = 0; i < LIMIT_ASSETS_IN_PAGE; i++) {
      var index = i.toString();
      me.retrieveList.push(index);
    }
  };
  var enable_returnAsset_bt = function() {
    $("#returnAsset_bt").attr("disabled", false);
    $("#returnAsset_bt").attr("style", "");
    $("#returnAsset_bt").attr("title", "");
  };
  var disable_returnAsset_bt = function() {
    $("#returnAsset_bt").prop("disabled", true);
    $("#returnAsset_bt").attr(
      "style",
      "cursor: not-allowed !important; background-color: #ddd;"
    );
    $("#returnAsset_bt").attr(
      "title",
      i18n("cart:right-side:return-asset:no-asset:lbl:title")
    );
  };
  var disable_input_user_domain = function() {
    $("#search_user_text_box").prop("disabled", true);
    $("#search_user_text_box").attr("style", "background-color: #ddd;");
    $(".addon-control").attr("style", "background-color: #ddd;");
    $("#search_user_bt").prop("disabled", true);
    $("#search_user_bt").attr("style", "cursor: not-allowed !important;");
    $(".addon-control").attr(
      "title",
      i18n("cart:right-side:input:user-domain:no-permission:lbl:title")
    );
  };
  var isSelected = function(AssetName) {
    for (var i = 0; i < me.selectedAsset().length; i++) {
      if (me.selectedAsset()[i].AssetName == AssetName) {
        return true;
      }
    }
    return false;
  };
  this.do_SelectAllRetriveAssetInPage = function() {
    me.isCheckAllPage(true);
    for (var i = 0; i < me.myAsset().length; i++) {
      var data = me.myAsset()[i];
      var productItem = {
        AssetModel: data.COMPONENTNAME,
        AssetModelID: data.COMPONENTID,
        AssetType: data.COMPONENTTYPENAME,
        AssetTypeID: null,
        AssetName: data.RESOURCENAME
      };
      if (!isSelected(productItem.AssetName)) {
        me.selectedAsset.push(productItem);
        Component.AssetsCart.AddToCart(productItem, me.user_domain);
      }
    }
    me.retrieveList([]);
    for (var i = 0; i < LIMIT_ASSETS_IN_PAGE; i++) {
      var index = i.toString();
      me.retrieveList.push(index);
    }
  };
  this.getProductThumbnail = function(model) {
    return (
      "/resource-content/Asset/APIAssetModelDetail/GetThubmails/?ModelID=" +
      model.COMPONENTID
    );
  };
  this.getOtherProductThumbnail = function() {
    return "/wp-content/uploads/component/buy-1.png";
  };
  var getReportingLine = function(myDomain, callback) {
    APIData.Request.GetReportingLine(myDomain, function(res) {
      if (res.code == AMResponseCode.ok) {
        if (res.data.length > 0) {
          callback(res.data[0].Domain);
        } else {
          callback("");
        }
      } else {
        callback("");
      }
    });
  };
  var getNote = function() {
    var note = $("#note_new_text_area").val();
    var address = $("#p_seat").val();
    note =
      i18n("cart:message:note:address", [address]) +
      "\n" +
      i18n("cart:message:note:other-note", [note]) +
      "\n";
    if (
      (me.ProductCart().length > 0 || me.OtherProductCart().length > 0) &&
      me.selectedAsset().length
    ) {
      note = i18n("cart:message:note:change_asset_order") + note;
    }
    return note;
  };
  // var getRequestOtherAssetData = function() {
  //   var requestData = {
  //     CurrentUser: me.user_domain,
  //     Note: getNote(),
  //     ReportingLine: reportingLine,
  //     AssetList: []
  //   };
  //   for (
  //     var itemIndex = 0;
  //     itemIndex < me.OtherProductCart().length;
  //     itemIndex++
  //   ) {
  //     var cartOtherItem = me.OtherProductCart()[itemIndex];
  //     var price = cartOtherItem.ReferPrice;
  //     price = price.toString().replace(",", "");
  //     for (var i = 0; i < cartOtherItem.Quantity; i++) {
  //       var productItem = {
  //         AssetModel: cartOtherItem.ModelName,
  //         AssetType: cartOtherItem.ModelType,
  //         Note: cartOtherItem.Note,
  //         Link: cartOtherItem.LinkDescription,
  //         Price: price
  //       };
  //       requestData.AssetList.push(productItem);
  //     }
  //   }
  //   return requestData;
  // };
  var getRequestAssetData = function() {
    var sr = SignalREngine.getDefault();
    var requestData = {
      CurrentUser: me.user_domain,
      Note: getNote(),
      ReportingLine: reportingLine,
      AssetList: []
    };
    for (var itemIndex = 0; itemIndex < me.ProductCart().length; itemIndex++) {
      var cartItem = me.ProductCart()[itemIndex];
      for (var i = 0; i < cartItem.QUANTITY; i++) {
        var productItem = {
          AssetModel: cartItem.COMPONENTNAME,
          AssetType: cartItem.COMPONENTTYPENAME,
          Note: "",
          Link: "",
          Price: ""
        };
        requestData.AssetList.push(productItem);
      }
    }
    for (
      var itemIndex = 0;
      itemIndex < me.OtherProductCart().length;
      itemIndex++
    ) {
      var cartOtherItem = me.OtherProductCart()[itemIndex];
      var price = cartOtherItem.ReferPrice; //price = price.toString().replace(',', '');
      for (var i = 0; i < cartOtherItem.Quantity; i++) {
        var productItem = {
          AssetModel: cartOtherItem.ModelName,
          AssetType: cartOtherItem.ModelType,
          Note: cartOtherItem.Note,
          Link: cartOtherItem.LinkDescription,
          Price: price
        };
        requestData.AssetList.push(productItem);
      }
    }
    return requestData;
  };
  var getRequestReturnAssetData = function() {
    var requestData = {
      CurrentUser: me.user_domain,
      Note: me.noteEditItem(),
      ReportingLine: reportingLine,
      AssetList: []
    };

    for (var i = 0; i < me.listReturnItem().length; i++) {
      var cartItem = me.listReturnItem()[i];
      var productItem = {
        AssetName: cartItem.RESOURCENAME,
        AssetTypeID: cartItem.COMPONENTTYPEID,
        AssetType: cartItem.COMPONENTTYPENAME,
        AssetModelID: cartItem.COMPONENTID,
        AssetModel: cartItem.COMPONENTNAME
      };
      requestData.AssetList.push(productItem);
    }
    return requestData;
  };
  var getRequestFixAssetData = function() {
    var requestData = {
      CurrentUser: me.user_domain,
      Note: me.noteEditItem(),
      ReportingLine: reportingLine,
      AssetList: []
    };

    for (var i = 0; i < me.listFixItem().length; i++) {
      var cartItem = me.listFixItem()[i];
      var productItem = {
        AssetName: cartItem.RESOURCENAME,
        AssetType: cartItem.COMPONENTTYPENAME,
        AssetModel: cartItem.COMPONENTNAME
      };
      requestData.AssetList.push(productItem);
    }
    return requestData;
  };
  var getRequestLoseAssetData = function() {
    var requestData = {
      CurrentUser: me.user_domain,
      Note: me.noteEditItem(),
      ReportingLine: reportingLine,
      AssetList: []
    };

    for (var i = 0; i < me.listLoseItem().length; i++) {
      var cartItem = me.listLoseItem()[i];
      var productItem = {
        AssetName: cartItem.RESOURCENAME,
        AssetType: cartItem.COMPONENTTYPENAME,
        AssetModel: cartItem.COMPONENTNAME
      };
      requestData.AssetList.push(productItem);
    }
    return requestData;
  };
  this.selectItem = function(data, e) {
    var checked = e.currentTarget.checked;
    if (checked) {
      var assetItem = {
        AssetModel: data.COMPONENTNAME,
        AssetModelID: data.COMPONENTID,
        AssetType: data.COMPONENTTYPENAME,
        AssetTypeID: data.COMPONENTTYPEID,
        AssetName: data.RESOURCENAME
      };
      addItem(assetItem);
    } else {
      removeItem(data.RESOURCENAME);
    }
  };
  this.returnAllItemSelected = function() {
    handleAllEditItem("listReturnItem");
  };
  this.fixAllItemSelected = function() {
    handleAllEditItem("listFixItem");
  };
  this.loseAllItemSelected = function() {
    handleAllEditItem("listLoseItem");
  };
  var handleAllEditItem = function(localName) {
    let list = Component.AssetsCart.GetCart();

    list.forEach(item => {
      if (!Component.ShopCart.IsExisted(item.AssetName, localName)) {
        var assetItem = {
          COMPONENTNAME: item.AssetModel,
          COMPONENTID: item.AssetModelID,
          COMPONENTTYPENAME: item.AssetType,
          COMPONENTTYPEID: item.AssetTypeID,
          RESOURCENAME: item.AssetName
        };
        RemoveItemInOldLocal(assetItem);
        SaveDataToLocalStorage(assetItem, localName);
      }
    });
    revert_asset_selected();
  };
  var removeItem = function(assetID) {
    Component.AssetsCart.RemoveFormCart(assetID, me.user_domain);
    Component.ShopCart.RemoveItemListEdit(assetID, "listFixItem");
    Component.ShopCart.RemoveItemListEdit(assetID, "listLoseItem");
    Component.ShopCart.RemoveItemListEdit(assetID, "listReturnItem");
    revert_asset_selected();
    if (me.retrieveList().length == LIMIT_ASSETS_IN_PAGE) {
      me.isCheckAllPage("");
      me.isCheckAllPage(true);
    } else if (me.retrieveList().length < LIMIT_ASSETS_IN_PAGE) {
      me.isCheckAllPage("");
      me.isCheckAllPage(false);
    }
  };
  this.checkRemoveItem = function(data) {
    removeItem(data.AssetName);
  };
  this.txb_SearchAsset_OnKeyUp = function(e) {
    var keyword = $("#txb_SearchAsset").val();
    if (e.key === "Enter") {
      currentPage = 1;
      loadAsset(keyword);
    }
  };
  this.search_asset_btn_onClick = function(e) {
    var keyword = $("#txb_SearchAsset").val();
    currentPage = 1;
    loadAsset(keyword);
  };
  var setFullNameAndSeat = function(user_domain) {
    me.full_name("");
    me.seat("");
    if (user_domain == "") {
      me.full_name(i18n("cart:right-side:input:wrong-domain:input:text"));
      me.user_domain = null;
      me.seat(i18n("cart:right-side:input:wrong-address:input:text"));

      return;
    }
    APIData.CMDB.GetUserInformation(user_domain, 0, 1, function(res) {
      if (res != "") {
        me.full_name(res.FullName);
        me.user_domain = res.AccountName;
      } else {
        me.full_name(i18n("cart:right-side:input:wrong-domain:input:text"));
        me.user_domain = null;
      }
      APIData.User.GetUserByDomain(me.user_domain, function(res) {
        if (res != null) {
          res = JSON.parse(res);
          if (res != null) {
            if (res.seat != null && res.seat != undefined && res.seat != "") {
              me.seat(res.seat);
            } else {
              me.seat(i18n("cart:right-side:input:wrong-address:input:text"));
            }
          } else {
            me.seat(i18n("cart:right-side:input:wrong-address:input:text"));
          }
        } else {
          me.seat(i18n("cart:right-side:input:wrong-address:input:text"));
        }
      });
    });
  };
  var change_user = function(new_user_domain, do_remove) {
    if (do_remove === null || do_remove === undefined) {
      do_remove = true;
    }
    me.user_domain = new_user_domain;
    setFullNameAndSeat(new_user_domain);
    loadAsset();
    if (do_remove) {
      me.do_RemoveAllRetriveAsset();
    }
    getReportingLine(me.user_domain, function(res) {
      reportingLine = res;
    });
  };
  this.search_user = function() {
    var key_word = $("#search_user_text_box").val();
    $("#p_seat").focus();
    if (me.checkEditItem()) {
      Component.System.alert.show(
        i18n("cart:message:confirm-change-user") +
          "<br>" +
          i18n("cart:message:consequence-change-user"),
        {
          name: i18n("all-page:yes:btn:text"),
          cb: function() {
            Component.AssetsCart.ResetEditCart();
            me.listFixItem([]);
            me.listLoseItem([]);
            me.listReturnItem([]);
            change_user(key_word);
          }
        },
        {
          name: i18n("all-page:no:btn:text"),
          cb: function() {
            change_user(me.user_domain, false);
          }
        }
      );
    } else {
      change_user(key_word);
    }
  };

  this.view_detail_product = function(data) {
    var url = "/detail/" + data.COMPONENTID;
    var win = window.open(url, "_blank");
    win.focus();
  };
  this.view_detail_order_product = function(data) {
    var url = data.LinkDescription;
    var win = window.open("//" + url, "_blank");
    win.focus();
  };
  var do_PageChange = function(paging) {
    currentPage = paging.currentPage();
    loadAsset();
    $("html").animate(
      {
        scrollTop: 0
      },
      500
    );
    var home = $('[section="home"]');
    home.removeClass("toggle");
  };
  var revert_asset_selected = function() {
    /*
        if (me.user_domain != Component.AssetsCart.GetUserName()) {
            Component.AssetsCart.SetUserName(me.user_domain);
            Component.AssetsCart.RemoveAll();
        }
        */

    var listAsset = Component.AssetsCart.GetCart();
    var fixItem = JSON.parse(localStorage.getItem("listFixItem"));
    var loseItem = JSON.parse(localStorage.getItem("listLoseItem"));
    var returnItem = JSON.parse(localStorage.getItem("listReturnItem"));

    me.retrieveList([]);
    me.selectedAsset(listAsset);
    var flag = false;
    for (var i = 0; i < me.myAsset().length; i++) {
      flag = false;
      if (fixItem.length > 0 && !flag) {
        for (var j = 0; j < fixItem.length; j++) {
          if (me.myAsset()[i].RESOURCENAME == fixItem[j].RESOURCENAME) {
            me.retrieveList.push({
              type: "fix",
              id: i.toString()
            });
            flag = true;
            continue;
          }
        }
      }

      if (loseItem.length > 0 && !flag) {
        for (var j = 0; j < loseItem.length; j++) {
          if (me.myAsset()[i].RESOURCENAME == loseItem[j].RESOURCENAME) {
            me.retrieveList.push({
              type: "lose",
              id: i.toString()
            });
            flag = true;
            continue;
          }
        }
      }
      if (returnItem.length > 0 && !flag) {
        for (var j = 0; j < returnItem.length; j++) {
          if (me.myAsset()[i].RESOURCENAME == returnItem[j].RESOURCENAME) {
            me.retrieveList.push({
              type: "return",
              id: i.toString()
            });
            flag = true;
            continue;
          }
        }
      }
      if (!flag) {
        for (var j = 0; j < listAsset.length; j++) {
          if (me.myAsset()[i].RESOURCENAME == listAsset[j].AssetName) {
            me.retrieveList.push({
              type: "none",
              id: i.toString()
            });
            flag = true;
            continue;
          }
        }
      }
    }

    if (me.retrieveList().length == LIMIT_ASSETS_IN_PAGE) {
      me.isCheckAllPage("");
      me.isCheckAllPage(true);
    } else {
      me.isCheckAllPage("");
      me.isCheckAllPage(false);
    }
  };

  this.revertItemRetrieList = function(asset) {
    return me.retrieveList().some(item => {
      return item.id === asset.toString();
    });
  };
  this.revertTypeRetrieList = function(indexItem, typeItem) {
    return me.retrieveList().some(item => {
      return item.id === indexItem.toString() && item.type === typeItem;
    });
  };
  this.refresh_load_asset = function() {
    currentPage = 1;
    loadAsset();
  };
  this.reset_asset = function() {
    Component.AssetsCart.ResetCart();
    me.retrieveList([]);
    me.selectedAsset([]);
    loadAsset();
    $("#quantity_product").text("0");
  };
  this.loadAssetAfter = function() {
    loadAsset();
  };
  var loadAsset = function(keyword) {
    me.retrieveList([]);
    if (keyword === undefined || keyword === null) {
      keyword = "";
    }
    APIData.Resources.GetUserAsset(
      me.user_domain,
      "",
      keyword,
      currentPage - 1,
      LIMIT_ASSETS_IN_PAGE,
      function(res) {
        res = JSON.parse(res);
        if (res.code == AMResponseCode.denied) {
          Component.System.alert.show(
            i18n("cart:message:not-allow-seeing-those-assets"),
            {
              name: i18n("all-page:ok:btn:text"),
              cb: function() {
                change_user(me._parent.currentLoginUserDomain);
              }
            }
          );
        } else if (res.State) {
          //cpnguyen
          localStorage.getItem("listFixItem");
          localStorage.getItem("listReturnItem");
          localStorage.getItem("listLoseItem");

          res = res.Result; // Load Asset vào Table
          me.myAsset(res.Assets);
          if (res.Total == 0) {
            disable_returnAsset_bt();
          } else {
            enable_returnAsset_bt();
          } // Revert Asset đã chọn vào Table
          revert_asset_selected(); // Phân trang
          pageCountLeft = res.PageCount;
          if (keyword == "") {
            me.totalAssetLeft(res.Total);
          }
          totalAssetLeft = res.Total;
          Component.Custom.Paging.items["assetPaging"].render(
            pageCountLeft,
            LIMIT_ASSETS_IN_PAGE,
            totalAssetLeft,
            currentPage
          );
        } else {
          disable_returnAsset_bt();
        }
      }
    );
  };
  this.checkItemInCart = function() {
    return (
      Component.ShopCart.GetCart().length > 0 ||
      Component.ShopCart.GetCart("cartother").length > 0
    );
  };
  this.checkEditItem = function() {
    return (
      me.listFixItem().length +
        me.listLoseItem().length +
        me.listReturnItem().length !==
      0
    );
  };
  this.getLengthListEditItem = function() {
    return (
      me.listFixItem().length +
      me.listLoseItem().length +
      me.listReturnItem().length
    );
  };
  //cpnguyen
  this.do_RemoveFromlistFixItem = function(model, e) {
    Component.ShopCart.RemoveFormCart(model.ID, "listFixItem");
    me.emptyPageHandler();
  };

  this.do_RemoveFromlistLoseItem = function(model, e) {
    Component.ShopCart.RemoveFormCart(model.ID, "listLoseItem");
    me.emptyPageHandler();
  };

  this.do_RemoveFromlistReturnItem = function(model, e) {
    Component.ShopCart.RemoveFormCart(model.ID, "listReturnItem");
    me.emptyPageHandler();
  };

  this.emptyPageHandler = function() {
    let totalItemsInPage =
      me.listFixItem().length +
      me.listLoseItem().length +
      me.listReturnItem().length +
      me.ProductCart().length +
      me.OtherProductCart().length;
    let currentPage = parseInt(Component.Custom.LocalPaging.items['localPaging'].currentPage());
    me.calculatePageCount(currentPage);
    //if the page is empty
    if (totalItemsInPage == 1) {
      Component.Custom.LocalPaging.items[
        "localPaging"
      ].changeActivePageToPrevious();
      me.generatePageViewData(currentPage - 1, maximumCellPerPage);
      // Component.Custom.LocalPaging.items['localPaging'].render(currentPage, currentPageCount - 1, maximumCellPerPage);
    } else {
      me.generatePageViewData(currentPage, maximumCellPerPage);
    }
  };

  var initLocalListItemAction = function() {
    if (!localStorage.getItem("listFixItem")) {
      localStorage.setItem("listFixItem", JSON.stringify([]));
    }
    if (!localStorage.getItem("listLoseItem")) {
      localStorage.setItem("listLoseItem", JSON.stringify([]));
    }
    if (!localStorage.getItem("listReturnItem")) {
      localStorage.setItem("listReturnItem", JSON.stringify([]));
    }

    me.listFixItem(JSON.parse(localStorage.getItem("listFixItem")));
    me.listReturnItem(JSON.parse(localStorage.getItem("listReturnItem")));
    me.listLoseItem(JSON.parse(localStorage.getItem("listLoseItem")));
  };
  //cpnguyen
  this.txbSearchItems_OnKeyUp = function(e) {
    if (e.key === "Enter") {
      me.searchItemsHandler();
    }
  }

  this.searchItems_BtnPressed = function(model, e) {
    me.searchItemsHandler();
  }

  this.searchItemsHandler = function() {
    me.generatePageViewData(1, maximumCellPerPage);
    me.calculatePageCount(1);
  }

  //cpnguyen
  this.localPagingChangeHandler = function(model, e) {
    console.log(e)
    let currentPageIndex = $(e.currentTarget)
      .find("span.active")
      .html();
    me.generatePageViewData(currentPageIndex, maximumCellPerPage);
  };

  this.getCurrentItemsInLocalStorage = function() {
    //keyword filter layer
    let keyword = $("#txbSearchItems").val().trim().toLowerCase();

    //joins all items
    //this implement is bad , should be fixed later
    const localStorageFields = [
      "carts",
      "cartother",
      "listFixItem",
      "listLoseItem",
      "listReturnItem"
    ];
    let itemsData = [];
    localStorageFields.forEach(function(fieldName) {
      let fieldArray = JSON.parse(localStorage.getItem(fieldName));

      if(keyword.trim() != '') {
        let filteredArray = [];
        fieldArray.forEach(function(fieldItem) {
          if(fieldName == 'carts' || fieldName == 'cartother') {
            if(fieldItem.COMPONENTNAME.toLowerCase().includes(keyword) || fieldItem.COMPONENTTYPENAME.toLowerCase().includes(keyword)) {
              filteredArray.push(fieldItem);
            }
          } else {
            if(fieldItem.ACTIONTYPE.toLowerCase().includes(keyword) || fieldItem.COMPONENTNAME.toLowerCase().includes(keyword) || 
              fieldItem.COMPONENTTYPENAME.toLowerCase().includes(keyword) || fieldItem.RESOURCENAME.toLowerCase().includes(keyword)) {
              filteredArray.push(fieldItem);
            }
          }
        });
        itemsData = itemsData.concat(filteredArray);
      } else {
        itemsData = itemsData.concat(fieldArray);
      }
    });

    return itemsData;
  };

  this.generatePageViewData = function(pageIndex, maximumCellPerPage) {
    let itemsData = me.getCurrentItemsInLocalStorage();

    let startIndex = (pageIndex - 1) * maximumCellPerPage;

    //logic for empty pages and items in page
    if (startIndex < 0) {
      startIndex = 0;
    }

    let returnArr = [];
    let fixArr = [];
    let loseArr = [];
    let cartArr = [];

    for (var i = startIndex; i < startIndex + 5 && i < itemsData.length; i++) {
      if (itemsData[i].ACTIONTYPE == "Trả") {
        returnArr.push(itemsData[i]);
      } else if (itemsData[i].ACTIONTYPE == "Báo mất") {
        loseArr.push(itemsData[i]);
      } else if (itemsData[i].ACTIONTYPE == "Báo hư hỏng") {
        fixArr.push(itemsData[i]);
      } else {
        cartArr.push(itemsData[i]);
      }
    }

    me.ProductCart(cartArr);
    me.listFixItem(fixArr);
    me.listLoseItem(loseArr);
    me.listReturnItem(returnArr);
  };

  this.calculatePageCount = function(currentPage) {
    let totalItems = me.getCurrentItemsInLocalStorage().length;
    let pagesCount =
      parseInt(totalItems / maximumCellPerPage) +
      (totalItems % maximumCellPerPage == 0 ? 0 : 1);

    if (totalItems == maximumCellPerPage) {
      pagesCount = 1;
    }

    Component.Custom.LocalPaging.items["localPaging"].render(
      currentPage,
      pagesCount,
      maximumCellPerPage
    );
  };

  this.localPagingRendered = function() {
    console.log('aasdasd');
    return true;
  }

  this.localPagingBeforeRemove = function() {

  }

  var generateLocalPaging = function() {
    Component.Custom.LocalPaging.init();
    Component.Custom.LocalPaging.items[
      "localPaging"
    ].on_PageChange = local_pageChange;

    me.calculatePageCount(1);
    me.generatePageViewData(1, maximumCellPerPage);
  };


  var local_pageChange = function(localPaging) {
  };

  this.init = function() {
    APIData.RoleAndPermission.checkUserPermission(
      "resource.asset.view",
      function(res) {
        console.log(res);
        res = JSON.parse(res);
        if (res.Result == false) {
          me.isUser(res.Result);
          disable_input_user_domain();
        }
      }
    );
    Component.AssetsCart.SetUserName(me.user_domain);
    Component.AssetsCart.ResetCart();

    me.ProductCart(Component.ShopCart.GetCart()) &&
      me.OtherProductCart(Component.ShopCart.GetCart("cartother"));
    getReportingLine(me.user_domain, function(res) {
      reportingLine = res;
    });
    setFullNameAndSeat(me.user_domain);
    initLocalListItemAction();

    //cpnguyen
    generateLocalPaging();
    // Component.Custom.Popup.items["popReturnConfirmname"].Popup.show();
  };
};
