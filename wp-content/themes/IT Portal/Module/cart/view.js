Module.View = function(_model, _onDone) {
  var me = this;
  var model = _model;
  Component.Module.AbstractModuleView.call(this, model, _onDone);
  Config.Global.isInitMenuMouseHover = false;
  Config.Global.isInitMouseWheel = false;

  $("#txb_SearchAsset").on("keyup", model.txb_SearchAsset_OnKeyUp);

  $("#search_asset_bt").on("click", model.search_asset_btn_onClick);

  $("#txb_SearchAsset").on("mousedown", function(e) {
    e = e || window.event;
    e.preventDefault();
    $("#txb_SearchAsset").select();
  });

  $("#search_user_text_box").on("mousedown", function(e) {
    e = e || window.event;
    e.preventDefault();
    $("#search_user_text_box").select();
  });

  $("#search_user_bt").on("click", function(e) {
    if ($("#search_user_text_box").val() != model.full_name()) {
      model.search_user();
    }
  });

  $("#search_user_text_box").on("keypress", function(e) {
    if (e.which === 13) {
      if ($("#search_user_text_box").val() != model.full_name()) {
        model.search_user();
      }
    }
  });

  $("#search_user_text_box").on("change", function(e) {
    model.search_user();
  });

  $('[class="fa fa-close"]').on("click", model.do_AddRetriveAsset);

  $("#txb_SearchAsset").bind("input propertychange", function() {
    if (this.value == "") {
      model.refresh_load_asset();
    }
  });

  //cpnguyen
  $('#txbSearchItems').on('keyup', model.txbSearchItems_OnKeyUp);
  $('#searchItemsBtn').on('click', model.searchItems_BtnPressed);

  $(document).ready(function() {
    model.user_domain = model._parent.currentLoginUserDomain;
    model.init();
    me.onDone();
  });
};
