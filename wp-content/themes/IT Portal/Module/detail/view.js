Module.View = function (_model, _onDone) {
    var me = this;
    var model = _model;
    var ComponentData = {};
    Component.Module.AbstractModuleView.call(this, model, _onDone);
    Config.Global.isInitMenuMouseHover = false;
    Config.Global.isInitMouseWheel = false;

    $('[name="btnAddToCart"]').on('click', function () {
        var parent = $(this).parent();
        var COMPONENTID = parent.attr('COMPONENTID');
        var COMPONENTNAME = parent.find('[name="COMPONENTNAME"]').text();
        var COMPONENTTYPEID = parent.find('[name="COMPONENTTYPEID"]').val();
        var COMPONENTTYPENAME = parent.find('[name="COMPONENTTYPENAME"]').val();

        Component.ShopCart.AddToCart({
            COMPONENTID: COMPONENTID,
            COMPONENTNAME: COMPONENTNAME,
            COMPONENTTYPEID: COMPONENTTYPEID,
            COMPONENTTYPENAME: COMPONENTTYPENAME
        });
    });

    $(document).ready(function () {

        var photoList = $('.all-photo img');
        var thubmails = $('.selected-photo span.preview');

        photoList.on('click', function () {
            var src = this.src;
            thubmails.css({
                backgroundImage: 'url(' + src + ')'
            });
        });

        $('.all-photo img[alt="' + thubmails.attr('thubmails') + '"]').click();

        ComponentData = (function () {
            return $('[section="detail"]')[0].Model;
        })();
        ComponentData.Component = JSON.parse(ComponentData.Component).Result;
        $('#btnAddToCart').on('click', function () {
            Component.ShopCart.AddToCart({
                COMPONENTID: ComponentData.Component.COMPONENTID,
                COMPONENTNAME: ComponentData.Component.COMPONENTNAME,
                COMPONENTTYPENAME: ComponentData.Component.COMPONENTTYPENAME,
                COMPONENTTYPEID: ComponentData.Component.COMPONENTTYPEID,
                QUANTITY: parseInt($('#quantity').val())
            });
        });

        $('[name="ReviewForm"]').on('ajaxsubmit', function (e, res) {
            res = JSON.parse(res);
            if (res.State) {
                Component.System.alert.show('Đánh giá của bạn đã được gửi',
                    {
                        name: 'Đồng ý',
                        cb: function () {
                            var rateField = Component.Custom.Form.get('ReviewForm').field('Rate');
                            var commentField = Component.Custom.Form.get('ReviewForm').field('Comment');
                            rateField[0].reset();
                            commentField[0].reset();
                            window.location.reload();
                        }
                    })
            } else {
                Component.System.alert.show('Đánh giá của bạn chưa gửi được',
                    {
                        name: 'Đồng ý'
                    })
            }
        });

        me.onDone();
    });
};