Module.Model = function () {
    var me = this;
    this.price = ko.observable('');
    this.pagecontent = ko.observable('');

    /*
    var temp = APIData.ResourceCategory.getAll(function (res) {
        res = JSON.parse(res);
        console.log(res);
    });
    */
    var getModelIDFromUrl = function() {
        var splitURL = window.location.pathname.split('/');
        return splitURL[2];
    }

    var init = function () {

        APIData.Component.GetByID(getModelIDFromUrl(), function (res) {
            res = JSON.parse(res).Result;
            var typeID = res.COMPONENTTYPEID;
            var modelID = res.COMPONENTID;
            var startIndex = 0;
            var limit = -1;
            /*APIData.CMDB.GetAssetInStoreByTypeAndModel(typeID, modelID, startIndex, limit, function (res1) {
                console.log(res1);
                var temp = res1[0].RESOURCENAME;

            });*/
        });


        APIData.EditPage.GetPageContentForBinding("detail.php", function (res) {
            res = JSON.parse(res);
            me.pagecontent(res);
        });
    };

    init();
};