Module.Model = function () {
    var me = this;
    this.Products = ko.observableArray([]);
    this.Filter = ko.observableArray([]);
    var currentPage = 0;
    var limit = 14;
    var total = 0;
    var pageCount = 0;


    var ani_productContainer = function (isShow) {

        var productContainer = $('.product-container');
        var productLoading = $('.product-loading');

        if (isShow) {
            productContainer.css({
                opacity: 0,
                height: 'auto'
            });
            productContainer.animate({
                opacity: 1
            }, 1000);

            productLoading.animate({
                height: 0
            }, 500, function () {
                productLoading.hide();
            });
        } else {
            productContainer.animate({
                opacity: 0
            }, 500, function () {
                productContainer.css({
                    height: 0
                })
            });
            productContainer.css({
                height: 'auto'
            });
            productLoading.animate({
                opacity: 1
            }, 500);
        }
    };

    this.inlineShortDescription = function (desc) {
        var temp = $(desc);
        temp = temp.text();
        if (temp.length > 70) {
            temp = temp.substring(0, 70) + '...';
        }
        return temp
    };

    this.getProductThumbnail = function (model, e) {
        APIData.AssetModelDetail.GetThubmailsPath(model.COMPONENTID, function (res) {
            var eml = e.currentTarget;
            if (res !== 'null') {
                eml.src = res;
            }
        });
    };

    var rebinding_product = function () {
        var temp = me.Products();
        me.Products([]);
        me.Products(temp);
    };

    var do_PageChange = function (paging) {
        currentPage = paging.currentPage() - 1;
        loadProduct();
        $('html').animate({scrollTop: 0}, 500);
        var home = $('[section="home"]');
        home.removeClass('toggle');
    };

    this.getTransferDays = function (model) {
        if (model.Stock === 0) {
            return model.TransferDays + model.BuyDays;
        } else {
            return model.TransferDays;
        }
    };

    this.formatPrice = function () {
        /*
        var form = Component.Custom.Form.items['formOtherAsset'].Form;
        if (form.isValid()) {
            var price = form.val().ReferPrice;
            price = (price).Number.prototype.formatMoney(2, '.', ',');
            console.log(price);
        }
        */
    }

    var loadProduct = function () {
        var typeName = Libs.Location.path_param[1];
        var modelName = Libs.Location.path_param[2];

        if (modelName === undefined) {
            modelName = null;
        }

        APIData.MainMenu.GetProducts(typeName, modelName, me.Filter(), currentPage, limit, function (res) {
            res = JSON.parse(res);
            if (res.State) {
                ani_productContainer(true);
                res = res.Result;

                /*if(res.Products !== null) {
                    res.Products.forEach(function (item) {
                        item.Thubmails = '';
                        item.IsDisplay = 1;
                        item.IsStandard = 1;
                        item.NeedApproval = 0;
                        item.OtherAsset = 0;
                        APIData.AssetModelDetail.GetByModelID(item.COMPONENTID, function (res) {
                            res = JSON.parse(res);
                            item.Cost = 0;
                            item.ShortDescription = '';
                            item.ProductPageDescription = '';
                            if (res !== null) {
                                item.Cost = res.Cost.formatMoney(0, '.', ',');
                                item.ShortDescription = res.ShortDescription;
                                item.ProductPageDescription = res.ProductPageDescription;
                                item.Thubmails = res.Thubmails === '' ? 'default.png' : '/' + item.COMPONENTID + '/' + res.Thubmails;
                                item.IsDisplay = res.IsDisplay;
                                item.IsStandard = res.IsStandard;
                                item.NeedApproval = res.NeedApproval;
                            } else {
                                item.Thubmails = 'default.png';
                            }
                            rebinding_product();
                        });
                    });
                }
                */
                if (res.Products === null || res.Products === undefined) {
                    res.Products = [];
                }

                if (res.Products.length === 0) {
                    res.Products.push({
                        IsDisplay: 1,
                        OtherAsset: 1,
                        ModelID: '',
                        Thubmails: 'buy-1.png'
                    });
                } else {
                    res.Products.splice(0, 0, {
                        IsDisplay: 1,
                        OtherAsset: 1,
                        ModelID: '',
                        Thubmails: 'buy-1.png'
                    });
                }
                me.Products(res.Products);
                pageCount = res.PageCount;
                total = res.Total;
                Component.Custom.Paging.items['productPaging'].render(pageCount, limit, total);
            }
        });

    };

    this.do_AddToCart = function (model) {
        Component.ShopCart.AddToCart(model);
    };

    this.do_AddOtherAsset = function () {
        Component.Custom.Popup.items.popOtherAsset.Popup.show();
    };

    var checkQuantity = function () {
        if ($('[name="Quantity"]').val() > 10) {
            Component.System.alert.show('Bạn chỉ được đặt tối đa 10 sản phẩm', {
                name: 'Đóng',
                cb: function () {
                }
            });
            return false;
        }
        return true;
    }

    this.do_CreateRequestOrder = function () {
        if (checkQuantity()) {
            var form = Component.Custom.Form.items['formOtherAsset'].Form;
            if (form.isValid()) {
                var data = form.val();
                var ID = (new Date()).getTime();
                var productItem = {
                    ID: ID,
                    ModelType: data.ModelType,
                    ModelName: data.ModelName,
                    LinkDescription: data.LinkDescription,
                    ReferPrice: data.ReferPrice,
                    Quantity: data.Quantity,
                    Note: data.Note
                };
                Component.ShopCart.AddToCartOther(productItem);
                form.resetData();
                Component.Custom.Popup.items.popOtherAsset.Popup.hide();
            }
        }
    };

    var init = function () {
        if (Libs.Location.query_param['fil'] !== undefined) {
            me.Filter(JSON.parse(decodeURI(Libs.Location.query_param['fil'])));
        }
        var form = Component.Custom.Form.items['formOtherAsset'].Form;
        form.val({
            ModelType: Libs.Location.path_param[2]
        });
        loadProduct();
        Component.Custom.Paging.items['productPaging'].on_PageChange = do_PageChange;
    };

    this.chkFil_OnClick = function () {
        window.location = '?fil=' + JSON.stringify(me.Filter());
    };
    init();
};