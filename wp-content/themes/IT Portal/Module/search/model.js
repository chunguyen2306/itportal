Module.Model = function () {
    var me = this;
    this.searchType = ko.observable('');
    this.searchkeyword = ko.observable('');
    this.searchCustomType = ko.observable('');
    this.data = ko.observableArray([]);
    this.limit = ko.observable(16);
    this.pageTotal = ko.observable(0);
    this.pageCount = ko.observable(0);
    this.currentPage = ko.observable(0);

    var rebinding_paging = function () {
        Component.Custom.Paging.items['searchPaging'].render(me.pageCount(), me.limit(), me.pageTotal());
        Component.System.loading.hide();
    };

    var do_OnPageChange = function (paging) {
        me.currentPage(paging.currentPage() - 1);
        search();
        $('html').animate({scrollTop: 0}, 500);
    };

    var builddata_productModel = function (res) {
        res = JSON.parse(res).Result;
        var result = res.Result;
        me.pageTotal(res.Total);
        me.pageCount(res.PageCount);
        me.data([]);
        result.forEach(function (item) {
            item.merge({
                Thubmails: '',
                TransferDays: 15,
                BuyDays: 2,
                NeedApproval: 1,
                IsDisplay: 0
            });
            APIData.AssetModelDetail.GetByModelID(item.ModelID, function (detail) {
                detail = JSON.parse(detail);
                item.merge(detail);
                if (item.Thubmails === '') {
                    var regex = /Icon: (((?!Notes|Icon|Code).)*)/;
                    var found = item.TypeDescription.match(regex);
                    if (found !== null && found.length > 1) {
                        item.Thubmails = 'component_type/' + found[1];
                    } else {
                        item.Thubmails = 'component/default.png';
                    }
                } else {
                    item.Thubmails = 'component/' + item.ModelID + '/' + item.Thubmails;
                }

                if (item.Stock === 0) {
                    item.TransferDays = item.TransferDays + item.BuyDays;
                } else {
                    item.TransferDays = item.TransferDays;
                }
                var temp = me.data();
                me.data([]);
                me.data(temp);

            });
            me.data().push(item);
            me.data(me.data());
        });
        rebinding_paging();
    };

    var builddata_productOwned = function (res) {
        res = JSON.parse(res).Result;
        var result = res.Result;
        var total = res.Total;
        var pageCount = res.PageCount;
        me.data(result);
    };

    var searchProduct = function () {
        APIData.Component.SearchPaging(unescape(me.searchkeyword()),
            me.currentPage(),
            me.limit,
            me.isOwned,
            me._parent.currentLoginUserDomain,
            me.searchCustomType(),
            builddata_productModel);
    };

    var searchRequest = function () {

        APIData.APIRequestFilter.filter((me.searchkeyword().indexOf('#') === 0) ? {
                requestID: me.searchkeyword().substr(1),
                pageIndex: me.currentPage(),
                limit: me.limit(),
                isOwned: me.isOwned
            } : {
                //assetname: me.searchkeyword(),
                //assetType: me.searchkeyword(),
                //assetModel: me.searchkeyword(),
                //personInFlow: me.searchkeyword(),
                requestOwner:  me.searchkeyword(),
                filterType: 'AND',
                pageIndex: me.currentPage(),
                limit: me.limit(),
                isOwned: me.isOwned
            },
            function (res) {
                res = JSON.parse(res);
                //console.log(res);
                me.pageTotal(res.Total);
                me.pageCount(res.PageCount);
                me.data([]);
                me.data(res.Result);
                rebinding_paging();
            });
    };

    this.inlineShortDescription = function (desc) {
        var temp = $(desc);
        //console.log(temp);
        temp = temp.text();
        if (temp.length > 70) {
            temp = temp.substring(0, 500) + '...';
        }

        return temp
    };

    var searchGuild = function () {
        APIData.PostCategory.FilterPaging({
                keyword: me.searchkeyword(),
                pageIndex: me.currentPage(),
                limit: me.limit()
            },
            function (res) {
                res = JSON.parse(res);
                me.pageTotal(res.Total);
                me.pageCount(res.PageCount);
                me.data([]);
                me.data(res.Result);
                rebinding_paging();
            });
    };

    var unknowSearch = function () {

    };

    this.hightlineContent = function (content) {
        var re = new RegExp('(' + me.searchkeyword() + ')', 'gi');
        var result = $('<div>' + content + '</div>').text().substr(0, 400) + '...';
        return result.replace(re, '<i><b>$1</b></i>');
    };

    var search = function () {
        Component.System.loading.show();
        switch (me.searchType()) {
            case 'product':
                searchProduct();
                break;
            case 'request':
                searchRequest();
                break;
            case 'guild':
                searchGuild();
                break;
            default:
                unknowSearch();
                break;
        }
    };

    this.do_AddToCart = function (model, e) {
        Component.ShopCart.AddToCart({
            COMPONENTID: model.ModelID,
            COMPONENTNAME: model.ModelName,
            COMPONENTTYPEID: model.TypeID,
            COMPONENTTYPENAME: model.TypeName,
            COMMENTS: '',
            QUANTITY: 1
        });
    };

    var init = function () {
        me.searchType(Common.getUrlParameters()['st']);
        me.searchkeyword(Common.getUrlParameters()['sk'].trim());
        me.searchCustomType(Common.getUrlParameters()['sct']);
        me.searchkeyword(unescape(decodeURI(me.searchkeyword().replace(/\+/g, ' '))).trim());
        $(document).ready(function () {
            search();
            Component.Custom.Paging.items['searchPaging'].on_PageChange = do_OnPageChange;
        });
    };

    init();
};