Module.View = function (model, _onDone) {
    var me = this;
    Component.Module.AbstractModuleView.call(this, model,_onDone );
    Config.Global.isInitMenuMouseHover = false;
    Config.Global.isInitMouseWheel = false;

    model.isOwned = JSON.parse($('#isOwnedSearch').val());

    me.onDone();
};