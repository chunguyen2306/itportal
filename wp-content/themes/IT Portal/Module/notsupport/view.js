Module.View = function (_model, _onDone) {
    var me = this;
    var model = _model;
    Component.Module.AbstractModuleView.call(this, model,_onDone );
    Config.Global.isInitMenuMouseHover = false;
    Config.Global.mouseWheelBaseOn = '.footer';
    Config.Global.mouseWheelLimitHeight = true;

    $(document).ready(function () {
       var eml = $('[section="notsupport"]');
       console.log(eml);
       var emlHeight = eml[0].offsetHeight;
       console.log(emlHeight);
       eml.css({
           top: '50%',
           marginTop: -(emlHeight/2)+'px'
       });
    });

    me.onDone();
};