
Module.Model = function () {
    var me = this;
    this.pagecontent = ko.observable('');

    var init = function () {
        APIData.EditPage.GetPageContentForBinding("home.php", function (res) {
            res = JSON.parse(res);
            me.pagecontent(res);
        });
    };

    init();
};