HomeSearch = new function () {
    var me = this;
    var count = 7;
    me.value = ko.observable();

    me.search = function (keyword, start, limit, endSearch) {
        if (!keyword) {
            setTimeout(function () {
                endSearch([]);
            }, 1);

            return;
        }

        APIData.Search.HomeSearch(keyword, 0, count, function (res) {
            if (res.code == AMResponseCode.ok) {
                endSearch(res.data)
            }
        });
    };

    if(location.pathname.match('/search[^\w]*') == null){
        me.value.subscribe(function (val) {
            if(!val.Name)
                return;

            if(val.itemType == null){
                location = '/search?q=' + val.Name;
            }else{
                location = '/detail/' + val.ID;
            }
        });
    }
};