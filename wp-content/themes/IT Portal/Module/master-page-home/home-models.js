// kolog = function (val) {
//     console.log(val);
//     return val;
// }

Notification = new function () {
    var t = this;
    var isFirstLoad = true;
    var sr = null;

    t.list = ko.observableArray();
    t.isShowing = ko.observable();
    t.isLoading = ko.observable(true);
    t.unSeenNumber = ko.observable();
    t.orderLink = '/user-page/order/detail?';
    t.allNotiLink = '/user-page/notifications';

    function load() {
        if (isFirstLoad == false && t.seenNumber() == 0)
            return;

        isFirstLoad = true;
        t.isLoading(t.list().length == 0);
        APIData.Notification.GetListForPreview(function (res) {
            if (res.code == AMResponseCode.ok) {
                t.list(res.data);
                t.unSeenNumber(0);
            } else {
            }
            t.isLoading(false);
        });

    }

    function markAsRead(data) {
        APIData.Notification.MaskAsRead([data.ID]);
    }

    function windowClick(e) {
        t.showChange(null, e);
    }

    function showChange(val) {
        if(t.isShowing() == val)
            return;

        if(val){
            load();
            setTimeout(function () {

                $(window).on('click', windowClick);
            }, 1);
        }else{
            $(window).off('click', windowClick);
        }


        t.isShowing(val);
    }

    function countUnseen() {
        APIData.Notification.CountUnseen(function (res) {
            if (res.code == AMResponseCode.ok) {
                var count = res.data < 100 ?
                    res.data : '99<span style="font-weight: bold;right: -2px;position: absolute;top: -10px;color: #FFC107;">+</span>';
                t.unSeenNumber(count);
                $("#notis")[0].style.display = "";
            }
        });

    }

    t.showChange = function (data, e) {
        var flag = t.isShowing();
        var dom = e.target;
        var domParent = dom.parentElement;
        if(flag){
            if(dom.attributes.clicktoshow === undefined){
                while (domParent){
                    if(domParent.attributes.clicktoshow !== undefined)
                        return;

                    domParent = domParent.parentElement;
                }
            }
            showChange(!flag);
        }else{
            showChange(!flag);
        }
    };

    t.toAllNoti = function (data, e) {
        window.location = t.allNotiLink;
    };

    t.toNoti = function (data) {
        APIData.Notification.MarkAsRead([data.ID], true, function (res) {

        });
        window.location = t.orderLink + "ID=" + data.Content;
    };

    countUnseen();

    // signalr setup
    $(function () {
        function onSignalrConnected(unKnowParams, signalrID) {
            APIData.Notification.ListenNew(signalrID, onListenNewDone);
        }
        function onListenNewDone(res) {
            if(res.code != AMResponseCode.ok) {}
                //console.log(res);
        }

        function onGet( data) {
            if(!t.isShowing()){
                var count = t.unSeenNumber();
                count = count ? (count + 1) : 1;
                t.unSeenNumber(count);
            }
            t.list.splice(0, 0, data);
        }

        sr = SignalREngine.getDefault();
        sr.onConnected(onSignalrConnected);
        sr.connect();
        sr.onGet(SignalREngine.method.newNoti, onGet);
    });
};

// HomeSearch = new function () {
//     var t = this;
//     var count = 7;
//     t.value = ko.observable();
//
//     t.search = function (keyword, start, limit, endSearch) {
//         if (!keyword) {
//             setTimeout(function () {
//                 endSearch([]);
//             }, 1);
//
//             return;
//         }
//
//         APIData.Search.HomeSearch(keyword, 0, count, function (res) {
//             if (res.code == AMResponseCode.ok) {
//                 endSearch(res.data)
//             }
//         });
//     };
//
//     if (location.pathname.match('/search[^\w]*') == null) {
//         t.value.subscribe(function (val) {
//             if (!val.Name)
//                 return;
//
//             if (val.itemType == null) {
//                 location = '/search?q=' + val.Name;
//             } else {
//                 location = '/detail/' + val.ID;
//             }
//         });
//     }
// };

Footer = new function () {
    var me = this;
    me.pagecontentMPH = ko.observable('');

    APIData.EditPage.GetPageContentForBinding("master-page-home.php", function (res) {
        res = JSON.parse(res);
        me.pagecontentMPH(res);
    });
};