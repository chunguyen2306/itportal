Module.Model = function() {
  var me = this;
  this.userAccount = ko.observable(Common.getUrlParameters()["asset"]);
  this.myDept = ko.observable();
  this.keyword = ko.observable();
  this.quantityAssets = ko.observable("");
  this.SelectedAsset = [];
  this.AssetsInPage = [];
  this.requestData = ko.observable({});
  //cpnguyen
  this.keyword = ko.observable("");
  var current_user = "";
  var reportingLine = "";
  var actionSelected = "";
  const limitAssetInPage = 15;
  var currentPage = 0;
  var quantitySelectedInpage = 0;
  var isHelpDesk;

  var do_PageChange = function(e) {
    window.location = Common.build_param({
      page: e.currentPage()
    });
  };

  /*
    var do_PageChange1 = function (paging) {
        currentPage = paging.currentPage() - 1;
        loadProduct();
        $('html').animate({scrollTop: 0}, 500);
        var home = $('[section="home"]');
        home.removeClass('toggle');
    };
    */

  this.txbSearchAsset_OnKeyUp = function(e) {
    if (e.key === "Enter") {
      me.searchAsset(me.keyword());
    } else {
      me.keyword(this.value);
    }
  };

  this.searchAsset_BtnPressed = function() {
    me.searchAsset(me.keyword());
  };

  this.searchAsset = function(keyword) {
    let url = new URL(window.location.href);
    let categoryType = url.searchParams.get("categoryType") || "All";
    if (!url.searchParams.get(categoryType)) {
      window.location = Common.build_param({
        page: 1,
        categoryType,
        keyword
      });
    }
  };

  this.txbUserAccount_OnKeyUp = function(e) {
    var keyword = $("#txbUserAccount").val();
    if (e.key === "Enter") {
      me.btnHelpdeskCheckAccount_OnClick();
    }
  };

  this.btnHelpdeskCheckAccount_OnClick = function(e) {
    window.location = Common.build_param({
      page: 1,
      keyword: "#ignore",
      productType: "#ignore",
      username: me.userAccount(),
      asset: me.userAccount()
    });
  };

  this.chkMyDept_OnCheck = function(e) {
    window.location = Common.build_param({
      page: 1,
      keyword: "#ignore",
      username: "#ignore",
      mydept: this.myDept()
    });
  };

  var enableButton = function(idButton) {
    $(idButton).attr("disabled", false);
    $(idButton).attr("style", "");
    $(idButton).attr("title", "");
  };

  var enableAllButton = function() {
    $("#lost_button").attr("class", "gray");
    enableButton("#lost_button");

    $("#return_button").attr("class", "gray");
    enableButton("#return_button");

    $("#damage_button").attr("class", "gray");
    enableButton("#damage_button");

    //$("#replace_button").attr("class", "red");
    //enableButton("#replace_button");
  };

  var disableButton = function(idButton) {
    $(idButton).prop("disabled", true);
    $(idButton).attr("class", "gray");
    $(idButton).attr("style", "cursor: not-allowed !important");
    $(idButton).attr("title", "Chưa chọn tài sản nào");
  };

  var disableAllButton = function() {
    disableButton("#lost_button");
    disableButton("#return_button");
    disableButton("#damage_button");
    disableButton("#replace_button");
  };

  this.do_close = function() {
    localStorage.removeItem("listAsset");
  };

  this.fill_check_to_selected_Asset = function() {
    var listAssetTemp = Libs.LocalStorage.val("listAsset");
    for (var i = 0; i < listAssetTemp.length; i++) {}
  };

  var isSelected = function(assetID) {
    for (var i = 0; i < me.SelectedAsset.length; i++) {
      if (assetID == me.SelectedAsset[i].AssetName) {
        return true;
      }
    }
    return false;
  };

  var addAllItem = function() {
    var user = me.userAccount();
    if (user == "" || user === undefined || user === null) {
      user = wp_current_user_domain;
    }
    var assetVal = Common.getUrlParameters()["asset"];
    APIData.Resources.GetUserAsset(
      user,
      assetVal,
      "",
      0,
      10000,
      function(res) {
        res = JSON.parse(res);
        var assetsListTemp = res.Result.Assets;
        for (var i = 0; i < assetsListTemp.length; i++) {
          if (!isSelected(assetsListTemp[i].RESOURCENAME)) {
            addItem(assetsListTemp[i]);
          }
        }
        me.quantityAssets(Component.AssetsCart.GetQuantityAssetSelected());
      },
      isHelpDesk
    );
  };

  var addAllItemInPage = function() {
    //cpnguyen
    //AssetsInPage is empty [] so it won't have any effect on page
    // for (var i = 0; i < me.AssetsInPage.length; i++) {
    //     if (!isSelected(me.AssetsInPage[i].RESOURCENAME)) {
    //         addItem(me.AssetsInPage[i]);
    //     }
    // }

    let assets_list = $("table tbody tr input[type=hidden]");
    for (var i = 0; i < assets_list.length; i++) {
      let asset = JSON.parse(assets_list[i].value);
      if (!isSelected(asset.RESOURCENAME)) {
        addItem(asset);
      }
    }
  };

  var removeAllItem = function() {
    Component.AssetsCart.RemoveAll();
    restore_selected_asset(me.AssetsInPage);
    removeCheckedItemInView(null, true);
  };

  var removeAllItemInPage = function() {
    //cpnguyen
    //AssetsInPage is empty [] so it won't have any effect on page
    // for (var i = 0; i < me.AssetsInPage.length; i++) {
    //     if (isSelected(me.AssetsInPage[i].RESOURCENAME)) {
    //         removeItem(me.AssetsInPage[i].RESOURCENAME);
    //     }
    // }

    let assets_list = $("table tbody tr input[type=hidden]");
    for (var i = 0; i < assets_list.length; i++) {
      let asset = JSON.parse(assets_list[i].value);
      if (isSelected(asset.RESOURCENAME)) {
        removeItem(asset.RESOURCENAME);
      }
    }
  };

  var addItem = function(data) {
    var assetItem = {
      AssetModel: data.COMPONENTNAME,
      AssetModelID: data.COMPONENTID,
      AssetType: data.COMPONENTTYPENAME,
      AssetTypeID: data.COMPONENTTYPEID,
      AssetName: data.RESOURCENAME
    };
    Component.AssetsCart.AddToCart(assetItem, current_user);
    me.SelectedAsset = Component.AssetsCart.GetCart();
    enableAllButton();
    quantitySelectedInpage += 1;
    if (quantitySelectedInpage == me.AssetsInPage.length) {
      $("#product_checked_all_id").prop("checked", true);
    }
    $("#" + data.RESOURCENAME).prop("checked", true);
  };

  var removeItem = function(assetID) {
    Component.AssetsCart.RemoveFormCart(assetID, current_user);
    me.SelectedAsset = Component.AssetsCart.GetCart();
    restore_selected_asset(me.AssetsInPage);
    removeCheckedItemInView(assetID);
  };

  var removeCheckedItemInView = function(assetID, isAll) {
    $("#product_checked_all_id").prop("checked", false);
    if (isAll === null || isAll === undefined) {
      isAll = false;
    }
    if (isAll) {
      for (var i = 0; i < me.AssetsInPage.length; i++) {
        $("#" + me.AssetsInPage[i].RESOURCENAME).prop("checked", false);
      }
    } else {
      $("#" + assetID).prop("checked", false);
    }
  };

  this.chk_Asset = function(model, e) {
    var checked = e.currentTarget.checked;
    var data = JSON.parse(
      $(e.currentTarget.parentNode.parentNode)
        .find("input[type=hidden]")
        .val()
    );
    if (checked) {
      addItem(data);
    } else {
      removeItem(data.RESOURCENAME);
    }

    //cpnguyen
    me.chkAllButtonChecked();
    me.quantityAssets(Component.AssetsCart.GetQuantityAssetSelected());
  };

  this.sel_actionSelected = function(model, e) {
    if (me.SelectedAsset.length === 0) {
      Component.System.alert.show("Chưa có tài sản nào được chọn", {
        name: "Đồng ý",
        func: function() {}
      });
      return;
    }
    actionSelected = e.currentTarget.value;

    me.requestData({
      CurrentUser: current_user,
      ReportingLine: reportingLine,
      AssetList: me.SelectedAsset,
      Note: ""
    });
    APIData.User.GetUserByDomain(current_user, function(res) {
      if (res != null) {
        res = JSON.parse(res);
        if (res != null) {
          if (res.seat != null && res.seat != undefined && res.seat != "") {
            me.requestData().Address = res.seat;
          } else {
            me.requestData().Address = "Không xác định";
          }
        } else {
          me.requestData().Address = "Không xác định";
        }
      } else {
        me.requestData().Address = "Không xác định";
      }
    });

    var selectedAsset = Component.AssetsCart.GetCart();

    //get action type
    let action_type = e.currentTarget.innerHTML.trim();
    asset_array = [];
    for (var i = 0; i < selectedAsset.length; i++) {
      asset_array.push({
        RESOURCENAME: selectedAsset[i].AssetName,
        COMPONENTID: selectedAsset[i].AssetModelID,
        COMPONENTNAME: selectedAsset[i].AssetModel,
        COMPONENTTYPEID: selectedAsset[i].AssetName,
        COMPONENTTYPENAME: selectedAsset[i].AssetType,
        ACTIONTYPE: action_type,
        COMMENTS: "",
        QUANTITY: 1
      });
    }

    Component.ShopCart.AddToCartFromMyProduct(asset_array);

    Component.System.alert.show(
      "Sản phẩm bạn chọn đã được thêm vào Giỏ hàng </br> Truy cập Giỏ hàng để tiếp tục.",
      {
        name: "Đóng",
        cb: function() {}
      }
    );
  };

  this.btnSubmitCreateRequest_OnClick = function() {
    var form = Component.Custom.Form.get("CreateRequestForm");
    var data = form.val();
    var errors = [];
    if (data.Note === undefined || data.Note === "") {
      errors.push("Ghi chú không được để trống");
    }
    if (errors.length > 0) {
      Component.System.alert.show(errors.join("<br>"), {
        name: "Đồng ý",
        cb: function() {}
      });
    } else {
      me.requestData().Note = data.Note;
      var requestList = [];
      requestList.push({
        actionType: actionSelected,
        data: me.requestData()
      });
      Component.System.loading.show();
      Libs.Workflow.createWithMappingData(requestList, function(res) {
        var mess = "";
        for (var i = 0; i < res.length; i++) {
          if (res[0].result) {
            mess +=
              "Yêu cầu đã được tạo thành công. Mã đơn hàng là <b>" +
              res[i].response.request.ID +
              "</b>.";
          } else {
            mess +=
              "Yêu cầu <b>" +
              res[i].response.workflow.DisplayName +
              "</b> của bạn chưa được tạo, " +
              "vui lòng liên hệ helpdesk để được xử lý.";
          }
          mess += "<br>";
        }
        Component.System.loading.hide();
        if (Component.Custom.Popup.HasActive) {
          Component.Custom.Popup.Active.hide();
        }
        Component.AssetsCart.ResetCart();
        Component.System.alert.show(
          mess,
          {
            name: "Xem đơn hàng",
            cb: function() {
              window.location =
                "/user-page/order/detail/?ID=" + res[0].response.request.ID;
            }
          },
          {
            name: "Đóng",
            cb: function() {
              location.reload();
            }
          }
        );
      });
    }
  };

  this.btnCancelCreateRequest_OnClick = function() {
    Component.Custom.Popup.Active.hide();
  };

  this.chk_ShowAll = function(model, e) {
    if (e.currentTarget.checked) {
      window.location = Common.build_param({
        page: "#ignore",
        limit: 0,
        keyword: "#ignore"
      });
    } else {
      window.location = Common.build_param({
        keyword: "",
        limit: limitAssetInPage
      });
    }
  };
  //cpnguyen
  this.scroll_left = function(e) {
    document.getElementsByClassName("scrollable-chip-row")[0].scrollLeft =
      document.getElementsByClassName("scrollable-chip-row")[0].scrollLeft < 0
        ? 0
        : document.getElementsByClassName("scrollable-chip-row")[0].scrollLeft -
          200;
  };

  this.scroll_right = function(e) {
    document.getElementsByClassName("scrollable-chip-row")[0].scrollLeft += 200;
  };

  //cpnguyen
  this.pageLoaded = function(e) {
    //check if current page assets has been selected
    let assets_list = $('table tbody tr input[component="checkbox"]');
    let selected_assets = JSON.parse(localStorage["listAsset"]);
    for (var i = 0; i < assets_list.length; i++) {
      for (var j = 0; j < selected_assets.length; j++) {
        if (selected_assets[j].AssetName == assets_list[i].id) {
          $("#" + assets_list[i].id).prop("checked", true);
          break;
        }
      }
    }

    //chk all button logic
    me.chkAllButtonChecked();
  };

  //cpnguyen
  this.chkAllButtonChecked = function() {
    //if all assets in page is checked , checked header
    let assets_list = $('table tbody tr input[component="checkbox"]');
    let check = true;
    for (var i = 0; i < assets_list.length; i++) {
      if ($("#" + assets_list[i].id).prop("checked") == false) {
        check = false;
        break;
      }
    }

    $("#product_checked_all_id").prop("checked", check);
  };

  this.chip_select = function(model, e) {
    let categoryType = e.currentTarget.innerHTML;
    let url = new URL(window.location.href);
    let keyword = url.searchParams.get("keyword") || "";
    let isDifferentPage = url.searchParams.get("categoryType") != categoryType;

    if (isDifferentPage) {
      window.location = Common.build_param({
        page: 1,
        categoryType,
        keyword
      });
    }
  };

  this.select_all_event = function() {
    $("#product_checked_all_id").prop("checked", true);
    $('[name=asset-table] tbody input[component="checkbox"]').prop(
      "checked",
      true
    );
    addAllItem();
  };

  this.remove_all_event = function() {
    $('[name=asset-table] tbody input[component="checkbox"]').prop(
      "checked",
      false
    );
    removeAllItem();
    me.quantityAssets(0);
  };

  this.chk_SelectAllInPage = function(model, e) {
    var isChecked = e.currentTarget.checked;

    if (isChecked) {
      $('[name=asset-table] tbody input[component="checkbox"]').prop(
        "checked",
        true
      );
      addAllItemInPage();
    } else {
      $('[name=asset-table] tbody input[component="checkbox"]').prop(
        "checked",
        false
      );
      removeAllItemInPage();
    }
    me.quantityAssets(Component.AssetsCart.GetQuantityAssetSelected());
    //$('[name=asset-table] tbody input[component="checkbox"]').trigger('change');
  };

  var restore_selected_asset = function(asset_in_table) {
    quantitySelectedInpage = 0;
    var carts = Component.AssetsCart.GetCart();
    var quantityAssets = Component.AssetsCart.GetQuantityAssetSelected();
    me.quantityAssets(quantityAssets);
    me.SelectedAsset = carts;
    if (carts === undefined || carts === null || carts.length == 0) {
      disableAllButton();
    } else {
      for (var i = 0; i < carts.length; i++) {
        for (var j = 0; j < asset_in_table.length; j++) {
          if (carts[i].AssetName == asset_in_table[j].RESOURCENAME) {
            quantitySelectedInpage += 1;
            $("#" + asset_in_table[j].RESOURCENAME).prop("checked", true);
            if (quantitySelectedInpage == asset_in_table.length) {
              $("#product_checked_all_id").prop("checked", true);
            }
          }
        }
      }
    }
  };

  this.init = function() {
    isHelpDesk = Common.getUrlParameters()["helpdesk"];
    if (
      isHelpDesk === null ||
      isHelpDesk === undefined ||
      isHelpDesk === "" ||
      isHelpDesk != "true"
    ) {
      isHelpDesk = false;
    } else {
      isHelpDesk = true;
    }
    var status = JSON.parse(Libs.LocalStorage.val("isHelpDesk"));

    //current_user = Common.getUrlParameters()['username'] || me._parent.currentLoginUserDomain;
    var assetVal = Common.getUrlParameters()["asset"];
    var userInPreviousSession = Component.AssetsCart.GetUserName();

    if (isHelpDesk ^ status) {
      Libs.LocalStorage.val("isHelpDesk", JSON.stringify(isHelpDesk));
      me.remove_all_event();
    }
    // Kiem tra change search user
    if (isHelpDesk) {
      if (current_user != userInPreviousSession) {
        me.remove_all_event();
      }
    }
    Component.AssetsCart.SetUserName(current_user);

    currentPage = parseInt(Common.getUrlParameters()["page"]) || 1;
    var keywordTemp = Common.getUrlParameters()["keyword"];
    if (keywordTemp === undefined || keywordTemp === null) {
      keywordTemp = "";
    }

    var limitTemp = parseInt(Common.getUrlParameters()["limit"]);
    var limit = limitAssetInPage;
    if (limitTemp !== undefined && limitTemp !== null && limitTemp == 0) {
      limit = limitTemp;
    }
    APIData.Request.GetReportingLine(current_user, function(res) {
      if (res.code == AMResponseCode.ok) {
        if (res.data.length > 0) {
          reportingLine = res.data[0].Domain;
        } else {
          reportingLine = "";
        }
      } else {
        reportingLine = "";
      }
    });
    APIData.Resources.GetUserAsset(
      current_user,
      assetVal,
      keywordTemp,
      currentPage - 1,
      limit,
      function(res) {
        res = JSON.parse(res);
        me.AssetsInPage = res.Result.Assets;
        restore_selected_asset(me.AssetsInPage);
      },
      isHelpDesk
    );

    if (Component.Custom.Paging.items["productPaging"] !== undefined) {
      Component.Custom.Paging.items["productPaging"].render();
      Component.Custom.Paging.items[
        "productPaging"
      ].on_PageChange = do_PageChange;
    }
  };

  me.init();
};
