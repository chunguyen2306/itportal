Module.View = function (model, _onDone) {
    var me = this;
    Component.Module.AbstractModuleView.call(this, model,_onDone );
  
    $('#txbSearchAsset').on('keyup', model.txbSearchAsset_OnKeyUp);

    $('#txbUserAccount').on('keyup', model.txbUserAccount_OnKeyUp);
  
    $(document).ready(model.pageLoaded);
  
    //cpnguyen
    $('#searchBtn').on('click', model.searchAsset_BtnPressed);
  
    var URL_PARAM = Common.getUrlParameters();
  
    if(URL_PARAM['mydept'] === 'true'){
        model.myDept(true);
    } else {
        model.myDept(false);
    }
    if(URL_PARAM['keyword'] !== undefined){
        model.keyword( decodeURI(URL_PARAM['keyword']) );
    }
  
    me.onDone();
  };