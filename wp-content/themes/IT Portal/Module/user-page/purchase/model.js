Module.Model = function () {
    var t = this;
    var RequestStatusID = APIData.Request.RequestStatusID;
    var com = new function () {
        var me = this;
        var sendID = 0;
        var tryAgainTime = 0;
        var onReplyDone = null;
        var sr = SignalREngine.getDefault();
        var method = SignalREngine.method;
        me.isSending = false;

        sr.onGet(method.showRequestReply, function (rep) {
            if (rep.sendID == sendID) {
                me.isSending = false;
                if (typeof onReplyDone == 'function')
                    onReplyDone();
                me.stopTryAgain();
            }
        });

        function send(curSendID, requests) {
            if (curSendID != sendID || tryAgainTime < 1)
                return;

            tryAgainTime--;
            sr.send(
                method.showRequestQR,
                {
                    requests: requests,
                    sendID: curSendID,
                    pcID: sr.signalRID
                },
                [wp_current_user_domain],
                SignalREngine.typeOfReciverID.Domain
            );

            setTimeout(send, 3000, curSendID, requests);
        }

        me.sendRequests = function (requestIDs, onDone) {
            tryAgainTime = 40;
            me.isSending = true;
            onReplyDone = onDone;
            send(++sendID, requestIDs, onDone);
        }

        me.stopTryAgain = function () {
            sendID++;
        }
    };

    this.listRequestDefined = ko.observableArray([]);
    this.requestFilter = [];
    t.requests = ko.observableArray();
    t.selectedRequest = ko.observableArray();
    t.renderTickets = ko.observable(false);
    t.requestLink = "/user-page/order/detail?";
    t.user_info = ko.observable();
    t.listWorkFlow = ko.observableArray();
    this.searchKeyword = '{{all}}';

    var isSort = false;
    var requests_storage = [];
    var requests_storage_temp = [];
    var requests_storage_sorted = [];
    var selectedRequestTemp = [];

    var pageCount = 0;
    var total = 0;
    var limitInPage = 9;
    var currentPage = 0;

    var pageCountQR = 0;
    var totalQR = 0;
    var limitInPageQR = 1;
    var currentPageQR = 1;

    var limitAsssetToView = 2;

    this.getPageResult = function () {
        return "{0}/{1}".format((currentPage * limitInPage)+t.requests().length, total);
    };

    t.selectWF = function (data) {
        data.selected(!data.selected());
    };

    t.showTicket = function (data) {
        if (!data.selected()) {
            data.selected(true);
        }

        var indexQR = 0;
        for (var i = 0; i < t.selectedRequest().length; i++) {
            if (t.selectedRequest()[i].ma_don_hang == data.ma_don_hang) {
                indexQR = i;
                continue;
            }
        }
        indexQR++;
        currentPageQR = Math.floor(indexQR / limitInPageQR);
        if (indexQR % limitInPageQR != 0) {
            currentPageQR++;
        }

        totalQR = t.selectedRequest().length;
        pageCountQR = Math.ceil(totalQR / limitInPageQR);

        selectedRequestTemp = t.selectedRequest().slice();
        var temp = selectedRequestTemp.slice((currentPageQR - 1) * limitInPageQR ,currentPageQR * limitInPageQR);
        t.selectedRequest(temp);

        $("#popup_qr_code").attr("width", "350px");

        Component.Custom.Popup.items['popReturnAsset'].Popup.show(function () {
            currentPageQR = parseInt(currentPageQR);
            Component.Custom.Paging.items['qrCodePaging'].render(pageCountQR, limitInPageQR, totalQR, currentPageQR);
            Component.Custom.Paging.items['qrCodePaging'].on_PageChange = do_PageChange_QRPopup;
        });

        t.renderTickets(true);
        $('.sys-blank').show();

    };

    t.hideTicket = function () {
        //t.selectedRequest(selectedRequestTemp);
        //selectedRequestTemp = [];
        Component.Custom.Popup.items['popReturnAsset'].Popup.hide();
        for (var i = 0; i < t.requests().length;  i++) {
            t.requests()[i].selected(false);
        }
        t.renderTickets(false);
        $('.sys-blank').hide();
    };

    t.toCellPhone = function () {
        var isDone = false;
        var time = 35;
        var suggestions = '<i style="font-size: smaller;"><br><br>'
            + 'Hãy chắc chắn rằng điện thoại của bạn có kết nối mạng và đang truy cập vào đường dẫn: <a>'
            + location.origin + '/user-page/order/kios-order-qrs </a></i>'
        ;

        com.sendRequests(t.selectedRequest(), function () {
            isDone = true;
            Component.System.loading.hide();
            t.hideTicket();
            Component.System.alert.hide();
        });

        var ivHandle = setInterval(function () {
            time--;
            if (time > 1 && isDone == false) {
                if (time <= 30) {
                    if (time == 30) {
                        Component.System.loading.hide();
                        t.renderTickets(false);
                    }

                    var temp = ['.', '.', '.'];
                    temp.length = 3 - (time % 3);

                    Component.System.alert.show(
                        'Đang cố gắng kết nối tới điện thoại.' + temp.join('') + suggestions,
                        {
                            name: 'Dừng kết nối',
                            cb: function () {
                                clearInterval(ivHandle);
                                com.stopTryAgain();
                            }
                        }
                    );
                }
            } else {
                clearInterval(ivHandle);
                com.stopTryAgain();
                if (isDone == false)
                    Component.System.alert.show('Không thể kết nối tới điện thoại' + suggestions);
            }
        }, 1000);

        Component.System.loading.show();
    };

    this.search_request = function () {
        $('.head-filter a').removeClass('active');
        t.searchKeyword = $("#txbUserAccount").val();

        currentPage = 0;
        if (t.searchKeyword === '' || t.searchKeyword === null || t.searchKeyword === undefined) {
            t.searchKeyword = '{{all}}';
        }

        loadRequest();
    };

    var render_paging = function () {
        Component.Custom.Paging.items['assetPaging'].render(pageCount, limitInPage, total, currentPage+1);

    };

    t.sort_user = function () {
        if (isSort) {
            t.requests(requests_storage_temp);
        } else {
            requests_storage_sorted = requests_storage_temp.slice();
            requests_storage_sorted.sort(SortByName);
            t.requests(requests_storage_sorted);
        }
        isSort = !isSort;
    }

    var SortByName = function (a, b){
        var aName = a.testNDH; //.toLowerCase();
        var bName = b.testNDH; //.toLowerCase();
        return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
    }

    t.showDetail = function (data) {
        if (!data.selected()) {
            data.selected(true);
        }
        if (t.selectedRequest().length > 1) {
            Component.System.alert.show("Chỉ xem được chi tiết của 1 đơn hàng");
            return;
        }
        var id = t.selectedRequest()[0].ma_don_hang;

    }

    var do_PageChange_QRPopup = function (paging) {
        currentPageQR = paging.currentPage();

        var temp = selectedRequestTemp.slice((currentPageQR - 1) * limitInPageQR ,currentPageQR * limitInPageQR);
        t.selectedRequest(temp);

        $('html').animate({scrollTop: 0}, 500);
        var home = $('[section="home"]');
        home.removeClass('toggle');
    };

    var do_PageChange = function (paging) {
        currentPage = paging.currentPage()-1;
        loadRequest();
    };

    this.filterRequest_OnClick = function (model, e) {
        if(model.ID !== 0){
            t.requestFilter = [model.ID];
        } else {
            t.requestFilter = [];
        }
        loadRequest();
    };

    var loadRequest = function () {
        var agrs = {
            pageIndex: currentPage,
            limit: limitInPage,
            isOwned: false,
            'stepStatus__AND': 'wait-buying'
        };

        if(t.searchKeyword.indexOf('#') == 0){
            t.searchKeyword = t.searchKeyword.substr(1);
            agrs['requestID'] = t.searchKeyword;
        } else if (t.searchKeyword !=='{{all}}'){
            agrs['assetname'] = t.searchKeyword;
            agrs['assetType'] = t.searchKeyword;
            agrs['assetModel'] = t.searchKeyword;
            agrs['personInFlow'] = t.searchKeyword;
            agrs['filterType'] = 'OR';
        }

        if(t.requestFilter.length > 0){
            agrs['requests'] = t.requestFilter;
        }

        APIData.APIRequestFilter.filter(agrs,
            function (res) {
                res = JSON.parse(res);
                pageCount = res.PageCount;
                total = res.Total;
                t.requests([]);
                t.requests(res.Result);

                render_paging();
            });
    };

    function loadData() {
        t.user_info.domain = wp_current_user_domain;
        APIData.User.GetUserByDomain(t.user_info.domain, function (res) {
            res = JSON.parse(res);
            t.user_info.fullName = res.fullName || "Không xác định";
            t.user_info.jobTitle = res.jobTitle || "Không xác định";
            t.user_info.seat = res.seat || "Không xác định";
        });

        APIData.Workflow.GetListWorkflow(function (res) {
            if (res.code == AMResponseCode.ok) {
                t.listWorkFlow(res.data);
            } else {
                Component.System.alert.show("Xảy ra lỗi trong quá trình tải dữ liệu");
            }
        });

        APIData.Request.GetListRequest('', 0, 0, function (res) {
            res.splice(0, 0, {
                DisplayName: '- Tất cả -',
                ID: 0
            });
            t.listRequestDefined(res);
        });

        loadRequest();

        Component.Custom.Paging.items['assetPaging'].on_PageChange = do_PageChange;
    }

    loadData();
};
