Module.Model = function () {
    var me = this;
    var roles = [];

    me.Domain = ko.observable(wp_current_user_domain);
    me.DelegatedDomain = ko.observable();
    me.CommentForPending = ko.observable();
    me.canSettingForAnotherUser = ko.observable();
    me.isSelectAll = ko.observable();
    me.requests = ko.observableArray();
    me.isNoRequest = ko.observableArray();
    me.isDataReady = ko.observable();
    me.filter = ko.observable(0);
    me.filter.options = [{value: 0, name: 'Chưa ủy quyền'}, {value: 1, name: 'Đã ủy quyền'}, {value: 2, name: 'Tất cả'}];
    me.requestCheckChange = function (data, arg) {
        var lstItem = $(arg.currentTarget.parentElement);
        if (data._checked)
            lstItem.addClass('checked');
        else
            lstItem.removeClass('checked');

        return true;
    };
    me.selectAll = function (sender, e) {
        if (me.isSelectAll())
            return true;


        me.requests().forEach(function (data) {
            if (!data._checked) {
                data._checked = true;
            }
        });
        me.requests.update();
        $('.request-list>li').addClass('checked');


        return true;
    };
    me.delegate = function () {
        var stepIDs = [];
        var roleRequires = [];
        var ownerDomain = me.Domain();
        var delegatedDomain = me.DelegatedDomain();
        var requests = me.requests();
        requests.forEach(function (request) {
            if (!request._checked)
                return;

            request.Steps.forEach(function (step) {
                var isProcessed = step.IsApproved || step.IsApproved === 0 || step.IsApproved === '0';
                if (isProcessed)
                    return;

                if (step.AssignedOwner != ownerDomain)
                    return;

                stepIDs.push(step.ID);

                if(step.Role && !isNaN(step.Role)){
                    roleRequires.push({
                        requestID: request.ID,
                        roleID: step.Role
                    });
                }
            });
        });

        var errorMessage = validDelegateData(delegatedDomain, stepIDs);
        if(errorMessage){
            Component.System.alert.show(errorMessage);
            return;
        }

        // var form = Component.Custom.Form.get('ChangeInUseUserForm');
        APIData.RequestDelegate.DelegateRequests(stepIDs, delegatedDomain, null, null, function (res) {
            if (res.code == AMResponseCode.ok) {
                alertResult('user-page:delegate-manual:btnDelegate', true);
                loadRequest();
            } else {
                alertResult('user-page:delegate-manual:btnDelegate', false);
            }
        });
    };

    function validDelegateData(delegatedDomain, stepIDs) {
        if(!delegatedDomain)
            return i18n('valid:notNull', [i18n('user-page:delegate-manual:delegatedDomain')]);

        if(!stepIDs || !stepIDs.length)
            return i18n('valid:atLeastOne', [i18n('user-page:delegate-manual:request')]);

        return null;
    }

    function initRequestsLayout() {
        var lstItem = $($('.request-list>li')[0]);
        lstItem.append('<input data-bind="event:{change: $root.requestCheckChange}, checked: $data._checked" ' +
            'class="nolabel request-checkbox" component="checkbox"' +
            ' type="checkbox">');
    }

    function filterRequest() {
        if(!me.requests.rawData)
            return;

        var shows = [];
        var ownerDomain = me.Domain();
        var fnStepFilter = null;
        switch (me.filter()) {
            case 0:
                fnStepFilter = function (step) {
                    return step.IsApproved === null && step.Owner == ownerDomain && step.AssignedOwner == ownerDomain;};
                break;
            case 1:
                fnStepFilter = function (step) {
                    return step.IsApproved === null && step.Owner != ownerDomain && step.AssignedOwner == ownerDomain;};
                break;
            case 2:
                fnStepFilter = function (step) {
                    return step.IsApproved === null;};
                break;
        }
        me.requests.rawData.forEach(function (request) {
            var matchedStep = request.Steps.find(fnStepFilter);
            if(matchedStep)
                shows.push(request);
        });

        me.requests(shows);
    }

    function loadRequest() {
        me.requests([]);
        me.isDataReady(false);
        var agrs = {
            pageIndex: 0,
            limit: 500,
            isOwned: true,
            requestStatus: 'open',
            assignedPersonInFlow: me.Domain()
        };
        APIData.APIRequestFilter.filter(agrs, function (resInJson) {
            if (!resInJson)
                Component.System.alert.show(i18n('alert:load-failed'));

            var res = JSON.parse(resInJson);
            var ownerDomain = me.Domain();
            var delegateRequests = [];
            res.Result.forEach(function (request) {
                var assignedStep = request.Steps.find(function (step) {
                    var isProcessed = step.IsApproved || step.IsApproved === 0 || step.IsApproved === '0';
                    if (isProcessed)
                        return false;

                    return true;
                });

                if (assignedStep)
                    delegateRequests.push(request);
            });

            me.isNoRequest(delegateRequests.length == 0);
            me.requests.rawData = delegateRequests;
            filterRequest();
            me.isDataReady(true);
        });
    }

    function setDisplayName(attrName, domain) {
        if (domain) {
            APIData.CMDB.GetUserFullName(domain, function (res) {
                if (!res)
                    return;

                var fullName = JSON.parse(res);
                if (!fullName)
                    return;

                $('.VNGSearch.form-control[name="' + attrName + '"]').val(fullName);
            });
        } else {
            $('.VNGSearch.form-control[name="' + attrName + '"]').val('');
        }
    }

    function alertResult(taskName, isSuccess) {
        var i18nKey = isSuccess ? 'alert:success' : 'alert:failed';
        var mess = i18n(i18nKey, [i18n(taskName)]);

        Component.System.alert.show(mess);
    }

    me.Domain.subscribe(loadRequest);
    me.filter.subscribe(filterRequest);
    setDisplayName('Domain', wp_current_user_domain);

    initRequestsLayout();
    loadRequest();
    APIData.RequestDelegate.CanSettingForAnotherUser(function (res) {
        if (res.code == AMResponseCode.ok) {
            me.canSettingForAnotherUser(res.data);
        }
    });
    APIData.UserRoles.GetAllRole(null, 0, 111, function(resInJson){
        if(!resInJson){
            Component.System.alert.show('alert:load-failed');
            return;
        }

        var res = JSON.parse(resInJson);
        if(!res){
            Component.System.alert.show('alert:load-failed');
            return;
        }

        roles = res;
    });
};
