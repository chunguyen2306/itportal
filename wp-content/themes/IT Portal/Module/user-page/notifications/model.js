Module.Model = function () {
    var me = this;
    this.list = ko.observableArray();
    this.isShowDelete = ko.observable(true);
    this.pendingMe = ko.observable(true);
    this.orderLink = '/user-page/order/detail?';
    this.searchType = 'all';
    var currentPage = 1;
    var pageCount = 0;
    var totalOrder = 0;
    var isRead = null;
    const LIMIT_ASSETS_IN_PAGE = 9;

    var load_order_in_page = function (typeID) {
        var index = (currentPage - 1) * LIMIT_ASSETS_IN_PAGE;
        var limit = LIMIT_ASSETS_IN_PAGE;

        APIData.Notification.GetList(typeID, isRead, index, limit, function (res) {
            if (res.code == AMResponseCode.ok) {
                me.list(res.data);
                render_paging();
            } else {
                Component.System.alert.show('Xảy ra lỗi trong quá trình lấy dữ liệu.' +
                    'Vui lòng nhấn F5 để tải lại trang hoặc liên hệ IT Helpdesk để được hỗ trợ.', {
                    name: 'Đóng',
                    cb: function () {
                    }
                });
            }
        });
        update_quantity_notification();
    }

    function loadOrder(typeID) {
        APIData.Notification.GetQuantity(typeID, isRead, function (res) {
            if (res.code == AMResponseCode.ok) {
                totalOrder = res.data;
            }
            currentPage = 1;
            load_order_in_page(typeID, isRead);
        });
    }

    this.filterControl_OnClick = function (model, e) {
        var allControl = $(e.currentTarget).parent().find('a');
        allControl.removeClass('active');
        $(e.currentTarget).addClass('active');

        me.searchType = $(e.currentTarget).attr('searchType');
        switch (me.searchType) {
            case 'read':
                me.isShowDelete(false);
                isRead = 1;
                break;
            case 'pending':
                me.isShowDelete(true);
                isRead = 0;
                break;
            default:
                me.isShowDelete(true);
                isRead = null;
        }
        loadOrder();
    };

    var do_PageChange = function (paging) {
        currentPage = paging.currentPage();
        load_order_in_page();
        $('html').animate({scrollTop: 0}, 500);
        var home = $('[section="home"]');
        home.removeClass('toggle');
    };

    var render_paging = function () {
        pageCount = Math.floor(totalOrder / LIMIT_ASSETS_IN_PAGE);
        if (totalOrder % LIMIT_ASSETS_IN_PAGE !== 0) {
            pageCount = pageCount + 1;
        }
        Component.Custom.Paging.items['infoPaging'].render(pageCount, LIMIT_ASSETS_IN_PAGE, totalOrder, currentPage);
        Component.Custom.Paging.items['infoPaging'].on_PageChange = do_PageChange;
    };

    this.detailMouseDown = function (data, e) {
        if (e.button != 0 && e.button != 1) // left and mid
            return;
        me.maskAsRead(data);
    }

    var removeAll = function () {
        var ids = [];
        APIData.Notification.GetList(null, isRead, null, null, function (res) {
            if (res.code == AMResponseCode.ok) {
                var allOrder = res.data.slice();
                allOrder.forEach(function (item) {
                    ids.push(item.ID);
                });
                APIData.Notification.DeleteNotification(ids, function (res) {
                    if (res.code == AMResponseCode.ok) {
                        me.list([]);
                        update_quantity_notification();
                    } else {
                        Component.System.alert.show('Xảy ra lỗi trong quá trình xoá dữ liệu. ' +
                            'Vui lòng liên hệ IT Helpdesk để được hỗ trợ.', {
                            name: 'Đóng',
                            cb: function () {
                            }
                        });
                    }
                })
            }
        });
    };

    this.remove_all = function () {
        if (me.list().length == 0) {
            Component.System.alert.show('Bạn chưa có thông báo nào', {
                name: 'Đóng',
                cb: function () {
                }
            });
            return;
        } else {
            Component.System.alert.show("Bạn có thực sự muốn xóa tất cả thông báo này?",
                {
                    name: "Xóa",
                    cb: function () {
                        removeAll();
                    }
                },
                {
                    name: "Đóng"
                }
            );
        }

    };

    this.remove = function (data) {
        Component.System.alert.show("Bạn có thực sự muốn xóa thông báo này?",
            {
                name: "Xóa",
                cb: function () {
                    APIData.Notification.DeleteNotification([data.ID], function (res) {
                        if (res.code == AMResponseCode.ok) {
                            me.list.remove(data);
                            loadOrder();
                        } else {
                            Component.System.alert.show('Xảy ra lỗi trong quá trình xoá dữ liệu. ' +
                                'Vui lòng liên hệ IT Helpdesk để được hỗ trợ.', {
                                name: 'Đóng',
                                cb: function () {
                                }
                            });
                        }
                    });
                }
            },
            {
                name: "Hoàn tác"
            }
        );
    };

    this.maskAsRead = function (data) {
        APIData.Notification.MarkAsRead([data.ID], true, function (res) {
            if (res.code == AMResponseCode.ok) {
                data.IsRead = true;
                me.list.update(data);
                update_quantity_notification();
            } else {
            }
        });
    };

    this.maskAllAsRead = function () {
        var ids = [];
        APIData.Notification.GetList(null, isRead, null, null, function (res) {
            if (res.code == AMResponseCode.ok) {
                var allOrder = res.data.slice();
                allOrder.forEach(function (item) {
                    ids.push(item.ID);
                });
                if (me.list().length == 0) {
                    Component.System.alert.show('Bạn chưa có thông báo nào', {
                        name: 'Đóng',
                        cb: function () {
                        }
                    });
                    return;
                } else {
                    if (ids.length == 0) {
                        Component.System.alert.show('Tất cả thông báo đã đánh dấu đọc', {
                            name: 'Đóng',
                            cb: function () {
                            }
                        });
                        return;
                    } else {
                        APIData.Notification.MarkAsRead(ids, true, function (res) {
                            if (res.code == AMResponseCode.ok) {
                                load_order_in_page();
                            } else {
                            }
                        });
                    }
                }
            }
        });
    };

    var update_quantity_notification = function () {
        APIData.Notification.GetQuantity(null, null, function (res) {
            if (res.code == AMResponseCode.ok) {
                $("#all_quantity").text(res.data);
            } else {
                $("#all_quantity").text('...');
            }
        });
        APIData.Notification.GetQuantity(null, 1, function (resIsRead) {
            if (resIsRead.code == AMResponseCode.ok) {
                $("#read_quantity").text(resIsRead.data);
            } else {
                $("#read_quantity").text('...');
            }
        });
        APIData.Notification.GetQuantity(null, 0, function (resIsPending) {
            if (resIsPending.code == AMResponseCode.ok) {
                $("#pending_quantity").text(resIsPending.data);
            } else {
                $("#pending_quantity").text('...');
            }
        });
    }

    var init = function () {
        me.searchKeyword = 'pending';
        var params = Common.getUrlParameters();

        if (params['pending-me'] === '') {
            me.pendingMe(false);
        } else {
        }
        loadOrder();
    }

    init();

    // signalr setup
    $(function () {
        function onGet(data) {
            me.list.splice(0, 0, data);
        }

        var singalrMethod_HasNew = 1;
        var sr = SignalREngine.getDefault();
        sr.onGet(singalrMethod_HasNew, onGet);
    });

};


// function model(){
//
//     var maxDuration = 100; // 100ms
//     var input = "";
//     var lastTime = null;
//
//     function getTimeNow() {
//         return (new Date()).getTime();
//     }
//
//     function clearInput() {
//
//     }
//
//     function sendInput() {
//
//     }
//
//     $(windown).on('input', function(e){
//         if(input){
//             var nowTime = getTimeNow();
//             var duration = nowTime - lastTime;
//
//             if(duration > maxDuration){
//                 clearInput();
//                 return;
//             }
//
//             if(e.key == "ENTER"){
//                 sendInput();
//                 return;
//             }
//
//         }
//
//         lastTime = getTimeNow();
//         input  = input + e.key;
//     });
//
// }