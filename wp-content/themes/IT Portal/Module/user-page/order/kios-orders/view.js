Module.View = function (model, _onDone) {
    var me = this;
    Component.Module.AbstractModuleView.call(this, model,_onDone );

    $('#txbUserAccount').on('keypress', function (e) {
        if (e.which === 13) {
            model.search_request();
        }
    });

    $('#txbUserAccount').on('keyup', function (e) {
        if (e.which === 8 && $("#txbUserAccount").val() == '') {
            model.search_request();
        }
    });

    me.onDone();
};