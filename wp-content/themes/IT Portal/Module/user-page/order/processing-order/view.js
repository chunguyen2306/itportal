Module.View = function (model, _onDone) {
    var me = this;
    Component.Module.AbstractModuleView.call(this, model,_onDone );

    $('#txbUserAccount').on('keypress', model.Keyword_OnKeyUp);

    $('#txbUserAccount').on('change', function () {
        if ($('#txbUserAccount').val() === '') {
            model.search_request();
        }

    });

    me.onDone();
};