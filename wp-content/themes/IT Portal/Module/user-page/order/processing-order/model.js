Module.Model = function () {
    var me = this;
    this.listRequestDefined = ko.observableArray([]);
    this.listRequestStatus = ko.observableArray([]);
    this.listRequest = ko.observableArray([]);
    this.Checked = ko.observableArray([]);
    this.requestStatusIDSelected = ko.observable(Utils.getURLValue("statusID"));
    this.listRequestStatusID = ko.observableArray();
    this.requestLink = "/user-page/order/detail?";
    this.pendingMe = ko.observable(true);
    var listRequest = [];
    var currentPage = 0;
    var pageCount = 0;
    this.totalRequest = ko.observable(0);
    const limitRequestInPage = 10;
    const IDSeeAllCheckBox = 0;
    var RequestStatusID = APIData.Request.RequestStatusID;
    this.searchType;
    this.searchKeyword = ko.observable();
    this.searchKeywordTbx = '';
    this.requestFilter = '';
    this.requestStatusFilter = '';
    this.pendingFilterRequest = '';
    this.pendingToFilterRequest = '';
    this.isOnRemoveFilter = ko.observable(false);

    this.do_InitComponent = function () {
        Component.Custom.ComboBox.bindingItem();
    };

    this.getPageResult = function () {
        //(currentPage * limitRequestInPage) + me.listRequest().length
        return "Bạn đang xem " + "{0}/{1}".format(me.listRequest().length, me.totalRequest()) + " kết quả";
    };

    var getRequestByStatusID = function (requests, requestStatusID) {
        var currentListRequest = [];
        for (var i = 0; i < requests.length; i++) {
            for (var j = 0; j < me.listRequestStatusID().length; j++) {
                if (requests[i].RequestStatusID == me.listRequestStatusID()[j].ID) {
                    requests[i].RequestStatusDisplayName = me.listRequestStatusID()[j].DisplayName;
                }
            }
        }
        if (requestStatusID == IDSeeAllCheckBox) {
            return requests;
        } else {
            for (var i = 0; i < requests.length; i++) {
                if (requestStatusID == requests[i].RequestStatusID) {
                    currentListRequest.push(requests[i]);
                }
            }
        }
        return currentListRequest;
    };

    this.search_request = function () {
        //$('.head-filter a').removeClass('active');
        me.searchKeywordTbx = $("#txbUserAccount").val();
        me.searchType = 'search';

        if (me.searchKeywordTbx === '') {
            $('.head-filter a[st="pending"]').click();
        }

        currentPage = 0;
        if (me.searchKeywordTbx !== '' && me.searchKeywordTbx !== null && me.searchKeywordTbx !== undefined) {
            loadRequests();
        }
        if (me.searchKeywordTbx=== '') {
            loadRequests();
        }
    };

    this.do_FilterRequest = function () {
        Component.Custom.Popup.items['popupFilter'].Popup.show();
    };

    this.pendingFilterRequest_OnClick = function (model, e) {
        loadRequests();
    };

    this.filterRequest_Apply = function (model, e) {
        if (me.requestStatusFilter !== undefined) {
            if (!Array.isArray(me.requestStatusFilter)) {
                me.requestStatusFilter = me.requestStatusFilter;
            }
        } else {
            me.requestStatusFilter = [];
        }
        if (me.requestFilter !== 0) {
            if (!Array.isArray(me.requestFilter)) {
                me.requestFilter = [me.requestFilter];
            }
        } else {
            me.requestFilter = [];
        }
        loadRequests();
        Component.Custom.Popup.items['popupFilter'].Popup.hide();
    };

    this.filterControl_OnClick = function (model, e) {
        $("#txbUserAccount").val('');
        var allControl = $(e.currentTarget).parent().find('a');
        allControl.removeClass('active');
        $(e.currentTarget).addClass('active');
        me.searchType = $(e.currentTarget).attr('st');
        me.searchKeyword($(e.currentTarget).attr('sk'));
        loadRequests();
    };

    function loadRequests(limit, callback) {
        Component.System.loading.show();
        var agrs = {
            pageIndex: currentPage,
            limit: limit || limitRequestInPage,
            isOwned: true
        };

        /*if (me.searchType == 'pending') {
            agrs['pendingAt'] = '{{me}}';
            agrs['requestStatus'] = 'open';
        } else */

        if (me.searchType == 'search') {
            // Search theo ID
            if (me.searchKeywordTbx.indexOf('#') == 0) {
                me.searchKeywordTbx = me.searchKeywordTbx.substr(1);
                agrs['requestID'] = me.searchKeywordTbx;
            }
            // Search khac
            else {
                agrs['assetname'] = me.searchKeywordTbx;
                agrs['assetType'] = me.searchKeywordTbx;
                agrs['assetModel'] = me.searchKeywordTbx;
                agrs['personInFlow'] = me.searchKeywordTbx;
                agrs['filterType'] = 'OR';
            }
        }
        if (me.searchKeyword() === 'approved') {
            //viet lai

            agrs['personInFlow'] = '{{me}}';
            agrs['isApproved'] = 1;
            //agrs['requestStatus'] = me.searchKeyword;
            //agrs['requestOwner'] = me._parent.currentLoginUserDomain;
        } else {
            agrs['pendingAt'] = '{{me}}';
            agrs['requestStatus'] = 'open';
        }

        if (Array.isArray(me.requestFilter) && me.requestFilter.length > 0) {
            agrs['requests'] = me.requestFilter;
        }

        if (me.requestStatusFilter !== undefined && me.requestStatusFilter !== null && me.requestStatusFilter != "") {
            agrs['isOwned'] = false;
            agrs['stepStatus__AND'] = me.requestStatusFilter;
        }

        if (me.pendingFilterRequest !== "" && me.pendingFilterRequest !== undefined && me.pendingFilterRequest !== null) {
            agrs['pendingTime'] = [
                me.pendingFilterRequest + " 00:00:00",
                (me.pendingToFilterRequest != '') ? me.pendingToFilterRequest + " 23:59:59" : me.pendingFilterRequest + " 23:59:59"
            ]
        }

        if (Object.keys(agrs).length > 5) {
            me.isOnRemoveFilter(true);
        } else {
            me.isOnRemoveFilter(false);
        }

        APIData.APIRequestFilter.filter(agrs, function (res) {
            Component.System.loading.hide();
            res = JSON.parse(res);
            if (limit === undefined) {
                pageCount = res.PageCount;
                me.totalRequest(res.Total);
                me.listRequest([]);
                me.listRequest(res.Result);
                render_paging();
                Component.Custom.Paging.items['requestsPaging'].on_PageChange = do_PageChange;
            } else {
                listRequest = res.Result.slice();
                exportExcel();
            }

        });
    }

    this.Keyword_OnKeyUp = function (e) {
        if (e.key === 'Enter') {
            me.search_request();
        }
    };

    var do_PageChange = function (paging) {
        currentPage = paging.currentPage() - 1;
        loadRequests();
        $('html').animate({scrollTop: 0}, 500);
        var home = $('[section="home"]');
        home.removeClass('toggle');
    };

    var render_paging = function () {
        Component.Custom.Paging.items['requestsPaging'].render(pageCount, limitRequestInPage, me.totalRequest(), currentPage + 1);
    };

    this.chk_real_time = function (model, e) {
        if (e.currentTarget.checked) {
            setTimeout(function () {
                loadRequests();
                me.chk_real_time(model, e);
            }, 30000);
        } else {
        }
    }

    this.remove_filter = function () {
        location.reload();
    }

    var init = function () {
        me.Checked(['0']);
        me.searchType = 'pending';
        me.searchKeyword('not_approve');
        APIData.Request.GetListRequestStatus(function (res) {
            res.data.splice(0, 0, {
                ID: 0,
                DisplayName: '---'
            });
            me.listRequestStatus(res.data);
            Component.Custom.ComboBox.bindingItem();
        });

        APIData.Request.GetListRequest('', 0, 0, function (res) {
            res.splice(0, 0, {
                ID: 0,
                DisplayName: '---'
            });
            me.listRequestDefined(res);
            Component.Custom.ComboBox.bindingItem();
        });

        APIData.Request.GetListRequestStatus(function (res) {
            if (res.code == AMResponseCode.ok) {
                me.listRequestStatusID(res.data);
                loadRequests();
            } else {
                if (location.hostname == 'itportal') {
                    Popup.error(res);
                }
            }
        });
    };

    init();

    var exportExcel = function () {
        var data = [];
        for (var i = 0; i < listRequest.length; i++) {
            var asset = Libs.Workflow.getTableData(listRequest[i], false);
            asset.forEach(function (elm) {
                var order = {
                    "Đơn hàng": listRequest[i].ID,
                    "Loại đơn hàng": listRequest[i].Definition.DisplayName.toString(),
                    "Ngày tạo": listRequest[i].CreateDate,
                    "Loại sản phẩm": elm.AssetType,
                    "Tên sản phẩm": elm.AssetModel,
                    "Mã sản phẩm": elm.AssetName,
                    "Số Lượng": elm.Count
                };
                data.push(order);
            });
        }
        if (data == '')
            return;
        var datatable = $('#exporttoExcel>table');
        datatable.empty();
        datatable = $('#exporttoExcel>table')[0];
        buildHtmlTable(datatable, data);

        setTimeout(function () {
            exportTableToExcel(datatable, 'Danh sách đơn hàng');
        }, 1);
    }

    this.ExportExcel_OnClick = function () {
        loadRequests(-1);

    };

    function exportTableToExcel(table, filename) {
        var downloadLink;
        var dataType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        var tableHTML = table.outerHTML.replace(/ /g, '%20');

        // Specify file name
        filename = filename ? filename + '.xls' : 'excel_data.xls';

        // Create download link element
        downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);

        if (navigator.msSaveOrOpenBlob) {
            var blob = new Blob(['\ufeff', tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob(blob, filename);
        } else {
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

            // Setting the file name
            downloadLink.download = filename;

            //triggering the function
            downloadLink.click();
        }
    };

    function buildHtmlTable(selector, myList) {

        var columns = addAllColumnHeaders(myList, selector);

        var excel = $(selector);

        for (var i = 0; i < myList.length; i++) {
            var row$ = $('<tr/>');
            for (var colIndex = 0; colIndex < columns.length; colIndex++) {
                var cellValue = myList[i][columns[colIndex]];
                if (cellValue == null) cellValue = "";
                row$.append($('<td/>').html(cellValue));
            }
            excel.append(row$);
        }
    };

    function addAllColumnHeaders(myList, selector) {
        var columnSet = [];
        var headerTr$ = $('<tr/>');

        for (var i = 0; i < myList.length; i++) {
            var rowHash = myList[i];
            for (var key in rowHash) {
                if ($.inArray(key, columnSet) == -1) {
                    columnSet.push(key);
                    headerTr$.append($('<th/>').html(key));
                }
            }
        }
        $(selector).append(headerTr$);

        return columnSet;
    };


};