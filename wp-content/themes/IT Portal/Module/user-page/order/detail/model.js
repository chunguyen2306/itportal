kolog = kodebug = function (val) {
    console.log(val);
    return val;
};

function slideByMouseDrag(jo) {
    var dom = jo[0];
    var dx = 0;
    var preScroll = 0;
    var joWin = $(window);

    function mouseMove(e) {
        if (dx == 0)
            return;

        var scrollValue = preScroll - (e.screenX - dx);

        if (scrollValue <= 0) {
            if (dom.scrollLeft !== 0)
                dom.scrollLeft = 0;

            dx = e.screenX;
            preScroll = dom.scrollLeft;
        } else if (scrollValue >= dom.scrollWidth) {
            if (dom.scrollLeft !== dom.scrollWidth)
                dom.scrollLeft = dom.scrollWidth;

            dx = e.screenX;
            preScroll = dom.scrollLeft;
        } else {
            dom.scrollLeft = scrollValue;
        }
    }

    function mouseUp(e) {
        if (dx == 0)
            return;

        jo.removeClass('prevent-select-text');
        dx = 0;

        joWin.unbind("mousemove", mouseMove);
        joWin.unbind("mouseup", mouseUp);
    }

    jo.bind("mousedown", function (e) {
        if (e.currentTarget != dom)
            return;

        jo.addClass('prevent-select-text');
        dx = e.screenX;
        preScroll = dom.scrollLeft;


        joWin.bind("mousemove", mouseMove);
        joWin.bind("mouseup", mouseUp);
    });
}

function classFileUpload(path, name, allowMulti) {
    var f = this;
    var UploadList = [];
    var inprocessCount = 0;
    var uploadCount = 0;
    var haveFailed = false;
    var fileInput = $('<input type="file" style="display: none"/>');
    var camInput = null;

    if (allowMulti)
        fileInput.attr('multiple', '');

    path = 'upload.' + path;

    function removeFile(dir, fullName) {

    }

    function upload(file, name) {
        var uploadInfo = UploadList.find(function (item) {
            return item.file == file;
        });

        if (uploadInfo)
            return;

        uploadInfo = {
            file: file,
            url: null,
        };
        inprocessCount++;
        UploadList.push(uploadInfo);
        APIData.UploadFile(path, name, file, true, function (res) {

            if (UploadList.indexOf(uploadInfo) == -1) {
                removeFile(path, name)
            } else {
                //uploadInfo.url =  expectUrl;
                if (res.State)
                    uploadInfo.url = path.replace('upload', 'uploads').replace(/\./g, '/') + '/' + res.Result;
                else
                    haveFailed = true;

                if (uploadInfo.onUploadDone) {
                    uploadInfo.onUploadDone(uploadInfo, res);
                }
            }

            inprocessCount--;
            processAllUploadDone();
        });
        return uploadInfo;
    }

    function processAllUploadDone() {
        if (inprocessCount == 0 && f.waitUploadDone.list) {
            var list = f.waitUploadDone.list;
            for (var i = 0; i < list.length; i++)
                list[i](f, haveFailed);

            f.waitUploadDone.list = null;
            haveFailed = false;
        }
    }

    function onPickFiles(files, onUploadDone) {
        if (files == null || files.length == 0) {
            onUploadDone([]);
            return;
        }

        var newUploadInfos = [];
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var uploadInfo = upload(file, name + '-' + (uploadCount++).toString());

            if (uploadInfo)
                newUploadInfos.push(uploadInfo);
        }

        if (onUploadDone)
            onUploadDone(newUploadInfos);
        processAllUploadDone();
    }

    f.pickFiles = function (onUploadDone) {
        if (fileInput.parent().length == 0) {
            fileInput.on('change', function () {
                onPickFiles(fileInput[0].files, onUploadDone);
                fileInput.remove();
            });
            $('body').append(fileInput);
        }
        fileInput.click();
    };

    /**
     * @param type : 'photo' | 'video' . default is photo(png)
     */
    f.pickCam = function (onUploadDone) {
        if (camInput == null)
            camInput = new classCameraInput('videoinput');

        camInput.pickCam(function (camFiles) {
            onPickFiles(camFiles, onUploadDone);
        });
    }

    f.dropFile = function (uploadInfo) {
        Utils.removeFromArray(UploadList, uploadInfo);
    };

    f.commitkAll = function () {

    };

    f.rollbackAll = function () {
        //waitUploadDone.list = [];
    }

    f.waitUploadDone = function (callback) {
        if (inprocessCount < 1)
            return false;

        var list = f.waitUploadDone.list || (f.waitUploadDone.list = []);
        list.push(callback);
    };

    f.setName = function (newFileName) {
        name = newFileName;
    };

    f.clearSelected = function () {
        if (fileInput.parent().length)
            fileInput.val('');
    };

    $(window).on('unload', f.rollbackAll)
}

Module.Model = function () {
    var t = this;
    var ID = Utils.getURLValue("ID");
    var wfID = Utils.getURLValue("wfID");
    var wfName = Utils.getURLValue("wfName");
    var DataTypeID = APIData.Workflow.RequestDataTypeID;
    var requestData = t.requestData = null;
    var RoleID = APIData.Workflow.RoleID;
    var Tasks = APIData.Workflow.Tasks;
    var listRole = [];
    var permision = {
        cancel: false,
        delegate: false
    };
    var curUserRoleIDs = null;


    ID = ID ? parseInt(ID) : null;
    wfID = wfID ? parseInt(wfID) : null;

    t.TaskID = APIData.Workflow.TaskID;
    t.workflows = ko.observableArray();
    t.workflowID = ko.observable();
    t.workflowName = ko.observable();
    t.request = ko.observable();
    t.listFieldDefinition = ko.observableArray();
    t.listStepDefinition = ko.observableArray();
    t.listComment = ko.observableArray();
    t.taskData = ko.observable(); // bool value
    t.entrusting = ko.observable();
    t.commentRequesting = ko.observable();
    t.commentReplying = ko.observable();
    t.editAble = ko.observable();
    t.listToEntrust = [];
    t.RequestStatusID = APIData.Request.RequestStatusID;
    t.currentDomain = ko.observable();
    t.currentStepDefinition = ko.observable();
    t.currentEditable = {};
    t.showProcess = ko.observable(localStorage.order_showProcess ? Utils.jsonToData(localStorage.order_showProcess) : false);
    t.requestUser = ko.observable();
    t.history = ko.observableArray();
    t.showButton = {
        hideAll: ko.observable(),
        approves: ko.observable(),
        delegate: ko.observable(),
        raise: ko.observable(),
        reply: ko.observable(),
        comment: ko.observable(),
        requestInfo: ko.observable(),
        cancel: ko.observable(),
        reCreate: ko.observable(),
        pickup: ko.observable()
    };
    t.mentionUser = (function () {
        function getName(domain) {
            return domain;
        };

        var rSource = new ReferSource("", null, "user", APIData.CMDB.GetListUser, getName, 'AccountName', 'AccountName');

        var re = new MentionSearchMeta();
        re.listSource = [rSource];
        re.defaultSource = rSource;
        return re;
    })();
    t.task = new function () {
        var tt = this;
        var uploader = null;
        var joAlert = null;
        var joBlank = null;

        function showTaskGUI() {
            var taskData = t.taskData();

            function onSubmitDone(res) {
                try {
                    var handled = taskData.onSubmitDone(res);

                    if (res && res.code == AMResponseCode.ok)
                        tt.attachmentInfos([]);

                    if (handled)
                        return;

                    var task = Tasks.find(function (item) {
                        return item.id == taskData.taskID;
                    });
                    var taskName = task ? task.name : '';
                    switch (res.code) {
                        case AMResponseCode.ok:
                            alertTaskResult(taskName, true);
                            break;

                        case AMResponseCode.denied:
                            Component.System.alert.show("Bạn không thể " + taskName.toLowerCase() + " lúc này");
                            break;
                        default:
                            alertTaskResult(taskName, false);
                            break;
                    }
                } finally {
                    Component.System.loading.hide();
                }
            }

            function submitTask_AfterUpload() {
                var attachments = [];
                var attachmentNameFaileds = [];
                tt.attachmentInfos().forEach(function (item) {
                    var attachmentName = item.attachment.name.trim();
                    if (attachmentName.endsWith('.')) {
                        attachmentName = attachmentName.substr(0, attachmentName.length - 1);
                        item.attachment.name = attachmentName;
                    }

                    if (item.url)
                        attachments.push(item.attachment);
                    else
                        attachmentNameFaileds.push(attachmentName);

                    var extension = item.file.name.trim().match(/\.\w+$/)[0];
                    if (extension && attachmentName.endsWith(extension) == false) {
                        attachmentName += extension;
                        item.attachment.name = attachmentName;
                    }

                });

                if (attachmentNameFaileds.length) {
                    Component.System.alert.show(
                        "Vui lòng xóa các đính kèm tải lên không thành công: <br> &nbsp; &nbsp; "
                        + attachmentNameFaileds.join("<br> &nbsp; &nbsp; "),
                        {
                            name: 'Đóng',
                            cb: function () {
                                t.taskData(taskData);
                            }
                        }
                    );

                    Component.System.loading.hide();
                    return;
                }

                taskData.attachments = attachments;
                taskData.fnSubmit(onSubmitDone);
            }

            function submitTask() {
                t.taskData(null);

                if (taskData.fnCheck) {
                    var errorStr = taskData.fnCheck();
                    if (errorStr) {
                        Component.System.alert.show(
                            errorStr,
                            {
                                name: 'Đóng',
                                cb: function () {
                                    t.taskData(taskData);
                                }
                            }
                        );
                        return;
                    }
                }

                Component.System.loading.show();
                if (uploader.waitUploadDone(submitTask_AfterUpload) == false)
                    submitTask_AfterUpload();
            }

            function cancelTask() {
                t.taskData(null);
            }

            Component.System.alert.show(
                $("#task-gui")[0],
                {
                    name: taskData.btnTextSubmit || 'Đồng ý',
                    cb: submitTask
                }, {
                    name: taskData.btnTextCancel || 'Xem lại',
                    cb: cancelTask
                }
            );
        }

        function initUploader(taskID) {
            var request = t.request();
            var stepDef = t.currentStepDefinition();
            if (stepDef == null || request == null || requestData.isDisposed()) {
                uploader = null;
                return;
            }

            var time = new Date().getTime();
            uploader = new classFileUpload('workflow.request.' + request.ID, 's' + stepDef.step.ID + '-t' + taskID + '-' + time, true);
        }

        function onFileSelected(uploadInfos) {
            uploadInfos.forEach(function (uploadInfo) {
                var attachment = {
                    name: uploadInfo.file.name,
                    url: null
                };

                uploadInfo.attachment = attachment;
                uploadInfo.status = 'wating';
                uploadInfo.onUploadDone = function (uploadInfo, res) {
                    switch (res.Code) {
                        case 200:
                            attachment.url = uploadInfo.url;
                            uploadInfo.status = 'success';
                            break;
                        case 494:
                            uploadInfo.status = 'error';
                            uploadInfo.errorMessage = 'Tập tin lớn hơn 10Mb';
                            break;
                        case 405:
                            uploadInfo.status = 'error';
                            uploadInfo.errorMessage = 'Loại tập tin bị chặn';
                            break;
                        default:
                            uploadInfo.status = 'error';
                            uploadInfo.errorMessage = 'Tải lên không thành công';
                            break;
                    }

                    tt.attachmentInfos.update(uploadInfo);
                };

                tt.attachmentInfos.push(uploadInfo);
            });

            uploader.clearSelected();
        }

        tt.attachmentInfos = ko.observableArray();

        tt.pickFiles = function () {
            uploader.pickFiles(onFileSelected);
        };

        tt.pickCam = function () {
            if (joAlert == null) {
                joAlert = $('.sys-alert');
                joBlank = $('.sys-blank');
            }

            joAlert.classes = joAlert.attr('class');
            joBlank.display = joBlank[0].style.display;
            joAlert.removeClass('show');

            uploader.pickCam(function (uploadInfos) {
                setTimeout(function () {
                    joAlert.attr('class', joAlert.classes);
                    joBlank[0].style.display = joBlank.display;
                }, 1);
                onFileSelected(uploadInfos)
            });
        };

        tt.removeAttackment = function (data) {
            tt.attachmentInfos.remove(data);
        };

        t.taskData.subscribe(function (data) {
            if (data == null)
                return;

            initUploader(data.taskID);
            setTimeout(showTaskGUI, 1);
        });
    };
    t.download = function (attachment) {
        var url = attachment.url;
        if (url.startsWith('/') == false && url.startsWith('https:') == false && url.startsWith('http:') == false)
            url = '/' + url;

        url = url.replace('uploads/', '').replace(/\//g, '.');
        var ext = url.substring(url.lastIndexOf('.') + 1);
        url = url.substring(0, url.lastIndexOf('.'));

        url = '/download/?i=' + encodeURI(url) + '&e=' + ext + '&n=' + attachment.name;

        var link = $('<a download/>');
        //link.attr('download',  attachment.name);
        link.attr('href', url);
        $('body').append(link);
        link[0].click();
        link.remove();
    };
    t.attachmentMouseDown = function (data, e) {
        if (e.button == 0) {
            if (data.name.endsWith('.jpg') || data.name.endsWith('.png') || data.name.endsWith('.jpeg') || data.name.endsWith('.bmp')) {

                Component.System.alert.show(
                    '<img style="width: 100%;" src="/wp-content/' + data.url + '">',
                    {
                        name: 'Đóng',
                        //
                        // cb: function () {
                        //     alert.css('width', '');
                        //     alert.css('left', '');
                        // }
                    }
                );
                //var alert = $('.sys-alert.show');
                // alert.css('width', '90%');
                // alert.css('left', '5%');
            } else {
                Component.System.alert.show(
                    'Tập tin này chưa được hỗ trợ xem trực tuyến. Bạn có thể download để xem',
                    {
                        name: 'Download',
                        cb: function () {
                            t.download(data);
                        }
                    },
                    {
                        name: 'Đóng',
                    }
                );
            }
        }
    };
    t.setSearchHandle = function (handle) {
        t.mentionUser.searchHandle = handle;
    };
    t.selectWorkflow = function (wf) {
        t.workflowName(wf.DisplayName);
        t.workflowID(wf.ID);
    };
    t.createRequest = function () {

        if (checkFieldData() == false)
            return;

        function createRequest() {
            Component.System.loading.show();
            requestData.createRequest(function (res) {
                Component.System.loading.hide();
                t.createRequest.isProcessing = 0;
                if (res.code == AMResponseCode.ok) {
                    alertSubmitOrderResult('Đặt hàng', true);
                    wfID = null;
                    ID = res.data.request.ID;
                    loadRequestData();
                    Utils.setURLValue("ID", ID);
                } else if (res.code == AMResponseCode.denied) {
                    alertSubmitOrderResultDenied();
                } else {
                    alertSubmitOrderResult('Đặt hàng', false);
                }
            });
        }

        var jo = $('<strong>Bạn đồng ý đặt hàng với các sản phẩm đã chọn?</strong>');
        Component.System.alert.show(jo,
            {
                name: "Đồng ý đặt hàng", cb: createRequest
            },
            {
                name: "Xem lại"
            }
        )

    };
    t.reCreate = function () {
        if (checkFieldData() == false)
            return;

        function reCreateRequest() {
            Component.System.loading.show();
            var field = requestData.getField();
            APIData.Request.ApproveRequestStep(t.request().ID, 0, true, null, null, field, function (res) {
                Component.System.loading.hide();
                if (res.code == AMResponseCode.ok) {
                    alertSubmitOrderResult('Đặt hàng', true);
                    t.entrusting(null);
                    APIData.Request.GetRequestEditData(ID, loadDataDone);
                } else if (res.code == AMResponseCode.denied) {
                    alertSubmitOrderResultDenied();
                } else {
                    alertSubmitOrderResult('Đặt hàng', false);
                }
            });
        }

        var jo = $('<strong>Bạn đồng ý đặt hàng với các sản phẩm đã chọn?</strong>');
        Component.System.alert.show(jo,
            {
                name: "Đồng ý đặt hàng", cb: reCreateRequest
            },
            {
                name: "Xem lại"
            }
        );
    };
    t.approve = function () {
        if (checkFieldData() == false)
            return;

        var stepDef = t.currentStepDefinition();
        var approving = {
            taskID: t.TaskID.approve,
            title: 'Bạn đang duyệt đồng ý cho đơn hàng',
            placeholder: "Nội dung ghi chú",
            fnCheck: function () {
                return null;
            },
            fnSubmit: function (onSubmitDone) {
                var field = requestData.getField();
                APIData.Request.ApproveRequestStep(
                    t.request().ID,
                    stepDef.StepIndex,
                    true,
                    approving.comment,
                    approving.attachments,
                    field,
                    onSubmitDone
                );
            },
            onSubmitDone: function (res) {
                var taskName = stepDef.DisplayName;
                if (res.code == AMResponseCode.ok) {
                    alertTaskResult(taskName, true);
                    APIData.Request.GetRequestEditData(ID, loadDataDone);
                    return true;  // không xử lý thông báo cho task này
                } else if (res.code == AMResponseCode.denied) {
                    Component.System.alert("Bạn chưa có quyền " + taskName.toLowerCase() + " trên đơn hàng này");
                    return true;  // không xử lý thông báo cho task này
                } else {
                    alertTaskResult(taskName, false);
                    return true;  // không xử lý thông báo cho task này
                }
            }
        };
        t.taskData(approving);
    };
    t.reject = function () {
        var stepDef = t.currentStepDefinition();
        var approving = {
            taskID: t.TaskID.reject,
            title: "Bạn đang từ chối đơn hàng",
            placeholder: "Vui lòng cho biết lý do",
            fnCheck: function () {
                if (!approving.comment)
                    return 'Bạn vui lòng cho biết lý do từ chối';
            },
            fnSubmit: function (onSubmitDone) {
                APIData.Request.ApproveRequestStep(
                    t.request().ID,
                    stepDef.StepIndex,
                    false,
                    approving.comment,
                    approving.attachments,
                    null,
                    onSubmitDone
                )
                ;
            },
            onSubmitDone: function (res) {
                var taskName = 'Từ chối ' + stepDef.DisplayName.toLowerCase();
                if (res.code == AMResponseCode.ok) {
                    alertTaskResult(taskName, true);
                    APIData.Request.GetRequestEditData(ID, loadDataDone);
                    return true;  // không xử lý thông báo cho task này
                } else if (res.code != AMResponseCode.denied) {
                    alertTaskResult(taskName, false);
                    return true;  // không xử lý thông báo cho task này
                }
                // else if (res.code == AMResponseCode.denied) {
                //     Component.System.alert("Bạn chưa có quyền " + taskName.toLowerCase() + " trên đơn hàng này");
                //     return true;  // không xử lý thông báo cho task này
                // }
            },
            btnTextSubmit: 'Từ chối'
        };
        t.taskData(approving);
    };
    t.cancelRequest = function () {
        var taskData = {
            taskID: t.TaskID.cancel,
            title: "Bạn đang <b>HỦY</b> đơn hàng",
            placeholder: "Lý do hủy",
            fnCheck: function () {
                if (!taskData.comment)
                    return 'Bạn vui lòng cho biết lý do hủy đơn hàng';
            },
            fnSubmit: function (onSubmitDone) {
                APIData.Request.CancelRequest(t.request().ID, taskData.comment, taskData.attachments, onSubmitDone);
            },
            onSubmitDone: function (res) {
                if (res.code == AMResponseCode.ok) {
                    loadRequestData();
                }
            },
            btnTextSubmit: 'Hủy đơn hàng'
        };
        t.taskData(taskData);
    };
    t.pickupStepApproval = function () {
        var stepDef = t.currentStepDefinition();
        var taskData = {
            taskID: t.TaskID.pickup,
            title: "Bạn đang <b>nhận xử lý " + stepDef.DisplayName.toLowerCase() + "</b>",
            placeholder: "Ghi chú",
            fnCheck: function () {
            },
            fnSubmit: function (onSubmitDone) {
                var stepID = stepDef.step.ID;
                APIData.Request.PickupStepApproval(stepID, taskData.comment, taskData.attachments, onSubmitDone);
            },
            onSubmitDone: function (res) {
                if (res.code == AMResponseCode.ok) {
                    loadRequestData();
                }
            },
            btnTextSubmit: 'Hủy đơn hàng'
        };
        t.taskData(taskData);
    };
    t.raiseRequestStep = function () {
        var curStepDef = t.currentStepDefinition();
        var taskData = {
            taskID: t.TaskID.raise,
            title: "Bạn đang nhắc <b>" + curStepDef.step.Owner + "</b> xử lý đơn hàng",
            placeholder: "Nội dung ghi chú",
            fnCheck: function () {
                return null;
            },
            fnSubmit: function (onSubmitDone) {
                APIData.Request.RaiseRequestStep(curStepDef.step.ID, taskData.comment, taskData.attachments, onSubmitDone);
            },
            onSubmitDone: function (res) {
                if (res.code == AMResponseCode.ok) {
                    t.listComment.push(res.data);
                }
            },
            btnTextSubmit: 'Nhắc xử lý'
        };
        t.taskData(taskData);
    };
    t.entrust = function () {
        var curStepDef = t.currentStepDefinition();
        var taskData = {
            taskID: t.TaskID.entrust,
            title: "Chọn người bạn muốn ủy quyền",
            //placeholder: "Liên hệ IT Helpdesk nếu bạn muốn ủy quyền 1 người không có trong danh sách",
            taskID: t.TaskID.entrust,
            domain: t.listToEntrust.length == 1 ? t.listToEntrust[0] : null,
            fnCheck: function () {
                if (!taskData.domain)
                    return "Bạn chưa chọn người để ủy quyền";
            },
            fnSubmit: function (onSubmitDone) {
                var step = curStepDef.step;
                APIData.Request.EntrustRequestStep(step.ID, taskData.domain, taskData.comment, taskData.attachments, onSubmitDone);
            },
            onSubmitDone: function (res) {
                if (res.code == AMResponseCode.ok) {
                    Object.assign(curStepDef.step, res.data.step);
                    Utils.removeFromArray(t.listToEntrust, taskData.domain);
                    t.listToEntrust.push(t.currentDomain());
                    t.listStepDefinition.update(curStepDef);
                    t.listComment.push(res.data.comment);
                }
            },
            btnTextSubmit: 'Ủy quyền'
        };
        t.taskData(taskData);
    };
    t.requestComment = function () {
        var curStepDef = t.currentStepDefinition();
        var taskData = {
            taskID: t.TaskID.ask,
            title: "Thông tin",
            placeholder: 'Bạn cần mention ít nhất một người để yêu cầu họ cung cấp thông tin. ' +
            'Sử dụng @ trước tài khoản, sau đó gõ Enter, ví dụ @helpdesk',
            contentType: 'mention',
            fnCheck: function () {
                var domains = [];
                var content = Mention.parse(taskData.comment, function (data) {
                    domains.push(data.value);
                    return data.value;
                });
                if (domains.length == 0) {
                    return "Bạn cần mention ít nhất một người để yêu cầu họ cung cấp thông tin. Hãy sử dụng @ trước tài khoản, sau đó gõ Enter, ví dụ @helpdesk";
                }

                taskData.content = content;
                taskData.domains = domains;
            },
            fnSubmit: function (onSubmitDone) {
                var stepID = curStepDef.step.ID;
                APIData.Request.RequestComment(stepID, taskData.domains, taskData.content, taskData.attachments, onSubmitDone);
            },
            onSubmitDone: function (res) {
                if (res.code == AMResponseCode.ok) {
                    t.listComment.push(res.data);
                    alertTaskResult('Đã gởi yêu cầu thông tin', true);
                    return true;
                }
            },
            btnTextSubmit: 'Gởi'
        };
        t.taskData(taskData);
    };
    t.replyComment = function () {
        var curStepDef = t.currentStepDefinition();
        var asker = curStepDef.step.Owner.toUpperCase();
        var taskData = {
            taskID: t.TaskID.answer,
            title: "Trả lời " + "<b>" + asker + "</b>",
            placeholder: "Cho " + asker + " biết ý kiến của bạn",
            fnCheck: function () {
                if (!taskData.comment)
                    return 'Vui lòng không để trống nội dung';
            },
            fnSubmit: function (onSubmitDone) {
                APIData.Request.AnswerComment(curStepDef.step.ID, [], taskData.comment, taskData.attachments, onSubmitDone);
            },
            onSubmitDone: function (res) {
                var mes = 'Trả lời ' + asker;
                if (res.code == AMResponseCode.ok) {
                    t.listComment.push(res.data);
                    alertTaskResult(mes, true);
                    return true;
                } else {
                    alertTaskResult(mes, false);
                    return true;
                }
            },
            btnTextSubmit: 'Gởi câu trả lời'
        };
        t.taskData(taskData);
    };
    t.comment = function () {
        var curStepDef = t.currentStepDefinition();
        var taskData = {
            taskID: t.TaskID.comment,
            title: "Thêm ghi chú",
            placeholder: 'Sử dụng @ trước tài khoản, sau đó gõ Enter, ví dụ @helpdesk',
            contentType: 'mention',
            fnCheck: function () {
                var domains = [];
                var content = Mention.parse(taskData.comment, function (data) {
                    domains.push(data.value);
                    return data.value;
                });

                taskData.content = content;
                taskData.domains = domains;
            },
            fnSubmit: function (onSubmitDone) {
                var stepID = curStepDef.step.ID;
                APIData.Request.Comment(stepID, taskData.content, taskData.domains, taskData.attachments, onSubmitDone);
            },
            onSubmitDone: function (res) {
                if (res.code == AMResponseCode.ok) {
                    t.listComment.push(res.data);
                }
            },
            btnTextSubmit: 'Gởi'
        };
        t.taskData(taskData);
    };
    t.intendDurationInString = function (stepDefinition) {
        if (stepDefinition.StepIndex == 0)
            return '';

        if (stepDefinition.MinIntendDuration == 0)
            return "dự kiến vài phút";

        var msg = "dự kiến ";
        if (stepDefinition.MinIntendDuration == stepDefinition.MaxIntendDuration)
            msg = msg + stepDefinition.MinIntendDuration + " giờ";
        else
            msg = msg + stepDefinition.MinIntendDuration + "-" + stepDefinition.MaxIntendDuration + " giờ";
        return msg;
    };
    t.openingMessage = function () {
        var lastStep = requestData.listStepDefinition[requestData.listStepDefinition.length - 1];
        var time = new Date(lastStep.step.MinIntendTime.replace(" ", "T"));
        return "Yêu cầu dự kiến sẽ hoàn thành trước " + time.toLocaleString();
    };
    t.processShowChange = function () {
        var val = !t.showProcess();
        t.showProcess(val);
    };
    t.stepClass = function (stepDefinition, currentIndex) {
        if (stepDefinition.listSubRequest)
            return "workflow-step sub";

        if (requestData == null || requestData.request == null)
            return stepDefinition.StepIndex == 0 ? 'inprocess' : "";


        currentIndex || (currentIndex = requestData.request.CurrentIndex);
        var step = stepDefinition.step;
        if (stepDefinition.StepIndex == currentIndex && step.IsApproved === null) {
            if (requestData.request && requestData.isDisposed())
                return 'reject';
            else
                return "inprocess";
        }

        if (step == null)
            return "";

        var re = stepDefinition.isTargetScroll ? "target-scroll " : "";
        if (step.ActionDate)
            return re + (step.IsApproved ? "approved" : "reject");

        return re;
    };
    t.getNewStepDefinitionDisplayName = function (data) {
        var role = data.Role;
        if (role && isNaN(role))
            role = parseInt(role[0]);

        if (role == null || role == RoleID.user) {
            if (data.step.Owner == null)
                return data.DisplayName;

            return data.step.Owner.toUpperCase() + ' ' + data.DisplayName.toLowerCase();
        }

        var roleData = listRole.find(function (item) {
            return item.ID == role;
        });

        if (roleData)
            return roleData.Name + ' ' + data.DisplayName.toLowerCase();
        else
            return data.DisplayName;
    }
    t.getStepDefinitionStateName = function (data) {
        var isDisposed = requestData.request && requestData.isDisposed();
        if (data.step.ActionDate) {

            var actionName = data.step.IsApproved ? 'Đã ' : 'Từ chối ';
            return actionName + data.DisplayName.toLowerCase();
        } else {
            if (requestData.request && requestData.request.RequestStatusID == t.RequestStatusID.cancel) {
                if (requestData.request.CurrentIndex == data.StepIndex)
                    return 'Đã hủy';
            }

            if (isDisposed)
                return '';

            if (data.StepIndex == 0)
                return '';

            if (data.MaxIntendDuration == 0)
                return "Dự kiến vài phút";

            var msg = "Dự kiến " + APIData.Workflow.RangeOfTimeInString(data.MinIntendDuration, data.MaxIntendDuration).toLowerCase();
            return msg;
        }
    }
    t.getTaskName = function (comment) {
        try {
            var taskID = comment.EventID;
            var task = Tasks.find(function (item) {
                return item.id == taskID;
            });

            if (taskID == t.TaskID.entrust && !comment.Comment)
                comment.Comment = 'Ủy quyền cho ' + comment.Data.toUpperCase();

            return task.name;
        } catch (ex) {
            return '';
        }

    }
    t.getHistoryDefinitionDisplayName = function (comment) {
        try {

            var stepDefinition = requestData.listStepDefinition.find(function (item) {
                return item.step.ID == comment.StepID;
            });


            return stepDefinition.DisplayName;
        } catch (ex) {
            return '';
        }
    }

    function alertTaskResult(taskName, isSuccess, additionMes) {

        var mes = '<b>' + taskName + '</b>';
        mes += isSuccess ? ' thành công' : ' không thành công';

        if (additionMes)
            mes += '. ' + additionMes;

        if (!isSuccess)
            mes += '<br>Vui lòng kiểm tra lại hoặc liên hệ IT Helpdesk để được hỗ trợ.';

        Component.System.alert.show(
            mes,
            {
                name: 'Đóng'
            }
        );
    }

    function alertSubmitOrderResult(taskName, isSuccess) {
        var additionMes = null;
        if (!isSuccess)
            additionMes = 'Nguyên nhân có thể do: ' +
                '<br> &nbsp; &nbsp;- Loại sản phẩm hoặc tên sản phẩm không tồn tại' +
                '<br> &nbsp; &nbsp;- Số lượng đặt không hợp lệ';
        alertTaskResult(taskName, isSuccess, additionMes);
    }

    function alertSubmitOrderResultDenied() {
        var owner = m.requestData.getFieldDefinition(m.requestData.workflow.OwnerField);
        var mes = 'Việc đặt hàng cho người khác chỉ áp dụng cho IT Hepdesk, Admin team, Sếp trực tiếp và Trưởng phòng';
        Component.System.alert.show(mes);
    }

    function checkFieldData() {
        var check = requestData.checkData();
        if (check) {
            Component.System.alert.show(check);
            return false;
        } else {
            return true;
        }
    }

    $(window).bind("keydown", function (e) {
        if (e.keyCode == 27) {  //  ESC
            t.taskData() && t.cancelEntrust();
            t.entrusting() && t.cancelApprove();
        }
    });

    function setRequestData(data) {
        t.currentEditable = data.currentEditable;
        t.requestUser((function (user) {
            var match = user.domainAccout.match(/\d+$/);
            if (match)
                user.fullName = ru.fullName + " (" + match[0] + ")";

            return user;
        })(data.requestUser));
        t.request(data.request);
        t.workflowName(data.workflow.DisplayName);
        t.currentDomain(data.currentDomain);

        data.listFieldDefinition.forEach(function (item) {
            item.value = field[item.Name];
        });

        data.listStepDefinition.forEach(function (item) {
            var n = listStep.length;
            for (var i = 0; i < n; i++) {
                if (listStep[i].DefinitionID == item.ID) {
                    item.step = listStep[i];
                    break;
                }
            }

            if (item.step == null) {
                var val = new APIData.Workflow.RequestStepEntity();
                val.Owner = mapExpectedDomain[item.Role];
                val.DefinitionID = item.ID;
                if (item.StepIndex == 0) {
                    val.Owner = res.referData.currentDomain;
                }

                item.step = val;
            }
        });

        t.listFieldDefinition(data.listFieldDefinition);
        t.listStepDefinition(data.listStepDefinition);
    }

    function pushParentAssetField(requestData) {
        if (!requestData.currentStepAction || !requestData.currentStepAction.length)
            return;

        var addParrentAssetField = function (assetFieldName, fields) {
            var l = fields || requestData.listFieldDefinition;
            var n = l.length;
            var i = 0;
            for (; i < n; i++) {
                if (l[i].Name == assetFieldName)
                    break;

                if (l[i].listChild)
                    addParrentAssetField(assetFieldName, l[i].listChild);
            }

            if (i >= n)
                return;

            var assetField = l[i];
            var parentAssetField = {
                DisplayName: "Tài sản cha",
                //ID: 717,
                ///Index: null,
                //MappingName: "Field1",
                Name: "_action_asset_parent",
                ParentID: assetField.ParentID,
                RequestDataTypeID: DataTypeID.parrentAsset,
                RequestDefinitionID: assetField.RequestDefinitionID,
                cf: {
                    style: {
                        display: "",
                        float: "",
                        width: assetField.cf.width
                    },
                    // submitedStyle: {},
                    constrain: null
                }
            };
            l.splice(i + 1, 0, parentAssetField);
            requestData.currentFieldCan[parentAssetField.Name] = {
                view: 1,
                edit: 1,
                update: 1
            }

        };
        requestData.currentStepAction.forEach(function (action) {
            if (action.ActionTypeID != APIData.Workflow.ActionTypeID.associateAsset)
                return;

            Mention.parse(action.cf.list, function (refer) {
                var assetName = refer.value;

                if (assetName)
                    addParrentAssetField(assetName);
            });
        });
    }

    function loadDataDone(res) {
        if (res.code == AMResponseCode.ok) {
            listRole = res.referData.listRole;
            t.workflows(res.referData.listWorkflow);
            var searchMember = [{IsUnicode: true}];
            t.listToEntrust = res.referData.listDomainForEntrustment || [];

            requestData = t.requestData = new APIData.Request.RequestData(res.data);
            var request = requestData.request;
            if (ID == null) {
                requestData.setField();
                requestData.setListStep();
                var mapExpect = res.referData.mapExpectedDomain;
                requestData.listStepDefinition.forEach(function (stepDefinition) {
                    var owner = stepDefinition.Role == null ? requestData.currentDomain : mapExpect[stepDefinition.Role];
                    if (owner)
                        stepDefinition.step.Owner = owner;
                });
            }

            pushParentAssetField(requestData);

            requestData.currentFieldCan = (function () {
                var fieldCan = requestData.currentFieldCan;
                if (requestData.request) {
                    var isDisposed = requestData.isDisposed();

                    if (!isDisposed) {
                        var curDomain = requestData.currentDomain;
                        var currentStepDefinition = requestData.listStepDefinition[requestData.request.CurrentIndex];
                        if (curDomain == currentStepDefinition.step.Owner)
                            return fieldCan;
                    }
                } else {
                    return fieldCan;
                }

                var fieldCan = requestData.currentFieldCan;
                for (var pro in fieldCan) {
                    fieldCan[pro].edit = false;
                }

                return fieldCan;
            })();
            t.editAble = requestData.currentEditable;
            t.requestUser((function (user) {
                if (user == null)
                    return;

                var match = user.domainAccount.match(/\d+$/);
                if (match)
                    user.fullName = user.fullName + " (" + match[0] + ")";

                return user;
            })(res.referData.requestUser));
            t.request(request);
            t.workflowName(requestData.workflow.DisplayName);
            t.currentDomain(requestData.currentDomain);
            t.listStepDefinition((function (listStep, listSubRequest) {
                if (listStep == null || listStep.length == 0)
                    return [];

                function stepRoleIsSame(step1, step2) {
                    var r1 = step1.Role;
                    var r2 = step2.Role;

                    if (isNaN(r1) && isNaN(r2)) {
                        return r1.length > 1 && r2.length > 1 && r1[0] == r2[0];
                    } else {
                        return r1 == r2;
                    }
                }

                listSubRequest.forEach(function (subRequest) {
                    var ls = subRequest.listStepDefinition;
                    var i = 0;
                    while (ls.length && i < listStep.length) {
                        if (stepRoleIsSame(ls[0], listStep[i]))
                            ls.shift();
                        else
                            break;
                    }

                });

                if (listSubRequest && listSubRequest.length) {
                    listStep.push({
                        listSubRequest: listSubRequest
                    });
                }

                return listStep;
            })(requestData.listStepDefinition, res.referData.subRequests));
            t.listFieldDefinition((function (list) {
                list.forEach(function (item) {
                    if (item.RequestDataTypeID != DataTypeID.intendTime)
                        return;


                    function getLastTime(stepDefinitions) {
                        var max = "";
                        for (var i = stepDefinitions.length - 1; i >= 0; i--) {
                            var item = stepDefinitions[i];
                            if (item.listSubRequest) {
                                item.listSubRequest.forEach(function (subStepData) {
                                    subStepData.listStepDefinition.forEach(function (subStep) {
                                        if (subStep.step.MaxIntendTime > max)
                                            max = subStep.step.MaxIntendTime;
                                    })
                                });
                            } else if (item.step.MaxIntendTime > max)
                                max = item.step.MaxIntendTime;
                        }
                        return max;
                    }

                    var lastTime = getLastTime(t.listStepDefinition());
                    var time = new Date(lastTime.replace(" ", "T"));
                    item.value = time.toLocaleString();
                });

                return list;

            })(requestData.listFieldDefinition));
            t.listComment(requestData.listComment);
            t.showButton.reply(res.referData.canComment);
            t.showButton.comment((function () {
                var stepDef = requestData.listStepDefinition.find(function (item) {
                    return item.step.Owner == requestData.currentDomain ||item.step.AssignedOwner == requestData.currentDomain;
                });

                if(stepDef)
                    return true;

                return requestData.getRequestOwner() == requestData.currentDomain;
            })());
            t.workflowID(requestData.workflow.ID);

            if (res.referData.canComment) {
                t.showProcess.dontSave = 1;
                t.showProcess(true);
                t.showProcess.dontSave = 0;
            }
            if (res.referData.requestUser && res.referData.requestUser.domainAccount == requestData.currentDomain) {
                APIData.Request.GetUserRecentRequests(function (res) {
                    if (res.code == AMResponseCode.ok) {
                        res.data.forEach(function (request) {
                            var progress;
                            if (request.RequestStatusID == APIData.Request.RequestStatusID.cancel) {
                                progress = "";
                            } else {
                                progress = (request.CurrentIndex + 1).toString()
                                    + "/"
                                    + request.des.totalStep.toString();
                            }
                            request.des.progress = progress;
                            request.name = request.ID
                        });

                        t.history(res.data);
                    } else {
                        Component.System.alert.show(res);
                    }
                });
            }
        } else if (res.code == AMResponseCode.notFound) {
            if (ID)
                Component.System.alert.show("Không tìm thấy đơn hàng");
            else
                Component.System.alert.show("Không tìm thấy qui trình");
        } else if (res.code == AMResponseCode.denied) {
            Component.System.alert.show("Bạn không thể xem đơn hàng này");
        } else {
            Component.System.alert.show(res);
        }
    }

    function loadRequestData() {
        APIData.Request.GetRequestEditData(ID, loadDataDone);
    }

    t.workflowID.subscribe(function (value) {
        wfID = value;
        Utils.setURLValue("wfID", value);
        if (t.request() == null)
            APIData.Request.GetRequestDefinition(wfID, loadDataDone);
    });
    t.listStepDefinition.subscribe(function (list) {
        var request = t.request();
        var currentDomain = t.currentDomain();
        var targetScroll = list[0];
        for (var i = 0; i < list.length; i++) {
            var item = list[i];
            var show = item._show = {};
            if (request == null) {
                t.currentStepDefinition(item);
                break;
            }

            if (item.StepIndex + 1 == request.CurrentIndex)
                targetScroll = item;

            if (item.StepIndex == request.CurrentIndex) {
                if (currentDomain == item.step.Owner) {
                    show.approveButton = 1;
                    if (t.listToEntrust.length)
                        show.entrustButton = 1;
                } else if (t.listToEntrust.indexOf(currentDomain) != -1) {
                    show.approveButton = 1;
                } else {
                    show.waitting = 1;
                }

                if (request.RequestStatusID == t.RequestStatusID.open)
                    t.currentStepDefinition(item);
                else
                    t.currentStepDefinition(null);
            } else {
                if (item.step && item.step.ActionDate)
                    show.approvedStatus = 1;
            }
        }

        targetScroll && (targetScroll.isTargetScroll = 1);

        setTimeout(function () {
            var target = $('.target-scroll');
            if (target[0] == null)
                return;

            $('.workflow-step>ul').animate({scrollLeft: target.position().left}, 500);
        }, 200);
    });
    t.showProcess.subscribe(function (val) {
        if (t.showProcess.dontSave)
            return;

        localStorage.order_showProcess = val;
    });
    t.workflowName.subscribe(function (val) {
        if (t.workflowName.__beingStandardized || !val || val.length < 2)
            return;

        // lowercase chữ cái đầu tiên cửa từ đầu tiên, nếu chỉ có chữ đầu tiên viết hoa

        // tìm từ đầu tiên
        var iFirstSpace = val.indexOf(' ');
        var firstWord = null;
        if (iFirstSpace == -1)
            firstWord = val;
        else
            firstWord = val.substr(0, iFirstSpace);

        // check nếu có chữ không phải đầu tiên mà viết hoa thì thoát
        var i = 1;
        for (; i < firstWord.length; i++) {
            if (firstWord[i] == firstWord[i].toUpperCase())
                return;
        }

        var newStr = firstWord.toLowerCase() + val.substr(iFirstSpace);
        t.workflowName.__beingStandardized = true;
        t.workflowName(newStr);
        t.workflowName.__beingStandardized = false;
    });
    t.currentStepDefinition.subscribe(function (stepDefinition) {
        if (stepDefinition == null)
            return;

        var curDomain = requestData.currentDomain;
        var curIsRequestOwner = curDomain == requestData.getRequestOwner();
        var curIsStepOwner = curDomain == stepDefinition.step.Owner || curDomain == stepDefinition.step.AssignedOwner;

        t.showButton.requestInfo(curIsStepOwner && stepDefinition.CanRequestInfo);

        if (curIsStepOwner) {
            if (stepDefinition.StepIndex == 0 && stepDefinition.step.ID) {
                t.showButton.reCreate(true);
                t.showButton.approves(false);
            } else {
                t.showButton.reCreate(false);
                t.showButton.approves(true);
            }
        } else {
            t.showButton.reCreate(false);
            t.showButton.approves(false);
        }

        if (curIsRequestOwner && curIsStepOwner == false && stepDefinition.CanRaise) {
            t.showButton.raise("Nhắc " + stepDefinition.step.Owner.toUpperCase() + " xử lý");
        }

        t.showButton.cancel(stepDefinition.CanCancel && (permision.cancel || curIsRequestOwner || curIsStepOwner));
        t.showButton.delegate(permision.delegate || curIsStepOwner);

        if(typeof stepDefinition.Role == 'number'){
            var fnCheckCanPickup = function () {
                if(!curUserRoleIDs)
                    return;

                var canPickup = !t.showButton.approves();
                if (canPickup){
                    canPickup = curUserRoleIDs.indexOf(stepDefinition.Role.toString()) != -1;
                }
                t.showButton.pickup(canPickup);
            };

            if(curUserRoleIDs == null){
                APIData.RoleAndPermission.GetUserRoleByDomain(wp_current_user_domain, function(resInJson){
                    if(!resInJson)
                        return;

                    var temp = JSON.parse(resInJson);
                    if(!temp || !temp.Permission)
                        return;

                    var roleIDs = JSON.parse(temp.Permission);
                    if(!roleIDs)
                        return;

                    curUserRoleIDs = roleIDs;
                    fnCheckCanPickup();
                });
            }else{
                fnCheckCanPickup();
            }
        }

        // dev_upgrade no depthead
        if (curIsStepOwner) {
            if (curDomain == 'tult' && typeof stepDefinition.Role == 'string' && stepDefinition.Role.startsWith(RoleID.deptHead.toString() + ':')) {
                Component.System.alert.show(
                    '<b>Chú ý</b><br>' +
                    'Dear Tú. Lê Thanh - IT Manager,<br>' +
                    'Bước này không thuộc quyền phê duyệt của bạn, nhưng hệ thống không thể xác định được người có quyền.' +
                    'Vì vậy hệ thống chọn IT Manager để xử lý tay cho bước phê duyệt này'
                );
            }
        }
    });  // set acction button is show or hiden

    // load permission
    (function(){
        APIData.RoleAndPermission.checkUserPermission('workflow.request.cancel', function (resInJson) {
            var res = JSON.parse(resInJson);
            permision.cancel = res.Result;
            if (permision.cancel && !t.showButton.cancel()) {
                var stepDefinition = t.currentStepDefinition();
                if (stepDefinition) {
                    t.showButton.cancel(stepDefinition.CanCancel && permision.cancel);
                }
            }
        });
        APIData.RoleAndPermission.checkUserPermission('delegate', function (resInJson) {
            var res = JSON.parse(resInJson);
            permision.delegate = res.Result;
            if (permision.delegate && !t.showButton.delegate()) {
                t.showButton.delegate(permision.delegate);
            }
        });
    })();

    if (ID) {
        loadRequestData();
    } else if (wfID) {
        t.workflowID(wfID);
    } else if (wfName) {
        APIData.Workflow.GetListWorkflow(function (res) {
            if (res.code == AMResponseCode.ok) {
                var requestDefinition = res.data.find(function (item) {
                    return item.DisplayName === wfName;
                });
                if (requestDefinition !== null) {
                    t.workflowID(requestDefinition.ID);
                } else {
                    t.workflows(res.data);
                }
            }
        });
    } else {
        APIData.Workflow.GetListWorkflow(function (res) {
            if (res.code == AMResponseCode.ok)
                t.workflows(res.data);
        });
    }
};
