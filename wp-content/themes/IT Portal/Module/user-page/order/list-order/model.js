Module.Model = function () {
    var me = this;
    this.listRequestDefined = ko.observableArray([]);
    this.listRequestStatus = ko.observableArray([]);
    this.listRequest = ko.observableArray([]);
    this.Checked = ko.observableArray([]);
    this.requestStatusIDSelected = ko.observable(Utils.getURLValue("statusID"));
    this.listRequestStatusID = ko.observableArray();
    this.requestLink = "/user-page/order/detail?";
    this.pendingMe = ko.observable(false);
    var listRequest = [];
    var currentPage = 0;
    var pageCount = 0;
    this.totalRequest = ko.observable(0);
    const limitRequestInPage = 10;
    const IDSeeAllCheckBox = 0;
    var RequestStatusID = APIData.Request.RequestStatusID;
    this.searchType = 'pending';
    this.searchKeyword = '{{me}}';
    this.searchKeywordTbx = '';
    this.requestFilter = '';
    this.requestStatusFilter = '';
    this.pendingFilterRequest = '';
    this.pendingToFilterRequest = '';
    this.isOnRemoveFilter = ko.observable(false);

    this.do_InitComponent = function () {
        Component.Custom.ComboBox.bindingItem();
    };

    this.getPageResult = function () {
        //(currentPage * limitRequestInPage) + me.listRequest().length
        return "Bạn đang xem " + "{0}/{1}".format(me.listRequest().length, me.totalRequest()) + " kết quả";
    };

    var getRequestByStatusID = function (requests, requestStatusID) {
        var currentListRequest = [];
        for (var i = 0; i < requests.length; i++) {
            for (var j = 0; j < me.listRequestStatusID().length; j++) {
                if (requests[i].RequestStatusID == me.listRequestStatusID()[j].ID) {
                    requests[i].RequestStatusDisplayName = me.listRequestStatusID()[j].DisplayName;
                }
            }
        }
        if (requestStatusID == IDSeeAllCheckBox) {
            return requests;
        } else {
            for (var i = 0; i < requests.length; i++) {
                if (requestStatusID == requests[i].RequestStatusID) {
                    currentListRequest.push(requests[i]);
                }
            }
        }
        return currentListRequest;
    };

    this.search_request = function () {
        //$('.head-filter a').removeClass('active');
        me.searchKeywordTbx = $("#txbUserAccount").val();
        me.searchType = 'search';

        if (me.searchKeywordTbx === '') {
            $('.head-filter a[st="pending"]').click();
        }

        currentPage = 0;
        if (me.searchKeywordTbx !== '' && me.searchKeywordTbx !== null && me.searchKeywordTbx !== undefined) {
            loadRequests();
        }
        if (me.searchKeywordTbx=== '') {
            loadRequests();
        }
    };

    this.do_FilterRequest = function () {
        Component.Custom.Popup.items['popupFilter'].Popup.show();
    };

    this.pendingFilterRequest_OnClick = function (model, e) {
        loadRequests();
    };

    this.filterRequest_Apply = function (model, e) {
        if (me.requestStatusFilter !== undefined) {
            if (!Array.isArray(me.requestStatusFilter)) {
                me.requestStatusFilter = me.requestStatusFilter;
            }
        } else {
            me.requestStatusFilter = [];
        }
        if (me.requestFilter !== 0) {
            if (!Array.isArray(me.requestFilter)) {
                me.requestFilter = [me.requestFilter];
            }
        } else {
            me.requestFilter = [];
        }
        loadRequests();
        Component.Custom.Popup.items['popupFilter'].Popup.hide();
    };

    this.filterControl_OnClick = function (model, e) {
        $("#txbUserAccount").val('');
        var allControl = $(e.currentTarget).parent().find('a');
        allControl.removeClass('active');
        $(e.currentTarget).addClass('active');
        me.searchType = $(e.currentTarget).attr('st');
        me.searchKeyword = $(e.currentTarget).attr('sk');
        loadRequests();
    };

    function loadRequests(limit, callback) {
        Component.System.loading.show();
        var agrs = {
            pageIndex: currentPage,
            limit: limit || limitRequestInPage,
            isOwned: true
        };

        if (me.searchType == 'pending') {
            agrs['pendingAt'] = me.searchKeyword;
            agrs['requestStatus'] = 'open';
        } else if (me.searchType == 'search') {
            if (me.searchKeywordTbx.indexOf('#') == 0) {
                me.searchKeywordTbx = me.searchKeywordTbx.substr(1);
                agrs['requestID'] = me.searchKeywordTbx;
                agrs['requestOwner'] = me._parent.currentLoginUserDomain;
            } else {
                agrs['assetname'] = me.searchKeywordTbx;
                agrs['assetType'] = me.searchKeywordTbx;
                agrs['assetModel'] = me.searchKeywordTbx;
                agrs['personInFlow'] = me.searchKeywordTbx;
                agrs['requestOwner'] = me._parent.currentLoginUserDomain;
                agrs['filterType'] = 'OR';
            }
        } else {
            agrs['requestStatus'] = me.searchKeyword;
            agrs['requestOwner'] = me._parent.currentLoginUserDomain;
        }

        if (Array.isArray(me.requestFilter) && me.requestFilter.length > 0) {
            agrs['requests'] = me.requestFilter;
        }

        if (me.requestStatusFilter !== undefined && me.requestStatusFilter !== null && me.requestStatusFilter != "") {
            agrs['stepStatus'] = me.requestStatusFilter;
        }

        if (me.pendingFilterRequest !== "" && me.pendingFilterRequest !== undefined && me.pendingFilterRequest !== null) {
            agrs['pendingTime'] = [
                me.pendingFilterRequest + " 00:00:00",
                (me.pendingToFilterRequest != '') ? me.pendingToFilterRequest + " 23:59:59" : me.pendingFilterRequest + " 23:59:59"
            ]
        }

        if (Object.keys(agrs).length > 5) {
            me.isOnRemoveFilter(true);
        } else {
            me.isOnRemoveFilter(false);
        }

        APIData.APIRequestFilter.filter(agrs, function (res) {
            Component.System.loading.hide();
            res = JSON.parse(res);
            if (limit === undefined) {
                pageCount = res.PageCount;
                me.totalRequest(res.Total);
                me.listRequest([]);
                me.listRequest(res.Result);
                render_paging();
                Component.Custom.Paging.items['requestsPaging'].on_PageChange = do_PageChange;
            } else {
                listRequest = res.Result.slice();
                exportExcel();
            }
        });
    }

    function loadRequests_optimize() {
        var args = {
            pageIndex: currentPage,
            limit: limitRequestInPage,
            isOwned: true
        };

        var param = buildSearchAI(args);

        if (Array.isArray(me.requestFilter) && me.requestFilter.length > 0) {
            args['requests'] = me.requestFilter;
        }

        if (Array.isArray(me.requestStatusFilter) && me.requestStatusFilter.length > 0) {
            var stepStatus = me.requestStatusFilter;
            if (stepStatus)
                args['stepStatus'] = stepStatus
        }

        if (me.pendingFilterRequest !== '') {
            args['pendingTime'] = [
                me.pendingFilterRequest + " 00:00:00",
                (me.pendingToFilterRequest != '') ? me.pendingToFilterRequest + " 23:59:59" : me.pendingFilterRequest + " 23:59:59"
            ]
        }

        APIData.APIRequestFilter.filterAI(param.keyword, param.keywordDoc, param.args, function (res) {
            res = JSON.parse(res);
            pageCount = res.PageCount;
            me.totalRequest(res.Total);
            me.listRequest([]);
            me.listRequest(res.Result);
            render_paging();
            Component.Custom.Paging.items['requestsPaging'].on_PageChange = do_PageChange;
        });

    }

    function buildSearchAI(agrs) {
        var re = {
            keyword: me.searchKeyword,
            keywordDoc: {
                '#': ['requestID'],
                user: ['requestOwner'],
                'asset': ['assetname'],
                'asset-model': ['assetModel'],
                'asset-type': ['assetType'],
            },
            args: agrs
        };

        if (me.searchType == 'pending') {
            agrs['requestStatus'] = 'open';
        } else if (me.searchType == 'search') {
            if (me.searchKeyword.indexOf('#') == 0) {
                agrs['requestOwner'] = me._parent.currentLoginUserDomain;
            } else {
                re.keywordDoc.user = ['personInFlow'];
                agrs['requestOwner'] = me._parent.currentLoginUserDomain;
                agrs['filterType'] = 'OR';
            }
        } else {
            agrs['requestStatus'] = me.searchKeyword;
            agrs['requestOwner'] = me._parent.currentLoginUserDomain;
        }
        return re;
    }

    this.Keyword_OnKeyUp = function (e) {
        if (e.key === 'Enter') {
            me.search_request();
        }
    };

    var do_PageChange = function (paging) {
        currentPage = paging.currentPage() - 1;
        loadRequests();
        $('html').animate({scrollTop: 0}, 500);
        var home = $('[section="home"]');
        home.removeClass('toggle');
    };

    var render_paging = function () {
        Component.Custom.Paging.items['requestsPaging'].render(pageCount, limitRequestInPage, me.totalRequest(), currentPage + 1);
    };

    this.chk_real_time = function (model, e) {
        if (e.currentTarget.checked) {
            setTimeout(function () {
                loadRequests();
                me.chk_real_time(model, e);
            }, 30000);
        } else {
        }
    }

    this.remove_filter = function () {
        /*var params = Common.getUrlParameters();
        if (params['pending-me'] === '') {
            me.pendingMe(false);
            me.searchType = 'pending';
            me.searchKeyword = '{{me}}';
        } else {
            me.searchType = 'status';
            me.searchKeyword = 'open';
        }
        me.requestFilter = [];
        me.requestStatusFilter = '';
        me.pendingFilterRequest = '';
        me.pendingToFilterRequest = '';
        loadRequests();*/
        location.reload();
    }

    var init = function () {
        me.Checked(['0']);

        me.searchType = 'status';
        me.searchKeyword = 'open';

        APIData.Request.GetListRequestStatus(function (res) {
            res.data.splice(0, 0, {
                ID: 0,
                DisplayName: '---'
            });
            me.listRequestStatus(res.data);
            Component.Custom.ComboBox.bindingItem();
        });

        APIData.Request.GetListRequest('', 0, 0, function (res) {
            res.splice(0, 0, {
                ID: 0,
                DisplayName: '---'
            });
            me.listRequestDefined(res);
            Component.Custom.ComboBox.bindingItem();
        });

        APIData.Request.GetListRequestStatus(function (res) {
            if (res.code == AMResponseCode.ok) {
                //res.data.push({ID: IDSeeAllCheckBox, Name: "all", DisplayName: "Xem tất cả"});
                //res.data.splice(1, 0, {ID: -1, DisplayName: "Cần duyệt"});
                //var prvSelected = me.requestStatusIDSelected();
                me.listRequestStatusID(res.data);
                //me.requestStatusIDSelected(prvSelected);
                //me.requestStatusIDSelected.subscribe(loadData);

                loadRequests();
            } else {
                if (location.hostname == 'itportal') {
                    Popup.error(res);
                }
            }
        });

        //me.listRequestStatusID.push({ID: me.requestStatusIDSelected(), DisplayName: ""});
    };

    init();
};