Module.Model = function () {
    var t = this;

    t.requests = ko.observableArray();
    function initImageSlider(container) {
        new Swiper(container,  {
            mode: 'horizontal',
            loop: false,
            slideElement: 'li',
            autoplay: false,
            initialSlide: 0,
            slidesPerView: 'auto',
            spaceBetween: 0,
            paginationClickable: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
                renderBullet: function (index, className) {
                    return '<span class="' + className + '">' + (index + 1) + '</span>';
                },
            },
            autoplayDisableOnInteraction: true
        });
    };


    var sr = SignalREngine.getDefault();
    sr.onGet(SignalREngine.method.showRequestQR, function (mes) {
        t.requests(mes.requests);
        initImageSlider($('.swiper-container'));
        sr.send(
            SignalREngine.method.showRequestReply,
            {sendID: mes.sendID},
            [mes.pcID],
            SignalREngine.typeOfReciverID.signalRID
        );
    });
};
