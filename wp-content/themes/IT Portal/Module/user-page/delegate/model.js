Module.Model = function () {
    var me = this;
    me.Domain = ko.observable(wp_current_user_domain);
    me.DelegatedDomain = ko.observable();
    me.isUpdate = ko.observable();
    me.canSettingForAnotherUser = ko.observable();
    me.active = function () {
        var data = Component.Custom.Form.items['ChangeInUseUserForm'].Form.val();

        var mes = valid(data);
        if (mes) {
            Component.System.alert.show(mes);
            return;
        }

        active(data);
    };
    me.deactive = function () {
        var domain = me.Domain();
        if(domain)
        var mes = checkNull(domain, 'user-page:delegate:domain');
        if (mes) {
            Component.System.alert.show(mes);
            return;
        }

        deactive(domain);
    };

    function valid(data) {
        data.Domain = me.Domain();

        mes = checkNull(data.Domain, 'user-page:delegate:domain');
        if (mes)
            return mes;

        var mes = checkNull(data.StartTime, 'user-page:delegate:startTime');
        if (mes)
            return mes;

        mes = checkNull(data.EndTime, 'user-page:delegate:endTime');
        if (mes)
            return mes;

        mes = checkNull(data.DelegatedDomain, 'user-page:delegate:delegatedDomain');
        if (mes)
            return mes;

        if (data.StartTime >= data.EndTime) {
            return i18n(
                'valid:timeBeforeTime',
                [
                    i18n('user-page:delegate:startTime'),
                    i18n('user-page:delegate:endTime')
                ]
            );
        }

        return null;
    }

    function checkNull(val, name) {
        if (val && val !== false)
            return null;

        return i18n('valid:notNull', [i18n(name)]);
    }

    function active(data) {
        var apiHandle = me.isUpdate() ? APIData.RequestDelegate.UpdateDelegate : APIData.RequestDelegate.InsertDelegate;
        apiHandle(data, function (res) {
            var form = Component.Custom.Form.get('ChangeInUseUserForm');

            if (res.code == AMResponseCode.ok) {
                var data = res.data;
                form.val(data);
                alertResult('user-page:delegate:alert:active', true);
            } else {
                alertResult('user-page:delegate:alert:active', false);
            }
        });
    }

    function deactive(domain) {
        APIData.RequestDelegate.DeleteDelegate(domain, function (res) {
            if(res.code == AMResponseCode.ok){
                alertResult('user-page:delegate:alert:deactive', true);
            }else{
                alertResult('user-page:delegate:alert:deactive', false);
            }
        });
    }

    function alertResult(taskName, isSuccess) {
        var i18nKey = isSuccess ? 'alert:success' : 'alert:failed';
        var mess = i18n(i18nKey, [i18n(taskName)]);

        Component.System.alert.show(mess);
    }

    function load() {
        APIData.RequestDelegate.GetDelegate(me.Domain(), function (res) {
            setDisplayName('DelegatedDomain', '');

            var form = Component.Custom.Form.get('ChangeInUseUserForm');
            if (res.code == AMResponseCode.ok) {
                var data = res.data;
                if (data) {
                    me.isUpdate(true);
                    setDisplayName('Domain', me.Domain());
                    setDisplayName('DelegatedDomain', data.DelegatedDomain);
                } else {
                    me.isUpdate(false);
                    var start = new Date();
                    var end = new Date();

                    if (start.getDay() == 5) { // thu 6
                        end.setDate(start.getDate() + 3);
                    } else if (start.getDay() == 6) { // thu 7
                        end.setDate(start.getDate() + 2);
                    } else {
                        end.setDate(start.getDate() + 1);
                    }
                    end.setHours(0);
                    end.setMinutes(0);
                    end.setSeconds(0);

                    data = {
                        StartTime: new Date(),
                        EndTime: end,
                        delegateMode: false
                    };
                }

                form.val(data);
            } else {
                Component.System.alert.show(i18n('alert:load-failed'));
            }
        });
    }

    function setDisplayName(attrName, domain) {
        if (domain){
            APIData.CMDB.GetUserFullName(domain, function (res) {
                if (!res)
                    return;

                var fullName = JSON.parse(res);
                if (!fullName)
                    return;

                $('.VNGSearch.form-control[name="' + attrName + '"]').val(fullName);
            });
        }else{
            $('.VNGSearch.form-control[name="' + attrName + '"]').val('');
        }
    }

    load();
    APIData.RequestDelegate.CanSettingForAnotherUser(function (res) {
        if (res.code == AMResponseCode.ok) {
            me.canSettingForAnotherUser(res.data);
            if (me.Domain())
                setDisplayName('Domain', me.Domain());

            var lastDomainSelected = me.Domain();
            me.Domain.subscribe(function (domain) {
                if(lastDomainSelected == me.Domain())
                    return;

                lastDomainSelected = me.Domain();
                load();
            });
        }
    });
};
