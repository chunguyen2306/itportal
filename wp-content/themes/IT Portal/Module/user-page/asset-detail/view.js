Module.View = function (_model, _onDone) {
    var me = this;
    var model = _model;
    var ComponentData = {};
    Component.Module.AbstractModuleView.call(this, model,_onDone );
    Config.Global.isInitMenuMouseHover = false;
    Config.Global.isInitMouseWheel = false;
    $(document).ready(function () {
        var assetInfo = $('#assetInfo').text();
        var assetNoteHistory = $('#assetNoteHistory').text();
        $('#assetNoteHistory').remove();
        $('#assetInfo').remove();
        model.assetNoteHistory = JSON.parse(assetNoteHistory);
        model.AssetInfo = JSON.parse(assetInfo);
        APIData.Request.GetReportingLine(model.AssetInfo.OwnerAccount, function (res) {
            if (res.code == AMResponseCode.ok) {
                if (res.data.length > 0) {
                   model.AssetInfo.ReportingLine = res.data[0].Domain
                } else {
                    model.AssetInfo.ReportingLine='không có thông tin';
                }
            } else {
                model.AssetInfo.ReportingLine='không có thông tin';
            }
        });
        var photoList = $('.all-photo img');
        var thubmails = $('.selected-photo span.preview');


        photoList.on('click', function () {
            var src = this.src;
            thubmails.css({
                backgroundImage: 'url('+src+')'
            });
        });

        $('.all-photo img[alt="'+thubmails.attr('thubmails')+'"]').click();


        $('[name="ReviewForm"]').on('ajaxsubmit', function (e, res) {
            res = JSON.parse(res);

            Component.System.alert.show(res);
            if(res.State){
                Component.System.alert.show('Đánh giá của bạn đã được gửi',
                    {
                        name: 'Đồng ý',
                        cb: function () {
                            var rateField = Component.Custom.Form.get('ReviewForm').field('Rate');
                            var commentField = Component.Custom.Form.get('ReviewForm').field('Comment');
                            rateField[0].reset();
                            commentField[0].reset();
                            window.location.reload();
                        }
                    })
            } else {
                Component.System.alert.show('Đánh giá của bạn chưa gửi được',
                    {
                        name: 'Đồng ý'
                    })
            }
        });
    });

    me.onDone();
};