Module.Model = function () {
    let me = this;
    this.AssetStates = ko.observableArray([]);
    this.UserName = ko.observable('');
    this.AssetNameParent = ko.observable('');
    this.AssetInfo = {};
    this.assetNoteHistory = ko.observableArray([]);
    //this.AssetDetail = ko.observable({});
    this.requestData = ko.observable({
        CurrentUser: null,
        AssetList: {},
        Note: '',
        CurrentDate: null
    });
    this.Notes = ko.observableArray([]);
    let vaildFileType = [];
    let images =[];
    let camInput = null;
    let PickFiles =[];
    let init = function () {
        APIData.CMDB.GetListAssetState('', '', '', function (res) {
            res.unshift({ID:0, Name: '-- Chọn một loại trạng thái cho sản phẩm --', Description: null});
            me.AssetStates(res);
            Component.Custom.ComboBox.bindingItem();
        });
        APIData.MenuNotes.GetAll(function (res) {
            res= JSON.parse(res);
            me.Notes(res);
        });
        APIData.WhiteListUpload.GetAll(function (res) {
            res= JSON.parse(res);
            for(let i=0;i<res.length;i++){
                vaildFileType.push(res[i].Name);
            }

        });
        APIData.WhiteListUpload.GetImageType(function (res) {
            res =JSON.parse(res);
            for(let i=0;i<res.length;i++){
                images.push(res[i].Name);
            }

        });

    };
    let UpdateTypeEnum = {
        State: 1,
        Owner: 2,
        Associated: 3
    };
    let cb_UpdateAssetState = function (res, data, sysComment) {
        let statuscode = null;
        try {
            statuscode = res.API.response.operation.result.statuscode;
            if (statuscode === '200') {
                APIData.CMDB.GetLastStatusHistory(data.AssetName, function (res) {
                    if (res !== null) {
                        APIData.AssetStateHistory.InsertStateHistory(JSON.stringify({
                            stateId: res.ID,
                            assetName: data.AssetName,
                            newState: res.NewState,
                            previousState: res.PreviousState,
                            actionDate: new Date(),
                            userAction: me._parent.currentLoginUserDomain,
                            comment: data.Comment,
                            sysComment: sysComment
                        }), function (result) {
                            window.location.reload();
                            Component.System.alert.show('Cập nhật trạng thái thành công');
                        });
                    }
                });
            } else {
                Component.System.loading.hide();
                Component.System.alert.show('Cập nhật trạng thái không thành công vui lòng kiểm tra lại');
            }
        }catch (e) {
            Component.System.loading.hide();
            Component.System.alert.show('Cập nhật trạng thái không thành công vui lòng kiểm tra lại');
        }finally {
            Component.System.loading.hide();
        }

    };
    let updateAssetState = function (updateType, data) {
        let sysMess = '';
        let SysComment = 'Thay đổi trạng thái bởi '+ me._parent.currentLoginUserDomain + ' từ IT Portal ';
        Component.System.loading.show();
        switch (updateType) {
            case UpdateTypeEnum.State:
                APIData.CMDB.UpdateAssetState(data.AssetType, data.AssetName, data.AssetState, function (res) {
                    cb_UpdateAssetState(res, data, SysComment);
                });
                break;
            case UpdateTypeEnum.Owner:
                sysMess = JSON.stringify({
                    type: 'UpdateOwner',
                    oldOwner: $('#assetOwner').text(),
                    newOwner: data.UserName
                });
                APIData.CMDB.GetUserInformation(data.UserName,0,10,function (res) {
                    if(res!=''){
                        APIData.CMDB.UpdateAssetOwner(data.AssetType, data.AssetName, data.UserName, function (res) {
                            cb_UpdateAssetState(res, data, sysMess);
                        });
                    }else {
                        Component.System.alert.show('Người dùng không  tồn tại. Không thể gán thiết bị');
                        Component.System.loading.hide();
                    }
                });
                break;
            case UpdateTypeEnum.Associated:
                sysMess = JSON.stringify({
                    type: 'UpdateAssociated',
                    oldParent: $('#assetParent').text(),
                    newParent: data.AssetNameParent
                });
                APIData.CMDB.GetAllAssetDetail(data.AssetNameParent,function (res) {
                   if(res.IsSuccess==false){
                        Component.System.alert.show(data.AssetNameParent+" Không phải là sản phẩm IT");
                       Component.System.loading.hide();
                       return;
                   }
                   if(res.IsComponent){
                       Component.System.alert.show('Không thể gắn '+data.AssetName+' với '+ data.AssetNameParent);
                       Component.System.loading.hide();
                   }else {
                       APIData.CMDB.UpdateAssociateAsset(data.AssetType, data.AssetName, data.AssetNameParent, function (res) {
                           cb_UpdateAssetState(res, data, sysMess)
                       });
                   }
                });

                break;
        }
    };

    let createRequestListItem = function (listChild) {
        let item = {};
        for (let i = 0; i < listChild.length; i++) {
            item[listChild[i].Name] = null;
        }

        return item;
    };
    let UploadFileforNote = function (res) {
        res = JSON.parse(res);

        let Filename = res.data.id +'_'+new Date().getTime();
        let FileNames =[];
        if(res.code===200){
            let form = Component.Custom.Form.get('AssetNotesDetail');
            let data = form.val();
            let fileNamePrefix = data.AssetName;
            if(data.Gallery.length>0){
                for(let i = 0; i<data.Gallery.length;i++){
                    PickFiles.push(data.Gallery[i]);

                }
            }

            if(PickFiles.length>0){
                for(let i = 0; i<PickFiles.length; i++){
                    APIData.UploadFile('upload.Asset_Note.' + fileNamePrefix,Filename+'_'+i,PickFiles[i],true);
                    let src = PickFiles[i].name;
                    let type =  src.split('.').pop().toLowerCase();
                    FileNames.push(Filename+'_'+i+'.'+type);
                }
                res.data.sysComment = FileNames;
                APIData.AssetStateHistory.UpdateSysComment(res.data,cb_SunmitNoteDetail);
            }

        }else{
            alert('Ghi chú tình trạng thiết bị không thành công');
            Component.System.loading.hide();

        }
    };
    var cb_SunmitNoteDetail = function (res) {
        res = JSON.parse(res);

        if (res.State) {
            Component.System.alert.show('Ghi chú tình trạng thiết bị thành công');
            Component.Custom.Popup.items['add-notes'].Popup.hide();
            Component.System.loading.hide();
            Component.Custom.Form.get('AssetNotesDetail').resetData();

        } else {
            alert('Ghi chú tình trạng thiết bị không thành công');
            Component.System.loading.hide();
        }
    };
    var Preview_OnClick = function (name, img) {
        var mess = 'Bạn có muốn xóa file ' + name;
        Component.System.alert.show( mess , {
            name: 'Đồng ý',
            cb: function () {
                var ComponentForm = Component.Custom.Form.get('AssetNotesDetail');
                var fieldInput = ComponentForm.field('Gallery').find('input')[0];

                var newFileIndex = -1;

                if (fieldInput.files !== undefined) {
                    for (var i in fieldInput.files) {
                        if (fieldInput.files[i].name === name) {
                            newFileIndex = i;
                        }
                    }
                }
                if (newFileIndex !== -1) {
                    delete fieldInput.files[newFileIndex.toString()];
                }
                img.remove();
            }
        }, {
            name: 'Không',
            cb: function () {
                Component.System.alert.hide();
            }
        })
    };
    var validFile = function (file) {
        var response={};
        var type = file.name.split('.').pop().toLowerCase();
        if (!vaildFileType.includes(type)) {
            response.State =false;
            response.Mess =' định dạng file không phù hơp.';
            return response;
        }
        if (file.size > 10440000) {
            response.State =false;
            response.Mess =' vượt quá kích thước cho phép(10MB).';
            return response;
        }
        response.State=true;
        response.Mess ='';
        return response;
    };
    var reBindingComment = function (model,e) {

    }
    this.selectAllComment = function (model,e) {
        APIData.AssetStateHistory.GetAssetCommentHistory(me.AssetInfo.ResourceName,function (res) {
            me.assetNoteHistory(res);
        });
    }
    this.lblTitle_OnClick = function (model, e) {
        var lblTitle = $(e.currentTarget);
        if (lblTitle.parent().find('.detail')[0].style.display !== 'block') {
            lblTitle.parent().find('.detail').show();
        } else {
            lblTitle.parent().find('.detail').hide();
        }
    };
    this.childDetail_OnClick = function (model, e) {
        var name = e.currentTarget.getAttribute('name');
        var type = e.currentTarget.getAttribute('type');

        if (type === 'Category') {
            window.location = Common.build_param({
                page: 1,
                keyword: '#ignore',
                productType: name
            });
        } else {
            window.location = '/user-page/asset-detail' + Common.build_param({
                page: '#ignore',
                keyword: '#ignore',
                asset: name
            });
        }
    };
    this.btnChangeAssetDetail_OnClick = function (model, e) {
        //currentTarget > options > button > div
        var assetName = e.currentTarget.parentNode.parentNode.parentNode.getAttribute('assetname');

    };
    this.btnSubmitChangeAssetDetail_OnClick = function () {

    };
    this.btnCancelChangeAssetDetail_OnClick = function () {

    }
    this.btnInUseToUser_OnClick = function (model, e) {
        var assetName = e.currentTarget.parentNode.parentNode.parentNode.getAttribute('assetname');
        var assetType = e.currentTarget.parentNode.parentNode.parentNode.getAttribute('assettype');
        var form = Component.Custom.Form.get('ChangeInUseUserForm');
        form.val({
            AssetName: assetName,
            AssetType: assetType,
            Comment: ''
        });
        Component.Custom.Popup.items['change-in-use-user'].Popup.show();
    };
    this.btnInUseToAsset_OnClick = function (model, e) {
        var assetName = e.currentTarget.parentNode.parentNode.parentNode.getAttribute('assetname');
        var assetType = e.currentTarget.parentNode.parentNode.parentNode.getAttribute('assettype');
        var form = Component.Custom.Form.get('ChangeInUseAssetForm');
        form.val({
            AssetName: assetName,
            AssetType: assetType
        });
        Component.Custom.Popup.items['change-in-use-asset'].Popup.show();
    };
    this.btnChangeAssetState_OnClick = function (model, e) {
        //currentTarget > options > button > div
        var assetName = e.currentTarget.parentNode.parentNode.parentNode.getAttribute('assetname');
        var assetType = e.currentTarget.parentNode.parentNode.parentNode.getAttribute('assettype');

        var form = Component.Custom.Form.get('ChangeAssetStateForm');
        form.val({
            AssetName: assetName,
            AssetType: assetType

        });
        Component.Custom.Popup.items['change-asset-state'].Popup.show();
    };
    this.btnSubmitChangeAssetState_OnClick = function () {
        var form = Component.Custom.Form.get('ChangeAssetStateForm');
        var data = form.val();

        var errors = [];
        if (data.AssetState === undefined || data.AssetState === undefined) {
            errors.push('Trạng thái tài sản không được để trống');
        }
        if (data.Comment === undefined || data.Comment === undefined) {
            errors.push('Ghi chú không được để trống');
        }
        if (errors.length > 0) {
            Component.System.alert.show(errors.join('<br>'));
        } else {
            updateAssetState(UpdateTypeEnum.State, data);
        }
    };
    this.btnCancelChangeAssetState_OnClick = function () {
        var form = Component.Custom.Form.get('ChangeAssetStateForm');
        form.resetData();
        Component.Custom.Popup.items['change-asset-state'].Popup.hide();
    };
    this.btnSubmitChangeInUseUser_OnClick = function () {
        var form = Component.Custom.Form.get('ChangeInUseUserForm');
        var data = form.val();

        var errors = [];
        if (data.UserName === '' || data.UserName === undefined) {
            errors.push('Tên người dùng không được để trống');
        }
        if (data.Comment === undefined || data.Comment === undefined) {
            errors.push('Ghi chú không được để trống');
        }
        if (errors.length > 0) {
            Component.System.alert.show(errors.join('<br>'));
        } else {
            updateAssetState(UpdateTypeEnum.Owner, data);
        }
        //
    };
    this.btnCancelChangeInUseUser_OnClick = function () {
        var form = Component.Custom.Form.get('ChangeInUseUserForm');
        form.resetData();
        Component.Custom.Popup.items['change-in-use-user'].Popup.hide();
    };
    this.btnSubmitChangeInUseAsset_OnClick = function () {
        var form = Component.Custom.Form.get('ChangeInUseAssetForm');
        var data = form.val();
        var errors = [];
         if(data.AssetNameParent === '' || data.AssetNameParent === undefined){
             errors.push('Tên tài sản cha không được để trống');
         }
        if (data.Comment === undefined || data.Comment === '') {
            errors.push('Ghi chú không được để trống');
        }
        if (errors.length > 0) {
            Component.System.alert.show(errors.join('<br>'));
        } else {
            updateAssetState(UpdateTypeEnum.Associated, data);
        }
    };
    this.btnCancelChangeInUseAsset_OnClick = function () {
        var form = Component.Custom.Form.get('ChangeInUseAssetForm');
        form.resetData();
        Component.Custom.Popup.items['change-in-use-asset'].Popup.hide();
    };
    this.btnCreateRequest = function (model, e) {

        var eml = e.currentTarget;
        currentActionRequest = eml.getAttribute('actiontype');
        if(me.AssetInfo.IsComponent){
            me.AssetInfo.OwnerAccount = me.AssetInfo.ParentOwnerAccount;
        }
        me.requestData({
            Requester: me._parent.currentLoginUserDomain,
            CurrentUser: me.AssetInfo.OwnerAccount,
            ReportingLine: me.AssetInfo.ReportingLine,
            AssetList: [
                {
                    AssetName: me.AssetInfo.ResourceName,
                    AssetModelID: me.AssetInfo.ResourceModelID,
                    AssetModel: me.AssetInfo.ResourceModel,
                    AssetType: me.AssetInfo.ResourceType,
                    AssetTypeID: me.AssetInfo.ResourceTypeID
                }
            ],
            Note: '',
            Address: ''

        });
        Component.Custom.Popup.items['create-request-comment'].Popup.show();
    };
    this.btnAddNotes = function (model, e) {
        PickFiles=[];
        Component.Custom.ComboBox.bindingItem();
        var assetName = model.AssetInfo.ResourceName;
        var currentuser = me._parent.currentLoginUserDomain;
        var form = Component.Custom.Form.get('AssetNotesDetail');
        form.val({
            AssetName: assetName,
            UserAction: currentuser,
            Gallery: [],
            comment: ''

        });
        form.field('Gallery')[0].File.Preview_OnClick = Preview_OnClick;
        Component.Custom.Popup.items['add-notes'].Popup.show();
    };
    this.do_SubmitNoteDetail = function () {
        var form = Component.Custom.Form.get('AssetNotesDetail');
        var data = form.val();
        var fieldUpload = form.field('Gallery')[0].File;
        if(fieldUpload.length>0){
            for(var i = 0; i<fieldUpload.length;i++){
                PickFiles.push(fieldUpload[i]);
            }
        }
        var errors = [];
        if (data.comment === undefined || data.comment === '') {
            errors.push('Ghi chú không được để trống');
        }
        if(data.Classify ==='--Chọn một loại ghi chú--'|| data.Classify ===''||data.Classify === undefined){
            errors.push('Chọn một loại ghi chú');
        }
        for(var i = 0; i<data.Gallery.length;i++){
            var res = validFile(data.Gallery[i]);
            if(!res.State){
                errors.push('<b>'+data.Gallery[i].name +'</b>'+' '+res.Mess +'<br>');
            }
        }
        if (errors.length > 0) {
            Component.System.alert.show(errors.join('<br>'));
        } else {
            Component.System.loading.show();
            var submitData = {
                AssetName: data.AssetName,
                Comment: data.comment,
                ActionDate: new Date(),
                UserAction: data.UserAction,
                SysComment: [],
                TargetUser: data.Classify

            };
            if (data !== undefined && data.comment !== '') {
                submitData.SysComment = null;
                APIData.AssetStateHistory.InsertStateHistory(JSON.stringify({
                    stateId: 0,
                    assetName: submitData.AssetName,
                    newState: '',
                    previousState: '',
                    actionDate: new Date(),
                    userAction: submitData.UserAction,
                    targetUser: submitData.TargetUser,
                    comment: submitData.Comment,
                    sysComment: submitData.SysComment
                }), UploadFileforNote);

            }
        }

    };
    this.cancel_SubmitNoteDetail = function () {
        Component.Custom.Popup.items['add-notes'].Popup.hide();
    };
    this.takeAPhoto = function () {
        if (camInput == null){
            camInput = new  classCameraInput();
        }
       camInput.pickCam((function (camFiles) {
           for(let i=0;i<camFiles.length;i++){
               PickFiles.push(camFiles[i]);
           }

       }));

    };
    this.btnSubmitCreateRequest_OnClick = function () {

        var form = Component.Custom.Form.get('CreateRequestForm');
        var data = form.val();
        var errors = [];
        if (data.Note === undefined || data.Note === '') {
            errors.push('Ghi chú không được để trống');
        }
        if (errors.length > 0) {
            Component.System.alert.show(errors.join('<br>'));
        } else {
            me.requestData().Note = data.Note;
            createRequestData();
        }
    };
    this.btnCancelCreateRequest_OnClick = function () {
        var form = Component.Custom.Form.get('CreateRequestForm');
        form.resetData();
        Component.Custom.Popup.items['create-request-comment'].Popup.hide();
    };
    this.btnBack_OnClick = function () {
        window.location = '/user-page/asset' + Common.build_param({
            page: '#ignore',
            asset: '#ignore'
        });
    };
    this.reviewImg_OnClick = function (model, e) {
        var src = e.currentTarget.src;
        var name = e.currentTarget.name;
        var type =  src.split('.').pop().toLowerCase();
        if(images.includes(type)){
            var popup = document.querySelectorAll('#reviewIMG')[0];
            var imgReview = popup.querySelectorAll('img')[0];
            imgReview.src = src;
            Component.Custom.Popup.items['reviewIMG'].Popup.show();
        }else {
            if (src.startsWith('/') == false && src.startsWith('https:') == false && src.startsWith('http:') == false)
                src = '/' + src;

            src = src.replace('uploads/', '').replace(/\//g,'.');
            var ext = src.substring(src.lastIndexOf('.')+1);
            src = src.substring(0,src.lastIndexOf('.'));
            src = '/download/?i=' + encodeURI(src) + '&e=' + encodeURI(ext);

            var link = $('<a download/>');
            link.attr('href', src);
            $('body').append(link);
            link[0].click();
            link.remove();
        }

    };
    this.viewCommentHistory = function (model, e) {

    }
    var createRequestData = function () {
        if (Component.Custom.Popup.HasActive) {
            Component.Custom.Popup.Active.hide();
        }
        Component.System.loading.show();
        var requestList = [];
        requestList.push({
            actionType: currentActionRequest,
            data: me.requestData()
        });
        console.log(requestList);
        Libs.Workflow.createWithMappingData(requestList, function (res) {

            var mess = '';
            try{
            for (var i = 0; i < res.length; i++) {
                if (res[0].result) {
                    mess += 'Đơn hàng đã được tạo thành công. Mã đơn hàng là <b>' + res[i].response.request.ID + '</b>.';
                } else {
                    mess += 'Đơn hàng <b>' + res[i].response.workflow.DisplayName + '</b> của bạn chưa được tạo, ' +
                        'vui lòng liên hệ helpdesk để được xử lý.';
                }
                mess += '<br>';
                me.requestData('');
                Component.System.alert.show(mess, {
                    name: 'Xem đơn hàng',
                    cb: function () {
                        window.location = '/user-page/order/detail/?ID=' + res[0].response.request.ID;
                    }
                }, {
                    name: 'Đóng',
                    cb: function () {
                            Component.System.loading.hide();

                    }
                });
            }}catch (e) {
            mess+='Đơn hàng của bạn tạo không thành công  vui lòng liên hệ helpdesk để được xử lý.';
            Component.System.alert.show(mess);
                Component.System.loading.hide();
        }finally {
                Component.System.loading.hide();      }
        });

    };
    init();
};