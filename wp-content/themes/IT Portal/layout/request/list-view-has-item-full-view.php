<a target="_blank" data-bind="attr: { href: '/user-page/order/detail/?ID='+ID }" class="request-info">
    <span class="order-number">Đơn hàng <strong data-bind="text: '#'+ID"></strong></span>
    <span class="order-created">
                    <b data-bind="text: Steps[0].Owner"></b> đặt lúc
                    <b data-bind="text: new Date(Steps[0].ActionDate).format('DD/MM/YYYY hh:mm:ss')"></b> cho
                    <b data-bind="text: Libs.Workflow.getOwnedField(request)"></b>
                </span>

    <span class="status">
                    <span class="pendingAt" data-bind="attr: { status: Name },
                    html: ((Name==='open')
                    ? 'Đang chờ <b>'+ Steps[CurrentIndex].Owner+'</b> '
                    : (Name==='done')
                    ? 'Hoàn tất lúc <b>'+new Date(Steps[CurrentIndex].ActionDate).format('hh:mm ngày DD/MM/YYYY')+'</b>'
                    : 'Hủy lúc <b>'+new Date(Steps[CurrentIndex].StartDate).format('hh:mm ngày DD/MM/YYYY')+'</b>')"></span>
        <!-- ko if: Name==='open'  -->
                    <span class="stepName" data-bind="text: Steps[CurrentIndex].DisplayName"></span>
        <!-- /ko -->
                    <br>
                    <span class="pendingTime"
                          data-bind="text: Libs.Workflow.getPendingDate(Steps[CurrentIndex].StartDate)"></span>
                </span>
</a>
<div class="request-item" data-bind="foreach: Libs.Workflow.getTableData(request, false)">
    <div class="item">
        <span class="asset-type"
              data-bind="text: AssetType"></span>
        <span class="asset-model"
              data-bind="text: AssetModel"></span>
        <span class="asset-name" data-bind="text: AssetName"></span>
        <span class="asset-count">
            <span class="count-label">SL:</span>
            <span data-bind="text: Count"></span>
        </span>
    </div>
</div>