<!-- ko if: Steps.length > 0 -->
<li class="row">
    <div class="column selectControl">
        <input data-bind="checked: selected" component="checkbox" type="checkbox" class="nolabel onlychecked">
    </div>
    <div class="column left">
        <?php import('theme.layout.request.list-view-has-item')?>
    </div>
    <div class="column right">
        <button data-bind="visibile: selected(), click: $root.showTicket">QR </button>
    </div>
</li>
<!-- /ko -->
<!-- ko ifnot: Steps.length > 0 -->
<li class="row">
    <div class="column selectControl">
        <input data-bind="checked: selected" component="checkbox" type="checkbox" class="nolabel onlychecked">
    </div>
    <div class="column left">
        <?php import('theme.layout.request.list-view-has-no-item')?>
    </div>
    <div class="column right">
        <button data-bind="visibile: selected(), click: $root.showTicket">QR</button>
    </div>
</li>
<!-- /ko -->
