<?php get_template_part('layout\header\header', 'common'); ?>
    <link type="text/css" rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/main-shop.css?v=<?php echo date('hhmmss'); ?>" />
	<div class="header">
        <span class="logo"></span>
        <div class="search-header-block">
            <div class="menu-btn">
                <span class="glyphicon glyphicon-th"></span>
                <span>Menu</span>
            </div>
            <div class="search-block">
                <input type="text" placeholder="Enter product name to search"/>
                <span class="fa fa-search"></span>
            </div>
        </div>
        <div class="user-info-block">
             <?php dynamic_sidebar("Login Sidebar") ?>
        </div>
	</div>
