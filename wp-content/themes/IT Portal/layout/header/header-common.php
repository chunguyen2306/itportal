<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage IT Portal
 * @since IT Portal 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<style>
	    html { margin: 0px !important; }
	</style>
    <link type="text/css" rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/font.css?v=<?php echo date('hhmmss'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/main.css?v=<?php echo date('hhmmss'); ?>" />
<!--    <link type="text/css" rel="stylesheet" href="/wp-includes/css/bootstrap.min.css?v=--><?php //echo date('hhmmss'); ?><!--" />-->
    <script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/jquery-3.1.1.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/knockout-3.4.1.js'; ?>"></script>

<!--    <script type="text/javascript" src="--><?php //echo includes_url('/js/bootstrap.min.js'); ?><!--"></script>-->

</head>