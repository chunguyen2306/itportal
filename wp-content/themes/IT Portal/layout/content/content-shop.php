<div class="content">
    <?php
    //do_action('check_permission', 'Admin', "Read, Write", array('TestController', 'test'));
    global $amRole;
    $menu = array (
        0 => array(
            'Name' => 'Home',
            'Type' => 'normal',
            'Link' => '',
            'Role' => array('Users', 'Help Desk', 'Audit', 'FA', 'Store Keeper', 'Dept Head'),
            'Permission' => array('Read', 'View', 'Comment')
        ),
        1 => array(
            'Name' => 'Contact',
            'Type' => 'normal',
            'Link' => '',
            'Role' => array('Help Desk', 'Audit', 'FA', 'Store Keeper', 'Dept Head'),
            'Permission' => array('Read', 'View')
        ),
        2 => array(
            'Name' => 'Product',
            'Type' => 'normal',
            'Link' => '',
            'Role' => array('Users', 'Help Desk', 'Audit', 'FA', 'Store Keeper', 'Dept Head'),
            'Permission' => array('Read','Write')
        ),
        3 => array(
            'Name' => 'Report',
            'Type' => 'dropdown',
            'Link' => '',
            'Role' => array('Users', 'Help Desk', 'Audit', 'FA', 'Store Keeper', 'Dept Head'),
            'Permission' => array('')
        ),
        4 => array(
            'Name' => 'Report 1',
            'Type' => 'dropdown',
            'Link' => '',
            'Role' => array('Users', 'Help Desk', 'Audit', 'FA', 'Store Keeper', 'Dept Head'),
            'Permission' => array('Read','View','Comment')
        ),
        5 => array(
            'Name' => 'Report 2',
            'Type' => 'dropdown',
            'Link' => '',
            'Role' =>  array('Help Desk', 'Audit', 'FA', 'Store Keeper', 'Dept Head'),
            'Permission' => array('Write','Comment')
        ),
        6 => array(
            'Name' => 'Report 3',
            'Type' => 'dropdown',
            'Link' => '',
            'Role' => array('Users', 'Help Desk', 'Audit', 'FA', 'Store Keeper', 'Dept Head'),
            'Permission' => array('Read','Write','Comment')
        ),
        7 => array(
            'Name' => 'Report 4',
            'Type' => 'dropdown',
            'Link' => '',
            'Role' => array('Help Desk', 'Audit', 'FA', 'Store Keeper', 'Dept Head'),
            'Permission' => array('View')
        )
    );

    $menu = $amRole->FilterMenu($menu, 'permission');

    $i = 1;
    echo '<nav class="navbar navbar-inverse">';
    echo '<div class="container-fluid">';
    echo '<div class="navbar-header">';
    echo '<a class="navbar-brand" href="#">Asset Management</a>';
    echo '</div>';
    echo '<ul class="nav navbar-nav">';
    foreach ($menu as $key => $item):
        switch ($item['Type']) {
            case 'dropdown':
                if($i ===  1) {
                    echo '<li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">'.$item['Name'].'<span class="caret"></span></a>';
                    $i = $i + 1;
                } else if($i === 2) {
                    echo '<ul class="dropdown-menu">
                                    <li><a href="#">'. $item['Name'] .'</a></li>';
                    $i = $i + 1;
                } else {
                    echo '<li><a href="#">' . $item['Name'] . '</a></li>';
                    $i = $i + 1;
                }
                break;
            default:
                if($i>1) {
                    echo '</ul>';
                    echo '</li>';
                    echo '<li class=""><a href="#">' . $item['Name'] . '</a></li>';
                    $i = 1;
                } else {
                    echo '<li class=""><a href="#">' . $item['Name'] . '</a></li>';
                    $i = 1;
                }
                break;
        }
    endforeach;
    if($i>1) {echo '</ul>';
        echo '</li>';
    }
    echo '</ul>';
    echo '<ul class="nav navbar-nav navbar-right">';
    echo '<li><a href="#"><span class="glyphicon glyphicon-user"></span>Sign Up</a></li>';
    echo '<li><a href="#"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>';
    echo '</ul>';
    echo '</div>';
    echo '</nav>';
    ?>

    <?php
        global $RES_PATH;
        $path_infos = explode('/', $RES_PATH);
        if($path_infos[1] == ''){
            get_template_part('layout/content/shop/home');
        } else {
            get_template_part('layout/content/shop/'.$path_infos[1]);
        }
     ?>
</div>