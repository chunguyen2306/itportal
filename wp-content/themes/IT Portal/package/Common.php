<?php
function am_ripJs() {
    $template_dir = get_template_directory();
    $minJsFile    = fopen($template_dir . "/js/min.js", "w");
    //fwrite($minJsFile, '(function(){' . PHP_EOL);
    fwrite($minJsFile, 'var DIR_URL = "' . esc_url(get_template_directory_uri()) . '";' . PHP_EOL);
    am_ripPackage('Libs', '/js/Libs', $minJsFile, true);
    am_ripPackage('Component', '/js/Component', $minJsFile, true);
    am_ripPackage('Config', '/js/Config', $minJsFile, true);
    am_ripPackage('APIData', '/js/Data', $minJsFile, true);

    //am_copyContent($minJsFile, $template_dir . '/js/main.js');

    //am_initModuleComponent($minJsFile);

    //fwrite($minJsFile, '})()');
    fclose($minJsFile);
}

function am_initModuleComponent($minJsFile) {
    $template_dir = get_template_directory();
    $dri_path     = $template_dir . '/js/Module';
    $modules      = scandir($dri_path);
    foreach ($modules as $file) {

        if ($file != '.' && $file != '..') {
            am_ripPackage('Component.' . $file, '/js/Module/' . $file . '/components', $minJsFile, false);
        }
    }
}

function am_copyContent($fileInfo, $filePath) {
    $file        = fopen($filePath, 'r');
    $fileContent = fread($file, filesize($filePath));
    fwrite($fileInfo, $fileContent);
    fclose($file);
}

function am_ripPackage($name, $path, $minJsFile, $isVar) {
    $template_dir = get_template_directory();
    $dri_path     = $template_dir . $path;
    $files        = scandir($dri_path);
    fwrite($minJsFile, PHP_EOL . (($isVar) ? 'var ' : '') . $name . ' = {};' . PHP_EOL);
    foreach ($files as $file) {
        if ($file != '.' && $file != '..') {
            am_copyContent($minJsFile, $dri_path . '/' . $file);
        }
    }
}

function am_createFont() {
    $template_dir = get_template_directory();
    $template_url = get_template_directory_uri();
    $cssFontFile  = fopen($template_dir . "/css/font.css", "w");

    fwrite($cssFontFile, "@font-face {" . PHP_EOL);
    fwrite($cssFontFile, "font-family: 'Glyphicons Halflings';" . PHP_EOL);
    fwrite($cssFontFile, "src: url('" . $template_url . "/fonts/glyphicons-halflings-regular.eot');" . PHP_EOL);
    fwrite($cssFontFile, "src: url('" . $template_url . "/fonts/glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('" . $template_url . "/fonts/glyphicons-halflings-regular.woff') format('woff'), url('" . $template_url . "/fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('" . $template_url . "/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg'), url('" . $template_url . "/fonts/glyphicons-halflings-regular.woff2') format('woff2');" . PHP_EOL);

    fclose($cssFontFile);
}

function am_get_php_content($path, $model = null, $parent = null) {
    include $path;
}

function am_get_css_file($path) {

    $fileContent = fopen($path, "r");

    $content = fread($fileContent, filesize($path));

    fclose($fileContent);

    echo '<style>' . $content . '</style>';
}

function am_add_script_file($path) {

    echo '<script src="' . $path . '" ></script>';
}

function am_add_css_file($path) {

    echo '<link type="text/css" rel="stylesheet" href="' . $path . '" ></link>';
}

function am_json_decode(&$jsonData, $isArray = false) {
    if (strpos($jsonData, '\\') >= 0) {
        return json_decode(stripslashes($jsonData), $isArray);
    }
    else {
        return json_decode($jsonData, $isArray);
    }
}

function am_json_encode(&$object, $option = JSON_UNESCAPED_UNICODE) {
    $re = json_encode($object, $option);

    if ($re === false) {
        if (function_exists("json_recursion_encode") == false) {
            function json_recursion_encode(&$object, $option, &$list, $parent) {
                $isIndexArray = null;
                $isArray      = is_array($object);

                $child         = new stdClass();
                $child->value  = $object;
                $child->parent = $parent;

                foreach ($object as $key => $value) {
                    $p = $parent;
                    while ($p != null) {
                        if ($p->value == $value)
                            break;
                        $p = @$p->parent;
                    }
                    if ($p)
                        continue;
                    if ($key === 0 && $isArray)
                        $isIndexArray = true;

                    if ($isIndexArray == false)
                        $list[] = "\"$key\":";

                    if (is_array($value)) {
                        $list[] = "[";
                        json_recursion_encode($value, $option, $list, $child);
                        $list[] = "]";

                    }
                    else if (is_object($value)) {
                        $list[] = "{";
                        json_recursion_encode($value, $option, $list, $child);
                        $list[] = "}";
                    }
                    else
                        $list[] = json_encode($value, $option);
                }
            }

            $parent        = new stdClass();
            $parent->value = $object;
            $list          = is_array($object) ? ["["] : ["{"];
            json_recursion_encode($object, $option, $list, $parent);
            $list[] = is_array($object) ? "]" : "}";

            $re = join("", $list);
        }
    }
    return $re;
}

function logAM($msg, $backCount = 0) {
    $stack = debug_backtrace();
    $mes   = [$msg];
    $mes[] = "\n\tTrace ";

    $len = sizeof($stack) - 1;
    for ($i = $backCount; $i < $len; $i++) {
        $mes[] = $stack[$i]['line'];
        $mes[] = " ";
        $mes[] = $stack[$i + 1]['function'];
        $mes[] = " ";
        $mes[] = $stack[$i]['file'];
        $mes[] = "\t";

    }
    error_log(join('', $mes));
}

function dev_trace() {
    $mes = "";
    foreach (debug_backtrace() as $item)
        $mes = $mes . "\n\t" . $item['function'] . "\t" . "Line " . $item['line'] . " at " . $item['file'];

    error_log($mes);
}


if (!class_exists('AMRespone')) {
    if($_SERVER['SERVER_NAME'] == 'itportal'){
        define('devmode', true);
    }

    class AMRespone {
        public $code;
        public $data;
        public $message;
        public $exception;

        public static function paramsInvalid($data = null, $message = null) {
            $msg = "Missing request data";
            if ($message)
                $msg = "$msg: $message";
            return new AMRespone($data, AMResponeCode::$paramsInvalid, $msg, null, -1, 1);
        }

        public static function jsonFailed($jsonData = null) {
            return new AMRespone($jsonData, AMResponeCode::$jsonFailed, 'Invalid request data', null, -1, 1);
        }

        public static function dbFailed($data = null, $message = null) {
            $msg = "Database action failed";
            if ($message)
                $msg = "$msg: $message";
            return new AMRespone($data, AMResponeCode::$dbFailed, $msg, null, -1, 1);
        }

        public static function notFound($data = null) {
            return new AMRespone($data, AMResponeCode::$notFound, "Data not found", null, -1, 1);
        }

        public static function exception($data = null, $exception = null) {
            return new AMRespone($data, AMResponeCode::$exception, 'Exception', $exception, -1, 1);
        }

        public static function busFailed($data, $message = null, $backcount = 0) {
            $msg = "Business failed";
            if ($message)
                $msg = "$msg: $message";

            return new AMRespone($data, AMResponeCode::$busFailed, $msg, null, -1, $backcount + 1);
        }

        public static function denied($data = null, $message = null, $backcount = 0){
            $msg = "Access denied";
            if ($message)
                $msg = "$msg: $message";

            return new AMRespone($data, AMResponeCode::$denied, $msg, null, -1, $backcount + 1);
        }

        public function __construct(&$data = 1, $code = null, $message = null, $exception = null, $dontLog = -1, $backCount = 0) {
            if ($code == null)
                $code = AMResponeCode::$ok;

            $this->code      = $code;
            $this->message   = $message;

            if ($code == AMResponeCode::$ok)
                $this->data = $data;

            if ($code != AMResponeCode::$ok && $_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
                $this->exception = $exception;
                $this->data = $data;
            }

            if ($dontLog == false)
                return;

            $weirdo = $code != AMResponeCode::$ok || $message != null || $exception != null;
            if ($dontLog == -1 && $weirdo == false)
                return;

            if (is_object($data) || is_array($data))
                $data = am_json_encode($data);

            $logMsgArray   = [];
            $logMsgArray[] = "AMRespone log: " . $message;
            $logMsgArray[] = "\n\t\tdata: " . $data;
            if ($weirdo) {
                $logMsgArray[] = "\n\t\tcode: " . $code;
                $logMsgArray[] = "\n\t\tmessage: " . $message;
                $logMsgArray[] = "\n\t\texception: " . $exception;
            }

            $logMsg = join($logMsgArray, "");
            logAM($logMsg, $backCount + 1);

            $logMsg = str_replace("\n", "<br>", $logMsg);
            $logMsg = str_replace("\t", "&emsp;", $logMsg);
        }

        /**
         * @param null $title
         * @param null $message
         * @param null role|email
         *          default as admin
         */
        public function report($title = null, $message = null, $receiver = null) {
            try {

                if ($title == null)
                    $title = $this->message;
                if ($message == null)
                    $message = 'Error code: ' . $this->code;

                if($receiver == null)
                    $receiver = RoleID::$admin;

                if(is_numeric($receiver)){
                    $roleAPI      = using('plugin.itportal.API.RoleAndPermission.APIRoleAndPermission');
                    $adminDomains = $roleAPI->GetDomainsByRole(['RoleID' => $receiver]);

                    $adminDomains[] = 'itportaldev';
                    $receiver             = join('@vng.com.vn, ', $adminDomains) . '@vng.com.vn';
                }

                if(defined('devmode')  == false)
                    wp_mail($receiver, $title, $message);
                else
                    wp_mail('tiennv6@vng.com.vn', $title, $message);
            } catch (Exception $ex) {
                AMRespone::exception($this, $ex);
            }
        }
    }

    class AMResponeCode {
        // custom
        public static $paramsInvalid = 601;
        public static $jsonFailed    = 611;
        public static $dbFailed      = 621;
        public static $exception     = 701;
        public static $busFailed     = 701;

        // html code standard
        public static $ok       = 200;
        public static $denied   = 403;
        public static $notFound = 404;
    }
}

?>