<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/15/2017
 * Time: 5:46 PM
 */
if(!class_exists('HttpResponse')) {
    //import your package here
    class HttpResponse
    {
        public $reponseText = '';
        public $header = array();
        public function __construct($request){
            $response = curl_exec($request);
            $header_size = curl_getinfo($request, CURLINFO_HEADER_SIZE);
            $this->header = curl_getinfo($request);
            $this->reponseText = substr( $response, $header_size );
        }
    }
}
?>