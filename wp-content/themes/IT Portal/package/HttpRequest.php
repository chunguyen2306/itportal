<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/15/2017
 * Time: 5:46 PM
 */
if(!class_exists('HttpRequest')) {
    //import your package here
    import('theme.package.HttpResponse');
    class HttpRequest
    {
        private $opts = null;
        public function __construct($args){
            $this->opts = $args;
        }

        public function send($isAsync = false){
            $process = curl_init();
            $fullUrl = $this->opts['url'];

            if(!isset($this->opts['method'])){
                $this->opts['method'] = 'GET';
            }
            curl_setopt($process, CURLOPT_CUSTOMREQUEST, $this->opts['method']);
            curl_setopt($process, CURLOPT_URL, $fullUrl);
            curl_setopt($process, CURLOPT_HEADER, 1);

            if(isset($this->opts['certificate'])){
                curl_setopt($process, CURLOPT_USERPWD, $this->opts['certificate']['username'] . ":" . $this->opts['certificate']['password']);
            }

            if(!isset($this->opts['header'])){
                $this->opts['header'] = array();
            }

            //array_push($this->opts['header'], "Content-Type: application/x-www-form-urlencoded");
            //array_push($this->opts['header'], "Content-Length: 19");


            curl_setopt($process, CURLOPT_HTTPHEADER, $this->opts['header']);
            curl_setopt($process, CURLOPT_TIMEOUT, 0);
            //curl_setopt($process, CURLOPT_POST, 1);
            //curl_setopt($process, CURLOPT_SAFE_UPLOAD, false);
            curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);

            if(isset($this->opts['data'])){
                if($this->opts['method'] == 'GET'){
                    $queryStr = '';
                    foreach($this->opts['data'] as $key => $vals){
                        $queryStr = $queryStr.$key.'='.$vals.'&';
                    }

                    $queryStr = substr($queryStr, 0, -1);
                    //error_log($queryStr);
                    $fullUrl = $this->opts['url'].'?'.$queryStr;
                } else {
                    curl_setopt($process, CURLOPT_POSTFIELDS, $this->opts['data']);
                }
            }

            if(isset($this->opts['body'])){
                curl_setopt($process, CURLOPT_POSTFIELDS, json_encode($this->opts['body']));
            }

            $respone = new HttpResponse($process);

            curl_close($process);
            return $respone;
        }
    }
}
?>