<?php
/**
 * Created by PhpStorm.
 * User: CPU10722-local
 * Date: 6/9/2017
 * Time: 11:36 AM
 */
if(!class_exists('RouterActionHook')){
    import ('theme.package.Abstracts.AbstractDatabase');
    import ('theme.package.View');
    class RouterActionHook {

        public function doFilter(){
            $module = $_GET['ammodule'];
            $page = $_GET['page'];
            $themeDir = get_template_directory();

            if($module == null || $module == '') {
                View::am_get_view($themeDir.'/Module', 'notfound', null);
            } else {
                View::am_get_view($themeDir.'/Module/'.$module, $page, null);
            }
        }
    }
}
?>