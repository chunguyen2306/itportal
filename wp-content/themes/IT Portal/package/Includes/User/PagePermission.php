<?php
return [
    'admin-page.cost.discountcost' => 'Admins',
    'admin-page.gui.component' => 'Admins',
    'admin-page.gui.component-type' => 'Admins',
    'admin-page.gui.main-menu' => 'Admins',
    'admin-page.gui.menu-type' => 'Admins',
    'admin-page.gui.resource-category' => 'Admins',
    'admin-page.gui.resource-type' => 'Admins',
    'admin-page.gui.personal-menu' => 'Admins',
    'admin-page.order.search-order' => 'Admins',
    'admin-page.permissions.grant' => 'Admins',
    'admin-page.permissions.list' => 'Admins',
    'admin-page.workflow.definition' => 'Admins',
    'admin-page.workflow.index' => 'Admins',
    'admin-page.workflow.list-workflow' => 'Admins',
    'admin-page.workflow.list-workflow-model-group' => 'Admins',
    'admin-page.workflow.mail-approval' => 'Admins',
    'admin-page.workflow.mapping-setting' => 'Admins',
    'admin-page.workflow.sear-config' => 'Admins',
    'admin-page.workflow.request-category' => 'Admins',
    'admin-page.workflow.request-reason' => 'Admins',
    'admin-page.workflow.request-type' => 'Admins',
    'admin-page.workflow.workflow-model-group' => 'Admins',
    'admin-page.asset' => 'Admins',
    'admin-page.assetmodel' => 'Admins',
    'admin-page.assettype' => 'Admins',
    'admin-page.order.search-order' => 'Admins',
    'admin-page.permissions.grant' => 'Admins',
    'admin-page.permissions.grant-old' => 'Admins',
    'admin-page.permissions.list' => 'Admins',
    'admin-page.search.config' => 'Admins',
    'admin-page.search.list-config' => 'Admins',
    'admin-page.search.search-order' => 'Admins',
    'user-page.order.order' => 'Admins,Help Desk'
];

/*
    'admin-page.gui.component' => 'Admin',
    'admin-page.gui.component-type' => 'Admin',
    'admin-page.gui.main-menu' => 'Admin',
    'admin-page.gui.menu-type' => 'Admin',
    'admin-page.gui.resource-category' => 'Admin',
    'admin-page.gui.resource-type' => 'Admin',
    'admin-page.order.search-order' => 'Admin',
    'admin-page.permissions.grant' => 'Admin',
    'admin-page.permissions.list' => 'Admin',
    'admin-page.workflow.definition' => 'Admin',
    'admin-page.workflow.definition-b' => 'Admin',
    'admin-page.workflow.definitiono1' => 'Admin',
    'admin-page.workflow.index' => 'Admin',
    'admin-page.workflow.list-workflow' => 'Admin',
    'admin-page.workflow.mail-approval' => 'Admin',
    'admin-page.workflow.mapping-setting' => 'Admin',
    'admin-page.workflow.sear-config' => 'Admin',
    'admin-page.asset' => 'Admin',
    'admin-page.assetmodel' => 'Admin',
    'admin-page.assettype' => 'Admin',
    'user-page.order.order' => 'Admin,Help Desk',
 */