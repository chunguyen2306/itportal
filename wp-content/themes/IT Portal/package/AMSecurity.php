<?php


/**
 * DESCRIPTION:
 *   Sử dụng security document cho api function để check quyền truy cập
 *   mặc định nên sử dụng biến global $security
 *
 * SECURITY DOCUMENT SYNTAX:
 *   @security [feature-name1|security-role1:security-role-param11,security-role-param12]| ...
 *
 * TAGs:
 * feature-name: tên tính năng, hard code. vd: workflow.request.view, asset.view
 * security-role: các quyền hạn đặc biệt
 * security-role-param: tên tham số chứa giá trị mà security-role yêu cầu
 *
 * security-role list and param types
 *      - owner: [domain]
 *      - depthead: [emp-domain]
 *      - reporting-line: [emp-domain]
 *      - api-custom: no-params
 *
 * EXAMPLEs:
 *  vd1: @security resource.model.edit
 *  Allow cho các role được sử dụng tính năng resource.model.edit
 *
 *  vd2: @security resource.asset.view|owner:username
 *  Allow cho các role được sử dụng tính năng resource.asset.view
 *  Allow cho user 'username'. 'username' là tên tham số truyền vào API
 *
 *  vd3: @security workflow.request.view|secret-key:secretKey,workflow.request.view|api-custom
 *  Allow cho các role được sử dụng tính năng resource.asset.view
 *  Allow cho secret key 'secretKey' được quyền truy cập tính năng workflow.request.view.
 *       'secretKey' là tên tham số truyền vào API
 *
 *  Allway allow cho api-custom.
 *      api-custom sẽ ngăn security reject request trước khi gọi vào api
 *      sử dụng trong các trường hợp api cần check thêm xem user có quyền gì đặc biệt hơn không
 *      trường hợp api muốn biết user đã pass vòng security bên ngoài chưa, sử dụng biến global $security->allow
 *
 */
class AMSecurity {
    public $allow;
    public $haveAPICustom;

    private $_lazy;

    public function __construct() {
        $this->_lazy = new stdClass();
    }

    public function checkAPI($className, $methodName, $params) {
        $securityDoc = $this->getSecurityDoc($className, $methodName);
        if ($securityDoc == null) {
            $this->allow = null;
            return true;
        }

        return $this->checkBySecurityDoc($securityDoc, $params);
    }

    public function checkBySecurityDoc($securityDoc, $params) {
        $securityInfos = $this->getSecurityInfo($securityDoc);

        $this->allow = false;
        foreach ($securityInfos as $securityInfo) {
            $featureName = $securityInfo[0];
            $checker     = $this->getCheker($featureName);
            $result      = false;
            if ($checker) {
                $checkerParams = $this->getCheckerParams($securityInfo[1], $params);
                $result        = call_user_func_array(array($this, $checker), $checkerParams);

            }
            else {
                $result = $this->checkFeature($featureName);
            }

            if ($result) {
                $this->allow = true;
                return $this->allow;
            }
        }

        return $this->allow;
    }

    public function checkReportingLine($empDomain) {
        $reportingLineDomain = $this->getUserFac()->GetReportingLineDomain($empDomain);

        if (!$reportingLineDomain)
            return false;

        return $this->getDomain() == $reportingLineDomain;
    }

    public function checkDeptHead($empDomain) {
        $empInfo = $this->getUserFac()->GetUserInformationByDomain($empDomain);

        $roleAPI = $this->_lazy->apiRole;
        if (!$roleAPI) {
            $roleAPI              = using('plugin.itportal.API.RoleAndPermission.APIRoleAndPermission');
            $this->_lazy->apiRole = $roleAPI;
        }

        $role           = $roleAPI->GetDeptHeadByDeptName(['deptName' => $empInfo->Department]);
        $deptHeadDomain = $role->Domain;

        if (!$deptHeadDomain)
            return false;

        return $this->getDomain() == $deptHeadDomain;
    }

    public function checkOwner($domain) {
        if (!$domain)
            return false;

        return $this->getDomain() == $domain;
    }

    public function getRoles() {
        $re = $this->_lazy->roles;
        if ($re)
            return $re;

        import('plugin.itportal.Factory.AMUserRoleMappingFactory');
        $facUserRole = new AMUserRoleMappingFactory();
        $domain      = $this->getDomain();
        $roleMapping = $facUserRole->query([
            'filter' => [
                ["Domain = '$domain'"]
            ]
        ]);

        if ($roleMapping) {
            $re = json_decode($roleMapping[0]->Permission);
        }

        if ($re) {
            if (in_array(2, $re) == false)
                $re[] = 2;
        }
        else {
            $re = [2];
        }

        $this->_lazy->roles = $re;
        return $re;
    }

    public function checkFeature($featureName) {
        $fac = $this->getRoleFeatureFac();
        return $fac->checkFeaturePermissionByRoleIDs($featureName, $this->getRoles());
    }

    function getRoleFeatureFac() {
        if ($this->_lazy->facRoleFeature)
            return $this->_lazy->facRoleFeature;

        import('plugin.itportal.Factory.AMRoleFeatureFactory');
        $this->_lazy->facRoleFeature = new AMRoleFeatureFactory();

        return $this->_lazy->facRoleFeature;
    }

    /**
     * @return AMUserFactory
     */
    function getUserFac() {
        if ($this->_lazy->facUser)
            return $this->_lazy->facUser;

        import('plugin.itportal.Factory.AMUserFactory');
        $this->_lazy->facUser = new AMUserFactory();

        return $this->_lazy->facUser;
    }

    function getDomain() {
        if (isset($this->_lazy->loginDomain))
            return $this->_lazy->loginDomain;


        //Get current user
        $currentUser = wp_get_current_user();
        if ($currentUser == 0 || $currentUser->user_login == false)
            return null;

        if ($currentUser->user_login)
            $this->_lazy->loginDomain = $currentUser->user_login;
        else
            $this->_lazy->loginDomain = false;

        return $this->_lazy->loginDomain;
    }

    /**
     * Set flag specify API have customs security check
     * @param $dummyParam  no use -> pass not thing
     * @return false allway
     */
    function setAPICustomFlag($dummyParam = null) {
        $this->haveAPICustom = true;
        return false;
    }

    function getSecurityDoc($className, $methodName) {
        try {
            // get method doc
            $ref = new ReflectionMethod("$className::$methodName");
            $doc = $ref->getDocComment();

            if ($doc === false)
                return null;

            // get security doc
            $tagName = '@security';
            $start   = strpos($doc, $tagName);
            if ($start === false)
                return null;

            $start += strlen($tagName);
            $end   = $start + 1;
            $n     = strlen($doc);
            for (; $end < $n; $end++) {
                if ($doc[$end] == "\n" || $doc[$end] == '*')
                    break;
            }

            $secutiryDoc = substr($doc, $start, $end - $start);
            return trim($secutiryDoc);

        } catch (ReflectionException $refException) {
            AMRespone::exception("$className::$methodName", $refException);
            return false;
        }
    }

    /**
     * @param $securityDoc feature-name1:[param-info11,param-info12]|feature-name2:[param-info21,param-info22]|..
     * @return
     *      array (
     *          [feature-name1, param-infos1],
     *           ...
     *      )
     *      null => no security
     */
    function getSecurityInfo($securityDoc) {
        $securityDoc = trim($securityDoc);

        // security doc to security info
        $re = [];
        foreach (explode('|', $securityDoc) as $item) {
            if (!$item)
                continue;

            $re[] = explode(':', $item);
        }
        return $re;
    }

    function getCheckerParams($paramInfos, $paramValues) {
        $re = [];
        foreach (explode(',', $paramInfos) as $paramName) {
            $paramName = trim($paramName);
            $re[]      = $paramValues[$paramName];
        }

        return $re;
    }

    function getCheker($featureName) {
        if ($featureName == 'owner')
            return 'checkOwner';
        else if ($featureName == 'depthead')
            return 'checkDeptHead';
        else if ($featureName == 'reporting-line')
            return 'checkReportingLine';
        else if ($featureName == 'api-custom')
            return 'setAPICustomFlag';
        else
            return null;
    }
}