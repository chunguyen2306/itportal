<?php
/**
 * Created by PhpStorm.
 * User: CPU10722-local
 * Date: 6/14/2017
 * Time: 11:20 AM
 */
return array(
    array(
        regex => '^\/(.+)\/([^\?|\/]*)(.*)',
        query => 'index.php?ammodule={m1}&page={m2}',
        role => 'Admin'
    ),
    array(
        regex => '^\/(.+)\/(.+)',
        query => 'index.php?ammodule=workflow&page=RequestDefinition',
        role => 'Admin'
    )
);