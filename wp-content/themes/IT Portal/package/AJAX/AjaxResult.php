<?php

/**
 * Created by PhpStorm.
 * User: LAP10266-local
 * Date: 4/12/2017
 * Time: 5:41 PM
 */
if(!class_exists('AjaxResult')) {
    class AjaxResult
    {
        public $isError = false;
        public $data = null;
        public $message = "";

        public function __construct($_data = null, $_mess = "", $_isError = false)
        {
            $this->isError = $_isError;
            $this->data = $_data;
            $this->message = $_mess;
        }
    }
}