<?php
/**
 * Created by PhpStorm.
 * User: tientm2
 * Date: 10/10/2018
 * Time: 4:38 PM
 */
import('plugin.itportal.Factory.AMRoleFeatureFactory');
if(!class_exists('AMPermissionChecker')) {
    class AMPermissionChecker
    {
        private $documentRegexPattern = '/\@Permission([^@]*)/m';

        public function checkAPI($className, $methodName){
            $permissionInfo = $this->getPermissionInfos($className, $methodName);
            var_dump($permissionInfo);
        }

        private function getPermissionInfos($className, $methodName){
            $methodInfo = new ReflectionMethod("$className::$methodName");
            $methodDoc = $methodInfo->getDocComment();
            $permissionDoc = '';
            $permissionInfos = [];

            preg_match($this->documentRegexPattern, $methodDoc, $matches);

            if(sizeof($matches) > 1){
                $permissionDoc = $matches[1];
            }

            preg_match_all('/[^\\n]\*(.+[:].+)\\n/', $permissionDoc, $pemissionInfoMatches);

            foreach ($pemissionInfoMatches[1] as $pemissionInfo){
                $data = explode(':',$pemissionInfo);
                $key = trim($data[0]);
                $value = trim($data[1]);
                $permissionInfos[$key] = $value;
                if(method_exists($this, "is$key")){
                    $permissionInfos["is$key"] = call_user_func(array($this, "is$key"));
                }
            }

            if(sizeof($permissionInfos) > 0){
                return $permissionInfos;
            } else {
                return null;
            }
        }

        private function isReportingLine(){
            return true;
        }

        private function isOwner(){
            return true;
        }

        private function isDepHead(){
            return true;
        }

        public function checkPermission($featureName){
            $roleFeatureFac = new AMRoleFeatureFactory();
            return $roleFeatureFac->checkFeaturePermissionByRoleIDs($featureName, $this->getRoles());
        }
    }
}