<?php
/**
 * Created by PhpStorm.
 * User: tientm2
 * Date: 7/17/2018
 * Time: 5:51 PM
 */
class ArrayFilterWithProperty{
    private $propName = '';
    private $propValue = '';

    function __construct($prop, $value) {
        $this->propName = $prop;
        $this->propValue = $value;
    }

    private function convertToArray($data) {

        if (is_object($data)) {
            $data = get_object_vars($data);
        }

        if (is_array($data)) {
            return array_map(null, $data);
        }
        else {
            return $data;
        }
    }

    public function equal($i) {
        return $this->convertToArray($i)[$this->propName] === $this->propValue;
    }
}