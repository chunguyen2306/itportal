<?php

/**
 * Created by PhpStorm.
 * User: tientm2
 * Date: 8/14/2018
 * Time: 4:18 PM
 */
if(!class_exists('AMAsyncTask')) {
    import('theme.package.HttpRequest');
    import('theme.package.HttpResponse');
    class AMAsyncTask
    {
        private $headerPrefix = 'AM-Task';

        /**
         * AMAsyncTask constructor.
         * @param $agrs [
         *      $action string  : Action to do
         *      $data   array   : Argument to do
         * ]
         */
        private $args = [];

        public function __construct($_agrs){
            $this->args = $_agrs;
        }

        public function RunTask(){
            wp_remote_post( site_url().'/am_ajax', array(
                    'headers'       => [
                        "U-Class"   =>"AsyncTask",
                        "U-Type"    =>"Task",
                        "U-Method"  =>"DoTask",
                    ],
                    'blocking'      => fasle,
                    'timeout'       => 0,
                    'body'          => $this->args
                )
            );
        }
    }
}