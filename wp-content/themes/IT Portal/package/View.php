<?php
if(!class_exists('View')){
    class View {
        public static function am_get_view($dir, $name, $model = null, $parent = null) {
            am_get_php_content($dir.'/'.$name.'.php', $model, $parent);
        }
    }
}
?>