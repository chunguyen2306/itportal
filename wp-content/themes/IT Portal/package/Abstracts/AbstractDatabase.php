<?php
if (!class_exists('AbstractDatabase')) {
    abstract class AbstractDatabase {
        abstract protected function tableInfo();

        /**
         * trỏ tới mảng chứa dữ liệu chuẩn như trong database
         * @param $data
         * @return mixed
         */
        public function standardizeData(&$sourceArrayData) {
            $colums = $this->getColumnsForStandardizeData();
            if (is_array($colums)) {
                $re = [];
                foreach ($colums as $col) {
                    if (isset($sourceArrayData[$col]))
                        $re[$col] = $sourceArrayData[$col];
                }
                $sourceArrayData = $re;
            }
        }

        public function update(&$data, $where) {
            global $wpdb;
            $tableInfo = $this->tableInfo();
            return $wpdb->update($tableInfo['table_name'], $data, $where);
        }

        public function insert(&$data, $isLog = false) {
            global $wpdb;
            $tableInfo = $this->tableInfo();

            return $wpdb->insert($tableInfo['table_name'], $data);
        }

        public function truncate() {
            global $wpdb;
            $tableInfo = $this->tableInfo();
            return $wpdb->query("TRUNCATE TABLE `wp_questions`");
        }

        public function delete($where) {
            global $wpdb;
            $tableInfo = $this->tableInfo();
            return $wpdb->delete($tableInfo['table_name'], $where);
        }

        public function deleteByIDs($ids) {
            if(!$ids)
                return 0;

            global $wpdb;
            $temp = join(', ', $ids);
            $tableName = $this->tableInfo()['table_name'];
            $query = "DELETE FROM $tableName WHERE ID IN ($temp)";

            return $wpdb->query($query);
        }

        public function custom_delete($where, $log = false) {
            global $wpdb;
            $tableInfo = $this->tableInfo();
            $quertStr  = 'DELETE FROM ' . $tableInfo['table_name'];


            if (is_array($where) && sizeof($where) > 0) {
                $quertStr = $quertStr . ' WHERE';
                foreach ($where as $item) {
                    $quertStr = $quertStr . ' ' . $item[0] . ' ' . $item[1];
                }
            }else{
                AMRespone::dbFailed($where, 'where have to be array and can not be empty');
                return false;
            }
            if ($log)
                error_log($quertStr);

            $wpdb->query($quertStr);
            return $wpdb->rows_affected;
        }

        public function getOneBy($fields = array()) {
            return $this->getBy($fields, true);
        }

        public function getAllBy($fields = array()) {
            return $this->getBy($fields, false);
        }

        public function getBy($fields, $isSingle) {

            $agrs = array();
            if (is_array($fields) && sizeof($fields) != 0) {
                $agrs['filter'] = array();
                foreach ($fields as $key => $value) {
                    if (gettype($value) == 'string') {
                        $value = "'" . $value . "'";
                    }
                    $filter = array($key . " = " . $value,
                        "");

                    array_push($agrs['filter'], $filter);
                }
            }

            if ($isSingle) {
                return $this->getOne($agrs);
            }
            else {
                return $this->query($agrs);
            }
        }

        public function getOne($agr, $log = false) {
            $items = $this->query($agr, $log);
            if ($items === false)
                return false;

            if (sizeof($items) > 0)
                return $items[0];
            else
                return null;
        }

        public function getByID($ID) {
            $items = $this->query(array('filter' => array(array("ID = '$ID'", ''))));

            if ($items === false)
                return false;
            if (sizeof($items) == 1)
                return $items[0];
            else
                return null;
        }

        public function query($agr = array(), $log = false, $output = OBJECT) {
            global $wpdb;
            $tableInfo = $this->tableInfo();
            $quertStr  = 'SELECT';

            if (isset($agr['select'])) {
                $quertStr = $quertStr . ' ' . $agr['select'];
            }
            else {
                $quertStr = $quertStr . ' *';
            }
            $quertStr = $quertStr . ' FROM ' . $tableInfo['table_name'] . ' a';

            if(isset($agr['from'])) {
                $quertStr = $quertStr.$agr['from'];
            }

			if(isset($agr['join'])) {
				foreach ($agr['join'] as $join){
					$quertStr = $quertStr.' '.$join[0].' '.$join[1].' ON '.$join[2].' '.(isset($join[4])?$join[4]:'=').' '.$join[3];
				}
			}

            if (isset($agr['filter'])) {
                $quertStr = $quertStr . ' WHERE';
                foreach ($agr['filter'] as $filter) {
                    $quertStr = $quertStr . ' ' . $this->esc_filter($filter[0]) . ' ' . $filter[1];
//                    $quertStr = $quertStr . ' ' . $filter[0] . ' ' . $filter[1];
                }
            } else if (isset($agr['where'])) {
                $quertStr = $quertStr . ' WHERE';
                $orWhere = '';
                $andWhere = '';
                foreach ($agr['where'] as $filter) {
                    if(strtoupper($filter[1]) == 'OR'){
                        $orWhere = $orWhere . ' ' . $filter[0] . ' ' . strtoupper($filter[1]);
                    } else {
                        $andWhere = $andWhere . ' ' . $filter[0] . ' ' . strtoupper($filter[1]);
                    }
                }

                $orWhere = rtrim(rtrim($orWhere, ' '), 'OR');
                $andWhere = rtrim(rtrim($andWhere, ' '), 'AND');

                $quertStr = "$quertStr ".($orWhere!==''?"($orWhere)":$orWhere).
                                " ".($orWhere!=='' && $andWhere !== ''?'AND':'')." ".($andWhere!==''?"($andWhere)":$andWhere);


//                $quertStr = "$quertStr ".(
//                    $orWhere?"($orWhere)": ' '). " "
//                    .($orWhere && $andWhere ?'AND':'')." "
//                    .($andWhere ?"($andWhere)": ' ');

                //error_log($quertStr);
            }

            if (isset($agr['order'])) {
                $quertStr = $quertStr . ' ORDER BY ' . $agr['order'];
            }

            if (isset($agr['limit'])) {
                $quertStr = $quertStr . ' LIMIT ' . $agr['limit']['at'] . ', ' . $agr['limit']['length'];
            }


            // tiennv6 update
            // fix lỗi: tất cả các trường dữ liệu trả về đều là string
            mysqli_options($wpdb->dbh, MYSQLI_OPT_INT_AND_FLOAT_NATIVE, true);
//

            if ($log)
                error_log("$quertStr");

            $result = $wpdb->get_results($quertStr, $output);

            return $result;
        }

        public function custom_query($queryString){
            global $wpdb;

            // tiennv6 update
            // fix lỗi: tất cả các trường dữ liệu trả về đều là string
            mysqli_options($wpdb->dbh, MYSQLI_OPT_INT_AND_FLOAT_NATIVE, true);

            $result = $wpdb->get_results($queryString, OBJECT);
            return $result;
        }

        public function custom_query2($queryString, $args = null, $log = false){
            global $wpdb;

            if($args){
                foreach ($args as $key => $val){
                    if(is_array($val)){
                        $valBuilder = ['('];
                        $isFirstRow = true;
                        foreach ($val as $item){
                            if(!$isFirstRow)
                                $valBuilder[] = ',';

                            $valBuilder[] = '\'';
                            $valBuilder[] = $wpdb->_escape($item);
                            $valBuilder[] = '\'';
                            $isFirstRow = false;
                        }

                        $valBuilder = [')'];
                        $val = join('', $valBuilder);
                    }else{
                        $val = $wpdb->esc_like($wpdb->_escape($val));
                    }

                    $queryString = str_replace($key, $val, $queryString);
                }
            }
            

            // tiennv6 update
            // fix lỗi: tất cả các trường dữ liệu trả về đều là string
            mysqli_options($wpdb->dbh, MYSQLI_OPT_INT_AND_FLOAT_NATIVE, true);

            if($log)
                error_log($queryString);

            $result = $wpdb->get_results($queryString, OBJECT);
            return $result;
        }

        protected function getColumnsForStandardizeData(){
            $tableInfo = $this->tableInfo();
            return $tableInfo["columns"];
        }

        function esc_filter($filter){
            // prase like
            $pattern = '/([^\s]+)\s+([lL][iI][kK][eE])(\'.+)/';
            preg_match($pattern, $filter, $matches);

            // prase in
            if(sizeof($matches) != 4){
                $pattern = '/([^\s]+)\s+([iI][nN])(\(.+)/';
                preg_match($pattern, $filter, $matches);
            }

            // prase bin operators
            if(sizeof($matches) != 4){
                $pattern = '/([^=>!<]+)([!=><]{1,3})(.+)/';
                preg_match($pattern, $filter, $matches);
            }

            // parse chuẩn
            if(sizeof($matches) != 4){
                $pattern = '/([^\s]+)\s+([^\s]+)\s+(.+)/';
                preg_match($pattern, $filter, $matches);
            }

            if(sizeof($matches) != 4){
                $ex = new Exception('Can not parse filter to ESC');
                AMRespone::exception($filter, $ex);
            }

            $operator = strtolower($matches[2]);

            $value = $matches[3];
            if($operator == 'in'){
                $value = trim($value, " ()\t\n\r\0\x0B");
                $array = explode(',', $value);
                foreach ($array as $i => $iValue)
                    $array[$i] = $this->esc_value($iValue);

                $value = '(' . join(',', $array) . ')';
            }else{
                $value = $this->esc_value(trim($value));
            }

            //            error_log($filter . '|' . $matches[1] . ',' . $operator . ',' . $value);
            return $matches[1] . ' ' . $operator . ' ' . $value;
        }

        function esc_value($value){
            if(is_numeric($value) || is_string($value) == false)
                return $value;


            $size = strlen($value);
            $requiredQoute = $size > 1
                && (
                    $value[0] === '\'' && $value[$size - 1] === '\''
                    || $value[0] === '"' && $value[$size - 1] === '"'
                );
            if($requiredQoute){
                $value = substr($value, 1, $size - 2);
            }

            global $wpdb;
            $value = $wpdb->_escape($value);
            return $requiredQoute ? "'" . $value . "'" : $value;
        }
    }
}
?>