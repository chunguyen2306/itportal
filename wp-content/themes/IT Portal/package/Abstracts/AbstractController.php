<?php
if(!class_exists('AbstractController')){
    import ('theme.package.View');

    abstract class AbstractController
    {
        public $model = array();
        public $viewMapping = array();
        protected $db = null;

        public function __construct($args = array()) {
            $this->model = array();
            if(isset($args ['slug'])){
                $this->_model('slug', $args ['slug']);
            }
            $this->init($args);
        }

        public function index() {
            $viewType = $_REQUEST ['type'];
            $action = $_REQUEST ['action'];
            $this->model['errors'] = array();

            if($viewType == '' || $viewType == null){
                $viewType = 'list';
            }

            if(isset($action)){
                call_user_func(array($this, 'do_'.$action), $_REQUEST, $_POST);
            }

            call_user_func(array($this, $viewType.'_action'), $_REQUEST, $_POST);

            //$this->beforeGetView($_REQUEST, $_POST);
            $this->get_view($viewType, $this->model);
        }

        //protected abstract function beforeGetView($req, $post);

        protected function _model($name = null, $value = null){
            if($name != null){
                if($value != null){
                    $this->model[$name] = $value;
                } else {
                    return $this->model[$name];
                }
            } else {
                return $this->model;
            }
        }

        protected function _getPath(){
            return site_url().'/wp-admin/admin.php?page='.$this->_model('slug');
        }

        protected function get_view($viewType, $model) {
            View::am_get_view( $this->dir.'/template', $this->viewMapping[$viewType], $model, $this);
        }

        protected function init(){

        }
    }


}
?>