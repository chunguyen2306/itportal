<?php
/**
 * Created by PhpStorm.
 * User: LAP10637-local
 * Date: 4/3/2017
 * Time: 10:21 AM
 */
if(!class_exists('AbstractPlugin')) {
    if ( ! function_exists( 'import' ) ) {
        require_once get_template_directory() . '/package/import.php';
    }

    abstract class AbstractPlugin{

        protected $adminMenu = array();
        protected $ajaxName = '';
        protected $pluginName = '';
        public $db = array();
        public $model = array();

        public function __construct()
        {
            $this->init_hook();
        }

        private function init_hook(){

            add_action('init', array($this, 'init'), 0);
        }

        public function init(){
//            error_log('init');
            $this->custom_init();
            $this->init_custom_hook();
            add_action( 'am_ajax_'.$this->ajaxName, array($this, 'doAjaxAction') );
            add_action('admin_menu', array($this, 'init_admin_menu'));
        }

        public function doAjaxAction(){
            $_Param = null;
            if (empty($_POST)){
                $_Param = $_GET;
            } else{
                $_Param = $_POST;
            }

            $type = isset($_Param['t'])?$_Param['t']:null;
            $method = isset($_Param['m'])?$_Param['m']:null;

            if($type != null && $method != null) {
                $ajaxClass = using('plugin.' . $this->pluginName . '.class.ajax.' . $type . 'Ajax');
                $result = call_user_func(array($ajaxClass, $method),$_Param);
                echo is_string($result) ? $result : json_encode($result);
            } else {
                echo 'method not found';
            }
        }

        abstract protected function init_custom_hook();
        abstract protected function custom_init();

        public function init_admin_menu(){
            foreach ($this->adminMenu as $menuItem){
                add_menu_page(
                    $menuItem['pageTitle'],
                    $menuItem['menuTitle'],
                    $menuItem['capability'],
                    $menuItem['menuSlug'],
                    $menuItem['handler'],
                    $menuItem['icon']
                );

                if(isset($menuItem['child'])){
                    foreach ($menuItem['child'] as $subMenu){
                        if(isset($subMenu['default']) && $subMenu['default'] == true){
                            $subMenu['menuSlug'] = $menuItem['menuSlug'];
                            $subMenu['handler'] = null;
                        }
                        add_submenu_page(
                            $menuItem['menuSlug'],
                            $subMenu['pageTitle'],
                            $subMenu['menuTitle'],
                            $subMenu['capability'],
                            $subMenu['menuSlug'],
                            $subMenu['handler']
                        );
                    }
                }
            }
        }
    }
}