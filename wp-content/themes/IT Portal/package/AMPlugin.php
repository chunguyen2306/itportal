<?php
/**
 * Created by Administrator on 3/9/2017
 */

if(!class_exists('AMPlugin')){
    //import your package here
    
    class AMPlugin{
        //Your code here

        public function getPluginDir($pluginName = null){
            if($pluginName == null){
                $pluginName = '';
            }else{
                $pluginName = '/'.$pluginName;
            }

            return ABSPATH.'wp-content/plugins'.$pluginName;
        }

    }

    $GLOBALS['AMPlugin'] = new AMPlugin();
}
?>