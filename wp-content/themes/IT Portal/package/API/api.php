<?php
/**
 * Created by PhpStorm.
 * User: CPU11642-local
 * Date: 10-Mar-17
 * Time: 11:36
 */

status_header(200);

$uData = $_POST["u"];
$pData = $_POST["p"];
$vData = $_POST["v"];

if($uData == null){
    http_response_code(403); // Forbidden
    exit("Missing required parameter (null)");
}

$postData = [];
if(isset($pData)){

    if(($p = json_decode(stripslashes($pData), true)) == false){
        echo "JSON decode failed:\np = ";
        exit ($pData);
    }

    if(($v = json_decode(stripslashes($vData), true)) == false){
        echo"JSON decode failed:\nu = ";
        exit ($vData);
    }

    $n = sizeof($p);
    if($n == 0)
        exit ("Missing required parameter (no parameter)");

    for($i = 0 ; $i < $n; $i++){
        $postData[$p[$i]] = $v[$i];
    }
}


$_URL = [
    "cmdb" => "http://localhost:8175/API/cmdb.ashx",
    "cmdbex" => "http://localhost:8175/API/cmdbex.ashx",
    "asset" => "http://localhost:8176/API/asset.ashx"
];

$u = $_URL[$uData];
if($u == null){
    http_response_code(404); // Forbidden
    exit("Server not found: " . $uData);
}


$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($postData)
    )
);
$context  = stream_context_create($options);
$result = file_get_contents($u, false, $context);
if ($result === FALSE) {
    http_response_code(403); // Forbidden
    exit("Missing require parameter");
}
echo $result;
exit();


//$request = new HttpRequest($u, HttpRequest::METH_POST);
//$request->addPostFields($postFiled);
//
//try{
//    echo $request->send()->getBody();
//}catch (Exception $ex){
//    echo "call service with exception";
//    echo ex;
//}