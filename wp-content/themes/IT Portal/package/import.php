<?php
/**
 * Created by Administrator on 3/9/2017
 */

if (!function_exists('get_plugins')) {
    require_once ABSPATH . 'wp-admin/includes/plugin.php';
}

function getFullPath($package, $exten, $type = 'path') {
    $package = str_replace('.', '/', $package);

    $packPath       = explode('/', $package);
    $path           = '';
    $packageMapping = array();
    if ($type == 'path') {
        $packageMapping = array(
            'theme' => get_template_directory(),
            'plugin' => ABSPATH . 'wp-content/plugins',
            'upload' => ABSPATH . 'wp-content/uploads',
            'include' => ABSPATH . 'wp-includes'
        );
    }
    else {
        $packageMapping = array(
            'theme' => get_template_directory_uri(),
            'plugin' => site_url() . '/wp-content/plugins',
            'upload' => site_url() . '/wp-content/uploads',
            'include' => site_url() . '/wp-includes',
            'home' => site_url()
        );
    }

    $filePath = substr($package, strlen($packPath[0]) + 1);

    $returnPath = $path . $packageMapping[$packPath[0]] . '/' . $filePath;

    if($exten != ''){
        $returnPath = $returnPath.'.' . $exten;
    }

    return $returnPath;
}

function using($package) {
    return include getFullPath($package, 'php');
}

function import($package, $model = null) {
    include_once getFullPath($package, 'php');
}

function importScript($name, $package) {
    $path = getFullPath($package, 'js', 'uri');
    wp_enqueue_script($name, $path, null, null, true);
}

function importCSS($name, $package) {
    error_log(getFullPath($package, 'css', 'uri'));
    wp_enqueue_style($name, getFullPath($package, 'css', 'uri'));
}

function getContent($package){
    return file_get_contents(getFullPath($package, 'php'));
}

function importJs($name, $package, $isExten = true) {
    $path = getFullPath($package, '', 'uri', true);
    $path = $path.'/'.$name.($isExten ? '.js' : '');
//    echo '<script type="text/javascript" src="'.$path.'?v='.time().'"></script>';


    global $_AppVersion;
    echo '<script type="text/javascript" src="'.$path.'?v='.$_AppVersion.'"></script>';
}

function importStyle($name, $package) {
    $path = getFullPath($package, '', 'uri', true);
    $path = $path.'/'.$name.'.css';
//    echo '<link type="text/css" rel="stylesheet" href="'.$path.'?v='.time().'"/>';
    global $_AppVersion;
    echo '<link type="text/css" rel="stylesheet" href="'.$path.'?v='.$_AppVersion.'"/>';
}

function importResource($package){
    $fileList = array_diff(scandir(getFullPath($package, '', 'path')), array('..', '.'));

    foreach ($fileList as $file){
        $fileName = pathinfo($file)['filename'];
        if(strpos($file, 'css') === false){
            importJs($fileName, $package);
        } else {
            importStyle($fileName, $package);
        }
    }
}

import('plugin.itportal.config.AppVersion');
?>