<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'j*Yi?wnFOpgco+%eh#;kJ*D#)Jli9  vE,e9qn2I;<Jz?vCiuz$UPxNW&+?wXJhZ');
define('SECURE_AUTH_KEY',  'AI|sJ=m*J!J;Id(e-;M_4d])h weV<RJE]ywNB,[<81bBURqg{|N`UI.y_#3NJ,c');
define('LOGGED_IN_KEY',    '53   QpkQdZRKwX#3jfp,-u+&v&ZF[ZevKGV5!}QJ4(Am(=EpHoi5659v}}CmSz{');
define('NONCE_KEY',        'p5g~di|dW]Z=g7Y}] J%:`.):n]|>,9)-7R50Iq62N)58?}0$F)d>SGT#{WjF-j4');
define('AUTH_SALT',        'Wbnj6LU;EGWQ[)G=*Ye`296sa7Ka_6i=zpA;4=x[Ue%(U3a6w/.9c2J!GK2y&_67');
define('SECURE_AUTH_SALT', ' 7PP_+fWor#G-jSZ].Wa= #szFwa:_QMCdQo8Sk9_&6RtaKgsj`ZI:_bWw`Z1$/`');
define('LOGGED_IN_SALT',   'w.@k<@LNIQ/y5~:o^(#.e@051:ju%e701FZ})afU,u@h(*p8vi>9xg-b(P;L:6t3');
define('NONCE_SALT',       ']._*R>M.pz[W614>K)G27+3PG4&,!fbM.8D,)F{U8$J GG2 WMQR@*sAV!qHY![>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
